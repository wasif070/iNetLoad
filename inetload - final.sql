CREATE DATABASE bdload;
use bdload;

CREATE TABLE `client` (
  `id` int(11) NOT NULL auto_increment,
  `client_id` varchar(50) NOT NULL,
  `client_type_id` int(3) NOT NULL,
  `client_parent_id` int(11) NOT NULL,
  `client_full_name` varchar(100) NOT NULL default '',
  `client_email` varchar(50) NOT NULL default '',
  `client_password` varchar(32) NOT NULL,
  `client_credit_all` decimal(18,4) NOT NULL default '0.0000',
  `client_status` tinyint(1) default '0',
  `client_reg_time` decimal(18,0) default '0',
  `client_deleted` tinyint(1) default '0',
  `client_version` int(3) NOT NULL default '0',
  `client_ip` varchar(200) NOT NULL default '',
  `client_previous_balance` decimal(18,4) NOT NULL default '0.0000',
  `client_discount` decimal(18,4) NOT NULL default 0.0,
  `client_deposit` decimal(18,4) NOT NULL default 0.0,
  `client_gp_active` tinyint(1) default '1',
  `client_rb_active` tinyint(1) default '1',
  `client_bl_active` tinyint(1) default '1',
  `client_tt_active` tinyint(1) default '1',
  `client_cc_active` tinyint(1) default '1',
  `client_wr_active` tinyint(1) default '1',
  PRIMARY KEY  (`id`)
);

CREATE TABLE `contact` (
  `id` int(11) NOT NULL auto_increment,
  `contact_name` varchar(50) NOT NULL default '',
  `contact_des` varchar(100) NOT NULL default '',
  `contact_group_id` int(11) NOT NULL default '0',
  `contact_phn_no` varchar(20) NOT NULL,
  `contact_type_id` int(3) NOT NULL,
  `contact_amount` decimal(18,4) NOT NULL default '0.0000',
  `contact_created_by` int(11) NOT NULL default '0',
  `status_id` int(3) NOT NULL default '1',
  `contact_delete` tinyint(1) default '0',  
  PRIMARY KEY  (`id`)
);

CREATE TABLE `contact_group` (
  `id` int(11) NOT NULL auto_increment,
  `group_name` varchar(50) NOT NULL default '',
  `group_des` varchar(100) NOT NULL default '',
  `group_created_by` int(11) NOT NULL default '0',
  `contact_group_delete` tinyint(1) default '0',   
  PRIMARY KEY  (`id`)
);

CREATE TABLE `contact_group_schedule` (
  `id` int(11) NOT NULL auto_increment,
  `schedule_group_id` int(11) NOT NULL default '0',
  `schedule_week_day` int(3) NOT NULL,
  `schedule_week_hour` int(3) NOT NULL,
  `schedule_week_minute` int(3) NOT NULL,
  `schedule_type` int(3) NOT NULL default '0',  
  `schedule_amount` decimal(18,4) NOT NULL default '0.0000',
  `schedule_executedat` decimal(18,0) NOT NULL default 0,
  `schedule_description` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`id`)
);

create table rechargecard_group(
`id` int(11) NOT NULL AUTO_INCREMENT,
`group_name` varchar(50) NOT NULL default '',
`group_des` varchar(250) NOT NULL default '',
`group_prefix` varchar(10) NOT NULL default '',
`group_created_time` decimal(18,0) NOT NULL default 0,
`group_balance` decimal(18,4) NOT NULL default 0.0,
`group_created_by` int(11) NOT NULL default 0,
`group_total_cards` decimal(10,0) NOT NULL,
`group_card_length` decimal(6,0) NOT NULL,
`group_country` decimal(3,0) NOT NULL default '0',
`group_package` decimal(3,0) NOT NULL default '0',
`group_identifier` varchar(75) NOT NULL,
`group_deleted` tinyint(1) default '0',
PRIMARY KEY (`id`));

create table rechargecard(
`id` int(11) NOT NULL AUTO_INCREMENT,
`card_no` varchar(100) NOT NULL,
`serial_no` int(11) NOT NULL,
`status_id` decimal(3,0) NOT NULL default '0',
`country_id` decimal(3,0) NOT NULL default '0',
`package_id` decimal(3,0) NOT NULL default '0',
`card_balance` decimal(18,4) NOT NULL default 0.0,
`activated_time` decimal(18,0) NOT NULL default 0,
`activated_by` int(11) NOT NULL default 0,
`blocked_time` decimal(18,0) NOT NULL default 0,
`blocked_by` int(11) NOT NULL default 0,
`group_identifier` varchar(75) NOT NULL,
`card_price` decimal(18,4) NOT NULL default 0.0,
`card_shop` int(11) NOT NULL default 0,
`card_deleted` tinyint(1) default '0',
`refill_no` varchar(20) NOT NULL default '',
PRIMARY KEY (`id`));

CREATE TABLE `inetload_fdr` (
  `inetload_id` int(11) NOT NULL auto_increment,
  `operator_type_id` int(4) NOT NULL,
  `inetload_from_user_id` int(11) NOT NULL,
  `inetload_from_client_type_id` int(3) NOT NULL default '2',
  `inetload_to_user_id` int(11) NOT NULL,
  `inetload_to_client_type_id` int(3) NOT NULL default '1',
  `inetload_phn_no` varchar(20) NOT NULL,
  `inetload_type_id` int(3) NOT NULL,
  `inetload_amount` decimal(18,4) NOT NULL default '0.0000',
  `inetload_gw_id` int(11) NOT NULL default '0',
  `inetload_time` decimal(18,0) NOT NULL,
  `inetload_refill_success_time` decimal(18,0) NOT NULL default '0',
  `inetload_processed_time` decimal(18,0) NOT NULL default '0',
  `inetload_status` tinyint(1) NOT NULL default '1',
  `inetload_processing_status` tinyint(1) NOT NULL default '0',
  `inetload_marker` varchar(50) NOT NULL default '',
  `inetload_transaction_id` varchar(250) default '',
  `inetload_message` varchar(250) default '',
  `card_id` int(11) NOT NULL default '0',
  `status_changed` tinyint(1) default '0',
  `inetload_contact_id` int(11) NOT NULL default '0',
  `inetload_group_id` int(11) NOT NULL default '0',  
  PRIMARY KEY  (`inetload_id`),
  KEY `operator_type_id_index` (`operator_type_id`),
  KEY `inetload_from_user_id_index` (`inetload_from_user_id`),
  KEY `inetload_to_user_id_index` (`inetload_to_user_id`),
  KEY `inetload_gw_id_index` (`inetload_gw_id`),
  KEY `inetload_time_index` (`inetload_time`),
  KEY `inetload_refill_success_time_index` (`inetload_refill_success_time`),
  KEY `inetload_status_index` (`inetload_status`),
  KEY `inetload_processing_status_index` (`inetload_processing_status`),
  KEY `inetload_marker_index` (`inetload_marker`)
) ;

CREATE TABLE `inetload_gateway` (
  `id` int(11) NOT NULL auto_increment,
  `operator_id` decimal(3,0) NOT NULL,
  `sim_id` varchar(25) NOT NULL,
  `phone_number` varchar(20) NOT NULL default '',
  `pin` varchar(15) NOT NULL default '',
  `balance` decimal(18,4) NOT NULL default '0.0000',
  `status_id` decimal(3,0) NOT NULL default '0',
  `update_time` decimal(18,0) NOT NULL default '0',
  `version` int(3) NOT NULL default '0',
  `dealer_id` int(11) NOT NULL default '0', 
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL auto_increment,
  `payment_from_client_type_id` int(3) NOT NULL,
  `payment_to_client_type_id` int(3) NOT NULL,
  `payment_from_client_id` int(11) NOT NULL,
  `payment_to_client_id` int(11) NOT NULL,
  `payment_type_id` int(3) NOT NULL default '1',
  `payment_amount` decimal(18,4) NOT NULL,
  `payment_time` decimal(18,0) NOT NULL,
  `payment_description` varchar(100) NOT NULL default '',
  `payment_marker` varchar(50) NOT NULL default '',
  `payment_discount` decimal(18,4) NOT NULL default 0.0, 
  `payment_amount_temp` decimal(18,4) NOT NULL default '0.0000',
  `payment_discount_temp` decimal(18,4) NOT NULL default '0.0000', 
  PRIMARY KEY  (`payment_id`)
) ;

CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `role_id` varchar(50) NOT NULL,
  `role_description` varchar(100) default NULL,
  `role_created_by` int(11) NOT NULL,
  `role_permission_id` varchar(70) NOT NULL,
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL auto_increment,
  `role_permission_id` varchar(70) NOT NULL,
  `role_permission_item_id` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE `session_values` (
  `user_name` varchar(100) NOT NULL default '',
  `password` varchar(100) NOT NULL default '',
  `time` varchar(100) NOT NULL default '',
  `current_time` decimal(18,0) NOT NULL default '0'
) ;

CREATE TABLE `sim_log` (
  `id` int(11) NOT NULL auto_increment,
  `gateway_id` int(11) NOT NULL default '0',
  `message` varchar(250) default '',
  `log_time` decimal(18,0) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `log_time_index` (`log_time`)
) ;

CREATE TABLE `sim_recharge` (
  `id` int(11) NOT NULL auto_increment,
  `gateway_id` int(11) NOT NULL default '0',
  `recharge_amount` decimal(18,4) NOT NULL,
  `recharge_time` decimal(18,0) NOT NULL default '0',
  `recharge_description` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`id`)
);

CREATE TABLE `card_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(50) NOT NULL,
  `shop_owner` varchar(50) NOT NULL default '',
  `shop_description` varchar(100) NOT NULL default '',
  `shop_address` varchar(100) NOT NULL default '',
  `shop_contact_number` varchar(30) NOT NULL default '',
  `shop_deleted` tinyint(1) default '0',
  PRIMARY KEY (`id`)
);

CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` varchar(50) NOT NULL default '',
  `user_full_name` varchar(100) NOT NULL default '',
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_status` tinyint(1) default '0',
  `user_reg_time` decimal(18,0) default '0',
  `user_role_id` int(11) NOT NULL,
  `user_createdBy_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE `dealer` (
  `id` int(11) NOT NULL auto_increment,
  `dealer_name` varchar(100) NOT NULL default '',
  `dealer_address` varchar(250) NOT NULL default '',
  `dealer_phone` varchar(30) NOT NULL,
  `dealer_status` tinyint(1) default '0',
  `dealer_create_date` decimal(18,0) default '0',
  `dealer_delete` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE `dealer_transaction` (
  `transaction_id` int(11) NOT NULL auto_increment,
  `transaction_dealer_id` int(11) NOT NULL,
  `transaction_type_id` int(3) NOT NULL default '1',
  `transaction_amount` decimal(18,4) NOT NULL,
  `transaction_time` decimal(18,0) NOT NULL,
  `transaction_description` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`transaction_id`)
) ;

CREATE TABLE `settings` (
  `prepaid_min_refill_amount` decimal(18,4) NOT NULL default 10.0,
  `postpaid_min_refill_amount` decimal(18,4) NOT NULL default 50.0,
  `prepaid_max_refill_amount` decimal(18,4) NOT NULL default 1000.0,
  `postpaid_max_refill_amount` decimal(18,4) NOT NULL default 5000.0,
  `min_flexi_sim_balance` decimal(18,4) NOT NULL default 500.0,
  `min_allowed_discount` decimal(18,4) NOT NULL default -3.0,
  `max_allowed_discount` decimal(18,4) NOT NULL default 3.0,
  `same_number_refill_interval` int(11) NOT NULL default 15,
  `offset_from_server_time` int(11) NOT NULL default 0,
  `record_per_page_for_reporting` int(11) NOT NULL default 100,
  `sim_message_log_restore_interval` int(11) NOT NULL default 7,
  `data_backup_time` int(11) NOT NULL default 15,
  `notify_after_pending` int(11) NOT NULL default 25,   
  `pending_time_for_auto_system` int(11) NOT NULL default 6,
  `pending_time_for_manual_system` int(11) NOT NULL default 24,
  `emergency_notice_text` varchar(500) NOT NULL default '',
  `brand_name` varchar(50) NOT NULL default '',
  `motto` varchar(100) NOT NULL default '',
  `welcome_note` varchar(250) NOT NULL default '',
  `copyright_link` varchar(150) NOT NULL default '',
  `contact_email` varchar(100) NOT NULL default '',
  `contact_email_pass` varchar(50) NOT NULL default '',
  `notification_email` varchar(100) NOT NULL default '',
  `last_log_id` decimal(18,0) NOT NULL  default 0,
  `last_payment_id` decimal(18,0) NOT NULL default 0 
) ;

CREATE TABLE `inetload_api` (
  `id` int(11) NOT NULL auto_increment,
  `api_id` varchar(50) NOT NULL default '',
  `api_link` varchar(100) NOT NULL default '',
  `api_user` varchar(50) NOT NULL,
  `api_password` varchar(50) NOT NULL,
  `api_status` tinyint(1) default '0',
  `api_gp_active` tinyint(1) default '1',
  `api_rb_active` tinyint(1) default '1',
  `api_bl_active` tinyint(1) default '1',
  `api_tt_active` tinyint(1) default '1',
  `api_cc_active` tinyint(1) default '1',
  `api_wr_active` tinyint(1) default '1',  
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE `scratchcard` (
  `id` int(11) NOT NULL auto_increment,
  `company_name` varchar(50) NOT NULL default '',
  `refill_type` varchar(50) NOT NULL default '',
  `refill_amount` varchar(50) NOT NULL default '',  
  `serial_no` varchar(50) NOT NULL default '', 
  `pin` varchar(50) NOT NULL default '',
  `status_id` tinyint(1) default '0',
  `used_by_id` int(11) NOT NULL  default 0,
  `used_time` decimal(18,0) NOT NULL default 0,   
  PRIMARY KEY  (`id`)
) ; 

CREATE TABLE `scratchcard_type` (
  `id` int(11) NOT NULL auto_increment,
  `company_name` varchar(50) NOT NULL default '',
  `supported_refill_types` varchar(100) NOT NULL default '',
  `supported_refill_amounts` varchar(100) NOT NULL default '',  
  `status_id` tinyint(1) default '2',
  PRIMARY KEY  (`id`)
) ;

CREATE TABLE `sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_id` int(11) NOT NULL default '0',
  `sms_type` int(11) NOT NULL DEFAULT '0',
  `mobile_no` varchar(20) NOT NULL DEFAULT '',
  `sms` varchar(250) NOT NULL DEFAULT '',
  `log_time` decimal(18,0) NOT NULL DEFAULT '0',
  `processing_status` tinyint(1) NOT NULL default '0',
  `request_marker` varchar(50) NOT NULL default '',
  PRIMARY KEY (`id`)
);

CREATE TABLE `bt_surcharge` (
  `id` int(11) NOT NULL auto_increment,
  `bt_type_id` int(3) NOT NULL,
  `bt_max_amount` decimal(18,4) NOT NULL,
  `bt_min_amount` decimal(18,4) NOT NULL,
  `bt_surcharge_amount` decimal(18,4) NOT NULL,
  PRIMARY KEY  (`id`)
) ;

drop VIEW reseller2;
drop VIEW reseller3;
drop VIEW reseller;

CREATE VIEW reseller2 AS SELECT * FROM client WHERE client_type_id = 4;
CREATE VIEW reseller3 AS SELECT * FROM client WHERE client_type_id = 3;
CREATE VIEW reseller AS SELECT * FROM client WHERE client_type_id = 1;

ALTER TABLE `client` add column `total_recharged` decimal(18,4) NOT NULL default '0.0000';
ALTER TABLE `client` add column `total_received` decimal(18,4) NOT NULL default '0.0000';
ALTER TABLE `client` add column `total_used` decimal(18,4) NOT NULL default '0.0000';
ALTER TABLE `client` add column `client_ani` varchar(100) NOT NULL default '';
ALTER TABLE `client` add column `client_bt_active` tinyint(1) default '1';
ALTER TABLE `inetload_gateway` add column `sim_type` decimal(3,0) NOT NULL DEFAULT '2';
ALTER TABLE `inetload_gateway` add column `query` varchar(50) NOT NULL DEFAULT '';
ALTER TABLE `inetload_fdr` add column `surcharge` decimal(18,4) NOT NULL default '0.0000';

//ALTER TABLE `settings` add column `api_user` varchar(50) NOT NULL default '';
//ALTER TABLE `settings` add column `api_password` varchar(50) NOT NULL default '';
//For new DB
INSERT INTO `user` VALUES (1,'admin','Flamma','flamma@flammabd.com','aaaaaa',1,'0',0,0);
INSERT INTO `settings` VALUES (10.0,50.0,1000.0,5000.0,500.0,-3.0,3.0,15,0,100,7,15,25,6,24,'','iNetLoad','Best Solution for Your Online Mobile Refill System','Welcome to iNetLoad','http://inetload.com','flammabd@gmail.com','flamma@sylhet','support@flammabd.com',0,0);
//For old DB
CREATE TABLE client_old SELECT * FROM client;
delete from client where client_deleted=1;
delete from contact_group where group_created_by not in(select id from client);
delete from contact where contact_group_id not in(select id from contact_group);
delete from inetload_gateway where status_id=-1;
update client set client_previous_balance=client_credit_all;
update client set total_recharged=0, total_received=0, total_used=0;
update payment set payment_amount_temp=-1.0 where payment_amount=0.0 and payment_amount_temp=0.0;
update settings set last_log_id=0, last_payment_id=0;
update inetload_gateway set sim_type=2;

ALTER TABLE `inetload_fdr` ADD UNIQUE KEY `inetload_fdr_unique_key` (`inetload_from_user_id`, `inetload_to_user_id`, `inetload_marker`);

GRANT ALL PRIVILEGES ON bdload.* TO flexi@'%' IDENTIFIED BY 'flamma' WITH GRANT OPTION;
FLUSH PRIVILEGES;

//For DB Backup
//mysqldump -u root bdload > bdload.sql

//Restore DB Backup
//source /root/bdload.sql