package sendmail;

import com.myapp.struts.system.SettingsLoader;
import java.util.Date;
import java.util.Properties;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SimpleMail {

    static String host = "smtp.gmail.com";

    public SimpleMail() {
    }

    public synchronized static boolean sendMail(String from, String fromName, String to, String subject, String text, String cc) {
        try {
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.setProperty("mail.transport.protocol", "smtps");
            props.put("mail.smtp.user", SettingsLoader.getInstance().getSettingsDTO().getContactEmail());
            props.put("mail.smtp.password", SettingsLoader.getInstance().getSettingsDTO().getContactEmailPass());
            props.put("mail.smtp.port", "465");
            props.put("mail.smtps.auth", "true");
            Session session = Session.getDefaultInstance(props, null);
            MimeMessage message = new MimeMessage(session);
            InternetAddress fromAddress = null;
            InternetAddress toAddress = null;

            fromAddress = new InternetAddress(from, fromName);
            toAddress = new InternetAddress(to);

            message.setFrom(fromAddress);
            message.setRecipient(RecipientType.TO, toAddress);
            if (cc != null) {
                message.addRecipients(RecipientType.CC, cc);
            }
            message.setSubject(subject);
            message.setText(text);
            message.setSentDate(new Date());

            Transport transport = session.getTransport("smtps");
            transport.connect(host, SettingsLoader.getInstance().getSettingsDTO().getContactEmail(), SettingsLoader.getInstance().getSettingsDTO().getContactEmailPass());
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean validEmailID(String emailID) {
        String at = "@";
        String dot = ".";
        int lat = emailID.indexOf(at);
        int lstr = emailID.length();

        if (emailID.indexOf(at) == -1 || emailID.indexOf(at) == 0 || emailID.indexOf(at) == lstr) {
            return false;
        }

        if (emailID.indexOf(dot) == -1 || emailID.indexOf(dot) == 0 || emailID.indexOf(dot) == lstr) {
            return false;
        }

        if (emailID.indexOf(at, (lat + 1)) != -1) {
            return false;
        }

        if (emailID.substring(lat - 1, lat).equals(dot) || emailID.substring(lat + 1, lat + 2).equals(dot)) {
            return false;
        }

        if (emailID.indexOf(dot, (lat + 2)) == -1) {
            return false;
        }

        if (emailID.indexOf(" ") != -1) {
            return false;
        }
        return true;
    }
}
