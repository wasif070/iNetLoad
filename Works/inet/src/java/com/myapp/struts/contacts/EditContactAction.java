package com.myapp.struts.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;

import com.myapp.struts.util.MyAppError;

public class EditContactAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        ContactForm formBean = (ContactForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            if (!login_dto.getIsUser() && login_dto.getClientTypeId()==Constants.CLIENT_TYPE_AGENT ) {
                request.setAttribute(mapping.getAttribute(), formBean);

                ContactDTO dto = new ContactDTO();
                ContactTaskScheduler scheduler = new ContactTaskScheduler();

                dto.setContactName(formBean.getContactName());
                dto.setContactNo(formBean.getContactNo());
                dto.setGroupID(formBean.getGroupID());
                dto.setRefillAmount(formBean.getRefillAmount());
                dto.setRefillType(formBean.getRefillType());
                dto.setContactDescription(formBean.getContactDescription());
                dto.setContactCreatedBy(login_dto.getId());
                
                MyAppError error = new MyAppError();
                long id = Long.parseLong(request.getParameter("id"));
                dto.setId(id);
                error = scheduler.editContact(dto);
                
                if (error.getErrorType() == MyAppError.NoError) {
                    formBean.setMessage(false, "Contact is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + request.getParameter("searchLink") + "&action=" + Constants.EDIT+"&groupID="+formBean.getGroupID(), true);
                    return changedActionForward;
                }                
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return mapping.findForward(target);
    }
}
