package com.myapp.struts.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.MyAppError;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;

import org.apache.log4j.Logger;

public class UploadContactsAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(UploadContactsAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String groupIDStr = request.getParameter("groupID");
        long groupID = 0;
        if (groupIDStr != null && login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            groupID = Long.parseLong(groupIDStr);
            if (!login_dto.getIsUser() && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                MyAppError error = new MyAppError();
                String contentType = request.getContentType();
                if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                    DataInputStream in = new DataInputStream(request.getInputStream());
                    int formDataLength = request.getContentLength();
                    byte dataBytes[] = new byte[formDataLength];
                    int byteRead = 0;
                    int totalBytesRead = 0;

                    while (totalBytesRead < formDataLength) {
                        byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                        totalBytesRead += byteRead;
                    }
                    String file = new String(dataBytes);
                    String saveFile = file.substring(file.indexOf("filename=\"") + 10);
                    logger.debug("saveFile=" + saveFile);
                    saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));
                    logger.debug("saveFile" + saveFile);
                    saveFile = file.substring(file.indexOf("filename=\"") + 10);
                    saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                    saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));
                    int lastIndex = contentType.lastIndexOf("=");
                    String boundary = contentType.substring(lastIndex + 1, contentType.length());
                    int pos;
                    pos = file.indexOf("filename=\"");
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;
                    pos = file.indexOf("\n", pos) + 1;
                    int boundaryLocation = file.indexOf(boundary, pos) - 4;
                    int startPos = ((file.substring(0, pos)).getBytes()).length;
                    int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
                    File csvFile = new File(saveFile);
                    FileOutputStream fileOut = new FileOutputStream(csvFile);
                    fileOut.write(dataBytes, startPos, (endPos - startPos));
                    fileOut.flush();
                    fileOut.close();
                    logger.debug("File " + saveFile + " has been uploaded.");
                    String line = null;
                    int lineNumber = 0;

                    try {
                        BufferedReader input = new BufferedReader(new FileReader(saveFile));
                        ContactTaskScheduler scheduler = new ContactTaskScheduler();
                        while ((line = input.readLine()) != null) {
                            lineNumber++;
                            String str[] = line.split(",");
                            if (str.length == 5) {
                                ContactDTO dto = new ContactDTO();

                                dto.setContactName(str[0]);
                                dto.setContactDescription(str[1]);
                                dto.setContactNo(str[2]);
                                dto.setGroupID(groupID);
                                dto.setRefillType(Integer.parseInt(str[3]));
                                dto.setRefillAmount(Double.parseDouble(str[4]));
                                dto.setContactCreatedBy(login_dto.getId());

                                if (dto.getContactNo() == null || dto.getContactNo().length() == 0) {
                                    error.setErrorType(MyAppError.ValidationError);
                                    error.setErrorMessage(dto.getContactName() + ": Invalid contact number.");
                                    target = "failure";
                                    break;
                                } else if (dto.getContactNo().length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                                    error.setErrorType(MyAppError.ValidationError);
                                    error.setErrorMessage(dto.getContactName() + ": Invalid contact number.");
                                    target = "failure";
                                    break;
                                } else if (dto.getContactNo().length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                                    String phoneNo = dto.getContactNo();
                                    if (phoneNo.startsWith("+")) {
                                        phoneNo = phoneNo.replace("+", "");
                                    }
                                    if (phoneNo.startsWith("88")) {
                                        phoneNo = phoneNo.replaceFirst("88", "");
                                    }
                                    phoneNo = phoneNo.replace("-", "");
                                    phoneNo = phoneNo.replace(" ", "");
                                    if (phoneNo.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                                        dto.setContactNo(phoneNo);
                                    } else {
                                        error.setErrorType(MyAppError.ValidationError);
                                        error.setErrorMessage(dto.getContactName() + ": Invalid contact number.");
                                        target = "failure";
                                        break;
                                    }
                                }
                                if (dto.getRefillAmount() <= 0.0) {
                                    error.setErrorType(MyAppError.ValidationError);
                                    error.setErrorMessage(dto.getContactName() + ": Invalid refill amount.");
                                    target = "failure";
                                    break;
                                } else if (dto.getRefillType() == Constants.REFILL_TYPE_PREPAID) {
                                    if (dto.getRefillAmount() < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || dto.getRefillAmount() > SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount()) {
                                        error.setErrorType(MyAppError.ValidationError);
                                        error.setErrorMessage(dto.getContactName() + ": Invalid prepaid refill amount.");
                                        target = "failure";
                                        break;
                                    }
                                } else if (dto.getRefillType() == Constants.REFILL_TYPE_POSTPAID) {
                                    if (dto.getRefillAmount() < SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() || dto.getRefillAmount() > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                                        error.setErrorType(MyAppError.ValidationError);
                                        error.setErrorMessage(dto.getContactName() + ": Invalid postpaid refill amount.");
                                        target = "failure";
                                        break;
                                    }
                                }
                                
                                error = scheduler.addContact(dto);
                                if (error.getErrorType() > 0) {
                                    target = "failure";
                                    error.setErrorMessage(dto.getContactName() + ": " + error.getErrorMessage());
                                    break;
                                }
                            } else {
                                error.setErrorType(MyAppError.ValidationError);
                                error.setErrorMessage("Column numbers are not matched at line : " + lineNumber);
                                target = "failure";
                                break;
                            }
                        }
                        csvFile.delete();
                    } catch (Exception e) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("Exception in reading csv file. Line Number: " + lineNumber);
                        target = "failure";
                    }
                }

                if (error.getErrorType() > 0) {
                    target = "failure";
                    request.getSession(true).setAttribute(Constants.MESSAGE, error.getErrorMessage());
                } else {
                    request.getSession(true).setAttribute(Constants.MESSAGE, "Contacts are added successfully.");
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?list_all=2&groupID=" + groupID + "&action=" + Constants.ADD, true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return mapping.findForward(target);
    }
}