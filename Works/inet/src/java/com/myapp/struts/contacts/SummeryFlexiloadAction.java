package com.myapp.struts.contacts;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.Utils;
import org.apache.log4j.Logger;

public class SummeryFlexiloadAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(SummeryFlexiloadAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            if (!login_dto.getIsUser() && (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) )  {
                int pageNo = 1;
                int list_all = 0;
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                ContactDTO cdto = new ContactDTO();
                ContactForm contactForm = (ContactForm) form;
                if (list_all == 0) {
                    if (contactForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.CONTACT_SUMMERY_RECORD_PER_PAGE, contactForm.getRecordPerPage());
                    }
                    cdto.setEndDay(contactForm.getEndDay());
                    cdto.setEndMonth(contactForm.getEndMonth());
                    cdto.setEndYear(contactForm.getEndYear());
                    cdto.setStartDay(contactForm.getStartDay());
                    cdto.setStartMonth(contactForm.getStartMonth());
                    cdto.setStartYear(contactForm.getStartYear());
                    cdto.setReportType(contactForm.getReportType());
                    if (contactForm.getContactNamePar() != null && contactForm.getContactNamePar().trim().length() > 0) {
                        cdto.searchWithContactName = true;
                        cdto.setContactName(contactForm.getContactNamePar().toLowerCase());
                    }
                    if (contactForm.getGroupNamePar() != null && contactForm.getGroupNamePar().trim().length() > 0) {
                        cdto.searchWithGroupName = true;
                        cdto.setGroupName(contactForm.getGroupNamePar().toLowerCase());
                    }                    
                } else {
                    if (request.getSession(true).getAttribute(Constants.CONTACT_SUMMERY_RECORD_PER_PAGE) != null) {
                        contactForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CONTACT_SUMMERY_RECORD_PER_PAGE).toString()));
                    }
                    String dateParts[] = Utils.getDateParts(System.currentTimeMillis()+ (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    contactForm.setStartDay(Integer.parseInt(dateParts[0]));
                    contactForm.setStartMonth(Integer.parseInt(dateParts[1]));
                    contactForm.setStartYear(Integer.parseInt(dateParts[2]));
                    contactForm.setEndDay(contactForm.getStartDay());
                    contactForm.setEndMonth(contactForm.getStartMonth());
                    contactForm.setEndYear(contactForm.getStartYear());
                    contactForm.setReportType(0);
                    cdto.setStartDay(contactForm.getStartDay());
                    cdto.setStartMonth(contactForm.getStartMonth());
                    cdto.setStartYear(contactForm.getStartYear());
                    cdto.setEndDay(contactForm.getEndDay());
                    cdto.setEndMonth(contactForm.getEndMonth());
                    cdto.setEndYear(contactForm.getEndYear());
                    cdto.setReportType(0);                    
                }

                ContactTaskScheduler scheduler = new ContactTaskScheduler();
                contactForm.setSummeryList(scheduler.getSummeryRefill(login_dto, cdto));
                if (contactForm.getSummeryList() != null && contactForm.getSummeryList().size() > 0 && contactForm.getSummeryList().size() <= (contactForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
