package com.myapp.struts.contacts;

import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import java.util.ArrayList;

public class ContactForm extends org.apache.struts.action.ActionForm {
    
    private int refillType;   
    private int doValidate;
    private int pageNo;
    private int recordPerPage;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int reportType;
    private long id;
    private long groupID;
    private double refillAmount;    
    private String contactDescription;
    private String contactName;
    private String contactNamePar;
    private String contactNo;
    private String contactNoPar;
    private String groupName; 
    private String groupNamePar;
    private String message;
    private ArrayList contactList;
    private ArrayList summeryList;
    private long[] selectedIDs;
    private double[] amounts;


    public ContactForm() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    
    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }    
    
    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }    

    public double getRefillAmount() {
        return refillAmount;
    }

    public void setRefillAmount(double refillAmount) {
        this.refillAmount = refillAmount;
    }

    public String getContactDescription() {
        return contactDescription;
    }

    public void setContactDescription(String contactDescription) {
        this.contactDescription = contactDescription;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
    
    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }  
    
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }   
    
    public String getContactNamePar() {
        return contactNamePar;
    }

    public void setContactNamePar(String contactNamePar) {
        this.contactNamePar = contactNamePar;
    }   
    
    public String getContactNoPar() {
        return contactNoPar;
    }

    public void setContactNoPar(String contactNoPar) {
        this.contactNoPar = contactNoPar;
    }       

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getRefillType() {
        return refillType;
    }

    public void setRefillType(int refillType) {
        this.refillType = refillType;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }    

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public ArrayList getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList offerList) {
        this.contactList = offerList;
    }
    
    public ArrayList getSummeryList() {
        return summeryList;
    }

    public void setSummeryList(ArrayList summeryList) {
        this.summeryList = summeryList;
    }    

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }
    
    public double[] getAmounts() {
        return amounts;
    }

    public void setAmounts(double[] amounts) {
        this.amounts = amounts;
    }    

    public String getMessage() {
        return message;
    }
    
    public String getGroupNamePar() {
        return groupNamePar;
    }

    public void setGroupNamePar(String groupNamePar) {
        this.groupNamePar = groupNamePar;
    }    

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getContactName() == null || getContactName().length() < 1) {
                errors.add("contactName", new ActionMessage("errors.contact_name.required"));
            }   
            if (getGroupID() <= 0) {
                errors.add("groupID", new ActionMessage("errors.group_id.required"));
            }  
            if (getContactNo() == null || getContactNo().length() == 0) {
                errors.add("contactNo", new ActionMessage("errors.contactNo.required"));
            } else if (getContactNo().length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                errors.add("contactNo", new ActionMessage("errors.invalidPhn"));
            } else if (getContactNo().length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                String phoneNo = getContactNo();
                if (phoneNo.startsWith("+")) {
                    phoneNo = phoneNo.replace("+", "");
                }
                if (phoneNo.startsWith("88")) {
                    phoneNo = phoneNo.replace("88", "");
                }
                phoneNo = phoneNo.replace("-", "");
                phoneNo = phoneNo.replace(" ", "");
                if (phoneNo.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                    setContactNo(phoneNo);
                } else {
                    errors.add("contactNo", new ActionMessage("errors.invalidPhn"));
                }
            }            
            if (!Utils.isDouble(request.getParameter("refillAmount").toString())) {
                errors.add("refillAmount", new ActionMessage("errors.refill_amount.valid"));
            } else if (getRefillAmount() <= 0.0) {
                errors.add("refillAmount", new ActionMessage("errors.refill_amount.required"));
            }else if (getRefillType() == Constants.REFILL_TYPE_PREPAID) {
                if (getRefillAmount() < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || getRefillAmount() > SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount()) {
                    errors.add("refillAmount", new ActionMessage("errors.prepaid.amount"));
                }
            } else if (getRefillType() == Constants.REFILL_TYPE_POSTPAID) {
                if (getRefillAmount() < SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() || getRefillAmount() > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                    errors.add("refillAmount", new ActionMessage("errors.postpaid.amount"));
                }
            }                
        }
        return errors;
    }
}
