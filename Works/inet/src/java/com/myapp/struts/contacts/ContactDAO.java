package com.myapp.struts.contacts;

import com.myapp.struts.contactgroup.ContactGroupDTO;
import com.myapp.struts.contactgroup.ContactGroupLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.MyAppError;
import java.sql.Statement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class ContactDAO {

    static Logger logger = Logger.getLogger(ContactDAO.class.getName());

    public ContactDAO() {
    }

    public ArrayList<ContactDTO> getContactDTOsWithSearchParam(ArrayList<ContactDTO> list, ContactDTO cdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ContactDTO dto = (ContactDTO) i.next();
                if ((cdto.searchWithContactName && !dto.getContactName().toLowerCase().startsWith(cdto.getContactName()))
                        || (cdto.searchWithContactNo && !dto.getContactNo().toLowerCase().startsWith(cdto.getContactNo()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<ContactDTO> getContactDTOsSorted(ArrayList<ContactDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ContactDTO dto1 = (ContactDTO) o1;
                    ContactDTO dto2 = (ContactDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addContact(ContactDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select contact_phn_no from contact where contact_delete=0 and contact_created_by=? and contact_phn_no=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setLong(1, dto.getContactCreatedBy());
            ps1.setString(2, dto.getContactNo());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Contact Number.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "select contact_name from contact where contact_delete=0 and contact_group_id=? and contact_name=?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setLong(1, dto.getGroupID());
            ps2.setString(2, dto.getContactName());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Contact Name.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into contact(contact_name,contact_des,contact_group_id,contact_phn_no,contact_type_id,contact_amount,contact_created_by) values(?,?,?,?,?,?,?);";
            ps3 = dbConnection.connection.prepareStatement(sql);

            ps3.setString(1, dto.getContactName());
            ps3.setString(2, dto.getContactDescription());
            ps3.setLong(3, dto.getGroupID());
            ps3.setString(4, dto.getContactNo());
            ps3.setInt(5, dto.getRefillType());
            ps3.setDouble(6, dto.getRefillAmount());
            ps3.setLong(7, dto.getContactCreatedBy());
            ps3.executeUpdate();
            ContactLoader.getInstance().forceReload();
            ContactGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding contact request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }   
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }  
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }              
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editContact(ContactDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select contact_phn_no from contact where contact_delete=0 and contact_created_by=? and contact_phn_no=? and id!=" + dto.getId();
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setLong(1, dto.getContactCreatedBy());
            ps1.setString(2, dto.getContactNo());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Contact Number.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "select contact_name from contact where contact_delete=0 and contact_group_id=? and contact_name=? and id!=" + dto.getId();
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setLong(1, dto.getGroupID());
            ps2.setString(2, dto.getContactName());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Contact Name.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update contact set contact_name=?, contact_des=?, contact_group_id=?, contact_phn_no=?, contact_type_id=?, contact_amount=? where contact_delete=0 and id=?";
            ps3 = dbConnection.connection.prepareStatement(sql);

            ps3.setString(1, dto.getContactName());
            ps3.setString(2, dto.getContactDescription());
            ps3.setLong(3, dto.getGroupID());
            ps3.setString(4, dto.getContactNo());
            ps3.setInt(5, dto.getRefillType());
            ps3.setDouble(6, dto.getRefillAmount());
            ps3.setLong(7, dto.getId());
            ps3.executeUpdate();
            ContactLoader.getInstance().forceReload();
            ContactGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing contact request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }   
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }  
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }            
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public MyAppError activateContacts(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update contact set status_id=" + Constants.USER_STATUS_ACTIVE + " where contact_delete=0 and id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ContactLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while activating contact: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }  
    
    public MyAppError blockContacts(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update contact set status_id=" + Constants.USER_STATUS_BLOCK + " where contact_delete=0 and id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ContactLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blocking contact: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }     

    public MyAppError deleteContacts(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update contact set contact_delete=1 where contact_delete=0 and id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ContactLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting contact: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList getRefillSummery(LoginDTO login_dto, ContactDTO c_dto) {
        ArrayList data = new ArrayList();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(c_dto.getStartYear(), c_dto.getStartMonth() - 1, c_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
        long end = new GregorianCalendar(c_dto.getEndYear(), c_dto.getEndMonth() - 1, c_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
        String sql = "select inetload_status,inetload_contact_id,inetload_amount,inetload_refill_success_time from inetload_fdr where inetload_status in(" + Constants.INETLOAD_STATUS_SUCCESSFUL + "," +  Constants.INETLOAD_STATUS_SUBMITTED + "," +  Constants.INETLOAD_STATUS_REJECTED + ") and inetload_to_user_id =" + login_dto.getId() 
                + " and (inetload_time between " + start + " and " + end + ") ";

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ContactDTO condto = ContactLoader.getInstance().getAllContactDTOByID(rs.getLong("inetload_contact_id"));
                if (condto != null) {
                    if (c_dto.searchWithContactName && !condto.getContactName().toLowerCase().startsWith(c_dto.getContactName()) && !condto.getContactNo().toLowerCase().startsWith(c_dto.getContactName())) {
                        continue;
                    }
                    if (c_dto.searchWithGroupName && !condto.getGroupName().toLowerCase().startsWith(c_dto.getGroupName())) {
                        continue;
                    }
                    ContactDTO dto = new ContactDTO();                   
                    dto.setContactDescription(condto.getContactDescription());
                    dto.setGroupName(condto.getGroupName());
                    dto.setContactName(condto.getContactName());
                    dto.setContactNo(condto.getContactNo());
                    dto.setStatusName(Constants.INETLOAD_STATUS_STRING[rs.getInt("inetload_status")]);
                    dto.setTotalRefillAmount(rs.getDouble("inetload_amount"));
                    dto.setReportDate(rs.getLong("inetload_refill_success_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                    data.add(dto);
                }
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getRefillSummery:" + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList getRefillSummeryByContact(LoginDTO login_dto, ContactDTO c_dto) {
        ArrayList data = new ArrayList();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(c_dto.getStartYear(), c_dto.getStartMonth() - 1, c_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
        long end = new GregorianCalendar(c_dto.getEndYear(), c_dto.getEndMonth() - 1, c_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));

        String sql = "select inetload_contact_id,sum(inetload_amount) as totalLoadAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id =" + login_dto.getId() 
                + " and (inetload_refill_success_time between " + start + " and " + end + ") group by inetload_contact_id";
        
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ContactDTO condto = ContactLoader.getInstance().getAllContactDTOByID(rs.getLong("inetload_contact_id"));
                if (condto != null) {
                    if (c_dto.searchWithContactName && !condto.getContactName().toLowerCase().startsWith(c_dto.getContactName()) && !condto.getContactNo().toLowerCase().startsWith(c_dto.getContactName())) {
                        continue;
                    }
                    if (c_dto.searchWithGroupName && !condto.getGroupName().toLowerCase().startsWith(c_dto.getGroupName())) {
                        continue;
                    }
                    
                    ContactDTO dto = new ContactDTO();
                    dto.setContactDescription(condto.getContactDescription());
                    dto.setGroupName(condto.getGroupName());
                    dto.setContactName(condto.getContactName());
                    dto.setContactNo(condto.getContactNo());
                    dto.setTotalRefillAmount(rs.getDouble("totalLoadAmount"));
                    data.add(dto);
                }
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getRefillSummeryByDate:" + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
    
    public ArrayList getRefillSummeryByGroup(LoginDTO login_dto, ContactDTO c_dto) {
        ArrayList data = new ArrayList();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(c_dto.getStartYear(), c_dto.getStartMonth() - 1, c_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
        long end = new GregorianCalendar(c_dto.getEndYear(), c_dto.getEndMonth() - 1, c_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));

        String sql = "select inetload_group_id,sum(inetload_amount) as totalLoadAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id =" + login_dto.getId() 
                + " and (inetload_refill_success_time between " + start + " and " + end + ") group by inetload_group_id";
        
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ContactGroupDTO groupDTO = ContactGroupLoader.getInstance().getAllContactGroupDTOByID(rs.getLong("inetload_group_id"));
                if (groupDTO != null) {
                    if (c_dto.searchWithGroupName && !groupDTO.getGroupName().toLowerCase().startsWith(c_dto.getGroupName())) {
                        continue;
                    }    
                    ContactDTO dto = new ContactDTO();
                    dto.setGroupName(groupDTO.getGroupName());
                    dto.setTotalRefillAmount(rs.getDouble("totalLoadAmount")); 
                    data.add(dto);
                }               
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getRefillSummeryByDate:" + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }    
}
