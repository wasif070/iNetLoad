package com.myapp.struts.contacts;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.flexi.FlexiDAO;
import com.myapp.struts.flexi.FlexiDTO;
import com.myapp.struts.flexi.FlexiTaskScheduler;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListContactAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(ListContactAction.class.getName());
    static SimpleDateFormat bangDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (!login_dto.getIsUser() && (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT)) {
            ContactForm contactForm = (ContactForm) form;
            if (!login_dto.getIsUser()) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }

                ContactTaskScheduler scheduler = new ContactTaskScheduler();

                int action = 0;
                if (request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        if (contactForm.getSelectedIDs() != null) {
                            int length = contactForm.getSelectedIDs().length;
                            long[] idListArray = contactForm.getSelectedIDs();
                            if (length > 0) {
                                idList += idListArray[0];
                                for (int i = 1; i < length; i++) {
                                    idList += "," + idListArray[i];
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.ACTIVATION:
                                        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.activateContacts(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            contactForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            contactForm.setMessage(false, "Contacts are activated successfully.");
                                        }
                                        request.setAttribute(Constants.CONTACT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, contactForm.getMessage());

                                        break;
                                    case Constants.BLOCK:
                                        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.blockContacts(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            contactForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            contactForm.setMessage(false, "Contacts are blocked successfully.");
                                        }
                                        request.setAttribute(Constants.CONTACT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, contactForm.getMessage());
                                        break;
                                    case Constants.DELETE:
                                        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deleteContacts(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            contactForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            contactForm.setMessage(false, "Contacts are deleted successfully.");
                                        }
                                        request.getSession(true).setAttribute(Constants.MESSAGE, contactForm.getMessage());
                                        break;
                                    case Constants.RECHARGE:
                                        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        boolean errorFlag = false;
                                        double totalRefillAmount = 0.0;
                                        double[] amountListArray = contactForm.getAmounts();
                                        ArrayList<FlexiDTO> flexiDTOList = new ArrayList<FlexiDTO>();
                                        FlexiDAO flDAO = new FlexiDAO();
                                        FlexiTaskScheduler fscheduler = new FlexiTaskScheduler();
                                        for (int i = 0; i < length; i++) {
                                            ContactDTO cdto = ContactLoader.getInstance().getContactDTOByID(idListArray[i]);
                                            if (cdto != null && cdto.getStatusID() == Constants.USER_STATUS_ACTIVE) {
                                                String prefix = cdto.getContactNo().substring(0, 3);
                                                int operatorTypeID = 0;
                                                for (int j = 1; j < Constants.OPERATOR_TYPE_PREFIX.length; j++) {
                                                    if (prefix.equals(Constants.OPERATOR_TYPE_PREFIX[j])) {
                                                        operatorTypeID = j;
                                                        break;
                                                    }
                                                }
                                                if (operatorTypeID < Constants.GP) {
                                                    errorFlag = true;
                                                    target = "failure";
                                                    contactForm.setMessage(true, "Enter valid phone number for " + cdto.getContactName() + ". No refill request is posted.");
                                                    break;
                                                }
                                                long curTime = System.currentTimeMillis();
                                                long lastRefillTime = flDAO.getLastRefillTime(cdto.getContactNo(), curTime);
                                                if (lastRefillTime > 0) {
                                                    long refillInterval = curTime - lastRefillTime;
                                                    if (refillInterval < (SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000)) {
                                                        errorFlag = true;
                                                        target = "failure";
                                                        int minute = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000) - refillInterval) / 1000) / 60);
                                                        int sec = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000) - refillInterval) / 1000) % 60);
                                                        contactForm.setMessage(true, "You have already sent a request to same number for " + cdto.getContactName() + ".<br>Please wait " + minute + " minutes " + sec + " seconds. No refill request is posted.");
                                                        break;
                                                    }
                                                }
                                                if (cdto.getRefillType() == Constants.REFILL_TYPE_PREPAID) {
                                                    if (amountListArray[i] < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || amountListArray[i] > SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount()) {
                                                        errorFlag = true;
                                                        target = "failure";
                                                        contactForm.setMessage(true, "Enter valid prepaid amount for " + cdto.getContactName() + ". No refill request is posted.");
                                                        break;
                                                    }
                                                } else if (cdto.getRefillType() == Constants.REFILL_TYPE_POSTPAID) {
                                                    if (amountListArray[i] < SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() || amountListArray[i] > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                                                        errorFlag = true;
                                                        target = "failure";
                                                        contactForm.setMessage(true, "Enter valid postpaid amount for " + cdto.getContactName() + ". No refill request is posted.");
                                                        break;
                                                    }
                                                }
                                                FlexiDTO dto = new FlexiDTO();
                                                dto.setFlexiContactID(cdto.getId());
                                                dto.setFlexiGroupID(cdto.getGroupID());
                                                dto.setPhoneNumber(cdto.getContactNo());
                                                dto.setAmount(amountListArray[i]);
                                                dto.setRefillType(cdto.getRefillType());
                                                dto.setOperatorTypeID(operatorTypeID);
                                                totalRefillAmount += dto.getAmount();
                                                flexiDTOList.add(dto);
                                            }
                                        }
                                        if (!errorFlag) {
                                            ClientDTO clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getId());
                                            if ((clDTO.getClientCredit()-clDTO.getClientDeposit()) < totalRefillAmount) {
                                                errorFlag = true;
                                                target = "failure";
                                                contactForm.setMessage(true, "Your Credit Limit is Over. No refill request is posted.");
                                            }
                                            if(!errorFlag)
                                            {
                                            clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getClientParentId());
                                            while (clDTO != null && (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2)) {
                                                if (clDTO.getClientCredit()-clDTO.getClientDeposit() < totalRefillAmount) {
                                                    errorFlag = true;
                                                    target = "failure";
                                                    contactForm.setMessage(true, "Reseller Credit Limit is Over. No refill request is posted.");
                                                    break;
                                                }
                                                clDTO = ClientLoader.getInstance().getClientDTOByID(clDTO.getParentId());
                                            }
                                            }
                                        }
                                        if (!errorFlag) {
                                            for (int i = 0; i < flexiDTOList.size(); i++) {
                                                error = fscheduler.addFlexiRequest(login_dto, flexiDTOList.get(i));
                                                if (error.getErrorType() > 0) {
                                                    errorFlag = true;
                                                    target = "failure";
                                                    contactForm.setMessage(true, error.getErrorMessage());
                                                    break;
                                                }
                                            }
                                        }
                                        if (!errorFlag) {
                                            contactForm.setMessage(false, "All refill requests are posted successfully.");
                                        }
                                        request.setAttribute(Constants.CONTACT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, contactForm.getMessage());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            contactForm.setMessage(true, "No contact is selected.");
                            request.getSession(true).setAttribute(Constants.MESSAGE, contactForm.getMessage());
                        }
                    }
                }

                if (list_all == 0) {
                    if (contactForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.CONTACT_RECORD_PER_PAGE, contactForm.getRecordPerPage());
                    }
                    ContactDTO conatactdto = new ContactDTO();
                    if (contactForm.getContactNamePar() != null && contactForm.getContactNamePar().trim().length() > 0) {
                        conatactdto.searchWithContactName = true;
                        conatactdto.setContactName(contactForm.getContactNamePar().toLowerCase());
                    }
                    if (contactForm.getContactNoPar() != null && contactForm.getContactNoPar().trim().length() > 0) {
                        conatactdto.searchWithContactNo = true;
                        conatactdto.setContactNo(contactForm.getContactNoPar().toLowerCase());
                    }
                    contactForm.setContactList(scheduler.getContactDTOsWithSearchParam(contactForm.getGroupID(), conatactdto));
                } else if (list_all == 2) {
                    if (action == Constants.EDIT) {
                        contactForm.setMessage(false, "Contact is updated successfully.");
                    }
                    if (request.getSession(true).getAttribute(Constants.CONTACT_RECORD_PER_PAGE) != null) {
                        contactForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CONTACT_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, contactForm.getMessage());
                    contactForm.setContactList(scheduler.getContactDTOsSorted(contactForm.getGroupID()));
                } else {
                    if (request.getSession(true).getAttribute(Constants.CONTACT_RECORD_PER_PAGE) != null) {
                        contactForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CONTACT_RECORD_PER_PAGE).toString()));
                    }
                    contactForm.setContactList(scheduler.getContactDTOs(contactForm.getGroupID()));
                }
                contactForm.setSelectedIDs(null);
                if (contactForm.getContactList() != null && contactForm.getContactList().size() > 0 && contactForm.getContactList().size() <= (contactForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
