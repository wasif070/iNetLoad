package com.myapp.struts.contacts;

import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.contactgroup.ContactGroupDTO;
import com.myapp.struts.contactgroup.ContactGroupLoader;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class ContactLoader {

    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, ContactDTO> contactDTOById;
    private HashMap<Long, ContactDTO> allContactDTOById;
    private HashMap<Long, ArrayList<ContactDTO>> contactDTOListByGroupId;
    private ArrayList<ContactDTO> contactDTOList;
    static ContactLoader contactLoader = null;
    static Logger logger = Logger.getLogger(ContactLoader.class.getName());

    public ContactLoader() {
        forceReload();
    }

    public static ContactLoader getInstance() {
        if (contactLoader == null) {
            createContactLoader();
        }
        return contactLoader;
    }

    private synchronized static void createContactLoader() {
        if (contactLoader == null) {
            contactLoader = new ContactLoader();
        }
    }

    private void reload() {
        contactDTOList = new ArrayList<ContactDTO>();
        contactDTOById = new HashMap<Long, ContactDTO>();
        allContactDTOById = new HashMap<Long, ContactDTO>();
        contactDTOListByGroupId = new HashMap<Long, ArrayList<ContactDTO>>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from contact;";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                ContactDTO dto = new ContactDTO();
                dto.setId(rs.getLong("id"));
                dto.setContactName(rs.getString("contact_name"));
                dto.setContactDescription(rs.getString("contact_des"));
                dto.setStatusID(rs.getInt("status_id"));
                dto.setStatusName(Constants.LIVE_STATUS_STRING[rs.getInt("status_id")]);
                dto.setGroupID(rs.getLong("contact_group_id"));
                dto.setIsDeleted(rs.getInt("contact_delete"));
                ContactGroupDTO groupDTO = ContactGroupLoader.getInstance().getContactGroupDTOByID(rs.getLong("contact_group_id"));
                if (groupDTO != null) {
                    dto.setGroupName(groupDTO.getGroupName());
                } else {
                    continue;
                }
                dto.setContactNo(rs.getString("contact_phn_no"));
                dto.setContactNoWithPrefix();
                dto.setRefillType(rs.getInt("contact_type_id"));
                dto.setRefillAmount(rs.getDouble("contact_amount"));
                if (ClientLoader.getInstance().getClientDTOByID(rs.getLong("contact_created_by")) != null) {
                    dto.setContactCreatedBy(rs.getLong("contact_created_by"));
                } else {
                    continue;
                }
                if (!dto.getIsDeleted()) {
                    ArrayList<ContactDTO> dtoList = contactDTOListByGroupId.get(dto.getGroupID());
                    if (dtoList != null) {
                        dtoList.add(dto);
                    } else {
                        dtoList = new ArrayList<ContactDTO>();
                        dtoList.add(dto);
                        contactDTOListByGroupId.put(dto.getGroupID(), dtoList);
                    }
                    contactDTOList.add(dto);
                    contactDTOById.put(dto.getId(), dto);
                }
                allContactDTOById.put(dto.getId(), dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getContactDTOList() {
        checkForReload();
        return contactDTOList;
    }

    public synchronized ContactDTO getContactDTOByID(long id) {
        checkForReload();
        return contactDTOById.get(id);
    }
    

    public synchronized ContactDTO getAllContactDTOByID(long id) {
        checkForReload();
        return allContactDTOById.get(id);
    }    

    public synchronized ArrayList<ContactDTO> getContactDTOListByGroupId(long groupId) {
        checkForReload();
        return contactDTOListByGroupId.get(groupId);
    }
}
