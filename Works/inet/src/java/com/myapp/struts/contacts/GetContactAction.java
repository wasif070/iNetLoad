package com.myapp.struts.contacts;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetContactAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ContactForm formBean = (ContactForm) form;
            if (!login_dto.getIsUser() && login_dto.getClientTypeId()==Constants.CLIENT_TYPE_AGENT) {
                long id = Long.parseLong(request.getParameter("id"));
                ContactDTO dto = new ContactDTO();
                ContactTaskScheduler scheduler = new ContactTaskScheduler();
                dto = scheduler.getContactDTO(id);

                if (dto != null) {
                    formBean.setId(dto.getId());
                    formBean.setContactName(dto.getContactName());
                    formBean.setContactNo(dto.getContactNo());
                    formBean.setGroupID(dto.getGroupID());
                    formBean.setRefillAmount(dto.getRefillAmount());
                    formBean.setRefillType(dto.getRefillType());
                    formBean.setContactDescription(dto.getContactDescription());
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
