package com.myapp.struts.contacts;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class ContactTaskScheduler {

    public ContactTaskScheduler() {
    }

    public MyAppError addContact(ContactDTO cdto) {
        ContactDAO contactDAO = new ContactDAO();
        return contactDAO.addContact(cdto);
    }

    public MyAppError editContact(ContactDTO cdto) {
        ContactDAO contactDAO = new ContactDAO();
        return contactDAO.editContact(cdto);
    }
    
    public MyAppError deleteContacts(String list) {
        ContactDAO contactDAO = new ContactDAO();
        return contactDAO.deleteContacts(list);
    }
    
    public MyAppError activateContacts(String list) {
        ContactDAO contactDAO = new ContactDAO();
        return contactDAO.activateContacts(list);
    }    

    public MyAppError blockContacts(String list) {
        ContactDAO contactDAO = new ContactDAO();
        return contactDAO.blockContacts(list);
    }

    public ArrayList<ContactDTO> getContactDTOsWithSearchParam(long groupID, ContactDTO gwdto) {
        ContactDAO contactDAO = new ContactDAO();
        ArrayList<ContactDTO> list = ContactLoader.getInstance().getContactDTOListByGroupId(groupID);
        if (list != null) {
            return contactDAO.getContactDTOsWithSearchParam((ArrayList<ContactDTO>) list.clone(), gwdto);
        }
        return null;
    }

    public ArrayList<ContactDTO> getContactDTOsSorted(long groupID) {
        ContactDAO contactDAO = new ContactDAO();
        ArrayList<ContactDTO> list = ContactLoader.getInstance().getContactDTOListByGroupId(groupID);
        if (list != null) {
            return contactDAO.getContactDTOsSorted((ArrayList<ContactDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<ContactDTO> getContactDTOs(long groupID) {
        return ContactLoader.getInstance().getContactDTOListByGroupId(groupID);
    }

    public ContactDTO getContactDTO(long id) {
        return ContactLoader.getInstance().getContactDTOByID(id);
    }
    
    public ArrayList getSummeryRefill(LoginDTO login_dto, ContactDTO cdto) {
        ContactDAO contactDAO = new ContactDAO();
        ArrayList data = null;
        if(cdto.getReportType() == 2)
        {
            data = contactDAO.getRefillSummery(login_dto, cdto);
        }    
        else if (cdto.getReportType() == 1) {
            data = contactDAO.getRefillSummeryByContact(login_dto, cdto);
        } else {
            data = contactDAO.getRefillSummeryByGroup(login_dto, cdto);
        }
        return data;
    }    
}
