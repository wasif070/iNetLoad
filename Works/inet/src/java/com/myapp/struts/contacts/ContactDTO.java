package com.myapp.struts.contacts;

public class ContactDTO {
    public boolean searchWithContactName = false;
    public boolean searchWithContactNo = false;
    public boolean searchWithGroupName = false;
    public boolean isDeleted = false;
    private int refillType;   
    private int doValidation;
    private int pageNo;
    private int recordPerPage;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;  
    private int reportType;
    private int statusID;
    private long id;
    private long groupID;
    private long contactCreatedBy;
    private long reportDate;
    private double refillAmount;
    private double totalRefillAmount;
    private String statusName;
    private String contactDescription;
    private String contactName;
    private String contactNamePar;
    private String contactNo;
    private String contactNoPar;
    private String groupName;
    private String groupNamePar;
    private String contactNoWithPrefix;

    public ContactDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }    
    
    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }    
    
    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    } 
    
    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        if(isDeleted==1)
        {
           this.isDeleted = true;
        }
        else
        {
            this.isDeleted = false;
        }
    }    

    public double getRefillAmount() {
        return refillAmount;
    }

    public void setRefillAmount(double refillAmount) {
        this.refillAmount = refillAmount;
    }
    
    public double getTotalRefillAmount() {
        return totalRefillAmount;
    }

    public void setTotalRefillAmount(double totalRefillAmount) {
        this.totalRefillAmount = totalRefillAmount;
    }    

    public String getContactDescription() {
        return contactDescription;
    }

    public void setContactDescription(String contactDescription) {
        this.contactDescription = contactDescription;
    }
    
    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }    

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
    
    public String getContactNoWithPrefix() {
        return contactNoWithPrefix;
    }

    public void setContactNoWithPrefix() {
        this.contactNoWithPrefix = "88"+contactNo;
    }    
    
    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }  
    
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }  
    
    public String getGroupNamePar() {
        return groupNamePar;
    }

    public void setGroupNamePar(String groupNamePar) {
        this.groupNamePar = groupNamePar;
    }    
    
    public String getContactNamePar() {
        return contactNamePar;
    }

    public void setContactNamePar(String contactNamePar) {
        this.contactNamePar = contactNamePar;
    }   
    
    public String getContactNoPar() {
        return contactNoPar;
    }

    public void setContactNoPar(String contactNoPar) {
        this.contactNoPar = contactNoPar;
    }       

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidation() {
        return doValidation;
    }

    public void setDoValidation(int doValidation) {
        this.doValidation = doValidation;
    }

    public int getRefillType() {
        return refillType;
    }

    public void setRefillType(int refillType) {
        this.refillType = refillType;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    } 
    
    public long getReportDate() {
        return reportDate;
    }

    public void setReportDate(long reportDate) {
        this.reportDate = reportDate;
    }    
    
    public long getContactCreatedBy() {
        return contactCreatedBy;
    }

    public void setContactCreatedBy(long contactCreatedBy) {
        this.contactCreatedBy = contactCreatedBy;
    }     
}
