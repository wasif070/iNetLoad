package com.myapp.struts.util;

import com.myapp.struts.system.SettingsLoader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class Utils {

    static Random randomGenerator = new Random();

    public void Utils() {
    }

    public static boolean checkEmailId(String address) {

        if (address == null) {
            return false;
        }
        if (address.length() == 0) {
            return false;
        }
        Pattern p = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
        Matcher m = p.matcher(address);
        boolean b = m.matches();
        if (b == false) {
            return false;
        }
        int pos = address.indexOf('@');
        if (pos == -1) {
            return false;
        }
        String domain = address.substring(++pos);
        if (doLookup(domain) == 0) {
            return false;
        }
        return true;
    }

    public static int doLookup(String hostName) {
        try {
            Hashtable env = new Hashtable();
            env.put("java.naming.factory.initial",
                    "com.sun.jndi.dns.DnsContextFactory");
            DirContext ictx = new InitialDirContext(env);
            Attributes attrs =
                    ictx.getAttributes(hostName, new String[]{"MX"});
            Attribute attr = attrs.get("MX");
            if (attr == null) {
                return 0;
            }
            return (attr.size());
        } catch (Exception e) {
        }
        return 0;
    }

    public static long DateToLong(String date) {
        String[] values = date.split("/");
        int day = Integer.parseInt(values[0]);
        int month = Integer.parseInt(values[1]);
        int year = Integer.parseInt(values[2]);
        long dat = new GregorianCalendar(year, month - 1, day, 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000);
        return dat;
    }

    public static String LongToDate(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String curDate = formatter.format(new java.util.Date(date + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000)));
        return curDate;
    }

    public static String ToDate(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String curDate = formatter.format(new java.util.Date(date + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000)));
        return curDate;
    }

    public static String ToDateDDMMYYhhmmss(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        String curDate = formatter.format(new java.util.Date(date + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000)));
        return curDate;
    }
    
    public static String ToDateDDMMYYYYhhmmss(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String curDate = formatter.format(new java.util.Date(date + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000)));
        return curDate;
    }    

    public static String ToDateDDMMYYhhmm(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static long ToLong(String date) {
        String[] values = date.split("/");
        int day = Integer.parseInt(values[0]);
        int month = Integer.parseInt(values[1]);
        int year = Integer.parseInt(values[2]);
        long dat = new GregorianCalendar(year, month - 1, day, 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000);;
        return dat;
    }

    public static String[] getDateParts(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String curDate = formatter.format(new java.util.Date(date));
        String date_parts[] = curDate.split("/");
        return date_parts;
    }

    public static int getCurrentMonth(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM");
        String curDate = formatter.format(new java.util.Date(date));
        return Integer.parseInt(curDate);
    }

    public static int getCurrentYear(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        String curDate = formatter.format(new java.util.Date(date));
        return Integer.parseInt(curDate);
    }

    public static ArrayList<Integer> getDay() {
        ArrayList<Integer> options = new ArrayList<Integer>();
        for (int i = 1; i <= 31; i++) {
            options.add(i);
        }
        return options;
    }

    public static ArrayList<String> getMonth() {
        ArrayList<String> options = new ArrayList<String>();
        options.add("Jan");
        options.add("Feb");
        options.add("Mar");
        options.add("Apr");
        options.add("May");
        options.add("Jun");
        options.add("Jul");
        options.add("Aug");
        options.add("Sep");
        options.add("Oct");
        options.add("Nov");
        options.add("Dec");
        return options;
    }

    public static ArrayList<Integer> getYear() {
        ArrayList<Integer> options = new ArrayList<Integer>();
        int currentYear = 2010;
        int i = currentYear;
        while (i - currentYear < 20) {
            options.add(i);
            i++;
        }
        return options;
    }

    public static boolean checkValidUserId(String userId) {

        if (userId == null) {
            return false;
        }
        if (userId.length() == 0) {
            return false;
        }

        Pattern p = Pattern.compile("^[a-zA-Z0-9_.]+$");
        Matcher m = p.matcher(userId);
        boolean b = m.matches();
        if (b == false) {
            return false;
        }
        return true;
    }

    public static boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isLong(String number) {
        try {
            Long.parseLong(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isFloat(String number) {
        try {
            Float.parseFloat(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String number) {
        try {
            Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isFloatNumber(String number) {
        boolean isValid = false;

        /*Number: A numeric value will have following format:
        ^[-+]?: Starts with an optional "+" or "-" sign.
        [0-9]*: May have one or more digits.
        \\.? : May contain an optional "." (decimal point) character.
        [0-9]+$ : ends with numeric digit.
         */

        //Initialize reg ex for numeric data.
        String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static String implodeArray(long[] inputArray, String glueString) {

        /** Output variable */
        String output = "";

        if (inputArray.length > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(inputArray[0]);

            for (int i = 1; i < inputArray.length; i++) {
                sb.append(glueString);
                sb.append(inputArray[i]);
            }

            output = sb.toString();
        }

        return output;
    }

    public static long getRandomNumber() {
        return randomGenerator.nextLong();
    }
}
