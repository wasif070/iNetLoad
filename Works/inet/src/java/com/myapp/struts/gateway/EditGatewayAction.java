package com.myapp.struts.gateway;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

public class EditGatewayAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        GatewayForm formBean = (GatewayForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FLEXI_SIM_SETTINGS_EDIT)) {
                request.setAttribute(mapping.getAttribute(), formBean);

                GatewayDTO dto = new GatewayDTO();
                GatewayTaskScheduler scheduler = new GatewayTaskScheduler();
                dto.setTypeID(formBean.getTypeID());
                dto.setOperatorID(formBean.getOperatorID());
                dto.setSimID(formBean.getSimID());
                dto.setPinNumber(formBean.getPinNumber());
                dto.setBalance(formBean.getBalance());
                dto.setGatewayStatus(formBean.getGatewayStatus());
                dto.setPhoneNo(formBean.getPhoneNo());
                dto.setDealerID(formBean.getDealerID());
                dto.setSimVersion(formBean.getSimVersion());

                MyAppError error = new MyAppError();
                long id = Long.parseLong(request.getParameter("id"));
                dto.setId(id);
                error = scheduler.editGateway(dto);

                if (error.getErrorType() == MyAppError.NoError) {
                    formBean.setMessage(false, "Flexi SIM is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());                    
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return mapping.findForward(target);
    }
}
