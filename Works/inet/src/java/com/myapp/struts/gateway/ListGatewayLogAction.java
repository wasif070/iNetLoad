package com.myapp.struts.gateway;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListGatewayLogAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(ListGatewayAction.class.getName());
    static SimpleDateFormat bangDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            GatewayForm gatewayForm = (GatewayForm) form;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FLEXI_MESSAGE_LOG) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }

                GatewayTaskScheduler scheduler = new GatewayTaskScheduler();

                int action = 0;
                if (request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action == Constants.DELETE) {
                        if (gatewayForm.getSelectedIDs() != null) {
                            int length = gatewayForm.getSelectedIDs().length;
                            long[] idListArray = gatewayForm.getSelectedIDs();
                            if (length > 0) {
                                idList += idListArray[0];
                                for (int i = 1; i < length; i++) {
                                    idList += "," + idListArray[i];
                                }
                                MyAppError error = new MyAppError();
                                if (userPerDTO != null && !userPerDTO.FLEXI_MESSAGE_LOG_DELETE) {
                                    request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                    request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                    return (mapping.findForward("index"));
                                }
                               error = scheduler.deleteGatewayMessages(idList);
                                if (error.getErrorType() > 0) {
                                    target = "failure";
                                    gatewayForm.setMessage(true, error.getErrorMessage());
                                } else {
                                    gatewayForm.setMessage(false, "Flexi Messages are deleted successfully.");
                                }
                                request.getSession(true).setAttribute(Constants.MESSAGE, gatewayForm.getMessage());

                            }
                        } else {
                            gatewayForm.setMessage(true, "No Flexi Message is selected.");
                            request.getSession(true).setAttribute(Constants.MESSAGE, gatewayForm.getMessage());
                        }
                    }
                }
                GatewayDTO gwdto = new GatewayDTO();
                if (list_all == 0) {
                    if (gatewayForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.FLEXI_MESSAGE_LOG_RECORD_PER_PAGE, gatewayForm.getRecordPerPage());
                    }
                    
                    if (gatewayForm.getPhoneNoPar() != null && gatewayForm.getPhoneNoPar().trim().length() > 0) {
                        gwdto.searchWithPhoneNo = true;
                        gwdto.setPhoneNoPar(gatewayForm.getPhoneNoPar().toLowerCase());
                    }
                    if (gatewayForm.getOperatorIDPar() >= 0) {
                        gwdto.searchWithOperatorID = true;
                        gwdto.setOperatorIDPar(gatewayForm.getOperatorIDPar());
                    }
                    if (gatewayForm.getSimIDPar() != null && gatewayForm.getSimIDPar().trim().length() > 0) {
                        gwdto.searchWithSIMID = true;
                        gwdto.setSimIDPar(gatewayForm.getSimIDPar().toLowerCase());
                    }     
                    if (gatewayForm.getTextPar() != null && gatewayForm.getTextPar().trim().length() > 0) {
                        gwdto.searchWithText = true;
                        gwdto.setTextPar(gatewayForm.getTextPar().toLowerCase());
                    }                     
                    gwdto.setStartDay(gatewayForm.getStartDay());
                    gwdto.setStartMonth(gatewayForm.getStartMonth());
                    gwdto.setStartYear(gatewayForm.getStartYear());
                    gwdto.setEndDay(gatewayForm.getEndDay());
                    gwdto.setEndMonth(gatewayForm.getEndMonth());
                    gwdto.setEndYear(gatewayForm.getEndYear());
                    gatewayForm.setGatewayMessageList(scheduler.getGatewayMessageDTOsWithSearchParam(gwdto));
                }
                else {
                    String dateParts[] = Utils.getDateParts(System.currentTimeMillis()  + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    gatewayForm.setStartDay(Integer.parseInt(dateParts[0]));
                    gatewayForm.setStartMonth(Integer.parseInt(dateParts[1]));
                    gatewayForm.setStartYear(Integer.parseInt(dateParts[2]));
                    gatewayForm.setEndDay(gatewayForm.getStartDay());
                    gatewayForm.setEndMonth(gatewayForm.getStartMonth());
                    gatewayForm.setEndYear(gatewayForm.getStartYear());
                    gwdto.setStartDay(gatewayForm.getStartDay());
                    gwdto.setStartMonth(gatewayForm.getStartMonth());
                    gwdto.setStartYear(gatewayForm.getStartYear());
                    gwdto.setEndDay(gatewayForm.getEndDay());
                    gwdto.setEndMonth(gatewayForm.getEndMonth());
                    gwdto.setEndYear(gatewayForm.getEndYear());
                    if (request.getSession(true).getAttribute(Constants.FLEXI_MESSAGE_LOG_RECORD_PER_PAGE) != null) {
                        gatewayForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_MESSAGE_LOG_RECORD_PER_PAGE).toString()));
                    }
                    gatewayForm.setGatewayMessageList(scheduler.getGatewayMessageDTOs(gwdto));
                }
                gatewayForm.setSelectedIDs(null);
                if (gatewayForm.getGatewayList() != null && gatewayForm.getGatewayList().size() > 0 && gatewayForm.getGatewayList().size() <= (gatewayForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
