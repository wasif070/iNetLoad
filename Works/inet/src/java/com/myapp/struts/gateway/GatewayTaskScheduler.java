package com.myapp.struts.gateway;

import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class GatewayTaskScheduler {

    public GatewayTaskScheduler() {
    }

    public MyAppError addGateway(GatewayDTO gwdto) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.addGateway(gwdto);
    }

    public MyAppError editGateway(GatewayDTO gwdto) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.editGateway(gwdto);
    }

    public MyAppError deactivateGateways(String list) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.deactivateGateways(list);
    }

    public MyAppError activateGateways(String list) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.activateGateways(list);
    }

    public MyAppError deleteGateways(String list) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.deleteGateways(list);
    }

    public MyAppError deleteGatewayMessages(String list) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.deleteGatewayMessages(list);
    }

    public MyAppError updateGatewayRecharges(long[] idListArray,String[] desListArray)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.updateGatewayRecharges(idListArray, desListArray);
    }

    public MyAppError rechargeGateways(long[] idListArray, double[] amountListArray, String[] descriptionListArray, long[] dateListArray)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.rechargeGateways(idListArray, amountListArray, descriptionListArray, dateListArray);
    }

    public MyAppError deleteGatewayRecharges(String list) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.deleteGatewayRecharges(list);
    }

    public ArrayList<GatewayDTO> getGatewayDTOsWithSearchParam(GatewayDTO gwdto) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        ArrayList<GatewayDTO> list = GatewayLoader.getInstance().getGatewayDTOList();
        if(list!=null)
        {
          return gatewayDAO.getGatewayDTOsWithSearchParam((ArrayList<GatewayDTO>) list.clone(),gwdto);
        }
        return null;
    }

    public ArrayList<GatewayDTO> getGatewayMessageDTOsWithSearchParam(GatewayDTO gwdto) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.getGatewayMessageDTOsWithSearchParam(gwdto);
    }

    public ArrayList<GatewayDTO> getGatewayMessageDTOs(GatewayDTO gwdto) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.getGatewayMessageDTOs(gwdto);
    }

    public ArrayList<GatewayDTO> getGatewayDTOsSorted() {
        GatewayDAO gatewayDAO = new GatewayDAO();
        ArrayList<GatewayDTO> list = GatewayLoader.getInstance().getGatewayDTOList();
        if(list!=null)
        {
          return gatewayDAO.getGatewayDTOsSorted((ArrayList<GatewayDTO>) list.clone());
        }
        return null;         
    }

    public ArrayList<GatewayDTO> getGatewayDTOs() {
        return GatewayLoader.getInstance().getGatewayDTOList();
    }

    public GatewayDTO getGatewayDTO(long id) {
        return GatewayLoader.getInstance().getGatewayDTOByID(id);
    }

    public ArrayList<GatewayDTO> getGatewayRechargeDTOs(GatewayDTO gwdto)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.getGatewayRechargeDTOs(gwdto);
    }

    public ArrayList<GatewayDTO> getGatewayRechargeDTOsWithSearchParam(GatewayDTO gwdto)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.getGatewayRechargeDTOsWithSearchParam(gwdto);
    }
}
