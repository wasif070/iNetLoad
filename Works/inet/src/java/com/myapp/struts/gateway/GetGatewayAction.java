package com.myapp.struts.gateway;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetGatewayAction
        extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            GatewayForm formBean = (GatewayForm) form;
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FLEXI_SIM_SETTINGS_EDIT)) {
                long id = Long.parseLong(request.getParameter("id"));
                GatewayDTO dto = new GatewayDTO();
                GatewayTaskScheduler scheduler = new GatewayTaskScheduler();
                dto = scheduler.getGatewayDTO(id);

                if (dto != null) {
                    formBean.setId(dto.getId());
                    formBean.setTypeID(dto.getTypeID());
                    formBean.setOperatorID(dto.getOperatorID());
                    formBean.setOperatorName(Constants.OPERATOR_TYPE_NAME[dto.getOperatorID()]);
                    formBean.setSimID(dto.getSimID());
                    formBean.setPinNumber(dto.getPinNumber().trim());
                    formBean.setBalance(dto.getBalance());
                    formBean.setGatewayStatus(dto.getGatewayStatus());
                    formBean.setPhoneNo(dto.getPhoneNo());
                    formBean.setDealerID(dto.getDealerID());
                    formBean.setSimVersion(dto.getSimVersion());
                    if(dto.getSimVersion() == -1)
                    {
                        request.getSession(true).setAttribute("addedByUser", 1);
                    }
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
