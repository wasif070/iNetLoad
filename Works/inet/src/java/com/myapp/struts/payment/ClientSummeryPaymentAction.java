package com.myapp.struts.payment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;

public class ClientSummeryPaymentAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.REPORT_PAYMENT_SUMMERY && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int pageNo = 1;
                int list_all = 0;
                long id = -1;
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                PaymentDTO pdto = new PaymentDTO();
                PaymentForm paymentForm = (PaymentForm) form;
                PaymentTaskScheduler scheduler = new PaymentTaskScheduler();

                if (list_all == 0) {
                    if (paymentForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.PAYMENT_CLIENT_SUMMERY_RECORD_PER_PAGE, paymentForm.getRecordPerPage());
                    }
                    if (paymentForm.getPaymentTypeId() >= 0) {
                        pdto.searchWithPaymentType = true;
                        pdto.setPaymentTypeId(paymentForm.getPaymentTypeId());
                    }

                    pdto.setEndDay(paymentForm.getEndDay());
                    pdto.setEndMonth(paymentForm.getEndMonth());
                    pdto.setEndYear(paymentForm.getEndYear());
                    pdto.setStartDay(paymentForm.getStartDay());
                    pdto.setStartMonth(paymentForm.getStartMonth());
                    pdto.setStartYear(paymentForm.getStartYear());
                    paymentForm.setClientPaymentSummery(scheduler.getClientPaymentSummeryDTOsWithSearchParam(id, pdto));
                } else {
                    String dateParts[] = Utils.getDateParts(System.currentTimeMillis()  + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    paymentForm.setStartDay(Integer.parseInt(dateParts[0]));
                    paymentForm.setStartMonth(Integer.parseInt(dateParts[1]));
                    paymentForm.setStartYear(Integer.parseInt(dateParts[2]));
                    paymentForm.setEndDay(paymentForm.getStartDay());
                    paymentForm.setEndMonth(paymentForm.getStartMonth());
                    paymentForm.setEndYear(paymentForm.getStartYear());
                    pdto.setStartDay(paymentForm.getStartDay());
                    pdto.setStartMonth(paymentForm.getStartMonth());
                    pdto.setStartYear(paymentForm.getStartYear());
                    pdto.setEndDay(paymentForm.getEndDay());
                    pdto.setEndMonth(paymentForm.getEndMonth());
                    pdto.setEndYear(paymentForm.getEndYear());
                    if (request.getSession(true).getAttribute(Constants.PAYMENT_CLIENT_SUMMERY_RECORD_PER_PAGE) != null) {
                        paymentForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.PAYMENT_CLIENT_SUMMERY_RECORD_PER_PAGE).toString()));
                    }
                    paymentForm.setClientPaymentSummery(scheduler.getClientPaymentSummeryDTOs(id, pdto));
                }
                if (paymentForm.getClientPaymentSummery() != null && paymentForm.getClientPaymentSummery().size() > 0 && paymentForm.getClientPaymentSummery().size() <= (paymentForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
