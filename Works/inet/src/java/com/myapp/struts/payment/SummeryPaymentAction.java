package com.myapp.struts.payment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

public class SummeryPaymentAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.REPORT_PAYMENT_SUMMERY && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int pageNo = 1;
                int list_all = 0;
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                PaymentDTO pdto = new PaymentDTO();
                PaymentForm paymentForm = (PaymentForm) form;
                PaymentTaskScheduler scheduler = new PaymentTaskScheduler();

                if (list_all == 0) {
                    if (paymentForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.PAYMENT_SUMMERY_RECORD_PER_PAGE, paymentForm.getRecordPerPage());
                    }
                    if (paymentForm.getPaymentToClient() != null && paymentForm.getPaymentToClient().trim().length() > 0) {
                        pdto.searchWithClient = true;
                        pdto.setPaymentToClient(paymentForm.getPaymentToClient().toLowerCase());
                    }
                    paymentForm.setPaymentSummery(scheduler.getPaymentSummeryDTOsWithSearchParam(login_dto, pdto));
                } else {
                    if (request.getSession(true).getAttribute(Constants.PAYMENT_SUMMERY_RECORD_PER_PAGE) != null) {
                        paymentForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.PAYMENT_SUMMERY_RECORD_PER_PAGE).toString()));
                    }
                    paymentForm.setPaymentSummery(scheduler.getPaymentSummeryDTOs(login_dto));
                }
                if (paymentForm.getPaymentSummery() != null && paymentForm.getPaymentSummery().size() > 0 && paymentForm.getPaymentSummery().size() <= (paymentForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
