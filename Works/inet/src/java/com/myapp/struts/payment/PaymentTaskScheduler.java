package com.myapp.struts.payment;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import java.util.ArrayList;

public class PaymentTaskScheduler {

    public PaymentTaskScheduler() {
    }

    public ArrayList<PaymentDTO> getPaymentMadeDTOs(LoginDTO login_dto, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentMadeDTOs(login_dto, dto);
    }

    public ArrayList<PaymentDTO> getPaymentMadeDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentMadeDTOsWithSearchParam(login_dto, dto);
    }

    public ArrayList<PaymentDTO> getPaymentReceivedDTOs(LoginDTO login_dto, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentReceivedDTOs(login_dto, dto);
    }   

    public ArrayList<PaymentDTO> getPaymentReceivedDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentReceivedDTOsWithSearchParam(login_dto, dto);
    }
    
    public ArrayList<PaymentDTO> getPaymentPaidDTOs(LoginDTO login_dto, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentPaidDTOs(login_dto, dto);
    } 
    
    public ArrayList<PaymentDTO> getPaymentPaidDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentPaidDTOsWithSearchParam(login_dto, dto);
    }    

    public ArrayList<PaymentDTO> getPaymentSummeryDTOs(LoginDTO login_dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentSummeryDTOs(login_dto);
    }
    
    public ArrayList<PaymentDTO> getAccountSummeryDTOs(LoginDTO login_dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getAccountSummeryDTOs(login_dto);
    }    

    public ArrayList<PaymentDTO> getPaymentSummeryDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getPaymentSummeryDTOsWithSearchParam(login_dto, dto);
    }

    public ArrayList<PaymentDTO> getClientPaymentSummeryDTOs(long id, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getClientPaymentSummeryDTOs(id, dto);
    }

    public ArrayList<PaymentDTO> getClientPaymentSummeryDTOsWithSearchParam(long id, PaymentDTO dto) {
        PaymentDAO dao = new PaymentDAO();
        return dao.getClientPaymentSummeryDTOsWithSearchParam(id, dto);
    }
}
