package com.myapp.struts.payment;

import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class PaymentForm extends org.apache.struts.action.ActionForm {
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int paymentTypeId;
    private int paymentFromClientTypeId;
    private int paymentToClientTypeId;
    private int recordPerPage;
    private long paymentId;
    private long paymentFromClientId;
    private long paymentToClientId;
    private long paymentTime;
    private double receivedAmount;
    private double paymentAmount;
    private double paymentDiscount;
    private String paymentTypeName;
    private String paymentFromClient;
    private String paymentToClient;
    private String paymentDescription;
    private ArrayList paymentMadeList;
    private ArrayList paymentReceivedList;
    private ArrayList paymentPaidList;
    private ArrayList paymentSummery;
    private ArrayList clientPaymentSummery;
    private ArrayList accountSummery;

    public PaymentForm() {
        super();
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis());
        this.startDay = Integer.parseInt(dateParts[0]);
        this.startMonth = Integer.parseInt(dateParts[1]);
        this.startYear = Integer.parseInt(dateParts[2]);
        this.endDay = this.startDay;
        this.endMonth = this.startMonth;
        this.endYear = this.startYear;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public String getPaymentFromClient() {
        return paymentFromClient;
    }

    public void setPaymentFromUserId(String paymentFromClient) {
        this.paymentFromClient = paymentFromClient;
    }

    public String getPaymentToClient() {
        return paymentToClient;
    }

    public void setPaymentToClient(String paymentToClient) {
        this.paymentToClient = paymentToClient;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(long paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
    
    public double getPaymentDiscount() {
        return paymentDiscount;
    }

    public void setPaymentDiscount(double paymentDiscount) {
        this.paymentDiscount = paymentDiscount;
    }     

    public double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(long receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public long getPaymentFromClientId() {
        return paymentFromClientId;
    }

    public void setPaymentFromClientId(long paymentFromClientId) {
        this.paymentFromClientId = paymentFromClientId;
    }

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public long getPaymentToClientId() {
        return paymentToClientId;
    }

    public void setPaymentToClientId(long paymentToClientId) {
        this.paymentToClientId = paymentToClientId;
    }

    public int getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(int paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public int getPaymentFromClientTypeId() {
        return paymentFromClientTypeId;
    }

    public void setPaymentFromClientTypeId(int paymentFromClientTypeId) {
        this.paymentFromClientTypeId = paymentFromClientTypeId;
    }

    public int getPaymentToClientTypeId() {
        return paymentToClientTypeId;
    }

    public void setPaymentToClientTypeId(int paymentToClientTypeId) {
        this.paymentToClientTypeId = paymentToClientTypeId;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public ArrayList getPaymentMadeList() {
        return paymentMadeList;
    }

    public void setPaymentMadeList(ArrayList paymentMadeList) {
        this.paymentMadeList = paymentMadeList;
    }

    public ArrayList getPaymentReceivedList() {
        return paymentReceivedList;
    }

    public void setPaymentReceivedList(ArrayList paymentReceivedList) {
        this.paymentReceivedList = paymentReceivedList;
    }
    
    public ArrayList getPaymentPaidList() {
        return paymentPaidList;
    }

    public void setPaymentPaidList(ArrayList paymentPaidList) {
        this.paymentPaidList = paymentPaidList;
    }    

    public ArrayList getPaymentSummery() {
        return paymentSummery;
    }

    public void setPaymentSummery(ArrayList paymentSummery) {
        this.paymentSummery = paymentSummery;
    }
    
    public ArrayList getAccountSummery() {
        return accountSummery;
    }

    public void setAccountSummery(ArrayList accountSummery) {
        this.accountSummery = accountSummery;
    }    

    public ArrayList getClientPaymentSummery() {
        return clientPaymentSummery;
    }

    public void setClientPaymentSummery(ArrayList clientPaymentSummery) {
        this.clientPaymentSummery = clientPaymentSummery;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
