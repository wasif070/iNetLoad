package com.myapp.struts.payment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

public class MyAccountSummeryAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.REPORT_PAYMENT_SUMMERY && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int pageNo = 1;
                int list_all = 0;
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                PaymentForm paymentForm = (PaymentForm) form;
                PaymentTaskScheduler scheduler = new PaymentTaskScheduler();

                if (request.getSession(true).getAttribute(Constants.MY_ACCOUNT_SUMMERY_RECORD_PER_PAGE) != null) {
                    paymentForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.MY_ACCOUNT_SUMMERY_RECORD_PER_PAGE).toString()));
                }
                paymentForm.setAccountSummery(scheduler.getAccountSummeryDTOs(login_dto));
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
