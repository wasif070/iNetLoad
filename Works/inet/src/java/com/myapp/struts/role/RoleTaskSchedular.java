package com.myapp.struts.role;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class RoleTaskSchedular {

    public RoleTaskSchedular() {
    }

    public MyAppError addRoleInformation(RoleDTO p_dto,LoginDTO ldto,long[] perIDs) {
        RoleDAO roleDAO = new RoleDAO();
        return roleDAO.addRoleInformation(p_dto,ldto,perIDs);
    }

    public MyAppError editRoleInformation(RoleDTO p_dto,long[] perIDs) {
        RoleDAO roleDAO = new RoleDAO();
        return roleDAO.editRoleInformation(p_dto,perIDs);
    }

    public RoleDTO getRoleDTO(long id) {
        return UserLoader.getInstance().getRoleDTOByID(id);
    }

    public ArrayList<RoleDTO> getRoleDTOs() {
        return (ArrayList<RoleDTO>)UserLoader.getInstance().getRoleDTOList().clone();
    }

    public ArrayList<RoleDTO> getRoleDTOsSorted() {
        RoleDAO roleDAO = new RoleDAO();
        ArrayList<RoleDTO> list = UserLoader.getInstance().getRoleDTOList();
        if(list!=null)
        {
           return roleDAO.getRoleDTOsSorted((ArrayList<RoleDTO>)list.clone());
        }
        return null;     
    }

    public ArrayList<RoleDTO> getRoleDTOsWithSearchParam(RoleDTO rdto) {
        RoleDAO roleDAO = new RoleDAO();
        ArrayList<RoleDTO> list = UserLoader.getInstance().getRoleDTOList();
        if(list!=null)
        {
           return roleDAO.getRoleDTOsWithSearchParam((ArrayList<RoleDTO>)list.clone(),rdto);
        }
        return null;
    }

    public MyAppError deleteRoles(String list) {
        RoleDAO roleDAO = new RoleDAO();
        return roleDAO.deleteRoles(list);
    }

}
