package com.myapp.struts.role;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetRoleAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);

        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
            RoleForm formBean = (RoleForm) form;
            RoleDTO dto = new RoleDTO();
            RoleTaskSchedular scheduler = new RoleTaskSchedular();
            dto = scheduler.getRoleDTO(Long.parseLong(request.getParameter("id")));
            if (dto != null) {
                formBean.setId(dto.getId());
                formBean.setRoleName(dto.getRoleName());
                formBean.setRoleDescription(dto.getRoleDescription());
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
