package com.myapp.struts.role;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class RoleDAO {

    static Logger logger = Logger.getLogger(RoleDAO.class.getName());

    public RoleDAO() {
    }

    public ArrayList<RoleDTO> getRoleDTOsWithSearchParam(ArrayList<RoleDTO> list, RoleDTO rdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RoleDTO dto = (RoleDTO) i.next();
                if ((rdto.searchWithRoleName && !dto.getRoleName().toLowerCase().startsWith(rdto.getRoleName()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<RoleDTO> getRoleDTOsSorted(ArrayList<RoleDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    RoleDTO dto1 = (RoleDTO) o1;
                    RoleDTO dto2 = (RoleDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addRoleInformation(RoleDTO p_dto, LoginDTO ldto, long[] perIDs) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select role_id from role where role_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getRoleName());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Role Name is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            String role_permission_id = p_dto.getRoleName() + ":" + System.currentTimeMillis();
            error = addRolePermissions(role_permission_id, perIDs);
            if (error.getErrorType() == 0) {
                sql = "insert into role(role_id,role_description,role_created_by,role_permission_id) values(?,?,?,?);";
                ps2 = dbConnection.connection.prepareStatement(sql);

                ps2.setString(1, p_dto.getRoleName());
                ps2.setString(2, p_dto.getRoleDescription());
                ps2.setLong(3, ldto.getId());
                ps2.setString(4, role_permission_id);
                ps2.executeUpdate();
                UserLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding role: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }            
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editRoleInformation(RoleDTO p_dto,long[] perIDs) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select role_id from role where role_id = ? and id!=" + p_dto.getId();
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getRoleName());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Role Name is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            error = deleteRolePermissions(p_dto.getId() + "");
            if(error.getErrorType()>0)
            {
                return error;
            }
            String role_permission_id = p_dto.getRoleName() + ":" + System.currentTimeMillis();
            error = addRolePermissions(role_permission_id,perIDs);
            if(error.getErrorType()>0)
            {
                return error;
            }
            sql = "update role set role_id=?,role_description=?,role_permission_id=? where id=" + p_dto.getId();
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, p_dto.getRoleName());
            ps2.setString(2, p_dto.getRoleDescription());
            ps2.setString(3, role_permission_id);
            ps2.executeUpdate();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing role: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }            
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRoles(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            error = deleteRolePermissions(list);
            if (error.getErrorType() == 0) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                String sql = "delete from role where id in(" + list + ");";
                stmt = dbConnection.connection.createStatement();
                stmt.execute(sql);
                UserLoader.getInstance().forceReload();
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleteing role: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addRolePermissions(String role_permission_id, long[] perIDs) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "insert into role_permission(role_permission_id,role_permission_item_id) values";
            if (perIDs != null && perIDs.length > 0) {
                sql += "('" + role_permission_id + "'," + perIDs[0] + ")";
                for (int i = 1; i < perIDs.length; i++) {
                    sql += ",('" + role_permission_id + "'," + perIDs[i] + ")";
                }
                stmt.execute(sql);
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding role permission: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRolePermissions(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select role_permission.id as pid from role,role_permission where role.id in(" + list + ") and role.role_permission_id=role_permission.role_permission_id;";
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            String sql1 = "delete from role_permission where id in(";
            boolean perFound = false;
            if (resultSet.next()) {
                perFound = true;
                sql1 += resultSet.getInt("pid");
            }
            while (resultSet.next()) {
                sql1 += "," + resultSet.getInt("pid");
            }
            resultSet.close();
            sql1 += ");";
            if (perFound) {
                stmt.execute(sql1);
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleteing role permission: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
