package com.myapp.struts.role;

import com.myapp.struts.user.PermissionDTO;

public class RoleDTO {

    public boolean searchWithRoleName = false;
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long roleCreatedByID;
    private String roleCreatedBy;
    private String roleName;
    private String roleDescription;
    private PermissionDTO permissionDTO;

    public RoleDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRoleCreatedByID() {
        return roleCreatedByID;
    }

    public void setRoleCreatedByID(long roleCreatedByID) {
        this.roleCreatedByID = roleCreatedByID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCreatedBy() {
        return roleCreatedBy;
    }

    public void setRoleCreatedBy(String roleCreatedBy) {
        this.roleCreatedBy = roleCreatedBy;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public PermissionDTO getPermissionDTO() {
        return permissionDTO;
    }

    public void setPermissionDTO(PermissionDTO permissionDTO) {
        this.permissionDTO = permissionDTO;
    }
}
