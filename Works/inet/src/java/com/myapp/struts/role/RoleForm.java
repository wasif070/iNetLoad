package com.myapp.struts.role;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class RoleForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private long id;
    private long roleCreatedByID;
    private String roleName;
    private String roleDescription;
    private String message;
    private long[] selectedIDs;
    private long[] permissionIDs;
    private ArrayList roleList;

    public RoleForm() {
        super();
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRoleCreatedByID() {
        return roleCreatedByID;
    }

    public void setRoleCreatedByID(long roleCreatedByID) {
        this.roleCreatedByID = roleCreatedByID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public long[] getPermissionIDs() {
        return permissionIDs;
    }

    public void setPermissionIDs(long[] permissionIDs) {
        this.permissionIDs = permissionIDs;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList getRoleList() {
        return roleList;
    }

    public void setRoleList(ArrayList roleList) {
        this.roleList = roleList;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getRoleName() == null || getRoleName().length() < 1) {
                errors.add("roleName", new ActionMessage("errors.role_name.required"));
            } 
        }
        return errors;
    }
}
