package com.myapp.struts.role;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;

public class EditRoleAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);

        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
            RoleForm formBean = (RoleForm) form;
            RoleDTO dto = new RoleDTO();

            RoleTaskSchedular scheduler = new RoleTaskSchedular();

            dto.setId(formBean.getId());
            dto.setRoleName(formBean.getRoleName());
            dto.setRoleDescription(formBean.getRoleDescription());

            MyAppError error = scheduler.editRoleInformation(dto,formBean.getPermissionIDs());

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                formBean.setMessage(false, "Role is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + request.getParameter("link"), true);
                return changedActionForward;
            }
        } else {
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
