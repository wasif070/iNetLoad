package com.myapp.struts.role;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListRoleAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }

            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            RoleTaskSchedular scheduler = new RoleTaskSchedular();
            RoleForm roleForm = (RoleForm) form;

            int action = 0;
            if (request.getParameter("action") != null) {
                action = Integer.parseInt(request.getParameter("action"));
                String idList = "";
                if (action > Constants.UPDATE) {
                    if (roleForm.getSelectedIDs() != null) {
                        int length = roleForm.getSelectedIDs().length;
                        long[] idListArray = roleForm.getSelectedIDs();
                        if (length > 0) {
                            idList += idListArray[0];
                            for (int i = 1; i < length; i++) {
                                idList += "," + idListArray[i];
                            }
                            MyAppError error = new MyAppError();
                            switch (action) {
                                case Constants.DELETE:
                                    error = scheduler.deleteRoles(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        roleForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        roleForm.setMessage(false, "Roles are deleted successfully.");
                                    }
                                    request.getSession(true).setAttribute(Constants.MESSAGE, roleForm.getMessage());
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        roleForm.setMessage(true, "No role is selected.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, roleForm.getMessage());
                    }
                }
            }

            if (list_all == 0) {
                if (roleForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.ROLE_RECORD_PER_PAGE, roleForm.getRecordPerPage());
                }
                RoleDTO rdto = new RoleDTO();
                rdto.setRoleName(roleForm.getRoleName());
                if (roleForm.getRoleName() != null && roleForm.getRoleName().trim().length() > 0) {
                    rdto.searchWithRoleName = true;
                    rdto.setRoleName(roleForm.getRoleName().toLowerCase());
                }
                roleForm.setRoleList(scheduler.getRoleDTOsWithSearchParam(rdto));
            } else if (list_all == 2) {
                if (action <= Constants.ADD) {
                    roleForm.setMessage(false, "Role is added successfully.");
                }
                if (request.getSession(true).getAttribute(Constants.ROLE_RECORD_PER_PAGE) != null) {
                    roleForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.ROLE_RECORD_PER_PAGE).toString()));
                }
                request.getSession(true).setAttribute(Constants.MESSAGE, roleForm.getMessage());
                roleForm.setRoleList(scheduler.getRoleDTOsSorted());
            } else {
                if (request.getSession(true).getAttribute(Constants.ROLE_RECORD_PER_PAGE) != null) {
                    roleForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.ROLE_RECORD_PER_PAGE).toString()));
                }
                roleForm.setRoleList(scheduler.getRoleDTOs());
            }

            roleForm.setSelectedIDs(null);
            if (roleForm.getRoleList() != null && roleForm.getRoleList().size() > 0 && roleForm.getRoleList().size() <= (roleForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
