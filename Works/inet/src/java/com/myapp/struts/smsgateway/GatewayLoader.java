package com.myapp.struts.smsgateway;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class GatewayLoader {

    private static long LOADING_INTERVAL = 60 * 1000;
    private long loadingTime = 0;
    public HashMap<Long, GatewayDTO> allGatewayDTOsById;
    public HashMap<String, GatewayDTO> allGatewayDTOsBySIMId;
    private ArrayList<GatewayDTO> allGatewayDTOList;
    public HashMap<Long, GatewayDTO> gatewayDTOsById;
    private ArrayList<GatewayDTO> gatewayDTOList;
    static GatewayLoader gatewayLoader = null;
    static Logger logger = Logger.getLogger(GatewayLoader.class.getName());

    public GatewayLoader() {
        forceReload();
    }

    public static GatewayLoader getInstance() {
        if (gatewayLoader == null) {
            createGatewayLoader();
        }
        return gatewayLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (gatewayLoader == null) {
            gatewayLoader = new GatewayLoader();
        }
    }

    private void reload() {
        allGatewayDTOsById = new HashMap<Long, GatewayDTO>();
        allGatewayDTOsBySIMId = new HashMap<String, GatewayDTO>();
        allGatewayDTOList = new ArrayList<GatewayDTO>();
        gatewayDTOsById = new HashMap<Long, GatewayDTO>();
        gatewayDTOList = new ArrayList<GatewayDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select id,sim_type,sim_id,status_id,phone_number,query from inetload_gateway where sim_type in(" + Constants.SENDER  + "," + Constants.RECEIVER + ")";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                GatewayDTO dto = new GatewayDTO();
                dto.setId(rs.getLong("id"));
                dto.setTypeID(rs.getInt("sim_type"));
                dto.setSimID(rs.getString("sim_id"));
                dto.setGatewayStatus(rs.getInt("status_id"));
                dto.setPhoneNo(rs.getString("phone_number"));
                dto.setQuery(rs.getString("query")); 
                if (dto.getGatewayStatus() != Constants.FLEXI_SIM_STATUS_DELETED) {
                    gatewayDTOList.add(dto);
                    gatewayDTOsById.put(dto.getId(), dto);
                }
                allGatewayDTOList.add(dto);
                allGatewayDTOsById.put(dto.getId(), dto);
                allGatewayDTOsBySIMId.put(dto.getSimID(), dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getGatewayDTOList() {
        checkForReload();
        return gatewayDTOList;
    }

    public synchronized GatewayDTO getGatewayDTOByID(long id) {
        checkForReload();
        return gatewayDTOsById.get(id);
    }

    public synchronized ArrayList getAllGatewayDTOList() {
        checkForReload();
        return allGatewayDTOList;
    }

    public synchronized GatewayDTO getAllGatewayDTOByID(long id) {
        checkForReload();
        return allGatewayDTOsById.get(id);
    }

//    public synchronized GatewayDTO getAllGatewayDTOBySIMID(String simID) {
//        checkForReload();
//        return (GatewayDTO) allGatewayDTOsBySIMId.get(simID);
//    }
}
