package com.myapp.struts.smsgateway;

import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class GatewayTaskScheduler
{
    public MyAppError editGateway(GatewayDTO gwdto)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.editGateway(gwdto);
    }

    public MyAppError deactivateGateways(String list)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.deactivateGateways(list);
    }

    public MyAppError activateGateways(String list)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.activateGateways(list);
    }

    public MyAppError deleteGateways(String list)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.deleteGateways(list);
    }

    public MyAppError deleteGatewayMessages(String list)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.deleteGatewayMessages(list);
    }

    public ArrayList getGatewayDTOsWithSearchParam(GatewayDTO gwdto)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        ArrayList list = GatewayLoader.getInstance().getGatewayDTOList();
        if(list != null)
        {
            return gatewayDAO.getGatewayDTOsWithSearchParam((ArrayList)list.clone(), gwdto);
        } else
        {
            return null;
        }
    }

    public ArrayList getGatewayMessageDTOsWithSearchParam(GatewayDTO gwdto)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.getGatewayMessageDTOsWithSearchParam(gwdto);
    }

    public ArrayList getGatewayMessageDTOs(GatewayDTO gwdto)
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        return gatewayDAO.getGatewayMessageDTOs(gwdto);
    }

    public ArrayList getGatewayDTOsSorted()
    {
        GatewayDAO gatewayDAO = new GatewayDAO();
        ArrayList list = GatewayLoader.getInstance().getGatewayDTOList();
        if(list != null)
        {
            return gatewayDAO.getGatewayDTOsSorted((ArrayList)list.clone());
        } else
        {
            return null;
        }
    }

    public ArrayList getGatewayDTOs()
    {
        return GatewayLoader.getInstance().getGatewayDTOList();
    }

    public GatewayDTO getGatewayDTO(long id)
    {
        return GatewayLoader.getInstance().getGatewayDTOByID(id);
    }
}
