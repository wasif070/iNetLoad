package com.myapp.struts.smsgateway;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import java.util.ArrayList;

public class GatewayForm extends org.apache.struts.action.ActionForm {

    private int operatorID;
    private int operatorIDPar;
    private int gatewayStatus;
    private int doValidate;
    private int pageNo;
    private int recordPerPage;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int simVersion;
    private int typeID;
    private int typeIDPar;  
    private long id;
    private long logTime;
    private long rechargeTime;
    private long dealerID;
    private double balance;    
    private String simID;
    private String simIDPar;
    private String pinNumber;
    private String phoneNo;
    private String phoneNoPar;
    private String textPar;
    private String operatorName;
    private String logMessage;
    private String query;
    private String message;
    private ArrayList gatewayList;
    private ArrayList gatewayMessageList;
    private long[] selectedIDs;
    private double[] amounts;
    private String[] descriptions;
    private String[] dates;

    public GatewayForm() {
        super();
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis());
        this.startDay = Integer.parseInt(dateParts[0]);
        this.startMonth = Integer.parseInt(dateParts[1]);
        this.startYear = Integer.parseInt(dateParts[2]);
        this.endDay = this.startDay;
        this.endMonth = this.startMonth;
        this.endYear = this.startYear;
        this.operatorIDPar = -1;
        this.typeIDPar = -1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPhoneNoPar() {
        return phoneNoPar;
    }

    public void setPhoneNoPar(String phoneNoPar) {
        this.phoneNoPar = phoneNoPar;
    }
    
    public String getTextPar() {
        return textPar;
    }

    public void setTextPar(String textPar) {
        this.textPar = textPar;
    }    

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
    
    public int getTypeID()
    {
        return typeID;
    }

    public void setTypeID(int typeID)
    {
        this.typeID = typeID;
    }

    public int getTypeIDPar()
    {
        return typeIDPar;
    }

    public void setTypeIDPar(int typeIDPar)
    {
        this.typeIDPar = typeIDPar;
    }    

    public ArrayList getGatewayList() {
        return gatewayList;
    }

    public void setGatewayList(ArrayList gatewayList) {
        this.gatewayList = gatewayList;
    }

    public ArrayList getGatewayMessageList() {
        return gatewayMessageList;
    }

    public void setGatewayMessageList(ArrayList gatewayMessageList) {
        this.gatewayMessageList = gatewayMessageList;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }
    
    public void setSimVersion(int simVersion) {
        this.simVersion = simVersion;
    }

    public int getSimVersion() {
        return this.simVersion;
    }    

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getGatewayStatus() {
        return gatewayStatus;
    }

    public void setGatewayStatus(int gatewayStatus) {
        this.gatewayStatus = gatewayStatus;
    }

    public int getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(int operatorID) {
        this.operatorID = operatorID;
    }

    public int getOperatorIDPar() {
        return operatorIDPar;
    }

    public void setOperatorIDPar(int operatorIDPar) {
        this.operatorIDPar = operatorIDPar;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public String getSimID() {
        return simID;
    }

    public void setSimID(String simID) {
        this.simID = simID;
    }

    public String getSimIDPar() {
        return simIDPar;
    }

    public void setSimIDPar(String simIDPar) {
        this.simIDPar = simIDPar;
    }

    public long getRechargeTime() {
        return rechargeTime;
    }

    public void setRechargeTime(long rechargeTime) {
        this.rechargeTime = rechargeTime;
    }

    public long getLogTime() {
        return logTime;
    }

    public void setLogTime(long logTime) {
        this.logTime = logTime;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public double[] getAmounts() {
        return amounts;
    }

    public void setAmounts(double[] amounts) {
        this.amounts = amounts;
    }

    public String[] getDates() {
        return dates;
    }

    public void setDates(String[] dates) {
        this.dates = dates;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }
    
    public long getDealerID() {
        return dealerID;
    }

    public void setDealerID(long dealerID) {
        this.dealerID = dealerID;
    }  
    
    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }      

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if(getId() == 0)
            {
                if (this.getPhoneNo() == null || this.getPhoneNo().length() == 0) {
                    errors.add("phoneNo", new ActionMessage("errors.phnNumber.required"));
                }
            }
        }
        return errors;
    }
}
