package com.myapp.struts.smsgateway;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import javax.servlet.http.*;
import org.apache.struts.action.*;

public class GetGatewayAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute("LOGIN_DTO");
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            GatewayForm formBean = (GatewayForm) form;
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.SMS_SETTINGS_EDIT) {
                long id = Long.parseLong(request.getParameter("id"));
                GatewayDTO dto = new GatewayDTO();
                GatewayTaskScheduler scheduler = new GatewayTaskScheduler();
                dto = scheduler.getGatewayDTO(id);
                if (dto != null) {
                    formBean.setId(dto.getId());
                    formBean.setTypeID(dto.getTypeID());
                    formBean.setSimID(dto.getSimID());
                    formBean.setGatewayStatus(dto.getGatewayStatus());
                    formBean.setPhoneNo(dto.getPhoneNo());
                    formBean.setQuery(dto.getQuery());
                }
                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }
        return mapping.findForward(target);
    }
}
