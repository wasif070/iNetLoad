package com.myapp.struts.smsgateway;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.session.Constants;
import java.sql.Statement;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class GatewayDAO {

    static Logger logger = Logger.getLogger(GatewayDAO.class.getName());

    public GatewayDAO() {
    }

    public ArrayList<GatewayDTO> getGatewayDTOsWithSearchParam(ArrayList<GatewayDTO> list, GatewayDTO gwdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                GatewayDTO dto = (GatewayDTO) i.next();
                if ((gwdto.searchWithPhoneNo && !dto.getPhoneNo().toLowerCase().startsWith(gwdto.getPhoneNoPar()))
                        || (gwdto.searchWithSIMID && !dto.getSimID().toLowerCase().startsWith(gwdto.getSimIDPar()))
                        || (gwdto.searchWithTypeID && dto.getTypeID() != gwdto.getTypeIDPar())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<GatewayDTO> getGatewayDTOsSorted(ArrayList<GatewayDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    GatewayDTO dto1 = (GatewayDTO) o1;
                    GatewayDTO dto2 = (GatewayDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError editGateway(GatewayDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            sql = "update inetload_gateway set sim_type=" + dto.getTypeID() + ",phone_number='" + dto.getPhoneNo() + "',query='" + dto.getQuery() + "',status_id=" + dto.getGatewayStatus() + " where id=" + dto.getId();
            statement.execute(sql);
            GatewayLoader.getInstance().forceReload();
            com.myapp.struts.gateway.GatewayLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing sms gateway request: ", ex);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deactivateGateways(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update inetload_gateway set status_id = " + Constants.FLEXI_SIM_STATUS_INACTIVE + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            GatewayLoader.getInstance().forceReload();
            com.myapp.struts.gateway.GatewayLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating sms sim status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateGateways(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update inetload_gateway set status_id = " + Constants.FLEXI_SIM_STATUS_ACTIVE + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            GatewayLoader.getInstance().forceReload();
            com.myapp.struts.gateway.GatewayLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating sms sim status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteGateways(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update inetload_gateway set status_id = " + Constants.FLEXI_SIM_STATUS_DELETED + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            GatewayLoader.getInstance().forceReload();
            com.myapp.struts.gateway.GatewayLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting sms sim: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteGatewayMessages(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from sms_log where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            GatewayLoader.getInstance().forceReload();
            com.myapp.struts.gateway.GatewayLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting sms sim log: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList<GatewayDTO> getGatewayMessageDTOs(GatewayDTO gwdto) {
        ArrayList<GatewayDTO> data = new ArrayList<GatewayDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(gwdto.getStartYear(), gwdto.getStartMonth() - 1, gwdto.getStartDay(), 0, 0, 0).getTimeInMillis();
        long end = new GregorianCalendar(gwdto.getEndYear(), gwdto.getEndMonth() - 1, gwdto.getEndDay(), 23, 59, 59).getTimeInMillis();
        String sql = "select * from sms_log where log_time between " + start + " and " + end;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {               
                GatewayDTO dto = new GatewayDTO();
                dto.setId(rs.getLong("id"));
                dto.setPhoneNo(" ");
                dto.setSimID(" ");
                GatewayDTO gdto = GatewayLoader.getInstance().getAllGatewayDTOByID(rs.getLong("gateway_id"));
                if(gdto != null)
                {    
                    dto.setPhoneNo(gdto.getPhoneNo());
                    dto.setSimID(gdto.getSimID());
                }
                dto.setPhoneNoPar(rs.getString("mobile_no"));
                dto.setTypeID(rs.getInt("sms_type"));
                dto.setLogTime(rs.getLong("log_time"));
                dto.setLogMessage(rs.getString("sms"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in sms getGatewayMessageDTOs: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<GatewayDTO> getGatewayMessageDTOsWithSearchParam(GatewayDTO gwdto) {
        ArrayList<GatewayDTO> data = new ArrayList<GatewayDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(gwdto.getStartYear(), gwdto.getStartMonth() - 1, gwdto.getStartDay(), 0, 0, 0).getTimeInMillis();
        long end = new GregorianCalendar(gwdto.getEndYear(), gwdto.getEndMonth() - 1, gwdto.getEndDay(), 23, 59, 59).getTimeInMillis();
        String sql = "select * from sms_log where log_time between " + start + " and " + end;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                GatewayDTO dto = new GatewayDTO();
                dto.setId(rs.getLong("id"));
                dto.setPhoneNo(" ");
                dto.setSimID(" ");
                GatewayDTO gdto = GatewayLoader.getInstance().getAllGatewayDTOByID(rs.getLong("gateway_id"));
                if(gdto != null)
                {    
                    dto.setPhoneNo(gdto.getPhoneNo());
                    dto.setSimID(gdto.getSimID());
                }
                dto.setPhoneNoPar(rs.getString("mobile_no"));
                dto.setTypeID(rs.getInt("sms_type"));
                dto.setLogTime(rs.getLong("log_time"));
                dto.setLogMessage(rs.getString("sms"));
                if ((!gwdto.searchWithPhoneNo || dto.getPhoneNoPar().toLowerCase().startsWith(gwdto.getPhoneNoPar())) && (!gwdto.searchWithTypeID || dto.getTypeID() == gwdto.getTypeIDPar())) {
                    data.add(dto);
                }
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in sms getGatewayMessageDTOsWithSearchParam: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
}
