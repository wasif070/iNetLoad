package com.myapp.struts.smsgateway;

import com.myapp.struts.util.Utils;

public class GatewayDTO
{
    public boolean searchWithTypeID;
    public boolean searchWithSIMID;
    public boolean searchWithPhoneNo;
    public boolean searchWithOperatorID;
    public boolean searchWithText;
    private int operatorID;
    private int operatorIDPar;    
    private int typeID;
    private int typeIDPar;
    private int gatewayStatus;
    private int doValidation;
    private int pageNo;
    private int recordPerPage;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int simVersion;    
    private long id;
    private long logTime;
    private long dealerID;
    private long rechargeTime;
    private double balance;
    private double rechargeAmount;
    private String textPar;
    private String operatorName;    
    private String simID;
    private String simIDPar;
    private String phoneNo;
    private String phoneNoPar;
    private String logMessage;
    private String query;
    private String pinNumber;
    private String rechargeDescription;

    public GatewayDTO()
    {
        searchWithTypeID = false;
        searchWithOperatorID = false;
        searchWithSIMID = false;
        searchWithPhoneNo = false;
        searchWithText = false;
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis());
        startDay = Integer.parseInt(dateParts[0]);
        startMonth = Integer.parseInt(dateParts[1]);
        startYear = Integer.parseInt(dateParts[2]);
        endDay = startDay;
        endMonth = startMonth;
        endYear = startYear;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }   
    
    public int getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(int operatorID) {
        this.operatorID = operatorID;
    }    

    public int getTypeID()
    {
        return typeID;
    }

    public void setTypeID(int typeID)
    {
        this.typeID = typeID;
    }

    public int getTypeIDPar()
    {
        return typeIDPar;
    }

    public void setTypeIDPar(int typeIDPar)
    {
        this.typeIDPar = typeIDPar;
    }
    
    public int getOperatorIDPar() {
        return operatorIDPar;
    }

    public void setOperatorIDPar(int operatorIDPar) {
        this.operatorIDPar = operatorIDPar;
    }
    
    public void setSimVersion(int simVersion) {
        this.simVersion = simVersion;
    }

    public int getSimVersion() {
        return this.simVersion;
    }    
    
    public long getDealerID() {
        return dealerID;
    }

    public void setDealerID(long dealerID) {
        this.dealerID = dealerID;
    }  
    
    public long getRechargeTime() {
        return rechargeTime;
    }

    public void setRechargeTime(long rechargeTime) {
        this.rechargeTime = rechargeTime;
    } 
    
    public double getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(double rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }     
    
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }    
    
    public String getRechargeDescription() {
        return rechargeDescription;
    }

    public void setRechargeDescription(String rechargeDescription) {
        this.rechargeDescription = rechargeDescription;
    }    
        

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }    
    
    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }    

    public String getPhoneNo()
    {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String getPhoneNoPar()
    {
        return phoneNoPar;
    }

    public void setPhoneNoPar(String phoneNoPar)
    {
        this.phoneNoPar = phoneNoPar;
    }
    
    public String getTextPar() {
        return textPar;
    }

    public void setTextPar(String textPar) {
        this.textPar = textPar;
    }        
    
    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }    

    public int getPageNo()
    {
        return pageNo;
    }

    public void setPageNo(int pageNo)
    {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage()
    {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage)
    {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidation()
    {
        return doValidation;
    }

    public void setDoValidation(int doValidation)
    {
        this.doValidation = doValidation;
    }

    public int getGatewayStatus()
    {
        return gatewayStatus;
    }

    public void setGatewayStatus(int gatewayStatus)
    {
        this.gatewayStatus = gatewayStatus;
    }

    public String getSimID()
    {
        return simID;
    }

    public void setSimID(String simID)
    {
        this.simID = simID;
    }

    public String getSimIDPar()
    {
        return simIDPar;
    }

    public void setSimIDPar(String simIDPar)
    {
        this.simIDPar = simIDPar;
    }

    public int getEndDay()
    {
        return endDay;
    }

    public void setEndDay(int endDay)
    {
        this.endDay = endDay;
    }

    public int getEndMonth()
    {
        return endMonth;
    }

    public void setEndMonth(int endMonth)
    {
        this.endMonth = endMonth;
    }

    public int getEndYear()
    {
        return endYear;
    }

    public void setEndYear(int endYear)
    {
        this.endYear = endYear;
    }

    public int getStartDay()
    {
        return startDay;
    }

    public void setStartDay(int startDay)
    {
        this.startDay = startDay;
    }

    public int getStartMonth()
    {
        return startMonth;
    }

    public void setStartMonth(int startMonth)
    {
        this.startMonth = startMonth;
    }

    public int getStartYear()
    {
        return startYear;
    }

    public void setStartYear(int startYear)
    {
        this.startYear = startYear;
    }

    public long getLogTime()
    {
        return logTime;
    }

    public void setLogTime(long logTime)
    {
        this.logTime = logTime;
    }

    public String getLogMessage()
    {
        return logMessage;
    }

    public void setLogMessage(String logMessage)
    {
        this.logMessage = logMessage;
    }     
}
