package com.myapp.struts.login;

import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class PassForm extends org.apache.struts.action.ActionForm {

    private int doValidate;
    private String password;
    private String retypePassword;
    private String message;

    public PassForm() {
        super();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getPassword() == null || getPassword().length() < 1) {
                errors.add("password", new ActionMessage("errors.user_pass.required"));
                return errors;
            }

            if (getRetypePassword() == null || getRetypePassword().length() < 1) {
                errors.add("retypePassword", new ActionMessage("errors.retype_pass.required"));
                return errors;
            }

            if (getPassword().length() < 6) {
                errors.add("retypePassword", new ActionMessage("errors.password_length"));
                return errors;
            }

            if (!getRetypePassword().equals(getPassword())) {
                errors.add("retypePassword", new ActionMessage("errors.password_matching"));
                return errors;
            }
        }
        return errors;
    }
}
