package com.myapp.struts.login;

import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class LoginForm extends org.apache.struts.action.ActionForm {

    private int doValidate;
    private int clientVersion;
    private long loginTime;
    private String loginId;
    private String loginPass;
    private String message = "<span style='color:#000000; font-size: 12px;font-weight:bold'>" + SettingsLoader.getInstance().getSettingsDTO().getMotto() + "</span>";

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginPass() {
        return loginPass;
    }

    public void setLoginPass(String loginPass) {
        this.loginPass = loginPass;
    }
    
    public int getClientVersion() {
        return clientVersion;
    }
    
    public void setClientVersion(int clientVersion) {
        this.clientVersion = clientVersion;
    }       

    public long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    public LoginForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getLoginId() == null || getLoginId().trim().length() == 0) {
                errors.add("loginId", new ActionMessage("errors.user_id.required"));
            }
            if (getLoginPass() == null || getLoginPass().length() < 1) {
                errors.add("loginPass", new ActionMessage("errors.user_pass.required"));
            }
        }

        return errors;
    }
}
