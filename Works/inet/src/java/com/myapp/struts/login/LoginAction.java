package com.myapp.struts.login;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.UserDTO;
import com.myapp.struts.user.UserLoader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginAction extends org.apache.struts.action.Action {
    
    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    static Logger logger = Logger.getLogger(LoginAction.class.getName());
    
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = SUCCESS;
        LoginForm formBean = (LoginForm) form;
        LoginDTO login_dto = null;
        ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByUserIdPass(formBean.getLoginId(), formBean.getLoginPass(), formBean.getClientVersion());
        if (clientDTO != null) {
            login_dto = new LoginDTO();
            login_dto.setId(clientDTO.getId());
            login_dto.setClientId(clientDTO.getClientId());
            login_dto.setClientEmail(clientDTO.getClientEmail());
            login_dto.setClientName(clientDTO.getClientName());
            login_dto.setClientPassword(clientDTO.getClientPassword());
            login_dto.setClientStatus(clientDTO.getClientStatus());
            login_dto.setClientTypeId(clientDTO.getClientTypeId());
            login_dto.setClientParentId(clientDTO.getParentId());
            login_dto.setIsBTActive(clientDTO.clientBTActive);
            login_dto.setIsSMSActive(clientDTO.clientSMSActive);
            //clientDTO.clientBTActive
            login_dto.setRoleID(-1);
            login_dto.setIsUser(false);
            if (clientDTO.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && clientDTO.getParentId() == Constants.ROOT_RESELLER_PARENT_ID) {
                login_dto.setIsSpecialAgent(true);                
            }
        } else {
            UserDTO userDTO = UserLoader.getInstance().getUserDTOByUserIDPass(formBean.getLoginId(), formBean.getLoginPass());
            if (userDTO != null) {
                login_dto = new LoginDTO();
                login_dto.setId(userDTO.getId());
                login_dto.setClientId(userDTO.getUserId());
                login_dto.setClientEmail(userDTO.getUserEmail());
                login_dto.setClientName(userDTO.getUserName());
                login_dto.setClientPassword(userDTO.getUserPassword());
                login_dto.setClientStatus(userDTO.getUserStatus());
                login_dto.setRoleID(userDTO.getUserRoleId());
                login_dto.setCreatedByID(userDTO.getUserCreatedById());
                login_dto.setIsUser(true);
            }
        }
        if (login_dto == null) {
            formBean.setMessage(true, "User Name or Password is not valid");
            target = FAILURE;
            request.setAttribute(mapping.getAttribute(), form);
        } else if (login_dto.getClientStatus() == Constants.USER_STATUS_BLOCK) {
            formBean.setMessage(true, "User Name is blocked");
            target = FAILURE;
            request.setAttribute(mapping.getAttribute(), form);
        } else if (login_dto.getId() > 0) {
            login_dto.setLoginTime(System.currentTimeMillis());
            request.getSession(true).setAttribute(Constants.LOGIN_DTO, login_dto);
        }
        return mapping.findForward(target);
    }
}
