package com.myapp.struts.login;

import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.UserLoader;
import databaseconnector.DBConnection;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ChangePassAction extends org.apache.struts.action.Action {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    static Logger logger = Logger.getLogger(ChangePassAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = SUCCESS;
        DBConnection dbConnection = null;
        Statement stmt = null;

        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        PassForm formBean = (PassForm) form;
        try {
            if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                String sql = "update client set client_password='" + formBean.getPassword() + "' where id=" + login_dto.getId();
                if (login_dto.getIsUser()) {
                    sql = "update user set user_password='" + formBean.getPassword() + "' where id=" + login_dto.getId();
                }
                stmt = dbConnection.connection.prepareStatement(sql);
                stmt.executeUpdate(sql);
                if (login_dto.getIsUser()) {
                    UserLoader.getInstance().forceReload();
                } else {
                    ClientLoader.getInstance().forceReload();
                }
            }
        } catch (Exception ex) {
            target = FAILURE;
            formBean.setMessage(true, "Database Error.");
            logger.fatal("Error while changing password: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mapping.findForward(target);
    }
}
