package com.myapp.struts.flexi;

public class OperatorTypeDTO {
    private String clientId;
    private double grameenPhone;
    private double robi;
    private double banglaLink;
    private double warid;
    private double teleTalk;
    private double cityCell;
    private double card;
    private double balanceTransfer;
    private double sms;
    private double surcharge;
    private long reportDate;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public double getBanglaLink() {
        return banglaLink;
    }

    public void setBanglaLink(double banglaLink) {
        this.banglaLink = banglaLink;
    }

    public double getCityCell() {
        return cityCell;
    }

    public void setCityCell(double cityCell) {
        this.cityCell = cityCell;
    }
    
    public double getBalanceTransfer() {
        return balanceTransfer;
    }

    public void setBalanceTransfer(double balanceTransfer) {
        this.balanceTransfer = balanceTransfer;
    }
    
    public double getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(double surcharge) {
        this.surcharge = surcharge;
    }    
    
    public double getCard() {
        return card;
    }

    public void setCard(double card) {
        this.card = card;
    }    

    public double getGrameenPhone() {
        return grameenPhone;
    }

    public void setGrameenPhone(double grameenPhone) {
        this.grameenPhone = grameenPhone;
    }

    public double getRobi() {
        return robi;
    }

    public void setRobi(double robi) {
        this.robi = robi;
    }

    public double getTeleTalk() {
        return teleTalk;
    }

    public void setTeleTalk(double teleTalk) {
        this.teleTalk = teleTalk;
    }

    public double getWarid() {
        return warid;
    }

    public void setWarid(double warid) {
        this.warid = warid;
    }
    
   

    public long getReportDate() {
        return reportDate;
    }

    public void setReportDate(long reportDate) {
        this.reportDate = reportDate;
    }

    public double getSms() {
        return sms;
    }

    public void setSms(double sms) {
        this.sms = sms;
    }
    
}
