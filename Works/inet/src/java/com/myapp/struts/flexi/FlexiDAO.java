package com.myapp.struts.flexi;

import com.myapp.struts.api.APIDTO;
import com.myapp.struts.api.APILoader;
import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.log4j.Logger;

public class FlexiDAO {

    static Logger logger = Logger.getLogger(FlexiDAO.class.getName());

    public FlexiDAO() {
    }

    public MyAppError addFlexiRequest(LoginDTO login_dto, FlexiDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        String marker = System.currentTimeMillis() + ":" + p_dto.getPhoneNumber();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String insertSQL = "insert into inetload_fdr(operator_type_id,inetload_phn_no,"
                    + "inetload_from_user_id,inetload_from_client_type_id,inetload_to_user_id,inetload_to_client_type_id,inetload_type_id,"
                    + "inetload_amount,inetload_time,inetload_marker,inetload_status,inetload_contact_id,inetload_group_id,surcharge,account_type) values";
            int fromTypeID = Constants.CLIENT_TYPE_AGENT;
            int toTypeID = fromTypeID;
            long fromID = login_dto.getId();
            long toID = fromID;
            ClientDTO clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getId());
            if ((clDTO.getClientCredit() - p_dto.getAmount()) < clDTO.getClientDeposit()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Your Credit Limit is Over.");
                return error;
            }
            switch (p_dto.getOperatorTypeID()) {
                case Constants.GP:
                    if (clDTO.clientGPActive == 0) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("You are not allowed for Grameen Refill.");
                        return error;
                    }
                    break;
                case Constants.BL:
                    if (clDTO.clientBLActive == 0) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("You are not allowed for BanglaLink Refill.");
                        return error;
                    }
                    break;
                case Constants.TT:
                    if (clDTO.clientTTActive == 0) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("You are not allowed for TeleTalk Refill.");
                        return error;
                    }
                    break;
                case Constants.CC:
                    if (clDTO.clientCCActive == 0) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("You are not allowed for Citycell Refill.");
                        return error;
                    }
                    break;
                case Constants.WR:
                    if (clDTO.clientWRActive == 0) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("You are not allowed for Airtel Refill.");
                        return error;
                    }
                    break;
                case Constants.RB:
                    if (clDTO.clientRBActive == 0) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("You are not allowed for Robi Refill.");
                        return error;
                    }
                    break;
                case Constants.BT:
                    if (clDTO.clientBTActive == 0) {
                        error.setErrorType(MyAppError.ValidationError);
                        error.setErrorMessage("You are not allowed for Balance Trasfer.");
                        return error;
                    }
                    break;
            }
            insertSQL += "(" + p_dto.getOperatorTypeID() + ",'" + p_dto.getPhoneNumber() + "'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ","
                    + p_dto.getRefillType() + "," + p_dto.getAmount() + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING + ","
                    + p_dto.getFlexiContactID() + "," + p_dto.getFlexiGroupID() + "," + p_dto.getSurcharge() + "," + p_dto.getAccountType() + ")";

            clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getClientParentId());
            while (clDTO != null) {
                if (clDTO.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Your parent is not active.");
                    return error;
                }
                if (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && clDTO.getClientDeposit() > 0 && (clDTO.getClientCredit() - p_dto.getAmount()) < clDTO.getClientDeposit()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Reseller Credit Limit is Over.");
                    return error;
                } else if (clDTO.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && (clDTO.getClientCredit() - p_dto.getAmount()) < clDTO.getClientDeposit()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Reseller Credit Limit is Over.");
                    return error;
                }
                switch (p_dto.getOperatorTypeID()) {
                    case Constants.GP:
                        if (clDTO.clientGPActive == 0) {
                            error.setErrorType(MyAppError.ValidationError);
                            error.setErrorMessage("You are not allowed for Grameen Refill.");
                            return error;
                        }
                        break;
                    case Constants.BL:
                        if (clDTO.clientBLActive == 0) {
                            error.setErrorType(MyAppError.ValidationError);
                            error.setErrorMessage("You are not allowed for BanglaLink Refill.");
                            return error;
                        }
                        break;
                    case Constants.TT:
                        if (clDTO.clientTTActive == 0) {
                            error.setErrorType(MyAppError.ValidationError);
                            error.setErrorMessage("You are not allowed for TeleTalk Refill.");
                            return error;
                        }
                        break;
                    case Constants.CC:
                        if (clDTO.clientCCActive == 0) {
                            error.setErrorType(MyAppError.ValidationError);
                            error.setErrorMessage("You are not allowed for Citycell Refill.");
                            return error;
                        }
                        break;
                    case Constants.WR:
                        if (clDTO.clientWRActive == 0) {
                            error.setErrorType(MyAppError.ValidationError);
                            error.setErrorMessage("You are not allowed for Airtel Refill.");
                            return error;
                        }
                        break;
                    case Constants.RB:
                        if (clDTO.clientRBActive == 0) {
                            error.setErrorType(MyAppError.ValidationError);
                            error.setErrorMessage("You are not allowed for Robi Refill.");
                            return error;
                        }
                        break;
                    case Constants.BT:
                        if (clDTO.clientBTActive == 0) {
                            error.setErrorType(MyAppError.ValidationError);
                            error.setErrorMessage("You are not allowed for Balance Trasfer.");
                            return error;
                        }
                }
                toID = clDTO.getId();
                toTypeID = clDTO.getClientTypeId();
                insertSQL += ",(" + p_dto.getOperatorTypeID() + ",'" + p_dto.getPhoneNumber() + "'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ","
                        + p_dto.getRefillType() + "," + p_dto.getAmount() + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING + ","
                        + p_dto.getFlexiContactID() + "," + p_dto.getFlexiGroupID() + "," + p_dto.getSurcharge() + "," + p_dto.getAccountType() + ")";
                clDTO = ClientLoader.getInstance().getClientDTOByID(clDTO.getParentId());
            }
            stmt.execute(insertSQL);
        } catch (Exception ex) {
            logger.fatal("Error while adding flexi request: ", ex);
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error is Occured.");
            return error;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError getFlexiResponse(long cl_id, String reqID) {
        String message = "Status:";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select inetload_status,inetload_transaction_id,inetload_message from inetload_fdr where inetload_to_user_id=" + cl_id + " and inetload_from_user_id=" + cl_id + " and inetload_marker = '" + reqID + "'";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                message += rs.getInt("inetload_status");
                message += ",TransactionID:";
                message += rs.getString("inetload_transaction_id");
                message += ",Message:";
                message += rs.getString("inetload_message");
                error.setErrorMessage(message);
            } else {
                error.setErrorType(MyAppError.ValidationError);
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getFlexiResponse: " + ex);
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error is Occured.");
            return error;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public boolean updateStatus(LoginDTO dto, long agentID, double amount, int status, String marker, int current_status, String transactionID, int refillType, int opType, long cardID) {
        boolean updated = false;
        if (dto != null && dto.getClientStatus() == Constants.USER_STATUS_ACTIVE && status == Constants.INETLOAD_STATUS_SUCCESSFUL && current_status == Constants.INETLOAD_STATUS_SUCCESSFUL) {
            DBConnection dbConnection = null;
            Statement stmt = null;
            String sql = null;
            try {
                sql = "update inetload_fdr set inetload_transaction_id='" + transactionID + "',inetload_message='" + transactionID + "' where inetload_marker='" + marker + "'";
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                stmt = dbConnection.connection.createStatement();
                stmt.execute(sql);
                updated = true;
            } catch (Exception ex) {
                logger.debug("Error while updating flexi request: ", ex);
            } finally {
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
            }
        } else if (dto != null && dto.getClientStatus() == Constants.USER_STATUS_ACTIVE && status != current_status) {
            DBConnection dbConnection = null;
            Statement stmt = null;
            String sql = null;
            long curTime = 0;
            String transactionIDVal = "";
            String idList = null;
            if (status == Constants.INETLOAD_STATUS_SUCCESSFUL) {
                transactionIDVal = transactionID;
                curTime = System.currentTimeMillis();
            }
            if (cardID == 0) {
                idList = "(" + agentID;
                ClientDTO clDTO = ClientLoader.getInstance().getAllClientDTOByID(ClientLoader.getInstance().getAllClientDTOByID(agentID).getParentId());
                while (clDTO != null && (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2)) {
                    idList += "," + clDTO.getId();
                    clDTO = ClientLoader.getInstance().getAllClientDTOByID(clDTO.getParentId());
                }
                idList += ")";
            }
            switch (current_status) {
                case Constants.INETLOAD_STATUS_SUBMITTED:
                    if (status == Constants.INETLOAD_STATUS_SUCCESSFUL) {
                        sql = "update inetload_fdr set inetload_status=" + status + ",inetload_refill_success_time=" + curTime + ",inetload_transaction_id='" + transactionIDVal + "',inetload_message='" + transactionIDVal + "' where inetload_marker='" + marker + "'";
                    } else if (status == Constants.INETLOAD_STATUS_REJECTED) {
                        sql = makeSQLForRejected(current_status, status, amount, marker, idList, cardID);
                    }
                    break;
                case Constants.INETLOAD_STATUS_SUCCESSFUL:
                    if (status == Constants.INETLOAD_STATUS_SUBMITTED) {
                        sql = "update inetload_fdr set inetload_gw_id=0,inetload_processing_status=0,inetload_status=" + status + ",inetload_time=" + System.currentTimeMillis() + ",inetload_type_id=" + refillType + ",inetload_refill_success_time=" + curTime + ",inetload_transaction_id='" + transactionIDVal + "',inetload_message='" + transactionIDVal + "' where inetload_marker='" + marker + "'";
                    } else if (status == Constants.INETLOAD_STATUS_REJECTED) {
                        sql = makeSQLForRejected(current_status, status, amount, marker, idList, cardID);
                    }
                    break;
                case Constants.INETLOAD_STATUS_REJECTED:
                    if (status == Constants.INETLOAD_STATUS_SUBMITTED) {
                        sql = makeSQLForSubmitted(current_status, status, amount, marker, idList, transactionID, refillType, cardID);
                    } else if (status == Constants.INETLOAD_STATUS_SUCCESSFUL) {
                        sql = makeSQLForSubmitted(current_status, status, amount, marker, idList, transactionID, refillType, cardID);
                    }
                    break;
                default:
                    break;
            }
            try {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                stmt = dbConnection.connection.createStatement();
                stmt.execute(sql);
                ClientLoader.getInstance().forceReload();
                updated = true;
            } catch (Exception ex) {
                logger.debug("Error while updating flexi request: ", ex);
            } finally {
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
            }
        }
        return updated;
    }

    public String makeSQLForRejected(int current_status, int status, double amount, String marker, String idList, long cardID) {
        String sql = "update inetload_fdr set inetload_status=" + status + ",status_changed=1,inetload_refill_success_time=0 where inetload_status=" + current_status + " and inetload_marker='" + marker + "'";
        return sql;
    }

    public String makeSQLForSubmitted(int current_status, int status, double amount, String marker, String idList, String transactionID, int refillType, long cardID) {
        long curTime = 0;
        String transactionIDVal = "";
        String pendingCon = "";
        String sql = null;
        if (status == Constants.INETLOAD_STATUS_SUCCESSFUL) {
            transactionIDVal = transactionID;
            curTime = System.currentTimeMillis();
        }
        if (status == Constants.INETLOAD_STATUS_SUBMITTED) {
            status = Constants.INETLOAD_STATUS_PENDING;
            pendingCon = " ,inetload_gw_id=0, inetload_processing_status=0, inetload_time=" + System.currentTimeMillis() + ", inetload_type_id=" + refillType;
        }
        sql = "update inetload_fdr set inetload_status=" + status + pendingCon + ",status_changed=1,inetload_refill_success_time=" + curTime + ", inetload_transaction_id='" + transactionIDVal + "', inetload_message='" + transactionIDVal + "' where inetload_status=" + current_status + " and inetload_marker='" + marker + "'";
        return sql;
    }

    public long getLastRefillTime(String phn_no, long currentTime) {
        long lastRefillTime = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select inetload_time,inetload_refill_success_time from inetload_fdr where inetload_time > " + (currentTime - (SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000)) + " and inetload_phn_no='" + phn_no + "' and inetload_status!=" + Constants.INETLOAD_STATUS_REJECTED;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                lastRefillTime = rs.getLong("inetload_time");
                if (rs.getLong("inetload_refill_success_time") > 0) {
                    lastRefillTime = rs.getLong("inetload_refill_success_time");
                }
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getLastRefillTime: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return lastRefillTime;
    }

    public ArrayList<FlexiDTO> getFlexiDTOs(LoginDTO login_dto, FlexiDTO f_dto, int flexiType) {
        ArrayList<FlexiDTO> data = new ArrayList<FlexiDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(f_dto.getStartYear(), f_dto.getStartMonth() - 1, f_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        long end = new GregorianCalendar(f_dto.getEndYear(), f_dto.getEndMonth() - 1, f_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        String condition = " and operator_type_id not in(" + Constants.BT + "," + Constants.SMS + ") and (inetload_time between " + start + " and " + end + ") ";
        String flexiTypeStr = null;
        if (flexiType != 5) {
            flexiTypeStr = "(" + flexiType + ")";
        } else {
            flexiTypeStr = "(1,2,3)";
        }
        if (f_dto.searchWithTransactionId) {
            condition += " and inetload_transaction_id like '" + f_dto.getTransactionId() + "%' ";
        }
        if (f_dto.searchWithRequestId) {
            condition += " and inetload_marker like '" + f_dto.getFlexiRequestId() + "%' ";
        }
        if (f_dto.searchWithPhoneNumber) {
            condition += " and inetload_phn_no like '" + f_dto.getPhoneNumber() + "%' ";
        }

        if (f_dto.searchWithAmount) {
            switch (f_dto.getSign()) {
                case Constants.BALANCE_EQUAL:
                    condition += " and inetload_amount=" + f_dto.getAmount() + " ";
                    break;
                case Constants.BALANCE_SMALLER_THAN:
                    condition += " and inetload_amount<" + f_dto.getAmount() + " ";
                    break;
                case Constants.BALANCE_GREATER_THAN:
                    condition += " and inetload_amount>" + f_dto.getAmount() + " ";
                    break;
                default:
                    break;
            }
        }

        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;
            } else if ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null) {
                condition += " and operator_type_id in(-1";
                if (userPerDTO.REFILL_GP) {
                    condition += "," + Constants.GP;
                }
                if (userPerDTO.REFILL_BL) {
                    condition += "," + Constants.BL;
                }
                if (userPerDTO.REFILL_RB) {
                    condition += "," + Constants.RB;
                }
                if (userPerDTO.REFILL_CC) {
                    condition += "," + Constants.CC;
                }
                if (userPerDTO.REFILL_TT) {
                    condition += "," + Constants.TT;
                }
                if (userPerDTO.REFILL_WR) {
                    condition += "," + Constants.WR;
                }
                if (userPerDTO.REFILL_BT) {
                    condition += "," + Constants.BT;
                }
                if (userPerDTO.REFILL_CARD) {
                    condition += "," + Constants.CARD;
                }
                condition += ") ";
                sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;
            }

        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;

        } else {
            sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id=" + login_dto.getId() + condition;
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ClientDTO cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("inetload_to_user_id"));
                if (f_dto.searchWithFromClientId && !cldto.getClientId().toLowerCase().startsWith(f_dto.getFromClientId())) {
                    continue;
                }
                FlexiDTO dto = new FlexiDTO();
                dto.setFlexiAgentID(rs.getLong("inetload_from_user_id"));
                dto.setFlexiRequestType(rs.getInt("inetload_status"));
                dto.setFlexiRequestTypeName(Constants.INETLOAD_STATUS_STRING[rs.getInt("inetload_status")]);
                dto.setFlexiFrom(cldto.getClientId());
                dto.setFlexiAmount(rs.getDouble("inetload_amount"));
                dto.setSurcharge(rs.getDouble("surcharge"));
                dto.setFlexiRequestId(rs.getString("inetload_marker"));
                dto.setFlexiTypeVal(rs.getInt("inetload_type_id"));
                dto.setFlexiOpType(rs.getInt("operator_type_id"));
                if (dto.getFlexiOpType() == Constants.BT) {
                    dto.setFlexiPhoneNo(rs.getString("inetload_phn_no") + " (" + Constants.ACCOUNT_TYPE[rs.getInt("account_type")] + ")");
                    dto.setFlexiType(Constants.TRANSFER_TYPE[rs.getInt("inetload_type_id")]);
                } else {
                    dto.setFlexiPhoneNo(rs.getString("inetload_phn_no"));
                    dto.setFlexiType(Constants.REFILL_TYPE[rs.getInt("inetload_type_id")]);
                }
                dto.setFlexiRequestTimeVal(rs.getLong("inetload_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                if (rs.getLong("inetload_refill_success_time") > 0) {
                    dto.setFlexiSuccessTimeVal(rs.getLong("inetload_refill_success_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                }
                if (flexiType != 5) {
                    dto.setFlexiTransactionId(rs.getString("inetload_transaction_id"));
                } else {
                    APIDTO apiDTO = APILoader.getInstance().getApiDTOByID(rs.getLong("inetload_gw_id"));
                    if (apiDTO != null) {
                        dto.setFlexiTransactionId(apiDTO.getApiId());
                    } else {
                        dto.setFlexiTransactionId(rs.getString("inetload_transaction_id"));
                    }
                }
                dto.setFlexiMessage(rs.getString("inetload_message"));
                if ((dto.getFlexiTransactionId() == null || dto.getFlexiTransactionId().trim().length() == 0) && dto.getFlexiMessage() != null && dto.getFlexiMessage().trim().length() > 0) {
                    dto.setFlexiTransactionId("Message:");
                }
                dto.setFlexiCardID(rs.getLong("card_id"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getFlexiDTOs: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<OperatorTypeDTO> getRefillSummery(LoginDTO login_dto, FlexiDTO f_dto) {
        ArrayList<OperatorTypeDTO> data = null;
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(f_dto.getStartYear(), f_dto.getStartMonth() - 1, f_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        long end = new GregorianCalendar(f_dto.getEndYear(), f_dto.getEndMonth() - 1, f_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        String condition = " and (inetload_refill_success_time between " + start + " and " + end + ") ";
        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            sql = "select inetload_to_user_id,operator_type_id,sum(inetload_amount) as totalLoadAmount,sum(surcharge) as totalSurchargeAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id in(" + list + ")" + condition + " group by operator_type_id,inetload_to_user_id";
        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select inetload_to_user_id,operator_type_id,sum(inetload_amount) as totalLoadAmount,sum(surcharge) as totalSurchargeAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id in(" + list + ")" + condition + " group by operator_type_id,inetload_to_user_id";

        } else {
            sql = "select inetload_to_user_id,operator_type_id,sum(inetload_amount) as totalLoadAmount,sum(surcharge) as totalSurchargeAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id =" + login_dto.getId() + condition + " group by operator_type_id,inetload_to_user_id";
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            HashMap opratorWiseMap = new HashMap();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ClientDTO cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("inetload_to_user_id"));
                if (f_dto.searchWithFromClientId && !cldto.getClientId().toLowerCase().startsWith(f_dto.getFromClientId())) {
                    continue;
                }
                OperatorTypeDTO optDTO = (OperatorTypeDTO) opratorWiseMap.get(rs.getString("inetload_to_user_id"));
                if (optDTO == null) {
                    optDTO = new OperatorTypeDTO();
                    optDTO.setClientId(ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("inetload_to_user_id")).getClientId());
                    opratorWiseMap.put(rs.getString("inetload_to_user_id"), optDTO);
                }
                int type = rs.getInt("operator_type_id");
                switch (type) {
                    case Constants.GP:
                        optDTO.setGrameenPhone(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.BL:
                        optDTO.setBanglaLink(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.WR:
                        optDTO.setWarid(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.RB:
                        optDTO.setRobi(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.TT:
                        optDTO.setTeleTalk(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.CC:
                        optDTO.setCityCell(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.BT:
                        optDTO.setBalanceTransfer(rs.getDouble("totalLoadAmount"));
                        optDTO.setSurcharge(rs.getDouble("totalSurchargeAmount"));
                        break;
                    case Constants.SMS:
                        optDTO.setSms(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.CARD:
                        optDTO.setCard(rs.getDouble("totalLoadAmount"));
                        break;
                    default:
                        break;
                }
            }
            rs.close();
            data = new ArrayList<OperatorTypeDTO>(opratorWiseMap.values());
        } catch (Exception e) {
            logger.fatal("Exception in getRefillSummery:" + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<OperatorTypeDTO> getRefillSummeryByDate(LoginDTO login_dto, FlexiDTO f_dto) {
        ArrayList<OperatorTypeDTO> data = new ArrayList<OperatorTypeDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(f_dto.getStartYear(), f_dto.getStartMonth() - 1, f_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        long end = new GregorianCalendar(f_dto.getEndYear(), f_dto.getEndMonth() - 1, f_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        String condition = " and (inetload_refill_success_time between " + start + " and " + end + ") ";
        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            sql = "select inetload_to_user_id,FROM_UNIXTIME((inetload_refill_success_time+" + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) + ")/1000,'%Y %D %M') as date,inetload_refill_success_time,operator_type_id,sum(inetload_amount) as totalLoadAmount,sum(surcharge) as totalSurchargeAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id in(" + list + ")" + condition + "  group by operator_type_id,FROM_UNIXTIME((inetload_refill_success_time+" + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) + ")/1000,'%Y %D %M')";
        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select inetload_to_user_id,FROM_UNIXTIME((inetload_refill_success_time+" + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) + ")/1000,'%Y %D %M') as date,inetload_refill_success_time,operator_type_id,sum(inetload_amount) as totalLoadAmount,sum(surcharge) as totalSurchargeAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id in(" + list + ")" + condition + "  group by operator_type_id,FROM_UNIXTIME((inetload_refill_success_time+" + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) + ")/1000,'%Y %D %M')";
        } else {
            sql = "select inetload_to_user_id,FROM_UNIXTIME((inetload_refill_success_time+" + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) + ")/1000,'%Y %D %M') as date,inetload_refill_success_time,operator_type_id,sum(inetload_amount) as totalLoadAmount,sum(surcharge) as totalSurchargeAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id =" + login_dto.getId() + condition + "  group by operator_type_id,FROM_UNIXTIME((inetload_refill_success_time+" + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) + ")/1000,'%Y %D %M')";
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            HashMap opratorWiseMap = new HashMap();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ClientDTO cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("inetload_to_user_id"));
                if (f_dto.searchWithFromClientId && !cldto.getClientId().toLowerCase().startsWith(f_dto.getFromClientId())) {
                    continue;
                }
                OperatorTypeDTO optDTO = (OperatorTypeDTO) opratorWiseMap.get(rs.getString("date"));
                if (optDTO == null) {
                    optDTO = new OperatorTypeDTO();
                    optDTO.setReportDate(rs.getLong("inetload_refill_success_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    opratorWiseMap.put(rs.getString("date"), optDTO);
                }
                int type = rs.getInt("operator_type_id");
                switch (type) {
                    case Constants.GP:
                        optDTO.setGrameenPhone(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.BL:
                        optDTO.setBanglaLink(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.WR:
                        optDTO.setWarid(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.RB:
                        optDTO.setRobi(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.TT:
                        optDTO.setTeleTalk(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.CC:
                        optDTO.setCityCell(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.BT:
                        optDTO.setBalanceTransfer(rs.getDouble("totalLoadAmount"));
                        optDTO.setSurcharge(rs.getDouble("totalSurchargeAmount"));
                        break;
                    case Constants.SMS:
                        optDTO.setSms(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.CARD:
                        optDTO.setCard(rs.getDouble("totalLoadAmount"));
                        break;
                    default:
                        break;
                }
            }
            rs.close();
            data = new ArrayList(opratorWiseMap.values());
        } catch (Exception e) {
            logger.fatal("Exception in getRefillSummeryByDate:", e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<OperatorTypeDTO> getRefillSummeryByAPI(LoginDTO login_dto, FlexiDTO f_dto) {
        ArrayList<OperatorTypeDTO> data = null;
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(f_dto.getStartYear(), f_dto.getStartMonth() - 1, f_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        long end = new GregorianCalendar(f_dto.getEndYear(), f_dto.getEndMonth() - 1, f_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        String condition = " and (inetload_refill_success_time between " + start + " and " + end + ") ";
        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            sql = "select inetload_to_user_id,inetload_gw_id,operator_type_id,sum(inetload_amount) as totalLoadAmount from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUCCESSFUL + " and inetload_to_user_id in(" + list + ")" + condition + " group by operator_type_id,inetload_gw_id";
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            HashMap opratorWiseMap = new HashMap();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ClientDTO cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("inetload_to_user_id"));
                if (f_dto.searchWithFromClientId && !cldto.getClientId().toLowerCase().startsWith(f_dto.getFromClientId())) {
                    continue;
                }
                OperatorTypeDTO optDTO = (OperatorTypeDTO) opratorWiseMap.get(rs.getLong("inetload_gw_id"));
                if (optDTO == null) {
                    optDTO = new OperatorTypeDTO();
                    APIDTO apiDTO = APILoader.getInstance().getApiDTOByID(rs.getLong("inetload_gw_id"));
                    if (apiDTO != null) {
                        optDTO.setClientId(apiDTO.getApiId());
                    } else {
                        continue;
                    }
                    opratorWiseMap.put(rs.getLong("inetload_gw_id"), optDTO);
                }
                int type = rs.getInt("operator_type_id");
                switch (type) {
                    case Constants.GP:
                        optDTO.setGrameenPhone(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.BL:
                        optDTO.setBanglaLink(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.WR:
                        optDTO.setWarid(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.RB:
                        optDTO.setRobi(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.TT:
                        optDTO.setTeleTalk(rs.getDouble("totalLoadAmount"));
                        break;
                    case Constants.CC:
                        optDTO.setCityCell(rs.getDouble("totalLoadAmount"));
                        break;
                    default:
                        break;
                }
            }
            rs.close();
            data = new ArrayList<OperatorTypeDTO>(opratorWiseMap.values());
        } catch (Exception e) {
            logger.fatal("Exception in getRefillSummeryByAPI:" + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    //Added By Anwar
    public ArrayList<FlexiDTO> getBalanceTransferDTOs(LoginDTO login_dto, FlexiDTO f_dto, int flexiType) {
        ArrayList<FlexiDTO> data = new ArrayList<FlexiDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(f_dto.getStartYear(), f_dto.getStartMonth() - 1, f_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        long end = new GregorianCalendar(f_dto.getEndYear(), f_dto.getEndMonth() - 1, f_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        String condition = " and operator_type_id =" + Constants.BT + " and (inetload_time between " + start + " and " + end + ") ";
        String flexiTypeStr = null;
        if (flexiType != 5) {
            flexiTypeStr = "(" + flexiType + ")";
        } else {
            flexiTypeStr = "(1,2,3)";
        }
        if (f_dto.searchWithTransactionId) {
            condition += " and inetload_transaction_id like '" + f_dto.getTransactionId() + "%' ";
        }
        if (f_dto.searchWithRequestId) {
            condition += " and inetload_marker like '" + f_dto.getFlexiRequestId() + "%' ";
        }
        if (f_dto.searchWithPhoneNumber) {
            condition += " and inetload_phn_no like '" + f_dto.getPhoneNumber() + "%' ";
        }

        if (f_dto.searchWithAmount) {
            switch (f_dto.getSign()) {
                case Constants.BALANCE_EQUAL:
                    condition += " and inetload_amount=" + f_dto.getAmount() + " ";
                    break;
                case Constants.BALANCE_SMALLER_THAN:
                    condition += " and inetload_amount<" + f_dto.getAmount() + " ";
                    break;
                case Constants.BALANCE_GREATER_THAN:
                    condition += " and inetload_amount>" + f_dto.getAmount() + " ";
                    break;
                default:
                    break;
            }
        }

        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;
            } else if ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null) {
                condition += " and operator_type_id in(-1";
                if (userPerDTO.REFILL_BT) {
                    condition += "," + Constants.BT;
                }
                condition += ") ";
                sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;
            }

        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;

        } else {
            sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id=" + login_dto.getId() + condition;
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ClientDTO cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("inetload_to_user_id"));
                if (f_dto.searchWithFromClientId && !cldto.getClientId().toLowerCase().startsWith(f_dto.getFromClientId())) {
                    continue;
                }
                FlexiDTO dto = new FlexiDTO();
                dto.setFlexiAgentID(rs.getLong("inetload_from_user_id"));
                dto.setFlexiRequestType(rs.getInt("inetload_status"));
                dto.setFlexiRequestTypeName(Constants.INETLOAD_STATUS_STRING[rs.getInt("inetload_status")]);
                dto.setFlexiFrom(cldto.getClientId());
                dto.setFlexiAmount(rs.getDouble("inetload_amount"));
                dto.setSurcharge(rs.getDouble("surcharge"));
                dto.setFlexiRequestId(rs.getString("inetload_marker"));
                dto.setFlexiTypeVal(rs.getInt("inetload_type_id"));
                dto.setFlexiOpType(rs.getInt("operator_type_id"));
                if (dto.getFlexiOpType() == Constants.BT) {
                    dto.setFlexiPhoneNo(rs.getString("inetload_phn_no") + " (" + Constants.ACCOUNT_TYPE[rs.getInt("account_type")] + ")");
                    dto.setFlexiType(Constants.TRANSFER_TYPE[rs.getInt("inetload_type_id")]);
                } else {
                    dto.setFlexiPhoneNo(rs.getString("inetload_phn_no"));
                    dto.setFlexiType(Constants.REFILL_TYPE[rs.getInt("inetload_type_id")]);
                }
                dto.setFlexiRequestTimeVal(rs.getLong("inetload_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                if (rs.getLong("inetload_refill_success_time") > 0) {
                    dto.setFlexiSuccessTimeVal(rs.getLong("inetload_refill_success_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                }
                if (flexiType != 5) {
                    dto.setFlexiTransactionId(rs.getString("inetload_transaction_id"));
                } else {
                    APIDTO apiDTO = APILoader.getInstance().getApiDTOByID(rs.getLong("inetload_gw_id"));
                    if (apiDTO != null) {
                        dto.setFlexiTransactionId(apiDTO.getApiId());
                    } else {
                        dto.setFlexiTransactionId(rs.getString("inetload_transaction_id"));
                    }
                }
                dto.setFlexiMessage(rs.getString("inetload_message"));
                if ((dto.getFlexiTransactionId() == null || dto.getFlexiTransactionId().trim().length() == 0) && dto.getFlexiMessage() != null && dto.getFlexiMessage().trim().length() > 0) {
                    dto.setFlexiTransactionId("Message:");
                }
                dto.setFlexiCardID(rs.getLong("card_id"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getFlexiDTOs: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
}
