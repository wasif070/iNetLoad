package com.myapp.struts.flexi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;
import org.apache.log4j.Logger;

public class SummeryFlexiloadAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(SummeryFlexiloadAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.REPORT_REFILL_SUMMERY && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int pageNo = 1;
                int list_all = 0;
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                FlexiDTO fdto = new FlexiDTO();
                FlexiForm flexiForm = (FlexiForm) form;
                    if (list_all == 0) {
                        if (flexiForm.getRecordPerPage() > 0) {
                            request.getSession(true).setAttribute(Constants.FLEXI_SUMMERY_RECORD_PER_PAGE, flexiForm.getRecordPerPage());
                        }
                    fdto.setEndDay(flexiForm.getEndDay());
                    fdto.setEndMonth(flexiForm.getEndMonth());
                    fdto.setEndYear(flexiForm.getEndYear());
                    fdto.setStartDay(flexiForm.getStartDay());
                    fdto.setStartMonth(flexiForm.getStartMonth());
                    fdto.setStartYear(flexiForm.getStartYear());
                    fdto.setReportType(flexiForm.getReportType());
                    if (flexiForm.getFromClientId() != null && flexiForm.getFromClientId().trim().length() > 0) {
                        fdto.searchWithFromClientId = true;
                        fdto.setFromClientId(flexiForm.getFromClientId().toLowerCase());
                    }
                } else {
                    if (request.getSession(true).getAttribute(Constants.FLEXI_SUMMERY_RECORD_PER_PAGE) != null) {
                        flexiForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_SUMMERY_RECORD_PER_PAGE).toString()));
                    }
                    String dateParts[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    flexiForm.setStartDay(Integer.parseInt(dateParts[0]));
                    flexiForm.setStartMonth(Integer.parseInt(dateParts[1]));
                    flexiForm.setStartYear(Integer.parseInt(dateParts[2]));
                    flexiForm.setEndDay(flexiForm.getStartDay());
                    flexiForm.setEndMonth(flexiForm.getStartMonth());
                    flexiForm.setEndYear(flexiForm.getStartYear());
                    flexiForm.setReportType(0);
                    fdto.setStartDay(flexiForm.getStartDay());
                    fdto.setStartMonth(flexiForm.getStartMonth());
                    fdto.setStartYear(flexiForm.getStartYear());
                    fdto.setEndDay(flexiForm.getEndDay());
                    fdto.setEndMonth(flexiForm.getEndMonth());
                    fdto.setEndYear(flexiForm.getEndYear());
                    fdto.setReportType(0);
                }

                FlexiTaskScheduler scheduler = new FlexiTaskScheduler();
                flexiForm.setFlexiSummeryList(scheduler.getSummeryRefill(login_dto, fdto));
                if (flexiForm.getFlexiSummeryList() != null && flexiForm.getFlexiSummeryList().size() > 0 && flexiForm.getFlexiSummeryList().size() <= (flexiForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
