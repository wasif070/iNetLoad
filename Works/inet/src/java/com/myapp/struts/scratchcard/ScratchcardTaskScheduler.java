package com.myapp.struts.scratchcard;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class ScratchcardTaskScheduler {

    public ScratchcardTaskScheduler() {
    }

    public MyAppError addScratchCardType(ScratchcardDTO sdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.addScratchCardType(sdto);
    }

    public MyAppError editScratchCardType(ScratchcardDTO cdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.editScratchCardType(cdto);
    }
    
    public MyAppError deleteScratchCardTypes(String list) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.deleteScratchCardTypes(list);
    }
    
    public MyAppError activateScratchCardTypes(String list) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.activateScratchCardTypes(list);
    }    

    public MyAppError blockScratchCardTypes(String list) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.blockScratchCardTypes(list);
    }

    public ArrayList<ScratchcardDTO> getScratchCardTypeDTOsWithSearchParam(ScratchcardDTO scdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        ArrayList<ScratchcardDTO> list = ScratchcardTypeLoader.getInstance().getScratchcardTypeDTOList();
        if (list != null) {
            return scratchcardDAO.getScratchCardTypeDTOsWithSearchParam((ArrayList<ScratchcardDTO>) list.clone(), scdto);
        }
        return null;
    }

    public ArrayList<ScratchcardDTO> getScratchCardTypeDTOsSorted() {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        ArrayList<ScratchcardDTO> list = ScratchcardTypeLoader.getInstance().getScratchcardTypeDTOList();
        if (list != null) {
            return scratchcardDAO.getScratchCardTypeDTOsSorted((ArrayList<ScratchcardDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<ScratchcardDTO> getScratchCardTypeDTOs() {
        return ScratchcardTypeLoader.getInstance().getScratchcardTypeDTOList();
    }

    public ScratchcardDTO getScratchCardTypeDTO(long id) {
        return ScratchcardTypeLoader.getInstance().getScratchcardTypeDTOByID(id);
    }    
    
    public MyAppError addScratchCard(ScratchcardDTO scdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.addScratchCard(scdto);
    }

    public MyAppError editScratchCard(ScratchcardDTO scdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.editScratchCard(scdto);
    }
    
    public MyAppError deleteScratchCards(String list) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.deleteScratchCards(list);
    }
    
    public MyAppError activateScratchCards(String list) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.activateScratchCards(list);
    }    

    public MyAppError blockScratchCards(String list) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.blockScratchCards(list);
    }

    public ArrayList<ScratchcardDTO> getScratchCardDTOsWithSearchParam(ScratchcardDTO scdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        ArrayList<ScratchcardDTO> list = ScratchcardLoader.getInstance().getScratchcardDTOList();
        if (list != null) {
            return scratchcardDAO.getScratchCardDTOsWithSearchParam((ArrayList<ScratchcardDTO>) list.clone(), scdto);
        }
        return null;
    }

    public ArrayList<ScratchcardDTO> getScratchCardDTOsSorted() {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        ArrayList<ScratchcardDTO> list = ScratchcardLoader.getInstance().getScratchcardDTOList();
        if (list != null) {
            return scratchcardDAO.getScratchCardDTOsSorted((ArrayList<ScratchcardDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<ScratchcardDTO> getScratchCardDTOs() {
        return ScratchcardLoader.getInstance().getScratchcardDTOList();
    }

    public ScratchcardDTO getScratchCardDTO(long id) {
        return ScratchcardLoader.getInstance().getScratchcardDTOByID(id);
    }     
    
    public MyAppError getScratchCard(LoginDTO login_dto, ScratchcardDTO scdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.getScratchCard(login_dto, scdto);
    } 
    
    public ArrayList<ScratchcardDTO> getMyScratchCardDTOs(LoginDTO login_dto, ScratchcardDTO scdto) {
        ScratchcardDAO scratchcardDAO = new ScratchcardDAO();
        return scratchcardDAO.getMyScratchCardDTOs(login_dto, scdto);
    }     
}
