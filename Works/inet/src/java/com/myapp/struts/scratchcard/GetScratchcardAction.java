package com.myapp.struts.scratchcard;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetScratchcardAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.SC_EDIT)) {
                ScratchcardForm formBean = (ScratchcardForm) form;
                long id = Long.parseLong(request.getParameter("id"));
                ScratchcardDTO dto = new ScratchcardDTO();
                ScratchcardTaskScheduler scheduler = new ScratchcardTaskScheduler();
                dto = scheduler.getScratchCardDTO(id);

                if (dto != null) {
                    formBean.setCompanyName(dto.getCompanyName());
                    formBean.setRefillType(dto.getRefillType());
                    formBean.setRefillAmount(dto.getRefillAmount());
                    formBean.setSerialNo(dto.getSerialNo());
                    formBean.setPin(dto.getPin());
                    request.setAttribute("refType", dto.getRefillType());
                    request.setAttribute("refAmount", dto.getRefillAmount());
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
