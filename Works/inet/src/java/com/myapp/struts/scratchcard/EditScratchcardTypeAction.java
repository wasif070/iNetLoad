package com.myapp.struts.scratchcard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

import com.myapp.struts.util.MyAppError;

public class EditScratchcardTypeAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        ScratchcardForm formBean = (ScratchcardForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.SC_EDIT)) {
                request.setAttribute(mapping.getAttribute(), formBean);

                ScratchcardDTO dto = new ScratchcardDTO();
                ScratchcardTaskScheduler scheduler = new ScratchcardTaskScheduler();

                dto.setCompanyName(formBean.getCompanyName());
                dto.setSupportedRefillTypes(formBean.getSupportedRefillTypes());
                dto.setSupportedRefillAmounts(formBean.getSupportedRefillAmounts());

                MyAppError error = new MyAppError();
                long id = Long.parseLong(request.getParameter("id"));
                dto.setId(id);
                error = scheduler.editScratchCardType(dto);

                if (error.getErrorType() == MyAppError.NoError) {
                    formBean.setMessage(false, "Scratch card type is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + request.getParameter("searchLink") + "&action=" + Constants.EDIT, true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return mapping.findForward(target);
    }
}
