package com.myapp.struts.scratchcard;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListScratchcardAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(ListScratchcardAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        PermissionDTO userPerDTO = null;
        if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.SC) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ScratchcardForm scratchcardForm = (ScratchcardForm) form;
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            ScratchcardTaskScheduler scheduler = new ScratchcardTaskScheduler();

            int action = 0;
            if (request.getParameter("action") != null) {
                action = Integer.parseInt(request.getParameter("action"));
                String idList = "";
                if (action > Constants.UPDATE) {
                    if (scratchcardForm.getSelectedIDs() != null) {
                        int length = scratchcardForm.getSelectedIDs().length;
                        long[] idListArray = scratchcardForm.getSelectedIDs();
                        if (length > 0) {
                            idList += idListArray[0];
                            for (int i = 1; i < length; i++) {
                                idList += "," + idListArray[i];
                            }
                            MyAppError error = new MyAppError();
                            switch (action) {
                                case Constants.ACTIVATION:
                                    if (userPerDTO != null && !userPerDTO.SC_EDIT) {
                                        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                        return (mapping.findForward("index"));
                                    }
                                    error = scheduler.activateScratchCards(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        scratchcardForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        scratchcardForm.setMessage(false, "Scratch cards are activated successfully.");
                                    }
                                    request.setAttribute(Constants.SCRATCH_CARD_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, scratchcardForm.getMessage());

                                    break;
                                case Constants.BLOCK:
                                    if (userPerDTO != null && !userPerDTO.SC_EDIT) {
                                        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                        return (mapping.findForward("index"));
                                    }
                                    error = scheduler.blockScratchCards(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        scratchcardForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        scratchcardForm.setMessage(false, "Scratch cards are blocked successfully.");
                                    }
                                    request.setAttribute(Constants.SCRATCH_CARD_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, scratchcardForm.getMessage());
                                    break;
                                case Constants.DELETE:
                                    if (userPerDTO != null && !userPerDTO.SC_DELETE) {
                                        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                        return (mapping.findForward("index"));
                                    }
                                    error = scheduler.deleteScratchCards(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        scratchcardForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        scratchcardForm.setMessage(false, "Scratch cards are deleted successfully.");
                                    }
                                    request.getSession(true).setAttribute(Constants.MESSAGE, scratchcardForm.getMessage());
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        scratchcardForm.setMessage(true, "No scratch card is selected.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, scratchcardForm.getMessage());
                    }
                }
            }

            if (list_all == 0) {
                if (scratchcardForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.CONTACT_RECORD_PER_PAGE, scratchcardForm.getRecordPerPage());
                }
                ScratchcardDTO scratchcardDTO = new ScratchcardDTO();
                if (scratchcardForm.getCompanyNamePar() != null && scratchcardForm.getCompanyNamePar().trim().length() > 0) {
                    scratchcardDTO.searchWithCompanyName = true;
                    scratchcardDTO.setCompanyName(scratchcardForm.getCompanyNamePar().toLowerCase());
                }
                if (scratchcardForm.getRefillTypePar() != null && scratchcardForm.getRefillTypePar().trim().length() > 0) {
                    scratchcardDTO.searchWithRefillType = true;
                    scratchcardDTO.setRefillType(scratchcardForm.getRefillTypePar().toLowerCase());
                }
                if (scratchcardForm.getRefillAmountPar() != null && scratchcardForm.getRefillAmountPar().trim().length() > 0) {
                    scratchcardDTO.searchWithRefillAmount = true;
                    scratchcardDTO.setRefillAmount(scratchcardForm.getRefillAmountPar().toLowerCase());
                }
                if (scratchcardForm.getSerialNoPar() != null && scratchcardForm.getSerialNoPar().trim().length() > 0) {
                    scratchcardDTO.searchWithSerialNo = true;
                    scratchcardDTO.setSerialNo(scratchcardForm.getSerialNoPar().toLowerCase());
                }
                scratchcardForm.setScratchCardList(scheduler.getScratchCardDTOsWithSearchParam(scratchcardDTO));
            } else if (list_all == 2) {
                if (action == Constants.ADD) {
                    scratchcardForm.setMessage(false, "Scratch card is added successfully!!!");
                } else if (action == Constants.EDIT) {
                    scratchcardForm.setMessage(false, "Scratch card is updated successfully.");
                }
                if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE) != null) {
                    scratchcardForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE).toString()));
                }
                request.getSession(true).setAttribute(Constants.MESSAGE, scratchcardForm.getMessage());
                scratchcardForm.setScratchCardList(scheduler.getScratchCardDTOsSorted());
            } else {
                if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE) != null) {
                    scratchcardForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE).toString()));
                }
                scratchcardForm.setScratchCardList(scheduler.getScratchCardDTOs());
            }
            scratchcardForm.setSelectedIDs(null);
            if (scratchcardForm.getScratchCardList() != null && scratchcardForm.getScratchCardList().size() > 0 && scratchcardForm.getScratchCardList().size() <= (scratchcardForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
