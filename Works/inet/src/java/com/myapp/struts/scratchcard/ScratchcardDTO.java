package com.myapp.struts.scratchcard;

public class ScratchcardDTO {

    public boolean searchWithCompanyName = false;
    public boolean searchWithRefillType = false;
    public boolean searchWithRefillAmount = false;
    public boolean searchWithSerialNo = false;
    private int doValidation;
    private int pageNo;
    private int recordPerPage;
    private int statusID;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;    
    private long id;
    private long usedTime;
    private String statusName;
    private String companyName;
    private String serialNo;
    private String pin;
    private String refillType;
    private String refillAmount;
    private String supportedRefillTypes;
    private String supportedRefillAmounts;
    private String companyNamePar;
    private String usedById;

    public ScratchcardDTO() {
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public long getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(long usedTime) {
        this.usedTime = usedTime;
    } 
    
    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }    
    
    public String getUsedById() {
        return usedById;
    }

    public void setUsedById(String usedById) {
        this.usedById = usedById;
    }    

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSupportedRefillTypes() {
        return supportedRefillTypes;
    }

    public void setSupportedRefillTypes(String supportedRefillTypes) {
        this.supportedRefillTypes = supportedRefillTypes;
    }

    public String getRefillType() {
        return refillType;
    }

    public void setRefillType(String refillType) {
        this.refillType = refillType;
    }

    public String getRefillAmount() {
        return refillAmount;
    }

    public void setRefillAmount(String refillAmount) {
        this.refillAmount = refillAmount;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSupportedRefillAmounts() {
        return supportedRefillAmounts;
    }

    public void setSupportedRefillAmounts(String supportedRefillAmounts) {
        this.supportedRefillAmounts = supportedRefillAmounts;
    }

    public String getCompanyNamePar() {
        return companyNamePar;
    }

    public void setCompanyNamePar(String companyNamePar) {
        this.companyNamePar = companyNamePar;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidation() {
        return doValidation;
    }

    public void setDoValidation(int doValidation) {
        this.doValidation = doValidation;
    }
}
