package com.myapp.struts.scratchcard;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class ScratchcardTypeLoader {

    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, ScratchcardDTO> scratchcardTypeDTOById;
    private HashMap<String, ScratchcardDTO> scratchcardTypeDTOByCompany;
    private ArrayList<ScratchcardDTO> scratchcardTypeDTOList;
    static ScratchcardTypeLoader scratchcardTypeLoader = null;
    static Logger logger = Logger.getLogger(ScratchcardTypeLoader.class.getName());

    public ScratchcardTypeLoader() {
        forceReload();
    }

    public static ScratchcardTypeLoader getInstance() {
        if (scratchcardTypeLoader == null) {
            createScratchardTypeLoader();
        }
        return scratchcardTypeLoader;
    }

    private synchronized static void createScratchardTypeLoader() {
        if (scratchcardTypeLoader == null) {
            scratchcardTypeLoader = new ScratchcardTypeLoader();
        }
    }

    private void reload() {
        scratchcardTypeDTOList = new ArrayList<ScratchcardDTO>();
        scratchcardTypeDTOById = new HashMap<Long, ScratchcardDTO>();
        scratchcardTypeDTOByCompany = new HashMap<String, ScratchcardDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from scratchcard_type;";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                ScratchcardDTO dto = new ScratchcardDTO();
                dto.setId(rs.getLong("id"));
                dto.setCompanyName(rs.getString("company_name"));
                dto.setSupportedRefillTypes(rs.getString("supported_refill_types"));
                dto.setSupportedRefillAmounts(rs.getString("supported_refill_amounts"));
                dto.setStatusID(rs.getInt("status_id"));
                dto.setStatusName(Constants.LIVE_STATUS_STRING[rs.getInt("status_id")]);
                scratchcardTypeDTOById.put(dto.getId(), dto);
                scratchcardTypeDTOByCompany.put(dto.getCompanyName(),dto);
                scratchcardTypeDTOList.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getScratchcardTypeDTOList() {
        checkForReload();
        return scratchcardTypeDTOList;
    }

    public synchronized ScratchcardDTO getScratchcardTypeDTOByID(long id) {
        checkForReload();
        return scratchcardTypeDTOById.get(id);
    }
    
    public synchronized ScratchcardDTO getScratchcardTypeDTOByCompany(String company) {
        checkForReload();
        return scratchcardTypeDTOByCompany.get(company);
    }    
}
