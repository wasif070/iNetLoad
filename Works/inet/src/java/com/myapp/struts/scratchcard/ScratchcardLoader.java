package com.myapp.struts.scratchcard;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class ScratchcardLoader {

    private static long LOADING_INTERVAL = 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, ScratchcardDTO> scratchcardDTOById;
    private ArrayList<ScratchcardDTO> scratchcardDTOList;
    static ScratchcardLoader scratchcardLoader = null;
    static Logger logger = Logger.getLogger(ScratchcardLoader.class.getName());

    public ScratchcardLoader() {
        forceReload();
    }

    public static ScratchcardLoader getInstance() {
        if (scratchcardLoader == null) {
            createScratchardLoader();
        }
        return scratchcardLoader;
    }

    private synchronized static void createScratchardLoader() {
        if (scratchcardLoader == null) {
            scratchcardLoader = new ScratchcardLoader();
        }
    }

    private void reload() {
        scratchcardDTOList = new ArrayList<ScratchcardDTO>();
        scratchcardDTOById = new HashMap<Long, ScratchcardDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from scratchcard;";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                ScratchcardDTO dto = new ScratchcardDTO();
                dto.setId(rs.getLong("id"));
                dto.setCompanyName(rs.getString("company_name"));
                dto.setRefillType(rs.getString("refill_type"));
                dto.setRefillAmount(rs.getString("refill_amount"));
                dto.setSerialNo(rs.getString("serial_no"));
                dto.setPin(rs.getString("pin"));
                dto.setStatusID(rs.getInt("status_id"));
                dto.setUsedTime(rs.getLong("used_time"));
                ClientDTO clDTO = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("used_by_id"));
                if(clDTO != null)
                {    
                  dto.setUsedById(clDTO.getClientId());
                }
                else
                {
                  dto.setUsedById(" ");  
                }    
                if(dto.getStatusID()!=-1)
                {
                    dto.setStatusName(Constants.LIVE_STATUS_STRING[rs.getInt("status_id")]);
                    scratchcardDTOById.put(dto.getId(), dto);                  
                    scratchcardDTOList.add(dto);                   
                }
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getScratchcardDTOList() {
        checkForReload();
        return scratchcardDTOList;
    }

    public synchronized ScratchcardDTO getScratchcardDTOByID(long id) {
        checkForReload();
        return scratchcardDTOById.get(id);
    }    
}
