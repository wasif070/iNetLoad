package com.myapp.struts.scratchcard;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.MyAppError;
import java.sql.Statement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class ScratchcardDAO {

    static Logger logger = Logger.getLogger(ScratchcardDAO.class.getName());

    public ScratchcardDAO() {
    }

    public ArrayList<ScratchcardDTO> getScratchCardTypeDTOsWithSearchParam(ArrayList<ScratchcardDTO> list, ScratchcardDTO scdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ScratchcardDTO dto = (ScratchcardDTO) i.next();
                if (scdto.searchWithCompanyName && !dto.getCompanyName().toLowerCase().startsWith(scdto.getCompanyName())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<ScratchcardDTO> getScratchCardTypeDTOsSorted(ArrayList<ScratchcardDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ScratchcardDTO dto1 = (ScratchcardDTO) o1;
                    ScratchcardDTO dto2 = (ScratchcardDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addScratchCardType(ScratchcardDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select company_name from scratchcard_type where company_name=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, dto.getCompanyName().trim());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Company Name.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into scratchcard_type(company_name,supported_refill_types,supported_refill_amounts) values(?,?,?);";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, dto.getCompanyName().trim());
            ps2.setString(2, dto.getSupportedRefillTypes());
            ps2.setString(3, dto.getSupportedRefillAmounts());
            ps2.executeUpdate();
            ScratchcardTypeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding scratch card type request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editScratchCardType(ScratchcardDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select company_name from scratchcard_type where company_name=? and id!=" + dto.getId();
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, dto.getCompanyName().trim());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate scratch card type.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update scratchcard_type set company_name=?, supported_refill_types=?, supported_refill_amounts=? where id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, dto.getCompanyName().trim());
            ps2.setString(2, dto.getSupportedRefillTypes());
            ps2.setString(3, dto.getSupportedRefillAmounts());
            ps2.setLong(4, dto.getId());
            ps2.executeUpdate();
            ScratchcardTypeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing scratch card type request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateScratchCardTypes(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update scratchcard_type set status_id=" + Constants.USER_STATUS_ACTIVE + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ScratchcardTypeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while activating scratch card type: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockScratchCardTypes(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update scratchcard_type set status_id=" + Constants.USER_STATUS_BLOCK + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ScratchcardTypeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blocking scratch card type: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteScratchCardTypes(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from scratchcard_type where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ScratchcardTypeLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting scratch card type: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList<ScratchcardDTO> getScratchCardDTOsWithSearchParam(ArrayList<ScratchcardDTO> list, ScratchcardDTO scdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ScratchcardDTO dto = (ScratchcardDTO) i.next();
                if ((scdto.searchWithCompanyName && !dto.getCompanyName().toLowerCase().startsWith(scdto.getCompanyName()))
                        || (scdto.searchWithRefillAmount && !dto.getRefillAmount().toLowerCase().startsWith(scdto.getRefillAmount()))
                        || (scdto.searchWithRefillType && !dto.getRefillType().toLowerCase().startsWith(scdto.getRefillType()))
                        || (scdto.searchWithSerialNo && !dto.getSerialNo().toLowerCase().startsWith(scdto.getSerialNo()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<ScratchcardDTO> getScratchCardDTOsSorted(ArrayList<ScratchcardDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ScratchcardDTO dto1 = (ScratchcardDTO) o1;
                    ScratchcardDTO dto2 = (ScratchcardDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addScratchCard(ScratchcardDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select company_name from scratchcard where company_name=? and refill_type=? and refill_amount=? and serial_no=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, dto.getCompanyName().trim());
            ps1.setString(2, dto.getRefillType().trim());
            ps1.setString(3, dto.getRefillAmount().trim());
            ps1.setString(4, dto.getSerialNo().trim());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Scratch Card.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into scratchcard(company_name,refill_type,refill_amount,serial_no,pin) values(?,?,?,?,?);";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, dto.getCompanyName().trim());
            ps2.setString(2, dto.getRefillType());
            ps2.setString(3, dto.getRefillAmount());
            ps2.setString(4, dto.getSerialNo());
            ps2.setString(5, dto.getPin());
            ps2.executeUpdate();
            ScratchcardLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding scratch card request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editScratchCard(ScratchcardDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select company_name from scratchcard where company_name=? and refill_type=? and refill_amount=? and serial_no=? and id!=" + dto.getId();
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, dto.getCompanyName().trim());
            ps1.setString(2, dto.getRefillType().trim());
            ps1.setString(3, dto.getRefillAmount().trim());
            ps1.setString(4, dto.getSerialNo().trim());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate scratch card type.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update scratchcard set company_name=?, refill_type=?, refill_amount=?, serial_no=?, pin=?  where id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, dto.getCompanyName().trim());
            ps2.setString(2, dto.getRefillType().trim());
            ps2.setString(3, dto.getRefillAmount().trim());
            ps2.setString(4, dto.getSerialNo());
            ps2.setString(5, dto.getPin());
            ps2.setLong(6, dto.getId());
            ps2.executeUpdate();
            ScratchcardLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing scratch card request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateScratchCards(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update scratchcard set status_id=" + Constants.USER_STATUS_ACTIVE + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ScratchcardLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while activating scratch card: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockScratchCards(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update scratchcard set status_id=" + Constants.USER_STATUS_BLOCK + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ScratchcardLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blocking scratch card: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteScratchCards(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update scratchcard set status_id=-1 where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ScratchcardLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting scratch card: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError getScratchCard(LoginDTO login_dto, ScratchcardDTO scdto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        String marker = System.currentTimeMillis() + ":" + login_dto.getId();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String insertSQL = "insert into inetload_fdr(operator_type_id,inetload_phn_no,"
                    + "inetload_from_user_id,inetload_from_client_type_id,inetload_to_user_id,inetload_to_client_type_id,"
                    + "inetload_type_id,inetload_amount,inetload_time,inetload_marker,inetload_status,inetload_transaction_id) values";
            int fromTypeID = Constants.CLIENT_TYPE_AGENT;
            int toTypeID = fromTypeID;
            long fromID = login_dto.getId();
            long toID = fromID;
            double cardAmount =  Double.parseDouble(scdto.getRefillAmount());
            ClientDTO clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getId());
            if ((clDTO.getClientCredit() - cardAmount) < clDTO.getClientDeposit()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Your Credit Limit is Over.");
                return error;
            } 
            String transactionID = scdto.getCompanyName() + "-" + scdto.getRefillType() + "-" + scdto.getRefillAmount() + "-" + marker;
            insertSQL += "(" + Constants.CARD + ",'Scratch Card'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ","
                    + Constants.REFILL_TYPE_SCRATCH_CARD + "," + cardAmount + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING 
                    + ",'" + transactionID + "')";

            clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getClientParentId());
            while (clDTO != null) {
                if (clDTO.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Your parent is not active.");
                    return error;
                }
                if (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && clDTO.getClientDeposit() > 0 && (clDTO.getClientCredit() - cardAmount) < clDTO.getClientDeposit()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Reseller Credit Limit is Over.");
                    return error;
                }
                else if(clDTO.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && (clDTO.getClientCredit() - cardAmount) < clDTO.getClientDeposit())
                {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Reseller Credit Limit is Over.");
                    return error;                    
                }
                
                toID = clDTO.getId();
                toTypeID = clDTO.getClientTypeId();
                insertSQL += ",(" + Constants.CARD + ",'Scratch Card'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ","
                        + Constants.REFILL_TYPE_SCRATCH_CARD + "," + cardAmount + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING 
                        + ",'" + transactionID + "')";
                clDTO = ClientLoader.getInstance().getClientDTOByID(clDTO.getParentId());
            }
            stmt.execute(insertSQL);
        } catch (Exception ex) {
            logger.fatal("Error while getScratchCard request: ", ex);
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error is Occured.");
            return error;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public ArrayList<ScratchcardDTO> getMyScratchCardDTOs(LoginDTO login_dto, ScratchcardDTO scdto) {    
        ArrayList<ScratchcardDTO> data = new ArrayList<ScratchcardDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(scdto.getStartYear(), scdto.getStartMonth() - 1, scdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        long end = new GregorianCalendar(scdto.getEndYear(), scdto.getEndMonth() - 1, scdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        String condition = " and (used_time between " + start + " and " + end + ") ";
        if (scdto.searchWithCompanyName) {
            condition += " and company_name like '" + scdto.getCompanyName() + "%' ";
        }
        if (scdto.searchWithRefillType) {
            condition += " and refill_type like '" + scdto.getRefillType() + "%' ";
        }
        if (scdto.searchWithRefillAmount) {
            condition += " and refill_amount like '" + scdto.getRefillAmount() + "%' ";
        }  
        if (scdto.searchWithSerialNo) {
            condition += " and serial_no like '" + scdto.getSerialNo() + "%' ";
        }    
        String sql = "select * from scratchcard where status_id = " + Constants.USER_STATUS_BLOCK + " and used_by_id = " + login_dto.getId() + condition;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ScratchcardDTO dto = new ScratchcardDTO();
                dto.setId(rs.getLong("id"));
                dto.setCompanyName(rs.getString("company_name"));
                dto.setRefillType(rs.getString("refill_type"));
                dto.setRefillAmount(rs.getString("refill_amount"));
                dto.setSerialNo(rs.getString("serial_no"));
                dto.setPin(rs.getString("pin"));
                dto.setStatusID(rs.getInt("status_id"));
                dto.setUsedTime(rs.getLong("used_time"));               
                data.add(dto);
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getMyScratchCardDTOs: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
    
    public long getLastBuyTime(LoginDTO login_dto, long currentTime) {
        long lastBuylTime = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select inetload_time,inetload_refill_success_time from inetload_fdr where operator_type_id = " + Constants.CARD + " and inetload_time > " + (currentTime - (60 * 1000)) + " and inetload_to_user_id=" + login_dto.getId() + " and inetload_status!=" + Constants.INETLOAD_STATUS_REJECTED;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                lastBuylTime = rs.getLong("inetload_time");
                if (rs.getLong("inetload_refill_success_time") > 0) {
                    lastBuylTime = rs.getLong("inetload_refill_success_time");
                }
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getLastBuyTime: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return lastBuylTime;
    }    
}
