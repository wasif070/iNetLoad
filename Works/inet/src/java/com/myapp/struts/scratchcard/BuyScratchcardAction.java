package com.myapp.struts.scratchcard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;

public class BuyScratchcardAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);

        if (login_dto != null && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ScratchcardForm formBean = (ScratchcardForm) form;
            request.setAttribute(mapping.getAttribute(), formBean);
            ScratchcardDAO scDAO = new ScratchcardDAO();
            long curTime = System.currentTimeMillis();
            long lastRefillTime = scDAO.getLastBuyTime(login_dto, curTime);
            if (lastRefillTime > 0) {
                long refillInterval = curTime - lastRefillTime;
                if (refillInterval < (60*1000)) {
                    target = "failure";
                    int minute = (int) ((((60*1000) - refillInterval) / 1000) / 60);
                    int sec = (int) ((((60*1000) - refillInterval) / 1000) % 60);
                    formBean.setMessage(true, "You have already sent a scratch card buy request.<br>Please wait " + minute + " minutes " + sec + " seconds");
                    return mapping.findForward(target);
                }
            }
            ScratchcardDTO dto = new ScratchcardDTO();
            ScratchcardTaskScheduler scheduler = new ScratchcardTaskScheduler();
            dto.setCompanyName(formBean.getCompanyName());
            dto.setRefillType(formBean.getRefillType());
            dto.setRefillAmount(formBean.getRefillAmount());
            MyAppError error = scheduler.getScratchCard(login_dto, dto);
            if (error.getErrorType() > MyAppError.NoError) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                request.getSession(true).setAttribute(Constants.SCRATCH_CARD_SUCCESS_DTO, dto);
            }
        } else {
            target = "index";
        }
        return mapping.findForward(target);
    }
}
