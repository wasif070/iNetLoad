package com.myapp.struts.scratchcard;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.Utils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class MyScratchcardsAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(MyScratchcardsAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ScratchcardForm scratchcardForm = (ScratchcardForm) form;
            ScratchcardDTO scratchcardDTO = new ScratchcardDTO();
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            ScratchcardTaskScheduler scheduler = new ScratchcardTaskScheduler();

            if (list_all == 0) {
                if (scratchcardForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.CONTACT_RECORD_PER_PAGE, scratchcardForm.getRecordPerPage());
                }

                if (scratchcardForm.getCompanyNamePar() != null && scratchcardForm.getCompanyNamePar().trim().length() > 0) {
                    scratchcardDTO.searchWithCompanyName = true;
                    scratchcardDTO.setCompanyName(scratchcardForm.getCompanyNamePar().toLowerCase());
                }
                if (scratchcardForm.getRefillTypePar() != null && scratchcardForm.getRefillTypePar().trim().length() > 0) {
                    scratchcardDTO.searchWithRefillType = true;
                    scratchcardDTO.setRefillType(scratchcardForm.getRefillTypePar().toLowerCase());
                }
                if (scratchcardForm.getRefillAmountPar() != null && scratchcardForm.getRefillAmountPar().trim().length() > 0) {
                    scratchcardDTO.searchWithRefillAmount = true;
                    scratchcardDTO.setRefillAmount(scratchcardForm.getRefillAmountPar().toLowerCase());
                }
                if (scratchcardForm.getSerialNoPar() != null && scratchcardForm.getSerialNoPar().trim().length() > 0) {
                    scratchcardDTO.searchWithSerialNo = true;
                    scratchcardDTO.setSerialNo(scratchcardForm.getSerialNoPar().toLowerCase());
                }
                scratchcardDTO.setStartDay(scratchcardForm.getStartDay());
                scratchcardDTO.setStartMonth(scratchcardForm.getStartMonth());
                scratchcardDTO.setStartYear(scratchcardForm.getStartYear());
                scratchcardDTO.setEndDay(scratchcardForm.getEndDay());
                scratchcardDTO.setEndMonth(scratchcardForm.getEndMonth());
                scratchcardDTO.setEndYear(scratchcardForm.getEndYear());
            } else {
                if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE) != null) {
                    scratchcardForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE).toString()));
                }                
                String dateParts[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                String dateParts1[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) - (24 * 60 * 60 * 1000));
                scratchcardForm.setStartDay(Integer.parseInt(dateParts1[0]));
                scratchcardForm.setStartMonth(Integer.parseInt(dateParts1[1]));
                scratchcardForm.setStartYear(Integer.parseInt(dateParts1[2]));
                scratchcardForm.setEndDay(Integer.parseInt(dateParts[0]));
                scratchcardForm.setEndMonth(Integer.parseInt(dateParts[1]));
                scratchcardForm.setEndYear(Integer.parseInt(dateParts[2]));
                scratchcardDTO.setStartDay(scratchcardForm.getStartDay());
                scratchcardDTO.setStartMonth(scratchcardForm.getStartMonth());
                scratchcardDTO.setStartYear(scratchcardForm.getStartYear());
                scratchcardDTO.setEndDay(scratchcardForm.getEndDay());
                scratchcardDTO.setEndMonth(scratchcardForm.getEndMonth());
                scratchcardDTO.setEndYear(scratchcardForm.getEndYear());          
            }
            scratchcardForm.setScratchCardList(scheduler.getMyScratchCardDTOs(login_dto, scratchcardDTO));
            if (scratchcardForm.getScratchCardList() != null && scratchcardForm.getScratchCardList().size() > 0 && scratchcardForm.getScratchCardList().size() <= (scratchcardForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
