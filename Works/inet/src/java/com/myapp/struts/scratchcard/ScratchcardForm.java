package com.myapp.struts.scratchcard;

import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class ScratchcardForm extends org.apache.struts.action.ActionForm {

    private int doValidate;
    private int type;
    private int pageNo;
    private int recordPerPage;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;    
    private long id;
    private long usedTime;
    private String companyName;
    private String serialNo;
    private String pin;
    private String refillType;
    private String refillAmount;
    private String supportedRefillTypes;
    private String supportedRefillAmounts;
    private String companyNamePar;
    private String refillAmountPar;
    private String refillTypePar;
    private String serialNoPar;
    private String message;
    private String usedById;
    private ArrayList scratchCardList;
    private ArrayList scratchCardTypeList;
    private long[] selectedIDs;
    static Logger logger = Logger.getLogger(ScratchcardForm.class.getName());

    public ScratchcardForm() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public long getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(long usedTime) {
        this.usedTime = usedTime;
    }   
    
    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }    
    
    public String getUsedById() {
        return usedById;
    }

    public void setUsedById(String usedById) {
        this.usedById = usedById;
    }      

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSupportedRefillTypes() {
        return supportedRefillTypes;
    }

    public void setSupportedRefillTypes(String supportedRefillTypes) {
        this.supportedRefillTypes = supportedRefillTypes;
    }

    public String getRefillType() {
        return refillType;
    }

    public void setRefillType(String refillType) {
        this.refillType = refillType;
    }

    public String getRefillAmount() {
        return refillAmount;
    }

    public void setRefillAmount(String refillAmount) {
        this.refillAmount = refillAmount;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSupportedRefillAmounts() {
        return supportedRefillAmounts;
    }

    public void setSupportedRefillAmounts(String supportedRefillAmounts) {
        this.supportedRefillAmounts = supportedRefillAmounts;
    }

    public String getCompanyNamePar() {
        return companyNamePar;
    }

    public void setCompanyNamePar(String companyNamePar) {
        this.companyNamePar = companyNamePar;
    }

    public String getRefillAmountPar() {
        return refillAmountPar;
    }

    public void setRefillAmountPar(String refillAmountPar) {
        this.refillAmountPar = refillAmountPar;
    }

    public String getRefillTypePar() {
        return refillTypePar;
    }

    public void setRefillTypePar(String refillTypePar) {
        this.refillTypePar = refillTypePar;
    }

    public String getSerialNoPar() {
        return serialNoPar;
    }

    public void setSerialNoPar(String serialNoPar) {
        this.serialNoPar = serialNoPar;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public ArrayList getScratchCardList() {
        return scratchCardList;
    }

    public void setScratchCardList(ArrayList scratchCardList) {
        this.scratchCardList = scratchCardList;
    }

    public ArrayList getScratchCardTypeList() {
        return scratchCardTypeList;
    }

    public void setScratchCardTypeList(ArrayList scratchCardTypeList) {
        this.scratchCardTypeList = scratchCardTypeList;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getType() == 0) {
                if (getCompanyName() == null || getCompanyName().length() < 1) {
                    errors.add("companyName", new ActionMessage("errors.company_name.required"));
                }
                if (getSerialNo() == null || getSerialNo().length() < 1) {
                    errors.add("serialNo", new ActionMessage("errors.serial_no.required"));
                }
                if (getPin() == null || getPin().length() < 1) {
                    errors.add("pin", new ActionMessage("errors.pin.required"));
                }
            } else if(getType() == 1){
                int commaInSupportedRefillTypes = 0;
                int commaInSupportedRefillAmounts = 0;
                if (getCompanyName() == null || getCompanyName().length() < 1) {
                    errors.add("companyName", new ActionMessage("errors.company_name.required"));
                }
                if (getSupportedRefillTypes() == null || getSupportedRefillTypes().length() < 1) {
                    errors.add("supportedRefillTypes", new ActionMessage("errors.supported_refill_types.required"));
                }
                else
                {
                    commaInSupportedRefillTypes = getSupportedRefillTypes().split(",",-1).length-1;
                }    
                if (getSupportedRefillAmounts() == null || getSupportedRefillAmounts().length() < 1) {
                    errors.add("supportedRefillAmounts", new ActionMessage("errors.supported_refill_amounts.required"));
                }
                else
                {
                    commaInSupportedRefillAmounts = getSupportedRefillAmounts().split(",",-1).length-1;
                }  
                if(commaInSupportedRefillTypes != commaInSupportedRefillAmounts)
                {
                   errors.add("supportedRefillAmounts", new ActionMessage("errors.comma.notmatched")); 
                }    
            } else if(getType() == 2){
                if (getCompanyName() == null || getCompanyName().length() < 1) {
                    errors.add("companyName", new ActionMessage("errors.company_name.required"));
                }
            }
        }
        return errors;
    }
}
