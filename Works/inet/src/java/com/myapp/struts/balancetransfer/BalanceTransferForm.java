package com.myapp.struts.balancetransfer;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import java.util.ArrayList;

public class BalanceTransferForm extends org.apache.struts.action.ActionForm {

    private int doValidate;
    private int pageNo;
    private int recordPerPage;
    private int typeID;
    private int typeIDPar;  
    private long id;
    private double maxTransferredAmount;
    private double minTransferredAmount;
    private double surchargeAmount;    
    private String message;
    private ArrayList surchargeList;
    private long[] selectedIDs;

    public BalanceTransferForm() {
        super();
        this.typeIDPar = -1;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }    

    public int getTypeID()
    {
        return typeID;
    }

    public void setTypeID(int typeID)
    {
        this.typeID = typeID;
    }

    public int getTypeIDPar()
    {
        return typeIDPar;
    }

    public void setTypeIDPar(int typeIDPar)
    {
        this.typeIDPar = typeIDPar;
    }
    
    public double getMaxTransferredAmount() {
        return maxTransferredAmount;
    }

    public void setMaxTransferredAmount(double maxTransferredAmount) {
        this.maxTransferredAmount = maxTransferredAmount;
    }     
    
    public double getMinTransferredAmount() {
        return minTransferredAmount;
    }

    public void setMinTransferredAmount(double minTransferredAmount) {
        this.minTransferredAmount = minTransferredAmount;
    }    
    
    public double getSurchargeAmount() {
        return surchargeAmount;
    }

    public void setSurchargeAmount(double surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }

    public ArrayList getSurchargeList() {
        return surchargeList;
    }

    public void setSurchargeList(ArrayList surchargeList) {
        this.surchargeList = surchargeList;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }   

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (!Utils.isDouble(request.getParameter("maxTransferredAmount").toString())) {
                errors.add("maxTransferredAmount", new ActionMessage("errors.maxtransferredamount.valid"));
            }
            else if (getMaxTransferredAmount() <= 0.0) {
               errors.add("maxTransferredAmount", new ActionMessage("errors.maxtransferredamount.valid"));
            }            
            if (!Utils.isDouble(request.getParameter("minTransferredAmount").toString())) {
                errors.add("minTransferredAmount", new ActionMessage("errors.mintransferredamount.valid"));
            } 
            else if (getMinTransferredAmount() <= 0.0) {
               errors.add("minTransferredAmount", new ActionMessage("errors.mintransferredamount.valid"));
            }            
            if (!Utils.isDouble(request.getParameter("surchargeAmount").toString())) {
                errors.add("surchargeAmount", new ActionMessage("errors.surchargeamount.valid"));
            }   
            else if (getSurchargeAmount() <= 0.0) {
               errors.add("surchargeAmount", new ActionMessage("errors.surchargeamount.valid"));
            }             
        }
        return errors;
    }
}
