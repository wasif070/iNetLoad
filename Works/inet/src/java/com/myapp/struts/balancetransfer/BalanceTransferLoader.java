package com.myapp.struts.balancetransfer;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class BalanceTransferLoader {

    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    public HashMap<Long, BalanceTransferDTO> btSurchargeDTOsById;
    private ArrayList<BalanceTransferDTO> btSurchargeDTOList;
    static BalanceTransferLoader balanceTransferLoader = null;
    static Logger logger = Logger.getLogger(BalanceTransferLoader.class.getName());

    public BalanceTransferLoader() {
        forceReload();
    }

    public static BalanceTransferLoader getInstance() {
        if (balanceTransferLoader == null) {
            createBalanceTransferLoader();
        }
        return balanceTransferLoader;
    }

    private synchronized static void createBalanceTransferLoader() {
        if (balanceTransferLoader == null) {
            balanceTransferLoader = new BalanceTransferLoader();
        }
    }

    private void reload() {
         btSurchargeDTOsById = new HashMap<Long, BalanceTransferDTO>();
         btSurchargeDTOList = new ArrayList<BalanceTransferDTO>();
         DBConnection dbConnection = null;
         Statement statement = null;
         ResultSet rs = null;
         try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from bt_surcharge";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                BalanceTransferDTO dto = new BalanceTransferDTO();
                dto.setId(rs.getLong("id"));
                dto.setTypeID(rs.getInt("bt_type_id"));
                dto.setTypeName(Constants.TRANSFER_TYPE[dto.getTypeID()]);
                dto.setMaxTransferredAmount(rs.getDouble("bt_max_amount"));
                dto.setMinTransferredAmount(rs.getDouble("bt_min_amount"));
                dto.setSurchargeAmount(rs.getDouble("bt_surcharge_amount"));
                btSurchargeDTOList.add(dto);
                btSurchargeDTOsById.put(dto.getId(), dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getBTSurchargeDTOList() {
        checkForReload();
        return btSurchargeDTOList;
    }

    public synchronized BalanceTransferDTO getBTSurchargeDTOByID(long id) {
        checkForReload();
        return btSurchargeDTOsById.get(id);
    }   
}
