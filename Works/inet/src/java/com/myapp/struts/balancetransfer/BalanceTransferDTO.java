package com.myapp.struts.balancetransfer;

public class BalanceTransferDTO
{
    public boolean searchWithTypeID;   
    private int typeID;
    private int typeIDPar;
    private int doValidation;
    private int pageNo;
    private int recordPerPage;    
    private long id;
    private double maxTransferredAmount;
    private double minTransferredAmount;
    private double surchargeAmount;
    private String typeName;

    public BalanceTransferDTO()
    {
        searchWithTypeID = false;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }    

    public int getTypeID()
    {
        return typeID;
    }

    public void setTypeID(int typeID)
    {
        this.typeID = typeID;
    }

    public int getTypeIDPar()
    {
        return typeIDPar;
    }

    public void setTypeIDPar(int typeIDPar)
    {
        this.typeIDPar = typeIDPar;
    }
    
    public double getMaxTransferredAmount() {
        return maxTransferredAmount;
    }

    public void setMaxTransferredAmount(double maxTransferredAmount) {
        this.maxTransferredAmount = maxTransferredAmount;
    }     
    
    public double getMinTransferredAmount() {
        return minTransferredAmount;
    }

    public void setMinTransferredAmount(double minTransferredAmount) {
        this.minTransferredAmount = minTransferredAmount;
    }    
    
    public double getSurchargeAmount() {
        return surchargeAmount;
    }

    public void setSurchargeAmount(double surchargeAmount) {
        this.surchargeAmount = surchargeAmount;
    }       

    public int getPageNo()
    {
        return pageNo;
    }

    public void setPageNo(int pageNo)
    {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage()
    {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage)
    {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidation()
    {
        return doValidation;
    }

    public void setDoValidation(int doValidation)
    {
        this.doValidation = doValidation;
    } 
    
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }      
}
