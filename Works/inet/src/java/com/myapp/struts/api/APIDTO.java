package com.myapp.struts.api;

public class APIDTO {
    public boolean searchWithApiID = false;
    private int pageNo;
    private int recordPerPage;
    private int apiStatus;
    public int apiGPActive = 0;
    public int apiRBActive = 0;
    public int apiBLActive = 0;
    public int apiTTActive = 0;
    public int apiCCActive = 0;
    public int apiWRActive = 0;      
    private long id;
    private String apiId;
    private String apiUser;
    private String apiStatusName;
    private String apiLink;
    private String apiPassword;

    public APIDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiUser() {
        return apiUser;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public String getApiStatusName() {
        return apiStatusName;
    }

    public void setApiStatusName(String apiStatusName) {
        this.apiStatusName = apiStatusName;
    }

    public String getApiLink() {
        return apiLink;
    }

    public void setApiLink(String apiLink) {
        this.apiLink = apiLink;
    }

    public String getApiPassword() {
        return apiPassword;
    }

    public void setApiPassword(String apiPassword) {
        this.apiPassword = apiPassword;
    }

    public int getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(int apiStatus) {
        this.apiStatus = apiStatus;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }
}
