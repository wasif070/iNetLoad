package com.myapp.struts.api;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class APILoader {

    static Logger logger = Logger.getLogger(APILoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<APIDTO> apiDTOList = null;
    private HashMap<Long, APIDTO> apiDTOByID = null;
    static APILoader apiLoader = null;

    public APILoader() {
        forceReload();
    }

    public static APILoader getInstance() {
        if (apiLoader == null) {
            createAPILoader();
        }
        return apiLoader;
    }

    private synchronized static void createAPILoader() {
        if (apiLoader == null) {
            apiLoader = new APILoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();          
            apiDTOByID = new HashMap<Long, APIDTO>();
            apiDTOList = new ArrayList<APIDTO>();
            String sql = "select * from inetload_api";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                APIDTO dto = new APIDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setApiId(resultSet.getString("api_id"));
                dto.setApiLink(resultSet.getString("api_link"));
                dto.setApiUser(resultSet.getString("api_user"));
                dto.setApiPassword(resultSet.getString("api_password"));
                dto.setApiStatus(resultSet.getInt("api_status"));
                dto.setApiStatusName(Constants.LIVE_STATUS_STRING[resultSet.getInt("api_status")]);
                dto.apiGPActive = resultSet.getInt("api_gp_active");
                dto.apiRBActive = resultSet.getInt("api_rb_active");
                dto.apiBLActive = resultSet.getInt("api_bl_active");
                dto.apiTTActive = resultSet.getInt("api_tt_active");
                dto.apiCCActive = resultSet.getInt("api_cc_active");
                dto.apiWRActive = resultSet.getInt("api_wr_active");                
                apiDTOByID.put(dto.getId(), dto);
                apiDTOList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in APILoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<APIDTO> getApiDTOList() {
        checkForReload();
        return apiDTOList;
    }

    public synchronized APIDTO getApiDTOByID(long id) {
        checkForReload();
        return apiDTOByID.get(id);
    }
}
