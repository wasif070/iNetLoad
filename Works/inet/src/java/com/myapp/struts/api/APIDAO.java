package com.myapp.struts.api;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class APIDAO {

    static Logger logger = Logger.getLogger(APIDAO.class.getName());

    public APIDAO() {
    }

    public ArrayList<APIDTO> getAPIDTOsWithSearchParam(ArrayList<APIDTO> list, APIDTO adto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                APIDTO dto = (APIDTO) i.next();
                if ((adto.searchWithApiID && !dto.getApiId().toLowerCase().startsWith(adto.getApiId()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<APIDTO> getAPIDTOsSorted(ArrayList<APIDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    APIDTO dto1 = (APIDTO) o1;
                    APIDTO dto2 = (APIDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addAPIInformation(APIDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select api_id from inetload_api where api_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getApiId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate API ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into inetload_api(api_id,api_link,api_user,api_password,api_status,api_gp_active,api_rb_active,api_bl_active,api_tt_active,api_cc_active,api_wr_active) values(?,?,?,?,?,?,?,?,?,?,?);";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, p_dto.getApiId());
            ps2.setString(2, p_dto.getApiLink());
            ps2.setString(3, p_dto.getApiUser());
            ps2.setString(4, p_dto.getApiPassword());
            ps2.setInt(5, p_dto.getApiStatus());
            ps2.setInt(6, p_dto.apiGPActive);
            ps2.setInt(7, p_dto.apiRBActive);
            ps2.setInt(8, p_dto.apiBLActive);
            ps2.setInt(9, p_dto.apiTTActive);
            ps2.setInt(10, p_dto.apiCCActive);
            ps2.setInt(11, p_dto.apiWRActive);
            ps2.executeUpdate();
            APILoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding user: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editAPIInformation(APIDTO p_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update inetload_api set api_link=?,api_user=?,api_password=?,api_status=?,api_gp_active=?,api_rb_active=?,api_bl_active=?,api_tt_active=?,api_cc_active=?,api_wr_active=? where id=" + p_dto.getId();
            ps1 = dbConnection.connection.prepareStatement(sql);

            ps1.setString(1, p_dto.getApiLink());
            ps1.setString(2, p_dto.getApiUser());
            ps1.setString(3, p_dto.getApiPassword());
            ps1.setInt(4, p_dto.getApiStatus());
            ps1.setInt(5, p_dto.apiGPActive);
            ps1.setInt(6, p_dto.apiRBActive);
            ps1.setInt(7, p_dto.apiBLActive);
            ps1.setInt(8, p_dto.apiTTActive);
            ps1.setInt(9, p_dto.apiCCActive);
            ps1.setInt(10, p_dto.apiWRActive);            
            ps1.executeUpdate();
            APILoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing API: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteAPIs(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from inetload_api where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            APILoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleteing API: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateAPIs(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update inetload_api set api_status = " + Constants.USER_STATUS_ACTIVE + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            APILoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.ValidationError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while activating API: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError inactivateAPIs(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update inetload_api set api_status = " + Constants.USER_STATUS_INACTIVE + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            APILoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deactivating API: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
