package com.myapp.struts.api;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetAPIAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Long.parseLong(request.getParameter("id"));
        PermissionDTO userPerDTO = null;
        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE 
                || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FA_EDIT && login_dto.getClientStatus() ==  Constants.USER_STATUS_ACTIVE)) {
            APIForm formBean = (APIForm) form;
            APIDTO dto = new APIDTO();
            APITaskSchedular scheduler = new APITaskSchedular();
            dto = scheduler.getAPIDTO(id);
            if (dto != null) {
                formBean.setApiId(dto.getApiId());
                formBean.setApiLink(dto.getApiLink());
                formBean.setApiUser(dto.getApiUser());
                formBean.setApiPassword(dto.getApiPassword());
                formBean.setRetypePassword(dto.getApiPassword());
                formBean.setApiStatus(dto.getApiStatus());
                    if(dto.apiGPActive==1)
                    {    
                       request.setAttribute("apiGPActive", "checked");
                    }   
                    if(dto.apiRBActive==1)
                    {    
                       request.setAttribute("apiRBActive", "checked");
                    }  
                    if(dto.apiBLActive==1)
                    {    
                       request.setAttribute("apiBLActive", "checked");
                    }   
                    if(dto.apiTTActive==1)
                    {    
                       request.setAttribute("apiTTActive", "checked");
                    }   
                    if(dto.apiCCActive==1)
                    {    
                       request.setAttribute("apiCCActive", "checked");
                    }  
                    if(dto.apiWRActive==1)
                    {    
                       request.setAttribute("apiWRActive", "checked");
                    }                 
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
