package com.myapp.struts.api;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class APITaskSchedular {

    public APITaskSchedular() {
    }

    public MyAppError addAPIInformation(APIDTO p_dto) {
        APIDAO apiDAO = new APIDAO();
        return apiDAO.addAPIInformation(p_dto);
    }

    public MyAppError editAPIInformation(APIDTO p_dto, LoginDTO l_dto) {
        APIDAO apiDAO = new APIDAO();
        return apiDAO.editAPIInformation(p_dto, l_dto);
    }

    public APIDTO getAPIDTO(long id) {
        return APILoader.getInstance().getApiDTOByID(id);
    }

    public ArrayList<APIDTO> getAPIDTOsSorted(LoginDTO l_dto) {
        APIDAO apiDAO = new APIDAO();
        ArrayList<APIDTO> list = APILoader.getInstance().getApiDTOList();
        if (list != null) {
            return apiDAO.getAPIDTOsSorted((ArrayList<APIDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<APIDTO> getAPIDTOs(LoginDTO l_dto) {
        return APILoader.getInstance().getApiDTOList();
    }

    public ArrayList<APIDTO> getAPIDTOsWithSearchParam(APIDTO adto, LoginDTO l_dto) {
        APIDAO apiDAO = new APIDAO();
        ArrayList<APIDTO> list = APILoader.getInstance().getApiDTOList();
        if (list != null) {
            return apiDAO.getAPIDTOsWithSearchParam((ArrayList<APIDTO>) list.clone(), adto);
        }
        return null;
    }

    public MyAppError deleteAPIs(String list) {
        APIDAO apiDAO = new APIDAO();
        return apiDAO.deleteAPIs(list);
    }

    public MyAppError activateAPIs(String list) {
        APIDAO apiDAO = new APIDAO();
        return apiDAO.activateAPIs(list);
    }

    public MyAppError inactivateAPIs(String list) {
        APIDAO apiDAO = new APIDAO();
        return apiDAO.inactivateAPIs(list);
    }
}
