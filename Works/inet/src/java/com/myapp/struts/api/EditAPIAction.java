package com.myapp.struts.api;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;

public class EditAPIAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        APIForm formBean = (APIForm) form;
        PermissionDTO userPerDTO = null;
        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE 
                || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FA_EDIT && login_dto.getClientStatus() ==  Constants.USER_STATUS_ACTIVE)) {
            APIDTO dto = new APIDTO();
            APITaskSchedular scheduler = new APITaskSchedular();
            String opPers[] = request.getParameterValues("opPers");
            if (opPers != null && opPers.length != 0) {
                for (int i = 0; i < opPers.length; i++) {
                    switch (Integer.parseInt(opPers[i])) {
                        case Constants.GP:
                            dto.apiGPActive = 1;
                            break;
                        case Constants.RB:
                            dto.apiRBActive = 1;
                            break;
                        case Constants.BL:
                            dto.apiBLActive = 1;
                            break;
                        case Constants.TT:
                            dto.apiTTActive = 1;
                            break;
                        case Constants.CC:
                            dto.apiCCActive = 1;
                            break;
                        case Constants.WR:
                            dto.apiWRActive = 1;
                            break;
                        default:
                            break;
                    }
                }
            }
            dto.setId(formBean.getId());
            dto.setApiId(formBean.getApiId());
            dto.setApiLink(formBean.getApiLink());
            dto.setApiUser(formBean.getApiUser());
            dto.setApiPassword(formBean.getApiPassword());
            dto.setApiStatus(formBean.getApiStatus());

            MyAppError error = scheduler.editAPIInformation(dto, login_dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else if (request.getParameter("searchLink") != null) {
                formBean.setMessage(false, "API is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + request.getParameter("searchLink") + "&action=" + Constants.EDIT, true);
                return changedActionForward;
            } else {
                ActionForward changedActionForward = new ActionForward("../home/home.do", true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
