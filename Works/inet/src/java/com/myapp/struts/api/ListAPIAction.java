package com.myapp.struts.api;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListAPIAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        PermissionDTO userPerDTO = null;
        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE 
                || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FA && login_dto.getClientStatus() ==  Constants.USER_STATUS_ACTIVE)) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            APITaskSchedular scheduler = new APITaskSchedular();
            APIForm apiForm = (APIForm) form;

            int action = 0;
            if (request.getParameter("action") != null) {
                action = Integer.parseInt(request.getParameter("action"));
                String idList = "";
                if (action > Constants.UPDATE) {
                    if (apiForm.getSelectedIDs() != null) {
                        int length = apiForm.getSelectedIDs().length;
                        long[] idListArray = apiForm.getSelectedIDs();
                        if (length > 0) {
                            idList += idListArray[0];
                            for (int i = 1; i < length; i++) {
                                idList += "," + idListArray[i];
                            }
                            MyAppError error = new MyAppError();
                            switch (action) {
                                case Constants.DELETE:
                                    error = scheduler.deleteAPIs(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        apiForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        apiForm.setMessage(false, "APIs are deleted successfully.");
                                    }
                                    request.getSession(true).setAttribute(Constants.MESSAGE, apiForm.getMessage());
                                    break;
                                case Constants.ACTIVATION:
                                    error = scheduler.activateAPIs(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        apiForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        apiForm.setMessage(false, "APIs are activated successfully.");
                                    }
                                    request.setAttribute(Constants.API_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, apiForm.getMessage());
                                    break;
                                case Constants.DEACTIVATION:
                                    error = scheduler.inactivateAPIs(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        apiForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        apiForm.setMessage(false, "APIs are inactivated successfully.");
                                    }
                                    request.setAttribute(Constants.API_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, apiForm.getMessage());
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        apiForm.setMessage(true, "No API is selected.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, apiForm.getMessage());
                    }
                }
            }

            if (list_all == 0) {
                if (apiForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.API_RECORD_PER_PAGE, apiForm.getRecordPerPage());
                }
                APIDTO adto = new APIDTO();
                if (apiForm.getApiId() != null && apiForm.getApiId().trim().length() > 0) {
                    adto.searchWithApiID = true;
                    adto.setApiId(apiForm.getApiId().toLowerCase());
                }
                apiForm.setApiList(scheduler.getAPIDTOsWithSearchParam(adto, login_dto));
            } else if (list_all == 2) {
                if (action == Constants.ADD) {
                    apiForm.setMessage(false, "API is added successfully.");
                } else if (action == Constants.EDIT) {
                    apiForm.setMessage(false, "API is updated successfully.");
                }
                if (request.getSession(true).getAttribute(Constants.API_RECORD_PER_PAGE) != null) {
                    apiForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.API_RECORD_PER_PAGE).toString()));
                }
                request.getSession(true).setAttribute(Constants.MESSAGE, apiForm.getMessage());
                apiForm.setApiList(scheduler.getAPIDTOsSorted(login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.API_RECORD_PER_PAGE) != null) {
                    apiForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.API_RECORD_PER_PAGE).toString()));
                }
                apiForm.setApiList(scheduler.getAPIDTOs(login_dto));
            }
            apiForm.setSelectedIDs(null);
            if (apiForm.getApiList() != null && apiForm.getApiList().size() > 0 && apiForm.getApiList().size() <= (apiForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
