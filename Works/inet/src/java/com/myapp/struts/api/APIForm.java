package com.myapp.struts.api;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class APIForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private int apiStatus;
    private long id;
    private String apiId;
    private String apiUser;
    private String apiStatusName;
    private String apiLink;
    private String apiPassword;
    private String retypePassword;
    private String message;
    private long[] selectedIDs;
    private ArrayList apiList;

    public APIForm() {
        super();
        apiStatus = -1;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiUser() {
        return apiUser;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public String getApiStatusName() {
        return apiStatusName;
    }

    public void setApiStatusName(String apiStatusName) {
        this.apiStatusName = apiStatusName;
    }

    public String getApiLink() {
        return apiLink;
    }

    public void setApiLink(String apiLink) {
        this.apiLink = apiLink;
    }

    public String getApiPassword() {
        return apiPassword;
    }

    public void setApiPassword(String apiPassword) {
        this.apiPassword = apiPassword;
    }
    
    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }    

    public int getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(int apiStatus) {
        this.apiStatus = apiStatus;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList getApiList() {
        return apiList;
    }

    public void setApiList(ArrayList apiList) {
        this.apiList = apiList;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            boolean pass = true;
            boolean re_pass = true;
            if (getApiId() == null || getApiId().length() < 1) {
                errors.add("apiId", new ActionMessage("errors.api_id.required"));
            } 
            
            if (getApiLink() == null || getApiLink().length() < 1) {
                errors.add("apiLink", new ActionMessage("errors.api_link.required"));
            } 
             
            if (getApiUser() == null || getApiUser().length() < 1) {
                errors.add("apiUser", new ActionMessage("errors.user_id.required"));
            }    

            if (getApiPassword() == null || getApiPassword().length() < 1) {
                errors.add("apiPassword", new ActionMessage("errors.user_pass.required"));
                pass = false;
            }

            if (getRetypePassword() == null || getRetypePassword().length() < 1) {
                errors.add("retypePassword", new ActionMessage("errors.retype_pass.required"));
                re_pass = false;
            }

            if (pass == true && re_pass == true) {
                if (getApiPassword().length() < 6) {
                    errors.add("retypePassword", new ActionMessage("errors.password_length"));
                } else if (!getRetypePassword().equals(getApiPassword())) {
                    errors.add("retypePassword", new ActionMessage("errors.password_matching"));
                }
            }
        }
        return errors;
    }
}
