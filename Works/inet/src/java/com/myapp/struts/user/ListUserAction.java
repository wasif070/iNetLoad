package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListUserAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            UserTaskSchedular scheduler = new UserTaskSchedular();
            UserForm userForm = (UserForm) form;

            int action = 0;
            if (request.getParameter("action") != null) {
                action = Integer.parseInt(request.getParameter("action"));
                String idList = "";
                if (action > Constants.UPDATE) {
                    if (userForm.getSelectedIDs() != null) {
                        int length = userForm.getSelectedIDs().length;
                        long[] idListArray = userForm.getSelectedIDs();
                        if (length > 0) {
                            idList += idListArray[0];
                            for (int i = 1; i < length; i++) {
                                idList += "," + idListArray[i];
                            }
                            MyAppError error = new MyAppError();
                            switch (action) {
                                case Constants.DELETE:
                                    error = scheduler.deleteUsers(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        userForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        userForm.setMessage(false, "Users are deleted successfully.");
                                    }
                                    request.getSession(true).setAttribute(Constants.MESSAGE, userForm.getMessage());
                                    break;
                                case Constants.ACTIVATION:
                                    error = scheduler.activateUsers(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        userForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        userForm.setMessage(false, "Users are activated successfully.");
                                    }
                                    request.setAttribute(Constants.USER_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, userForm.getMessage());
                                    break;
                                case Constants.BLOCK:
                                    error = scheduler.blockUsers(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        userForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        userForm.setMessage(false, "Users are blocked successfully.");
                                    }
                                    request.setAttribute(Constants.USER_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, userForm.getMessage());
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        userForm.setMessage(true, "No user is selected.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, userForm.getMessage());
                    }
                }
            }

            if (list_all == 0) {
                if (userForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, userForm.getRecordPerPage());
                }
                UserDTO udto = new UserDTO();
                if (userForm.getUserId() != null && userForm.getUserId().trim().length() > 0) {
                    udto.searchWithUserID = true;
                    udto.setUserId(userForm.getUserId().toLowerCase());
                }
                if (userForm.getUserRoleId() >= 0) {
                    udto.searchWithRole = true;
                    udto.setUserRoleId(userForm.getUserRoleId());
                }
                if (userForm.getUserStatus() >= 0) {
                    udto.searchWithStatus = true;
                    udto.setUserStatus(userForm.getUserStatus());
                }
                userForm.setUserList(scheduler.getUserDTOsWithSearchParam(udto, login_dto));
            } else if (list_all == 2) {
                if (action == Constants.ADD) {
                    userForm.setMessage(false, "User is added successfully.");
                } else if (action == Constants.EDIT) {
                    userForm.setMessage(false, "User is updated successfully.");
                }
                if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                    userForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString()));
                }
                request.getSession(true).setAttribute(Constants.MESSAGE, userForm.getMessage());
                userForm.setUserList(scheduler.getUserDTOsSorted(login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                    userForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString()));
                }
                userForm.setUserList(scheduler.getUserDTOs(login_dto));
            }
            userForm.setSelectedIDs(null);
            if (userForm.getUserList() != null && userForm.getUserList().size() > 0 && userForm.getUserList().size() <= (userForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
