package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class UserDAO {

    static Logger logger = Logger.getLogger(UserDAO.class.getName());

    public UserDAO() {
    }

    public ArrayList<UserDTO> getUserDTOsWithoutOwn(ArrayList<UserDTO> list, LoginDTO ldto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                UserDTO dto = (UserDTO) i.next();
                if (dto.getId() == ldto.getId()) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<UserDTO> getUserDTOsWithSearchParam(ArrayList<UserDTO> list, UserDTO udto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                UserDTO dto = (UserDTO) i.next();
                if ((udto.searchWithUserID && !dto.getUserId().toLowerCase().startsWith(udto.getUserId()))
                        || (udto.searchWithRole && dto.getUserRoleId() != udto.getUserRoleId())
                        || (udto.searchWithStatus && dto.getUserStatus() != udto.getUserStatus())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<UserDTO> getUserDTOsSorted(ArrayList<UserDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    UserDTO dto1 = (UserDTO) o1;
                    UserDTO dto2 = (UserDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addUserInformation(UserDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select user_id from user where user_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getUserId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, p_dto.getUserId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into user(user_id,user_role_id,user_full_name,user_email,user_password,user_status,user_reg_time,user_createdBy_id) values(?,?,?,?,?,?,?,?);";
            ps3 = dbConnection.connection.prepareStatement(sql);

            ps3.setString(1, p_dto.getUserId());
            ps3.setLong(2, p_dto.getUserRoleId());
            ps3.setString(3, p_dto.getUserName());
            ps3.setString(4, p_dto.getUserEmail());
            ps3.setString(5, p_dto.getUserPassword());
            ps3.setInt(6, p_dto.getUserStatus());
            ps3.setLong(7, System.currentTimeMillis());
            ps3.setLong(8, p_dto.getUserCreatedById());
            ps3.executeUpdate();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding user: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }       
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }            
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editUserInformation(UserDTO p_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        PreparedStatement ps4 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select user_id from user where user_id = ? and id!=" + p_dto.getId();
            logger.debug("Edit user duplicate sql: " + sql);
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getUserId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_id = ?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, p_dto.getUserId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            if (p_dto.getId() != l_dto.getId()) {
                sql = "update user set user_id=?,user_role_id=?,user_full_name=?,user_email=?,user_password=?,user_status=? where id=" + p_dto.getId();
                ps3 = dbConnection.connection.prepareStatement(sql);

                ps3.setString(1, p_dto.getUserId());
                ps3.setLong(2, p_dto.getUserRoleId());
                ps3.setString(3, p_dto.getUserName());
                ps3.setString(4, p_dto.getUserEmail());
                ps3.setString(5, p_dto.getUserPassword());
                ps3.setInt(6, p_dto.getUserStatus());
                ps3.executeUpdate();
            } else {
                sql = "update user set user_id=?,user_full_name=?,user_email=? where id=" + p_dto.getId();
                ps4 = dbConnection.connection.prepareStatement(sql);

                ps4.setString(1, p_dto.getUserId());
                ps4.setString(2, p_dto.getUserName());
                ps4.setString(3, p_dto.getUserEmail());
                ps4.executeUpdate();
            }           
            UserLoader.getInstance().forceReload();
            if (p_dto.getId() == l_dto.getId())
            {
                l_dto.setClientId(p_dto.getUserId());
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing user: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps4 != null) {
                    ps4.close();
                }
            } catch (Exception e) {
            }            
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteUsers(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from user where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleteing user: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateUsers(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update user set user_status = " + Constants.USER_STATUS_ACTIVE + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.ValidationError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while activating user: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockUsers(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update user set user_status = " + Constants.USER_STATUS_BLOCK + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blocking user: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
