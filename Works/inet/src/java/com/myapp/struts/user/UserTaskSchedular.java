package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class UserTaskSchedular {

    public UserTaskSchedular() {
    }

    public MyAppError addUserInformation(UserDTO p_dto) {
        UserDAO userDAO = new UserDAO();
        return userDAO.addUserInformation(p_dto);
    }

    public MyAppError editUserInformation(UserDTO p_dto, LoginDTO l_dto) {
        UserDAO userDAO = new UserDAO();
        return userDAO.editUserInformation(p_dto, l_dto);
    }

    public UserDTO getUserDTO(long id) {
        return UserLoader.getInstance().getUserDTOByID(id);
    }

    public ArrayList<UserDTO> getUserDTOsSorted(LoginDTO l_dto) {
        UserDAO userDAO = new UserDAO();
        ArrayList<UserDTO> list = UserLoader.getInstance().getUserDTOList();
        if (list != null) {
            return userDAO.getUserDTOsSorted(userDAO.getUserDTOsWithoutOwn((ArrayList<UserDTO>) list.clone(), l_dto));
        }
        return null;
    }

    public ArrayList<UserDTO> getUserDTOs(LoginDTO l_dto) {
        UserDAO userDAO = new UserDAO();
        return userDAO.getUserDTOsWithoutOwn((ArrayList<UserDTO>) UserLoader.getInstance().getUserDTOList().clone(), l_dto);
    }

    public ArrayList<UserDTO> getUserDTOsWithSearchParam(UserDTO udto, LoginDTO l_dto) {
        UserDAO userDAO = new UserDAO();
        ArrayList<UserDTO> list = UserLoader.getInstance().getUserDTOList();
        if (list != null) {
            return userDAO.getUserDTOsWithoutOwn((ArrayList<UserDTO>) userDAO.getUserDTOsWithSearchParam((ArrayList<UserDTO>) list.clone(), udto), l_dto);
        }
        return null;
    }

    public MyAppError deleteUsers(String list) {
        UserDAO userDAO = new UserDAO();
        return userDAO.deleteUsers(list);
    }

    public MyAppError activateUsers(String list) {
        UserDAO userDAO = new UserDAO();
        return userDAO.activateUsers(list);
    }

    public MyAppError blockUsers(String list) {
        UserDAO userDAO = new UserDAO();
        return userDAO.blockUsers(list);
    }
}
