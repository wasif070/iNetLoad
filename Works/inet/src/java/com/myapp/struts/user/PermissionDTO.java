package com.myapp.struts.user;

import com.myapp.struts.session.PermissionConstants;

public class PermissionDTO {

    public boolean CLIENT = false;
    public boolean CLIENT_ADD = false;
    public boolean CLIENT_EDIT = false;
    public boolean CLIENT_RECHARGE = false;
    public boolean CLIENT_RETURN = false;
    public boolean CLIENT_RECEIVE = false;  
    public boolean RCG = false;
    public boolean RCG_ADD = false;
    public boolean RCG_EDIT = false;
    public boolean RCG_DELETE = false;
    public boolean RCS = false;
    public boolean RCS_ADD = false;
    public boolean RCS_EDIT = false;
    public boolean RCS_DELETE = false;  
    public boolean SC = false;
    public boolean SC_ADD = false;
    public boolean SC_EDIT = false;
    public boolean SC_DELETE = false;     
    public boolean REFILL = false;
    public boolean REFILL_GP = false;
    public boolean REFILL_BL = false;
    public boolean REFILL_RB = false;
    public boolean REFILL_WR = false;
    public boolean REFILL_TT = false;
    public boolean REFILL_CC = false;
    public boolean REFILL_CARD = false;
    public boolean REFILL_BT = false;
    public boolean REFILL_SMS = false;
    public boolean FLEXI_SIM_SETTINGS = false;
    public boolean FLEXI_SIM_SETTINGS_EDIT = false;
    public boolean FLEXI_SIM_SETTINGS_DELETE = false;
    public boolean SMS_SETTINGS = false;
    public boolean SMS_SETTINGS_EDIT = false;
    public boolean SMS_SETTINGS_DELETE = false;    
    public boolean FLEXI_SIM_RECHARGE = false;
    public boolean FLEXI_SIM_RECHARGE_ADD = false;
    public boolean FLEXI_SIM_RECHARGE_EDIT = false;
    public boolean FLEXI_SIM_RECHARGE_DELETE = false;
    public boolean FLEXI_MESSAGE_LOG = false;
    public boolean FLEXI_MESSAGE_LOG_DELETE = false;
    public boolean SMS_LOG = false;
    public boolean SMS_LOG_DELETE = false;    
    public boolean REPORT = false;
    public boolean REPORT_REFILL_SUMMERY = false;
    public boolean REPORT_PAYMENT_SUMMERY = false;
    public boolean REPORT_PAYMENT_MADE_HISTORY = false;
    public boolean FD = false;
    public boolean FD_ADD = false;
    public boolean FD_EDIT = false;
    public boolean FD_DELETE = false; 
    public boolean FD_PR = false;
    public boolean FD_TH = false;
    public boolean FA = false;
    public boolean FA_ADD = false;
    public boolean FA_EDIT = false;
    public boolean FA_DELETE = false;     

    public void setPermission(int permissionID) {
        switch(permissionID)
        {
            case PermissionConstants.CLIENT:
                CLIENT = true;
                break;
            case PermissionConstants.CLIENT_ADD:
                CLIENT_ADD = true;
                break;
            case PermissionConstants.CLIENT_EDIT:
                CLIENT_EDIT = true;
                break;
            case PermissionConstants.CLIENT_RECHARGE:
                CLIENT_RECHARGE = true;
                break;
            case PermissionConstants.CLIENT_RETURN:
                CLIENT_RETURN = true;
                break;
            case PermissionConstants.CLIENT_RECEIVE:
                CLIENT_RECEIVE = true;
                break;                
            case PermissionConstants.RCG:
                RCG = true;
                break;
            case PermissionConstants.RCG_ADD:
                RCG_ADD = true;
                break;
            case PermissionConstants.RCG_EDIT:
                RCG_EDIT = true;
                break;
            case PermissionConstants.RCG_DELETE:
                RCG_DELETE = true;
                break; 
            case PermissionConstants.RCS:
                RCS = true;
                break;
            case PermissionConstants.RCS_ADD:
                RCS_ADD = true;
                break;
            case PermissionConstants.RCS_EDIT:
                RCS_EDIT = true;
                break;
            case PermissionConstants.RCS_DELETE:
                RCS_DELETE = true;
                break;      
            case PermissionConstants.SC:
                SC = true;
                break;
            case PermissionConstants.SC_ADD:
                SC_ADD = true;
                break;
            case PermissionConstants.SC_EDIT:
                SC_EDIT = true;
                break;
            case PermissionConstants.SC_DELETE:
                SC_DELETE = true;
                break;                
            case PermissionConstants.REFILL:
                REFILL = true;
                break;
            case PermissionConstants.REFILL_GP:
                REFILL_GP = true;
                break;
            case PermissionConstants.REFILL_BL:
                REFILL_BL = true;
                break;
            case PermissionConstants.REFILL_RB:
                REFILL_RB = true;
                break;
            case PermissionConstants.REFILL_WR:
                REFILL_WR = true;
                break;
            case PermissionConstants.REFILL_TT:
                REFILL_TT = true;
                break;
            case PermissionConstants.REFILL_CC:
                REFILL_CC = true;
                break;
            case PermissionConstants.REFILL_CARD:
                REFILL_CARD = true;
                break; 
            case PermissionConstants.REFILL_BT:
                REFILL_BT = true;
                break;                 
            case PermissionConstants.FLEXI_SIM_SETTINGS:
                FLEXI_SIM_SETTINGS = true;
                break;
            case PermissionConstants.FLEXI_SIM_SETTINGS_EDIT:
                FLEXI_SIM_SETTINGS_EDIT = true;
                break;
            case PermissionConstants.FLEXI_SIM_SETTINGS_DELETE:
                FLEXI_SIM_SETTINGS_DELETE = true;
                break; 
            case PermissionConstants.SMS_SETTINGS:
                SMS_SETTINGS = true;
                break;
            case PermissionConstants.SMS_SETTINGS_EDIT:
                SMS_SETTINGS_EDIT = true;
                break;
            case PermissionConstants.SMS_SETTINGS_DELETE:
                SMS_SETTINGS_DELETE = true;
                break;                
            case PermissionConstants.FLEXI_SIM_RECHARGE:
                FLEXI_SIM_RECHARGE = true;
                break;
            case PermissionConstants.FLEXI_SIM_RECHARGE_ADD:
                FLEXI_SIM_RECHARGE_ADD = true;
                break;
            case PermissionConstants.FLEXI_SIM_RECHARGE_EDIT:
                FLEXI_SIM_RECHARGE_EDIT = true;
                break;
            case PermissionConstants.FLEXI_SIM_RECHARGE_DELETE:
                FLEXI_SIM_RECHARGE_DELETE = true;
                break;
            case PermissionConstants.FLEXI_MESSAGE_LOG:
                FLEXI_MESSAGE_LOG = true;
                break;
            case PermissionConstants.FLEXI_MESSAGE_LOG_DELETE:
                FLEXI_MESSAGE_LOG_DELETE = true;
                break; 
            case PermissionConstants.SMS_LOG:
                SMS_LOG = true;
                break;
            case PermissionConstants.SMS_LOG_DELETE:
                SMS_LOG_DELETE = true;
                break;                 
            case PermissionConstants.REPORT:
                REPORT = true;
                break;
            case PermissionConstants.REPORT_REFILL_SUMMERY:
                REPORT_REFILL_SUMMERY = true;
                break;
            case PermissionConstants.REPORT_PAYMENT_SUMMERY:
                REPORT_PAYMENT_SUMMERY = true;
                break;
            case PermissionConstants.REPORT_PAYMENT_MADE_HISTORY:
                REPORT_PAYMENT_MADE_HISTORY = true;
                break;
            case PermissionConstants.FD:
                FD = true;
                break;
            case PermissionConstants.FD_ADD:
                FD_ADD = true;
                break;
            case PermissionConstants.FD_EDIT:
                FD_EDIT = true;
                break;
            case PermissionConstants.FD_DELETE:
                FD_DELETE = true;
                break; 
            case PermissionConstants.FD_PR:
                FD_PR = true;
                break;
            case PermissionConstants.FD_TH:
                FD_TH = true;
                break;   
            case PermissionConstants.FA:
                FA = true;
                break;
            case PermissionConstants.FA_ADD:
                FA_ADD = true;
                break;
            case PermissionConstants.FA_EDIT:
                FA_EDIT = true;
                break;
            case PermissionConstants.FA_DELETE:
                FA_DELETE = true;
                break;                
            default:
                break;
        }
    }
}
