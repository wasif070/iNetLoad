package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;

public class AddUserAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {

            UserForm formBean = (UserForm) form;
            UserDTO dto = new UserDTO();

            UserTaskSchedular scheduler = new UserTaskSchedular();

            dto.setUserId(formBean.getUserId());
            dto.setUserName(formBean.getUserName());
            dto.setUserEmail(formBean.getUserEmail());
            dto.setUserRoleId(formBean.getUserRoleId());
            dto.setUserPassword(formBean.getUserPassword());
            dto.setUserStatus(formBean.getUserStatus());
            dto.setUserCreatedById(login_dto.getCreatedByID());

            MyAppError error = scheduler.addUserInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                formBean.setMessage(false, "User is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?list_all=2&action="+Constants.ADD, true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
