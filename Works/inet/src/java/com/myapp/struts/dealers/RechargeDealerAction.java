package com.myapp.struts.dealers;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RechargeDealerAction extends Action {

    static Logger logger = Logger.getLogger(RechargeDealerAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FD_PR) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }

                DealerTaskScheduler scheduler = new DealerTaskScheduler();
                DealerForm dealerForm = (DealerForm) form;
                int action = 0;
                if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE && request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        if (dealerForm.getSelectedIDs() != null) {
                            int length = dealerForm.getSelectedIDs().length;
                            long[] idListArray = dealerForm.getSelectedIDs();

                            if (length > 0) {
                                idList += idListArray[0];
                                for (int i = 1; i < length; i++) {
                                    idList += "," + idListArray[i];
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.RECHARGE:
                                        double[] amountListArray = dealerForm.getAmounts();
                                        String[] descriptionListArray = dealerForm.getDescriptions();
                                        error = scheduler.payDealers(login_dto, idListArray, amountListArray, descriptionListArray);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            dealerForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            dealerForm.setMessage(false, "Payment to dealers are successful.");
                                        }
                                        request.setAttribute(Constants.DEALER_PAYMENT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, dealerForm.getMessage());

                                        break;

                                    case Constants.RECEIVE:
                                        double[] amountListArray2 = dealerForm.getAmounts();

                                        String[] descriptionListArray2 = dealerForm.getDescriptions();
                                        error = scheduler.receiveDealers(login_dto, idListArray, amountListArray2, descriptionListArray2);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            dealerForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            dealerForm.setMessage(false, "Received from dealers are successful.");
                                        }
                                        request.setAttribute(Constants.DEALER_PAYMENT_ID_LIST, "," + idList + ",");

                                        request.getSession(true).setAttribute(Constants.MESSAGE, dealerForm.getMessage());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            dealerForm.setMessage(true, "No Dealer is selected!!!");
                            request.getSession(true).setAttribute(Constants.MESSAGE, dealerForm.getMessage());
                        }
                    }
                }

                if (list_all == 0) {
                    if (dealerForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.DEALER_RECHARGE_RECORD_PER_PAGE, dealerForm.getRecordPerPage());
                    }
                    DealerDTO ddto = new DealerDTO();
                    if (dealerForm.getDealerName() != null && dealerForm.getDealerName().trim().length() > 0) {
                        ddto.searchWithName = true;
                        ddto.setDealerName(dealerForm.getDealerName().toLowerCase());
                    }
                    dealerForm.setDealerList(scheduler.getDealerDTOsWithSearchParam(ddto, login_dto));
                } else {
                    if (request.getSession(true).getAttribute(Constants.DEALER_RECHARGE_RECORD_PER_PAGE) != null) {
                        dealerForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.DEALER_RECHARGE_RECORD_PER_PAGE).toString()));
                    }
                    dealerForm.setDealerList(scheduler.getDealerDTOs(login_dto));
                }
                if (dealerForm.getDealerList() != null && dealerForm.getDealerList().size() <= (dealerForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
