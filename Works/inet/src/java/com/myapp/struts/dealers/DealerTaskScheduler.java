package com.myapp.struts.dealers;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class DealerTaskScheduler {

    public DealerTaskScheduler() {
    }

    public MyAppError addDealerInformation(DealerDTO p_dto) {
        DealerDAO dealerDAO = new DealerDAO();
        return dealerDAO.addDealerInformation(p_dto);
    }

    public MyAppError editDealerInformation(DealerDTO p_dto) {
        DealerDAO dealerDAO = new DealerDAO();
        return dealerDAO.editDealerInformation(p_dto);
    }

    public DealerDTO getDealerDTO(long id) {
        return DealerLoader.getInstance().getDealerDTOByID(id);
    }

    public ArrayList<DealerDTO> getDealerDTOsSorted(LoginDTO l_dto) {
        DealerDAO dealerDAO = new DealerDAO();
        ArrayList<DealerDTO> list = DealerLoader.getInstance().getDealerDTOList();
        if (list != null) {
            return dealerDAO.getDealerDTOsSorted((ArrayList<DealerDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<DealerDTO> getDealerDTOs(LoginDTO l_dto) {
        return DealerLoader.getInstance().getDealerDTOList();
    }

    public ArrayList<DealerDTO> getDealerDTOsWithSearchParam(DealerDTO udto, LoginDTO l_dto) {
        DealerDAO dealerDAO = new DealerDAO();
        ArrayList<DealerDTO> list = DealerLoader.getInstance().getDealerDTOList();
        if (list != null) {
            return dealerDAO.getDealerDTOsWithSearchParam((ArrayList<DealerDTO>) list.clone(), udto);
        }
        return null;
    }

    public MyAppError deleteDealers(String idList) {
        DealerDAO dao = new DealerDAO();
        return dao.deleteDealers(idList);
    }

    public MyAppError payDealers(LoginDTO l_dto, long[] idListArray, double[] amountListArray, String[] descriptionListArray) {
        DealerDAO dao = new DealerDAO();
        return dao.payDealers(l_dto,idListArray,amountListArray,descriptionListArray);
    }

    public MyAppError receiveDealers(LoginDTO l_dto, long[] idListArray, double[] amountListArray, String[] descriptionListArray) {
        DealerDAO dao = new DealerDAO();
        return dao.receiveDealers(l_dto,idListArray,amountListArray,descriptionListArray);
    }
    
    public ArrayList<DealerDTO> getPaymentList(LoginDTO l_dto, DealerDTO d_dto) {
        DealerDAO dao = new DealerDAO();
        return dao.getPaymentList(l_dto, d_dto);
    }  
    
    public ArrayList<DealerDTO> getPaymentListWithSearchParam(LoginDTO l_dto, DealerDTO d_dto) {
        DealerDAO dao = new DealerDAO();
        return dao.getPaymentListWithSearchParam(l_dto, d_dto);
    }     
}
