package com.myapp.struts.dealers;

import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.gateway.GatewayLoader;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class DealerLoader {

    static Logger logger = Logger.getLogger(DealerLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<DealerDTO> dealerList = null;
    private HashMap<Long, DealerDTO> dealerDTOByID = null;
    static DealerLoader destcodeLoader = null;

    public DealerLoader() {
        forceReload();
    }

    public static DealerLoader getInstance() {
        if (destcodeLoader == null) {
            createGatewayLoader();
        }
        return destcodeLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (destcodeLoader == null) {
            destcodeLoader = new DealerLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            dealerList = new ArrayList<DealerDTO>();
            dealerDTOByID = new HashMap<Long, DealerDTO>();

            String sql = "select * from dealer where dealer_delete=0";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                DealerDTO dto = new DealerDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setDealerName(resultSet.getString("dealer_name"));
                dto.setDealerPhone(resultSet.getString("dealer_phone"));
                dto.setDealerAddress(resultSet.getString("dealer_address"));
                dto.setDealerCreatedDate(resultSet.getLong("dealer_create_date"));
                dto.setDealerCreatedDateString(Utils.LongToDate(dto.getDealerCreatedDate()));
                ArrayList<GatewayDTO> gdtoList = GatewayLoader.getInstance().getDealerGatewayDTOList(dto.getId());
                String dgateway = "";
                if (gdtoList != null) {
                    for (int i = 0; i < gdtoList.size(); i++) {
                        GatewayDTO gdto = gdtoList.get(i);
                        dgateway += i + 1 + ") " + gdto.getPhoneNo() + "<br />";
                    }
                }
                dto.setDealerGateway(dgateway);
                dealerDTOByID.put(dto.getId(), dto);
                dealerList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in DealerLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<DealerDTO> getDealerDTOList() {
        checkForReload();
        return dealerList;
    }

    public synchronized DealerDTO getDealerDTOByID(long id) {
        checkForReload();
        return dealerDTOByID.get(id);
    }
}
