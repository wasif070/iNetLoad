package com.myapp.struts.dealers;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class DealerDAO {

    static Logger logger = Logger.getLogger(DealerDAO.class.getName());

    public DealerDAO() {
    }

    public ArrayList<DealerDTO> getDealerDTOsWithSearchParam(ArrayList<DealerDTO> list, DealerDTO ddto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                DealerDTO dto = (DealerDTO) i.next();
                if ((ddto.searchWithName && !dto.getDealerName().toLowerCase().startsWith(ddto.getDealerNamePar()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<DealerDTO> getDealerDTOsSorted(ArrayList<DealerDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    DealerDTO dto1 = (DealerDTO) o1;
                    DealerDTO dto2 = (DealerDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addDealerInformation(DealerDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select dealer_name from dealer where dealer_name=? and dealer_delete=0";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getDealerName());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Dealer Name.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into dealer(dealer_name,dealer_address,dealer_phone,dealer_create_date) values(?,?,?,?)";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, p_dto.getDealerName());
            ps2.setString(2, p_dto.getDealerAddress());
            ps2.setString(3, p_dto.getDealerPhone());
            ps2.setLong(4, System.currentTimeMillis());
            ps2.executeUpdate();
            DealerLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding dealer: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editDealerInformation(DealerDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select dealer_name from dealer where dealer_name = ? and id !=" + p_dto.getId() + " and dealer_delete=0";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getDealerName());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Dealer Name.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update dealer set dealer_name=?,dealer_address=?,dealer_phone=?  where id=" + p_dto.getId();
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, p_dto.getDealerName());
            ps2.setString(2, p_dto.getDealerAddress());
            ps2.setString(3, p_dto.getDealerPhone());
            ps2.executeUpdate();
            DealerLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Dealer: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteDealers(String idList) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "update dealer set dealer_delete = 1 where id in(" + idList + ")";
            stmt.execute(sql);
            DealerLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while deleting dealers!!!", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError payDealers(LoginDTO login_dto, long[] idList, double[] amountList, String[] descriptionList) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "insert into dealer_transaction(transaction_dealer_id,transaction_type_id,transaction_amount,transaction_time,transaction_description) values";
            int lastIndex = idList.length - 1;
            for (int i = 0; i < lastIndex; i++) {
                    sql += "(" + idList[i] + "," + Constants.PAYMENT_TYPE_PAID + "," + amountList[i] + "," + System.currentTimeMillis() + ",'" + descriptionList[i] + "'),";
            }
                sql += "(" + idList[lastIndex] + "," + Constants.PAYMENT_TYPE_PAID + "," + amountList[lastIndex] + "," + System.currentTimeMillis() + ",'" + descriptionList[lastIndex] + "')";
            stmt.execute(sql);
        } catch (Exception ex) {
            logger.fatal("Error while paying dealer: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError receiveDealers(LoginDTO login_dto, long[] idList, double[] amountList, String[] descriptionList) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "insert into dealer_transaction(transaction_dealer_id,transaction_type_id,transaction_amount,transaction_time,transaction_description) values";
            int lastIndex = idList.length - 1;
            for (int i = 0; i < lastIndex; i++) {
                    sql += "(" + idList[i] + "," + Constants.PAYMENT_TYPE_RECEIVED + "," + amountList[i] + "," + System.currentTimeMillis() + ",'" + descriptionList[i] + "'),";
            }
                sql += "(" + idList[lastIndex] + "," + Constants.PAYMENT_TYPE_RECEIVED + "," + amountList[lastIndex] + "," + System.currentTimeMillis() + ",'" + descriptionList[lastIndex] + "')";
            stmt.execute(sql);
        } catch (Exception ex) {
            logger.fatal("Error while receiving dealer: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public ArrayList<DealerDTO> getPaymentList(LoginDTO login_dto, DealerDTO ddto) {
        ArrayList<DealerDTO> data = new ArrayList<DealerDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(ddto.getStartYear(), ddto.getStartMonth() - 1, ddto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(ddto.getEndYear(), ddto.getEndMonth() - 1, ddto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));

            String sql = "select * from dealer_transaction where transaction_time between " + start + " and " + end;

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                DealerDTO dto = new DealerDTO();
                dto.setId(rs.getLong("transaction_dealer_id"));
                DealerDTO dealerDTO = DealerLoader.getInstance().getDealerDTOByID(dto.getId());
                if(dealerDTO != null)
                {    
                   dto.setDealerName(dealerDTO.getDealerName());
                } 
                else
                {
                    continue;
                }    
                dto.setDealerPaymentTypeId(rs.getInt("transaction_type_id"));
                dto.setDealerPaymentTypeName(Constants.PAYMENT_TYPE[dto.getDealerPaymentTypeId()]);
                dto.setDealerPaymentAmount(rs.getDouble("transaction_amount"));                
                dto.setDealerPaymentTime(rs.getLong("transaction_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setDealerPaymentDescription(rs.getString("transaction_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentList:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<DealerDTO> getPaymentListWithSearchParam(LoginDTO login_dto, DealerDTO ddto) {
        ArrayList<DealerDTO> data = new ArrayList<DealerDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(ddto.getStartYear(), ddto.getStartMonth() - 1, ddto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(ddto.getEndYear(), ddto.getEndMonth() - 1, ddto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));

            String sql = "select * from dealer_transaction where transaction_time between " + start + " and " + end;

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                DealerDTO dto = new DealerDTO();
                dto.setId(rs.getLong("transaction_dealer_id"));
                DealerDTO dealerDTO = DealerLoader.getInstance().getDealerDTOByID(dto.getId());
                if(dealerDTO != null)
                {    
                   dto.setDealerName(dealerDTO.getDealerName());
                } 
                else
                {
                    continue;
                }    
                if ((ddto.searchWithName && !dto.getDealerName().toLowerCase().startsWith(ddto.getDealerName()))) {
                    continue;
                }
                dto.setDealerPaymentTypeId(rs.getInt("transaction_type_id"));
                dto.setDealerPaymentTypeName(Constants.PAYMENT_TYPE[dto.getDealerPaymentTypeId()]);
                dto.setDealerPaymentAmount(rs.getDouble("transaction_amount"));                
                dto.setDealerPaymentTime(rs.getLong("transaction_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setDealerPaymentDescription(rs.getString("transaction_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentListWithSearchParam:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }    
}
