package com.myapp.struts.dealers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;

public class ListPaymentsAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FD_TH && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int pageNo = 1;
                int list_all = 0;
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                DealerDTO ddto = new DealerDTO();
                DealerForm dealerForm = (DealerForm) form;
                DealerTaskScheduler scheduler = new DealerTaskScheduler();

                if (list_all == 0) {
                    if (dealerForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.DEALER_PAYMENT_RECORD_PER_PAGE, dealerForm.getRecordPerPage());
                    }
                    if (dealerForm.getDealerNamePar() != null && dealerForm.getDealerNamePar().trim().length() > 0) {
                        ddto.searchWithName = true;
                        ddto.setDealerName(dealerForm.getDealerNamePar().toLowerCase());
                    }
                    ddto.setEndDay(dealerForm.getEndDay());
                    ddto.setEndMonth(dealerForm.getEndMonth());
                    ddto.setEndYear(dealerForm.getEndYear());
                    ddto.setStartDay(dealerForm.getStartDay());
                    ddto.setStartMonth(dealerForm.getStartMonth());
                    ddto.setStartYear(dealerForm.getStartYear());
                    dealerForm.setDealerPaymentList(scheduler.getPaymentListWithSearchParam(login_dto, ddto));
                } else {
                    String dateParts[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    dealerForm.setStartDay(Integer.parseInt(dateParts[0]));
                    dealerForm.setStartMonth(Integer.parseInt(dateParts[1]));
                    dealerForm.setStartYear(Integer.parseInt(dateParts[2]));
                    dealerForm.setEndDay(dealerForm.getStartDay());
                    dealerForm.setEndMonth(dealerForm.getStartMonth());
                    dealerForm.setEndYear(dealerForm.getStartYear());
                    ddto.setStartDay(dealerForm.getStartDay());
                    ddto.setStartMonth(dealerForm.getStartMonth());
                    ddto.setStartYear(dealerForm.getStartYear());
                    ddto.setEndDay(dealerForm.getEndDay());
                    ddto.setEndMonth(dealerForm.getEndMonth());
                    ddto.setEndYear(dealerForm.getEndYear());
                    if (request.getSession(true).getAttribute(Constants.DEALER_PAYMENT_RECORD_PER_PAGE) != null) {
                        dealerForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.DEALER_PAYMENT_RECORD_PER_PAGE).toString()));
                    }
                    dealerForm.setDealerPaymentList(scheduler.getPaymentList(login_dto, ddto));
                }
                if (dealerForm.getDealerPaymentList() != null && dealerForm.getDealerPaymentList().size() > 0 && dealerForm.getDealerPaymentList().size() <= (dealerForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
