package com.myapp.struts.dealers;

import com.myapp.struts.gateway.GatewayForm;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EditDealerAction extends Action {

    static Logger logger = Logger.getLogger(AddDealerAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        DealerForm formBean = (DealerForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FD_EDIT)) {
                request.setAttribute(mapping.getAttribute(), formBean);
                DealerDTO dto = new DealerDTO();
                DealerTaskScheduler scheduler = new DealerTaskScheduler();
                dto.setDealerGateway(formBean.getDealerGateway());
                dto.setDealerName(formBean.getDealerName());
                dto.setDealerAddress(formBean.getDealerAddress());
                dto.setDealerPhone(formBean.getDealerPhone());
                MyAppError error = new MyAppError();
                long id = Long.parseLong(request.getParameter("id"));
                dto.setId(id);
                error = scheduler.editDealerInformation(dto);

                if (error.getErrorType() > 0) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else if (request.getParameter("searchLink") != null) {
                    formBean.setMessage(false, "Dealer is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + request.getParameter("searchLink") + "&action=" + Constants.EDIT, true);
                    return changedActionForward;
                } else {
                    ActionForward changedActionForward = new ActionForward("../home/home.do", true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
