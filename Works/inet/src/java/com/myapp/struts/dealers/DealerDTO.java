package com.myapp.struts.dealers;

import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;

public class DealerDTO {

    public boolean searchWithName = false;    
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;    
    private int gatewayID;
    private int dealerPaymentTypeId;
    private long id;
    private long dealerCreatedDate;
    private long dealerPaymentTime;
    private double dealerPaymentAmount;
    private String dealerName;
    private String dealerNamePar;
    private String dealerPaymentTypeName;
    private String dealerAddress;
    private String dealerPhone;
    private String dealerGateway;  
    private String dealerCreatedDateString;
    private String dealerPaymentDescription;
    private ArrayList<GatewayDTO> gatewayList;

    public DealerDTO() {
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis());

        this.startDay = Integer.parseInt(dateParts[0]);
        this.startMonth = Integer.parseInt(dateParts[1]);
        this.startYear = Integer.parseInt(dateParts[2]);
        this.endDay = this.startDay;
        this.endMonth = this.startMonth;
        this.endYear = this.startYear;
        dealerPaymentAmount = 0.0;        
    }
    
    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }     

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public long getDealerCreatedDate() {
        return dealerCreatedDate;
    }

    public void setDealerCreatedDate(long dealerCreatedDate) {
        this.dealerCreatedDate = dealerCreatedDate;
    }
    
    public double getDealerPaymentAmount() {
        return dealerPaymentAmount;
    }

    public void setDealerPaymentAmount(double dealerPaymentAmount) {
        this.dealerPaymentAmount = dealerPaymentAmount;
    }  
    
    public long getDealerPaymentTime() {
        return dealerPaymentTime;
    }

    public void setDealerPaymentTime(long dealerPaymentTime) {
        this.dealerPaymentTime = dealerPaymentTime;
    }     

    public String getDealerCreatedDateString() {
        return dealerCreatedDateString;
    }

    public void setDealerCreatedDateString(String dealerCreatedDateString) {
        this.dealerCreatedDateString = dealerCreatedDateString;
    }

    public int getGatewayID() {
        return gatewayID;
    }

    public void setGatewayID(int gatewayID) {
        this.gatewayID = gatewayID;
    }

    public String getDealerGateway() {
        return dealerGateway;
    }

    public void setDealerGateway(String dealerGateway) {
        this.dealerGateway = dealerGateway;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }
    
    public String getDealerNamePar() {
        return dealerNamePar;
    }

    public void setDealerNamePar(String dealerNamePar) {
        this.dealerNamePar = dealerNamePar;
    }    
    
    public String getDealerPaymentTypeName() {
        return dealerPaymentTypeName;
    }

    public void setDealerPaymentTypeName(String dealerPaymentTypeName) {
        this.dealerPaymentTypeName = dealerPaymentTypeName;
    }      

    public String getDealerPhone() {
        return dealerPhone;
    }

    public void setDealerPhone(String dealerPhone) {
        this.dealerPhone = dealerPhone;
    }
    
    public int getDealerPaymentTypeId() {
        return dealerPaymentTypeId;
    }

    public void setDealerPaymentTypeId(int dealerPaymentTypeId) {
        this.dealerPaymentTypeId = dealerPaymentTypeId;
    }    
    
    public String getDealerPaymentDescription() {
        return dealerPaymentDescription;
    }

    public void setDealerPaymentDescription(String dealerPaymentDescription) {
        this.dealerPaymentDescription = dealerPaymentDescription;
    }    

    public ArrayList<GatewayDTO> getGatewayList() {
        return gatewayList;
    }

    public void setGatewayList(ArrayList<GatewayDTO> gatewayList) {
        this.gatewayList = gatewayList;
    }
}