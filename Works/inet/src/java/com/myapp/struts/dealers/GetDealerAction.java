package com.myapp.struts.dealers;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class GetDealerAction extends Action {

    static Logger logger = Logger.getLogger(GetDealerAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.FD_EDIT) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                long id = Long.parseLong(request.getParameter("id"));
                DealerForm formBean = (DealerForm) form;
                DealerDTO dto = new DealerDTO();
                DealerTaskScheduler scheduler = new DealerTaskScheduler();
                dto = scheduler.getDealerDTO(id);
                if (dto != null) {
                    formBean.setId(dto.getId());
                    formBean.setDealerGateway(dto.getDealerGateway());
                    formBean.setDealerName(dto.getDealerName());
                    formBean.setDealerAddress(dto.getDealerAddress());
                    formBean.setDealerPhone(dto.getDealerPhone());
                } else {
                    target = "failure";
                }
                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}