package com.myapp.struts.dealers;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class DealerForm extends org.apache.struts.action.ActionForm {

    private int gatewayID;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int pageNo;
    private int recordPerPage;    
    private int doValidate;
    private long id;
    private String dealerName;
    private String dealerNamePar;
    private String dealerAddress;
    private String dealerPhone;
    private String dealerGateway; 
    private String message;
    private long[] selectedIDs;
    private double[] amounts;
    private String[] descriptions;     
    private ArrayList dealerList;
    private ArrayList dealerPaymentList;

    public DealerForm() {
        super();
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis());
        this.startDay = Integer.parseInt(dateParts[0]);
        this.startMonth = Integer.parseInt(dateParts[1]);
        this.startYear = Integer.parseInt(dateParts[2]);
        this.endDay = this.startDay;
        this.endMonth = this.startMonth;
        this.endYear = this.startYear;        
    }
    
    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }    

    public ArrayList getDealerList() {
        return dealerList;
    }

    public void setDealerList(ArrayList dealerList) {
        this.dealerList = dealerList;
    }
    
    public ArrayList getDealerPaymentList() {
        return dealerPaymentList;
    }

    public void setDealerPaymentList(ArrayList dealerPaymentList) {
        this.dealerPaymentList = dealerPaymentList;
    }      

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public int getGatewayID() {
        return gatewayID;
    }

    public void setGatewayID(int gatewayID) {
        this.gatewayID = gatewayID;
    }

    public String getDealerGateway() {
        return dealerGateway;
    }

    public void setDealerGateway(String dealerGateway) {
        this.dealerGateway = dealerGateway;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerNamePar() {
        return dealerNamePar;
    }

    public void setDealerNamePar(String dealerNamePar) {
        this.dealerNamePar = dealerNamePar;
    }

    public String getDealerPhone() {
        return dealerPhone;
    }

    public void setDealerPhone(String dealerPhone) {
        this.dealerPhone = dealerPhone;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }
    
    public double[] getAmounts() {
        return amounts;
    }
    
    public void setAmounts(double[] amounts) {
        this.amounts = amounts;
    }
    
    public String[] getDescriptions() {
        return descriptions;
    }
    
    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }    

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getDealerName() == null || getDealerName().length() == 0) {
                errors.add("dealerName", new ActionMessage("errors.dealer_name.required"));
            }
        }
        return errors;
    }
}
