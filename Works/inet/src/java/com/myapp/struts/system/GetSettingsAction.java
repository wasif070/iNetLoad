package com.myapp.struts.system;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetSettingsAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            SettingsForm formBean = (SettingsForm) form;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                SettingsDTO settingsDTO = new SettingsDTO();
                SettingsTaskScheduler scheduler = new SettingsTaskScheduler();
                settingsDTO = scheduler.getSettingsDTO();
                if (settingsDTO != null) {
                    formBean.setPrepaidMinRefillAmount(settingsDTO.getPrepaidMinRefillAmount());
                    formBean.setPrepaidMaxRefillAmount(settingsDTO.getPrepaidMaxRefillAmount());
                    formBean.setPostpaidMinRefillAmount(settingsDTO.getPostpaidMinRefillAmount());
                    formBean.setPostpaidMaxRefillAmount(settingsDTO.getPostpaidMaxRefillAmount());
                    formBean.setMinFlexiSIMBalance(settingsDTO.getMinFlexiSIMBalance());
                    formBean.setMinAllowedDiscount(settingsDTO.getMinAllowedDiscount());
                    formBean.setMaxAllowedDiscount(settingsDTO.getMaxAllowedDiscount());
                    formBean.setSameNumberRefillInterval(settingsDTO.getSameNumberRefillInterval());
                    formBean.setOffsetFromServerTime(settingsDTO.getOffsetFromServerTime());
                    formBean.setPendingTimeForAutoSystem(settingsDTO.getPendingTimeForAutoSystem());
                    formBean.setPendingTimeForManualSystem(settingsDTO.getPendingTimeForManualSystem());
                    formBean.setDataBackupInterval(settingsDTO.getDataBackupInterval());
                    formBean.setNotifyAfterPending(settingsDTO.getNotifyAfterPending());
                    formBean.setSimMessageLogRestoreInterval(settingsDTO.getSimMessageLogRestoreInterval());
                    formBean.setRecordPerPageForReporting(settingsDTO.getRecordPerPageForReporting());                     
                    formBean.setBrandName(settingsDTO.getBrandName());
                    formBean.setMotto(settingsDTO.getMotto());
                    formBean.setWelcomeNote(settingsDTO.getWelcomeNote());
                    formBean.setEmergencyNoticeText(settingsDTO.getEmergencyNoticeText());
                    formBean.setNotificationEmail(settingsDTO.getNotificationEmail());
                    formBean.setContactEmail(settingsDTO.getContactEmail());
                    formBean.setContactEmailPass(settingsDTO.getContactEmailPass());
                    formBean.setCopyrightLink(settingsDTO.getCopyrightLink());
                    formBean.setCopyrightLink(settingsDTO.getCopyrightLink());
                    //formBean.setApiUserID(settingsDTO.getApiUserID());
                    //formBean.setApiUserPassword(settingsDTO.getApiUserPassword());
                }
                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
