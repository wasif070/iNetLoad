package com.myapp.struts.system;

import com.myapp.struts.util.MyAppError;
import java.sql.Statement;
import databaseconnector.DBConnection;
import org.apache.log4j.Logger;

public class SettingsDAO {

    static Logger logger = Logger.getLogger(SettingsDAO.class.getName());

    public SettingsDAO() {
    }

    public MyAppError changeSettings(SettingsDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "update settings set prepaid_min_refill_amount=" + dto.getPrepaidMinRefillAmount() + ",postpaid_min_refill_amount=" + dto.getPostpaidMinRefillAmount()
                    + ",prepaid_max_refill_amount=" + dto.getPrepaidMaxRefillAmount() + ",postpaid_max_refill_amount=" + dto.getPostpaidMaxRefillAmount() + ",min_flexi_sim_balance=" + dto.getMinFlexiSIMBalance()
                    + ",min_allowed_discount=" + dto.getMinAllowedDiscount() + ",max_allowed_discount=" + dto.getMaxAllowedDiscount() + ",same_number_refill_interval=" + dto.getSameNumberRefillInterval()
                    + ",record_per_page_for_reporting=" + dto.getRecordPerPageForReporting() + ",sim_message_log_restore_interval=" + dto.getSimMessageLogRestoreInterval() + ",data_backup_time=" + dto.getDataBackupInterval()
                    + ",pending_time_for_auto_system=" + dto.getPendingTimeForAutoSystem() + ",pending_time_for_manual_system=" + dto.getPendingTimeForManualSystem() + ",notify_after_pending=" + dto.getNotifyAfterPending()
                    + ",offset_from_server_time=" + dto.getOffsetFromServerTime() + ",emergency_notice_text='" + dto.getEmergencyNoticeText() + "',brand_name='" + dto.getBrandName() + "',motto='" + dto.getMotto()
                    + "',welcome_note='" + dto.getWelcomeNote() + "',copyright_link='" + dto.getCopyrightLink() + "',contact_email='" + dto.getContactEmail() + "',contact_email_pass='" + dto.getContactEmailPass() + "',notification_email='" + dto.getNotificationEmail() + "'";
                    //+ ",api_user='" + dto.getApiUserID() + "',api_password='" + dto.getApiUserPassword() + "'"; 
            statement.execute(sql);
            SettingsLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while change settings: ", ex);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
