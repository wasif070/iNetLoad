package com.myapp.struts.system;

import com.myapp.struts.util.MyAppError;

public class SettingsTaskScheduler {

    public SettingsTaskScheduler() {
    }

    public MyAppError changeSettings(SettingsDTO sdto) {
        SettingsDAO settingsDAO = new SettingsDAO();
        return settingsDAO.changeSettings(sdto);
    }

    public SettingsDTO getSettingsDTO() {
        return SettingsLoader.getInstance().getSettingsDTO();
    }
}
