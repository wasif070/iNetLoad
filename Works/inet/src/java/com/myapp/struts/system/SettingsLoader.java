package com.myapp.struts.system;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class SettingsLoader {

    private static long LOADING_INTERVAL = 10 * 60 * 1000;
    private long loadingTime = 0;
    private SettingsDTO settingsDTO;
    static SettingsLoader settingsLoader = null;
    static Logger logger = Logger.getLogger(SettingsLoader.class.getName());

    public SettingsLoader() {
        forceReload();
    }

    public static SettingsLoader getInstance() {
        if (settingsLoader == null) {
            createSettingsLoader();
        }
        return settingsLoader;
    }

    private synchronized static void createSettingsLoader() {
        if (settingsLoader == null) {
            settingsLoader = new SettingsLoader();
        }
    }

    private void reload() {
         settingsDTO = new SettingsDTO();
         DBConnection dbConnection = null;
         Statement statement = null;
         ResultSet rs = null;
         try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from settings;";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                settingsDTO.setPrepaidMinRefillAmount(rs.getDouble("prepaid_min_refill_amount"));
                settingsDTO.setPrepaidMaxRefillAmount(rs.getDouble("prepaid_max_refill_amount"));
                settingsDTO.setPostpaidMinRefillAmount(rs.getDouble("postpaid_min_refill_amount"));
                settingsDTO.setPostpaidMaxRefillAmount(rs.getDouble("postpaid_max_refill_amount"));
                settingsDTO.setMinFlexiSIMBalance(rs.getDouble("min_flexi_sim_balance"));
                settingsDTO.setMinAllowedDiscount(rs.getDouble("min_allowed_discount"));
                settingsDTO.setMaxAllowedDiscount(rs.getDouble("max_allowed_discount"));
                settingsDTO.setSameNumberRefillInterval(rs.getInt("same_number_refill_interval"));
                settingsDTO.setOffsetFromServerTime(rs.getInt("offset_from_server_time"));
                settingsDTO.setPendingTimeForAutoSystem(rs.getInt("pending_time_for_auto_system"));
                settingsDTO.setPendingTimeForManualSystem(rs.getInt("pending_time_for_manual_system"));
                settingsDTO.setDataBackupInterval(rs.getInt("data_backup_time"));
                settingsDTO.setNotifyAfterPending(rs.getInt("notify_after_pending"));
                settingsDTO.setSimMessageLogRestoreInterval(rs.getInt("sim_message_log_restore_interval"));
                settingsDTO.setRecordPerPageForReporting(rs.getInt("record_per_page_for_reporting"));                      
                settingsDTO.setBrandName(rs.getString("brand_name"));
                settingsDTO.setMotto(rs.getString("motto"));
                settingsDTO.setWelcomeNote(rs.getString("welcome_note"));
                settingsDTO.setEmergencyNoticeText(rs.getString("emergency_notice_text"));
                settingsDTO.setNotificationEmail(rs.getString("notification_email"));
                settingsDTO.setContactEmail(rs.getString("contact_email"));
                settingsDTO.setContactEmailPass(rs.getString("contact_email_pass"));
                settingsDTO.setCopyrightLink(rs.getString("copyright_link"));
                //settingsDTO.setApiUserID(rs.getString("api_user"));
                //settingsDTO.setApiUserPassword(rs.getString("api_password"));
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized SettingsDTO getSettingsDTO() {
        checkForReload();
        return settingsDTO;
    }
}
