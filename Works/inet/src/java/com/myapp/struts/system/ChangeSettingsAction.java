package com.myapp.struts.system;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ChangeSettingsAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(ChangeSettingsAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            SettingsForm formBean = (SettingsForm) form;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                SettingsTaskScheduler scheduler = new SettingsTaskScheduler();
                SettingsDTO settingsDTO = new SettingsDTO();
                settingsDTO.setPrepaidMinRefillAmount(formBean.getPrepaidMinRefillAmount());
                settingsDTO.setPrepaidMaxRefillAmount(formBean.getPrepaidMaxRefillAmount());
                settingsDTO.setPostpaidMinRefillAmount(formBean.getPostpaidMinRefillAmount());
                settingsDTO.setPostpaidMaxRefillAmount(formBean.getPostpaidMaxRefillAmount());
                settingsDTO.setMinFlexiSIMBalance(formBean.getMinFlexiSIMBalance());
                settingsDTO.setMinAllowedDiscount(formBean.getMinAllowedDiscount());
                settingsDTO.setMaxAllowedDiscount(formBean.getMaxAllowedDiscount());
                settingsDTO.setSameNumberRefillInterval(formBean.getSameNumberRefillInterval());
                settingsDTO.setOffsetFromServerTime(formBean.getOffsetFromServerTime());
                settingsDTO.setPendingTimeForAutoSystem(formBean.getPendingTimeForAutoSystem());
                settingsDTO.setPendingTimeForManualSystem(formBean.getPendingTimeForManualSystem());
                settingsDTO.setDataBackupInterval(formBean.getDataBackupInterval());
                settingsDTO.setNotifyAfterPending(formBean.getNotifyAfterPending());
                settingsDTO.setSimMessageLogRestoreInterval(formBean.getSimMessageLogRestoreInterval());
                settingsDTO.setRecordPerPageForReporting(formBean.getRecordPerPageForReporting());                 
                settingsDTO.setBrandName(formBean.getBrandName());
                settingsDTO.setMotto(formBean.getMotto());
                settingsDTO.setWelcomeNote(formBean.getWelcomeNote());
                settingsDTO.setEmergencyNoticeText(formBean.getEmergencyNoticeText());
                settingsDTO.setNotificationEmail(formBean.getNotificationEmail());
                settingsDTO.setContactEmail(formBean.getContactEmail());
                settingsDTO.setContactEmailPass(formBean.getContactEmailPass());
                settingsDTO.setCopyrightLink(formBean.getCopyrightLink());
                settingsDTO.setApiUserID(formBean.getApiUserID());
                settingsDTO.setApiUserPassword(formBean.getApiUserPassword());                
                MyAppError error = new MyAppError();
                error = scheduler.changeSettings(settingsDTO);
                if (error.getErrorType() == MyAppError.NoError) {
                    formBean.setMessage(false, "System settings are updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());                    
                }                
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
