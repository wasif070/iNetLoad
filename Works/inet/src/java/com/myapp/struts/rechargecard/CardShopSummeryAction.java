package com.myapp.struts.rechargecard;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class CardShopSummeryAction
        extends Action {

    static Logger logger = Logger.getLogger(CardShopSummeryAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            RechargecardForm rcForm = (RechargecardForm) form;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCS && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE))) {
                int list_all = 0;
                int pageNo = 1;
                long id = -1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }                
                RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();

                if (list_all == 0) {
                    if (rcForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.CARD_SHOP_SUMMERY_RECORD_PER_PAGE, rcForm.getRecordPerPage());
                    }
                    RechargecardDTO rcdto = new RechargecardDTO();
                    if (rcForm.getGroupPackage() >= 0) {
                        rcdto.searchWithGroupPackage = true;
                        rcdto.setGroupPackage(rcForm.getGroupPackage());
                    }
                    if (rcForm.getGroupName() != null && rcForm.getGroupName().trim().length() > 0) {
                        rcdto.searchWithGroupName = true;
                        rcdto.setGroupName(rcForm.getGroupName().toLowerCase());
                    }                    
                    if (rcForm.getSign() > 0) {
                        rcdto.searchWithAmount = true;
                        rcdto.setSign(rcForm.getSign());
                        rcdto.setGroupAmount(rcForm.getGroupAmount());
                    }
                    rcForm.setCardSummeryList(scheduler.getCardShopSummeryDTOsWithSearchParam(rcdto, id));
                } else {
                    if (request.getSession(true).getAttribute(Constants.CARD_SHOP_SUMMERY_RECORD_PER_PAGE) != null) {
                        rcForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CARD_SHOP_SUMMERY_RECORD_PER_PAGE).toString()));
                    }
                    rcForm.setCardSummeryList(scheduler.getCardShopSummeryDTOs(id));
                }
                if (rcForm.getCardSummeryList() != null && rcForm.getCardSummeryList().size() > 0 && rcForm.getCardSummeryList().size() <= (rcForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
