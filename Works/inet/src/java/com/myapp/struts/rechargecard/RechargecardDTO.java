package com.myapp.struts.rechargecard;

import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.Utils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class RechargecardDTO {

    public boolean searchWithCardNo = false;
    public boolean searchWithSerialNo = false;
    public boolean searchWithGroupCountry = false;
    public boolean searchWithGroupPackage = false;
     public boolean searchWithGroupName = false;
    public boolean searchWithAmount = false;
    public boolean searchWithStatus = false;
    public boolean searchWithCardShopName = false;
    public boolean isDeleted = false;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;    
    private int cardStatus;
    private int sign;
    private int groupCountry;
    private int groupPackage;
    private int refillType;
    private int operatorTypeID;
    private int totalDistributed;
    private int totalUsed;
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long activatedAt;
    private long blockedAt;
    private long cardShop;
    private double groupAmount;
    private double cardPrice;
    private String cardShopName;
    private String phoneNumber;
    private String blockedBy;
    private String serialNo;
    private String cardNo;
    private String activatedBy;
    private String cardStatusName;
    private String groupName;
    private String groupIdentifier;
    private String groupCountryName;
    private String groupPackageName;
    private String activatedAtStr;
    private String blockedAtStr;
    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public RechargecardDTO() {
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis()  + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
        String dateParts1[] = Utils.getDateParts(System.currentTimeMillis()  + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) -(24*60*60*1000));        
        this.startDay = Integer.parseInt(dateParts1[0]);
        this.startMonth = Integer.parseInt(dateParts1[1]);
        this.startYear = Integer.parseInt(dateParts1[2]);
        this.endDay = Integer.parseInt(dateParts[0]);
        this.endMonth = Integer.parseInt(dateParts[1]);
        this.endYear = Integer.parseInt(dateParts[2]);        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public int getTotalDistributed() {
        return totalDistributed;
    }

    public void setTotalDistributed(int totalDistributed) {
        this.totalDistributed = totalDistributed;
    }  
    
    public int getTotalUsed() {
        return totalUsed;
    }

    public void setTotalUsed(int totalUsed) {
        this.totalUsed = totalUsed;
    }     
    
   public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }    

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }
    
    public int getRefillType() {
        return refillType;
    }

    public void setRefillType(int refillType) {
        this.refillType = refillType;
    }  
    
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }      
    
    public String getCardShopName() {
        return cardShopName;
    }

    public void setCardShopName(String cardShopName) {
        this.cardShopName = cardShopName;
    }     

    public int getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(int cardStatus) {
        this.cardStatus = cardStatus;
    }

    public long getActivatedAt() {
        return activatedAt;
    }

    public void setActivatedAt(long activatedAt) {
        this.activatedAt = activatedAt;
    }

    public int getGroupCountry() {
        return groupCountry;
    }

    public void setGroupCountry(int groupCountry) {
        this.groupCountry = groupCountry;
    }

    public int getGroupPackage() {
        return groupPackage;
    }

    public void setGroupPackage(int groupPackage) {
        this.groupPackage = groupPackage;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getBlockedBy() {
        return blockedBy;
    }

    public void setBlockedBy(String blockedBy) {
        this.blockedBy = blockedBy;
    }

    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getActivatedBy() {
        return activatedBy;
    }

    public void setActivatedBy(String activatedBy) {
        this.activatedBy = activatedBy;
    }

    public String getGroupCountryName() {
        return groupCountryName;
    }

    public void setGroupCountryName(String groupCountryName) {
        this.groupCountryName = groupCountryName;
    }

    public String getGroupPackageName() {
        return groupPackageName;
    }

    public void setGroupPackageName(String groupPackageName) {
        this.groupPackageName = groupPackageName;
    }
    
    public long getBlockedAt() {
        return blockedAt;
    }

    public void setBlockedAt(long blockedAt) {
        this.blockedAt = blockedAt;
    }    

    public double getCardPrice() {
        return cardPrice;
    }

    public void setCardPrice(double cardPrice) {
        this.cardPrice = cardPrice;
    }  
    
    public long getCardShop() {
        return cardShop;
    }

    public void setCardShop(long cardShop) {
        this.cardShop = cardShop;
    }    

    public String getCardStatusName() {
        return cardStatusName;
    }

    public void setCardStatusName(String cardStatusName) {
        this.cardStatusName = cardStatusName;
    }

    public double getGroupAmount() {
        return groupAmount;
    }

    public void setGroupAmount(double groupAmount) {
        this.groupAmount = groupAmount;
    }

    public String getActivatedAtStr() {
        if (activatedAt > 0) {
            return dateFormat.format(activatedAt);
        } else {
            return " ";
        }
    }

    public void setActivatedAtStr(String activatedAtStr) {
        this.activatedAtStr = activatedAtStr;
    }

    public String getBlockedAtStr() {
        if (blockedAt > 0) {
            return dateFormat.format(blockedAt);
        } else {
            return " ";
        }
    }

    public void setBlockedAtStr(String blockedAtStr) {
        this.blockedAtStr = blockedAtStr;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        if (isDeleted == 1) {
            this.isDeleted = true;
        } else {
            this.isDeleted = false;
        }
    }
    
    public int getOperatorTypeID() {
        return operatorTypeID;
    }

    public void setOperatorTypeID(int operatorTypeID) {
        this.operatorTypeID = operatorTypeID;
    }       
}
