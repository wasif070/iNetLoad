package com.myapp.struts.rechargecard;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListRechargecardAction
        extends Action {

    static Logger logger = Logger.getLogger(ListRechargecardAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            RechargecardForm rcForm = (RechargecardForm) form;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCG && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE))) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
                int action = 0;
                if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE && request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        long startingSerial = 0;
                        if (request.getParameter("startingSerial") != null && request.getParameter("startingSerial").length() > 0) {
                            startingSerial = Long.parseLong(request.getParameter("startingSerial")) ;
                        }
                        if (rcForm.getSelectedIDs() != null || startingSerial > 0) {
                            int length = 0;
                            long[] idListArray = null;
                            if (rcForm.getSelectedIDs() != null) {
                                length = rcForm.getSelectedIDs().length;
                                idListArray = rcForm.getSelectedIDs();
                            }
                            if (length > 0 || startingSerial > 0) {
                                if (length > 0) {
                                    idList += idListArray[0];
                                    for (int i = 1; i < length; i++) {
                                        idList += "," + idListArray[i];
                                    }
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.BLOCK:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.blockRechargecards(idList, login_dto);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcForm.setMessage(false, "Recharge Cards are blocked successfully.");
                                        }
                                        request.setAttribute(Constants.RC_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcForm.getMessage());
                                        break;

                                    case Constants.ACTIVATION:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        long cardShop = Long.parseLong(request.getParameter("cardShop"));
                                        double cardPrice = Double.parseDouble(request.getParameter("cardPrice"));
                                        if (startingSerial > 0) {
                                            int totalCards = Integer.parseInt(request.getParameter("totalCards"));
                                            error = scheduler.activateRechargecards(login_dto, cardShop, cardPrice, startingSerial, totalCards);
                                        } else {
                                            error = scheduler.activateRechargecards(idList, login_dto, cardShop, cardPrice);
                                        }
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcForm.setMessage(false, "Recharge Cards are distributed successfully.");
                                        }
                                        request.setAttribute(Constants.RC_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcForm.getMessage());
                                        break;

                                    case Constants.DEACTIVATION:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deactivateRechargecards(idList, login_dto);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcForm.setMessage(false, "Recharge Cards are returned successfully.");
                                        }
                                        request.setAttribute(Constants.RC_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcForm.getMessage());
                                        break;

                                    case Constants.DELETE:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deleteRechargecards(idList, login_dto);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcForm.setMessage(false, "Recharge Cards are deleted successfully.");
                                        }
                                        request.setAttribute(Constants.RC_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcForm.getMessage());

                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            rcForm.setMessage(true, "No Recharge Card is selected.");
                            request.getSession(true).setAttribute(Constants.MESSAGE, rcForm.getMessage());
                        }
                    }
                }

                if (list_all == 0) {
                    if (rcForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.RC_RECORD_PER_PAGE, rcForm.getRecordPerPage());
                    }
                    RechargecardDTO rcdto = new RechargecardDTO();
                    if (rcForm.getCardNo() != null && rcForm.getCardNo().trim().length() > 0) {
                        rcdto.searchWithCardNo = true;
                        rcdto.setCardNo(rcForm.getCardNo().toLowerCase());
                    }
                    if (rcForm.getSerialNo() != null && rcForm.getSerialNo().trim().length() > 0) {
                        rcdto.searchWithSerialNo = true;
                        rcdto.setSerialNo(rcForm.getSerialNo().toLowerCase());
                    }
                    if (rcForm.getCardStatus() >= 0) {
                        rcdto.searchWithStatus = true;
                        rcdto.setCardStatus(rcForm.getCardStatus());
                    }
                    if (rcForm.getGroupCountry() >= 0) {
                        rcdto.searchWithGroupCountry = true;
                        rcdto.setGroupCountry(rcForm.getGroupCountry());
                    }
                    if (rcForm.getGroupPackage() >= 0) {
                        rcdto.searchWithGroupPackage = true;
                        rcdto.setGroupPackage(rcForm.getGroupPackage());
                    }
                    if (rcForm.getCardShopName() != null && rcForm.getCardShopName().trim().length() > 0) {
                        rcdto.searchWithCardShopName = true;
                        rcdto.setCardShopName(rcForm.getCardShopName().toLowerCase());
                    }                    
                    if (rcForm.getSign() > 0) {
                        rcdto.searchWithAmount = true;
                        rcdto.setSign(rcForm.getSign());
                        rcdto.setGroupAmount(rcForm.getGroupAmount());
                    }
                    rcForm.setCardList(scheduler.getRechargecardDTOsWithSearchParam(rcdto, rcForm.getGroupIdentifier()));
                } else {
                    if (request.getSession(true).getAttribute(Constants.RC_RECORD_PER_PAGE) != null) {
                        rcForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.RC_RECORD_PER_PAGE).toString()));
                    }
                    rcForm.setCardList(scheduler.getRechargecardDTOs(rcForm.getGroupIdentifier()));
                }
                rcForm.setSelectedIDs(null);
                if (rcForm.getCardList() != null && rcForm.getCardList().size() > 0 && rcForm.getCardList().size() <= (rcForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
