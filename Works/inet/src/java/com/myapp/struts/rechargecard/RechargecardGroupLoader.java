package com.myapp.struts.rechargecard;

import com.myapp.struts.session.Constants;
import com.myapp.struts.user.UserLoader;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class RechargecardGroupLoader {

    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    public HashMap<Long, RechargecardGroupDTO> rechargecardGroupDTOsById;
    public HashMap<String, RechargecardGroupDTO> rechargecardGroupDTOsByGroupIdentifier;
    private ArrayList<RechargecardGroupDTO> rechargecardGroupDTOList;
    static RechargecardGroupLoader rechargecardGroupLoader = null;
    static Logger logger = Logger.getLogger(RechargecardGroupLoader.class.getName());

    public RechargecardGroupLoader() {
        forceReload();
    }

    public static RechargecardGroupLoader getInstance() {
        if (rechargecardGroupLoader == null) {
            createRechargecardGroupLoader();
        }
        return rechargecardGroupLoader;
    }

    private synchronized static void createRechargecardGroupLoader() {
        if (rechargecardGroupLoader == null) {
            rechargecardGroupLoader = new RechargecardGroupLoader();
        }
    }

    private void reload() {
        rechargecardGroupDTOsById = new HashMap<Long, RechargecardGroupDTO>();
        rechargecardGroupDTOsByGroupIdentifier = new HashMap<String, RechargecardGroupDTO>();
        rechargecardGroupDTOList = new ArrayList<RechargecardGroupDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from rechargecard_group";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RechargecardGroupDTO dto = new RechargecardGroupDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setGroupName(resultSet.getString("group_name"));
                dto.setGroupDes(resultSet.getString("group_des"));
                dto.setGroupPrefix(resultSet.getString("group_prefix"));
                dto.setGroupCreatedTime(resultSet.getLong("group_created_time"));
                dto.setGroupAmount(resultSet.getDouble("group_balance"));
                dto.setGroupCreatedBy(resultSet.getLong("group_created_by"));
                dto.setGroupCreatedByName(UserLoader.getInstance().getUserDTOByID(dto.getGroupCreatedBy()).getUserId());
                dto.setGroupTotal(resultSet.getInt("group_total_cards"));
                dto.setGroupLength(resultSet.getInt("group_card_length"));
                dto.setGroupCountry(resultSet.getInt("group_country"));
                dto.setGroupPackage(resultSet.getInt("group_package"));
                dto.setGroupCountryName(Constants.COUNTRY_ID_STRING[dto.getGroupCountry()]);
                dto.setGroupPackage(resultSet.getInt("group_package"));  
                dto.setGroupPackageName(Constants.PACKAGE_ID_STRING[dto.getGroupPackage()]); 
                dto.setGroupIdentifier(resultSet.getString("group_identifier"));
                dto.setIsDeleted(resultSet.getInt("group_deleted"));
                if (!dto.getIsDeleted()) {
                    rechargecardGroupDTOsById.put(dto.getId(), dto);
                    rechargecardGroupDTOsByGroupIdentifier.put(dto.getGroupIdentifier(), dto);
                    rechargecardGroupDTOList.add(dto);
                }
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getRechargecardGroupDTOList() {
        checkForReload();
        return rechargecardGroupDTOList;
    }

    public synchronized RechargecardGroupDTO getRechargecardGroupDTOsByID(long id) {
        checkForReload();
        return rechargecardGroupDTOsById.get(id);
    }
    
    public synchronized RechargecardGroupDTO getRechargecardGroupDTOsByGroupIdentifier(String groupIdentifier) {
        checkForReload();
        return rechargecardGroupDTOsByGroupIdentifier.get(groupIdentifier);
    }    
}
