package com.myapp.struts.rechargecard;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListRechargecardGroupAction
        extends Action {

    static Logger logger = Logger.getLogger(ListRechargecardGroupAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            RechargecardGroupForm rcgForm = (RechargecardGroupForm) form;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCG && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE))) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
                int action = 0;
                if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE && request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        if (rcgForm.getSelectedIDs() != null) {
                            int length = rcgForm.getSelectedIDs().length;
                            String[] idListArray = rcgForm.getSelectedIDs();

                            if (length > 0) {
                                idList += "'"+idListArray[0]+"'";
                                for (int i = 1; i < length; i++) {
                                    idList += ",'" + idListArray[i]+"'";
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.BLOCK:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.blockRechargecardGroups(idList,login_dto);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcgForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcgForm.setMessage(false, "Recharge Card Groups are blocked successfully.");
                                        }
                                        request.setAttribute(Constants.RCG_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcgForm.getMessage());
                                        break;
                                    case Constants.ACTIVATION:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        long cardShop = Long.parseLong(request.getParameter("cardShop"));
                                        double cardPrice =  Double.parseDouble(request.getParameter("cardPrice"));                                     
                                        error = scheduler.activateRechargecardGroups(idList,login_dto, cardShop, cardPrice);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcgForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcgForm.setMessage(false, "Recharge Card Groups are distributed successfully.");
                                        }
                                        request.setAttribute(Constants.RCG_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcgForm.getMessage());

                                        break;
                                    case Constants.DEACTIVATION:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deactivatedRechargecardGroups(idList,login_dto);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcgForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcgForm.setMessage(false, "Recharge Card Groups are returned successfully.");
                                        }
                                        request.setAttribute(Constants.RCG_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcgForm.getMessage());

                                        break;                                        
                                    case Constants.DELETE:
                                        if (userPerDTO != null && !userPerDTO.RCG_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deleteRechargecardGroups(idList,login_dto);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            rcgForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            rcgForm.setMessage(false, "Recharge Card Groups are deleted successfully.");
                                        }
                                        request.setAttribute(Constants.RCG_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, rcgForm.getMessage());

                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            rcgForm.setMessage(true, "No Recharge Card Group is selected.");
                            request.getSession(true).setAttribute(Constants.MESSAGE, rcgForm.getMessage());
                        }
                    }
                }
                if (list_all == 0) {
                    if (rcgForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.RCG_RECORD_PER_PAGE, rcgForm.getRecordPerPage());
                    }
                    RechargecardGroupDTO rcgdto = new RechargecardGroupDTO();
                    if (rcgForm.getGroupName() != null && rcgForm.getGroupName().trim().length() > 0) {
                        rcgdto.searchWithGroupName = true;
                        rcgdto.setGroupName(rcgForm.getGroupName().toLowerCase());
                    }
                    if (rcgForm.getGroupCountry() >= 0) {
                        rcgdto.searchWithGroupCountry = true;
                        rcgdto.setGroupCountry(rcgForm.getGroupCountry());
                    }
                    if (rcgForm.getGroupPackage() >= 0) {
                        rcgdto.searchWithGroupPackage = true;
                        rcgdto.setGroupPackage(rcgForm.getGroupPackage());
                    }                    
                    if (rcgForm.getSign() > 0) {
                        rcgdto.searchWithAmount = true;
                        rcgdto.setSign(rcgForm.getSign());
                        rcgdto.setGroupAmount(rcgForm.getGroupAmount());
                    }                    
                    rcgForm.setGroupList(scheduler.getRechargecardGroupDTOsWithSearchParam(rcgdto));
                } else if (list_all == 2) {
                    if (action == Constants.ADD) {
                        rcgForm.setMessage(false, "Recharge Card Group is added successfully.");
                    } else if (action == Constants.EDIT) {
                        rcgForm.setMessage(false, "Recharge Card Group is updated successfully.");
                    }
                    if (request.getSession(true).getAttribute(Constants.RCG_RECORD_PER_PAGE) != null) {
                        rcgForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.RCG_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, rcgForm.getMessage());
                    rcgForm.setGroupList(scheduler.getRechargecardGroupDTOsSorted());
                } else {
                    if (request.getSession(true).getAttribute(Constants.RCG_RECORD_PER_PAGE) != null) {
                        rcgForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.RCG_RECORD_PER_PAGE).toString()));
                    }
                    rcgForm.setGroupList(scheduler.getRechargecardGroupDTOs());
                }
                rcgForm.setSelectedIDs(null);
                if (rcgForm.getGroupList() != null && rcgForm.getGroupList().size() > 0 && rcgForm.getGroupList().size() <= (rcgForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
