package com.myapp.struts.rechargecard;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class CardShopLoader {

    static Logger logger = Logger.getLogger(CardShopLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<CardShopDTO> cardShopDTOList = null;
    private HashMap<Long, CardShopDTO> cardShopDTOByID = null;
    static CardShopLoader cardShopLoader = null;

    public CardShopLoader() {
        forceReload();
    }

    public static CardShopLoader getInstance() {
        if (cardShopLoader == null) {
            createCardShopLoader();
        }
        return cardShopLoader;
    }

    private synchronized static void createCardShopLoader() {
        if (cardShopLoader == null) {
            cardShopLoader = new CardShopLoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            cardShopDTOByID = new HashMap<Long, CardShopDTO>();
            cardShopDTOList = new ArrayList<CardShopDTO>();
            String sql = "select * from card_shop where shop_deleted=0";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CardShopDTO dto = new CardShopDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setShopName(resultSet.getString("shop_name"));
                dto.setShopOwner(resultSet.getString("shop_owner"));
                dto.setShopDescription(resultSet.getString("shop_description"));
                dto.setShopAddress(resultSet.getString("shop_address"));
                dto.setShopContactNumber(resultSet.getString("shop_contact_number"));
                dto.setIsDeleted(resultSet.getInt("shop_deleted"));
                cardShopDTOByID.put(dto.getId(), dto);
                cardShopDTOList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in UserLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<CardShopDTO> getCardShopDTOList() {
        checkForReload();
        return cardShopDTOList;
    }

    public synchronized CardShopDTO getCardShopDTOByID(long id) {
        checkForReload();
        return cardShopDTOByID.get(id);
    }
}
