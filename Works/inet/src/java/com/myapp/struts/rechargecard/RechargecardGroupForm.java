package com.myapp.struts.rechargecard;

import com.myapp.struts.util.Utils;
import com.myapp.struts.session.Constants;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class RechargecardGroupForm extends org.apache.struts.action.ActionForm {

    private int doValidate;
    private int groupTotal;
    private int groupLength;
    private int sign;
    private int groupCountry;
    private int groupPackage;     
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long groupCreatedTime;
    public double groupAmount;
    private String groupName;
    private String groupDes;
    private String groupPrefix;
    private String groupCountryName;
    private String groupPackageName;
    private String groupCreatedByName;
    private String groupCreatedTimeStr;
    private String groupIdentifier;
    private long groupCreatedBy;
    private String message;
    private String[] selectedIDs;
    private ArrayList groupList;
    public static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public RechargecardGroupForm() {
        super();
        groupCountry = -1;
        groupPackage = -1;        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getGroupTotal() {
        return groupTotal;
    }

    public void setGroupTotal(int groupTotal) {
        this.groupTotal = groupTotal;
    }

    public int getGroupLength() {
        return groupLength;
    }
    
    public int getGroupCountry() {
        return groupCountry;
    }

    public void setGroupCountry(int groupCountry) {
        this.groupCountry = groupCountry;
    }

    public int getGroupPackage() {
        return groupPackage;
    }

    public void setGroupPackage(int groupPackage) {
        this.groupPackage = groupPackage;
    }     
    
    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }    

    public void setGroupLength(int groupLength) {
        this.groupLength = groupLength;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDes() {
        return groupDes;
    }
 
    public void setGroupDes(String groupDes) {
        this.groupDes = groupDes;
    }    
    
    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }     
    
    public String getGroupCreatedByName() {
        return groupCreatedByName;
    }

    public void setGroupCreatedByName(String groupCreatedByName) {
        this.groupCreatedByName = groupCreatedByName;
    }
    
    public String getGroupCreatedTimeStr() {
        return dateFormat.format(groupCreatedTime);
    }

    public void setGroupCreatedTimeStr(String groupCreatedTimeStr) {
        this.groupCreatedTimeStr = groupCreatedTimeStr;
    }     

    public String getGroupPrefix() {
        return groupPrefix;
    }

    public void setGroupPrefix(String groupPrefix) {
        this.groupPrefix = groupPrefix;
    }

    public long getGroupCreatedBy() {
        return groupCreatedBy;
    }

    public void setGroupCreatedBy(long groupCreatedBy) {
        this.groupCreatedBy = groupCreatedBy;
    }

    public double getGroupAmount() {
        return groupAmount;
    }

    public void setGroupAmount(double groupAmount) {
        this.groupAmount = groupAmount;
    }

    public long getGroupCreatedTime() {
        return groupCreatedTime;
    }

    public void setGroupCreatedTime(long groupCreatedTime) {
        this.groupCreatedTime = groupCreatedTime;
    }
    
    public String getGroupCountryName() {
        return groupCountryName;
    }

    public void setGroupCountryName(String groupCountryName) {
        this.groupCountryName = groupCountryName;
    }

    public String getGroupPackageName() {
        return groupPackageName;
    }

    public void setGroupPackageName(String groupPackageName) {
        this.groupPackageName = groupPackageName;
    }      

    public String getMessage() {
        return message;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public ArrayList getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList groupList) {
        this.groupList = groupList;
    }

    public String[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(String[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getGroupName() == null || getGroupName().length() < 1) {
                errors.add("groupName", new ActionMessage("errors.group_name.required"));
            }
            if (getGroupPrefix() == null || getGroupPrefix().length() < 3) {
                errors.add("groupPrefix", new ActionMessage("errors.group_prefix.required"));
            }
            if (request.getParameter("groupAmount") != null && (!Utils.isDouble(request.getParameter("groupAmount")) || Double.parseDouble(request.getParameter("groupAmount")) <= 0.0)) {
                errors.add("groupAmount", new ActionMessage("errors.group_amount.valid"));
            }
            if (request.getParameter("groupLength") != null && (!Utils.isInteger(request.getParameter("groupLength")) || Integer.parseInt(request.getParameter("groupLength")) < 10)) {
                errors.add("groupLength", new ActionMessage("errors.group_length.valid"));
            }
            if (request.getParameter("groupTotal") != null && (!Utils.isInteger(request.getParameter("groupTotal")) || Integer.parseInt(request.getParameter("groupTotal")) < 1)) {
                errors.add("groupTotal", new ActionMessage("errors.group_total.valid"));
            }
        }
        return errors;
    }
}
