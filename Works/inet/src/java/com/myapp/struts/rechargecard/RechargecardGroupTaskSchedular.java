package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class RechargecardGroupTaskSchedular {

    public RechargecardGroupTaskSchedular() {
    }
    
    public MyAppError refillByCard(RechargecardDTO rc_dto, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.refillByCard(rc_dto, l_dto);
    }     
    
    public MyAppError rechargeByCard(RechargecardDTO rc_dto, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.rechargeByCard(rc_dto, l_dto);
    }    

    public MyAppError addRechargecardGroupInformation(RechargecardGroupDTO rcg_dto, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.addRechargecardGroup(rcg_dto, l_dto);
    }
    
    public MyAppError editRechargecardGroupInformation(RechargecardGroupDTO rcg_dto, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.editRechargecardGroup(rcg_dto, l_dto);
    }    

    public ArrayList<RechargecardGroupDTO> getRechargecardGroupDTOsWithSearchParam(RechargecardGroupDTO rcdto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        ArrayList<RechargecardGroupDTO> list = RechargecardGroupLoader.getInstance().getRechargecardGroupDTOList();
        if (list != null) {
            return rcgDAO.getRechargecardGroupDTOsWithSearchParam((ArrayList<RechargecardGroupDTO>) list.clone(), rcdto);
        }
        return null;
    }

    public ArrayList<RechargecardGroupDTO> getRechargecardGroupDTOsSorted() {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        ArrayList<RechargecardGroupDTO> list = RechargecardGroupLoader.getInstance().getRechargecardGroupDTOList();
        if (list != null) {
            return rcgDAO.getRechargecardGroupDTOsSorted((ArrayList<RechargecardGroupDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<RechargecardGroupDTO> getRechargecardGroupDTOs() {
        return RechargecardGroupLoader.getInstance().getRechargecardGroupDTOList();
    }

    public RechargecardGroupDTO getRechargecardGroupDTO(long id) {
        return RechargecardGroupLoader.getInstance().getRechargecardGroupDTOsByID(id);
    }

    public MyAppError blockRechargecardGroups(String list, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.blockRechargecardGroups(list,  l_dto);
    }

    public MyAppError activateRechargecardGroups(String list, LoginDTO l_dto, long card_shop, double card_price) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.distributeRechargecardGroups(list, l_dto, card_shop, card_price);
    }
    
    public MyAppError deactivatedRechargecardGroups(String list, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.returnRechargecardGroups(list,  l_dto);
    }    

    public MyAppError deleteRechargecardGroups(String list, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.deleteRechargecardGroups(list, l_dto);
    }
    
    public MyAppError editRechargecardInformation(RechargecardDTO rc_dto, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.editRechargecard(rc_dto, l_dto);
    }    

    public ArrayList<RechargecardDTO> getRechargecardDTOsWithSearchParam(RechargecardDTO rcdto, String groupIdentifier) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        ArrayList<RechargecardDTO> list = rcgDAO.getRechargecardDTOsWithSearchParam(rcdto, groupIdentifier);
        return list;
    }

    public ArrayList<RechargecardDTO> getRechargecardDTOs(String groupIdentifier) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        ArrayList<RechargecardDTO> list = rcgDAO.getRechargecardDTOs(groupIdentifier);
        return list;         
    }

    public ArrayList<RechargecardDTO> getCardShopSummeryDTOsWithSearchParam(RechargecardDTO rcdto, long groupId) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        ArrayList<RechargecardDTO> list = rcgDAO.getCardShopSummeryDTOsWithSearchParam(rcdto, groupId);
        return list;
    }

    public ArrayList<RechargecardDTO> getCardShopSummeryDTOs(long groupId) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        ArrayList<RechargecardDTO> list = rcgDAO.getCardShopSummeryDTOs(groupId);
        return list;         
    }
    
    public RechargecardDTO getRechargecardDTO(long id) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.getRechargecardDTO(id);
    }

    public MyAppError blockRechargecards(String list, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.blockRechargecards(list,l_dto);
    }

    public MyAppError activateRechargecards(String list, LoginDTO l_dto, long card_shop, double card_price) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.distributeRechargecards(list, l_dto, card_shop, card_price);
    }
    
    public MyAppError activateRechargecards(LoginDTO l_dto, long card_shop, double card_price, long startingSerial, int totalCards) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.distributeRechargecards(l_dto, card_shop, card_price,startingSerial,totalCards);
    }    
    
    public MyAppError deactivateRechargecards(String list, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.returnRechargecards(list, l_dto);
    }    

    public MyAppError deleteRechargecards(String list, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.deleteRechargecards(list,l_dto);
    }  
    
    public ArrayList<RechargecardDTO> getCardUsageDTOs(LoginDTO login_dto, RechargecardDTO rdto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.getCardUsageDTOs(login_dto, rdto);
    }    
}
