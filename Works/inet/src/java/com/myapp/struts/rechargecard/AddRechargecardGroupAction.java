package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;

public class AddRechargecardGroupAction
        extends Action {
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            RechargecardGroupForm formBean = (RechargecardGroupForm) form;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCG_ADD)
                    && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                RechargecardGroupDTO dto = new RechargecardGroupDTO();
                RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
                dto.setGroupAmount(formBean.getGroupAmount());
                dto.setGroupDes(formBean.getGroupDes());
                dto.setGroupLength(formBean.getGroupLength());
                dto.setGroupName(formBean.getGroupName());
                dto.setGroupCreatedBy(login_dto.getId());
                dto.setGroupCreatedTime(System.currentTimeMillis());
                dto.setGroupPrefix(formBean.getGroupPrefix());
                dto.setGroupTotal(formBean.getGroupTotal());
                dto.setGroupCountry(formBean.getGroupCountry());
                dto.setGroupPackage(formBean.getGroupPackage());

                MyAppError error = scheduler.addRechargecardGroupInformation(dto, login_dto);

                if (error.getErrorType() > 0) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else {
                    formBean.setMessage(false, "Recharge Card Group is added successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?list_all=2&action=" + Constants.ADD, true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
