package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;


import org.apache.log4j.Logger;

public class ListCardUsageAction
        extends Action {

    static Logger logger = Logger.getLogger(ListCardUsageAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if ((!login_dto.getIsUser() && login_dto.getIsSpecialAgent()) || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCS && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }

                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
                RechargecardForm rechargecardForm = (RechargecardForm) form;
                RechargecardDTO rdto = new RechargecardDTO();
                if (list_all == 0) {
                if (rechargecardForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.RC_USAGE_RECORD_PER_PAGE, rechargecardForm.getRecordPerPage());
                }
                    if (rechargecardForm.getCardNo() != null && rechargecardForm.getCardNo().trim().length() > 0) {
                        rdto.searchWithCardNo = true;
                        rdto.setCardNo(rechargecardForm.getCardNo());
                    }
                    if (rechargecardForm.getSerialNo() != null && rechargecardForm.getSerialNo().trim().length() > 0) {
                        rdto.searchWithSerialNo = true;
                        rdto.setSerialNo(rechargecardForm.getSerialNo());
                    }
                    if (rechargecardForm.getGroupPackage() >= 0) {
                        rdto.searchWithGroupPackage = true;
                        rdto.setGroupPackage(rechargecardForm.getGroupPackage());
                    }
                    if (rechargecardForm.getSign() > 0) {
                        rdto.searchWithAmount = true;
                        rdto.setSign(rechargecardForm.getSign());
                        rdto.setGroupAmount(rechargecardForm.getGroupAmount());
                    }
                    rdto.setStartDay(rechargecardForm.getStartDay());
                    rdto.setStartMonth(rechargecardForm.getStartMonth());
                    rdto.setStartYear(rechargecardForm.getStartYear());
                    rdto.setEndDay(rechargecardForm.getEndDay());
                    rdto.setEndMonth(rechargecardForm.getEndMonth());
                    rdto.setEndYear(rechargecardForm.getEndYear());
                }
                else
                {
                    if (request.getSession(true).getAttribute(Constants.RC_USAGE_RECORD_PER_PAGE) != null) {
                        rechargecardForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.RC_USAGE_RECORD_PER_PAGE).toString()));
                    }
                    rechargecardForm.setStartDay(rdto.getStartDay());
                    rechargecardForm.setStartMonth(rdto.getStartMonth());
                    rechargecardForm.setStartYear(rdto.getStartYear());
                    rechargecardForm.setEndDay(rdto.getEndDay());
                    rechargecardForm.setEndMonth(rdto.getEndMonth());
                    rechargecardForm.setEndYear(rdto.getEndYear());
                }
                rechargecardForm.setCardUsageList(scheduler.getCardUsageDTOs(login_dto, rdto));
                if (rechargecardForm.getCardUsageList() != null && rechargecardForm.getCardUsageList().size() > 0 && rechargecardForm.getCardUsageList().size() <= (rechargecardForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
