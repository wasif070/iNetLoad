package com.myapp.struts.rechargecard;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListCardShopAction
        extends Action {

    static Logger logger = Logger.getLogger(ListCardShopAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            CardShopForm csForm = (CardShopForm) form;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCS && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE))) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                CardShopTaskSchedular scheduler = new CardShopTaskSchedular();
                int action = 0;
                if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE && request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        if (csForm.getSelectedIDs() != null) {
                            int length = csForm.getSelectedIDs().length;
                            long[] idListArray = csForm.getSelectedIDs();

                            if (length > 0) {
                                idList += "'"+idListArray[0]+"'";
                                for (int i = 1; i < length; i++) {
                                    idList += ",'" + idListArray[i]+"'";
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {                                                                        
                                    case Constants.DELETE:
                                        if (userPerDTO != null && !userPerDTO.RCS_DELETE && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deleteCardShop(idList,login_dto);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            csForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            csForm.setMessage(false, "Card Shops are deleted successfully.");
                                        }
                                        request.setAttribute(Constants.CARD_SHOP_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, csForm.getMessage());

                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            csForm.setMessage(true, "No Card Shop is selected.");
                            request.getSession(true).setAttribute(Constants.MESSAGE, csForm.getMessage());
                        }
                    }
                }
                if (list_all == 0) {
                    if (csForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.CARD_SHOP_RECORD_PER_PAGE, csForm.getRecordPerPage());
                    }
                    CardShopDTO csdto = new CardShopDTO();
                    if (csForm.getShopName() != null && csForm.getShopName().trim().length() > 0) {
                        csdto.searchWithShopName = true;
                        csdto.setShopName(csForm.getShopName().toLowerCase());
                    }
                    if (csForm.getShopOwner() != null && csForm.getShopOwner().trim().length() > 0) {
                        csdto.searchWithShopOwner = true;
                        csdto.setShopOwner(csForm.getShopOwner().toLowerCase());
                    }                   
                    csForm.setShopList(scheduler.getCardShopDTOsWithSearchParam(csdto));
                } else if (list_all == 2) {
                    if (action == Constants.ADD) {
                        csForm.setMessage(false, "Card Shop is added successfully.");
                    } else if (action == Constants.EDIT) {
                        csForm.setMessage(false, "Card Shop is updated successfully.");
                    }
                    if (request.getSession(true).getAttribute(Constants.CARD_SHOP_RECORD_PER_PAGE) != null) {
                        csForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CARD_SHOP_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, csForm.getMessage());
                    csForm.setShopList(scheduler.getCardShopDTOsSorted());
                } else {
                    if (request.getSession(true).getAttribute(Constants.CARD_SHOP_RECORD_PER_PAGE) != null) {
                        csForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CARD_SHOP_RECORD_PER_PAGE).toString()));
                    }
                    csForm.setShopList(scheduler.getCardShopDTOs());
                }
                csForm.setSelectedIDs(null);
                if (csForm.getShopList() != null && csForm.getShopList().size() > 0 && csForm.getShopList().size() <= (csForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
