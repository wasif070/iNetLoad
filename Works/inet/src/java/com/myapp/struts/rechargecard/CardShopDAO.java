package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class CardShopDAO {

    static Logger logger = Logger.getLogger(CardShopDAO.class.getName());

    public CardShopDAO() {
    }

    public ArrayList<CardShopDTO> getCardShopDTOsWithSearchParam(ArrayList<CardShopDTO> list, CardShopDTO csdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                CardShopDTO dto = (CardShopDTO) i.next();
                if ((csdto.searchWithShopName && !dto.getShopName().toLowerCase().startsWith(csdto.getShopName()))
                        || (csdto.searchWithShopOwner && !dto.getShopOwner().toLowerCase().startsWith(csdto.getShopOwner()))) {
                    continue;
                }
            }
        }
        return newList;
    }

    public ArrayList<CardShopDTO> getCardShopDTOsSorted(ArrayList<CardShopDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    CardShopDTO dto1 = (CardShopDTO) o1;
                    CardShopDTO dto2 = (CardShopDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
    
    public ArrayList<CardShopDTO> getCardShopDTOsSortedByShopName(ArrayList<CardShopDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    CardShopDTO dto1 = (CardShopDTO) o1;
                    CardShopDTO dto2 = (CardShopDTO) o2;
                    return dto1.getShopName().compareTo(dto2.getShopName());
                }
            });
        }
        return list;
    }        

    public MyAppError addCardShop(CardShopDTO cs_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select shop_name from card_shop where shop_deleted=0 and shop_name='" + cs_dto.getShopName() + "'";
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Shop Name is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into card_shop(shop_name,shop_owner,shop_description,shop_address,shop_contact_number) values('"
                    + cs_dto.getShopName() + "','" + cs_dto.getShopOwner() + "','" + cs_dto.getShopDescription()
                    + "','" + cs_dto.getShopAddress() + "','" + cs_dto.getShopContactNumber() + "');";
            stmt.execute(sql);
            CardShopLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding Card Shop: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editCardShop(CardShopDTO cs_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select shop_name from card_shop where shop_deleted=0 and shop_name='" + cs_dto.getShopName() + "' and id!=" + cs_dto.getId();
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Shop Name is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update card_shop set shop_name='" + cs_dto.getShopName() + "', shop_owner='" + cs_dto.getShopOwner()
                    + "', shop_description='" + cs_dto.getShopDescription() + "', shop_address='" + cs_dto.getShopAddress()
                    + "', shop_contact_number='" + cs_dto.getShopContactNumber() + "' where id=" + cs_dto.getId();
            stmt.execute(sql);
            CardShopLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing Card Shop: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteCardShop(String list, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "update card_shop set shop_deleted=1, shop_name=concat(shop_name,' (X)') where id in(" + list + ")";
            stmt.execute(sql);
            CardShopLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleteCardShop: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public CardShopDTO getCardShopDTO(long id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        CardShopDTO dto = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from card_shop where id=" + id;
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                dto = new CardShopDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setShopName(resultSet.getString("shop_name"));
                dto.setShopOwner(resultSet.getString("shop_owner"));
                dto.setShopDescription(resultSet.getString("shop_description"));
                dto.setShopAddress(resultSet.getString("shop_address"));
                dto.setShopContactNumber(resultSet.getString("shop_contact_number"));
                dto.setIsDeleted(resultSet.getInt("shop_deleted"));
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }
}
