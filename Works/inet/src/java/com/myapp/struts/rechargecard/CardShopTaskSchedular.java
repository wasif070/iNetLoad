package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class CardShopTaskSchedular {

    public CardShopTaskSchedular() {
    }

    public MyAppError addCardShopInformation(CardShopDTO cs_dto, LoginDTO l_dto) {
        CardShopDAO csDAO = new CardShopDAO();
        return csDAO.addCardShop(cs_dto, l_dto);
    }
    

    public MyAppError editCardShopInformation(CardShopDTO cs_dto, LoginDTO l_dto) {
        CardShopDAO csDAO = new CardShopDAO();
        return csDAO.editCardShop(cs_dto, l_dto);
    }    

    public ArrayList<CardShopDTO> getCardShopDTOsWithSearchParam(CardShopDTO cs_dto) {
        CardShopDAO csDAO = new CardShopDAO();
        ArrayList<CardShopDTO> list = CardShopLoader.getInstance().getCardShopDTOList();
        if (list != null) {
            return csDAO.getCardShopDTOsWithSearchParam((ArrayList<CardShopDTO>) list.clone(), cs_dto);
        }
        return null;
    }

    public ArrayList<CardShopDTO> getCardShopDTOsSorted() {
        CardShopDAO csDAO = new CardShopDAO();
        ArrayList<CardShopDTO> list = CardShopLoader.getInstance().getCardShopDTOList();
        if (list != null) {
            return csDAO.getCardShopDTOsSorted((ArrayList<CardShopDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<CardShopDTO> getCardShopDTOs() {
        return CardShopLoader.getInstance().getCardShopDTOList();
    }

    public CardShopDTO getCardShopDTO(long id) {
        return CardShopLoader.getInstance().getCardShopDTOByID(id);
    }

    public MyAppError deleteCardShop(String list, LoginDTO l_dto) {
        CardShopDAO csDAO = new CardShopDAO();
        return csDAO.deleteCardShop(list, l_dto);
    }
 
}
