package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

public class GetRechargecardAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            RechargecardForm formBean = (RechargecardForm) form;
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCG_EDIT)
                    && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                RechargecardDTO dto = new RechargecardDTO();
                RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
                long id = Long.parseLong(request.getParameter("id"));

                dto = scheduler.getRechargecardDTO(id);

                if (dto != null) {
                    formBean.setId(dto.getId());
                    formBean.setGroupAmount(dto.getGroupAmount());
                    formBean.setGroupName(request.getParameter("groupName"));
                    formBean.setCardStatus(dto.getCardStatus());
                    formBean.setCardStatusStr(dto.getCardStatus());
                    formBean.setCardStatusName(Constants.LIVE_STATUS_STRING[dto.getCardStatus()]);
                    formBean.setCardNo(dto.getCardNo());
                    formBean.setSerialNo(dto.getSerialNo());
                    formBean.setGroupCountry(dto.getGroupCountry());
                    formBean.setGroupCountryName(dto.getGroupCountryName());
                    formBean.setGroupPackage(dto.getGroupPackage());
                    formBean.setGroupPackageName(dto.getGroupPackageName());
                    formBean.setGroupIdentifier(dto.getGroupIdentifier());
                    formBean.setActivatedBy(dto.getActivatedBy());
                    formBean.setActivatedAt(dto.getActivatedAt());
                    formBean.setActivatedAtStr(dto.getActivatedAtStr());
                    formBean.setBlockedBy(dto.getBlockedBy());
                    formBean.setBlockedAt(dto.getBlockedAt());
                    formBean.setBlockedAtStr(dto.getBlockedAtStr());
                    formBean.setCardPrice(dto.getCardPrice());
                    formBean.setCardShop(dto.getCardShop());                     
                }

                MyAppError error = new MyAppError();

                if (error.getErrorType() > 0) {
                    target = "failure";
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
