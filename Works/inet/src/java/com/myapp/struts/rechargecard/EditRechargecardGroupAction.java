package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;

public class EditRechargecardGroupAction
        extends Action {
    static Logger logger = Logger.getLogger(EditRechargecardGroupAction.class.getName());
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            RechargecardGroupForm formBean = (RechargecardGroupForm) form;
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCG_EDIT)
                    && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                RechargecardGroupDTO dto = new RechargecardGroupDTO();
                RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
                dto.setId(formBean.getId());
                dto.setGroupAmount(formBean.getGroupAmount());
                dto.setGroupDes(formBean.getGroupDes());
                dto.setGroupLength(formBean.getGroupLength());
                dto.setGroupName(formBean.getGroupName());
                dto.setGroupCreatedBy(login_dto.getId());
                dto.setGroupCreatedByName(formBean.getGroupCreatedByName());
                dto.setGroupCreatedTimeStr(formBean.getGroupCreatedTimeStr());
                dto.setGroupCreatedTime(System.currentTimeMillis());
                dto.setGroupPrefix(formBean.getGroupPrefix());
                dto.setGroupTotal(formBean.getGroupTotal());
                dto.setGroupCountry(formBean.getGroupCountry());
                dto.setGroupPackage(formBean.getGroupPackage());
                dto.setGroupIdentifier(formBean.getGroupIdentifier());
                
                MyAppError error = scheduler.editRechargecardGroupInformation(dto, login_dto);

                if (error.getErrorType() > 0) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else if (request.getParameter("searchLink") != null) {
                    formBean.setMessage(false, "Recharge Card  Group is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + request.getParameter("searchLink")  + "&action=" + Constants.EDIT, true);
                    return changedActionForward;
                } else {
                    ActionForward changedActionForward = new ActionForward("../home/home.do", true);
                    return changedActionForward;
                }                
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
