package com.myapp.struts.rechargecard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class RechargecardGroupDTO {

    public boolean searchWithGroupName = false;
    public boolean searchWithGroupCountry = false;
    public boolean searchWithGroupPackage = false;
    public boolean searchWithAmount = false;
    public boolean isDeleted = false;
    private int groupTotal;
    private int groupLength;
    private int sign;
    private int groupCountry;
    private int groupPackage; 
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long groupCreatedTime;    
    public double groupAmount;
    private String groupName;
    private String groupDes;
    private String groupPrefix;
    private String groupCreatedByName;
    private String groupCreatedTimeStr;
    private String groupIdentifier;
    private String groupCountryName;
    private String groupPackageName;    
    private long groupCreatedBy;
    public static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public RechargecardGroupDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }    

    public int getGroupTotal() {
        return groupTotal;
    }

    public void setGroupTotal(int groupTotal) {
        this.groupTotal = groupTotal;
    }

    public int getGroupLength() {
        return groupLength;
    }

    public void setGroupLength(int groupLength) {
        this.groupLength = groupLength;
    }
    
    public int getGroupCountry() {
        return groupCountry;
    }

    public void setGroupCountry(int groupCountry) {
        this.groupCountry = groupCountry;
    }

    public int getGroupPackage() {
        return groupPackage;
    }

    public void setGroupPackage(int groupPackage) {
        this.groupPackage = groupPackage;
    }      

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    
    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }    

    public String getGroupDes() {
        return groupDes;
    }

    public void setGroupDes(String groupDes) {
        this.groupDes = groupDes;
    }

    public String getGroupPrefix() {
        return groupPrefix;
    }

    public void setGroupPrefix(String groupPrefix) {
        this.groupPrefix = groupPrefix;
    }
    
    public String getGroupCountryName() {
        return groupCountryName;
    }

    public void setGroupCountryName(String groupCountryName) {
        this.groupCountryName = groupCountryName;
    }

    public String getGroupPackageName() {
        return groupPackageName;
    }

    public void setGroupPackageName(String groupPackageName) {
        this.groupPackageName = groupPackageName;
    }    

    public long getGroupCreatedBy() {
        return groupCreatedBy;
    }

    public void setGroupCreatedBy(long groupCreatedBy) {
        this.groupCreatedBy = groupCreatedBy;
    }

    public String getGroupCreatedByName() {
        return groupCreatedByName;
    }

    public void setGroupCreatedByName(String groupCreatedByName) {
        this.groupCreatedByName = groupCreatedByName;
    }
    
    public String getGroupCreatedTimeStr() {
        return dateFormat.format(groupCreatedTime);
    }

    public void setGroupCreatedTimeStr(String groupCreatedTimeStr) {
        this.groupCreatedTimeStr = groupCreatedTimeStr;
    }    

    public double getGroupAmount() {
        return groupAmount;
    }

    public void setGroupAmount(double groupAmount) {
        this.groupAmount = groupAmount;
    }

    public long getGroupCreatedTime() {
        return groupCreatedTime;
    }

    public void setGroupCreatedTime(long groupCreatedTime) {
        this.groupCreatedTime = groupCreatedTime;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        if (isDeleted == 1) {
            this.isDeleted = true;
        } else {
            this.isDeleted = false;
        }
    }
     
}
