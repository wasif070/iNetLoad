package com.myapp.struts.rechargecard;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.UserDTO;
import com.myapp.struts.user.UserLoader;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class RechargecardGroupDAO {

    static Logger logger = Logger.getLogger(RechargecardGroupDAO.class.getName());

    public RechargecardGroupDAO() {
    }

    public long getNormalRechargeCard(RechargecardDTO dto) {
        long rcID = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select id,card_balance from rechargecard where card_no = '" + dto.getCardNo() + "' and serial_no = " + dto.getSerialNo() + " and status_id = " + Constants.USER_STATUS_ACTIVE + " and package_id = " + Constants.PACKAGE_ID_NORMAL;
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                rcID = resultSet.getLong("id");
                dto.setGroupAmount(resultSet.getDouble("card_balance"));
            }
            resultSet.close();
        } catch (Exception ex) {
            logger.fatal("Error while getNormalRechargeCard: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rcID;
    }

    public long getSpecialRechargeCard(RechargecardDTO dto) {
        long rcID = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select id from rechargecard where card_no = '" + dto.getCardNo() + "' and serial_no = " + dto.getSerialNo() + " and status_id = " + Constants.USER_STATUS_ACTIVE + " and package_id = " + Constants.PACKAGE_ID_SPECIAL;
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                rcID = resultSet.getLong("id");
            }
            resultSet.close();
        } catch (Exception ex) {
            logger.fatal("Error while getSpecialRechargeCard: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rcID;
    }

    public MyAppError refillByCard(RechargecardDTO rcDTO, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        String marker = System.currentTimeMillis() + ":" + rcDTO.getPhoneNumber();

        try {
            long rcID = getNormalRechargeCard(rcDTO);
            if (rcID > 0) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                stmt = dbConnection.connection.createStatement();
                String sql = "insert into inetload_fdr(operator_type_id,inetload_phn_no,"
                        + "inetload_from_user_id,inetload_from_client_type_id,inetload_to_user_id,inetload_to_client_type_id,"
                        + "inetload_type_id,inetload_amount,inetload_time,inetload_marker,inetload_status,card_id) values";
                int fromTypeID = Constants.CLIENT_TYPE_AGENT;
                int toTypeID = fromTypeID;
                long fromID = l_dto.getId();
                long toID = fromID;
                sql += "(" + rcDTO.getOperatorTypeID() + ",'" + rcDTO.getPhoneNumber() + "'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ","
                        + rcDTO.getRefillType() + "," + rcDTO.getGroupAmount() + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING + "," + rcID + ")";
                stmt.execute(sql);
                sql = "update rechargecard,inetload_fdr set inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + ",status_id=" + Constants.USER_STATUS_BLOCK + ",blocked_by=" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + ",refill_no='" + rcDTO.getPhoneNumber() + "' where id=" + rcID + " and inetload_marker='" + marker + "'";
                stmt.execute(sql);
                RechargecardGroupLoader.getInstance().forceReload();
            } else {
                l_dto.setWrongEntry(l_dto.getWrongEntry() + 1);
                error.setErrorType(MyAppError.OtherError);
                error.setErrorMessage("Invalid Recarge Card. Please check serial no, card no and card type.");
            }

        } catch (Exception ex) {
            logger.fatal("Error while refillByCard: ", ex);
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error is Occured.");
            return error;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError rechargeByCard(RechargecardDTO rcDTO, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            long rcID = getSpecialRechargeCard(rcDTO);
            if (rcID > 0) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                String sql = "update rechargecard,client set client_credit_all=(client_credit_all+card_balance),status_id=" + Constants.USER_STATUS_BLOCK + ",blocked_by=" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + " where rechargecard.id = " + rcID + " and client.id=" + l_dto.getId() + ";";
                stmt = dbConnection.connection.createStatement();
                stmt.execute(sql);
                RechargecardGroupLoader.getInstance().forceReload();
                ClientLoader.getInstance().forceReload();
            } else {
                l_dto.setWrongEntry(l_dto.getWrongEntry() + 1);
                error.setErrorType(MyAppError.OtherError);
                error.setErrorMessage("Invalid Recarge Card. Please check serial no, card no and card type.");
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blockRechargecards: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList<RechargecardGroupDTO> getRechargecardGroupDTOsWithSearchParam(ArrayList<RechargecardGroupDTO> list, RechargecardGroupDTO rcdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RechargecardGroupDTO dto = (RechargecardGroupDTO) i.next();
                if ((rcdto.searchWithGroupName && !dto.getGroupName().toLowerCase().startsWith(rcdto.getGroupName()))
                        || (rcdto.searchWithGroupCountry && dto.getGroupCountry() != rcdto.getGroupCountry())
                        || (rcdto.searchWithGroupPackage && dto.getGroupPackage() != rcdto.getGroupPackage())) {
                    continue;
                }
                switch (rcdto.getSign()) {
                    case 1:
                        if (rcdto.getGroupAmount() == dto.getGroupAmount()) {
                            newList.add(dto);
                        }
                        break;
                    case 2:
                        if (rcdto.getGroupAmount() > dto.getGroupAmount()) {
                            newList.add(dto);
                        }
                        break;
                    case 3:
                        if (rcdto.getGroupAmount() < dto.getGroupAmount()) {
                            newList.add(dto);
                        }
                        break;
                    default:
                        newList.add(dto);
                        break;
                }
            }
        }
        return newList;
    }

    public ArrayList<RechargecardGroupDTO> getRechargecardGroupDTOsSorted(ArrayList<RechargecardGroupDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    RechargecardGroupDTO dto1 = (RechargecardGroupDTO) o1;
                    RechargecardGroupDTO dto2 = (RechargecardGroupDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addRechargecardGroup(RechargecardGroupDTO rc_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select group_name from rechargecard_group where group_deleted=0 and group_name='" + rc_dto.getGroupName() + "'";
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Group Name is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            resultSet = stmt.executeQuery("select card_no from rechargecard where card_no like '" + rc_dto.getGroupPrefix() + "%'");
            HashMap cards = new HashMap(100000, 1.0f);
            while (resultSet.next()) {
                String cardNo = resultSet.getString("card_no");
                cards.put(cardNo, cardNo);
            }
            resultSet.close();

            resultSet = stmt.executeQuery("select count(*) as total from rechargecard where country_id=" + rc_dto.getGroupCountry() + " and package_id=" + rc_dto.getGroupPackage() + " and card_balance=" + rc_dto.getGroupAmount());
            long lastSerial = 0;
            if (resultSet.next()) {
                lastSerial = resultSet.getLong("total");
            }
            resultSet.close();

            String groupIdentifier = rc_dto.getGroupName() + ":" + System.currentTimeMillis();

            String newGroup = "insert into rechargecard_group (group_name, group_des, group_prefix, group_created_time, "
                    + "group_balance, group_created_by, group_total_cards, group_card_length, group_country, group_package, group_identifier) "
                    + "values('" + rc_dto.getGroupName() + "','" + rc_dto.getGroupDes() + "','" + rc_dto.getGroupPrefix() + "'," + rc_dto.getGroupCreatedTime() + ","
                    + rc_dto.getGroupAmount() + "," + rc_dto.getGroupCreatedBy() + "," + rc_dto.getGroupTotal() + "," + rc_dto.getGroupLength() + "," + rc_dto.getGroupCountry() + "," + rc_dto.getGroupPackage() + ",'" + groupIdentifier + "')";
            stmt.addBatch(newGroup);

            lastSerial++;
            int minLength = rc_dto.getGroupLength();
            long counter = 0;
            long totalTry = 0;
            while (counter < rc_dto.getGroupTotal()) {
                String cardNo = generateCardNo(rc_dto.getGroupPrefix(), minLength);
                totalTry++;
                if (totalTry > (3 * rc_dto.getGroupTotal())) {
                    minLength++;
                    totalTry = 0;
                }
                if (cards.get(cardNo) == null) {
                    cards.put(cardNo, cardNo);
                    long serialNo = Constants.CARD_BASE + lastSerial;
                    String newCard = "insert into rechargecard (card_no, serial_no, country_id, package_id, "
                            + "card_balance, group_identifier)"
                            + " values('" + cardNo + "'," + serialNo + "," + rc_dto.getGroupCountry() + "," + rc_dto.getGroupPackage() + ","
                            + rc_dto.getGroupAmount() + ",'" + groupIdentifier + "')";
                    stmt.addBatch(newCard);
                    lastSerial++;
                    counter++;
                }
            }
            stmt.executeBatch();
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding Recharge Card Group: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public String generateCardNo(String prefix, int length) {
        StringBuffer cardBuffer = new StringBuffer(length);
        cardBuffer.append(prefix);

        for (int i = prefix.length(); i < length; i++) {
            cardBuffer.append((int) (Math.random() * 10));
        }
        return cardBuffer.toString();
    }

    public MyAppError editRechargecardGroup(RechargecardGroupDTO rc_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard_group set group_des=? where id=" + rc_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, rc_dto.getGroupDes());
            ps.executeUpdate();
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing Recharge Card Group: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError distributeRechargecards(LoginDTO l_dto, long card_shop, double card_price, long starting_serial, int total_cards) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set status_id=" + Constants.USER_STATUS_ACTIVE + ",activated_by=" + l_dto.getId() + ",activated_time=" + System.currentTimeMillis() + ",card_shop=" + card_shop + ",card_price=" + card_price + " where serial_no between " + starting_serial + " and " + (starting_serial + total_cards - 1) + " and status_id =" + Constants.USER_STATUS_INACTIVE;
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while distributeRechargecards: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError distributeRechargecards(String list, LoginDTO l_dto, long card_shop, double card_price) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set status_id=" + Constants.USER_STATUS_ACTIVE + ",activated_by=" + l_dto.getId() + ",activated_time=" + System.currentTimeMillis() + ",card_shop=" + card_shop + ",card_price=" + card_price + " where id in(" + list + ") and status_id =" + Constants.USER_STATUS_INACTIVE;
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while distributeRechargecards: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError distributeRechargecardGroups(String list, LoginDTO l_dto, long card_shop, double card_price) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set status_id=" + Constants.USER_STATUS_ACTIVE + ",activated_by=" + l_dto.getId() + ",activated_time=" + System.currentTimeMillis() + ",card_shop=" + card_shop + ",card_price=" + card_price + " where group_identifier in(" + list + ") and status_id =" + Constants.USER_STATUS_INACTIVE;
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while activateRechargecardGroups: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError returnRechargecards(String list, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set status_id=" + Constants.USER_STATUS_INACTIVE + ",activated_by=" + l_dto.getId() + ",activated_time=" + System.currentTimeMillis() + ",card_shop=0,card_price=0.0 where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while rerunRechargecards: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError returnRechargecardGroups(String list, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set status_id=" + Constants.USER_STATUS_INACTIVE + ",activated_by=" + l_dto.getId() + ",activated_time=" + System.currentTimeMillis() + ",card_shop=0,card_price=0.0 where group_identifier in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while rerunRechargecards: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockRechargecards(String list, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set status_id=" + Constants.USER_STATUS_BLOCK + ",blocked_by=" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + " where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blockRechargecards: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockRechargecardGroups(String list, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set status_id=" + Constants.USER_STATUS_BLOCK + ",blocked_by=" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + " where group_identifier in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blockRechargecardGroups: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRechargecardGroups(String list, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "update rechargecard,rechargecard_group set card_deleted=1,group_deleted=1,group_name=concat(group_name,' (X)') " + ",blocked_by =" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + " where rechargecard_group.group_identifier in(" + list + ") and rechargecard.group_identifier in(" + list + ")";
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleteRechargecardGroups: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRechargecards(String list, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "update rechargecard set card_deleted=1,blocked_by=" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + " where id in(" + list + ")";
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting recharge card: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editRechargecard(RechargecardDTO rc_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update rechargecard set card_shop=" + rc_dto.getCardShop() + ",card_price=" + rc_dto.getCardPrice() + ",status_id=" + rc_dto.getCardStatus() + " where id =" + rc_dto.getId();
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RechargecardGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editRechargecard: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public RechargecardDTO getRechargecardDTO(long id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        RechargecardDTO dto = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from rechargecard where id=" + id;
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                dto = new RechargecardDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setSerialNo(resultSet.getString("serial_no"));
                dto.setCardNo(resultSet.getString("card_no"));
                dto.setCardStatus(resultSet.getInt("status_id"));
                dto.setCardStatusName(Constants.LIVE_STATUS_STRING[resultSet.getInt("status_id")]);
                dto.setGroupCountry(resultSet.getInt("country_id"));
                dto.setGroupCountryName(Constants.COUNTRY_ID_STRING[resultSet.getInt("country_id")]);
                dto.setGroupAmount(resultSet.getDouble("card_balance"));
                dto.setGroupPackage(resultSet.getInt("package_id"));
                dto.setGroupPackageName(Constants.PACKAGE_ID_STRING[dto.getGroupPackage()]);
                dto.setGroupIdentifier(resultSet.getString("group_identifier"));
                UserDTO userDTO = UserLoader.getInstance().getUserDTOByID(resultSet.getLong("activated_by"));
                if (userDTO != null) {
                    dto.setActivatedBy(userDTO.getUserId());
                } else {
                    dto.setActivatedBy(" ");
                }
                if(resultSet.getLong("activated_time") > 0)
                {    
                   dto.setActivatedAt(resultSet.getLong("activated_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                }
                userDTO = UserLoader.getInstance().getUserDTOByID(resultSet.getLong("blocked_by"));
                if (userDTO != null) {
                    dto.setBlockedBy(userDTO.getUserId());
                } else {
                    dto.setBlockedBy(" ");
                }
                if(resultSet.getLong("blocked_time") > 0)
                {    
                  dto.setBlockedAt(resultSet.getLong("blocked_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                }
                dto.setCardPrice(resultSet.getDouble("card_price"));
                dto.setCardShop(resultSet.getLong("card_shop"));
                dto.setCardShopName(" ");
                CardShopDTO cardShopDTO = CardShopLoader.getInstance().getCardShopDTOByID(dto.getCardShop());
                if(cardShopDTO != null)
                {    
                   dto.setCardShopName(cardShopDTO.getShopName());
                }   
                dto.setIsDeleted(resultSet.getInt("card_deleted"));
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public ArrayList<RechargecardDTO> getRechargecardDTOsWithSearchParam(RechargecardDTO rcdto, String groupIdentifier) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<RechargecardDTO> list = new ArrayList<RechargecardDTO>();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from rechargecard where card_deleted=0";
            if (groupIdentifier != null && groupIdentifier.indexOf(":") != -1) {
                sql += " and group_identifier='" + groupIdentifier + "'";
            }
            if (rcdto.searchWithCardNo) {
                sql += " and card_no like '" + rcdto.getCardNo() + "%'";
            }
            if (rcdto.searchWithSerialNo) {
                sql += " and serial_no like '" + rcdto.getSerialNo() + "%'";
            }
            if (rcdto.searchWithGroupCountry) {
                sql += " and country_id =" + rcdto.getGroupCountry();
            }
            if (rcdto.searchWithGroupPackage) {
                sql += " and package_id =" + rcdto.getGroupPackage();
            }
            if (rcdto.searchWithStatus) {
                sql += " and status_id =" + rcdto.getCardStatus();
            }
            if (rcdto.searchWithAmount) {
                switch (rcdto.getSign()) {
                    case 1:
                        sql += " and card_balance=" + rcdto.getGroupAmount();
                        break;
                    case 2:
                        sql += " and card_balance>=" + rcdto.getGroupAmount();
                        break;
                    case 3:
                        sql += " and card_balance<=" + rcdto.getGroupAmount();
                        break;
                    default:
                        break;
                }
            }
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RechargecardDTO dto = new RechargecardDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setSerialNo(resultSet.getString("serial_no"));
                dto.setCardNo(resultSet.getString("card_no"));
                dto.setCardStatus(resultSet.getInt("status_id"));
                dto.setCardStatusName(Constants.LIVE_STATUS_STRING[resultSet.getInt("status_id")]);
                dto.setGroupCountry(resultSet.getInt("country_id"));
                dto.setGroupCountryName(Constants.COUNTRY_ID_STRING[resultSet.getInt("country_id")]);
                dto.setGroupAmount(resultSet.getDouble("card_balance"));
                dto.setGroupPackage(resultSet.getInt("package_id"));
                dto.setGroupPackageName(Constants.PACKAGE_ID_STRING[dto.getGroupPackage()]);
                dto.setGroupIdentifier(resultSet.getString("group_identifier"));
                UserDTO userDTO = UserLoader.getInstance().getUserDTOByID(resultSet.getLong("activated_by"));
                if (userDTO != null) {
                    dto.setActivatedBy(userDTO.getUserId());
                } else {
                    dto.setActivatedBy(" ");
                }
                if(resultSet.getLong("activated_time") > 0)
                {    
                   dto.setActivatedAt(resultSet.getLong("activated_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                }
                userDTO = UserLoader.getInstance().getUserDTOByID(resultSet.getLong("blocked_by"));
                if (userDTO != null) {
                    dto.setBlockedBy(userDTO.getUserId());
                } else {
                    dto.setBlockedBy(" ");
                }
                if(resultSet.getLong("blocked_time") > 0)
                {    
                  dto.setBlockedAt(resultSet.getLong("blocked_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                }                
                dto.setCardPrice(resultSet.getDouble("card_price"));
                dto.setCardShop(resultSet.getLong("card_shop"));
                dto.setIsDeleted(resultSet.getInt("card_deleted"));
                dto.setCardShopName(" ");
                CardShopDTO cardShopDTO = CardShopLoader.getInstance().getCardShopDTOByID(dto.getCardShop());
                if(cardShopDTO != null)
                {    
                   dto.setCardShopName(cardShopDTO.getShopName());
                }  
                if (rcdto.searchWithCardShopName && !dto.getCardShopName().toLowerCase().startsWith(rcdto.getCardShopName())) {
                    continue;
                }                              
                list.add(dto);
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public ArrayList<RechargecardDTO> getRechargecardDTOs(String groupIdentifier) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<RechargecardDTO> list = new ArrayList<RechargecardDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from rechargecard where card_deleted=0";
            if (groupIdentifier != null && groupIdentifier.indexOf(":") != -1) {
                sql += " and group_identifier='" + groupIdentifier + "'";
            }
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RechargecardDTO dto = new RechargecardDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setSerialNo(resultSet.getString("serial_no"));
                dto.setCardNo(resultSet.getString("card_no"));
                dto.setCardStatus(resultSet.getInt("status_id"));
                dto.setCardStatusName(Constants.LIVE_STATUS_STRING[resultSet.getInt("status_id")]);
                dto.setGroupCountry(resultSet.getInt("country_id"));
                dto.setGroupCountryName(Constants.COUNTRY_ID_STRING[resultSet.getInt("country_id")]);
                dto.setGroupAmount(resultSet.getDouble("card_balance"));
                dto.setGroupPackage(resultSet.getInt("package_id"));
                dto.setGroupPackageName(Constants.PACKAGE_ID_STRING[dto.getGroupPackage()]);
                dto.setGroupIdentifier(resultSet.getString("group_identifier"));
                UserDTO userDTO = UserLoader.getInstance().getUserDTOByID(resultSet.getLong("activated_by"));
                if (userDTO != null) {
                    dto.setActivatedBy(userDTO.getUserId());
                } else {
                    dto.setActivatedBy(" ");
                }
                if(resultSet.getLong("activated_time") > 0)
                {    
                   dto.setActivatedAt(resultSet.getLong("activated_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                }
                userDTO = UserLoader.getInstance().getUserDTOByID(resultSet.getLong("blocked_by"));
                if (userDTO != null) {
                    dto.setBlockedBy(userDTO.getUserId());
                } else {
                    dto.setBlockedBy(" ");
                }
                if(resultSet.getLong("blocked_time") > 0)
                {    
                  dto.setBlockedAt(resultSet.getLong("blocked_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                }
                dto.setCardPrice(resultSet.getDouble("card_price"));
                dto.setCardShop(resultSet.getLong("card_shop"));
                dto.setCardShopName(" ");
                CardShopDTO cardShopDTO = CardShopLoader.getInstance().getCardShopDTOByID(dto.getCardShop());
                if(cardShopDTO != null)
                {    
                   dto.setCardShopName(cardShopDTO.getShopName());
                }                  
                dto.setIsDeleted(resultSet.getInt("card_deleted"));
                list.add(dto);
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public ArrayList<RechargecardDTO> getCardUsageDTOs(LoginDTO login_dto, RechargecardDTO r_dto) {
        ArrayList<RechargecardDTO> data = new ArrayList<RechargecardDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(r_dto.getStartYear(), r_dto.getStartMonth() - 1, r_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
        long end = new GregorianCalendar(r_dto.getEndYear(), r_dto.getEndMonth() - 1, r_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
        String condition = " and (blocked_time between " + start + " and " + end + ") ";

        if (r_dto.searchWithCardNo) {
            condition += " and card_no like '" + r_dto.getCardNo() + "%'";
        }
        if (r_dto.searchWithSerialNo) {
            condition += " and serial_no like '" + r_dto.getSerialNo() + "%'";
        }
        if (r_dto.searchWithGroupPackage) {
            condition += " and package_id =" + r_dto.getGroupPackage();
        }
        if (r_dto.searchWithAmount) {
            switch (r_dto.getSign()) {
                case 1:
                    condition += " and card_balance=" + r_dto.getGroupAmount();
                    break;
                case 2:
                    condition += " and card_balance<" + r_dto.getGroupAmount();
                    break;
                case 3:
                    condition += " and card_balance>" + r_dto.getGroupAmount();
                    break;
                default:
                    break;
            }
        }

        String sql = null;

        if (login_dto.getIsUser()) {
            sql = "select * from rechargecard where status_id=" + Constants.USER_STATUS_BLOCK + condition;
        } else {
            sql = "select * from rechargecard where status_id=" + Constants.USER_STATUS_BLOCK + " and blocked_by=" + login_dto.getId() + condition;
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RechargecardDTO dto = new RechargecardDTO();
                dto.setSerialNo(rs.getString("serial_no"));
                dto.setCardNo(rs.getString("card_no"));
                dto.setGroupAmount(rs.getDouble("card_balance"));
                dto.setGroupPackage(rs.getInt("package_id"));
                dto.setGroupPackageName(Constants.PACKAGE_ID_STRING[dto.getGroupPackage()]);
                dto.setBlockedBy(" ");
                ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(rs.getLong("blocked_by"));
                if (clientDTO != null) {
                    dto.setBlockedBy(clientDTO.getClientId());
                } else {
                UserDTO userDTO = UserLoader.getInstance().getUserDTOByID(rs.getLong("blocked_by"));
                if (userDTO != null) {
                    dto.setBlockedBy(userDTO.getUserId());
                }                    
                }
                if(rs.getLong("blocked_time") > 0)
                {    
                  dto.setBlockedAt(rs.getLong("blocked_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                }                
                dto.setPhoneNumber(rs.getString("refill_no"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getCardUsageDTOs: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
   
    public ArrayList<RechargecardDTO> getCardShopSummeryDTOsWithSearchParam(RechargecardDTO rcdto, long shopID) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<RechargecardDTO> list = new ArrayList<RechargecardDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select group_identifier,package_id,card_balance,card_price,sum(status_id) as status,count(*) as total from rechargecard where card_deleted=0 and card_shop=" + shopID + " and status_id in(" + Constants.USER_STATUS_ACTIVE + "," + Constants.USER_STATUS_BLOCK + ") group by group_identifier,card_price" ;
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RechargecardDTO dto = new RechargecardDTO();
                dto.setGroupAmount(resultSet.getDouble("card_balance"));
                dto.setGroupPackage(resultSet.getInt("package_id"));
                dto.setGroupPackageName(Constants.PACKAGE_ID_STRING[dto.getGroupPackage()]);
                dto.setGroupIdentifier(resultSet.getString("group_identifier"));
                dto.setGroupName(" ");
                RechargecardGroupDTO rcgDTO = RechargecardGroupLoader.getInstance().getRechargecardGroupDTOsByGroupIdentifier(dto.getGroupIdentifier());
                if(rcgDTO != null)
                {    
                   dto.setGroupName(rcgDTO.getGroupName());
                }
                if (rcdto.searchWithGroupName && !dto.getGroupName().toLowerCase().startsWith(rcdto.getGroupName())) {
                    continue;
                }  
                if (rcdto.searchWithGroupPackage && dto.getGroupPackage() != rcdto.getGroupPackage()) {
                    continue;
                }                
                dto.setCardPrice(resultSet.getDouble("card_price"));
                dto.setTotalDistributed(resultSet.getInt("total"));
                dto.setTotalUsed(resultSet.getInt("status") - resultSet.getInt("total"));
                switch (rcdto.getSign()) {
                    case 1:
                        if (rcdto.getGroupAmount() == dto.getGroupAmount()) {
                            list.add(dto);
                        }
                        break;
                    case 2:
                        if (rcdto.getGroupAmount() > dto.getGroupAmount()) {
                            list.add(dto);
                        }
                        break;
                    case 3:
                        if (rcdto.getGroupAmount() < dto.getGroupAmount()) {
                            list.add(dto);
                        }
                        break;
                    default:
                        list.add(dto);
                        break;
                }                 
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }    

    public ArrayList<RechargecardDTO> getCardShopSummeryDTOs(long shopID) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<RechargecardDTO> list = new ArrayList<RechargecardDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select group_identifier,package_id,card_balance,card_price,sum(status_id) as status,count(*) as total from rechargecard where card_deleted=0 and card_shop=" + shopID + " and status_id in(" + Constants.USER_STATUS_ACTIVE + "," + Constants.USER_STATUS_BLOCK + ") group by group_identifier,card_price" ;
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RechargecardDTO dto = new RechargecardDTO();
                dto.setGroupAmount(resultSet.getDouble("card_balance"));
                dto.setGroupPackage(resultSet.getInt("package_id"));
                dto.setGroupPackageName(Constants.PACKAGE_ID_STRING[dto.getGroupPackage()]);
                dto.setGroupIdentifier(resultSet.getString("group_identifier"));
                dto.setGroupName(" ");
                RechargecardGroupDTO rcgDTO = RechargecardGroupLoader.getInstance().getRechargecardGroupDTOsByGroupIdentifier(dto.getGroupIdentifier());
                if(rcgDTO != null)
                {    
                   dto.setGroupName(rcgDTO.getGroupName());
                }
                dto.setCardPrice(resultSet.getDouble("card_price"));
                dto.setTotalDistributed(resultSet.getInt("total"));
                dto.setTotalUsed(resultSet.getInt("status") - resultSet.getInt("total"));
                list.add(dto);
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }    
}
