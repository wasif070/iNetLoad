package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

public class GetCardShopAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            CardShopForm formBean = (CardShopForm) form;
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.RCS_EDIT)
                    && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                CardShopDTO dto = new CardShopDTO();
                CardShopTaskSchedular scheduler = new CardShopTaskSchedular();
                long id = Long.parseLong(request.getParameter("id"));

                dto = scheduler.getCardShopDTO(id);

                if (dto != null) {
                    formBean.setId(dto.getId());
                    formBean.setShopAddress(dto.getShopAddress());
                    formBean.setShopContactNumber(dto.getShopContactNumber());
                    formBean.setShopDescription(dto.getShopDescription());
                    formBean.setShopName(dto.getShopName());
                    formBean.setShopOwner(dto.getShopOwner());
                }

                MyAppError error = new MyAppError();

                if (error.getErrorType() > 0) {
                    target = "failure";
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
