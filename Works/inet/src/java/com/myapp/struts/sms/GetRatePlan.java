/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class GetRatePlan extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.SC_EDIT)) {
                RatePlanForm formBean = (RatePlanForm) form;
                int id = Integer.parseInt(request.getParameter("id"));
                RatePlanDTO dto = new RatePlanDTO();
                SMSTaskScheduler scheduler = new SMSTaskScheduler();
                dto = scheduler.getRatePlanDTO(id);

                if (dto != null) {
                    formBean.setCountry_name(dto.getCountry_name());
                    formBean.setCountry_code(dto.getCountry_code());
                    formBean.setRate_amount(dto.getRate_amount());
                    formBean.setRateplan_id(dto.getRateplan_id());
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}