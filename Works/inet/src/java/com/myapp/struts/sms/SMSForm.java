/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Wasif Islam
 */
public class SMSForm extends org.apache.struts.action.ActionForm {

    private String message;
    private String phoneNumber;
    private String senderName;
    private int operatorTypeID;
    private double amount;
    private String countryCode;
    private String msg;
    private int doValidate;

    public SMSForm() {
        super();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public int getOperatorTypeID() {
        return operatorTypeID;
    }

    public void setOperatorTypeID(int operatorTypeID) {
        this.operatorTypeID = operatorTypeID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(boolean error, String msg) {
        if (error) {
            this.msg = "<span style='color:red; font-size: 12px;font-weight:bold'>" + msg + "</span>";
        } else {
            this.msg = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + msg + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getPhoneNumber() == null || getPhoneNumber().length() == 0) {
                errors.add("phoneNumber", new ActionMessage("errors.phnNumber.required"));
            } else if (getPhoneNumber().length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
            } else if (getPhoneNumber().length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                String phoneNo = getPhoneNumber();
                if (phoneNo.startsWith("+")) {
                    phoneNo = phoneNo.replace("+", "");
                }
                if (phoneNo.startsWith("88")) {
                    phoneNo = phoneNo.replace("88", "");
                }
                phoneNo = phoneNo.replace("-", "");
                phoneNo = phoneNo.replace(" ", "");
                if (phoneNo.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                    setPhoneNumber(phoneNo);
                } else {
                    errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
                }
            }
            if (getCountryCode() == null || getCountryCode().length() == 0) {
                errors.add("countryCode", new ActionMessage("errors.country_code.required"));
            } 
            if (getMessage() == null || getMessage().length() == 0) {
                errors.add("message", new ActionMessage("errors.message.required"));
            } 
//            if (getSenderName() == null || getSenderName().length() == 0) {
//                errors.add("senderName", new ActionMessage("errors.senderName.required"));
//            }

            /*if (!Utils.isDouble(request.getParameter("amount").toString())) {
                errors.add("amount", new ActionMessage("errors.flexi_amount.valid"));
            } else if (getAmount() <= 0.0) {
                errors.add("amount", new ActionMessage("errors.amount.required"));
            }*/


        }


        return errors;
    }
}
