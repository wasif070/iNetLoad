/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.api.APIDTO;
import com.myapp.struts.api.APILoader;
import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.flexi.FlexiDTO;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author user
 */
public class SMSDAO {

    static Logger logger = Logger.getLogger(SMSDAO.class.getName());

    public SMSDAO() {
    }

    public ArrayList<FlexiDTO> getSMSDTOs(LoginDTO login_dto, FlexiDTO f_dto, int flexiType) {
        ArrayList<FlexiDTO> data = new ArrayList<FlexiDTO>();
        DBConnection dbConnection = null;
        Statement stmt = null;
        long start = new GregorianCalendar(f_dto.getStartYear(), f_dto.getStartMonth() - 1, f_dto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        long end = new GregorianCalendar(f_dto.getEndYear(), f_dto.getEndMonth() - 1, f_dto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000 * (-1));
        String condition = " and operator_type_id = " + Constants.SMS + " and (inetload_time between " + start + " and " + end + ") ";
        String flexiTypeStr = null;
        if (flexiType != 5) {
            flexiTypeStr = "(" + flexiType + ")";
        } else {
            flexiTypeStr = "(1,2,3)";
        }
        if (f_dto.searchWithTransactionId) {
            condition += " and inetload_transaction_id like '" + f_dto.getTransactionId() + "%' ";
        }
        if (f_dto.searchWithRequestId) {
            condition += " and inetload_marker like '" + f_dto.getFlexiRequestId() + "%' ";
        }
        if (f_dto.searchWithPhoneNumber) {
            condition += " and inetload_phn_no like '" + f_dto.getPhoneNumber() + "%' ";
        }

        if (f_dto.searchWithAmount) {
            switch (f_dto.getSign()) {
                case Constants.BALANCE_EQUAL:
                    condition += " and inetload_amount=" + f_dto.getAmount() + " ";
                    break;
                case Constants.BALANCE_SMALLER_THAN:
                    condition += " and inetload_amount<" + f_dto.getAmount() + " ";
                    break;
                case Constants.BALANCE_GREATER_THAN:
                    condition += " and inetload_amount>" + f_dto.getAmount() + " ";
                    break;
                default:
                    break;
            }
        }

        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;
            } else if ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null) {
                condition += " and operator_type_id in(-1";
                if (userPerDTO.REFILL_SMS) {
                    condition += "," + Constants.SMS;
                }
                condition += ") ";
                sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;
            }

        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id in(" + list + ")" + condition;

        } else {
            sql = "select * from inetload_fdr where inetload_status in" + flexiTypeStr + " and inetload_to_user_id=" + login_dto.getId() + condition;
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ClientDTO cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("inetload_to_user_id"));
                if (f_dto.searchWithFromClientId && !cldto.getClientId().toLowerCase().startsWith(f_dto.getFromClientId())) {
                    continue;
                }
                FlexiDTO dto = new FlexiDTO();
                dto.setFlexiAgentID(rs.getLong("inetload_from_user_id"));
                dto.setFlexiRequestType(rs.getInt("inetload_status"));
                dto.setFlexiRequestTypeName(Constants.INETLOAD_STATUS_STRING[rs.getInt("inetload_status")]);
                dto.setFlexiFrom(cldto.getClientId());
                dto.setFlexiAmount(rs.getDouble("inetload_amount"));
                dto.setSurcharge(rs.getDouble("surcharge"));
                dto.setFlexiRequestId(rs.getString("inetload_marker"));
                dto.setFlexiTypeVal(rs.getInt("inetload_type_id"));
                dto.setFlexiOpType(rs.getInt("operator_type_id"));
                if (dto.getFlexiOpType() == Constants.BT) {
                    dto.setFlexiPhoneNo(rs.getString("inetload_phn_no") + " (" + Constants.ACCOUNT_TYPE[rs.getInt("account_type")] + ")");
                    dto.setFlexiType(Constants.TRANSFER_TYPE[rs.getInt("inetload_type_id")]);
                } else if (dto.getFlexiOpType() == Constants.SMS) {
                    dto.setFlexiPhoneNo(rs.getString("inetload_phn_no"));
                    dto.setFlexiType("SMS");
                } else {
                    dto.setFlexiPhoneNo(rs.getString("inetload_phn_no"));
                    dto.setFlexiType(Constants.REFILL_TYPE[rs.getInt("inetload_type_id")]);
                }
                dto.setFlexiRequestTimeVal(rs.getLong("inetload_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                if (rs.getLong("inetload_refill_success_time") > 0) {
                    dto.setFlexiSuccessTimeVal(rs.getLong("inetload_refill_success_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                }
                if (flexiType != 5) {
                    dto.setFlexiTransactionId(rs.getString("inetload_transaction_id"));
                } else {
                    APIDTO apiDTO = APILoader.getInstance().getApiDTOByID(rs.getLong("inetload_gw_id"));
                    if (apiDTO != null) {
                        dto.setFlexiTransactionId(apiDTO.getApiId());
                    } else {
                        dto.setFlexiTransactionId(rs.getString("inetload_transaction_id"));
                    }
                }
                dto.setFlexiMessage(rs.getString("inetload_message"));
                if ((dto.getFlexiTransactionId() == null || dto.getFlexiTransactionId().trim().length() == 0) && dto.getFlexiMessage() != null && dto.getFlexiMessage().trim().length() > 0) {
                    dto.setFlexiTransactionId("Message:");
                }
                if (dto.getFlexiMessage().length() > 15) {
                    dto.setFlexiMessage(dto.getFlexiMessage().substring(0, 15) + " ...");
                }
                dto.setFlexiCardID(rs.getLong("card_id"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in getFlexiDTOs: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public MyAppError addSMSRequest(LoginDTO login_dto, SMSDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        String marker = System.currentTimeMillis() + ":" + p_dto.getPhoneNumber();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String insertSQL = "insert into inetload_fdr(operator_type_id,inetload_type_id,inetload_phn_no,"
                    + "inetload_from_user_id,inetload_from_client_type_id,inetload_to_user_id,inetload_to_client_type_id,"
                    + "inetload_message,inetload_amount,inetload_time,inetload_marker,inetload_status) values";
            int fromTypeID = Constants.CLIENT_TYPE_AGENT;
            int toTypeID = fromTypeID;
            long fromID = login_dto.getId();
            long toID = fromID;
            ClientDTO clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getId());
            if ((clDTO.getClientCredit() - p_dto.getAmount()) < clDTO.getClientDeposit()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Your Credit Limit is Over.");
                return error;
            }

            insertSQL += "(" + p_dto.getOperatorTypeID() + "," + Constants.SMS + ",'" + p_dto.getPhoneNumber() + "'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ","
                    + "'" + p_dto.getMessage() + "'," + p_dto.getAmount() + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING + ")";

            clDTO = ClientLoader.getInstance().getClientDTOByID(login_dto.getClientParentId());
            while (clDTO != null) {
                if (clDTO.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Your parent is not active.");
                    return error;
                }
                if (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && clDTO.getClientDeposit() > 0 && (clDTO.getClientCredit() - p_dto.getAmount()) < clDTO.getClientDeposit()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Reseller Credit Limit is Over.");
                    return error;
                } else if (clDTO.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && (clDTO.getClientCredit() - p_dto.getAmount()) < clDTO.getClientDeposit()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Reseller Credit Limit is Over.");
                    return error;
                }

                toID = clDTO.getId();
                toTypeID = clDTO.getClientTypeId();
                insertSQL += ",(" + p_dto.getOperatorTypeID() + "," + Constants.SMS + ",'" + p_dto.getPhoneNumber() + "'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ",'"
                        + p_dto.getMessage() + "'," + p_dto.getAmount() + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING + ")";

                clDTO = ClientLoader.getInstance().getClientDTOByID(clDTO.getParentId());
            }
            stmt.execute(insertSQL);
            error.setErrorMessage(marker);

            //sendSMS(p_dto, marker); // Message Sent to API
        } catch (Exception ex) {
            logger.fatal("Error while adding flexi request: ", ex);
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error is Occured.");
            return error;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public static double getSMSRate(String countryCode) {
        double rate_amount = -1.0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select rate_amount from sms_rateplan where country_code='" + countryCode + "' and rate_status=1";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                rate_amount = rs.getDouble("rate_amount");
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in isDuplicateLimitFound: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rate_amount;
    }
}
