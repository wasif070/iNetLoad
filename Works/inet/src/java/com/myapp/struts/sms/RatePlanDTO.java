/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class RatePlanDTO {

    private int rateplan_id;
    private String country_name;
    private String country_code;
    private double rate_amount;
    private int rate_status;
    private String statusName;
    private ArrayList ratePlanList;
    public boolean searchWithCountryName=false;
    public boolean searchWithCountryCode=false;

    public RatePlanDTO() {
    }

    public int getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(int rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public double getRate_amount() {
        return rate_amount;
    }

    public void setRate_amount(double rate_amount) {
        this.rate_amount = rate_amount;
    }

    public int getRate_status() {
        return rate_status;
    }

    public void setRate_status(int rate_status) {
        this.rate_status = rate_status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    
}