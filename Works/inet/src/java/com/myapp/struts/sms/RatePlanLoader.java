/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author user
 */
public class RatePlanLoader {

    private static long LOADING_INTERVAL = 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, RatePlanDTO> ratePlanDTOById;
    private ArrayList<RatePlanDTO> ratePlanDTOList;
    static RatePlanLoader ratePlanLoader = null;
    static Logger logger = Logger.getLogger(RatePlanLoader.class.getName());

    public RatePlanLoader() {
        forceReload();
    }

    public static RatePlanLoader getInstance() {
        if (ratePlanLoader == null) {
            createScratchardLoader();
        }
        return ratePlanLoader;
    }

    private synchronized static void createScratchardLoader() {
        if (ratePlanLoader == null) {
            ratePlanLoader = new RatePlanLoader();
        }
    }

    private void reload() {
        ratePlanDTOList = new ArrayList<RatePlanDTO>();
        ratePlanDTOById = new HashMap<Integer, RatePlanDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from sms_rateplan;";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                RatePlanDTO dto = new RatePlanDTO();
                dto.setRateplan_id(rs.getInt("rateplan_id"));
                dto.setCountry_name(rs.getString("country_name"));
                dto.setCountry_code(rs.getString("country_code"));
                dto.setRate_amount(rs.getDouble("rate_amount"));

                dto.setRate_status(rs.getInt("rate_status"));

                if (dto.getRate_status() != -1) {
                    dto.setStatusName(Constants.LIVE_STATUS_STRING[rs.getInt("rate_status")]);
                    ratePlanDTOById.put(dto.getRateplan_id(), dto);
                    ratePlanDTOList.add(dto);
                }
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getRatePlanDTOList() {
        checkForReload();
        return ratePlanDTOList;
    }

    public synchronized RatePlanDTO getRatePlanDTOByID(int id) {
        checkForReload();
        return ratePlanDTOById.get(id);
    }
}
