/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author user
 */
public class RatePlanForm extends org.apache.struts.action.ActionForm {

    private int rateplan_id;
    private String country_name;
    private String country_code;
    private double rate_amount;
    private int rate_status;
    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private String message;
    private ArrayList ratePlanList;
    private long[] selectedIDs;

    public RatePlanForm() {
        super();
    }

    public int getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(int rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public ArrayList getRatePlanList() {
        return ratePlanList;
    }

    public void setRatePlanList(ArrayList ratePlanList) {
        this.ratePlanList = ratePlanList;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public double getRate_amount() {
        return rate_amount;
    }

    public void setRate_amount(double rate_amount) {
        this.rate_amount = rate_amount;
    }

    public int getRate_status() {
        return rate_status;
    }

    public void setRate_status(int rate_status) {
        this.rate_status = rate_status;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getCountry_name() == null || getCountry_name().length() == 0) {
                errors.add("country_name", new ActionMessage("errors.country_name.required"));
            }
            if (getCountry_code() == null || getCountry_code().length() == 0) {
                errors.add("country_code", new ActionMessage("errors.country_code.required"));
            }
            
            if (!Utils.isDouble(request.getParameter("rate_amount").toString())) {
                errors.add("rate_amount", new ActionMessage("errors.rate_amount.valid"));
            } else if (getRate_amount() <= 0.0) {
                errors.add("rate_amount", new ActionMessage("errors.rate_amount.required"));
            }
        }
        return errors;
    }
}
