/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.flexi.FlexiDAO;
import com.myapp.struts.flexi.FlexiDTO;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class SMSTaskScheduler {

    public SMSTaskScheduler() {
    }

    public MyAppError addRatePlan(RatePlanDTO scdto) {
        RatePlanDAO scratchcardDAO = new RatePlanDAO();
        return scratchcardDAO.addRatePlan(scdto);
    }

    public MyAppError editRatePlan(RatePlanDTO scdto) {
        RatePlanDAO scratchcardDAO = new RatePlanDAO();
        return scratchcardDAO.editRatePlan(scdto);
    }

    public MyAppError deleteRatePlans(String list) {
        RatePlanDAO ratePlanDAO = new RatePlanDAO();
        return ratePlanDAO.deleteRatePlans(list);
    }

    public MyAppError activateRatePlans(String list) {
        RatePlanDAO ratePlanDAO = new RatePlanDAO();
        return ratePlanDAO.activateRatePlans(list);
    }

    public MyAppError blockRatePlans(String list) {
        RatePlanDAO ratePlanDAO = new RatePlanDAO();
        return ratePlanDAO.blockRatePlans(list);
    }

    public ArrayList<RatePlanDTO> getRatePlanDTOsWithSearchParam(RatePlanDTO scdto) {
        RatePlanDAO ratePlanDAO = new RatePlanDAO();
        ArrayList<RatePlanDTO> list = RatePlanLoader.getInstance().getRatePlanDTOList();
        if (list != null) {
            return ratePlanDAO.getRatePlanWithSearchParam((ArrayList<RatePlanDTO>) list.clone(), scdto);
        }
        return null;
    }

    public ArrayList<RatePlanDTO> getRatePlanDTOsSorted() {
        RatePlanDAO ratePlanDAO = new RatePlanDAO();
        ArrayList<RatePlanDTO> list = RatePlanLoader.getInstance().getRatePlanDTOList();
        if (list != null) {
            return ratePlanDAO.getRatePlanSorted((ArrayList<RatePlanDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<RatePlanDTO> getRatePlanDTOs() {
        return RatePlanLoader.getInstance().getRatePlanDTOList();
    }

    public RatePlanDTO getRatePlanDTO(int id) {
        return RatePlanLoader.getInstance().getRatePlanDTOByID(id);
    }

    public ArrayList<FlexiDTO> getSMSDTOs(LoginDTO login_dto, FlexiDTO fdto, int flexiType) {
        SMSDAO flexiDAO = new SMSDAO();
        return flexiDAO.getSMSDTOs(login_dto, fdto, flexiType);
    }
    
     public MyAppError sendSMSRequest(LoginDTO login_dto, SMSDTO dto) {
        SMSDAO smsDAO = new SMSDAO();
        return smsDAO.addSMSRequest(login_dto, dto);
    }
}
