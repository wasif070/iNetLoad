/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class RatePlanList extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(RatePlanList.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        PermissionDTO userPerDTO = null;
        if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.SC) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            RatePlanForm rateplanForm = (RatePlanForm) form;
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            SMSTaskScheduler scheduler = new SMSTaskScheduler();

            int action = 0;
            if (request.getParameter("action") != null) {
                action = Integer.parseInt(request.getParameter("action"));
                String idList = "";
                if (action > Constants.UPDATE) {
                    if (rateplanForm.getSelectedIDs() != null) {
                        int length = rateplanForm.getSelectedIDs().length;
                        long[] idListArray = rateplanForm.getSelectedIDs();
                        if (length > 0) {
                            idList += idListArray[0];
                            for (int i = 1; i < length; i++) {
                                idList += "," + idListArray[i];
                            }
                            MyAppError error = new MyAppError();
                            switch (action) {
                                case Constants.ACTIVATION:
                                    if (userPerDTO != null && !userPerDTO.SC_EDIT) {
                                        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                        return (mapping.findForward("index"));
                                    }
                                    error = scheduler.activateRatePlans(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        rateplanForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        rateplanForm.setMessage(false, "Rate Plan are activated successfully.");
                                    }
                                    request.setAttribute(Constants.SCRATCH_CARD_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, rateplanForm.getMessage());

                                    break;
                                case Constants.BLOCK:
                                    if (userPerDTO != null && !userPerDTO.SC_EDIT) {
                                        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                        return (mapping.findForward("index"));
                                    }
                                    error = scheduler.blockRatePlans(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        rateplanForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        rateplanForm.setMessage(false, "Rate Plan are blocked successfully.");
                                    }
                                    request.setAttribute(Constants.SCRATCH_CARD_ID_LIST, "," + idList + ",");
                                    request.getSession(true).setAttribute(Constants.MESSAGE, rateplanForm.getMessage());
                                    break;
                                case Constants.DELETE:
                                    if (userPerDTO != null && !userPerDTO.SC_DELETE) {
                                        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                        return (mapping.findForward("index"));
                                    }
                                    error = scheduler.deleteRatePlans(idList);
                                    if (error.getErrorType() > 0) {
                                        target = "failure";
                                        rateplanForm.setMessage(true, error.getErrorMessage());
                                    } else {
                                        rateplanForm.setMessage(false, "Rate Plan are deleted successfully.");
                                    }
                                    request.getSession(true).setAttribute(Constants.MESSAGE, rateplanForm.getMessage());
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        rateplanForm.setMessage(true, "No Rate Plan is selected.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, rateplanForm.getMessage());
                    }
                }
            }

            if (list_all == 0) {
                if (rateplanForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.CONTACT_RECORD_PER_PAGE, rateplanForm.getRecordPerPage());
                }
                RatePlanDTO ratePlanDTO = new RatePlanDTO();
                if (rateplanForm.getCountry_name() != null && rateplanForm.getCountry_name().trim().length() > 0) {
                    ratePlanDTO.searchWithCountryName = true;
                    ratePlanDTO.setCountry_name(rateplanForm.getCountry_name().toLowerCase());
                }
                if (rateplanForm.getCountry_code() != null && rateplanForm.getCountry_code().trim().length() > 0) {
                    ratePlanDTO.searchWithCountryCode = true;
                    ratePlanDTO.setCountry_code(rateplanForm.getCountry_code().toLowerCase());
                }

                rateplanForm.setRatePlanList(scheduler.getRatePlanDTOsWithSearchParam(ratePlanDTO));
            } else if (list_all == 2) {
                if (action == Constants.ADD) {
                    rateplanForm.setMessage(false, "Rate Plan is added successfully!!!");
                } else if (action == Constants.EDIT) {
                    rateplanForm.setMessage(false, "Rate Plan is updated successfully.");
                }
                if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE) != null) {
                    rateplanForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE).toString()));
                }
                request.getSession(true).setAttribute(Constants.MESSAGE, rateplanForm.getMessage());
                rateplanForm.setRatePlanList(scheduler.getRatePlanDTOsSorted());
            } else {
                if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE) != null) {
                    rateplanForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE).toString()));
                }
                rateplanForm.setRatePlanList(scheduler.getRatePlanDTOs());
            }
            rateplanForm.setSelectedIDs(null);
            if (rateplanForm.getRatePlanList() != null && rateplanForm.getRatePlanList().size() > 0 && rateplanForm.getRatePlanList().size() <= (rateplanForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}