/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class EditRatePlan extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        RatePlanForm formBean = (RatePlanForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.SC_EDIT)) {
                request.setAttribute(mapping.getAttribute(), formBean);

                RatePlanDTO dto = new RatePlanDTO();
                SMSTaskScheduler scheduler = new SMSTaskScheduler();
                dto.setCountry_name(formBean.getCountry_name());
                dto.setCountry_code(formBean.getCountry_code());
                dto.setRate_amount(formBean.getRate_amount());
                dto.setRateplan_id(formBean.getRateplan_id());

                MyAppError error = new MyAppError();
                int id = 0;
                if (formBean.getRateplan_id() > 0) {
                    dto.setRateplan_id(formBean.getRateplan_id());
                }
                error = scheduler.editRatePlan(dto);

                if (error.getErrorType() == MyAppError.NoError) {
                    formBean.setMessage(false, "Rate Plan is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + request.getParameter("searchLink") + "&action=" + Constants.EDIT, true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return mapping.findForward(target);
    }
}
