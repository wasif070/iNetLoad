/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author user
 */
public class RatePlanDAO {

    static Logger logger = Logger.getLogger(RatePlanDAO.class.getName());

    public RatePlanDAO() {
    }

    public ArrayList<RatePlanDTO> getRatePlanWithSearchParam(ArrayList<RatePlanDTO> list, RatePlanDTO scdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RatePlanDTO dto = (RatePlanDTO) i.next();
                if ((scdto.searchWithCountryName && !dto.getCountry_name().toLowerCase().startsWith(scdto.getCountry_name()))
                        || (scdto.searchWithCountryCode && !dto.getCountry_code().toLowerCase().startsWith(scdto.getCountry_code()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<RatePlanDTO> getRatePlanSorted(ArrayList<RatePlanDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {
                public int compare(Object o1, Object o2) {
                    int val = 0;
                    RatePlanDTO dto1 = (RatePlanDTO) o1;
                    RatePlanDTO dto2 = (RatePlanDTO) o2;
                    if (dto1.getRateplan_id() < dto2.getRateplan_id()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addRatePlan(RatePlanDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select rateplan_id from sms_rateplan where country_code=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, dto.getCountry_code().trim());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Rate Card.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into sms_rateplan(country_name,country_code,rate_amount,rate_status) values(?,?,?,?);";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, dto.getCountry_name().trim());
            ps2.setString(2, dto.getCountry_code().trim());
            ps2.setDouble(3, dto.getRate_amount());
            ps2.setInt(4, 1);
            ps2.executeUpdate();
            RatePlanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding Rate request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editRatePlan(RatePlanDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select rateplan_id from sms_rateplan where country_code=? and rateplan_id!=" + dto.getRateplan_id();
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, dto.getCountry_code().trim());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Rate Plan.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update sms_rateplan set country_name=?, country_code=?, rate_amount=?  where rateplan_id=" + dto.getRateplan_id();
            ps2 = dbConnection.connection.prepareStatement(sql);
            logger.debug("SQL: " + sql);
            ps2.setString(1, dto.getCountry_name().trim());
            ps2.setString(2, dto.getCountry_code().trim());
            ps2.setDouble(3, dto.getRate_amount());
            ps2.executeUpdate();
            RatePlanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing Rate Plan request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateRatePlans(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update sms_rateplan set rate_status =" + Constants.USER_STATUS_ACTIVE + " where rateplan_id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RatePlanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while activating Rate Plan ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockRatePlans(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update sms_rateplan set rate_status =" + Constants.USER_STATUS_BLOCK + " where rateplan_id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RatePlanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blocking scratch card: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRatePlans(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update sms_rateplan set rate_status =-1 where rateplan_id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            RatePlanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting scratch card: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public double getSMSRate(String countryCode) {
        double rate_amount = -1.0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select rate_amount from sms_rateplan where country_code='" + countryCode + "' and rate_status=1";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                rate_amount = rs.getDouble("rate_amount");
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in isDuplicateLimitFound: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rate_amount;
    }
}
