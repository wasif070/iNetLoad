package com.myapp.struts.contactgroup;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class ContactGroupTaskScheduler {

    public ContactGroupTaskScheduler() {
    }

    public MyAppError addContactGroup(ContactGroupDTO cdto, LoginDTO ldto) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        return contactGroupDAO.addContactGroup(cdto,ldto);
    }

    public MyAppError editContactGroup(ContactGroupDTO cdto, LoginDTO ldto) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        return contactGroupDAO.editContactGroup(cdto,ldto);
    }

    public MyAppError deleteContactGroups(String list) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        return contactGroupDAO.deleteContactGroups(list);
    }

    public ArrayList<ContactGroupDTO> getContactGroupDTOsWithSearchParam(ContactGroupDTO gwdto, LoginDTO ldto) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        ArrayList<ContactGroupDTO> list = ContactGroupLoader.getInstance().getContactGroupDTOListByAgentId(ldto.getId());
        if (list != null) {
            return contactGroupDAO.getContactGroupDTOsWithSearchParam((ArrayList<ContactGroupDTO>) list.clone(), gwdto);
        }
        return null;
    }

    public ArrayList<ContactGroupDTO> getContactGroupDTOsSorted(LoginDTO ldto) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        ArrayList<ContactGroupDTO> list = ContactGroupLoader.getInstance().getContactGroupDTOListByAgentId(ldto.getId());   
        if (list != null) {
            return contactGroupDAO.getContactGroupDTOsSorted((ArrayList<ContactGroupDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<ContactGroupDTO> getContactGroupDTOs(LoginDTO ldto) {
        return ContactGroupLoader.getInstance().getContactGroupDTOListByAgentId(ldto.getId());
    }

    public ContactGroupDTO getContactGroupDTO(long id) {
        return ContactGroupLoader.getInstance().getContactGroupDTOByID(id);
    }
    
    public MyAppError addGroupSchedule(GroupScheduleDTO gsdto, LoginDTO ldto) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        return contactGroupDAO.addGroupSchedule(gsdto,ldto);
    }

    public MyAppError deleteGroupSchedules(String list) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        return contactGroupDAO.deleteGroupSchedule(list);
    }

    public ArrayList<GroupScheduleDTO> getGroupScheduleDTOsWithSearchParam(GroupScheduleDTO gsdto, LoginDTO ldto) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        ArrayList<GroupScheduleDTO> list = GroupScheduleLoader.getInstance().getGroupScheduleDTOsByGroupID(gsdto.getGroupID());
        if (list != null) {
            return contactGroupDAO.getGroupScheduleDTOsWithSearchParam((ArrayList<GroupScheduleDTO>) list.clone(), gsdto);
        }
        return null;
    }

    public ArrayList<GroupScheduleDTO> getGroupScheduleDTOsSorted(GroupScheduleDTO gsdto) {
        ContactGroupDAO contactGroupDAO = new ContactGroupDAO();
        ArrayList<GroupScheduleDTO> list = GroupScheduleLoader.getInstance().getGroupScheduleDTOsByGroupID(gsdto.getGroupID()); 
        if (list != null) {
            return contactGroupDAO.getGroupScheduleDTOsSorted((ArrayList<GroupScheduleDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<GroupScheduleDTO> getGroupScheduleDTOs(GroupScheduleDTO gsdto) {
        return GroupScheduleLoader.getInstance().getGroupScheduleDTOsByGroupID(gsdto.getGroupID());
    }    
}
