package com.myapp.struts.contactgroup;

public class ContactGroupDTO {
    public boolean searchWithGroupName = false;  
    public boolean searchWithAgentID = false;
    public boolean isDeleted = false;
    private int doValidation;
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long createdBy;
    private String groupDescription;
    private String groupNamePar;
    private String groupName;
    private String agentName;

    public ContactGroupDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }
    
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }  
    
    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }     

    public String getGroupNamePar() {
        return groupNamePar;
    }

    public void setGroupNamePar(String groupNamePar) {
        this.groupNamePar = groupNamePar;
    }
    
    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        if(isDeleted==1)
        {
           this.isDeleted = true;
        }
        else
        {
            this.isDeleted = false;
        }
    }    

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidation() {
        return doValidation;
    }

    public void setDoValidation(int doValidation) {
        this.doValidation = doValidation;
    }      
    
    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }      
}
