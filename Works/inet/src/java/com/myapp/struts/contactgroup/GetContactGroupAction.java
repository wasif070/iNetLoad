package com.myapp.struts.contactgroup;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetContactGroupAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ContactGroupForm formBean = (ContactGroupForm) form;
            if (!login_dto.getIsUser() && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                long id = Long.parseLong(request.getParameter("id"));
                ContactGroupDTO dto = new ContactGroupDTO();
                ContactGroupTaskScheduler scheduler = new ContactGroupTaskScheduler();
                dto = scheduler.getContactGroupDTO(id);

                if (dto != null) {

                    formBean.setId(dto.getId());
                    formBean.setGroupName(dto.getGroupName());
                    formBean.setGroupDescription(dto.getGroupDescription());
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
