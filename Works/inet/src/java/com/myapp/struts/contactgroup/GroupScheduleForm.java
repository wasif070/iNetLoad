package com.myapp.struts.contactgroup;

import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class GroupScheduleForm extends org.apache.struts.action.ActionForm {

    private int scheduleWeekDay;
    private int scheduleWeekDayPar;
    private int scheduleHour;
    private int scheduleMinute;
    private int scheduleType;
    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private long id;
    private long groupID;
    private long executedAt;
    private double scheduleAmount;
    private String description;
    private String groupName;
    private String message;
    private ArrayList scheduleList;
    private long[] selectedIDs;

    public GroupScheduleForm() {       
        super();
        scheduleWeekDayPar = -1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public long getExecutedAt() {
        return executedAt;
    }

    public void setExecutedAt(long executedAt) {
        this.executedAt = executedAt;
    }    

    public int getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(int scheduleType) {
        this.scheduleType = scheduleType;
    }

    public int getScheduleWeekDay() {
        return scheduleWeekDay;
    }

    public void setScheduleWeekDay(int scheduleWeekDay) {
        this.scheduleWeekDay = scheduleWeekDay;
    }

    public int getScheduleWeekDayPar() {
        return scheduleWeekDayPar;
    }

    public void setScheduleWeekDayPar(int scheduleWeekDayPar) {
        this.scheduleWeekDayPar = scheduleWeekDayPar;
    }

    public int getScheduleHour() {
        return scheduleHour;
    }

    public void setScheduleHour(int scheduleHour) {
        this.scheduleHour = scheduleHour;
    }

    public int getScheduleMinute() {
        return scheduleMinute;
    }

    public void setScheduleMinute(int scheduleMinute) {
        this.scheduleMinute = scheduleMinute;
    }

    public double getScheduleAmount() {
        return scheduleAmount;
    }

    public void setScheduleAmount(double scheduleAmount) {
        this.scheduleAmount = scheduleAmount;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public ArrayList getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(ArrayList scheduleList) {
        this.scheduleList = scheduleList;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getScheduleAmount() < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || getScheduleAmount() > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                errors.add("scheduleAmount", new ActionMessage("errors.refill_amount.validAmount"));
            }
        }
        return errors;
    }
}
