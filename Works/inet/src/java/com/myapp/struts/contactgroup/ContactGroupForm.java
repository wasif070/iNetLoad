package com.myapp.struts.contactgroup;

import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import java.util.ArrayList;

public class ContactGroupForm extends org.apache.struts.action.ActionForm {

    private int doValidate;
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long createdBy;
    private String groupDescription;
    private String groupNamePar;
    private String groupName;    
    private String message;
    private ArrayList groupList;
    private long[] selectedIDs;


    public ContactGroupForm() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }
    
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }    

    public String getGroupNamePar() {
        return groupNamePar;
    }

    public void setGroupNamePar(String groupNamePar) {
        this.groupNamePar = groupNamePar;
    }     

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }     
    
    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }    

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public ArrayList getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList groupList) {
        this.groupList = groupList;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getGroupName() == null || getGroupName().length() < 1) {
                errors.add("groupName", new ActionMessage("errors.group_name.required"));
            }             
        }
        return errors;
    }
}
