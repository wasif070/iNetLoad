package com.myapp.struts.contactgroup;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;

import com.myapp.struts.util.MyAppError;

public class AddContactGroupAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        ContactGroupForm formBean = (ContactGroupForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            if (!login_dto.getIsUser() && login_dto.getClientTypeId()==Constants.CLIENT_TYPE_AGENT) {
                request.setAttribute(mapping.getAttribute(), formBean);

                ContactGroupDTO dto = new ContactGroupDTO();
                ContactGroupTaskScheduler scheduler = new ContactGroupTaskScheduler();
                dto.setGroupName(formBean.getGroupName());
                dto.setGroupDescription(formBean.getGroupDescription());
                dto.setCreatedBy(login_dto.getId());

                MyAppError error = scheduler.addContactGroup(dto,login_dto);

                if (error.getErrorType() > 0) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else {
                    formBean.setMessage(false, "Contact Group is added successfully!!!");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?list_all=2&action=" + Constants.ADD, true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return mapping.findForward(target);
    }
}