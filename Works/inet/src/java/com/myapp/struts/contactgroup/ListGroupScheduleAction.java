package com.myapp.struts.contactgroup;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListGroupScheduleAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(ListGroupScheduleAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            GroupScheduleForm groupScheduleForm = (GroupScheduleForm) form;
            if (!login_dto.getIsUser() && (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) ) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }

                ContactGroupTaskScheduler scheduler = new ContactGroupTaskScheduler();

                int action = 0;
                if (request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        if (groupScheduleForm.getSelectedIDs() != null) {
                            int length = groupScheduleForm.getSelectedIDs().length;
                            long[] idListArray = groupScheduleForm.getSelectedIDs();
                            if (length > 0) {
                                idList += idListArray[0];
                                for (int i = 1; i < length; i++) {
                                    idList += "," + idListArray[i];
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.DELETE:
                                        error = scheduler.deleteGroupSchedules(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            groupScheduleForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            groupScheduleForm.setMessage(false, "Group schedules are deleted successfully!!!");
                                        }
                                        request.getSession(true).setAttribute(Constants.MESSAGE, groupScheduleForm.getMessage());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            groupScheduleForm.setMessage(true, "No group schedule is selected!!!");
                            request.getSession(true).setAttribute(Constants.MESSAGE, groupScheduleForm.getMessage());
                        }
                    }
                }
                GroupScheduleDTO gsDTO = new GroupScheduleDTO();
                gsDTO.setGroupID(Long.parseLong(request.getParameter("groupID")));              
                if (list_all == 0) {
                    if (groupScheduleForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.GROUP_SCHEDULING_RECORD_PER_PAGE, groupScheduleForm.getRecordPerPage());
                    }              
                    if (groupScheduleForm.getScheduleWeekDayPar() >= 0) {
                        gsDTO.searchWithScheduleWeekDay = true;
                        gsDTO.setScheduleWeekDay(groupScheduleForm.getScheduleWeekDayPar());
                    }                   
                    groupScheduleForm.setScheduleList(scheduler.getGroupScheduleDTOsWithSearchParam(gsDTO,login_dto));
                } else if (list_all == 2) {
                    if (request.getSession(true).getAttribute(Constants.GROUP_SCHEDULING_RECORD_PER_PAGE) != null) {
                        groupScheduleForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.GROUP_SCHEDULING_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, groupScheduleForm.getMessage());
                    groupScheduleForm.setScheduleList(scheduler.getGroupScheduleDTOsSorted(gsDTO));
                } else {
                    
                    if (request.getSession(true).getAttribute(Constants.GROUP_SCHEDULING_RECORD_PER_PAGE) != null) {
                        groupScheduleForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.GROUP_SCHEDULING_RECORD_PER_PAGE).toString()));
                    }
                    groupScheduleForm.setScheduleList(scheduler.getGroupScheduleDTOs(gsDTO));
                }
                groupScheduleForm.setSelectedIDs(null);
                if (groupScheduleForm.getScheduleList() != null && groupScheduleForm.getScheduleList().size() > 0 && groupScheduleForm.getScheduleList().size() <= (groupScheduleForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }  
        return (mapping.findForward(target));
    }
}
