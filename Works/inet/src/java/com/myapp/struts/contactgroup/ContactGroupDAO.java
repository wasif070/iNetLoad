package com.myapp.struts.contactgroup;

import com.myapp.struts.contacts.ContactLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.sql.Statement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class ContactGroupDAO {

    static Logger logger = Logger.getLogger(ContactGroupDAO.class.getName());

    public ContactGroupDAO() {
    }

    public ArrayList<ContactGroupDTO> getContactGroupDTOsWithSearchParam(ArrayList<ContactGroupDTO> list, ContactGroupDTO cdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ContactGroupDTO dto = (ContactGroupDTO) i.next();
                if ( (cdto.searchWithGroupName && !dto.getGroupName().toLowerCase().startsWith(cdto.getGroupName().toLowerCase()))
                        || (cdto.searchWithAgentID && dto.getCreatedBy() != cdto.getCreatedBy())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<ContactGroupDTO> getContactGroupDTOsSorted(ArrayList<ContactGroupDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ContactGroupDTO dto1 = (ContactGroupDTO) o1;
                    ContactGroupDTO dto2 = (ContactGroupDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addContactGroup(ContactGroupDTO dto, LoginDTO ldto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select group_name from contact_group where contact_group_delete=0 and group_created_by =? and group_name=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setLong(1, dto.getCreatedBy());
            ps1.setString(2, dto.getGroupName());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Contact Group.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            
            sql = "insert into contact_group(group_name,group_des,group_created_by) values(?,?,?);";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, dto.getGroupName());
            ps2.setString(2, dto.getGroupDescription());
            ps2.setLong(3, dto.getCreatedBy());
            ps2.executeUpdate();
            ContactGroupLoader.getInstance().forceReload();
            ContactLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding contact group request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }  
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }              
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    

    public MyAppError editContactGroup(ContactGroupDTO dto, LoginDTO ldto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select group_name from contact_group where contact_group_delete=0 and group_created_by =? and group_name=? and id!=" + dto.getId();
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setLong(1, dto.getCreatedBy());
            ps1.setString(2, dto.getGroupName());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Contact Group.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            
            sql = "update contact_group set group_name=?, group_des=? where contact_group_delete=0 and id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setString(1, dto.getGroupName());
            ps2.setString(2, dto.getGroupDescription());
            ps2.setLong(3, dto.getId());
            ps2.executeUpdate();
            ContactGroupLoader.getInstance().forceReload();
            ContactLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing contact group request: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }    
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }             
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

   
    public MyAppError deleteContactGroups(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        
        try {
            deleteContacts(list);
            deleteGroupScheduleByGroup(list);
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update contact_group set contact_group_delete=1 where contact_group_delete=0 and id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ContactGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting contact: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public MyAppError deleteContacts(String groupIDList) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update contact set contact_delete=1 where contact_delete=0 and contact_group_id in(" + groupIDList + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ContactLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting contact: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    } 
    
    public ArrayList<GroupScheduleDTO> getGroupScheduleDTOsWithSearchParam(ArrayList<GroupScheduleDTO> list, GroupScheduleDTO gsdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                GroupScheduleDTO dto = (GroupScheduleDTO) i.next();
                if (gsdto.searchWithScheduleWeekDay && dto.getScheduleWeekDay() != gsdto.getScheduleWeekDay()) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<GroupScheduleDTO> getGroupScheduleDTOsSorted(ArrayList<GroupScheduleDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    GroupScheduleDTO dto1 = (GroupScheduleDTO) o1;
                    GroupScheduleDTO dto2 = (GroupScheduleDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addGroupSchedule(GroupScheduleDTO dto, LoginDTO ldto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select id from contact_group_schedule where schedule_group_id =? and schedule_week_day=? and schedule_week_hour=? and schedule_week_minute=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setLong(1, dto.getGroupID());
            ps1.setInt(2, dto.getScheduleWeekDay());
            ps1.setInt(3, dto.getScheduleHour());
            ps1.setInt(4, dto.getScheduleMinute());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("There is a schdule for this group already in this time.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            
            sql = "insert into contact_group_schedule(schedule_group_id,schedule_week_day,schedule_week_hour,schedule_week_minute,schedule_type,schedule_amount) values(?,?,?,?,?,?);";
            ps2 = dbConnection.connection.prepareStatement(sql);

            ps2.setLong(1, dto.getGroupID());
            ps2.setInt(2, dto.getScheduleWeekDay());
            ps2.setInt(3, dto.getScheduleHour());
            ps2.setInt(4, dto.getScheduleMinute());
            ps2.setInt(5, dto.getScheduleType());
            ps2.setDouble(6, dto.getScheduleAmount());
            ps2.executeUpdate();
            GroupScheduleLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while addGroupSchedule: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            } 
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }             
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }    
    
    public MyAppError deleteGroupSchedule(String scheduleIDList) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from contact_group_schedule where id in(" + scheduleIDList + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            GroupScheduleLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting group schedule: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }  
    
    public MyAppError deleteGroupScheduleByGroup(String groupIDList) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from contact_group_schedule where schedule_group_id in(" + groupIDList + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            GroupScheduleLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting group schedule: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }    
}
