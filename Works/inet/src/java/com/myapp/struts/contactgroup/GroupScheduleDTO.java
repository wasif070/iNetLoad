package com.myapp.struts.contactgroup;

import com.myapp.struts.session.Constants;

public class GroupScheduleDTO {
    public boolean searchWithScheduleWeekDay = false;
    private int scheduleWeekDay;
    private int scheduleHour;
    private int scheduleMinute;
    private int scheduleType; 
    private String scheduleWeekDayStr;
    private String scheduleHourStr;
    private String scheduleMinuteStr;   
    private String scheduleTypeStr; 
    private String description;
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long groupID;
    private long executedAt;
    private double scheduleAmount;
    
    public GroupScheduleDTO() {
    }  

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }   
    
    public long getExecutedAt() {
        return executedAt;
    }

    public void setExecutedAt(long executedAt) {
        this.executedAt = executedAt;
    }      

    public int getScheduleType() {
        return scheduleType;
    }    

    public void setScheduleType(int scheduleType) {
        this.scheduleType = scheduleType;
    }
    
    public String getScheduleTypeStr() {
        return scheduleTypeStr;
    }
    
    public void setScheduleTypeStr() {
        this.scheduleTypeStr = Constants.SCHEDULE_TYPE[scheduleType];
    }       
    
    public int getScheduleWeekDay() {
        return scheduleWeekDay;
    }

    public void setScheduleWeekDay(int scheduleWeekDay) {
        this.scheduleWeekDay = scheduleWeekDay;
    }
    
    public String getScheduleWeekDayStr() {
        return scheduleWeekDayStr;
    }
    
    public void setScheduleWeekDayStr() {
        this.scheduleWeekDayStr = Constants.WEEK_DAY_STRING[scheduleWeekDay];
    }      
    
    public int getScheduleHour() {
        return scheduleHour;
    }

    public void setScheduleHour(int scheduleHour) {
        this.scheduleHour = scheduleHour;
    }
    
    public String getScheduleHourStr() {
        return scheduleHourStr;
    }
    
    public void setScheduleHourStr() {
        if(scheduleHour == 0)
        {
            this.scheduleHourStr = "12 AM";
        }    
        else if(scheduleHour >= 1 && scheduleHour <= 11)
        {
          this.scheduleHourStr = scheduleHour+ " AM";
        } 
        if(scheduleHour == 12)
        {
            this.scheduleHourStr = "12 PM";
        }                
        else if(scheduleHour >= 13 && scheduleHour<= 23)
        {
          this.scheduleHourStr = (scheduleHour-12) + " PM";
        }        
    }    
    
    public int getScheduleMinute() {
        return scheduleMinute;
    }

    public void setScheduleMinute(int scheduleMinute) {
        this.scheduleMinute = scheduleMinute;
    } 
    
    public String getScheduleMinuteStr() {
        return scheduleMinuteStr;
    }
    
    public void setScheduleMinuteStr() {
        if(scheduleMinute < 10)
        {
            this.scheduleMinuteStr = "0" + scheduleMinute;
        }    
        else
        {
          this.scheduleMinuteStr = "" + scheduleMinute;
        }      
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    

    public double getScheduleAmount() {
        return scheduleAmount;
    }

    public void setScheduleAmount(double scheduleAmount) {
        this.scheduleAmount = scheduleAmount;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }
    
}
