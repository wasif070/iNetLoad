package com.myapp.struts.contactgroup;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class GroupScheduleLoader {

    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, ArrayList> groupScheduleDTOsByGroupID;
    static GroupScheduleLoader groupScheduleLoader = null;
    static Logger logger = Logger.getLogger(GroupScheduleLoader.class.getName());

    public GroupScheduleLoader() {
        forceReload();
    }

    public static GroupScheduleLoader getInstance() {
        if (groupScheduleLoader == null) {
            createGroupScheduleLoader();
        }
        return groupScheduleLoader;
    }

    private synchronized static void createGroupScheduleLoader() {
        if (groupScheduleLoader == null) {
            groupScheduleLoader = new GroupScheduleLoader();
        }
    }

    private void reload() {
        groupScheduleDTOsByGroupID = new HashMap<Long, ArrayList>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from contact_group_schedule;";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                GroupScheduleDTO dto = new GroupScheduleDTO();
                dto.setId(rs.getLong("id"));
                dto.setGroupID(rs.getLong("schedule_group_id"));
                dto.setScheduleWeekDay(rs.getInt("schedule_week_day"));
                dto.setScheduleHour(rs.getInt("schedule_week_hour"));
                dto.setScheduleMinute(rs.getInt("schedule_week_minute"));
                dto.setScheduleType(rs.getInt("schedule_type"));
                dto.setScheduleAmount(rs.getDouble("schedule_amount"));
                dto.setDescription(rs.getString("schedule_description"));
                dto.setExecutedAt(rs.getLong("schedule_executedat"));
                dto.setScheduleWeekDayStr();
                dto.setScheduleHourStr();
                dto.setScheduleMinuteStr();
                dto.setScheduleTypeStr();
                ArrayList<GroupScheduleDTO> groupScheduleList = groupScheduleDTOsByGroupID.get(dto.getGroupID());
                if (groupScheduleList != null) {
                    groupScheduleList.add(dto);
                } else {
                    groupScheduleList = new ArrayList<GroupScheduleDTO>();
                    groupScheduleList.add(dto);
                    groupScheduleDTOsByGroupID.put(dto.getGroupID(), groupScheduleList);
                }                
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in GroupScheduleLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<GroupScheduleDTO> getGroupScheduleDTOsByGroupID(long groupID) {
        checkForReload();
        return groupScheduleDTOsByGroupID.get(groupID);
    }
}
