package com.myapp.struts.contactgroup;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;

import com.myapp.struts.util.MyAppError;

public class AddGroupScheduleAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        GroupScheduleForm formBean = (GroupScheduleForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            if (!login_dto.getIsUser() && login_dto.getClientTypeId()==Constants.CLIENT_TYPE_AGENT) {
                request.setAttribute(mapping.getAttribute(), formBean);

                GroupScheduleDTO dto = new GroupScheduleDTO();
                ContactGroupTaskScheduler scheduler = new ContactGroupTaskScheduler();
                dto.setGroupID(formBean.getGroupID());
                dto.setScheduleWeekDay(formBean.getScheduleWeekDay());
                dto.setScheduleHour(formBean.getScheduleHour());
                dto.setScheduleMinute(formBean.getScheduleMinute());
                dto.setScheduleAmount(formBean.getScheduleAmount());
                dto.setScheduleType(formBean.getScheduleType());

                MyAppError error = scheduler.addGroupSchedule(dto,login_dto);

                if (error.getErrorType() > 0) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else {
                    formBean.setMessage(false, "Group Schedule is added successfully!!!");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?list_all=2&action=" + Constants.ADD+"&groupID="+formBean.getGroupID()+"&groupName="+request.getParameter("groupName"), true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return mapping.findForward(target);
    }
}