package com.myapp.struts.contactgroup;

import com.myapp.struts.client.ClientLoader;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class ContactGroupLoader {

    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, ContactGroupDTO> contactGroupDTOsById;
    private HashMap<Long, ContactGroupDTO> allContactGroupDTOsById;
    private HashMap<Long, ArrayList<ContactGroupDTO>> contactGroupDTOListByAgentId;
    private HashMap<Long, ArrayList<ContactGroupDTO>> contactGroupDTOListByCreatedBy;
    static ContactGroupLoader contactGroupLoader = null;
    static Logger logger = Logger.getLogger(ContactGroupLoader.class.getName());

    public ContactGroupLoader() {
        forceReload();
    }

    public static ContactGroupLoader getInstance() {
        if (contactGroupLoader == null) {
            createContactGroupLoader();
        }
        return contactGroupLoader;
    }

    private synchronized static void createContactGroupLoader() {
        if (contactGroupLoader == null) {
            contactGroupLoader = new ContactGroupLoader();
        }
    }

    private void reload() {
        contactGroupDTOsById = new HashMap<Long, ContactGroupDTO>();
        allContactGroupDTOsById = new HashMap<Long, ContactGroupDTO>();
        contactGroupDTOListByAgentId = new HashMap<Long, ArrayList<ContactGroupDTO>>();
        contactGroupDTOListByCreatedBy = new HashMap<Long, ArrayList<ContactGroupDTO>>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from contact_group;";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                ContactGroupDTO dto = new ContactGroupDTO();
                dto.setId(rs.getLong("id"));
                dto.setGroupName(rs.getString("group_name"));
                dto.setGroupDescription(rs.getString("group_des"));
                dto.setCreatedBy(rs.getLong("group_created_by"));
                dto.setIsDeleted(rs.getInt("contact_group_delete"));
                if (ClientLoader.getInstance().getClientDTOByID(dto.getCreatedBy()) != null) {
                    dto.setAgentName(ClientLoader.getInstance().getClientDTOByID(dto.getCreatedBy()).getClientId());
                } else {
                    continue;
                }
                if (!dto.getIsDeleted()) {
                    contactGroupDTOsById.put(dto.getId(), dto);
                    ArrayList<ContactGroupDTO> contactGroupList1 = contactGroupDTOListByAgentId.get(dto.getCreatedBy());
                    if (contactGroupList1 != null) {
                        contactGroupList1.add(dto);
                    } else {
                        contactGroupList1 = new ArrayList<ContactGroupDTO>();
                        contactGroupList1.add(dto);
                        contactGroupDTOListByAgentId.put(dto.getCreatedBy(), contactGroupList1);
                    }
                    ArrayList<ContactGroupDTO> contactGroupList2 = contactGroupDTOListByCreatedBy.get(dto.getCreatedBy());
                    if (contactGroupList2 != null) {
                        contactGroupList2.add(dto);
                    } else {
                        contactGroupList2 = new ArrayList<ContactGroupDTO>();
                        contactGroupList2.add(dto);
                        contactGroupDTOListByCreatedBy.put(dto.getCreatedBy(), contactGroupList2);
                    }
                }
                allContactGroupDTOsById.put(dto.getId(), dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in ContactGroupLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ContactGroupDTO getContactGroupDTOByID(long id) {
        checkForReload();
        return contactGroupDTOsById.get(id);
    }
    
    public synchronized ContactGroupDTO getAllContactGroupDTOByID(long id) {
        checkForReload();
        return allContactGroupDTOsById.get(id);
    }    

    public synchronized ArrayList<ContactGroupDTO> getContactGroupDTOListByAgentId(long agentID) {
        checkForReload();
        return contactGroupDTOListByAgentId.get(agentID);
    }

    public synchronized ArrayList<ContactGroupDTO> getContactGroupDTOListByCreatedBy(long resID) {
        checkForReload();
        return contactGroupDTOListByCreatedBy.get(resID);
    }
}
