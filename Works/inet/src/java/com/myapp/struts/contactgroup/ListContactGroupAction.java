package com.myapp.struts.contactgroup;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListContactGroupAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(ListContactGroupAction.class.getName());
    static SimpleDateFormat bangDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ContactGroupForm contactGroupForm = (ContactGroupForm) form;
            if (!login_dto.getIsUser() && (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) ) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }

                ContactGroupTaskScheduler scheduler = new ContactGroupTaskScheduler();

                int action = 0;
                if (request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        if (contactGroupForm.getSelectedIDs() != null) {
                            int length = contactGroupForm.getSelectedIDs().length;
                            long[] idListArray = contactGroupForm.getSelectedIDs();
                            if (length > 0) {
                                idList += idListArray[0];
                                for (int i = 1; i < length; i++) {
                                    idList += "," + idListArray[i];
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.DELETE:
                                        error = scheduler.deleteContactGroups(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            contactGroupForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            contactGroupForm.setMessage(false, "Contact groups are deleted successfully!!!");
                                        }
                                        request.getSession(true).setAttribute(Constants.MESSAGE, contactGroupForm.getMessage());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            contactGroupForm.setMessage(true, "No contact group is selected!!!");
                            request.getSession(true).setAttribute(Constants.MESSAGE, contactGroupForm.getMessage());
                        }
                    }
                }

                if (list_all == 0) {
                    if (contactGroupForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.CONTACT_GROUP_RECORD_PER_PAGE, contactGroupForm.getRecordPerPage());
                    }
                    ContactGroupDTO conatactgroupdto = new ContactGroupDTO();
                    if (contactGroupForm.getGroupNamePar() != null && contactGroupForm.getGroupNamePar().trim().length() > 0) {
                        conatactgroupdto.searchWithGroupName = true;
                        conatactgroupdto.setGroupName(contactGroupForm.getGroupNamePar().toLowerCase());
                    }                    
                    contactGroupForm.setGroupList(scheduler.getContactGroupDTOsWithSearchParam(conatactgroupdto,login_dto));
                } else if (list_all == 2) {
                    if (action == Constants.EDIT) {
                        contactGroupForm.setMessage(false, "Contact Group is updated successfully!!!");
                    }
                    if (request.getSession(true).getAttribute(Constants.CONTACT_GROUP_RECORD_PER_PAGE) != null) {
                        contactGroupForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CONTACT_GROUP_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, contactGroupForm.getMessage());
                    contactGroupForm.setGroupList(scheduler.getContactGroupDTOsSorted(login_dto));
                } else {
                    if (request.getSession(true).getAttribute(Constants.CONTACT_GROUP_RECORD_PER_PAGE) != null) {
                        contactGroupForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CONTACT_GROUP_RECORD_PER_PAGE).toString()));
                    }
                    contactGroupForm.setGroupList(scheduler.getContactGroupDTOs(login_dto));
                }
                contactGroupForm.setSelectedIDs(null);
                if (contactGroupForm.getGroupList() != null && contactGroupForm.getGroupList().size() > 0 && contactGroupForm.getGroupList().size() <= (contactGroupForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }  
        return (mapping.findForward(target));
    }
}
