package com.myapp.struts.client;

import com.myapp.struts.contactgroup.ContactGroupLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.payment.PaymentDAO;
import com.myapp.struts.payment.PaymentDTO;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;
import sendmail.SimpleMail;

public class ClientDAO {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ClientDAO() {
    }

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ArrayList<ClientDTO> list, ClientDTO cldto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ClientDTO dto = (ClientDTO) i.next();
                if ((cldto.searchWithClientID && !dto.getClientId().toLowerCase().startsWith(cldto.getClientId()))
                        || (cldto.searchWithType && dto.getClientTypeId() != cldto.getClientTypeId())
                        || (cldto.searchWithStatus && dto.getClientStatus() != cldto.getClientStatus())
                        || (cldto.searchWithClientANI && !dto.getClientAni().toLowerCase().startsWith(cldto.getClientAni()))) {
                    continue;
                }

                switch (cldto.getSign()) {
                    case Constants.BALANCE_EQUAL:
                        if (dto.getClientCredit() == cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    case Constants.BALANCE_SMALLER_THAN:
                        if (dto.getClientCredit() < cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    case Constants.BALANCE_GREATER_THAN:
                        if (dto.getClientCredit() > cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    default:
                        newList.add(dto);
                        break;
                }
            }
        }
        return newList;
    }

    public ArrayList<ClientDTO> getClientDTOsSorted(ArrayList<ClientDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {
                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public ArrayList<ClientDTO> getSpecialClientDTOsWithSearchParam(ArrayList<ClientDTO> list, ClientDTO cldto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ClientDTO dto = (ClientDTO) i.next();
                if ((cldto.searchWithClientID && !dto.getClientId().toLowerCase().startsWith(cldto.getClientId()))
                        || (cldto.searchWithStatus && dto.getClientStatus() != cldto.getClientStatus())) {
                    continue;
                }

                switch (cldto.getSign()) {
                    case Constants.BALANCE_EQUAL:
                        if (dto.getClientCredit() == cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    case Constants.BALANCE_SMALLER_THAN:
                        if (dto.getClientCredit() < cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    case Constants.BALANCE_GREATER_THAN:
                        if (dto.getClientCredit() > cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    default:
                        newList.add(dto);
                        break;
                }
            }
        }
        return newList;
    }

    public ArrayList<ClientDTO> getSpecialClientDTOsSorted(ArrayList<ClientDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {
                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public ArrayList<ClientDTO> getClientDTOsSortedByClientID(ArrayList<ClientDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {
                public int compare(Object o1, Object o2) {
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    return dto1.getClientId().compareToIgnoreCase(dto2.getClientId());
                }
            });
        }
        return list;
    }

    public MyAppError addClientInformation(ClientDTO cl_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        PaymentDAO dao = new PaymentDAO();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select user_id from user where user_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, cl_dto.getClientId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_deleted=0 and client_id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, cl_dto.getClientId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();


            if (cl_dto.getParentId() == 0 || getClientCredit(cl_dto.getParentId()) >= cl_dto.getClientCredit()) {
                sql = "insert into client (client_id,client_type_id,client_parent_id,client_full_name,client_email,client_password,client_status,client_reg_time,client_ip,client_discount,client_deposit,client_version,client_ani,client_sms_sender) ";
                sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps3 = dbConnection.connection.prepareStatement(sql);

                ps3.setString(1, cl_dto.getClientId());
                ps3.setInt(2, cl_dto.getClientTypeId());
                ps3.setLong(3, cl_dto.getParentId());
                ps3.setString(4, cl_dto.getClientName());
                ps3.setString(5, cl_dto.getClientEmail());
                ps3.setString(6, cl_dto.getClientPassword());
                ps3.setInt(7, cl_dto.getClientStatus());
                ps3.setLong(8, System.currentTimeMillis());
                if (cl_dto.getClientIp() != null && cl_dto.getClientIp().trim().length() > 0) {
                    ps3.setString(9, cl_dto.getClientIp());
                } else {
                    ps3.setString(9, "");
                }
                ps3.setDouble(10, cl_dto.getClientDiscount());
                ps3.setDouble(11, cl_dto.getClientDeposit());
                ps3.setInt(12, cl_dto.getClientVersion());
                if (cl_dto.getClientAni() != null && cl_dto.getClientAni().trim().length() > 0) {
                    ps3.setString(13, cl_dto.getClientAni());
                } else {
                    ps3.setString(13, "");
                }
                ps3.setString(14, cl_dto.getSmsSender());
                ps3.executeUpdate();
                ClientLoader.getInstance().forceReload();
                if (cl_dto.getClientCredit() != 0.0) {
                    sql = "select id from client where client_id='" + cl_dto.getClientId() + "'";
                    Statement stmt = dbConnection.connection.createStatement();
                    ResultSet rs = stmt.executeQuery(sql);

                    if (rs.next()) {
                        PaymentDTO dto = new PaymentDTO();
                        if (l_dto.getIsUser()) {
                            if (cl_dto.getParentId() > Constants.ROOT_RESELLER_PARENT_ID) {
                                dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(cl_dto.getParentId()).getClientTypeId());
                                dto.setPaymentFromClientId(cl_dto.getParentId());
                            } else {
                                dto.setPaymentFromClientTypeId(Constants.USER_TYPE);
                                dto.setPaymentFromClientId(Constants.ROOT_RESELLER_PARENT_ID);
                            }
                        } else {
                            dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(cl_dto.getParentId()).getClientTypeId());
                            dto.setPaymentFromClientId(cl_dto.getParentId());
                        }

                        dto.setPaymentToClientTypeId(cl_dto.getClientTypeId());
                        dto.setPaymentToClientId(rs.getLong("id"));
                        dto.setPaymentTypeId(Constants.PAYMENT_TYPE_INITIAL);
                        dto.setPaymentAmount(cl_dto.getClientCredit());
                        dto.setPaymentDescription(Constants.INITIAL_CREDIT);
                        dto.setPaymentDiscount((dto.getPaymentAmount() * cl_dto.getClientDiscount()) / 100.00);
                        error = dao.addRechargePaymentRequest(dto);
                    }
                    rs.close();
                }
            } else {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Insufficient Parent Balance.");
                return error;
            }
        } catch (Exception ex) {
            logger.fatal("Error while adding client: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addSpecialAgentInformation(ClientDTO cl_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select user_id from user where user_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, cl_dto.getClientId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_deleted=0 and client_id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, cl_dto.getClientId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into client (client_id,client_type_id,client_parent_id,client_full_name,client_email,client_password,client_status,client_reg_time,client_ip) ";
            sql += "values(?,?,?,?,?,?,?,?,?)";
            ps3 = dbConnection.connection.prepareStatement(sql);

            ps3.setString(1, cl_dto.getClientId());
            ps3.setInt(2, Constants.CLIENT_TYPE_AGENT);
            ps3.setLong(3, Constants.ROOT_RESELLER_PARENT_ID);
            ps3.setString(4, cl_dto.getClientName());
            ps3.setString(5, cl_dto.getClientEmail());
            ps3.setString(6, cl_dto.getClientPassword());
            ps3.setInt(7, Constants.USER_STATUS_ACTIVE);
            ps3.setLong(8, System.currentTimeMillis());
            ps3.setString(9, "");
            ps3.executeUpdate();
            ClientLoader.getInstance().forceReload();
            try {
                SimpleMail.sendMail(SettingsLoader.getInstance().getSettingsDTO().getContactEmail(), SettingsLoader.getInstance().getSettingsDTO().getBrandName(), cl_dto.getClientEmail(), "Your Special Agent Account Info",
                        "Your registration for special agent is successful. Your Client ID: " + cl_dto.getClientId() + " and Password: " + cl_dto.getClientPassword(), null);
            } catch (Exception e) {
                logger.fatal("Exception in sendingg mail:" + e);
            }
        } catch (Exception ex) {
            logger.fatal("Error while adding client: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editClientInformation(ClientDTO cl_dto, LoginDTO login_dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        PreparedStatement ps4 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select user_id from user where user_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, cl_dto.getClientId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_deleted=0 and client_id=? and id!=" + cl_dto.getId();
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, cl_dto.getClientId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            if (login_dto.getIsUser() || cl_dto.getId() != login_dto.getId()) {
                sql = "update client set client_id=?,client_full_name=?,client_email=?,client_password=?,client_ip=?,client_discount=?,client_deposit=?,client_ani=?,client_sms_sender=? where id=" + cl_dto.getId();
                ps3 = dbConnection.connection.prepareStatement(sql);

                ps3.setString(1, cl_dto.getClientId());
                ps3.setString(2, cl_dto.getClientName());
                ps3.setString(3, cl_dto.getClientEmail());
                ps3.setString(4, cl_dto.getClientPassword());
                if (cl_dto.getClientIp() != null && cl_dto.getClientIp().trim().length() > 0) {
                    ps3.setString(5, cl_dto.getClientIp());
                } else {
                    ps3.setString(5, "");
                }
                ps3.setDouble(6, cl_dto.getClientDiscount());
                ps3.setDouble(7, cl_dto.getClientDeposit());
                if (cl_dto.getClientAni() != null && cl_dto.getClientAni().trim().length() > 0) {
                    ps3.setString(8, cl_dto.getClientAni());
                } else {
                    ps3.setString(8, "");
                }
                ps3.setString(9, cl_dto.getSmsSender());
                ps3.executeUpdate();
                String list = "" + cl_dto.getId();
                if (cl_dto.getClientStatus() != ClientLoader.getInstance().getClientDTOByID(cl_dto.getId()).getClientStatus()) {
                    switch (cl_dto.getClientStatus()) {
                        case Constants.USER_STATUS_ACTIVE:
                            activateClients(list);
                            break;
                        case Constants.USER_STATUS_INACTIVE:
                            deactivateClients(list);
                            break;
                        case Constants.USER_STATUS_BLOCK:
                            blockClients(list);
                            break;
                        default:
                            break;
                    }
                }
                setOperators(list, cl_dto);
            } else {
                sql = "update client set client_id=?,client_full_name=?,client_email=? where id=" + cl_dto.getId();
                ps4 = dbConnection.connection.prepareStatement(sql);

                ps4.setString(1, cl_dto.getClientId());
                ps4.setString(2, cl_dto.getClientName());
                ps4.setString(3, cl_dto.getClientEmail());
                ps4.executeUpdate();
            }

            ClientLoader.getInstance().forceReload();
            if (!login_dto.getIsUser() && cl_dto.getId() == login_dto.getId()) {
                login_dto.setClientId(cl_dto.getClientId());
            }
        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps4 != null) {
                    ps4.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public double getClientCredit(long id) {
        String sql = "select client_credit_all from client where id=" + id;
        double credit = 0.0;
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                credit = resultSet.getDouble("client_credit_all");
            }
            resultSet.close();
        } catch (Exception ex) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return credit;
    }

    public double[] getBalanceByParent(String parent) {
        double[] balance = new double[2];
        ArrayList<ClientDTO> allClientList = ClientLoader.getInstance().getClientDTOList();
        String parentChildrenList = "," + parent + ",";
        balance[0] = 0.0;
        balance[1] = 0.0;
        if (allClientList != null) {
            for (int i = 0; i < allClientList.size(); i++) {
                ClientDTO dto = allClientList.get(i);
                if (parentChildrenList.contains("," + dto.getParentId() + ",")) {
                    parentChildrenList += dto.getId() + ",";
                    balance[0] += dto.getClientPreviousBalance();
                    balance[1] += dto.getClientCredit();
                }
            }
        }
        return balance;
    }

    public String getParentChildrenList(String parentList) {
        ArrayList<ClientDTO> allClientList = ClientLoader.getInstance().getClientDTOList();
        String parentChildrenList = "," + parentList + ",";
        if (allClientList != null) {
            for (int i = 0; i < allClientList.size(); i++) {
                ClientDTO dto = allClientList.get(i);
                if (parentChildrenList.contains("," + dto.getParentId() + ",")) {
                    parentChildrenList += dto.getId() + ",";
                }
            }
        }
        return parentChildrenList.substring(parentChildrenList.indexOf(",") + 1, parentChildrenList.lastIndexOf(","));
    }

    public MyAppError setOperators(String list, ClientDTO dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update client set client_gp_active=" + dto.clientGPActive
                    + ",client_rb_active=" + dto.clientRBActive
                    + ",client_bl_active=" + dto.clientBLActive
                    + ",client_tt_active=" + dto.clientTTActive
                    + ",client_cc_active=" + dto.clientCCActive
                    + ",client_wr_active=" + dto.clientWRActive
                    + ",client_bt_active=" + dto.clientBTActive
                    + ",client_sms_active=" + dto.clientSMSActive
                    + " where id in(" + getParentChildrenList(list) + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating user status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deactivateClients(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update client set client_status=" + Constants.USER_STATUS_INACTIVE + " where id in(" + getParentChildrenList(list) + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating user status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateClients(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update client set client_status = " + Constants.USER_STATUS_ACTIVE + " where id in(" + getParentChildrenList(list) + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating user status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockClients(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update client set client_status=" + Constants.USER_STATUS_BLOCK + " where id in(" + getParentChildrenList(list) + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating user status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteClients(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String clList = getParentChildrenList(list);
            String sql = "select * from client where id in(" + clList + ") and client_type_id in(" + Constants.CLIENT_TYPE_RESELLER + "," + Constants.CLIENT_TYPE_AGENT + ") and client_credit_all > 0";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Please return from clients first.");
                rs.close();
                return error;
            }
            rs.close();
            sql = "update client set client_deleted=1, client_id=concat(client_id,' (X)') where id in(" + clList + ");";
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
            sql = "delete from contact_group where group_created_by in(" + clList + ");";
            stmt.execute(sql);
            sql = "delete from contact where contact_created_by in(" + clList + ");";
            stmt.execute(sql);
            ContactGroupLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting client: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError rechargeClients(LoginDTO login_dto, long[] idList, double[] amountList, String[] descriptionList) {
        MyAppError error = new MyAppError();
        PaymentDAO dao = new PaymentDAO();
        for (int i = 0; i < idList.length; i++) {
            PaymentDTO dto = new PaymentDTO();
            ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(idList[i]);
            if (login_dto.getIsUser()) {
                if (clientDTO.getParentId() > Constants.ROOT_RESELLER_PARENT_ID) {
                    dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
                    dto.setPaymentFromClientId(clientDTO.getParentId());
                } else {
                    dto.setPaymentFromClientTypeId(Constants.USER_TYPE);
                    dto.setPaymentFromClientId(Constants.ROOT_RESELLER_PARENT_ID);
                }
            } else {
                dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
                dto.setPaymentFromClientId(clientDTO.getParentId());
            }

            dto.setPaymentToClientTypeId(clientDTO.getClientTypeId());
            dto.setPaymentToClientId(idList[i]);
            dto.setPaymentTypeId(Constants.PAYMENT_TYPE_PAID);
            dto.setPaymentAmount(amountList[i]);
            dto.setPaymentDiscount((dto.getPaymentAmount() * clientDTO.getClientDiscount()) / 100.00);
            dto.setPaymentDescription(descriptionList[i]);
            error = dao.addRechargePaymentRequest(dto);
            if (error.getErrorType() != MyAppError.NoError) {
                return error;
            }
        }
        return error;
    }

    public MyAppError returnClients(LoginDTO login_dto, long[] idList, double[] amountList, String[] descriptionList) {
        MyAppError error = new MyAppError();
        PaymentDAO dao = new PaymentDAO();
        for (int i = 0; i < idList.length; i++) {
            PaymentDTO dto = new PaymentDTO();
            ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(idList[i]);
            if (login_dto.getIsUser()) {
                if (clientDTO.getParentId() > Constants.ROOT_RESELLER_PARENT_ID) {
                    dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
                    dto.setPaymentFromClientId(clientDTO.getParentId());
                } else {
                    dto.setPaymentFromClientTypeId(Constants.USER_TYPE);
                    dto.setPaymentFromClientId(Constants.ROOT_RESELLER_PARENT_ID);
                }
            } else {
                dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
                dto.setPaymentFromClientId(clientDTO.getParentId());
            }

            dto.setPaymentToClientTypeId(clientDTO.getClientTypeId());
            dto.setPaymentToClientId(idList[i]);
            dto.setPaymentTypeId(Constants.PAYMENT_TYPE_RETURNED);
            dto.setPaymentAmount(amountList[i]);
            dto.setPaymentDescription(descriptionList[i]);
            error = dao.addReturnPaymentRequest(dto);
            if (error.getErrorType() != MyAppError.NoError) {
                return error;
            }
        }
        return error;
    }

    public MyAppError receiveClients(LoginDTO login_dto, long[] idList, double[] amountList, String[] descriptionList) {
        MyAppError error = new MyAppError();
        PaymentDAO dao = new PaymentDAO();
        for (int i = 0; i < idList.length; i++) {
            PaymentDTO dto = new PaymentDTO();
            ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(idList[i]);
            if (login_dto.getIsUser()) {
                if (clientDTO.getParentId() > Constants.ROOT_RESELLER_PARENT_ID) {
                    dto.setPaymentToClientTypeId(Constants.CLIENT_TYPE_RESELLER);
                    dto.setPaymentToClientId(clientDTO.getParentId());
                } else {
                    dto.setPaymentToClientTypeId(Constants.USER_TYPE);
                    dto.setPaymentToClientId(Constants.ROOT_RESELLER_PARENT_ID);
                }
            } else {
                dto.setPaymentToClientTypeId(Constants.CLIENT_TYPE_RESELLER);
                dto.setPaymentToClientId(clientDTO.getParentId());
            }

            dto.setPaymentFromClientTypeId(clientDTO.getClientTypeId());
            dto.setPaymentFromClientId(idList[i]);
            if (amountList[i] < 0) {
                dto.setPaymentTypeId(Constants.PAYMENT_TYPE_CANCELLED);
            } else {
                dto.setPaymentTypeId(Constants.PAYMENT_TYPE_RECEIVED);
            }
            dto.setPaymentAmount(amountList[i]);
            dto.setPaymentDescription(descriptionList[i]);

            error = dao.addReceivePaymentRequest(dto);
            if (error.getErrorType() != MyAppError.NoError) {
                return error;
            }
        }
        return error;
    }

    public long countPendingRequests(LoginDTO login_dto) {
        long totalPendingReqequests = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                sql = "select count(*) as total from inetload_fdr where operator_type_id not in(" + Constants.SMS + "," + Constants.BT + ") and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
            } else if ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null) {
                String condition = " operator_type_id not in(" + Constants.SMS + "," + Constants.BT + ") and operator_type_id in(-1";
                if (userPerDTO.REFILL_GP) {
                    condition += "," + Constants.GP;
                }
                if (userPerDTO.REFILL_BL) {
                    condition += "," + Constants.BL;
                }
                if (userPerDTO.REFILL_RB) {
                    condition += "," + Constants.RB;
                }
                if (userPerDTO.REFILL_CC) {
                    condition += "," + Constants.CC;
                }
                if (userPerDTO.REFILL_TT) {
                    condition += "," + Constants.TT;
                }
                if (userPerDTO.REFILL_WR) {
                    condition += "," + Constants.WR;
                }
                if (userPerDTO.REFILL_SMS) {
                    condition += "," + Constants.SMS;
                }
                if (userPerDTO.REFILL_BT) {
                    condition += "," + Constants.BT;
                }
                if (userPerDTO.REFILL_CARD) {
                    condition += "," + Constants.CARD;
                }
                condition += ") ";
                sql = "select count(*) as total from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")" + condition;
            }

        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select count(*) as total from inetload_fdr where operator_type_id not in(" + Constants.SMS + "," + Constants.BT + ") and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
        } else {
            sql = "select count(*) as total from inetload_fdr where operator_type_id not in(" + Constants.SMS + "," + Constants.BT + ") and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id=" + login_dto.getId();
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                totalPendingReqequests = rs.getLong("total");
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in countPendingRequests: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalPendingReqequests;
    }

    public long countPendingBalanceRequests(LoginDTO login_dto) {
        long totalPendingReqequests = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                sql = "select count(*) as total from inetload_fdr where operator_type_id=" + Constants.BT + " and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
            } else if ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null) {
                String condition = " and operator_type_id=" + Constants.BT + "  and operator_type_id in(-1";
                if (userPerDTO.REFILL_BT) {
                    condition += "," + Constants.BT;
                }
                condition += ") ";
                sql = "select count(*) as total from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")" + condition;
            }

        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select count(*) as total from inetload_fdr where operator_type_id=" + Constants.BT + " and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
        } else {
            sql = "select count(*) as total from inetload_fdr where operator_type_id=" + Constants.BT + " and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id=" + login_dto.getId();
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                totalPendingReqequests = rs.getLong("total");
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in countPendingRequests: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalPendingReqequests;
    }

    public long countPendingSMSRequests(LoginDTO login_dto) {
        long totalPendingReqequests = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                sql = "select count(*) as total from inetload_fdr where operator_type_id=" + Constants.SMS + " and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
            } else if ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null) {
                String condition = " and operator_type_id=" + Constants.SMS + "  and operator_type_id in(-1";
                if (userPerDTO.REFILL_SMS) {
                    condition += "," + Constants.SMS;
                }
                condition += ") ";
                sql = "select count(*) as total from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")" + condition;
            }

        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select count(*) as total from inetload_fdr where operator_type_id=" + Constants.SMS + " and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
        } else {
            sql = "select count(*) as total from inetload_fdr where operator_type_id=" + Constants.SMS + " and inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id=" + login_dto.getId();
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                totalPendingReqequests = rs.getLong("total");
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in countPendingRequests: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalPendingReqequests;
    }
    
    public ArrayList<PaymentDTO> getSummeryDTOs(LoginDTO login_dto) {
        HashMap map = new HashMap();
        ArrayList<PaymentDTO> data = null;
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + ")";
            if (!login_dto.getIsUser() && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                sql = "select * from payment where payment_to_client_id=" + id
                        + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + ")";
            }
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                ClientDTO clDTO = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("payment_to_client_id"));
                if (clDTO != null) {
                    PaymentDTO dto = null;
                    switch (rs.getInt("payment_type_id")) {
                        case Constants.PAYMENT_TYPE_INITIAL:
                            dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                            if (dto != null) {
                                dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount") + rs.getDouble("payment_discount"));
                            } else {
                                dto = new PaymentDTO();
                                dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                                dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                                dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                                dto.setPaymentToClient(clDTO.getClientId());
                                if (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER) {
                                    double[] balanceVal = getBalanceByParent(clDTO.getId() + "");
                                    dto.setClientPreviousBalance(clDTO.getClientPreviousBalance() + balanceVal[0]);
                                    dto.setClientBalance(clDTO.getClientCredit() + balanceVal[1]);
                                } else {
                                    dto.setClientPreviousBalance(clDTO.getClientPreviousBalance());
                                    dto.setClientBalance(clDTO.getClientCredit());
                                }
                                dto.setPaymentAmount(rs.getDouble("payment_amount") + rs.getDouble("payment_discount"));
                                map.put(dto.getPaymentToClientId(), dto);
                            }
                            break;
                        case Constants.PAYMENT_TYPE_PAID:
                            dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                            if (dto != null) {
                                dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount") + rs.getDouble("payment_discount"));
                            } else {
                                dto = new PaymentDTO();
                                dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                                dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                                dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                                dto.setPaymentToClient(clDTO.getClientId());
                                if (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER) {
                                    double[] balanceVal = getBalanceByParent(clDTO.getId() + "");
                                    dto.setClientPreviousBalance(clDTO.getClientPreviousBalance() + balanceVal[0]);
                                    dto.setClientBalance(clDTO.getClientCredit() + balanceVal[1]);
                                } else {
                                    dto.setClientPreviousBalance(clDTO.getClientPreviousBalance());
                                    dto.setClientBalance(clDTO.getClientCredit());
                                }
                                dto.setPaymentAmount(rs.getDouble("payment_amount") + rs.getDouble("payment_discount"));
                                map.put(dto.getPaymentToClientId(), dto);
                            }
                            break;
                        case Constants.PAYMENT_TYPE_RETURNED:
                            dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                            if (dto != null) {
                                dto.setPaymentAmount(dto.getPaymentAmount() - rs.getDouble("payment_amount"));
                            } else {
                                dto = new PaymentDTO();
                                dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                                dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                                dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                                dto.setPaymentToClient(clDTO.getClientId());
                                if (clDTO.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER) {
                                    double[] balanceVal = getBalanceByParent(clDTO.getId() + "");
                                    dto.setClientPreviousBalance(clDTO.getClientPreviousBalance() + balanceVal[0]);
                                    dto.setClientBalance(clDTO.getClientCredit() + balanceVal[1]);
                                } else {
                                    dto.setClientPreviousBalance(clDTO.getClientPreviousBalance());
                                    dto.setClientBalance(clDTO.getClientCredit());
                                }
                                dto.setPaymentAmount(0.0 - rs.getDouble("payment_amount"));
                                map.put(dto.getPaymentToClientId(), dto);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            rs.close();

            if (login_dto.getIsUser()) {
                String list = "-1";
                ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
                if (rootChildList != null) {
                    for (int i = 0; i < rootChildList.size(); i++) {
                        list += "," + rootChildList.get(i).getId();
                    }
                }
                sql = "select inetload_to_user_id,sum(inetload_amount+surcharge) as totalLoadAmount from inetload_fdr where inetload_status in(" + Constants.INETLOAD_STATUS_SUCCESSFUL + "," + Constants.INETLOAD_STATUS_SUBMITTED + ") and inetload_to_user_id in(" + list + ") group by inetload_to_user_id";
            } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
                String list = "-1";
                ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
                if (childList != null) {
                    for (int i = 0; i < childList.size(); i++) {
                        list += "," + childList.get(i).getId();
                    }
                }
                sql = "select inetload_to_user_id,sum(inetload_amount+surcharge) as totalLoadAmount from inetload_fdr where inetload_status in(" + Constants.INETLOAD_STATUS_SUCCESSFUL + "," + Constants.INETLOAD_STATUS_SUBMITTED + ") and inetload_to_user_id in(" + list + ") group by inetload_to_user_id";
            } else {
                sql = "select inetload_to_user_id,sum(inetload_amount+surcharge) as totalLoadAmount from inetload_fdr where inetload_status in(" + Constants.INETLOAD_STATUS_SUCCESSFUL + "," + Constants.INETLOAD_STATUS_SUBMITTED + ") and inetload_to_user_id =" + login_dto.getId() + " group by inetload_to_user_id";
            }
            logger.debug(sql);
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = (PaymentDTO) map.get(rs.getLong("inetload_to_user_id"));
                if (dto != null) {
                    dto.setClientUses(rs.getDouble("totalLoadAmount"));
                }
            }
            rs.close();
            data = new ArrayList<PaymentDTO>(map.values());
        } catch (Exception e) {
            logger.fatal("Exception getSummeryDTOs:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
}
