package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;

public class EditClientAction
        extends Action {

    static Logger logger = Logger.getLogger(EditClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ClientForm formBean = (ClientForm) form;
            PermissionDTO userPerDTO = null;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT_EDIT)
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == formBean.getParentId())
                    || login_dto.getId() == formBean.getId()) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                if ((formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER)
                        || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID)
                        || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3)) {
                    target = "failure";
                    return (mapping.findForward(target));
                }
                MyAppError error = new MyAppError();
                ClientDTO dto = new ClientDTO();
                ClientTaskSchedular scheduler = new ClientTaskSchedular();
                ClientDTO parentDTO = ClientLoader.getInstance().getClientDTOByID(formBean.getParentId());
                String opPers[] = request.getParameterValues("opPers");
                if (opPers != null && opPers.length != 0) {
                    for (int i = 0; i < opPers.length; i++) {
                        switch (Integer.parseInt(opPers[i])) {
                            case Constants.GP:
                                if (parentDTO == null || parentDTO.clientGPActive == 1) {
                                    dto.clientGPActive = 1;
                                }
                                break;
                            case Constants.RB:
                                if (parentDTO == null || parentDTO.clientRBActive == 1) {
                                    dto.clientRBActive = 1;
                                }
                                break;
                            case Constants.BL:
                                if (parentDTO == null || parentDTO.clientBLActive == 1) {
                                    dto.clientBLActive = 1;
                                }
                                break;
                            case Constants.TT:
                                if (parentDTO == null || parentDTO.clientTTActive == 1) {
                                    dto.clientTTActive = 1;
                                }
                                break;
                            case Constants.CC:
                                if (parentDTO == null || parentDTO.clientCCActive == 1) {
                                    dto.clientCCActive = 1;
                                }
                                break;
                            case Constants.WR:
                                if (parentDTO == null || parentDTO.clientWRActive == 1) {
                                    dto.clientWRActive = 1;
                                }
                                break;
                            case Constants.BT:
                                if (parentDTO == null || parentDTO.clientBTActive == 1) {
                                    dto.clientBTActive = 1;
                                }
                                break;
                            case Constants.SMS:
                                if (parentDTO == null || parentDTO.clientSMSActive == 1) {
                                    dto.clientSMSActive = 1;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                dto.setId(formBean.getId());
                dto.setClientId(formBean.getClientId());
                dto.setClientName(formBean.getClientName());
                dto.setClientEmail(formBean.getClientEmail());
                dto.setSmsSender(formBean.getSmsSender());
                dto.setClientTypeId(formBean.getClientTypeId());
                dto.setClientPassword(formBean.getClientPassword());
                dto.setRetypePassword(formBean.getRetypePassword());
                dto.setClientStatus(formBean.getClientStatus());
                dto.setClientDiscount(formBean.getClientDiscount());
                dto.setClientDeposit(formBean.getClientDeposit());
                dto.setParentId(formBean.getParentId());
                dto.setClientIp(formBean.getClientIp());
                dto.setClientAni(formBean.getClientAni());
                error = scheduler.editClientInformation(dto, login_dto);

                if (error.getErrorType() > 0) {
                    request.setAttribute("parentId", formBean.getParentId());
                    request.setAttribute("clientCredit", formBean.getClientCredit());
                    request.setAttribute("clientTypeId", formBean.getClientTypeId());
                    request.setAttribute("clientRegister", formBean.getClientRegister());
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else if (request.getParameter("searchLink") != null) {
                    formBean.setMessage(false, "Client is updated successfully!!!");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    String newPath = mapping.findForward(target).getPath() + request.getParameter("searchLink") + "&parentId=" + formBean.getParentId() + "&action=" + Constants.EDIT;
                    if (formBean.getClientRegister() == 1) {
                        newPath = "/client/listSpecialClient.do" + request.getParameter("searchLink") + "&parentId=" + formBean.getParentId() + "&action=" + Constants.EDIT;
                    }
                    ActionForward changedActionForward = new ActionForward(newPath, true);
                    return changedActionForward;
                } else {
                    ActionForward changedActionForward = new ActionForward("../home/home.do", true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
