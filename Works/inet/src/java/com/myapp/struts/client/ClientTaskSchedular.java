package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class ClientTaskSchedular {

    public ClientTaskSchedular() {
    }

    public MyAppError addClientInformation(ClientDTO cl_dto, LoginDTO l_dto) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.addClientInformation(cl_dto, l_dto);
    }
    
    public MyAppError addSpecialAgentInformation(ClientDTO cl_dto) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.addSpecialAgentInformation(cl_dto);
    }    

    public MyAppError editClientInformation(ClientDTO cl_dto, LoginDTO l_dto) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.editClientInformation(cl_dto, l_dto);
    }

    public MyAppError deactivateClients(String list) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.deactivateClients(list);
    }

    public MyAppError activateClients(String list) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.activateClients(list);
    }

    public MyAppError blockClients(String list) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.blockClients(list);
    }

    public MyAppError deleteClients(String list) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.deleteClients(list);
    }

    public MyAppError rechargeClients(LoginDTO l_dto, long[] idListArray, double[] amountListArray, String[] descriptionListArray)
    {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.rechargeClients(l_dto,idListArray,amountListArray,descriptionListArray);
    }

    public MyAppError returnClients(LoginDTO l_dto, long[] idListArray, double[] amountListArray, String[] descriptionListArray)
    {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.returnClients(l_dto,idListArray,amountListArray,descriptionListArray);
    }

    public MyAppError receiveClients(LoginDTO l_dto, long[] idListArray, double[] amountListArray, String[] descriptionListArray)
    {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.receiveClients(l_dto,idListArray,amountListArray,descriptionListArray);
    }

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ClientDTO cldto,long parentId) {
        ClientDAO clinetDAO = new ClientDAO();
        ArrayList<ClientDTO> list = ClientLoader.getInstance().getClientDTOsByParentID(parentId);
        if(list!=null)
        {
          return clinetDAO.getClientDTOsWithSearchParam((ArrayList<ClientDTO>) list.clone(),cldto);
        }
        return null;
    }

    public ArrayList<ClientDTO> getClientDTOsSorted(long parentId) {
        ClientDAO clinetDAO = new ClientDAO();
        ArrayList<ClientDTO> list = ClientLoader.getInstance().getClientDTOsByParentID(parentId);
        if(list!=null)
        {
          return clinetDAO.getClientDTOsSorted((ArrayList<ClientDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<ClientDTO> getClientDTOs(long parentId) {
            return ClientLoader.getInstance().getClientDTOsByParentID(parentId);
    }

    public ClientDTO getClientDTO(long id) {
        return ClientLoader.getInstance().getClientDTOByID(id);
    }
    
    public ArrayList<ClientDTO> getSpecialClientDTOsWithSearchParam(ClientDTO cldto) {
        ClientDAO clinetDAO = new ClientDAO();
        ArrayList<ClientDTO> list = ClientLoader.getInstance().getSpecialClientDTOList();
        if(list!=null)
        {
          return clinetDAO.getSpecialClientDTOsWithSearchParam((ArrayList<ClientDTO>) list.clone(),cldto);
        }
        return null;
    }

    public ArrayList<ClientDTO> getSpecialClientDTOsSorted() {
        ClientDAO clinetDAO = new ClientDAO();
        ArrayList<ClientDTO> list = ClientLoader.getInstance().getSpecialClientDTOList();
        if(list!=null)
        {
          return clinetDAO.getSpecialClientDTOsSorted((ArrayList<ClientDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<ClientDTO> getSpecialClientDTOs() {
            return ClientLoader.getInstance().getSpecialClientDTOList();
    }        
}
