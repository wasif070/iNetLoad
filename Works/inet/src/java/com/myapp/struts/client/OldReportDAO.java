package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.payment.PaymentDTO;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class OldReportDAO {

    static Logger logger = Logger.getLogger(OldReportDAO.class.getName());

    public OldReportDAO() {
    }

    public double[] getBalanceByParent(String parent) {
        double[] balance = new double[2];
        ArrayList<ClientDTO> allClientList = OldClientLoader.getInstance().getOldClientDTOList();
        String parentChildrenList = "," + parent + ",";
        balance[0] = 0.0;
        balance[1] = 0.0;
        if (allClientList != null) {
            for (int i = 0; i < allClientList.size(); i++) {
                ClientDTO dto = allClientList.get(i);
                if (parentChildrenList.contains("," + dto.getParentId() + ",")) {
                    parentChildrenList += dto.getId() + ",";
                    balance[0] += dto.getClientPreviousBalance();
                    balance[1] += dto.getClientCredit();
                }
            }
        }
        return balance;
    }

    public ArrayList<PaymentDTO> getOldReportDTOs(LoginDTO login_dto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select id,client_parent_id,client_id,client_type_id,client_previous_balance,client_credit_all,total_recharged,total_received,total_used from client_old where client_parent_id=" + id + " order by client_id";
            if (!login_dto.getIsUser() && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                sql = "select id,client_parent_id,client_id,client_type_id,client_previous_balance,client_credit_all,total_recharged,total_received,total_used from client_old where id=" + id;
            }
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                if(resultSet.getLong("client_parent_id") == Constants.ROOT_RESELLER_PARENT_ID && resultSet.getInt("client_type_id") == Constants.CLIENT_TYPE_AGENT)
                {
                    continue;
                }
                PaymentDTO dto = new PaymentDTO();
                dto.setPaymentToClientId(resultSet.getLong("id"));
                dto.setPaymentToClient(resultSet.getString("client_id"));
                dto.setPaymentToClientTypeId(resultSet.getInt("client_type_id"));
                dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                if (dto.getPaymentToClientTypeId() == Constants.CLIENT_TYPE_RESELLER) {
                    double[] balanceVal = getBalanceByParent(dto.getPaymentToClientId() + "");
                    dto.setClientPreviousBalance(resultSet.getDouble("client_previous_balance") + balanceVal[0]);
                    dto.setClientBalance(resultSet.getDouble("client_credit_all") + balanceVal[1]);
                } else {
                    dto.setClientPreviousBalance(resultSet.getDouble("client_previous_balance"));
                    dto.setClientBalance(resultSet.getDouble("client_credit_all"));
                }
                dto.setPaymentAmount(resultSet.getDouble("total_recharged"));
                dto.setReceivedAmount(resultSet.getDouble("total_received"));
                dto.setClientUses(resultSet.getDouble("total_used"));
                data.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getOldReportDTOs:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
}
