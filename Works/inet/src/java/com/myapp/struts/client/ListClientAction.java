package com.myapp.struts.client;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListClientAction
        extends Action {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";

        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            ClientForm clientForm = (ClientForm) form;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == clientForm.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == clientForm.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == clientForm.getParentId()))) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                ClientTaskSchedular scheduler = new ClientTaskSchedular();
                int action = 0;
                if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE && request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        logger.debug("tapu22");
                        if (clientForm.getSelectedIDs() != null) {
                            int length = clientForm.getSelectedIDs().length;
                            long[] idListArray = clientForm.getSelectedIDs(); 
                            if (length > 0) {
                                idList += idListArray[0];
                                for (int i = 1; i < length; i++) {
                                    idList += "," + idListArray[i];
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.DEACTIVATION:
                                        if (userPerDTO != null && !userPerDTO.CLIENT_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deactivateClients(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            clientForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            clientForm.setMessage(false, "Clients are inactivated successfully!!!");
                                        }
                                        request.setAttribute(Constants.CLIENT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());
                                        break;
                                    case Constants.ACTIVATION:
                                        if (userPerDTO != null && !userPerDTO.CLIENT_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.activateClients(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            clientForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            clientForm.setMessage(false, "Clients are activated successfully!!!");
                                        }
                                        request.setAttribute(Constants.CLIENT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());

                                        break;
                                    case Constants.BLOCK:
                                        if (userPerDTO != null && !userPerDTO.CLIENT_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.blockClients(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            clientForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            clientForm.setMessage(false, "Clients are blocked successfully!!!");
                                        }
                                        request.setAttribute(Constants.CLIENT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());

                                        break;
                                    case Constants.DELETE:
                                        if (userPerDTO != null && !userPerDTO.CLIENT_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        error = scheduler.deleteClients(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            clientForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            clientForm.setMessage(false, "Clients are deleted successfully!!!");
                                        }
                                        request.setAttribute(Constants.CLIENT_ID_LIST, "," + idList + ",");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());

                                        break;
                                    case Constants.RECHARGE:
                                        logger.debug("tapu1");
                                        if (userPerDTO != null && !userPerDTO.CLIENT_RECHARGE && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        logger.debug("tapu2");
                                        double[] amountListArray = clientForm.getAmounts();
                                        boolean invalidAmountFound = false;
                                        for (int i = 0; i < amountListArray.length; i++) {
                                            if (amountListArray[i] < 0.0) {
                                                invalidAmountFound = true;
                                                clientForm.setMessage(true, "Invalid amount: " + amountListArray[i]);
                                                break;
                                            }
                                        }
                                        logger.debug("tapu3");
                                        if (!invalidAmountFound) {
                                            String[] descriptionListArray = clientForm.getDescriptions();
                                            logger.debug("tapu4");
                                            error = scheduler.rechargeClients(login_dto, idListArray, amountListArray, descriptionListArray);
                                            logger.debug("tapu5");
                                            if (error.getErrorType() > 0) {
                                                target = "failure";
                                                clientForm.setMessage(true, error.getErrorMessage());
                                            } else {
                                                clientForm.setMessage(false, "Your request will be processed. Please wait!!!");
                                            }
                                            request.setAttribute(Constants.CLIENT_ID_LIST, "," + idList + ",");
                                        }
                                        logger.debug("tapu6");
                                        request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());

                                        break;
                                    case Constants.RETURN:
                                        if (userPerDTO != null && !userPerDTO.CLIENT_RETURN && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }                 
                                        double[] amountListArray1 = clientForm.getAmounts();
                                        boolean invalidAmountFound1 = false;
                                        for (int i = 0; i < amountListArray1.length; i++) {
                                            if (amountListArray1[i] < 0.0) {
                                                invalidAmountFound1 = true;
                                                clientForm.setMessage(true, "Invalid amount: " + amountListArray1[i]);
                                                break;
                                            }
                                        }
                                        if (!invalidAmountFound1) {
                                            String[] descriptionListArray1 = clientForm.getDescriptions();
                                            error = scheduler.returnClients(login_dto, idListArray, amountListArray1, descriptionListArray1);
                                            if (error.getErrorType() > 0) {
                                                target = "failure";
                                                clientForm.setMessage(true, error.getErrorMessage());
                                            } else {
                                                clientForm.setMessage(false, "Your request will be processed. Please wait!!!");
                                            }
                                            request.setAttribute(Constants.CLIENT_ID_LIST, "," + idList + ",");
                                        }
                                        request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());
                                        break;
                                    case Constants.RECEIVE:
                                        if (userPerDTO != null && !userPerDTO.CLIENT_RECEIVE && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                                            return (mapping.findForward("index"));
                                        }
                                        double[] amountListArray2 = clientForm.getAmounts();

                                        String[] descriptionListArray2 = clientForm.getDescriptions();
                                        error = scheduler.receiveClients(login_dto, idListArray, amountListArray2, descriptionListArray2);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            clientForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            clientForm.setMessage(false, "Received from clients are successfully!!!");
                                        }
                                        request.setAttribute(Constants.CLIENT_ID_LIST, "," + idList + ",");

                                        request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } else {
                            clientForm.setMessage(true, "No Client is selected!!!");
                            request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());
                        }
                    }
                }
                if (list_all == 0) {
                    if (clientForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.CLIENT_RECORD_PER_PAGE, clientForm.getRecordPerPage());
                    }
                    ClientDTO cldto = new ClientDTO();
                    if (clientForm.getClientId() != null && clientForm.getClientId().trim().length() > 0) {
                        cldto.searchWithClientID = true;
                        cldto.setClientId(clientForm.getClientId().toLowerCase());
                    }
                    if (clientForm.getClientTypeId() > 0) {
                        cldto.searchWithType = true;
                        cldto.setClientTypeId(clientForm.getClientTypeId());
                    }
                    if (clientForm.getClientStatus() >= 0) {
                        cldto.searchWithStatus = true;
                        cldto.setClientStatus(clientForm.getClientStatus());
                    }
                    if (clientForm.getSign() > 0) {
                        cldto.searchWithCredit = true;
                        cldto.setSign(clientForm.getSign());
                        cldto.setClientCredit(clientForm.getClientCredit());
                    }
                    if (clientForm.getClientAni() != null && clientForm.getClientAni().trim().length() > 0) {
                        cldto.searchWithClientANI = true;
                        cldto.setClientAni(clientForm.getClientAni().toLowerCase());
                    }                    
                    clientForm.setClientList(scheduler.getClientDTOsWithSearchParam(cldto, clientForm.getParentId()));
                } else if (list_all == 2) {
                    if (action == Constants.ADD) {
                        clientForm.setMessage(false, "Client is added successfully!!!");
                    } else if (action == Constants.EDIT) {
                        clientForm.setMessage(false, "Client is updated successfully!!!");
                    }
                    if (request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE) != null) {
                        clientForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());
                    clientForm.setClientList(scheduler.getClientDTOsSorted(clientForm.getParentId()));
                } else {
                    if (request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE) != null) {
                        clientForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE).toString()));
                    }
                    clientForm.setClientList(scheduler.getClientDTOs(clientForm.getParentId()));
                }
                clientForm.setSelectedIDs(null);
                if (clientForm.getClientList() != null && clientForm.getClientList().size() > 0 && clientForm.getClientList().size() <= (clientForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
