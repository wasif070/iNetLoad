package com.myapp.struts.client;

import com.myapp.struts.util.Utils;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ClientForm extends org.apache.struts.action.ActionForm {

    private int clientTypeId;
    private int clientStatus;
    private int clientRegister;
    private int clientVersion;
    private int doValidate;
    private int sign;
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long parentId;
    private double clientCredit;
    private double clientDiscount;
    private double clientDeposit;
    private String clientIp;
    private String clientAni;
    private String clientId;
    private String clientName;
    private String clientEmail;
    private String smsSender;
    private String clientPassword;
    private String retypePassword;
    private String message;
    private ArrayList clientList;
    private long[] selectedIDs;
    private double[] amounts;
    private String[] descriptions;
    
    static Logger logger = Logger.getLogger(ClientForm.class.getName());

    public ClientForm() {
        super();
        clientVersion = 0;
        clientRegister = 0;
        clientStatus = -1;
        parentId = Constants.ROOT_RESELLER_PARENT_ID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }
    
    public String getClientAni() {
        return clientAni;
    }

    public void setClientAni(String clientAni) {
        this.clientAni = clientAni;
    }      

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public int getClientRegister() {
        return clientRegister;
    }

    public void setClientRegister(int clientRegister) {
        this.clientRegister = clientRegister;
    }

    public int getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(int clientVersion) {
        this.clientVersion = clientVersion;
    }

    public int getClientTypeId() {
        return clientTypeId;
    }

    public void setClientTypeId(int clientTypeId) {
        this.clientTypeId = clientTypeId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public double getClientCredit() {
        return clientCredit;
    }

    public void setClientCredit(double clientCredit) {
        this.clientCredit = clientCredit;
    }

    public double getClientDiscount() {
        return clientDiscount;
    }

    public void setClientDiscount(double clientDiscount) {
        this.clientDiscount = clientDiscount;
    }

    public double getClientDeposit() {
        return clientDeposit;
    }

    public void setClientDeposit(double clientDeposit) {
        this.clientDeposit = clientDeposit;
    }

    public int getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(int clientStatus) {
        this.clientStatus = clientStatus;
    }

    public String getMessage() {
        return message;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public ArrayList getClientList() {
        return clientList;
    }

    public void setClientList(ArrayList clientList) {
        this.clientList = clientList;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public double[] getAmounts() {
        return amounts;
    }

    public void setAmounts(double[] amounts) {
        this.amounts = amounts;
    }

    public String[] getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String[] descriptions) {
        this.descriptions = descriptions;
    }

    public String getSmsSender() {
        return smsSender;
    }

    public void setSmsSender(String smsSender) {
        this.smsSender = smsSender;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            boolean pass = true;
            boolean re_pass = true;
            if (getClientRegister() == 1) {
                if (!request.getParameter("submit").equals("Cancel")) {
                    if (getClientId() == null || getClientId().length() < 1) {
                        errors.add("clientId", new ActionMessage("errors.client_id.required"));
                    } else if (Utils.checkValidUserId(getClientId()) == false) {
                        errors.add("clientId", new ActionMessage("errors.invalid.client_id"));
                    }
                    if (getClientPassword() == null || getClientPassword().length() < 1) {
                        errors.add("clientPassword", new ActionMessage("errors.client_pass.required"));
                        pass = false;
                    }

                    if (getRetypePassword() == null || getRetypePassword().length() < 1) {
                        errors.add("retypePassword", new ActionMessage("errors.retype_pass.required"));
                        re_pass = false;
                    }
                    if (pass == true && re_pass == true) {
                        if (getClientPassword().length() < 6) {
                            errors.add("retypePassword", new ActionMessage("errors.password_length"));
                        } else if (!getRetypePassword().equals(getClientPassword())) {
                            errors.add("retypePassword", new ActionMessage("errors.password_matching"));
                        }
                    }
                    if (getClientEmail() == null || getClientEmail().length() < 1) {
                        errors.add("clientEmail", new ActionMessage("errors.client_email.required"));
                    } else if (Utils.checkEmailId(getClientEmail()) == false) {
                        errors.add("clientEmail", new ActionMessage("errors.invalid.client_email"));
                    }
                }
            } else {
                if (getClientId() == null || getClientId().length() < 1) {
                    errors.add("clientId", new ActionMessage("errors.client_id.required"));
                } else if (Utils.checkValidUserId(getClientId()) == false) {
                    errors.add("clientId", new ActionMessage("errors.invalid.client_id"));
                }
                if (getClientPassword() == null || getClientPassword().length() < 1) {
                    errors.add("clientPassword", new ActionMessage("errors.client_pass.required"));
                    pass = false;
                }

                if (getRetypePassword() == null || getRetypePassword().length() < 1) {
                    errors.add("retypePassword", new ActionMessage("errors.retype_pass.required"));
                    re_pass = false;
                }

                if (pass == true && re_pass == true) {
                    if (getClientPassword().length() < 6) {
                        errors.add("retypePassword", new ActionMessage("errors.password_length"));
                    } else if (!getRetypePassword().equals(getClientPassword())) {
                        errors.add("retypePassword", new ActionMessage("errors.password_matching"));
                    }
                }

                if (request.getParameter("clientCredit") != null && (!Utils.isDouble(request.getParameter("clientCredit")) || Double.parseDouble(request.getParameter("clientCredit")) < 0.0)) {
                    errors.add("clientCredit", new ActionMessage("errors.credit.valid.value"));
                }

                if (request.getParameter("clientDiscount") != null && (!Utils.isDouble(request.getParameter("clientDiscount")) || Double.parseDouble(request.getParameter("clientDiscount")) > SettingsLoader.getInstance().getSettingsDTO().getMaxAllowedDiscount() || Double.parseDouble(request.getParameter("clientDiscount")) < SettingsLoader.getInstance().getSettingsDTO().getMinAllowedDiscount())) {
                    errors.add("clientDiscount", new ActionMessage("errors.discount.valid.value"));
                }

                if (getClientTypeId() == Constants.CLIENT_TYPE_AGENT && getParentId() == 0) {
                    errors.add("parentId", new ActionMessage("errors.agent_must_have_parent"));
                }

                if (getClientIp() != null && getClientIp().trim().length() > 0) {
                    String[] ips = getClientIp().trim().split(",");
                    for (int i = 0; i < ips.length; i++) {
                        boolean invalid = false;
                        String[] parts = ips[i].split("\\.");

                        if (parts.length != 4) {
                            invalid = true;
                            errors.add("clientIp", new ActionMessage("errors.client_ip"));
                        }
                        if (!invalid) {
                            for (String s : parts) {
                                int j = Integer.parseInt(s);

                                if ((j < 0) || (j > 255)) {
                                    invalid = true;
                                    errors.add("clientIp", new ActionMessage("errors.client_ip"));
                                    break;
                                }
                            }
                        }
                        if (invalid) {
                            break;
                        }
                    }
                }
            }
            request.setAttribute("parentId", getParentId());
            request.setAttribute("clientCredit", getClientCredit());
            request.setAttribute("clientTypeId", getClientTypeId());
        }
        return errors;
    }
}
