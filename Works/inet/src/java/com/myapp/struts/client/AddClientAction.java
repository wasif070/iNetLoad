package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

public class AddClientAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {        
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            PermissionDTO userPerDTO = null;
            ClientForm formBean = (ClientForm) form;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT_ADD)
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId()==formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId()==formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId()==formBean.getParentId()))
                    && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                if((formBean.getClientTypeId()==Constants.CLIENT_TYPE_RESELLER && formBean.getParentId()!= Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId()!=Constants.CLIENT_TYPE_RESELLER)
                        ||(formBean.getClientTypeId()==Constants.CLIENT_TYPE_RESELLER3 && formBean.getParentId()!= Constants.ROOT_RESELLER_PARENT_ID)
                        ||(formBean.getClientTypeId()==Constants.CLIENT_TYPE_RESELLER2 && formBean.getParentId()!= Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId()!=Constants.CLIENT_TYPE_RESELLER3)
                        )
                {
                   target = "failure";
                   return (mapping.findForward(target));
                }
                MyAppError error = new MyAppError();
               
                ClientDTO dto = new ClientDTO();
                ClientTaskSchedular scheduler = new ClientTaskSchedular();
                dto.setClientId(formBean.getClientId());
                dto.setClientName(formBean.getClientName());
                dto.setSmsSender(formBean.getSmsSender());
                dto.setClientEmail(formBean.getClientEmail());
                dto.setClientTypeId(formBean.getClientTypeId());
                dto.setClientPassword(formBean.getClientPassword());
                dto.setRetypePassword(formBean.getRetypePassword());
                dto.setClientStatus(formBean.getClientStatus());
                dto.setClientCredit(formBean.getClientCredit());
                dto.setClientDiscount(formBean.getClientDiscount());
                dto.setClientDeposit(formBean.getClientDeposit());
                dto.setParentId(formBean.getParentId());
                dto.setClientIp(formBean.getClientIp());
                dto.setClientAni(formBean.getClientAni());
                dto.setClientVersion(formBean.getClientVersion());

                error = scheduler.addClientInformation(dto, login_dto);

                if (error.getErrorType() > 0) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else {
                    formBean.setMessage(false, "Client is added successfully!!!");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?list_all=2&parentId=" + formBean.getParentId() + "&action=" + Constants.ADD, true);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
