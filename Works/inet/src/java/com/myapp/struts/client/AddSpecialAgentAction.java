package com.myapp.struts.client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import org.apache.log4j.Logger;

public class AddSpecialAgentAction
        extends Action {

    static Logger logger = Logger.getLogger(AddSpecialAgentAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        
        if (request.getParameter("submit").equals("Cancel")) {
            ActionForward changedActionForward = new ActionForward("/index.do", true);
            return changedActionForward;
        }

        ClientForm formBean = (ClientForm) form;
        MyAppError error = new MyAppError();

        ClientDTO dto = new ClientDTO();
        ClientTaskSchedular scheduler = new ClientTaskSchedular();

        dto.setClientId(formBean.getClientId());
        dto.setClientName(formBean.getClientName());
        dto.setClientEmail(formBean.getClientEmail());
        dto.setClientTypeId(formBean.getClientTypeId());
        dto.setClientPassword(formBean.getClientPassword());
        dto.setRetypePassword(formBean.getRetypePassword());
        dto.setClientStatus(formBean.getClientStatus());
        dto.setClientCredit(formBean.getClientCredit());
        dto.setParentId(formBean.getParentId());
        dto.setClientIp(formBean.getClientIp());
        dto.setClientVersion(formBean.getClientVersion());
        error = scheduler.addSpecialAgentInformation(dto);

        if (error.getErrorType() > 0) {
            target = "failure";
            formBean.setMessage(true, error.getErrorMessage());
        } else {
            formBean.setMessage(false, "Client is added successfully!!!");
            request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
            ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?list_all=2&parentId=" + formBean.getParentId() + "&action=" + Constants.ADD, true);
            return changedActionForward;
        }
        return (mapping.findForward(target));
    }
}
