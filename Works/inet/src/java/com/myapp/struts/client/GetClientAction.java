package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetClientAction
        extends Action {

    static Logger logger = Logger.getLogger(GetClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ClientForm formBean = (ClientForm) form;
            PermissionDTO userPerDTO = null;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT_EDIT)
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == formBean.getParentId())
                    || login_dto.getId() == formBean.getId()) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                if ((formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER)
                        || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID)
                        || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3)) {
                    target = "failure";
                    return (mapping.findForward(target));
                }
                long id = Long.parseLong(request.getParameter("id"));

                ClientDTO dto = new ClientDTO();
                ClientTaskSchedular scheduler = new ClientTaskSchedular();

                dto = scheduler.getClientDTO(id);

                if (dto != null && (login_dto.getIsUser() || dto.getParentId() == login_dto.getId())) {
                    formBean.setId(dto.getId());
                    formBean.setParentId(dto.getParentId());
                    request.setAttribute("parentId", dto.getParentId());
                    formBean.setRetypePassword(dto.getClientPassword());
                    formBean.setClientCredit(dto.getClientCredit());
                    request.setAttribute("clientCredit", dto.getClientCredit());
                    formBean.setClientEmail(dto.getClientEmail());
                    formBean.setClientId(dto.getClientId());
                    formBean.setClientName(dto.getClientName());
                    formBean.setSmsSender(dto.getSmsSender());
                    formBean.setClientPassword(dto.getClientPassword());
                    formBean.setClientStatus(dto.getClientStatus());
                    formBean.setClientTypeId(dto.getClientTypeId());
                    formBean.setClientIp(dto.getClientIp());
                    formBean.setClientAni(dto.getClientAni());
                    formBean.setClientDiscount(dto.getClientDiscount());
                    formBean.setClientDeposit(dto.getClientDeposit());
                    request.setAttribute("clientTypeId", dto.getClientTypeId());
                    if (dto.clientGPActive == 1) {
                        request.setAttribute("clientGPActive", "checked");
                    }
                    if (dto.clientRBActive == 1) {
                        request.setAttribute("clientRBActive", "checked");
                    }
                    if (dto.clientBLActive == 1) {
                        request.setAttribute("clientBLActive", "checked");
                    }
                    if (dto.clientTTActive == 1) {
                        request.setAttribute("clientTTActive", "checked");
                    }
                    if (dto.clientCCActive == 1) {
                        request.setAttribute("clientCCActive", "checked");
                    }
                    if (dto.clientWRActive == 1) {
                        request.setAttribute("clientWRActive", "checked");
                    }
                    if (dto.clientBTActive == 1) {
                        request.setAttribute("clientBTActive", "checked");
                    }
                    if (dto.clientSMSActive == 1) {
                        request.setAttribute("clientSMSActive", "checked");
                    }
                } else {
                    target = "failure";
                }

                MyAppError error = new MyAppError();

                if (error.getErrorType() > 0) {
                    target = "failure";
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
