package com.myapp.struts.client;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class OldClientLoader {

    private static long LOADING_INTERVAL = 60 * 60 * 1000L;
    private long loadingTime = 0;
    private ArrayList<ClientDTO> oldClientDTOList;
    static OldClientLoader oldClientLoader = null;
    static Logger logger = Logger.getLogger(OldClientLoader.class.getName());

    public OldClientLoader() {
        forceReload();
    }

    public static OldClientLoader getInstance() {
        if (oldClientLoader == null) {
            createOldClientLoader();
        }
        return oldClientLoader;
    }

    private synchronized static void createOldClientLoader() {
        if (oldClientLoader == null) {
            oldClientLoader = new OldClientLoader();
        }
    }

    private void reload() {
        oldClientDTOList = new ArrayList<ClientDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select id,client_parent_id,client_credit_all,client_previous_balance from client_old order by id";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                ClientDTO dto = new ClientDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setParentId(resultSet.getLong("client_parent_id"));
                dto.setClientCredit(resultSet.getDouble("client_credit_all"));
                dto.setClientPreviousBalance(resultSet.getDouble("client_previous_balance"));
                oldClientDTOList.add(dto);
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList getOldClientDTOList() {
        checkForReload();
        return oldClientDTOList;
    }
}
