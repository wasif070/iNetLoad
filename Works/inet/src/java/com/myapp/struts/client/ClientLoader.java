package com.myapp.struts.client;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class ClientLoader {
    
    private static long LOADING_INTERVAL = 60 * 1000L;
    private long loadingTime = 0;
    public HashMap<String, ClientDTO> clientDTOsByUserId;
    public HashMap<Long, ClientDTO> clientDTOsById;
    private HashMap<Long, ArrayList> clientDTOsByParentId;
    private ArrayList<ClientDTO> clientDTOList;
    private ArrayList<ClientDTO> specialClientDTOList;
    public HashMap<Long, ClientDTO> allClientDTOsById;
    private HashMap<Long, ArrayList> allClientDTOsByParentId;
    static ClientLoader clientLoader = null;
    static Logger logger = Logger.getLogger(ClientLoader.class.getName());
    
    public ClientLoader() {
        forceReload();
    }
    
    public static ClientLoader getInstance() {
        if (clientLoader == null) {
            createClientLoader();
        }
        return clientLoader;
    }
    
    private synchronized static void createClientLoader() {
        if (clientLoader == null) {
            clientLoader = new ClientLoader();
        }
    }
    
    private void reload() {
        clientDTOsByUserId = new HashMap<String, ClientDTO>();
        clientDTOsById = new HashMap<Long, ClientDTO>();
        clientDTOsByParentId = new HashMap<Long, ArrayList>();
        clientDTOList = new ArrayList<ClientDTO>();
        specialClientDTOList = new ArrayList<ClientDTO>();
        allClientDTOsById = new HashMap<Long, ClientDTO>();
        allClientDTOsByParentId = new HashMap<Long, ArrayList>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from client order by id";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                ClientDTO dto = new ClientDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setClientId(resultSet.getString("client_id"));
                dto.setParentId(resultSet.getLong("client_parent_id"));
                dto.setClientCredit(resultSet.getDouble("client_credit_all"));
                dto.setClientPreviousBalance(resultSet.getDouble("client_previous_balance"));
                dto.setClientDiscount(resultSet.getDouble("client_discount"));
                dto.setClientDeposit(resultSet.getDouble("client_deposit"));
                dto.setClientEmail(resultSet.getString("client_email"));
                dto.setClientName(resultSet.getString("client_full_name"));
                dto.setClientPassword(resultSet.getString("client_password"));
                dto.setClientStatus(resultSet.getInt("client_status"));
                dto.setClientTypeId(resultSet.getInt("client_type_id"));
                dto.setIsDeleted(resultSet.getInt("client_deleted"));
                dto.setClientIp(resultSet.getString("client_ip"));
                dto.setClientAni(resultSet.getString("client_ani"));
                dto.setClientVersion(resultSet.getInt("client_version"));
                dto.clientGPActive = resultSet.getInt("client_gp_active");
                dto.clientRBActive = resultSet.getInt("client_rb_active");
                dto.clientBLActive = resultSet.getInt("client_bl_active");
                dto.clientTTActive = resultSet.getInt("client_tt_active");
                dto.clientCCActive = resultSet.getInt("client_cc_active");
                dto.clientWRActive = resultSet.getInt("client_wr_active");
                dto.clientBTActive = resultSet.getInt("client_bt_active");
                dto.clientSMSActive = resultSet.getInt("client_sms_active");
                dto.setSmsSender(resultSet.getString("client_sms_sender"));
                if (!dto.getIsDeleted()) {
                    clientDTOsById.put(dto.getId(), dto);
                    clientDTOsByUserId.put(dto.getClientId(), dto);
                    ArrayList<ClientDTO> childList = clientDTOsByParentId.get(dto.getParentId());
                    if (childList != null) {
                        if (dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && dto.getParentId() == Constants.ROOT_RESELLER_PARENT_ID) {
                            specialClientDTOList.add(dto);
                        } else {
                            childList.add(dto);
                        }
                    } else {
                        if (dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && dto.getParentId() == Constants.ROOT_RESELLER_PARENT_ID) {
                            specialClientDTOList.add(dto);
                        } else {
                            childList = new ArrayList<ClientDTO>();
                            childList.add(dto);
                            clientDTOsByParentId.put(dto.getParentId(), childList);
                        }
                    }
                }
                clientDTOList.add(dto);
                allClientDTOsById.put(dto.getId(), dto);
                ArrayList<ClientDTO> allChildList = allClientDTOsByParentId.get(dto.getParentId());
                if (allChildList != null) {
                    allChildList.add(dto);
                } else {
                    allChildList = new ArrayList<ClientDTO>();
                    allChildList.add(dto);
                    allClientDTOsByParentId.put(dto.getParentId(), allChildList);
                }
            }
        } catch (Exception e) {
            logger.fatal("Exception:" + e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }
    
    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }
    
    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }
    
    public synchronized ArrayList getClientDTOList() {
        checkForReload();
        return clientDTOList;
    }
    
    public synchronized ArrayList getSpecialClientDTOList() {
        checkForReload();
        return specialClientDTOList;
    }
    
    public synchronized ClientDTO getClientDTOByUserId(String client_id) {
        checkForReload();
        return clientDTOsByUserId.get(client_id);
    }
    
    public synchronized ClientDTO getClientDTOByUserIdPass(String client_id, String password, int client_version) {
        checkForReload();
        ClientDTO clientDTO = clientDTOsByUserId.get(client_id);
        if (clientDTO != null) {
            if (clientDTO.getClientPassword().equals(password) && clientDTO.getClientVersion() == client_version) {
                return clientDTO;
            }
        }
        return null;
    }
    
    public synchronized ArrayList<ClientDTO> getClientDTOsByParentID(long parentID) {
        forceReload();
        return clientDTOsByParentId.get(parentID);
    }
    
    public synchronized ClientDTO getClientDTOByID(long id) {
        checkForReload();
        return clientDTOsById.get(id);
    }
    
    public synchronized ArrayList<ClientDTO> getAllClientDTOsByParentID(long parentID) {
        checkForReload();
        return allClientDTOsByParentId.get(parentID);
    }
    
    public synchronized ClientDTO getAllClientDTOByID(long id) {
        checkForReload();
        return allClientDTOsById.get(id);
    }
}
