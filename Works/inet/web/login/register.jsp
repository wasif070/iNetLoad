<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page language="java"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="com.myapp.struts.session.Constants" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Register</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="fl_left" style="height: 400px; width: 100%">
                <div style="clear: both; height: 30px"></div>
                <div class="body" align="center">
                    <html:form action="/client/addSpecialAgent" method="post">
                        <table class="reg_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 10px;">Registration For Special Agent</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px; " valign="bottom">
                                        <bean:write name="ClientForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Client ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientId"  /><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientName" /><br/>
                                        <html:messages id="clientName" property="clientName">
                                            <bean:write name="clientName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Email</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientEmail" /><br/>
                                        <html:messages id="clientEmail" property="clientEmail">
                                            <bean:write name="clientEmail"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="clientPassword" /><br/>
                                        <html:messages id="clientPassword" property="clientPassword">
                                            <bean:write name="clientPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                            
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="clientVersion" value="0" />
                                        <input type="hidden" name="clientRegister" value="1" />
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Register" />
                                        <input name="submit" type="submit" class="custom-button" value="Cancel" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
    </div>
    </body>
</html>