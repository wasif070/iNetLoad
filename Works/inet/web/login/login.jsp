<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page language="java"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="com.myapp.struts.session.Constants" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Login</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="fl_left" style="height: 400px; width: 100%">
                <div style="clear: both; height: 80px"></div>
                <div class="body" align="center">
                    <html:form action="/login/login" method="post">
                        <table class="login_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">Login to <%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%></th>
                                </tr>                                   
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center"><a href="../login/register.jsp"><font style="color: blue;text-decoration: underline; font-size: 11px; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">Create A Special Agent For You!!!</font></a></td>
                                </tr>                                 
                                <tr>
                                    <th valign="top" style="padding-top: 14px; width: 140px !important">User ID</th>
                                    <td valign="top" style="padding-top: 10px;">
                                        <html:text property="loginId"/><br/>
                                        <html:messages id="loginId" property="loginId">
                                            <bean:write name="loginId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 10px; width: 140px !important">Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="loginPass"/><br/>
                                        <html:messages id="loginPass" property="loginPass">
                                            <bean:write name="loginPass"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="height: 40px; width: 140px;">&nbsp;</th>
                                    <td style="height: 40px;">
                                        <input name="submit" type="submit" class="custom-button" value="Login" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" style="height: 20px;" valign="middle">
                                        <%
                                                    request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                                                    if (request.getSession(true).getAttribute(Constants.LOGIN_TIMEOUT) != null) {
                                                        request.getSession(true).removeAttribute(Constants.LOGIN_TIMEOUT);
                                                        out.println("<span style='color:red;font-size: 12px;font-weight:bold'>" + Constants.LOGIN_TIMEOUT + "</span>");
                                                    }
                                                    else if(request.getSession(true).getAttribute(Constants.LOGIN_ACCESS_DENIED) != null) {
                                                        request.getSession(true).removeAttribute(Constants.LOGIN_ACCESS_DENIED);
                                                        out.println("<span style='color:red;font-size: 12px;font-weight:bold'>" + Constants.LOGIN_ACCESS_DENIED + "</span>");
                                                    }
                                                    else {
                                        %>
                                        <bean:write name="LoginForm" property="message" filter="false"/>
                                        <%                                                    }
                                        %>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2" style="height: 20px;" valign="middle">
                                         <a href="../iNetLoad.jar"><font style="color: blue;text-decoration: underline;">Download <%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> FlexiDialer</font></a><BR>
                                         <a href="../opera.jar"><font style="color: blue;text-decoration: underline;">Download Opera Mini</font></a>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                        <input type="hidden" name="clientVersion" value="0" />            
                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>