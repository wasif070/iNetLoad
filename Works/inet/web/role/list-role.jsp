<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.ROLE_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.ROLE_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j=0;
        var selectedIDsArray = [];
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j]=i;
                    j++;
                }
            }
        }
        document.forms[0].reset();
        for(i=0; i<selectedIDsArray.length; i++)
        {
            tempCheckbox = allInputArray[selectedIDsArray[i]];
            tempCheckbox.checked = true;
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }

        link = "../role/listRole.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../role/listRole.do?d-49216-p="+document.forms[0].pageNo.value;
    }

    function editRole(value1,value2)
    {
        document.forms[0].reset();
        link = "roleName="+document.forms[0].roleName.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = "../role/getRole.do?id="+value1+"&name="+value2+"&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Role List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/role/listRole.do" method="post">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Role Name</th>
                                        <td><html:text property="roleName" /></td>
                                    </tr>

                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="full-div">
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                            %>
                        </div>
                        <div style="border : 1px solid #848284;" align="center">
                            <div class="blank-height"></div>
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.RoleForm.roleList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Role" />
                                <display:setProperty name="paging.banner.items_name" value="Roles" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>                              
                                <display:column class="custom_column2" title="Nr" style="width:7%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Role Name" sortable="true"  style="width:25%" >
                                    <a href="#" onclick="editRole('${data.id}','${data.roleName}')"><u>${data.roleName}</u></a>
                                </display:column>
                                <display:column  property="roleCreatedBy" title="Created By" sortable="true" style="width:25%" />
                                <display:column  property="roleDescription" title="Description" sortable="true" style="width:25%" />
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <%
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <div class="clear height-5px"></div>
                            <input type="submit" class="action-button" value="Delete Selected" style="width: 115px;" onclick="submitform('<%=Constants.DELETE%>')" />
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>