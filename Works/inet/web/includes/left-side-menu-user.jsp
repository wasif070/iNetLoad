<%@page import="com.myapp.struts.session.PermissionConstants,com.myapp.struts.user.UserLoader"%>
<%
    if (login_dto.getRoleID() > Constants.ROOT_RESELLER_PARENT_ID) {
        perDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO();
    }
%>
<div>
    <div class="WUItreeNode" style="margin-left: 0px;">
        <div>
            <div class="WUItreeClicked"></div>
            <div class="WUItreeLabel">
                <div><img src="../images/tree_node_minus.gif" alt="node" vspace="0" width="13" height="16"></div>
                <div class="fl_left"><a href="../home/home.do" style="font-size: 14px">Homepage</a></div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="WUItreeNodeContent" style="display: block; background: none repeat scroll 0% 0% transparent;">

            <%
                if (perDTO != null && perDTO.CLIENT) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../client/listClient.do?list_all=1&parentId=0" style="font-size: 14px">Clients</a></div>
                        <div class="clear"></div>
                    </div>
                    <%
                        if (perDTO.CLIENT_ADD || perDTO.CLIENT_RECHARGE || perDTO.CLIENT_RECEIVE) {
                    %>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <%
                            if (perDTO.CLIENT_ADD) {
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/add-client.jsp" style="font-size: 14px">Add Client</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <%                                                       }
                            if (perDTO.CLIENT_RECHARGE || perDTO.CLIENT_RECEIVE) {
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/rechargeClient.do?list_all=1&recharge=1" style="font-size: 14px">Recharge/Receive</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <%                                                       }
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/listSpecialClient.do?list_all=1" style="font-size: 14px">Special Agents</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>                         
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
            <%
                }
                if (perDTO != null && perDTO.RCG) {
            %>                
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../rechargecard/listRechargecardGroup.do?list_all=1" style="font-size: 14px">Recharge Card Groups</a></div>
                        <div class="clear"></div>
                    </div>
                    <%
                        if (perDTO.RCG_ADD) {
                    %>
                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/add-rechargecard-group.jsp" style="font-size: 14px">Add Card Group</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <%                            }
                    %>
                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/listRechargecard.do?list_all=1" style="font-size: 14px">Search Card</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>                     
                    <%
                        if (perDTO.RCG_ADD) {
                    %>
                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/add-card-shop.jsp" style="font-size: 14px">Add Card Shop</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>    
                    <%                            }
                    %>                    
                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/listCardShop.do?list_all=1" style="font-size: 14px">Card Shop Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>                         

                </div>
            </div>                 
            <%
                }
                if (perDTO != null && (perDTO.REPORT || perDTO.REFILL)) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">Reports</a></div>
                        <div class="clear"></div>
                    </div>
                    <%
                        if (perDTO.REFILL || perDTO.REPORT_REFILL_SUMMERY || perDTO.REPORT_PAYMENT_MADE_HISTORY || perDTO.REPORT_PAYMENT_SUMMERY) {
                    %>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <%
                            if (perDTO.REFILL) {
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <%                                            }
                            if (perDTO.REPORT_REFILL_SUMMERY) {
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/summeryFlexiload.do?list_all=1" style="font-size: 14px">Succ. Refill Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <%                                            }
                            if (perDTO.REPORT_PAYMENT_MADE_HISTORY) {
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/listMadePayments.do?list_all=1" style="font-size: 14px">Payment Made History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <%                                            }
                            if (perDTO.REPORT_PAYMENT_SUMMERY) {
                        %>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/summeryPayments.do?list_all=1" style="font-size: 14px">Payment Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <%                                                                            }
                        %>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
            <%
                }
                if (perDTO != null && (perDTO.FLEXI_SIM_SETTINGS || perDTO.FLEXI_MESSAGE_LOG || perDTO.FLEXI_SIM_RECHARGE)) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left">
                            <%
                                if (perDTO.FLEXI_SIM_SETTINGS) {
                            %>
                            <a href="../gateway/listGateway.do?list_all=1" style="font-size: 14px">Flexi SIM Settings</a>
                            <%                            } else {
                            %>
                            <a href="#" style="font-size: 14px">Flexi SIM Settings</a>
                            <%                                }
                            %>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <%
                        if (perDTO.FLEXI_MESSAGE_LOG) {
                    %>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left" >
                            <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                            <div class="fl_left"><a href="../gateway/listGatewayLog.do?list_all=1" style="font-size: 14px">SIM Message Log</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <%                        }
                        if (perDTO.FLEXI_SIM_RECHARGE) {
                    %>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../gateway/rechargeGateway.do?list_all=1" style="font-size: 14px">Flexi SIM Recharge</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <%                            }
                    %>
                </div>
            </div>
            <%                        }
                if (perDTO != null && (perDTO.FLEXI_SIM_SETTINGS)) {
            %>

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../dealers/listDealer.do?list_all=1" style="font-size: 14px">Flexi Dealers</a></div>
                        <div class="clear"></div>
                    </div>                   
                    <div class="WUItreeNodeContent" style="display: block;">
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../dealers/add-dealer.jsp" style="font-size: 14px">Add Dealer</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../dealers/rechargeDealer.do?list_all=1" style="font-size: 14px">Payment/Receive</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../dealers/listPayments.do?list_all=1" style="font-size: 14px">Transaction History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>           
                </div>
            </div> 
            <%                }
                if (perDTO != null && (perDTO.SMS_SETTINGS || perDTO.SMS_LOG)) {
            %>
            <%--
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left">
                            <%
                                if (perDTO.SMS_SETTINGS) {
                            %>
                            <a href="../smsgateway/listGateway.do?list_all=1" style="font-size: 14px">SMS Gateways</a>
                            <%                            } else {
                            %>
                            <a href="#" style="font-size: 14px">SMS Gateway</a>
                            <%                                }
                            %>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <%
                        if (perDTO.SMS_LOG) {
                    %>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left" >
                            <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                            <div class="fl_left"><a href="../smsgateway/listGatewayLog.do?list_all=1" style="font-size: 14px">SMS Log</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <%                        }
                    %>
                </div>
            </div>
            --%>
            <%                        }
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/change-pass.jsp" style="font-size: 14px">Change Password</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div class="WUItreeItem" >
                        <div><img src="../images/tree_node_minus.gif" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/logout.do" style="font-size: 14px">Logout</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>