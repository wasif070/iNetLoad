<%@page import="com.myapp.struts.session.Constants"%>
<div style="margin-bottom:20px;">
    <div class="WUItreeNode" style="margin-left: 0px;">
        <div>
            <div class="WUItreeClicked"></div>
            <div class="WUItreeLabel">
                <div><img src="../images/tree_node_minus.gif" alt="node" vspace="0" width="13" height="16"></div>
                <div class="fl_left"><a href="../home/home.do" style="font-size: 14px">Homepage</a></div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="WUItreeNodeContent" style="display: block; background: none repeat scroll 0% 0% transparent;">

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../client/listClient.do?list_all=1&parentId=0" style="font-size: 14px">Clients</a></div>
                        <div class="clear"></div>
                    </div>

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/add-client.jsp" style="font-size: 14px">Add Client</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/rechargeClient.do?list_all=1&parentId=0" style="font-size: 14px">Recharge/Receive</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/listSpecialClient.do?list_all=1" style="font-size: 14px">Special Agents</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>                        

                    </div>

                </div>
            </div>  
            <%--             
                        <div class="WUItreeNode">
                            <div>
                                <div class="WUItreeClicked"></div>
                                <div  class="WUItreeLabel">
                                    <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                                    <div class="fl_left"><a href="../scratchcard/listScratchcard.do?list_all=1" style="font-size: 14px">Scratch Cards</a></div>
                                    <div class="clear"></div>
                                </div>

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../scratchcard/add-scratchcard.jsp" style="font-size: 14px">Add Scratch Card</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../scratchcard/listScratchcardType.do?list_all=1" style="font-size: 14px">Scratch Card Types</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                    </div>

                </div>
            </div>              
            --%>            
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../rechargecard/listRechargecardGroup.do?list_all=1" style="font-size: 14px">Recharge Card Groups</a></div>
                        <div class="clear"></div>
                    </div>

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/add-rechargecard-group.jsp" style="font-size: 14px">Add Card Group</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/listRechargecard.do?list_all=1" style="font-size: 14px">Search Card</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>                     

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/add-card-shop.jsp" style="font-size: 14px">Add Card Shop</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>      

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/listCardShop.do?list_all=1" style="font-size: 14px">Card Shop Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>                         

                </div>
            </div>                        

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">Reports</a></div>
                        <div class="clear"></div>
                    </div>

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/summeryFlexiload.do?list_all=1" style="font-size: 14px">Succ. Refill Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/listMadePayments.do?list_all=1" style="font-size: 14px">Payment Made History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/summeryPayments.do?list_all=1" style="font-size: 14px">Payment Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">Balance Transfer Reports</a></div>
                        <div class="clear"></div>
                    </div>

                    <div class="WUItreeNodeContent" style="display: block;">
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">SMS Report</a></div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending SMS Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful SMS Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected SMS Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div> 
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../user/listUser.do?list_all=1" style="font-size: 14px">Users</a></div>
                        <div class="clear"></div>
                    </div>

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../user/add-user.jsp" style="font-size: 14px">Add User</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem  fl_left" >
                                <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../role/add-role.jsp" style="font-size: 14px">Add User Role</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../role/listRole.do?list_all=1" style="font-size: 14px">Search User Role</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>

                </div>
            </div> 

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../gateway/listGateway.do?list_all=1" style="font-size: 14px">Flexi SIM Settings</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../gateway/listGatewayLog.do?list_all=1" style="font-size: 14px">SIM Message Log</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../gateway/listRechargeGateway.do?list_all=1" style="font-size: 14px">SIM Recharge History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>                                

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../dealers/listDealer.do?list_all=1" style="font-size: 14px">Flexi Dealers</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../dealers/add-dealer.jsp" style="font-size: 14px">Add Dealer</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../dealers/rechargeDealer.do?list_all=1" style="font-size: 14px">Payment/Receive</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../dealers/listPayments.do?list_all=1" style="font-size: 14px">Transaction History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>  
            <%--
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../smsgateway/listGateway.do?list_all=1" style="font-size: 14px">SMS Gateways</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../smsgateway/listGatewayLog.do?list_all=1" style="font-size: 14px">SMS Log</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>                                
                                 
                        <div class="WUItreeNode">
                            <div>
                                <div class="WUItreeClicked"></div>
                                <div  class="WUItreeLabel">
                                    <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                                    <div class="fl_left"><a href="../api/listApi.do?list_all=1" style="font-size: 14px">Flexi APIs</a></div>
                                    <div class="clear"></div>
                                </div>
                                <div class="WUItreeNodeContent" style="display: block;">
                                    <div>
                                        <div class="WUItreeClicked"></div>
                                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                                        <div class="WUItreeItem fl_left" >
                                            <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                            <div class="fl_left"><a href="../api/add-api.jsp" style="font-size: 14px">Add API</a></div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>                                 
            --%>              

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../system/getSettings.do" style="font-size: 14px">Change Settings</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>                                

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/change-pass.jsp" style="font-size: 14px">Change Password</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../balancetransfer/listSurcharge.do?list_all=1" style="font-size: 14px">BT Surcharge Plan</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>             

            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../sms/listRatePlan.do?list_all=1" style="font-size: 14px">SMS Rate Plan</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div class="WUItreeItem" >
                        <div><img src="../images/tree_node_minus.gif" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/logout.do" style="font-size: 14px">Logout</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="clear"></div>
</div>