<div>
    <div class="WUItreeNode" style="margin-left: 0px;">
        <div>
            <div class="WUItreeClicked"></div>
            <div class="WUItreeLabel">
                <div><img src="../images/tree_node_minus.gif" alt="node" vspace="0" width="13" height="16"></div>
                <div class="fl_left"><a href="../home/home.do" style="font-size: 14px">Homepage</a></div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="WUItreeNodeContent" style="display: block; background: none repeat scroll 0% 0% transparent;">
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../client/listClient.do?list_all=1&parentId=<%=login_dto.getId()%>" style="font-size: 14px">Clients</a></div>
                        <div class="clear"></div>
                    </div>

                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/add-client.jsp" style="font-size: 14px">Add Client</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left" >
                                <div class="fl_left"><img src="../images/tree_item_last.gif" width="13" height="16"></div>
                                <div class="fl_left"><a href="../client/rechargeClient.do?list_all=1&parentId=<%=login_dto.getId()%>" style="font-size: 14px">Recharge/Receive</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>

                </div>
            </div>


            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">Reports</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/summeryFlexiload.do??list_all=1" style="font-size: 14px">Succ. Refill Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/listMadePayments.do?list_all=1" style="font-size: 14px">Payment Made History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/myAccountSummery.do?list_all=1" style="font-size: 14px">My Account Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/summeryPayments.do?list_all=1" style="font-size: 14px">Payment Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <%
                if (login_dto.getIsBTActive() == 1) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">Balance Transfer Reports</a></div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

            </div>
            <%}
            %>
            <%
                if (login_dto.getIsSMSActive() == 1) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">SMS Reports</a></div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

            </div>
            <%}
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/change-pass.jsp" style="font-size: 14px">Change Password</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div class="WUItreeItem" >
                        <div><img src="../images/tree_node_minus.gif" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/logout.do" style="font-size: 14px">Logout</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>