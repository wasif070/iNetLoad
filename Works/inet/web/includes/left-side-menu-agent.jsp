<div>
    <div class="WUItreeNode" style="margin-left: 0px;">
        <div>
            <div class="WUItreeClicked"></div>
            <div class="WUItreeLabel">
                <div><img src="../images/tree_node_minus.gif" alt="node" vspace="0" width="13" height="16"></div>
                <div class="fl_left"><a href="../home/home.do" style="font-size: 14px">Homepage</a></div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="WUItreeNodeContent" style="display: block; background: none repeat scroll 0% 0% transparent;">
            <%
                if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/add-flexiload.jsp" style="font-size: 14px">Refill Now</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <%
                if (login_dto.getIsBTActive() == 1) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/add-bt.jsp" style="font-size: 14px">Balance Transfer</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>  
            <%}
            %>
            <%
                if (login_dto.getIsSMSActive() == 1) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../sms/send-sms.jsp" style="font-size: 14px">Send SMS</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>  
            <%}
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../scratchcard/buy-scratchcard.jsp" style="font-size: 14px">Buy Scratch Card</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../scratchcard/myScratchcards.do?list_all=1" style="font-size: 14px">My Scratch Cards</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div> 
            <%
                if (login_dto.getIsSpecialAgent()) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../rechargecard/refill-card.jsp" style="font-size: 14px">Refill By Card</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>            
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../rechargecard/recharge-card.jsp" style="font-size: 14px">Recharge Account</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <%                    }
                }
            %>             
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../contactgroup/listContactGroup.do?list_all=1" style="font-size: 14px">Contact Groups</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="WUItreeNodeContent" style="display: block;">

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../contactgroup/add-contact-group.jsp" style="font-size: 14px">Add Group</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>                         

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../contacts/summeryFlexiload.do?list_all=1" style="font-size: 14px">Contact Refill Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>                        

                    </div>
                </div>
            </div>            
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">Reports</a></div>
                        <div class="clear"></div>
                    </div>
                    <div class="WUItreeNodeContent" style="display: block;">
                        <%
                            if (login_dto.getIsSpecialAgent()) {
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../rechargecard/listCardUsage.do?list_all=1" style="font-size: 14px">Card Usage History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>                           
                        <%                            }
                        %>                        
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../flexiload/summeryFlexiload.do?list_all=1" style="font-size: 14px">Succ. Refill Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <%
                            if (!login_dto.getIsSpecialAgent()) {
                        %>
                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/listReceivedPayments.do?list_all=1" style="font-size: 14px">Payment Recv. History</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div>
                            <div class="WUItreeClicked"></div>
                            <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                            <div class="WUItreeItem fl_left">
                                <div class="fl_left"><img src="../images/tree_item_last.gif" alt="node" vspace="0" width="13" height="16"></div>
                                <div class="fl_left"><a href="../payment/myAccountSummery.do?list_all=1" style="font-size: 14px">My Account Summery</a></div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>                               
                        <%    }
                        %>
                    </div>
                </div>
            </div>
            <%
                if (login_dto.getIsBTActive() == 1) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">Balance Transfer Reports</a></div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

            </div>
            <%}
            %>
            <%
                if (login_dto.getIsSMSActive() == 1) {
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>&refresh=1" style="font-size: 14px">SMS Reports</a></div>
                        <div class="clear"></div>
                    </div>
                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-size: 14px">Pending Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_REJECTED%>" style="font-size: 14px">Rejected Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div>
                        <div class="WUItreeClicked"></div>
                        <div class="fl_left "><img src="../images/tree.gif" width="13" height="16" /></div>
                        <div class="WUItreeItem fl_left">
                            <div class="fl_left"><img src="../images/tree_item.gif" alt="node" vspace="0" width="13" height="16"></div>
                            <div class="fl_left"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="font-size: 14px">Successful Refill Details</a></div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

            </div>
            <%}
            %>
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div  class="WUItreeLabel">
                        <div><img  src="../images/tree_node_minus.gif" vspace="0" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/change-pass.jsp" style="font-size: 14px">Change Password</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>                                  
            <div class="WUItreeNode">
                <div>
                    <div class="WUItreeClicked"></div>
                    <div class="WUItreeItem" >
                        <div><img src="../images/tree_node_minus.gif" width="13" height="16"></div>
                        <div class="fl_left"><a href="../login/logout.do" style="font-size: 14px">Logout</a></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>