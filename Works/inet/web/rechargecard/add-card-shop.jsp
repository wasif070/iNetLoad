<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
            && perDTO != null && !perDTO.RCS_ADD)
            || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
%>

<%@page import="java.util.ArrayList" %>
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Card Shop</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/rechargecard/addCardShop" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Add New Card Shop</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="CardShopForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="shopName"  /><br/>
                                        <html:messages id="shopName" property="shopName">
                                            <bean:write name="shopName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Address</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="shopAddress"  /><br/>
                                        <html:messages id="shopAddress" property="shopAddress">
                                            <bean:write name="shopAddress"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Description</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="shopDescription"  /><br/>
                                        <html:messages id="shopDescription" property="shopDescription">
                                            <bean:write name="shopDescription"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Owner Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="shopOwner"  /><br/>
                                        <html:messages id="shopOwner" property="shopOwner">
                                            <bean:write name="shopOwner"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Contact Number</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="shopContactNumber"  /><br/>
                                        <html:messages id="shopContactNumber" property="shopContactNumber">
                                            <bean:write name="shopContactNumber"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                                                                                                                            
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>