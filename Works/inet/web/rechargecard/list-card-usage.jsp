<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
                    && perDTO != null && !perDTO.RCG)
                    || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,com.myapp.struts.util.Utils" %>
<%@page import="com.myapp.struts.rechargecard.CardShopDAO,com.myapp.struts.rechargecard.CardShopLoader,java.util.ArrayList,com.myapp.struts.rechargecard.CardShopDTO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 1000;

   if (request.getSession(true).getAttribute(Constants.RC_USAGE_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.RC_USAGE_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../rechargecard/listCardUsage.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Recharge Card Usage</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body1">
                    <html:form action="/rechargecard/listCardUsage.do" method="post" >
                        <div class="full-div">
                            <div class="three-fourth-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Serial No</th>
                                        <td>
                                            <html:text property="serialNo">
                                            </html:text>
                                        </td>  
                                        <th>Card No</th>
                                        <td>
                                            <html:text property="cardNo">
                                            </html:text>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <th>Package Type</th>
                                        <td>
                                            <html:select property="groupPackage">
                                                <html:option value="-1">Select</html:option>
                                                <%
                                                 for(int i=0;i<Constants.PACKAGE_ID_VALUE.length;i++)
                                                 {
                                                %>
                                                <html:option value="<%=Constants.PACKAGE_ID_VALUE[i]%>"><%=Constants.PACKAGE_ID_STRING[i]%></html:option>
                                                <%
                                                 }
                                                %>
                                            </html:select>
                                        </td>
                                        <th>Card Amount</th>
                                        <td>
                                            <html:select property="sign" style="width:47px">
                                                <html:option value="0">All</html:option>
                                                <html:option value="1">=</html:option>
                                                <html:option value="2"><</html:option>
                                                <html:option value="3">></html:option>
                                            </html:select>
                                            <html:text property="groupAmount" style="width:100px" />
                                        </td>                      
                                    </tr> 
                                    <tr>
                                        <th>Start Time</th>
                                        <td>
                                            <html:select property="startDay" styleClass="width-50px" styleId="startDay">
                                                <%
                                                            ArrayList<Integer> days = Utils.getDay();
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=increment%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="startMonth" styleClass="width-50px" styleId="startMonth">
                                                <%
                                                            ArrayList<String> months = Utils.getMonth();
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=month%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="startYear" styleClass="width-60px" styleId="startYear">
                                                <%
                                                            ArrayList<Integer> years = Utils.getYear();
                                                            for (int i = 0; i < years .size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                %>
                                                <html:option value="<%=year%>"><%=year%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                        <th>End Time</th>
                                        <td>
                                            <html:select property="endDay" styleClass="width-50px" styleId="endDay">
                                                <%
                                                            ArrayList<Integer> days1 = Utils.getDay();
                                                            for (int i = 0; i < days1.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=increment%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="endMonth" styleClass="width-50px" styleId="endMonth">
                                                <%
                                                            ArrayList<String> months1 = Utils.getMonth();
                                                            for (int i = 0; i < months1.size(); i++) {
                                                                String month = months1.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=month%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="endYear" styleClass="width-60px" styleId="endYear">
                                                <%
                                                            ArrayList<Integer> years1 = Utils.getYear();
                                                            for (int i = 0; i < years1.size(); i++) {
                                                                String year = String.valueOf(years1.get(i));
                                                %>
                                                <html:option value="<%=year%>"><%=year%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                    </tr>                                        
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>     
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>                                              
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                            %>
                        </div>

                        <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                        <div class="full-div" align="center">
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>                             
                            <display:table class="reporting_table" defaultsort="8" defaultorder="descending" cellpadding="0" cellspacing="0" id="data" name="sessionScope.RechargecardForm.cardUsageList" pagesize="<%=recordPerPage%>" export="true">
                                <display:setProperty name="paging.banner.item_name" value="Card Usage" />
                                <display:setProperty name="paging.banner.items_name" value="Card Usages" /> 
                                <display:column class="custom_column2" title="Nr" style="width:5%;" media="html">
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>  
                                <display:column title="Serial No" property="serialNo" sortable="true" style="width:15%;" />        
                                <display:column title="Card No" property="cardNo" sortable="true" style="width:17%;" />                                                    
                                <display:column class="custom_column1" property="groupPackageName" title="Package Type" sortable="true" style="width:11%" />
                                <display:column class="custom_column2" property="groupAmount" title="Card Amount" format="{0,number,0.00}" sortable="true" style="width:11%" />
                                <display:column class="custom_column1" property="phoneNumber" title="Refill Number" sortable="true" style="width:15%" />
                                <display:column class="custom_column1" property="blockedBy" title="Used By" sortable="true" style="width:11%" />
                                <display:column class="custom_column1" property="blockedAt" title="Used At" decorator="implement.displaytag.LongDateWrapper" sortable="true" style="width:15%" />
                            </display:table>                                                 
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <%
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>