<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
            && perDTO != null && !perDTO.RCG_EDIT)
            || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
    String link = request.getQueryString();
    if (request.getParameter("searchLink") != null) {
        link = request.getParameter("searchLink");
    } else {
        link = "?" + link.substring(link.indexOf("&link=") + 6);
    }
%>

<%@page import="java.util.ArrayList" %>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Edit Recharge Card Group</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/rechargecard/editRechargecardGroup" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Edit Recharge Card Group: <%=request.getParameter("name")%></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="RechargecardGroupForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Group Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupName"  readonly="true" /><br/>
                                        <html:messages id="groupName" property="groupName">
                                            <bean:write name="groupName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Group Description</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupDes"  /><br/>
                                        <html:messages id="groupDes" property="groupDes">
                                            <bean:write name="groupDes"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Country ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupCountryName"  readonly="true" /><br/>
                                        <html:messages id="groupCountryName" property="groupCountryName">
                                            <bean:write name="groupCountryName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Package Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupPackageName" readonly="true" /><br/>
                                        <html:messages id="groupPackageName" property="groupPackageName">
                                            <bean:write name="groupPackageName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                         
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card Prefix</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupPrefix" readonly="true" /><br/>
                                        <html:messages id="groupPrefix" property="groupPrefix">
                                            <bean:write name="groupPrefix"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Min. Card Length</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupLength" readonly="true" /><br/>
                                        <html:messages id="groupLength" property="groupLength">
                                            <bean:write name="groupLength"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card Amount</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupAmount" readonly="true" /><br/>
                                        <html:messages id="groupAmount" property="groupAmount">
                                            <bean:write name="groupAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Total Cards</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupTotal" readonly="true" /><br/>
                                        <html:messages id="groupTotal" property="groupTotal">
                                            <bean:write name="groupTotal"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Created By</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupCreatedByName" readonly="true" /><br/>
                                        <html:messages id="groupCreatedByName" property="groupCreatedByName">
                                            <bean:write name="groupCreatedByName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Created At</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupCreatedTimeStr" readonly="true" /><br/>
                                        <html:messages id="groupCreatedTimeStr" property="groupCreatedTimeStr">
                                            <bean:write name="groupCreatedTimeStr"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                         
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <html:hidden property="id" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Edit" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                        <input type="hidden" name="searchLink" value="<%=link%>" />
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>