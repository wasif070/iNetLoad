<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
                    && perDTO != null && !perDTO.RCS)
                    || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.rechargecard.CardShopDAO,com.myapp.struts.rechargecard.CardShopLoader,java.util.ArrayList,com.myapp.struts.rechargecard.CardShopDTO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 100;
   long id = -1;
   String name = " ";   

   if (request.getSession(true).getAttribute(Constants.CARD_SHOP_SUMMERY_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.CARD_SHOP_SUMMERY_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   if (request.getParameter("id") != null) {
     id = Long.parseLong(request.getParameter("id"));
   }
   if (request.getParameter("name") != null) {
     name = request.getParameter("name");
   }   
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../rechargecard/cardShopSummery.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value+"&id=<%=id%>&name=<%=name%>";
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Card Shop Summery</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/rechargecard/cardShopSummery.do" method="post" >
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Group Name</th>
                                        <td>
                                            <html:text property="groupName">
                                            </html:text>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Package Type</th>
                                        <td>
                                            <html:select property="groupPackage">
                                                <html:option value="-1">Select</html:option>
                                                <%
                                                 for(int i=0;i<Constants.PACKAGE_ID_VALUE.length;i++)
                                                 {
                                                %>
                                                <html:option value="<%=Constants.PACKAGE_ID_VALUE[i]%>"><%=Constants.PACKAGE_ID_STRING[i]%></html:option>
                                                <%
                                                 }
                                                %>
                                            </html:select>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <th>Card Amount</th>
                                        <td>
                                            <html:select property="sign" style="width:47px">
                                                <html:option value="0">All</html:option>
                                                <html:option value="1">=</html:option>
                                                <html:option value="2"><</html:option>
                                                <html:option value="3">></html:option>
                                            </html:select>
                                            <html:text property="groupAmount" style="width:100px" />
                                        </td>                      
                                    </tr>
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>    
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                            %>
                        </div>
                        <div style="border : 1px solid #848284;" align="center">
                            <div class="height-10px"></div>
                            <div class="height-30px"><B>Card Shop: <%=name%></B></div>
                            <script type="text/javascript">
                                var total_distributed_amount=0;
                                var total_used_amount=0;
                                var total_remaining_amount=0;
                            </script>                            
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.RechargecardForm.cardSummeryList" pagesize="<%=recordPerPage%>">
                                <display:setProperty name="paging.banner.item_name" value="Card Shop Summery Entry" />
                                <display:setProperty name="paging.banner.items_name" value="Card Shop Summery Entries" />
                                <display:column title="Group Name" property="groupName" sortable="true" style="width:20%;" />                     
                                <display:column class="custom_column1" property="groupPackageName" title="Package Type" sortable="true" style="width:12%" />
                                <display:column class="custom_column2" property="groupAmount" title="Card Amount" format="{0,number,0.00}" sortable="true" style="width:12%" />
                                <display:column class="custom_column2" property="cardPrice" title="Distribution Price"  format="{0,number,0.00}" sortable="true" style="width:15%" />
                                <display:column class="custom_column2" property="totalDistributed" title="Distributed"  sortable="true" style="width:15%" />
                                <display:column class="custom_column2" property="totalUsed" title="Used" sortable="true" style="width:12%" />
                                <display:column class="custom_column2" title="Remaining" sortable="true" style="width:14%" >
                                    ${data.totalDistributed-data.totalUsed}
                                    <script type="text/javascript">
                                        total_distributed_amount = total_distributed_amount+(${data.totalDistributed}*${data.cardPrice});
                                        total_used_amount = total_used_amount+(${data.totalUsed}*${data.cardPrice});
                                        total_remaining_amount=total_remaining_amount+(${data.totalDistributed-data.totalUsed}*${data.cardPrice});                                   
                                    </script>                                    
                                </display:column>    
                            </display:table>
                            <div class="clear"></div>
                            <div class="three-fourth-div" align="center">
                                <table  cellspacing="0" border="1" cellpadding="0" style="width:65%; background-color: #FFFFDD;" >
                                    <tr>
                                        <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="25%">Distributed Amount:</td>
                                        <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                            <script type="text/javascript">
                                                document.write(parseInt(total_distributed_amount+"")+".00");
                                            </script>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="25%">Used Amount:</td>
                                        <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                            <script type="text/javascript">
                                                document.write(parseInt(total_used_amount+"")+".00");
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="color: #2D4173; font-weight: bold; padding-right: 5px;border-width: 2px;" align="right" width="25%">Remaining Amount:</td>
                                        <td style="color: #1d5987; font-weight: bold; border-width: 2px;" align="right" width="35%">
                                            <script type="text/javascript">
                                                document.write(parseInt(total_remaining_amount+"")+".00");
                                            </script>
                                        </td>
                                    </tr>                              
                                </table>
                            </div>                            
                            <script type="text/javascript">highlightTableRows("data");</script>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>