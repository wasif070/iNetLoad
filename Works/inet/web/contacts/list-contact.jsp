<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,com.myapp.struts.contacts.ContactDTO" %>

<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE || login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            response.sendRedirect("../home/home.do");
            return;
        }
    }
%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />


<%
    int pageNo = 1;
    int sortingOrder = 0;
    int sortedItem = 0;
    int list_all = 0;
    int recordPerPage = 250;

    if (request.getSession(true).getAttribute(Constants.CONTACT_RECORD_PER_PAGE) != null) {
        recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.CONTACT_RECORD_PER_PAGE).toString());
    }
    if (request.getParameter("d-49216-p") != null) {
        pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
    }
    if (request.getParameter("d-49216-s") != null) {
        sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
    }
    if (request.getParameter("d-49216-o") != null) {
        sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
    }
    if (request.getParameter("list_all") != null) {
        list_all = Integer.parseInt(request.getParameter("list_all"));
    }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }
    
    function setAmount()
    {
        if(document.forms[0].changeAmount.checked==true)
        {    
            var allInputArray = document.getElementsByTagName("input");
            var newAmount = document.forms[0].newAmount.value;
            for(i=0; i<allInputArray.length; i++)
            {
                if(allInputArray[i].name == "amounts")
                {
                    allInputArray[i].value = newAmount;
                }
            }     
        }
    }    

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j= 0;
        var k = 0;
        var l = 0;
        var amountFlag = false;
        var selectedIDsArray = [];
        var amountIDsArray = [];
        var amountArray = [];        
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j] = i;
                    j++;
                    amountFlag = true;
                }
            }
            else if(allInputArray[i].name == "amounts")
            {
                amountIDsArray[l] = i;
                l++;
                if(amountFlag)
                {
                    tempInputbox = allInputArray[i];
                    amountArray[k]=tempInputbox.value;
                    k++;
                    amountFlag = false;
                }
            }
        }
        document.forms[0].reset(); 
        for(t=0; t<allInputArray.length; t++)
        {
                            
            checkFlag = 0;
            for(i=0; i<selectedIDsArray.length; i++)
            {
                if(t==selectedIDsArray[i])
                {
                    checkFlag = 1;
                    break;
                }
            }
            if(checkFlag==1)
            {
                tempCheckbox = allInputArray[selectedIDsArray[i]];
                tempCheckbox.checked = true;
                tempInputbox = allInputArray[amountIDsArray[i]];
                tempInputbox.value = amountArray[i];
            }
            else
            {
                tempCheckbox = allInputArray[t];
                tempCheckbox.checked = false;
            }
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }    
        link = "../contacts/listContact.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../contacts/listContact.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editContact(value1,value2,value3)
    {
        document.forms[0].reset();
        link = "contactNamePar="+document.forms[0].contactNamePar.value+"&contactNoPar="+document.forms[0].contactNoPar.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = "../contacts/getContact.do?id="+value1+"&name="+value2+"&groupID="+value3+"&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Contact List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <%
                      if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT)    
                      {
                    %>                                          
                    <div class="blank-height" align="right">
                        <a href="../contacts/add-contact.jsp?groupID=<%=request.getParameter("groupID")%>"><font color="blue" style="text-decoration: underline">Add Contact</u></a>&nbsp;|&nbsp;<a href="../contacts/upload-contacts.jsp?groupID=<%=request.getParameter("groupID")%>"><u>Upload Contacts</u></a>
                    </div>
                    <%
                       }
                    %>                                       
                    <html:form action="/contacts/listContact.do" method="post">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">                                                                      
                                    <tr>
                                        <th>Contact Name</th>
                                        <td>
                                            <html:text property="contactNamePar" />
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <th>Contact No</th>
                                        <td>
                                            <html:text property="contactNoPar" />
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="full-div">
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-30px">
                            <%
                                if (request.getSession(true).getAttribute(Constants.MESSAGE) != null) {
                                    out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                } 
                            %>
                        </div>
                        <div style="border : 1px solid #848284;" align="center">
                        <jsp:scriptlet>
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                            public String addRowClass() {
                                ContactDTO obj = (ContactDTO) getCurrentRowObject();
                                if (obj.getStatusID() == Constants.USER_STATUS_BLOCK) {
                                    return "red";
                                } else {
                                    return "";
                                }
                            }
                        });
                        </jsp:scriptlet>                            
                            <div class="blank-height"></div>
                            <script type="text/javascript">var count=<%=(pageNo - 1) * recordPerPage%>;</script>
                            <display:table class="reporting_table" decorator="dyndecorator" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ContactForm.contactList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Contact" />
                                <display:setProperty name="paging.banner.items_name" value="Contacts" />
                                <display:column class="custom_column1" media="html" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" media="html" style="width:7%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Contact Name" sortable="true" style="width:16%" media="html"  >  
                                    <a href="#" onclick="editContact('${data.id}','${data.contactName}','${data.groupID}')"><u>${data.contactName}</u></a>                                      
                                </display:column> 
                                <display:column  title="Contact Name" property="contactName" sortable="true" style="width:16%" media="csv excel pdf"  /> 
                                <display:column  class="custom_column1" property="contactNo" title="Contact No" sortable="true" style="width:15%"  media="html excel pdf" />
                                <display:column  property="contactDescription" title="Description" sortable="true" style="width:15%" />
                                <display:column  class="custom_column1" property="contactNoWithPrefix" title="Contact No" sortable="true" style="width:15%"  media="csv" />
                                <display:column  class="custom_column1" title="Refill Amount" sortable="true" media="html" style="width:17%" >
                                    <input type="text" name="amounts" style="width: 80px;border-color: #848284; border-style: solid;border-width: 1px;" value="${data.refillAmount}" />
                                </display:column> 
                                <display:column  property="refillType"  title="Refill Type" sortable="true" style="width:12%" media="csv" />    
                                <display:column  class="custom_column1" property="refillAmount" title="Refill Amount" sortable="true" media="csv excel pdf" style="width:15%" />    
                                <display:column  property="groupName"  sortProperty="groupID" title="Group Name" sortable="true" style="width:15%" media="html excel pdf" />
                                <display:column  property="statusName"  sortProperty="statusID" title="Status" sortable="true" style="width:12%" media="html excel pdf" />                               
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.CONTACT_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <%
                                request.removeAttribute(Constants.CONTACT_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                                if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT)
                                {    
                            %>
                            <BR>
                                <input type="checkbox" name="changeAmount" style="width: 20px; vertical-align: middle"><font style="color: #363636; font-weight: bold;">Change Refill Amount To</font> <input type="text" name="newAmount" style="width: 60px;border-color: #848284; border-style: solid;border-width: 1px;" value="0.0" onkeyup="setAmount();" />
                                    <BR><BR>
                                            <input type="submit" class="action-button" value="Refill Selected" style="width: 115px;" onclick="submitform('<%=Constants.RECHARGE%>')" />
                                            <input type="submit" class="action-button" value="Activate Selected" style="width: 120px;" onclick="submitform('<%=Constants.ACTIVATION%>')" /> 
                                            <input type="submit" class="action-button" value="Block Selected" style="width: 115px;" onclick="submitform('<%=Constants.BLOCK%>')" /> 
                                            <input type="submit" class="action-button" value="Delete Selected" style="width: 115px;" onclick="submitform('<%=Constants.DELETE%>')" />                                  
                                            <%
                                                                               }
                                            %>
                                            <div class="blank-height"></div>
                                            </div>
                                            <div class="blank-height"></div>
                                            <html:hidden property="groupID" />
                                        </html:form>
                                        </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div><%@include file="../includes/footer.jsp"%></div>
                                        </div>
                                        </body>
                                        </html>