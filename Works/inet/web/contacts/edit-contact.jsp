<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
            response.sendRedirect("../home/home.do");
            return;
        }
    }

    String link = request.getQueryString();
    if (request.getParameter("searchLink") != null) {
        link = request.getParameter("searchLink");
    } else {
        link = "?" + link.substring(link.indexOf("&link=") + 6);
    }

%>

<%@page import="com.myapp.struts.client.ClientLoader,com.myapp.struts.client.ClientDTO" %>
<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,java.util.ArrayList" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Edit Contact</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/contacts/editContact" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Edit Contact: <font color="#e17009"><%=request.getParameter("name")%></font></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="ContactForm" property="message" filter="false"/>
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th>Contact Name</th>
                                    <td>
                                        <html:text property="contactName"  />
                                        <html:messages id="contactName" property="contactName">
                                            <bean:write name="contactName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>
                                        <html:text property="contactDescription"  />
                                        <html:messages id="contactDescription" property="contactDescription">
                                            <bean:write name="contactDescription"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Contact Number</th>
                                    <td>
                                        <html:text property="contactNo"  />
                                        <html:messages id="contactNo" property="contactNo">
                                            <bean:write name="contactNo"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th>Refill Type</th>
                                    <td>
                                        <html:select property="refillType" >
                                            <html:option value="<%=String.valueOf(Constants.REFILL_TYPE_PREPAID)%>"><%=Constants.REFILL_TYPE[Constants.REFILL_TYPE_PREPAID]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.REFILL_TYPE_POSTPAID)%>"><%=Constants.REFILL_TYPE[Constants.REFILL_TYPE_POSTPAID]%></html:option>
                                        </html:select>
                                        <html:messages id="refillType" property="refillType">
                                            <bean:write name="refillType"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th>Refill Amount</th>
                                    <td>
                                        <html:text property="refillAmount" />
                                        <html:messages id="refillAmount" property="refillAmount">
                                            <bean:write name="refillAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <html:hidden property="groupID" value="<%=request.getParameter("groupID")%>" />
                                        <input type="hidden" name="searchLink" value="<%=link%>" />
                                        <html:hidden property="id" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Edit" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>