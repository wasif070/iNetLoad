<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.FLEXI_SIM_RECHARGE && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
            int pageNo = 1;
            int sortingOrder = 0;
            int sortedItem = 0;
            int recordPerPage = 100;
            int list_all = 0;

            if (request.getSession(true).getAttribute(Constants.FLEXI_SIM_HISTORY_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_SIM_HISTORY_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }
            if (request.getParameter("d-49216-s") != null) {
                sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
            }
            if (request.getParameter("d-49216-o") != null) {
                sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
            }
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j = 0;
        var k = 0;
        var l = 0;

        var descriptionFlag = false;
        var selectedIDsArray = [];
        var descriptionIDsArray = [];
        var descriptionArray = [];
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j] = i;
                    j++;
                    descriptionFlag = true;
                }
            }
            else if(allInputArray[i].name == "descriptions")
            {
                descriptionIDsArray[k] = i;
                k++;
                if(descriptionFlag)
                {
                    tempInputbox = allInputArray[i];
                    descriptionArray[l]=tempInputbox.value;
                    l++;
                    descriptionFlag = false;
                }
            }
        }
        document.forms[0].reset();
        for(t=0; t<allInputArray.length; t++)
        {

            checkFlag = 0;
            for(i=0; i<selectedIDsArray.length; i++)
            {
                if(t==selectedIDsArray[i])
                {
                    checkFlag = 1;
                    break;
                }
            }
            if(checkFlag==1)
            {
                tempCheckbox = allInputArray[selectedIDsArray[i]];
                tempCheckbox.checked = true;
                tempInputbox = allInputArray[descriptionIDsArray[i]];
                tempInputbox.value = descriptionArray[i];
            }
            else
            {
                tempCheckbox = allInputArray[t];
                tempCheckbox.checked = false;
            }
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }

        link = "../gateway/listRechargeGateway.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../gateway/listRechargeGateway.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: SIM Recharge History</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>              
                <div class="view-page-body">
                    <div class="blank-height" align="right"><a href="../gateway/rechargeGateway.do?list_all=1"><u>Add Recharge History</u></a></div>
                    <html:form action="/gateway/listRechargeGateway.do" method="post">
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Start Time</th>
                                    <td>
                                        <html:select property="startDay" styleClass="width-50px" styleId="startDay">
                                            <%
                                                        ArrayList<Integer> days = Utils.getDay();
                                                        for (int i = 0; i < days.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startMonth" styleClass="width-50px" styleId="startMonth">
                                            <%
                                                        ArrayList<String> months = Utils.getMonth();
                                                        for (int i = 0; i < months.size(); i++) {
                                                            String month = months.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startYear" styleClass="width-60px" styleId="startYear">
                                            <%
                                                        ArrayList<Integer> years = Utils.getYear();
                                                        for (int i = 0; i < years.size(); i++) {
                                                            String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>Phone No</th>
                                    <td>
                                        <html:text property="phoneNoPar" styleId="phoneNo"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>End Time</th>
                                    <td>
                                        <html:select property="endDay" styleClass="width-50px" styleId="endDay">
                                            <%
                                                        ArrayList<Integer> days1 = Utils.getDay();
                                                        for (int i = 0; i < days1.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endMonth" styleClass="width-50px" styleId="endMonth">
                                            <%
                                                        ArrayList<String> months1 = Utils.getMonth();
                                                        for (int i = 0; i < months1.size(); i++) {
                                                            String month = months1.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endYear" styleClass="width-60px" styleId="endYear">
                                            <%
                                                        ArrayList<Integer> years1 = Utils.getYear();
                                                        for (int i = 0; i < years1.size(); i++) {
                                                            String year = String.valueOf(years1.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>Operator</th>
                                    <td>
                                        <html:select property="operatorIDPar">
                                            <html:option value="-1" >Select</html:option>
                                            <html:option value="<%=String.valueOf(Constants.GP)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.GP]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.BL)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.BL]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.WR)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.WR]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.RB)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.RB]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.TT)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.TT]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.CC)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.CC]%></html:option>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="pageNo" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div class="full-div" style="text-align: center;">
                                            <div class="clear height-5px"></div>
                                            <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                            <html:reset styleClass="search-button" value="Reset" />
                                            <div class="clear height-5px"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clear height-20px">
                            <%
                                        if (request.getSession(true).getAttribute(Constants.MESSAGE) != null) {
                                            out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                        }
                                        boolean gatewayRechargeAddPer = false;
                                        if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.FLEXI_SIM_RECHARGE_ADD && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                                            gatewayRechargeAddPer = true;
                                        }
                                        boolean gatewayRechargeEditPer = false;
                                        if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.FLEXI_SIM_RECHARGE_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                                            gatewayRechargeEditPer = true;
                                        }
                                        boolean gatewayRechargeDeletePer = false;
                                        if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.FLEXI_SIM_RECHARGE_DELETE && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                                            gatewayRechargeDeletePer = true;
                                        }
                            %>
                        </div>          
                        <div style="border : 1px solid #848284;" align="center">
                            <div class="blank-height"></div>
                            <script type="text/javascript">
                                var total_amount=0;
                                var count=<%=(pageNo - 1) * recordPerPage%>;
                            </script>
                            <display:table class="reporting_table" defaultsort="7" defaultorder="descending" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.GatewayForm.gatewayList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Recharge" />
                                <display:setProperty name="paging.banner.items_name" value="Recharges" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%" media="html">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:6%;" media="html">
                                    <script type="text/javascript">
                                        total_amount=total_amount+${data.rechargeAmount};
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  property="phoneNo" title="Phone No." sortable="true" style="width:14%;" />
                                <display:column  class="custom_column1" property="simID" title="Sim Id" sortable="true" style="width:13%;" />
                                <display:column  property="operatorName" title="Operator" sortable="true" style="width:14%;" />
                                <display:column class="custom_column2" property="rechargeAmount" title="Amount" format="{0,number,0.00}" sortable="true" style="width:12%;" />
                                <display:column class="custom_column1" property="rechargeTime" title="Log Time" decorator="implement.displaytag.LongDateWrapper" sortable="true" style="width:13%;" />
                                <display:column property="rechargeDescription" title="Description" style="width:12%;" />
                                <display:column title="Description" sortable="false" class="custom_column1" style="width:12%" media="html">
                                    <input type="text" name="descriptions" style="width: 80px;border-color: #848284; border-style: solid;border-width: 1px;" value=" " />
                                </display:column>
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.FLEXI_SIM_HISTORY_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                        <div class="clear"></div>
                        <div class="half-div">
                            <table class="reporting_table" cellspacing="0" border="0" cellpadding="0" style="width:100%;">
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold" align="center" >Total Recharge Amount :&nbsp;
                                        <script type="text/javascript">
                                            document.write(parseInt(total_amount+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                            </table>
                        </div>
                            <div class="clear height-5px"></div>
                            <%
                                        request.removeAttribute(Constants.FLEXI_SIM_HISTORY_ID_LIST);
                                        request.getSession(true).removeAttribute(Constants.MESSAGE);
                                        if (gatewayRechargeEditPer) {
                            %>
                            <input type="submit" class="action-button" value="Update Selected" style="width: 125px;" onclick="submitform('<%=Constants.UPDATE%>')" />
                            <%
                                        }
                                        if (gatewayRechargeDeletePer) {
                            %>
                            <input type="submit" class="action-button" value="Delete Selected" style="width: 115px;" onclick="submitform('<%=Constants.DELETE%>')" />
                            <%
                                        }
                            %>
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

