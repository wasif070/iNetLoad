<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.FLEXI_SIM_SETTINGS && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 100;

   if (request.getSession(true).getAttribute(Constants.FLEXI_SIM_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_SIM_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j=0;
        var selectedIDsArray = [];
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j]=i;
                    j++;
                }
            }
        }
        document.forms[0].reset();
        for(i=0; i<selectedIDsArray.length; i++)
        {
            tempCheckbox = allInputArray[selectedIDsArray[i]];
            tempCheckbox.checked = true;
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }

        link = "../gateway/listGateway.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../gateway/listGateway.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editGateway(value1,value2)
    {
        document.forms[0].reset();
        link = "phoneNoPar="+document.forms[0].phoneNoPar.value+"&simIDPar="+document.forms[0].simIDPar.value+"&operatorIDPar="+document.forms[0].operatorIDPar.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = "../gateway/getGateway.do?id="+value1+"&name="+value2+"&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Flexi SIM List</title>
    </head>
    <body style="height: 100%; ">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <div class="blank-height" align="right"><a href="../gateway/add-gateway.jsp"><u>Add Flexi SIM</u></a></div>
                    <html:form action="/gateway/listGateway.do" method="post">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Phone No</th>
                                        <td><html:text property="phoneNoPar" /></td>
                                    </tr>
                                    <tr>
                                        <th>SIM ID</th>
                                        <td><html:text property="simIDPar" /></td>
                                    </tr>
                                    <tr>
                                        <th>Operator</th>
                                        <td>
                                            <html:select property="operatorIDPar">
                                                <html:option value="-1" >Select</html:option>
                                                <html:option value="<%=String.valueOf(Constants.GP)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.GP]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.BL)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.BL]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.WR)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.WR]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.RB)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.RB]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.TT)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.TT]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.CC)%>" ><%=Constants.OPERATOR_TYPE_NAME[Constants.CC]%></html:option>                                                
                                            </html:select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="full-div">
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                              boolean gatewayEditPer = false;
                              if((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.FLEXI_SIM_SETTINGS_EDIT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE))
                              {
                                 gatewayEditPer = true;
                              }
                              boolean gatewayDeletePer = false;
                              if((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.FLEXI_SIM_SETTINGS_DELETE && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE))
                              {
                                 gatewayDeletePer = true;
                              }
                            %>
                        </div>
                        <c:set var="gatewayEdit" scope="page" value="<%=gatewayEditPer%>"/>
                        <div style="border : 1px solid #848284;" align="center">
                            <div class="blank-height"></div>
                            <script type="text/javascript">var count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" defaultsort="5"  cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.GatewayForm.gatewayList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Gateway" />
                                <display:setProperty name="paging.banner.items_name" value="Gateways" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%" media="html">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:7%;" media="html">
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  class="custom_column1" title="SIM ID" sortProperty="simID" sortable="true"  style="width:15%" media="html">
                                    <c:choose>
                                        <c:when test="${gatewayEdit==false}">${data.simID}</c:when>
                                        <c:otherwise><a href="#" onclick="editGateway('${data.id}','${data.simID}')"><u>${data.simID}</u></a></c:otherwise>
                                    </c:choose>                                    
                                </display:column>
                                <display:column  class="custom_column1" title="SIM ID" property="simID" media="csv excel pdf" />        
                                <display:column  property="phoneNo" title="Phone No" sortable="true" style="width:20%" />
                                <display:column  property="operatorName" sortProperty="operatorID" title="Operator" sortable="true" style="width:20%" />
                                <display:column  class="custom_column2" property="balance" title="Balance" sortable="true" style="width:18%" />
                                <display:column class="custom_column1" title="Status" sortProperty="gatewayStatus" sortable="true" style="width:15%" >
                                    <c:choose>
                                        <c:when test="${data.gatewayStatus==1}">Active</c:when>
                                        <c:otherwise>Inactive</c:otherwise>
                                    </c:choose>
                                </display:column>
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.FLEXI_SIM_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <%
                                request.removeAttribute(Constants.FLEXI_SIM_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                                if(gatewayEditPer)
                                {
                            %>
                            <div class="clear height-5px"></div>
                            <input type="submit" class="action-button" value="Activate Selected" style="width: 125px;" onclick="submitform('<%=Constants.ACTIVATION%>')" />
                            <input type="submit" class="action-button" value="Inactivate Selected" style="width: 135px;" onclick="submitform('<%=Constants.DEACTIVATION%>')" />
                            <%
                                }
                                if(gatewayDeletePer)
                                {
                            %>
                            <input type="submit" class="action-button" value="Delete Selected" style="width: 115px;" onclick="submitform('<%=Constants.DELETE%>')" />
                            <%
                                }
                            %>
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>