<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.client.ClientDAO,com.myapp.struts.payment.PaymentDTO,java.util.ArrayList" %>
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<script language="javascript" type="text/javascript">

    window.onload=function(){
        countLoadRequest();
        countBalanceRequest();
        countSMSRequest();
        creditSummaryRequest();
    }

    function countLoadRequest(){
        $.ajax({
            url:"../includes/load-count.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random()},
            cache: false,
            dataType:'html',
            async:false,
            beforeSend:function(XMLHttpRequest){
            },
            success:function(html) {
                if(html!=''){
                    $(".load-count").html($.trim(html));
                    setTimeout('countLoadRequest();',30000);
                }
            }
        });
    }
    
    function countBalanceRequest(){
        $.ajax({
            url:"../includes/balance-transfer-count.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random()},
            cache: false,
            dataType:'html',
            async:false,
            beforeSend:function(XMLHttpRequest){
            },
            success:function(html) {
                if(html!=''){
                    $(".balance-transfer-count").html($.trim(html));
                    setTimeout('countLoadRequest();',30000);
                }
            }
        });
    }
    
    function countSMSRequest(){
        $.ajax({
            url:"../includes/sms-count.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random()},
            cache: false,
            dataType:'html',
            async:false,
            beforeSend:function(XMLHttpRequest){
            },
            success:function(html) {
                if(html!=''){
                    $(".sms-count").html($.trim(html));
                    setTimeout('countLoadRequest();',30000);
                }
            }
        });
    }

    function creditSummaryRequest(){
        $.ajax({
            url:"../includes/credit-summary.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random()},
            cache: false,
            dataType:'html',
            async:false,
            success:function(html) {
                if(html!=''){
                    $(".credit-summary").html($.trim(html));
                    setTimeout('creditSummaryRequest();',30000);
                }
            }
        });
    }

    function getSID(){
        var sid=new Date();
        return sid.getTime();
    }

</script>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Home</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>                
                <%--
                <div align="right" class="blank-height" style="padding-right: 15px;"><a href="../includes/old-report.jsp"><u>Old Report</u></a></div>
                --%>
                <div style="height: 40px"></div>
                <div class="body">
                    <table class="input_table" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <td colspan="2" align="center">
                                    <div class="clear height-5px"></div>
                                    <div class="full-div">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet"><%=SettingsLoader.getInstance().getSettingsDTO().getWelcomeNote()%></span><br>
                                            <%
                                                if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                                    if (login_dto.getIsUser()) {
                                            %>
                                            <a href='../user/getUser.do?id=<%=login_dto.getId()%>&name=<%=login_dto.getClientId()%>' style="font-weight: bold">[ <u>Edit Profile</u> ]</a>
                                            <%
                                            } else {
                                            %>
                                            <a href='../client/getClient.do?id=<%=login_dto.getId()%>&name=<%=login_dto.getClientId()%>' style="font-weight: bold">[ <u>Edit Profile</u> ]</a>
                                            <%
                                                    }
                                                }
                                            %>
                                    </div>
                                    <div class="clear height-20px"></div>
                                    <%
                                        if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (perDTO != null && perDTO.REFILL)) {
                                    %>
                                    <div class="full-div"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-weight: bold"><u>Pending Refill Requests</u> (<span class="load-count blue">0</span>)</a></div>
                                    <div class="full-div" style="margin-bottom:5px;margin-top:5px;"><a href="../flexiload/listBalanceTrans.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-weight: bold"><u>Pending Balance Transfer Requests</u> (<span class="balance-transfer-count blue">0</span>)</a></div>
                                    <div class="full-div"><a href="../sms/listSMS.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="font-weight: bold"><u>Pending SMS Requests</u> (<span class="sms-count blue">0</span>)</a></div>
                                    <%
                                        }
                                    %>
                                    <div class="clear height-20px"></div>
                                    <%
                                        if (!login_dto.getIsUser()) {
                                    %>
                                    <div class="full-div"><span class="credit-summary blue"></span></div>
                                    <div class="clear height-20px"></div>
                                    <%                                                }
                                        ClientDAO clDAO = new ClientDAO();
                                        ArrayList summeryList = clDAO.getSummeryDTOs(login_dto);
                                        if (summeryList != null && summeryList.size() > 0) {
                                            double totalPaid = 0.0;
                                            double totalUsed = 0.0;
                                            double totalPreviousBalance = 0.0;
                                            double totalBalance = 0.0;
                                    %>
                                    <table style="width:85%;border: 1px solid #6c6c6c;;" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="6" align="center"  style="width:20%; font-size: 12px; font-weight: bold; color: blue;border-bottom:  1px solid #6c6c6c;">My Transaction Summery</td>
                                        </tr>
                                        <tr>
                                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Client ID</td>
                                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Type</td>
                                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Previous Balance</td>
                                            <%
                                                if (!login_dto.getIsUser() && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                                            %>
                                            <td align="center"  style="width:20%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Total Received</td>
                                            <%                                                                                                } else {
                                            %>
                                            <td align="center"  style="width:20%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Total Paid</td>
                                            <%                                                                                                }
                                            %>
                                            <td align="center"  style="width:20%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Total Used</td>                                           
                                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-bottom: 1px solid #6c6c6c;">Current Balance</td>
                                        </tr>
                                        <%
                                            for (int i = 0; i < summeryList.size(); i++) {
                                                PaymentDTO pdto = (PaymentDTO) summeryList.get(i);
                                                totalPaid += pdto.getPaymentAmount();
                                                totalUsed += pdto.getClientUses();
                                                totalPreviousBalance += pdto.getClientPreviousBalance();
                                        %>
                                        <tr>
                                            <td align="left"  style="width:15%; padding-left: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=pdto.getPaymentToClient()%></td>
                                            <td align="center"  style="width:15%; padding-left: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=pdto.getPaymentToClientTypeName()%></td>
                                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c; border-bottom:  1px solid #6c6c6c;"><%=(long) pdto.getClientPreviousBalance()%></td>
                                            <td align="right"  style="width:20%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=(long) pdto.getPaymentAmount()%></td>
                                            <td align="right"  style="width:20%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=(long) pdto.getClientUses()%></td>
                                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-bottom:  1px solid #6c6c6c;"><%=(long) (pdto.getClientBalance())%></td>
                                        </tr>
                                        <%
                                                totalBalance += (long) (pdto.getClientBalance());
                                            }
                                        %>
                                        <tr>
                                            <td align="left"  colspan="2" style="width:30%; text-align: center;padding-left: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;">Total</td>
                                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;"><%=(long) totalPreviousBalance%></td>
                                            <td align="right"  style="width:20%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;"><%=(long) totalPaid%></td>
                                            <td align="right"  style="width:20%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;"><%=(long) totalUsed%></td>                                      
                                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; "><%=(long) totalBalance%></td>                                      
                                        </tr>
                                    </table>
                                    <%                                                 }
                                    %>
                                    <div class="clear height-20px"></div>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="clear height-20px"></div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>