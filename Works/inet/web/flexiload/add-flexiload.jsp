<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.client.ClientDAO,java.util.ArrayList" %>

<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>
<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            response.sendRedirect("../home/home.do");
            return;
        } else {
            if (login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                response.sendRedirect("../home/home.do");
                return;
            }
        }
    }
    ClientDAO dao = new ClientDAO();
    double credit = dao.getClientCredit(login_dto.getId());
%>


<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Refill Request</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px" ><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    
                    <html:form action="/flexiload/addFlexiload.do" method="post" styleId="flexi_form">
                        <table class="input_table" cellspacing="0" cellpadding="0" style="width: 70%">
                            <thead>
                                <tr>
                                    <th colspan="2" align="center">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet;" >New Refill Request</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2" align="center">
                                        <span style="font-size: 16px; font-weight: bold; color: blue;" >Balance: <%=credit%></span>
                                    </th>
                                </tr>                                            
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="FlexiForm" property="message" filter="false"/>
                                    </td>
                                </tr>                                              
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Phone Number</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="phoneNumber" styleId="phoneNumber"  /><BR>
                                        <html:messages id="phoneNumber" property="phoneNumber">
                                            <bean:write name="phoneNumber"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Refill Amount</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="amount" styleId="amount"  /><BR>
                                        <html:messages id="amount" property="amount">
                                            <bean:write name="amount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Refill Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="refillType" >
                                            <html:option value="<%=String.valueOf(Constants.REFILL_TYPE_PREPAID)%>"><%=Constants.REFILL_TYPE[Constants.REFILL_TYPE_PREPAID]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.REFILL_TYPE_POSTPAID)%>"><%=Constants.REFILL_TYPE[Constants.REFILL_TYPE_POSTPAID]%></html:option>
                                        </html:select>
                                        <html:messages id="flexiType" property="flexiType">
                                            <bean:write name="flexiType"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <div class="blank-height"></div>
                                        <input type="hidden" name="balanceTransfer" value="0" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submitButton" type="button" class="custom-button" value="Send" onclick="submitConfirm();"/>
                                        <input type="reset" class="custom-button" value="Reset" />
                                        <div class="blank-height"></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>

                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

<div id="dialog-confirm" title="Recheck!!">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><div id="confirm-message" style="font-weight: normal; color:#e17009; font-size: 1.05em; text-align: left;margin-left: 50px"></div></p>
</div>

<script type="text/javascript" language="javascript">
    function submitConfirm(){
        var message="Are these information correct?<br>";
        message+="Phone No. : "+$("#phoneNumber").val();
        message+="<br>Amount : "+$("#amount").val()+"<br>";
        $("#confirm-message").html(message);
        $('#dialog-confirm').dialog('option','title','Please Recheck!!').dialog('open');
        return true;
    }
    $(function() {
        $("#dialog-confirm").dialog({autoOpen: false, modal: true, width:350,
            buttons: {
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Ok: function() {
                    $(this).dialog( "close" );
                    $("#flexi_form").submit();
                }
            }
        });
        // End Changed
    });
</script>
