<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.client.ClientDAO,java.util.ArrayList" %>

<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>
<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            response.sendRedirect("../home/home.do");
            return;
        } else {
            if (login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                response.sendRedirect("../home/home.do");
                return;
            }
        }
    }
    ClientDAO dao = new ClientDAO();
    double credit = dao.getClientCredit(login_dto.getId());
%>

<script language="javascript" type="text/javascript">

    window.onload=function(){
        surchargeSummaryRequest();
    }
    
    function surchargeSummaryRequest(){
        $.ajax({
            url:"../balancetransfer/surcharge-summary.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random(),btType:document.forms[0].refillType.value},
            cache: false,
            dataType:'html',
            async:false,
            success:function(html) {
                if(html!=''){                
                    $(".surcharge-summary").html($.trim(html));
                }
            }
        });
    }
    
    function getSID(){
        var sid=new Date();
        return sid.getTime();
    }    
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Balance Transfer Request</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px" ><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">

                    <html:form action="/flexiload/addBt.do" method="post" styleId="flexi_form">
                        <table width="100%">
                            <tr>
                                <td width="60%" valign="top" >
                                    <table width="100%" class="input_table" cellspacing="0" cellpadding="0" >                            
                                        <thead>
                                            <tr>
                                                <th colspan="2" align="center">
                                                    <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet;" >Balance Transfer Request</span>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="2" align="center">
                                                    <span style="font-size: 16px; font-weight: bold; color: blue;" >Balance: <%=credit%></span>
                                                </th>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                                    <bean:write name="FlexiForm" property="message" filter="false"/>
                                                </td>
                                            </tr>                                              
                                            <tr>
                                                <th valign="top" style="padding-top: 8px;">Account/Mobile Number</th>
                                                <td valign="top" style="padding-top: 6px;">
                                                    <html:text property="phoneNumber" styleId="phoneNumber"  /><BR>
                                                        <html:messages id="phoneNumber" property="phoneNumber">
                                                            <bean:write name="phoneNumber"  filter="false"/>
                                                        </html:messages>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th valign="top" style="padding-top: 8px;">Account Type</th>
                                                <td valign="top" style="padding-top: 6px;">
                                                    <html:select property="accountType">
                                                        <html:option value="<%=String.valueOf(Constants.ACCOUNT_TYPE_PERSONAL)%>"><%=Constants.ACCOUNT_TYPE[Constants.ACCOUNT_TYPE_PERSONAL]%></html:option>
                                                        <html:option value="<%=String.valueOf(Constants.ACCOUNT_TYPE_AGENT)%>"><%=Constants.ACCOUNT_TYPE[Constants.ACCOUNT_TYPE_AGENT]%></html:option>
                                                    </html:select>
                                                    <html:messages id="accountType" property="accountType">
                                                        <bean:write name="accountType"  filter="false"/>
                                                    </html:messages>
                                                </td>
                                            </tr>                                                        
                                            <tr>
                                                <th valign="top" style="padding-top: 8px;">Amount To Transfer</th>
                                                <td valign="top" style="padding-top: 6px;">
                                                    <html:text property="amount" styleId="amount"  /><BR>
                                                        <html:messages id="amount" property="amount">
                                                            <bean:write name="amount"  filter="false"/>
                                                        </html:messages>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th valign="top" style="padding-top: 8px;">Transfer Type</th>
                                                <td valign="top" style="padding-top: 6px;">
                                                    <html:select property="refillType" onchange="surchargeSummaryRequest();">
                                                        <html:option value="<%=String.valueOf(Constants.TRANSFER_TYPE_BKASH)%>"><%=Constants.TRANSFER_TYPE[Constants.TRANSFER_TYPE_BKASH]%></html:option>
                                                        <html:option value="<%=String.valueOf(Constants.TRANSFER_TYPE_DBBL)%>"><%=Constants.TRANSFER_TYPE[Constants.TRANSFER_TYPE_DBBL]%></html:option>
                                                    </html:select>
                                                    <html:messages id="flexiType" property="flexiType">
                                                        <bean:write name="flexiType"  filter="false"/>
                                                    </html:messages>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <td>
                                                    <div class="blank-height"></div>
                                                    <input type="hidden" name="balanceTransfer" value="1" />
                                                    <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                                    <input name="submitButton" type="button" class="custom-button" value="Send" onclick="submitConfirm();"/>
                                                    <input type="reset" class="custom-button" value="Reset" />
                                                    <div class="blank-height"></div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="35%" align="right">
                                    <div class="full-div"><span class="surcharge-summary blue"></span></div>
                                </td>
                            </tr>
                        </table>
                    </html:form>

                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

<div id="dialog-confirm" title="Recheck!!">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><div id="confirm-message" style="font-weight: normal; color:#e17009; font-size: 1.05em; text-align: left;margin-left: 50px"></div></p>
</div>

<script type="text/javascript" language="javascript">
    function submitConfirm(){
        var message="Are these information correct?<br>";
        message+="Account No. : "+$("#phoneNumber").val();
        message+="<br>Amount To Transfer : "+$("#amount").val()+"<br>";
        $("#confirm-message").html(message);
        $('#dialog-confirm').dialog('option','title','Please Recheck!!').dialog('open');
        return true;
    }
    $(function() {
        $("#dialog-confirm").dialog({autoOpen: false, modal: true, width:350,
            buttons: {
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Ok: function() {
                    $(this).dialog( "close" );
                    $("#flexi_form").submit();
                }
            }
        });
        // End Changed
    });
</script>
