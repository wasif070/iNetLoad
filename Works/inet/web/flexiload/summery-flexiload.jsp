<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO!=null && !perDTO.REPORT_REFILL_SUMMERY
                && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3
                && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT)
                 {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.flexi.FlexiDTO,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
            int pageNo = 1;
            int list_all = 0;
            int recordPerPage = SettingsLoader.getInstance().getSettingsDTO().getRecordPerPageForReporting();

            if (request.getSession(true).getAttribute(Constants.FLEXI_SUMMERY_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_SUMMERY_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }
            if (request.getParameter("list_all") != null) {
               list_all = Integer.parseInt(request.getParameter("list_all"));
            }
%>
<script language="javascript" type="text/javascript">

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../flexiload/summeryFlexiload.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

</script>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Refill Summery</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body1">
                    <div class="full-div height-5px"></div>
                    <div class="full-div"><strong>Refill Summery</strong></div>
                    <div class="clear"></div>
                    <div class="three-fourth-div" align="center">
                        <%
                            String action = "/flexiload/summeryFlexiload.do";
                        %>
                        <html:form action="<%=action%>" method="post">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0" style="width: 70%;">
                                <tr>
                                    <th>Start Time</th>
                                    <td>
                                        <html:select property="startDay" styleClass="width-50px" styleId="startDay">
                                            <%
                                                        ArrayList<Integer> days = Utils.getDay();
                                                        for (int i = 0; i < days.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startMonth" styleClass="width-50px" styleId="startMonth">
                                            <%
                                                        ArrayList<String> months = Utils.getMonth();
                                                        for (int i = 0; i < months.size(); i++) {
                                                            String month = months.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startYear" styleClass="width-60px" styleId="startYear">
                                            <%
                                                        ArrayList<Integer> years = Utils.getYear();
                                                        for (int i = 0; i < years .size(); i++) {
                                                            String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>End Time</th>
                                    <td>
                                        <html:select property="endDay" styleClass="width-50px" styleId="endDay">
                                            <%
                                                        ArrayList<Integer> days1 = Utils.getDay();
                                                        for (int i = 0; i < days1.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endMonth" styleClass="width-50px" styleId="endMonth">
                                            <%
                                                        ArrayList<String> months1 = Utils.getMonth();
                                                        for (int i = 0; i < months1.size(); i++) {
                                                            String month = months1.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endYear" styleClass="width-60px" styleId="endYear">
                                            <%
                                                        ArrayList<Integer> years1 = Utils.getYear();
                                                        for (int i = 0; i < years1.size(); i++) {
                                                            String year = String.valueOf(years1.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>                                        
                                <tr>    
                                    <th>Report Type</th>
                                    <td>
                                        <html:radio property="reportType" style="width:10px" value="0"></html:radio>Client Wise
                                        <html:radio property="reportType" value="1" style="width:10px"></html:radio>Date Wise
                                        <%--
                                        <%
                                          if (login_dto.getIsUser()) {                                        
                                        %>
                                        <html:radio property="reportType" value="2" style="width:10px"></html:radio>API Wise
                                        <%
                                                                                 }
                                        %>
                                        --%>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Request From</th>
                                    <td>
                                        <html:text property="fromClientId" styleId="fromClientId"/>
                                    </td>
                                </tr>                                     
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                </tr>
                                <tr>                                      
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="pageNo" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: center;">
                                        <html:submit property="startSearch" styleClass="search-button" value="Search" onclick="search()" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </html:form>
                    </div>
                    <div class="clear"></div>
                    <%
                       int reportType  = 0;
                       if(request.getParameter("reportType")!=null)
                        {
                           reportType = Integer.parseInt(request.getParameter("reportType"));
                        }
                    %>
 
                    <% if(reportType==2){  %>
                    <div>
                        <script type="text/javascript">
                            var total_amount=0;
                            var total_gp=0;
                            var total_rb=0;
                            var total_bl=0;
                            var total_ar=0;
                            var total_tt=0;
                            var total_cc=0;                              
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>
                        <display:table class="reporting_table" defaultsort="2" defaultorder="ascending" style="width:100%;" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.FlexiForm.flexiSummeryList" pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Record" />
                            <display:setProperty name="paging.banner.items_name" value="Records" />
                            <display:column class="custom_column2" title="Nr" style="width:6%;" media="html" >
                                <script type="text/javascript" >
                                    total_gp = total_gp+${data.grameenPhone};
                                    total_rb = total_rb+${data.robi};
                                    total_bl = total_bl+${data.banglaLink};
                                    total_ar = total_ar+${data.warid};
                                    total_tt = total_tt+${data.teleTalk};
                                    total_cc = total_cc+${data.cityCell};
                                    total_amount=total_amount+${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell};                                    
                                    document.write(++count+".");
                                </script>
                            </display:column>
                            <display:column property="clientId"  title="API ID" sortable="true" style="width:12%;" />
                            <display:column class="custom_column2" property="grameenPhone" title="Grameen Phone" format="{0,number,0.0}" sortable="true" style="width:12%;" />
                            <display:column class="custom_column2" property="robi" title="Robi" format="{0,number,0.0}" sortable="true" style="width:10%;" />
                            <display:column class="custom_column2" property="banglaLink" title="Bangla Link" format="{0,number,0.0}" sortable="true" style="width:14%;" />
                            <display:column class="custom_column2" property="warid" title="Airtel" format="{0,number,0.00}" sortable="true"  style="width:10%;" />
                            <display:column class="custom_column2" property="teleTalk" title="Tele Talk" format="{0,number,0.0}" sortable="true" style="width:11%;" />
                            <display:column class="custom_column2" property="cityCell" title="City Cell" format="{0,number,0.0}" sortable="true" style="width:11%;" />
                            <display:column class="custom_column2" property="sms" title="SMS" format="{0,number,0.0}" sortable="true" style="width:11%;" />
                            <display:column class="custom_column2" title="Total" sortable="true" format="{0,number,0.00}" style="width:14%;" >
                                ${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell}
                            </display:column>
                        </display:table>
                        <div class="clear"></div>
                        <div class="three-fourth-div" align="center">
                            <table  cellspacing="0" border="1" cellpadding="0" style="width:65%; background-color: #FFFFDD;" >
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="40%">GP Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_gp+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                
                               <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="40%">RB Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_rb+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="40%">BL Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_bl+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="40%">AR Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_ar+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="40%">TT Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_tt+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="40%">CC Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_cc+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #2D4173; font-weight: bold;  padding-right: 5px;border-width: 2px;" align="right" width="40%">Total Refill Amount:</td>
                                    <td style="color: #1d5987; font-weight: bold; border-width: 2px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_amount+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>  
                            </table>
                        </div>
                    </div>                   
                    <% } else if(reportType==1){  %>
                    <div>
                        <script type="text/javascript">
                            var total_amount=0;
                            var total_gp=0;
                            var total_rb=0;
                            var total_bl=0;
                            var total_ar=0;
                            var total_tt=0;
                            var total_cc=0; 
                            var total_bt=0;
                            var total_surcharge=0;
                            var total_sms=0;
                            //var total_card=0;
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>
                        <display:table class="reporting_table" defaultsort="2" defaultorder="descending" style="width:100%;" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.FlexiForm.flexiSummeryList" pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Record" />
                            <display:setProperty name="paging.banner.items_name" value="Records" />
                            <display:column class="custom_column2" title="Nr" style="width:4%;" media="html">
                                <script type="text/javascript">
                                    total_gp = total_gp+${data.grameenPhone};
                                    total_rb = total_rb+${data.robi};
                                    total_bl = total_bl+${data.banglaLink};
                                    total_ar = total_ar+${data.warid};
                                    total_tt = total_tt+${data.teleTalk};
                                    total_cc = total_cc+${data.cityCell};
                                    total_sms =total_sms+${data.sms};
                                    total_bt = total_bt+${data.balanceTransfer};
                                    total_surcharge = total_surcharge+${data.surcharge};
                                    //total_card = total_card+${data.card};
                                    //total_amount=total_amount+${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.card};  
                                    total_amount=total_amount+${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.balanceTransfer+data.surcharge};
                                    document.write(++count+".");
                                </script>
                            </display:column>
                            <display:column class="custom_column1" property="reportDate"  title="Date" format="{0,date,d MMM yyyy}" sortable="true"  style="width:11%;" />
                            <display:column class="custom_column2" property="grameenPhone" title="GP" format="{0,number,0.0}" sortable="true" total="true" style="width:9%;" />
                            <display:column class="custom_column2" property="robi" title="Robi" format="{0,number,0.0}" sortable="true" total="true" style="width:9%;" />
                            <display:column class="custom_column2" property="banglaLink" title="BL" format="{0,number,0.0}" sortable="true"  style="width:9%;" />
                            <display:column class="custom_column2" property="warid" title="Airtel" format="{0,number,0.0}" sortable="true"  style="width:9%;" />
                            <display:column class="custom_column2" property="teleTalk" title="TT" format="{0,number,0.0}" sortable="true"  style="width:9%;" />
                            <display:column class="custom_column2" property="cityCell" title="CC" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="sms" title="SMS" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="balanceTransfer" title="Balance Transfer" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="surcharge" title="Surcharge" format="{0,number,0.0}" sortable="true" style="width:12%;" />
                            <%--
                            <display:column class="custom_column2" property="card" title="Sc. Card" format="{0,number,0.0}" sortable="true" style="width:10%;" />
                            --%>
                            <display:column class="custom_column2" title="Total" sortable="true" format="{0,number,0.00}" style="width:10%;" >
                            ${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.balanceTransfer+data.surcharge+data.sms}
                            <%--    
                                ${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.card}
                            --%>
                            </display:column>
                        </display:table>
                        <div class="clear"></div>
                        <div class="three-fourth-div" align="center">
                            <table  cellspacing="0" cellpadding="0" style="width:65%; background-color: #FFFFDD; border-style:solid;border-color: #0c66ca; border-width: 1px;" >
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">GP Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_gp+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                
                               <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">RB Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_rb+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">BL Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_bl+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">AR Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_ar+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">TT Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_tt+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">CC Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_cc+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                  <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">SMS Charge Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(total_sms.toFixed(2)+" Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">Balance Transfer Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_bt+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>  
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">Balance Transfer Surcharge:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_surcharge+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>                                 
                                <%--
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="25%">Sc. Card Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_card+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>  
                                --%>
                              <tr>
                                    <td style="color: #2D4173; font-weight: bold;  padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 2px;" align="right" width="40%">Total Refill Amount:</td>
                                    <td style="color: #1d5987; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 2px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(total_amount.toFixed(2)+" Tk.");
                                        </script>
                                    </td>
                                </tr>                              
                                
                            </table>
                        </div>
                    </div>
                    <% } else { %>
                    <div>
                        <script type="text/javascript">
                            var total_amount=0;
                            var total_gp=0;
                            var total_rb=0;
                            var total_bl=0;
                            var total_ar=0;
                            var total_tt=0;
                            var total_cc=0;
                            var total_bt=0;
                            var total_surcharge=0;
                            var total_sms=0.0;
                            //var total_card=0;
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>
                        
                        <display:table class="reporting_table" defaultsort="2" defaultorder="ascending" style="width:100%;" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.FlexiForm.flexiSummeryList" pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Record" />
                            <display:setProperty name="paging.banner.items_name" value="Records" />
                            <display:column class="custom_column2" title="Nr" style="width:5%;" media="html" >
                                <script type="text/javascript" >
                                    total_gp = total_gp+${data.grameenPhone};
                                    total_rb = total_rb+${data.robi};
                                    total_bl = total_bl+${data.banglaLink};
                                    total_ar = total_ar+${data.warid};
                                    total_tt = total_tt+${data.teleTalk};
                                    total_cc = total_cc+${data.cityCell};
                                    total_bt = total_bt+${data.balanceTransfer};
                                    total_surcharge = total_surcharge+${data.surcharge};
                                    total_sms =total_sms+parseFloat(${data.sms});
                                    //total_card = total_card+${data.card};
                                    //total_amount=total_amount+${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.card};  
                                    total_amount=total_amount+${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.balanceTransfer+data.surcharge+data.sms};
                                    document.write(++count+".");
                                </script>
                            </display:column>
                            <display:column property="clientId"  title="Client ID" sortable="true" style="width:11%;" />
                            <display:column class="custom_column2" property="grameenPhone" title="GP" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="robi" title="Robi" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="banglaLink" title="BL" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="warid" title="Airtel" format="{0,number,0.00}" sortable="true"  style="width:9%;" />
                            <display:column class="custom_column2" property="teleTalk" title="TT" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="cityCell" title="CC" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="sms" title="SMS" format="{0,number,0.0}" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="balanceTransfer" title="Balance Transfer" format="{0,number,0.0}" sortable="true" style="width:10%;" />
                            <display:column class="custom_column2" property="surcharge" title="Surcharge" format="{0,number,0.0}" sortable="true" style="width:12%;" />
                            <%--
                            <display:column class="custom_column2" property="card" title="Sc. Card" format="{0,number,0.0}" sortable="true" style="width:10%;" />
                            --%>
                            <display:column class="custom_column2" title="Total" sortable="true" format="{0,number,0.00}" style="width:9%;" >
                            ${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.balanceTransfer+data.surcharge+data.sms}
                            <%--    
                                ${data.grameenPhone+data.robi+data.banglaLink+data.warid+data.teleTalk+data.cityCell+data.card}
                            --%>
                            </display:column>
                        </display:table>
                        <div class="clear"></div>
                        <div class="three-fourth-div" align="center">
                            <table  cellspacing="0"  cellpadding="0" style="width:65%; background-color: #FFFFDD; border-style:solid;border-color: #0c66ca; border-width: 1px; " >
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">GP Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_gp+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                
                               <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">RB Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_rb+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">BL Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_bl+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">AR Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_ar+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">TT Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_tt+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">CC Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_cc+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">SMS Charge Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(total_sms.toFixed(2)+" Tk.");
                                        </script>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="40%">Balance Transfer Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_bt+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>  
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="40%">Balance Transfer Surcharge:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-style:solid;border-color: #0c66ca; border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_surcharge+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>                                
                                <%--
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 1px;" align="right" width="40%">Sc. Card Refill Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 1px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(parseInt(total_card+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>  
                                --%>
                                <tr>
                                    <td style="color: #2D4173; font-weight: bold;  padding-right: 5px;border-style:solid;border-color:#0c66ca; border-width: 2px;" align="right" width="40%">Total Refill Amount:</td>
                                    <td style="color: #1d5987; font-weight: bold;border-style:solid;border-color:#0c66ca; border-width: 2px;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write(total_amount.toFixed(2)+" Tk.");
                                        </script>
                                    </td>
                                </tr>  
                            </table>
                        </div>
                    </div>
                    <% }%>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>