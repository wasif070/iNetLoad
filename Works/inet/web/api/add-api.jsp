<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE  && perDTO != null && !perDTO.FA_ADD && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
    String apiGPActive = "checked";
    String apiRBActive = "checked";
    String apiBLActive = "checked";
    String apiTTActive = "checked";
    String apiCCActive = "checked";
    String apiWRActive = "checked";
%>
<%@page import="com.myapp.struts.user.UserLoader,java.util.ArrayList" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add API</title>
    </head>    
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">                    
                    <html:form action="/api/addApi" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Add New API</span> 
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="APIForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">API ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="apiId" /><br/>
                                        <html:messages id="apiId" property="apiId">
                                            <bean:write name="apiId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">API Link</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="apiLink" /><br/>
                                        <html:messages id="apiLink" property="apiLink">
                                            <bean:write name="apiLink"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">API User ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="apiUser" /><br/>
                                        <html:messages id="apiUser" property="apiUser">
                                            <bean:write name="apiUser"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">API User Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="apiPassword" /><br/>
                                        <html:messages id="apiPassword" property="apiPassword">
                                            <bean:write name="apiPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Status</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="apiStatus">
                                            <%
                                                for (int i = 0; i < Constants.LIVE_STATUS_VALUE.length - 1; i++) {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="apiStatus" property="apiStatus">
                                            <bean:write name="apiStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Allowed Operators</th>
                                    <td valign="top" style="padding-top: 6px;">                 
                                        <input type="checkbox" name="opPers" <%=apiGPActive%> value="<%=Constants.GP%>" align="right" style="width: 20px" />Grameen 
                                        <input type="checkbox" name="opPers" <%=apiRBActive%> value="<%=Constants.RB%>" align="right" style="width: 20px" />Robi 
                                        <input type="checkbox" name="opPers" <%=apiBLActive%> value="<%=Constants.BL%>" align="right" style="width: 20px" />BanglaLink
                                        <BR>
                                            <input type="checkbox" name="opPers" <%=apiTTActive%> value="<%=Constants.TT%>" align="right" style="width: 20px" />TeleTalk 
                                            <input type="checkbox" name="opPers" <%=apiWRActive%> value="<%=Constants.WR%>" align="right" style="width: 20px" />Airtel 
                                            <input type="checkbox" name="opPers" <%=apiCCActive%> value="<%=Constants.CC%>" align="right" style="width: 20px" />Citycell 
                                    </td>
                                </tr>                                         
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>