<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE) {
        response.sendRedirect("../home/home.do");
        return;
    }
%>

<%@page import="com.myapp.struts.client.ClientLoader,com.myapp.struts.client.ClientDTO" %>
<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,java.util.ArrayList" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Change Settings</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/system/changeSettings" method="post">
                        <table class="settings_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Change Settings</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px; padding-bottom: 10px;" valign="bottom">
                                        <bean:write name="SettingsForm" property="message" filter="false"/>
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th>Prepaid Min. Refill Amount</th>
                                    <td>
                                        <html:text property="prepaidMinRefillAmount" style="width: 40%;"  />
                                        <html:messages id="prepaidMinRefillAmount" property="prepaidMinRefillAmount">
                                            <bean:write name="prepaidMinRefillAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Prepaid Max. Refill Amount</th>
                                    <td>
                                        <html:text property="prepaidMaxRefillAmount" style="width: 40%;" />
                                        <html:messages id="prepaidMaxRefillAmount" property="prepaidMaxRefillAmount">
                                            <bean:write name="prepaidMaxRefillAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Postpaid Min. Refill Amount</th>
                                    <td>
                                        <html:text property="postpaidMinRefillAmount" style="width: 40%;" />
                                        <html:messages id="postpaidMinRefillAmount" property="postpaidMinRefillAmount">
                                            <bean:write name="postpaidMinRefillAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Postpaid Max. Refill Amount</th>
                                    <td>
                                        <html:text property="postpaidMaxRefillAmount" style="width: 40%;" />
                                        <html:messages id="postpaidMaxRefillAmount" property="postpaidMaxRefillAmount">
                                            <bean:write name="postpaidMaxRefillAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 

                                <tr>
                                    <th>Min. Flexi SIM Balance</th>
                                    <td>
                                        <html:text property="minFlexiSIMBalance" style="width: 40%;" />
                                        <html:messages id="minFlexiSIMBalance" property="minFlexiSIMBalance">
                                            <bean:write name="minFlexiSIMBalance"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Min. Allowed Discount (%)</th>
                                    <td>
                                        <html:text property="minAllowedDiscount" style="width: 40%;" />
                                        <html:messages id="minAllowedDiscount" property="minAllowedDiscount">
                                            <bean:write name="minAllowedDiscount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Max. Allowed Discount (%)</th>
                                    <td>
                                        <html:text property="maxAllowedDiscount" style="width: 40%;" />
                                        <html:messages id="maxAllowedDiscount" property="maxAllowedDiscount">
                                            <bean:write name="maxAllowedDiscount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        

                                <tr>
                                    <th>Same Number Refill Interval (Minutes)</th>
                                    <td>
                                        <html:text property="sameNumberRefillInterval" style="width: 40%;" />
                                        <html:messages id="sameNumberRefillInterval" property="sameNumberRefillInterval">
                                            <bean:write name="sameNumberRefillInterval"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
<%--                                        
                                <tr>
                                    <th>API User ID</th>
                                    <td>
                                        <html:text property="apiUserID" style="width: 40%;" />
                                        <html:messages id="apiUserID" property="apiUserID">
                                            <bean:write name="apiUserID"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                        
                                <tr>
                                    <th>API User Password</th>
                                    <td>
                                        <html:text property="apiUserPassword" style="width: 40%;" />
                                        <html:messages id="apiUserPassword" property="apiUserPassword">
                                            <bean:write name="apiUserPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>    
--%> 
                                <tr>
                                    <th>Offset From Server Time</th>
                                    <td>                                   
                                        <html:select property="offsetFromServerTime" style="width: 42%;">
                                            <html:option value="-12"  >-12.00</html:option>
                                            <html:option value="-11"  >-11.00</html:option>
                                            <html:option value="-10"  >-10.00</html:option>
                                            <html:option value="-9"  >-9.00</html:option>
                                            <html:option value="-8"  >-8.00</html:option>
                                            <html:option value="-7"  >-7.00</html:option>
                                            <html:option value="-6"  >-6.00</html:option>
                                            <html:option value="-5"  >-5.00</html:option>
                                            <html:option value="-4"  >-4.00</html:option>
                                            <html:option value="-3"  >-3.00</html:option>
                                            <html:option value="-2"  >-2.00</html:option>
                                            <html:option value="-1"  >-1.00</html:option>
                                            <html:option value="0" >0.00</html:option>
                                            <html:option value="1" >+01:00</html:option>
                                            <html:option value="2" >+02:00</html:option>
                                            <html:option value="3" >+03:00</html:option>
                                            <html:option value="4" >+04:00</html:option>
                                            <html:option value="5" >+05:00</html:option>
                                            <html:option value="6" >+06:00</html:option>
                                            <html:option value="7" >+07:00</html:option>
                                            <html:option value="8" >+08:00</html:option>
                                            <html:option value="9" >+09:00</html:option>
                                            <html:option value="10" >+10:00</html:option>
                                            <html:option value="11" >+11:00</html:option>
                                            <html:option value="12" >+12:00</html:option>
                                        </html:select>   
                                    </td>
                                </tr> 
                                        
                                <tr>
                                    <th>Record Per Page For Reporting</th>
                                    <td>
                                        <html:text property="recordPerPageForReporting" style="width: 40%;" />
                                        <html:messages id="recordPerPageForReporting" property="recordPerPageForReporting">
                                            <bean:write name="recordPerPageForReporting"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                        
                                <tr>
                                    <th>SIM Message Log Restore Interval (Days)</th>
                                    <td>
                                        <html:text property="simMessageLogRestoreInterval" style="width: 40%;" />
                                        <html:messages id="simMessageLogRestoreInterval" property="simMessageLogRestoreInterval">
                                            <bean:write name="simMessageLogRestoreInterval"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                        
                                <tr>
                                    <th>Data Backup Interval (Minutes)</th>
                                    <td>
                                        <html:text property="dataBackupInterval" style="width: 40%;" />
                                        <html:messages id="dataBackupInterval" property="dataBackupInterval">
                                            <bean:write name="dataBackupInterval"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                          
                                <tr>
                                    <th>Pending Time For Auto System (Hours)</th>
                                    <td>
                                        <html:text property="pendingTimeForAutoSystem" style="width: 40%;" />
                                        <html:messages id="pendingTimeForAutoSystem" property="pendingTimeForAutoSystem">
                                            <bean:write name="pendingTimeForAutoSystem"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                       
                                <tr>
                                    <th>Pending Time For Manual System (Hours)</th>
                                    <td>
                                        <html:text property="pendingTimeForManualSystem" style="width: 40%;" />
                                        <html:messages id="pendingTimeForManualSystem" property="pendingTimeForManualSystem">
                                            <bean:write name="pendingTimeForManualSystem"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                        
                                <tr>
                                    <th>Notify After When Total Pending Requests Are</th>
                                    <td>
                                        <html:text property="notifyAfterPending" style="width: 40%;" />
                                        <html:messages id="notifyAfterPending" property="notifyAfterPending">
                                            <bean:write name="notifyAfterPending"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                         

                                <tr>
                                    <th>Brand Name</th>
                                    <td>
                                        <html:text property="brandName" style="width: 60%;" />
                                        <html:messages id="brandName" property="brandName">
                                            <bean:write name="brandName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 

                                <tr>
                                    <th>Copyright Link</th>
                                    <td>
                                        <html:text property="copyrightLink" style="width: 60%;" />
                                        <html:messages id="copyrightLink" property="copyrightLink">
                                            <bean:write name="copyrightLink"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 

                                <tr>
                                    <th>Contact Email (gmail)</th>
                                    <td>
                                        <html:text property="contactEmail" style="width: 60%;" />
                                        <html:messages id="contactEmail" property="contactEmail">
                                            <bean:write name="contactEmail"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                        
                                <tr>
                                    <th>Contact Email Password</th>
                                    <td>
                                        <html:password property="contactEmailPass" style="width: 60%;" />
                                        <html:messages id="contactEmailPass" property="contactEmailPass">
                                            <bean:write name="contactEmailPass"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                        
                                <tr>
                                    <th>Notification Email</th>
                                    <td>
                                        <html:text property="notificationEmail" style="width: 60%;" />
                                        <html:messages id="notificationEmail" property="notificationEmail">
                                            <bean:write name="notificationEmail"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                         

                                <tr>
                                    <th>Motto</th>
                                    <td>
                                        <html:textarea property="motto" rows="3" style="width: 60%;" />
                                        <html:messages id="motto" property="motto">
                                            <bean:write name="motto"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  

                                <tr>
                                    <th>Welcome Note</th>
                                    <td>
                                        <html:textarea property="welcomeNote" rows="3" style="width: 60%;" />
                                        <html:messages id="welcomeNote" property="welcomeNote">
                                            <bean:write name="welcomeNote"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                        
                                <tr>
                                    <th>Emergency Notice Text</th>
                                    <td>
                                        <html:textarea property="emergencyNoticeText" rows="3" style="width: 60%;" />
                                        <html:messages id="emergencyNoticeText" property="emergencyNoticeText">
                                            <bean:write name="emergencyNoticeText"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                         

                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">           
                                        <input name="submit" type="submit" class="custom-button" value="Save" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>