<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    long edid = Long.parseLong(request.getParameter("id"));
    if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
            && perDTO != null && !perDTO.CLIENT_EDIT && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2
            && login_dto.getId() != edid && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
    String link = request.getQueryString();
    if (login_dto.getIsUser() || login_dto.getId() != edid) {
        if (request.getParameter("searchLink") != null) {
            link = request.getParameter("searchLink");
        } else {
            link = "?" + link.substring(link.indexOf("&link=") + 6);
        }
    }
    int clientRegister = 0;
    if (request.getParameter("special") != null) {
        clientRegister = Integer.parseInt(request.getParameter("special"));
    } else if (request.getAttribute("clientRegister") != null) {
        clientRegister = Integer.parseInt(request.getAttribute("clientRegister").toString());
    }
    String clientGPActive = " ";
    if (request.getAttribute("clientGPActive") != null) {
        clientGPActive = request.getAttribute("clientGPActive").toString();
    }
    String clientRBActive = " ";
    if (request.getAttribute("clientRBActive") != null) {
        clientRBActive = request.getAttribute("clientRBActive").toString();
    }
    String clientBLActive = " ";
    if (request.getAttribute("clientBLActive") != null) {
        clientBLActive = request.getAttribute("clientBLActive").toString();
    }
    String clientTTActive = " ";
    if (request.getAttribute("clientTTActive") != null) {
        clientTTActive = request.getAttribute("clientTTActive").toString();
    }
    String clientCCActive = " ";
    if (request.getAttribute("clientCCActive") != null) {
        clientCCActive = request.getAttribute("clientCCActive").toString();
    }
    String clientWRActive = " ";
    if (request.getAttribute("clientWRActive") != null) {
        clientWRActive = request.getAttribute("clientWRActive").toString();
    }
    String clientBTActive = " ";
    if (request.getAttribute("clientBTActive") != null) {
        clientBTActive = request.getAttribute("clientBTActive").toString();
    }
    String clientSMSActive = " ";
    if (request.getAttribute("clientSMSActive") != null) {
        clientSMSActive = request.getAttribute("clientSMSActive").toString();
    }
%>

<%@page import="com.myapp.struts.client.ClientLoader"%>
<%@page import="java.util.ArrayList,com.myapp.struts.client.ClientDTO" %>
<script language="javascript" type="text/javascript">

    window.onload=function(){
        init();
    }

    function init(){
        if(document.forms[0].clientTypeId.value=="<%=Constants.CLIENT_TYPE_AGENT%>")
        {
            document.forms[0].clientIp.disabled = false;
            // document.forms[0].clientAni.disabled = false;
        }
        else
        {
            document.forms[0].clientIp.disabled = true;
            // document.forms[0].clientAni.disabled = true;
        } 
    } 
</script>        
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Edit Client</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/client/editClient" method="post" >
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Edit Client: <font color="#e17009"><%=request.getParameter("name")%></font></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="ClientForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Client ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientId" /><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Client Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:hidden property="clientTypeId" />
                                        <%
                                            int clientType = Integer.parseInt(request.getAttribute("clientTypeId").toString());
                                            String clientTypeID = Constants.CLIENT_TYPE[clientType];
                                        %>
                                        <html:text property="clientTypeIdVal"  disabled="true" value="<%=clientTypeID%>" /><br/>
                                        <html:messages id="clientTypeId" property="clientTypeId">
                                            <bean:write name="clientTypeId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <html:hidden property="parentId" />
                                <%
                                    if (login_dto.getIsUser()) {
                                %>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Parent</th>
                                    <td valign="top" style="padding-top: 6px;">

                                        <%
                                            String parentID = "None";
                                            ClientDTO clientDTO = (ClientDTO) ClientLoader.getInstance().getClientDTOByID(Integer.parseInt(request.getAttribute("parentId").toString()));
                                            if (clientDTO != null) {
                                                parentID = clientDTO.getClientId();
                                            }
                                        %>
                                        <html:text property="parentIdVal"  disabled="true" value='<%=parentID%>' /><br/>
                                        <html:messages id="parentId" property="parentId">
                                            <bean:write name="parentId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <%
                                    }
                                    if (login_dto.getIsUser() || edid != login_dto.getId()) {
                                %>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientPassword" /><br/>
                                        <html:messages id="clientPassword" property="clientPassword">
                                            <bean:write name="clientPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                          
                                <%                                 } else {
                                %>
                                <html:hidden property="clientPassword" />
                                <html:hidden property="retypePassword" />
                                <%                                    }
                                %>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Status</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                            boolean disabled = true;
                                            if (login_dto.getIsUser() || edid != login_dto.getId()) {
                                                disabled = false;
                                        %>
                                        <input type="hidden" name="searchLink" value="<%=link%>" />
                                        <%
                                            }
                                        %>
                                        <html:select property="clientStatus" disabled="<%=disabled%>" >
                                            <%
                                                for (int i = 0; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientStatus" property="clientStatus">
                                            <bean:write name="clientStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <%
                                    if (clientRegister == 0) {
                                %>                                         
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Discount(%)</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientDiscount" /><br/>
                                        <html:messages id="clientDiscount" property="clientDiscount">
                                            <bean:write name="clientDiscount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Min. Deposit</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientDeposit" /><br/>
                                        <html:messages id="clientDeposit" property="clientDeposit">
                                            <bean:write name="clientDeposit"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                         
                                <%                                    }
                                %> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Full Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientName" /><br/>
                                        <html:messages id="clientName" property="clientName">
                                            <bean:write name="clientName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Email Address</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientEmail" /><br/>
                                        <html:messages id="clientEmail" property="clientEmail">
                                            <bean:write name="clientEmail"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">SMS Sender</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="smsSender" /><br/>
                                        <html:messages id="smsSender" property="smsSender">
                                            <bean:write name="smsSender"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <%
                                    if (login_dto.getIsUser() || edid != login_dto.getId()) {
                                %>   
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Allowed IP</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientIp" /><br/>
                                        <html:messages id="clientIp" property="clientIp">
                                            <bean:write name="clientIp"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <%--        
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Allowed ANI</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientAni" /><br/>
                                        <html:messages id="clientAni" property="clientAni">
                                            <bean:write name="clientAni"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>   
                                --%>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Allowed Operators</th>
                                    <td valign="top" style="padding-top: 6px;">                 
                                        <input type="checkbox" name="opPers" <%=clientGPActive%> value="<%=Constants.GP%>" align="right" style="width: 20px" />Grameen 
                                        <input type="checkbox" name="opPers" <%=clientRBActive%> value="<%=Constants.RB%>" align="right" style="width: 20px" />Robi 
                                        <input type="checkbox" name="opPers" <%=clientBLActive%> value="<%=Constants.BL%>" align="right" style="width: 20px" />BanglaLink
                                        <BR>
                                            <input type="checkbox" name="opPers" <%=clientTTActive%> value="<%=Constants.TT%>" align="right" style="width: 20px" />TeleTalk 
                                            <input type="checkbox" name="opPers" <%=clientWRActive%> value="<%=Constants.WR%>" align="right" style="width: 20px" />Airtel 
                                            <input type="checkbox" name="opPers" <%=clientCCActive%> value="<%=Constants.CC%>" align="right" style="width: 20px" />Citycell
                                            <BR>
                                                <input type="checkbox" name="opPers" <%=clientBTActive%> value="<%=Constants.BT%>" align="right" style="width: 20px" />Balance Transfer 
                                                <input type="checkbox" name="opPers" <%=clientSMSActive%> value="<%=Constants.SMS%>" align="right" style="width: 20px" />SMS
                                                </td>
                                                </tr>  
                                                <%
                                                    }
                                                %>                                      
                                                <tr>
                                                    <th style="height: 40px;"></th>
                                                    <td style="height: 40px;">
                                                        <html:hidden property="id" />
                                                        <input type="hidden" name="clientRegister" value="<%=clientRegister%>" />
                                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                                        <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />  
                                                        <input name="submit" type="submit" class="custom-button" value="Save" />
                                                        <input type="reset" class="custom-button" value="Reset" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                                </table>
                                                <div class="blank-height"></div>
                                            </html:form>
                                            </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div><%@include file="../includes/footer.jsp"%></div>
                                            </div>
                                            </body>
                                            </html>