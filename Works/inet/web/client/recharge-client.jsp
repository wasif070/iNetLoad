<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO!=null && !perDTO.CLIENT
                && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                ) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.client.ClientLoader"%>
<%@page import="java.util.ArrayList,com.myapp.struts.client.ClientDTO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />


<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 100;

   if (request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j = 0;
        var k = 0;
        var l = 0;
        var n = 0;
        var m = 0;
        var amountFlag = false;
        var descriptionFlag = false;
        var selectedIDsArray = [];
        var amountIDsArray = [];
        var amountArray = [];
        var descriptionIDsArray = [];
        var descriptionArray = [];
        var allInputArray = document.forms[0].elements;
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j] = i;
                    j++;
                    amountFlag = true;
                    descriptionFlag = true;
                    operatorFlag = true;
                }
            }
            else if(allInputArray[i].name == "amounts")
            {
                amountIDsArray[l] = i;
                l++;
                if(amountFlag)
                {
                    tempInputbox = allInputArray[i];
                    amountArray[k]=tempInputbox.value;
                    k++;
                    amountFlag = false;
                }
            }
            else if(allInputArray[i].name == "descriptions")
            {
                descriptionIDsArray[m] = i;
                m++;
                if(descriptionFlag)
                {
                    tempInputbox = allInputArray[i];
                    descriptionArray[n]=tempInputbox.value;
                    n++;
                    descriptionFlag = false;
                }
            }
        }
        document.forms[0].reset();
        for(t=0; t<allInputArray.length; t++)
        {
                            
            checkFlag = 0;
            for(i=0; i<selectedIDsArray.length; i++)
            {
                if(t==selectedIDsArray[i])
                {
                    checkFlag = 1;
                    break;
                }
            }
            if(checkFlag==1)
            {
                tempCheckbox = allInputArray[selectedIDsArray[i]];
                tempCheckbox.checked = true;
                tempInputbox = allInputArray[amountIDsArray[i]];
                tempInputbox.value = amountArray[i];
                tempInputbox = allInputArray[descriptionIDsArray[i]];
                tempInputbox.value = descriptionArray[i];
            }
            else
            {
                tempCheckbox = allInputArray[t];
                tempCheckbox.checked = false;
            }
        }

        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }

        link = "../client/rechargeClient.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }
        document.forms[0].action = link;
    }    

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../client/rechargeClient.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Client List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/client/rechargeClient.do" method="post" >
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Client ID</th>
                                        <td>
                                            <html:text property="clientId">
                                            </html:text>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Client Type</th>
                                        <td>
                                            <html:select property="clientTypeId">
                                                <html:option value="0">Select</html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER3)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER3]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER2)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER2]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_AGENT)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_AGENT]%></html:option>
                                            </html:select>
                                        </td> 
                                    </tr>    
                                    <tr>
                                        <th>Balance</th>
                                        <td>
                                            <html:select property="sign" style="width:47px">
                                                <html:option value="0">All</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_EQUAL)%>">=</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_SMALLER_THAN)%>"><</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_GREATER_THAN)%>">></html:option>
                                            </html:select>
                                            <html:text property="clientCredit" style="width:100px" />
                                        </td>
                                        <html:hidden property="parentId" />
                                    </tr>
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>     
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                              boolean clientRechargePer = false;
                              boolean clientReturnPer = false;
                              boolean clientReceivePer = false;
                              if((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                              {
                                  clientRechargePer = true;
                                  clientReturnPer = true;
                                  clientReceivePer = true;
                              }
                              else if(login_dto.getIsUser() && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                              {
                                  if(perDTO.CLIENT_RECHARGE)
                                  {
                                     clientRechargePer = true;
                                  }
                                  if(perDTO.CLIENT_RETURN)
                                  {
                                     clientReturnPer = true;
                                  }
                                  if(perDTO.CLIENT_RECEIVE)
                                  {
                                     clientReceivePer = true;
                                  }
                              }
                            %>
                        </div>
                        <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                        <div style="border : 1px solid #848284;" align="center">
                            <%
                               boolean parFound = false;
                               long parID = 0;
                               String parName = "";
                               String parLink = "";
                               if(request.getParameter("parentId")!=null)
                               {
                                  parID = Long.parseLong(request.getParameter("parentId"));
                               }
                               if(parID>0 && login_dto.getIsUser())
                               {
                                   parFound = true;
                                   parName = ClientLoader.getInstance().getClientDTOByID(parID).getClientId();
                                   parLink = "rechargeClient.do?list_all=1&parentId="+ClientLoader.getInstance().getClientDTOByID(parID).getParentId();
                               }
                               if(parFound)
                               {
                            %>
                            <div class="height-5px"></div>
                            <div class="height-20px">Parent: <a href="<%=parLink%>" ><u><%=parName%></u></a></div>
                            <%
                               }
                               else
                               {
                            %>
                            <div class="height-20px"></div>
                            <%
                               }
                            %>
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.ClientForm.clientList" pagesize="<%=recordPerPage%>">
                                <display:setProperty name="paging.banner.item_name" value="Client" />
                                <display:setProperty name="paging.banner.items_name" value="Clients" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column title="Client ID" sortProperty="clientId" sortable="true" style="width:15%;">
                                    <c:choose>
                                        <c:when test="${is_user==true && (data.clientTypeId==1 || data.clientTypeId==3 || data.clientTypeId==4)}"><a href="../client/rechargeClient.do?list_all=1&parentId=${data.id}"><u>${data.clientId}</u></a></c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${data.clientTypeId==1 || data.clientTypeId==3 || data.clientTypeId==4}"><font color="blue">${data.clientId}</font></c:when>
                                                <c:otherwise>${data.clientId} </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </display:column>
                                <display:column property="clientTypeName" title="Type" sortable="true" class="custom_column1" style="width:10%" /> 
                                <display:column  title="Balance" property="clientCredit" format="{0,number,0.0000}" sortProperty="clientCredit" sortable="true" class="custom_column2" style="width:20%" />
                                <display:column  title="Discount(%)" property="clientDiscount" format="{0,number,0.0000}" sortProperty="clientDiscount" sortable="true" class="custom_column2" style="width:15%" />
                                <display:column title="Amount" sortable="false" class="custom_column1" style="width:15%">
                                    <input type="text" name="amounts" style="width: 80px;border-color: #848284; border-style: solid;border-width: 1px;" value="0" />
                                </display:column>
                                <display:column title="Description" sortable="false" class="custom_column1" style="width:15%">
                                    <input type="text" name="descriptions" style="width: 100px;border-color: #848284; border-style: solid;border-width: 1px;" value=" " />
                                </display:column>
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.CLIENT_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <div class="clear height-5px"></div>
                            <%
                                request.removeAttribute(Constants.CLIENT_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                                if(clientRechargePer)
                                {
                            %>
                            <input type="submit" class="action-button" value="Recharge Selected" style="width: 140px;" onclick="submitform('<%=Constants.RECHARGE%>')" />
                            <% 
                                }
                                if(clientReturnPer)
                                {
                            %>
                            <input type="submit" class="action-button" value="Return Selected" style="width: 125px;" onclick="submitform('<%=Constants.RETURN%>')" />
                            <% 
                                }
                                if(clientReceivePer)
                                {
                            %>
                            <input type="submit" class="action-button" value="Receive Selected" style="width: 135px;" onclick="submitform('<%=Constants.RECEIVE%>')" />
                            <%
                                }
                            %>
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>