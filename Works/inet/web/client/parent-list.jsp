<%@page import="com.myapp.struts.client.ClientDAO"%>
<%@page import="com.myapp.struts.client.ClientLoader,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.client.ClientDTO,com.myapp.struts.login.LoginDTO" %>
<%
            LoginDTO LOGIN_DTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            if (LOGIN_DTO.getIsUser()) {
%>
<select name="parentId">
    <%
          int type = Integer.parseInt(request.getParameter("type"));
          if (type == Constants.CLIENT_TYPE_RESELLER || type == Constants.CLIENT_TYPE_RESELLER3 || type == Constants.CLIENT_TYPE_RESELLER2) {
    %>
    <option value="<%=String.valueOf(Constants.ROOT_RESELLER_PARENT_ID)%>">None</option>
    <%
          }
          ClientDAO clDAO =  new ClientDAO();
          ArrayList<ClientDTO> clientList = clDAO.getClientDTOsSortedByClientID(ClientLoader.getInstance().getClientDTOList());
          if (clientList != null) {
              int size = clientList.size();
              for (int i = 0; i < size; i++) {
                  ClientDTO c_dto = clientList.get(i);
                  if (c_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                      continue;
                  }
                  if ((type == Constants.CLIENT_TYPE_RESELLER && c_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER)
                          || (type == Constants.CLIENT_TYPE_RESELLER2 && c_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3)
                          || (type == Constants.CLIENT_TYPE_AGENT)) {
    %>
    <option  value="<%=String.valueOf(c_dto.getId())%>" ><%=c_dto.getClientId()%></option>
    <%
                            }
                        }
                    }
                }
    %>
</select><br/>
