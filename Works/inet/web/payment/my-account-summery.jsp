<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.REPORT_PAYMENT_SUMMERY && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = SettingsLoader.getInstance().getSettingsDTO().getRecordPerPageForReporting();

   if (request.getSession(true).getAttribute(Constants.MY_ACCOUNT_SUMMERY_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.MY_ACCOUNT_SUMMERY_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../payment/myAccountSummery.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: My Account Summery</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <div class="clear height-50px">
                        <%
                          if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                          {
                            out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                          }
                        %>
                    </div>
                    <div class="full-div">
                        <display:table class="reporting_table" style="width:100%;" defaultsort="2" defaultorder="ascending" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.PaymentForm.accountSummery"  pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Entry" />
                            <display:setProperty name="paging.banner.items_name" value="Entries" />
                            <display:column  class="custom_column2" title="Paid To Parent" format="{0,number,0.0000}" style="width:25%"  >
                                <a href="../payment/listPaidPayments.do?list_all=1&id=${data.paymentToClientId}&name=${data.paymentToClient}&type=${data.paymentToClientTypeName}"><u>${data.receivedAmount}</u></a>
                            </display:column>    
                            <display:column  class="custom_column2" title="Received From Parent" format="{0,number,0.0000}" style="width:25%"  >
                                <a href="../payment/listReceivedPayments.do?list_all=1&id=${data.paymentToClientId}&name=${data.paymentToClient}&type=${data.paymentToClientTypeName}"><u>${data.paymentAmount}</u></a>
                            </display:column>                             
                            <display:column  class="custom_column2" title="Available From Parent" value="${data.receivedAmount - data.paymentAmount}" format="{0,number,0.0000}" style="width:25%" />
                            <display:column  class="custom_column2" property="paymentDiscount" title="Discount From Parent" format="{0,number,0.0000}" style="width:25%"  />
                        </display:table>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>