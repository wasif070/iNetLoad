<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.client.ClientDAO,com.myapp.struts.scratchcard.ScratchcardDTO,com.myapp.struts.scratchcard.ScratchcardTypeLoader,java.util.ArrayList"%>

<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            response.sendRedirect("../home/home.do");
            return;
        } else {
            if (login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                response.sendRedirect("../home/home.do");
                return;
            }
        }
    }
    ClientDAO dao = new ClientDAO();
    double credit = dao.getClientCredit(login_dto.getId());
%>

<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>
<script language="javascript" type="text/javascript">

    window.onload=function(){
        getTypeList();
    }

    function getTypeList(){     
        $.ajax({
            url:"../scratchcard/type-list.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random(),company:document.forms[0].companyName.value},
            cache: false,
            dataType:'html',
            async:false,
            success:function(html) {
                if(html!=''){
                    $(".card-type-list").html($.trim(html));
                }
            }
        });
    }
    
    function getTypeList(type){     
        $.ajax({
            url:"../scratchcard/type-list.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random(),company:document.forms[0].companyName.value,refType:type},
            cache: false,
            dataType:'html',
            async:false,
            success:function(html) {
                if(html!=''){
                    $(".card-type-list").html($.trim(html));
                }
            }
        });
    }      

    function getSID(){
        var sid=new Date();
        return sid.getTime();
    }
    
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Buy Scratch Card</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px" ><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">

                    <html:form action="/scratchcard/buyScratchcard" method="post" styleId="scratchcard_form">
                        <table class="input_table" cellspacing="0" cellpadding="0" style="width: 70%">
                            <thead>
                                <tr>
                                    <th colspan="2" align="center">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #DFBC00;" >Scratch Card Buy Request</span>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="2" align="center">
                                        <span style="font-size: 16px; font-weight: bold; color: #C1C5C9;" >Balance: <%=credit%></span>
                                    </th>
                                </tr>                                            
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="ScratchcardForm" property="message" filter="false"/>
                                    </td>
                                </tr>                                              
                                <tr>
                                    <th>Card Name</th>
                                    <td>
                                        <html:select property="companyName" onchange="getTypeList()" styleId="companyName" >
                                            <%
                                                ArrayList<ScratchcardDTO> cardTypeList = ScratchcardTypeLoader.getInstance().getScratchcardTypeDTOList();
                                                if (cardTypeList != null && cardTypeList.size() > 0) {
                                                    for (int i = 0; i < cardTypeList.size(); i++) {
                                                        ScratchcardDTO dto = cardTypeList.get(i);
                                                        if (dto.getStatusID() != Constants.USER_STATUS_ACTIVE) {
                                                            continue;
                                                        }
                                            %>
                                            <html:option value="<%=dto.getCompanyName()%>"><%=dto.getCompanyName()%></html:option>
                                            <%
                                                    }
                                                }
                                            %>                                            
                                        </html:select>    
                                        <html:messages id="companyName" property="companyName">
                                            <bean:write name="companyName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Card Type</th>
                                    <td> 
                                        <div class="card-type-list"></div>  
                                    </td>
                                </tr>                                                                                 
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <div class="blank-height"></div>
                                        <input type="hidden" name="type" value="2" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submitButton" type="button" class="custom-button" value="Send" onclick="submitConfirm();"/>
                                        <input type="reset" class="custom-button" value="Reset" />
                                        <div class="blank-height"></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>

                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

<div id="dialog-confirm" title="Recheck!!">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><div id="confirm-message" style="font-weight: normal; color:#e17009; font-size: 1.05em; text-align: left;margin-left: 50px"></div></p>
</div>

<script type="text/javascript" language="javascript">
    function submitConfirm(){      
        var message="Are these information correct?<br>";
        message+="Card Name : "+$("#companyName").val()+"<br>";
        message+="Card Type : "+$("#refillType").val()+"<br>";
        message+="Card Amount : "+$("#refillAmount").val()+"<br>";          
        $("#confirm-message").html(message);
        $('#dialog-confirm').dialog('option','title','Please Recheck!!!').dialog('open');
        return true;
    }
    $(function() {
        $("#dialog-confirm").dialog({autoOpen: false, modal: true, width:350,
            buttons: {
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Ok: function() {
                    $(this).dialog( "close" );
                    $("#scratchcard_form").submit();
                }
            }
        });
        // End Changed
    });
</script>
