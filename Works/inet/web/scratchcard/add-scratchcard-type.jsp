<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.SC_ADD && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
%>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Scratch Card Type</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/scratchcard/addScratchcardType" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Add Scratch Card Type</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="ScratchcardForm" property="message" filter="false"/>
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th>Company Name</th>
                                    <td>
                                        <html:text property="companyName"  />
                                        <html:messages id="companyName" property="companyName">
                                            <bean:write name="companyName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Supported Refill Types</th>
                                    <td> 
                                        <html:text property="supportedRefillTypes"  /> 
                                        <html:messages id="supportedRefillTypes" property="supportedRefillTypes">
                                            <bean:write name="supportedRefillTypes"  filter="false"/>
                                        </html:messages>    
                                    </td>
                                </tr>                                        
                                <tr>                                        
                                    <th>&nbsp;</th>
                                    <td valign="top">(Comma separated list)</td>
                                </tr>                                        
                                <tr>
                                    <th>Supported Refill Amounts</th>
                                    <td>
                                        <html:text property="supportedRefillAmounts"  />
                                        <html:messages id="supportedRefillAmounts" property="supportedRefillAmounts">
                                            <bean:write name="supportedRefillAmounts"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>                                        
                                    <th>&nbsp;</th>
                                    <td valign="top">(Comma separated list. And supported refill amounts of a refill type should be separated by '|'.)</td>                                        
                                </tr>                                                                       
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="type" value="1" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>                        
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>