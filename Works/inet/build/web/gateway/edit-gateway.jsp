<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.FLEXI_SIM_SETTINGS_EDIT && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
            String link = request.getQueryString();
            if (request.getParameter("searchLink") != null) {
                link = request.getParameter("searchLink");
            } else {
                link = "?" + link.substring(link.indexOf("&link=") + 6);
            }
            boolean addedByUser = false;
            if(request.getSession(true).getAttribute("addedByUser") != null)
            {
                addedByUser = true;
            }
            request.getSession(true).removeAttribute("addedByUser");
%>

<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,java.util.ArrayList,com.myapp.struts.dealers.DealerDTO,com.myapp.struts.dealers.DealerLoader" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> ::  Edit FLEXI SIM</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/gateway/editGateway" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Edit Flexi SIM: <font color="#e17009"><%=request.getParameter("name")%></font></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="GatewayForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <%
                                    if(addedByUser)
                                    {  
                                %>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Operator</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="operatorName" readonly="true" styleClass="readonly-input-box" /><br/>
                                        <html:messages id="operatorName" property="operatorName">
                                            <bean:write name="operatorName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>    
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">SIM ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="simID" styleClass="readonly-input-box" /><br/>
                                        <html:messages id="simID" property="simID">
                                            <bean:write name="simID"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                   <tr>
                                    <th>Balance</th>
                                    <td>
                                        <html:text property="balance" />
                                        <html:messages id="balance" property="balance">
                                            <bean:write name="balance"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="gatewayStatus" >
                                            <%
                                               for (int i = 0; i < Constants.FLEXI_SIM_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.FLEXI_SIM_STATUS_VALUE[i]%>"><%=Constants.FLEXI_SIM_STATUS_STRING[i]%></html:option>
                                            <%
                                                                                            }
                                            %>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <td>
                                        <html:select property="typeID" >
                                            <%
                                                for (int i = 0; i < Constants.FLEXI_SIM_TYPE_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.FLEXI_SIM_TYPE_VALUE[i]%>"><%=Constants.FLEXI_SIM_TYPE_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th>Phone Number</th>
                                    <td>
                                        <html:text property="phoneNo"  />
                                        <html:messages id="phoneNo" property="phoneNo">
                                            <bean:write name="phoneNo"  filter="false"/>
                                        </html:messages>

                                    </td>
                                </tr>
                                <tr>
                                    <th>Pin Number</th>
                                    <td>
                                        <html:text property="pinNumber"  />
                                        <html:messages id="pinNumber" property="pinNumber">
                                            <bean:write name="pinNumber"  filter="false"/>
                                        </html:messages>

                                    </td>
                                </tr>                                          
                                <tr>
                                    <th>Dealer</th>
                                    <td>
                                        <%
                                            ArrayList<DealerDTO> dealerList = DealerLoader.getInstance().getDealerDTOList();
                                        %>                                        
                                        <html:select property="dealerID">
                                            <html:option value="<%=String.valueOf(Constants.ROOT_RESELLER_PARENT_ID)%>">Select Dealer</html:option>
                                            <%
                                                if (dealerList != null && dealerList.size() > 0) {
                                                    int size = dealerList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        DealerDTO d_dto = (DealerDTO) dealerList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(d_dto.getId())%>"><%=d_dto.getDealerName()%></html:option>
                                            <%}
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="dealerID" property="dealerId">
                                            <bean:write name="dealerID"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <html:hidden property="simVersion" value="-1" />        
                                <%
                                   }
                                   else
                                   {  
                                %>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Operator</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="operatorName" readonly="true" styleClass="readonly-input-box" /><br/>
                                        <html:messages id="operatorName" property="operatorName">
                                            <bean:write name="operatorName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>    
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">SIM ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="simID" readonly="true" styleClass="readonly-input-box" /><br/>
                                        <html:messages id="simID" property="simID">
                                            <bean:write name="simID"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                   <tr>
                                    <th>Balance</th>
                                    <td>
                                        <html:text property="balance" readonly="true" styleClass="readonly-input-box"  />
                                        <html:messages id="balance" property="balance">
                                            <bean:write name="balance"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="gatewayStatus" >
                                            <%
                                               for (int i = 0; i < Constants.FLEXI_SIM_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.FLEXI_SIM_STATUS_VALUE[i]%>"><%=Constants.FLEXI_SIM_STATUS_STRING[i]%></html:option>
                                            <%
                                                                                            }
                                            %>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <td>
                                        <html:select property="typeID" >
                                            <%
                                                for (int i = 2; i < Constants.FLEXI_SIM_TYPE_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.FLEXI_SIM_TYPE_VALUE[i]%>"><%=Constants.FLEXI_SIM_TYPE_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th>Phone Number</th>
                                    <td>
                                        <html:text property="phoneNo"  />
                                        <html:messages id="phoneNo" property="phoneNo">
                                            <bean:write name="phoneNo"  filter="false"/>
                                        </html:messages>

                                    </td>
                                </tr>
                                <tr>
                                    <th>Pin Number</th>
                                    <td>
                                        <html:text property="pinNumber"  />
                                        <html:messages id="pinNumber" property="pinNumber">
                                            <bean:write name="pinNumber"  filter="false"/>
                                        </html:messages>

                                    </td>
                                </tr>
                                <tr>
                                    <th>SIM Version</th>
                                    <td>
                                        <html:select property="simVersion">
                                            <html:option value="0">Default</html:option>
                                            <html:option value="1">One</html:option>
                                            <html:option value="2">Two</html:option>
                                            <html:option value="3">Three</html:option>
                                        </html:select>
                                        <html:messages id="simVersion" property="simVersion">
                                            <bean:write name="simVersion"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                          
                                <tr>
                                    <th>Dealer</th>
                                    <td>
                                        <%
                                            ArrayList<DealerDTO> dealerList = DealerLoader.getInstance().getDealerDTOList();
                                        %>                                        
                                        <html:select property="dealerID">
                                            <html:option value="<%=String.valueOf(Constants.ROOT_RESELLER_PARENT_ID)%>">Select Dealer</html:option>
                                            <%
                                                if (dealerList != null && dealerList.size() > 0) {
                                                    int size = dealerList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        DealerDTO d_dto = (DealerDTO) dealerList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(d_dto.getId())%>"><%=d_dto.getDealerName()%></html:option>
                                            <%}
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="dealerID" property="dealerId">
                                            <bean:write name="dealerID"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <%
                                   }
                                %>
                                
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="searchLink" value="<%=link%>" />
                                        <html:hidden property="id" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Update" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>