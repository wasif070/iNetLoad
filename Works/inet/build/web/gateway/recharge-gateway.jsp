<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.FLEXI_SIM_RECHARGE_ADD && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,java.util.ArrayList,java.text.SimpleDateFormat,java.util.Date" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
            int pageNo = 1;
            int sortingOrder = 0;
            int sortedItem = 0;
            int list_all = 0;
            int recordPerPage = 100;

            if (request.getSession(true).getAttribute(Constants.FLEXI_SIM_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_SIM_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }
            if (request.getParameter("d-49216-s") != null) {
                sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
            }
            if (request.getParameter("d-49216-o") != null) {
                sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
            }
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j = 0;
        var k = 0;
        var l = 0;
        var n = 0;
        var m = 0;
        var x = 0;
        var y = 0;
        var amountFlag = false;
        var descriptionFlag = false;
        var dateFlag = false;
        var selectedIDsArray = [];
        var amountIDsArray = [];
        var amountArray = [];
        var descriptionIDsArray = [];
        var descriptionArray = [];
        var dateIDsArray = [];
        var dateArray = [];
        var allInputArray = document.forms[0].elements;
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j] = i;
                    j++;
                    amountFlag = true;
                    descriptionFlag = true;
                    dateFlag = true;
                }
            }
            else if(allInputArray[i].name == "amounts")
            {
                amountIDsArray[l] = i;
                l++;
                if(amountFlag)
                {
                    tempInputbox = allInputArray[i];
                    amountArray[k]=tempInputbox.value;
                    k++;
                    amountFlag = false;
                }
            }
            else if(allInputArray[i].name == "descriptions")
            {
                descriptionIDsArray[m] = i;
                m++;
                if(descriptionFlag)
                {
                    tempInputbox = allInputArray[i];
                    descriptionArray[n]=tempInputbox.value;
                    n++;
                    descriptionFlag = false;
                }
            }
            else if(allInputArray[i].name == "dates")
            {
                dateIDsArray[x] = i;
                x++;
                if(dateFlag)
                {
                    tempInputbox = allInputArray[i];
                    dateArray[y]=tempInputbox.value;
                    y++;
                    dateFlag = false;
                }
            }
        }
            document.forms[0].reset();
            for(t=0; t<allInputArray.length; t++)
            {

                checkFlag = 0;
                for(i=0; i<selectedIDsArray.length; i++)
                {
                    if(t==selectedIDsArray[i])
                    {
                        checkFlag = 1;
                        break;
                    }
                }
                if(checkFlag==1)
                {
                    tempCheckbox = allInputArray[selectedIDsArray[i]];
                    tempCheckbox.checked = true;
                    tempInputbox = allInputArray[amountIDsArray[i]];
                    tempInputbox.value = amountArray[i];
                    tempInputbox = allInputArray[descriptionIDsArray[i]];
                    tempInputbox.value = descriptionArray[i];
                    tempInputbox = allInputArray[dateIDsArray[i]];
                    tempInputbox.value = dateArray[i];
                }
                else
                {
                    tempCheckbox = allInputArray[t];
                    tempCheckbox.checked = false;
                }
            }

            if(document.forms[0].pageNo.value<=0)
            {
                document.forms[0].pageNo.value = 1;
            }

            link = "../gateway/rechargeGateway.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
            sortedItemVal = <%=sortedItem%>;
            if(sortedItemVal>0)
            {
                link += "&d-49216-s="+sortedItemVal;
            }
            sortingOrderVal = <%=sortingOrder%>;
            if(sortingOrderVal>0)
            {
                link += "&d-49216-o="+sortingOrderVal;
            }
            document.forms[0].action = link;
        }

        function search()
        {
            if(document.forms[0].pageNo.value<=0)
            {
                document.forms[0].pageNo.value = 1;
            }
            document.forms[0].action = "../gateway/rechargeGateway.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
        }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Flexi SIM Recharge History</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/gateway/rechargeGateway.do" method="post">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Phone No</th>
                                        <td><html:text property="phoneNoPar" /></td>
                                    </tr>
                                    <tr>
                                        <th>SIM ID</th>
                                        <td><html:text property="simIDPar" /></td>
                                    </tr>
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="full-div">
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                                        if (request.getSession(true).getAttribute(Constants.MESSAGE) != null) {
                                            out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                        }
                                        boolean gatewayRechargePer = false;
                                        if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.FLEXI_SIM_RECHARGE_ADD && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                                            gatewayRechargePer = true;
                                        }
                                        SimpleDateFormat bangDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            %>
                        </div>
                        <div style="border : 1px solid #848284;" align="center">
                            <div class="blank-height"></div>
                            <script type="text/javascript">count=<%=(pageNo - 1) * recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.GatewayForm.gatewayList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Gateway" />
                                <display:setProperty name="paging.banner.items_name" value="Gateways" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:7%;" >
                                    <script type="text/javascript">
                                            document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  class="custom_column1" property="simID" title="SIM ID" sortable="true"  style="width:15%" />
                                <display:column  property="phoneNo" title="Phone No" sortable="true" style="width:15%" />
                                <display:column  class="custom_column2" property="balance" title="Balance" sortable="true" style="width:15%" />
                                <display:column title="Amount" sortable="false" class="custom_column1" style="width:12%">
                                    <input type="text" name="amounts" style="width: 80px;border-color: #848284; border-style: solid;border-width: 1px;" value="0" />
                                </display:column>
                                <display:column title="Description" sortable="false" class="custom_column1" style="width:12%">
                                    <input type="text" name="descriptions" style="width: 80px;border-color: #848284; border-style: solid;border-width: 1px;" value=" " />
                                </display:column>
                                <display:column title="Date" sortable="false" class="custom_column1" style="width:13%">
                                    <input type="text" name="dates" style="width: 80px;border-color: #848284; border-style: solid;border-width: 1px;" value="<%=bangDateFormat.format(new Date())%>" />
                                </display:column>
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                    idList = "<%=request.getAttribute(Constants.FLEXI_SIM_ID_LIST)%>";
                                    if(idList.indexOf(",")!=-1)
                                    {
                                        inputArray = document.getElementsByTagName("input");
                                        for(i=0; i<inputArray.length; i++)
                                        {
                                            if(inputArray[i].name == "selectedIDs" )
                                            {
                                                tempCheckbox = inputArray[i];

                                                if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                                {
                                                    tempCheckbox.checked = true;
                                                }
                                                else
                                                {
                                                    tempCheckbox.checked = false;
                                                }
                                            }
                                        }
                                    }
                                    if(count==0)
                                    {
                                        document.write("<BR><BR>");
                                    }
                            </script>
                            <%
                                        request.removeAttribute(Constants.FLEXI_SIM_ID_LIST);
                                        request.getSession(true).removeAttribute(Constants.MESSAGE);
                                        if (gatewayRechargePer) {
                            %>
                            <input type="submit" class="action-button" value="Add Selected" style="width: 120px;" onclick="submitform('<%=Constants.RECHARGE%>')" />
                            <div class="blank-height"></div>
                            <%
                                        }
                            %>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>