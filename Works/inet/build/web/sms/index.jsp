<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO!=null && !perDTO.REFILL
                && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3
                && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT)
                 {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.flexi.FlexiDTO,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>

<%
            int requestType = Constants.INETLOAD_STATUS_SUBMITTED;
            if (request.getParameter("requestType") != null) {
                requestType = Integer.parseInt(request.getParameter("requestType"));
            }
            String title = SettingsLoader.getInstance().getSettingsDTO().getBrandName() + " :: Pending Refill List";
            String des = "Pending ";
            String export = "false";
            int order = 8;
            if (requestType == Constants.INETLOAD_STATUS_SUCCESSFUL) {
                title = SettingsLoader.getInstance().getSettingsDTO().getBrandName() + " :: Successful Refill List";
                des = "Successful ";
                export = "true";
                order = 9;
            } else if (requestType == Constants.INETLOAD_STATUS_REJECTED) {
                title = SettingsLoader.getInstance().getSettingsDTO().getBrandName() + " :: Rejected Refill List";
                des = "Rejected ";
            }
            int pageNo = 1;
            int recordPerPage = SettingsLoader.getInstance().getSettingsDTO().getRecordPerPageForReporting();
            int list_all = 0;
            int refresh = 0;

            if (request.getSession(true).getAttribute(Constants.FLEXI_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }
            if (request.getParameter("list_all") != null) {
               list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("refresh") != null) {
               refresh = Integer.parseInt(request.getParameter("refresh"));
            }
            String action = "/sms/listSMS.do?requestType=" + requestType;
%>

<script language="javascript" type="text/javascript">

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        requestTypeVal = <%=requestType%>;
        document.forms[0].action = "../sms/listSMS.do?list_all=0&requestType=" + requestTypeVal+"&d-49216-p="+document.forms[0].pageNo.value;
    }

</script>

<html>
    <head>
        <title><%=title%></title>
        <%
                    if (refresh == 1) {
        %>
        <meta http-equiv="refresh" content="30" />
        <%                    }
        %>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <div class="full-div height-5px"></div>
                    <div class="full-div"><strong><%=des%>Refill Requests</strong></div>
                    <div class="clear"></div>
                    <div class="full-div">
                        <html:form action="<%=action%>" method="post" >
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Phone Number</th>
                                    <td><html:text property="phoneNumber" styleId="phnNumber"/></td>
                                    <th>Transaction Id</th>
                                    <td><html:text property="transactionId" styleId="transactionId" /></td>
                                </tr>
                                <tr>
                                    <th>Request From</th>
                                    <td><html:text property="fromClientId" styleId="fromClientId"/></td>
                                    <th>Amount</th>
                                    <td>
                                        <html:select property="sign" style="width:47px" styleId="sign">
                                            <html:option value="0">All</html:option>
                                            <html:option value="1">=</html:option>
                                            <html:option value="2"><=</html:option>
                                            <html:option value="3">>=</html:option>
                                        </html:select>
                                        <html:text property="amount" style="width:100px" styleId="amount" />
                                    </td>
                                </tr>
                                <tr>
                                    <th>Start Time</th>
                                    <td>
                                        <html:select property="startDay" styleClass="width-50px" styleId="startDay">
                                            <%
                                                        ArrayList<Integer> days = Utils.getDay();
                                                        for (int i = 0; i < days.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startMonth" styleClass="width-50px" styleId="startMonth">
                                            <%
                                                        ArrayList<String> months = Utils.getMonth();
                                                        for (int i = 0; i < months.size(); i++) {
                                                            String month = months.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startYear" styleClass="width-60px" styleId="startYear">
                                            <%
                                                        ArrayList<Integer> years = Utils.getYear();
                                                        for (int i = 0; i < years .size(); i++) {
                                                            String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>End Time</th>
                                    <td>
                                        <html:select property="endDay" styleClass="width-50px" styleId="endDay">
                                            <%
                                                        ArrayList<Integer> days1 = Utils.getDay();
                                                        for (int i = 0; i < days1.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endMonth" styleClass="width-50px" styleId="endMonth">
                                            <%
                                                        ArrayList<String> months1 = Utils.getMonth();
                                                        for (int i = 0; i < months1.size(); i++) {
                                                            String month = months1.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endYear" styleClass="width-60px" styleId="endYear">
                                            <%
                                                        ArrayList<Integer> years1 = Utils.getYear();
                                                        for (int i = 0; i < years1.size(); i++) {
                                                            String year = String.valueOf(years1.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="pageNo" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div class="full-div" style="text-align: center;">
                                            <div class="clear height-5px"></div>
                                            <html:submit property="startSearching" styleClass="search-button" value="Search"  onclick="search()" />
                                            <html:reset styleClass="search-button" value="Reset" />
                                            <div class="clear height-5px"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </html:form>
                    </div>
                    <div class="clear height-20px">
                        <%
                                  if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                                  {
                                    out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                  }
                                  boolean refillPer = false;
                                  boolean message = false;
                                  if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.REFILL))
                                          && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                                  {
                                      refillPer = true;
                                  }
                                  if (login_dto.getIsUser() || requestType == Constants.INETLOAD_STATUS_REJECTED || requestType == Constants.INETLOAD_STATUS_SUBMITTED)
                                  {
                                      message = true;
                                  }                                      
                        %>
                    </div>
                    <c:set var="status" scope="page" value="<%=des.trim()%>"/>
                    <c:set var="refillPer" scope="page" value="<%=refillPer%>"/>
                    <c:set var="requestType" scope="page" value="<%=requestType%>"/>
                    <c:set var="is_mes" scope="page" value="<%=message%>"/>
                    <c:set var="order" scope="page" value="<%=order%>"/>
                    <c:set var="export" scope="page" value="<%=export%>"/>
                    <div class="clear"></div>
                    <div class="full-div">
                        <jsp:scriptlet>
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                            public String addRowClass() {
                                long time_diff = 30 * 60 * 1000;
                                FlexiDTO obj = (FlexiDTO) getCurrentRowObject();
                                if ((System.currentTimeMillis() - obj.getFlexiRequestTimeVal()) > time_diff && obj.getFlexiRequestType() == Constants.INETLOAD_STATUS_SUBMITTED) {
                                    return "red";
                                } else {
                                    return "";
                                }
                            }
                        });
                        </jsp:scriptlet>
                        <script type="text/javascript">
                            var total_amount=0;
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>
                        <display:table class="reporting_table" style="width:100%;" decorator="dyndecorator"  defaultsort="${order}" defaultorder="descending" name="sessionScope.FlexiForm.flexiList" cellpadding="0" cellspacing="0" export="${export}" id="data" pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Request" />
                            <display:setProperty name="paging.banner.items_name" value="Requests" />
                            <display:column class="custom_column2" title="Nr" media="html" style="width:6%;">
                                <script type="text/javascript">
                                    total_amount=total_amount+${data.flexiAmount};
                                    document.write(++count+".");
                                </script>
                            </display:column>
                            <display:column class="custom_column1" property="flexiPhoneNo" title="Phone No." sortable="false" style="width:10%;" />
                            <display:column property="flexiFrom" title="Request From" sortable="true" style="width:12%;" />
                            <display:column class="custom_column1" property="flexiType" title="Type" sortable="true" style="width:9%;" />
                            <display:column class="custom_column2" property="flexiAmount" format="{0,number,0.00}" title="Amount" sortable="true" style="width:10%;" />
                            <display:column class="custom_column1" title="Status" sortProperty="flexiRequestType" sortable="true" style="width:10%;" media="html">
                                <c:choose>
                                    <c:when test="${refillPer==true && requestType!=5 && data.flexiOpType!=7}"><span class="pointer blue" onclick="javascript:update(${data.flexiAgentID},${data.flexiAmount},${data.flexiRequestType},'${data.flexiRequestId}',${data.flexiTypeVal},${data.flexiOpType},${data.flexiCardID})"><u>${status}</u></span></c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${requestType==5}">${data.flexiRequestTypeName}</c:when>
                                            <c:otherwise>${status}</c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose> 
                            </display:column>
                            <display:column class="custom_column1" title="Status" media="csv excel pdf" style="width:10%;" >
                                ${status}
                            </display:column>
                            <display:column class="custom_column1" property="flexiRequestTimeVal" decorator="implement.displaytag.LongDateWrapper" title="Request Time" sortable="true" style="width:11%;" />
                            <display:column class="custom_column1" property="flexiSuccessTimeVal" decorator="implement.displaytag.LongDateWrapper" title="Success Time" sortable="true" style="width:11%;" />
                            <display:column sortable="false" title="Message" style="width:14%;" media="html" >
                                <span>${data.flexiMessage}</span>
                            </display:column>
                            <display:column property="flexiTransactionId" title="Transaction ID" style="width:14%;"  media="csv excel pdf" />
                        </display:table>
                        <div class="clear"></div>
                        <div class="half-div">
                            <table class="reporting_table" cellspacing="0" border="0" cellpadding="0" style="width:100%;">
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold" align="center" >Total Refill Amount :&nbsp;
                                        <script type="text/javascript">
                                            document.write(parseInt(total_amount+"")+".00 Tk.");
                                        </script>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <%
            request.getSession(true).removeAttribute(Constants.MESSAGE);
            %>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>

        <div id="status-form" title="Change Status" style="font-weight: normal">
            <form id="status_form" method="post" action="/update.jsp">
                <input type="hidden" id="jagentID"/>
                <input type="hidden" id="joperatorType"/>
                <input type="hidden" id="jamount"/>
                <input type="hidden" id="jmarker"/>
                <input type="hidden" id="jcurrent_status"/>
                <input type="hidden" id="jflexiCardID"/>
                <table width="10%" border="0" cellspacing="1" cellpadding="1">
                    <tr>
                        <td  valign="middle" align="right" width="30%">
                            <input type="radio" class="jstatus" name="status" value="<%=Constants.INETLOAD_STATUS_SUBMITTED%>" style="width: 15px" />
                        </td>
                        <td align="left">
                            <label for="pending">Pending</label>
                        </td>
                    </tr>
                    <tr>
                        <td  valign="middle" align="right" width="30%">
                            <input type="radio" class="jstatus" name="status" value="<%=Constants.INETLOAD_STATUS_SUCCESSFUL%>" style="width: 15px" />
                        </td><td align="left">
                            <label for="successful">Successful</label><br>
                                <label for="transactionID">Transaction ID</label>
                                <input type="text" class="jtransactionID" name="transactionID" style="width: 80px" />
                        </td>
                    </tr>
                    <tr>
                        <td  valign="middle" align="right" width="30%">
                            <input type="radio" class="jstatus" name="status" value="<%=Constants.INETLOAD_STATUS_REJECTED%>" style="width: 15px" />
                        </td><td align="left">
                            <label for="rejected">Rejected</label>
                        </td>
                    </tr>
                </table>
            </form>
        </div>


        <script language="javascript" type="text/javascript">

            function update(agentID,amount,current_status,marker,refType,operatorType,flexiCardID){       
                                                                                 
                $('#jagentID').val(agentID);
                $('#jamount').val(amount);
                $('#jmarker').val(marker);
                $('#jcurrent_status').val(current_status);
                $('#joperatorType').val(operatorType);
                $('#jflexiCardID').val(flexiCardID); 
                $("input[name='status']:eq("+(current_status-1)+")").attr('checked', 'checked');
                //                                                                                    $("input[name='refillType']:eq("+(refType-1)+")").attr('checked', 'checked');                                                                                
                $('#status-form').dialog('option','title','Change Status').dialog('open');
                                                                                
                                           
            }

            $(function(){
                $("#status-form").dialog({autoOpen: false, modal: true, height:300,width:350,
                    buttons: {
                        Cancel: function() {
                            $(this).dialog('close');
                        },
                        Ok: function() {
                            var agentID=$('#jagentID').val();
                            var amount=$('#jamount').val();
                            var marker=$('#jmarker').val();
                            var current_status=$('#jcurrent_status').val();
                            var status=$("input:radio[name='status']:checked").val();
                            var transactionID=$("input:text[name='transactionID']").val();
                            //var refillType=$("input:radio[name='refillType']:checked").val();
                            var refillType=<%=Constants.SMS%>;
                            var operatorType=$('#joperatorType').val();
                            var flexiCardID=$('#jflexiCardID').val();
                                                                                            
                            $.ajax({
                                url:"update.jsp",
                                type:"POST",
                                data:{agentID:agentID,amount:amount,status:status,marker:marker,current_status:current_status,transactionID:transactionID,refillType:refillType,operatorType:operatorType,flexiCardID:flexiCardID,sid:getSID(),rand:Math.random()},
                                cache: false,
                                dataType:'html',
                                async:false,
                                success:function(html) {
                                    if(html!=''){
                                        $(location).attr("href",location.href+"&sid="+getSID()+"&rand="+Math.random());
                                    }
                                },
                                error:function(a,b,c){
                                    alert(a+b+c);
                                }
                            });
                        }
                    },
                    close: function() {
                    }
                });
            });
            function getSID(){
                var sid=new Date();
                return sid.getTime();
            }

        </script>
    </body>
</html>

