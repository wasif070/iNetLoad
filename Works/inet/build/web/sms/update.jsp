<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.login.LoginDTO"%>
<%@page import="com.myapp.struts.session.Constants"%>
<%
    FlexiDAO dao = new FlexiDAO();
    LoginDTO LOGIN_DTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
    long agentID = Long.parseLong(request.getParameter("agentID"));
    int status = Integer.parseInt(request.getParameter("status"));
    double amount = Double.parseDouble(request.getParameter("amount"));
    String marker = request.getParameter("marker");
    int current_status = Integer.parseInt(request.getParameter("current_status"));
    String transactionID = request.getParameter("transactionID");
    int refill_type = Integer.parseInt(request.getParameter("refillType"));
    int op_type = Integer.parseInt(request.getParameter("operatorType"));
    long card_id = Long.parseLong(request.getParameter("flexiCardID"));
    boolean result = dao.updateStatus(LOGIN_DTO, agentID, amount, status, marker, current_status, transactionID, refill_type, op_type, card_id);
    if (result) {
        request.getSession(true).setAttribute(Constants.MESSAGE, "<span style='color:blue; font-size: 12px;font-weight:bold'>Flexi Status is changed successfully.</span>");
    } else {
        request.getSession(true).setAttribute(Constants.MESSAGE, "<span style='color:red; font-size: 12px;font-weight:bold'>Flexi Status is not changed.</span>");
    }
%>
