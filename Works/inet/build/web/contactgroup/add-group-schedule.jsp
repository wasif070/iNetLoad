<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
            response.sendRedirect("../home/home.do");
            return;
        }
    }
%>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Group Schedule</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/contactgroup/addGroupSchedule" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Add Group Schedule For: <font color="#e17009"><%=request.getParameter("groupName")%></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="GroupScheduleForm" property="message" filter="false"/>
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th>Week Day</th>
                                    <td>
                                        <html:select property="scheduleWeekDay" >
                                            <%
                                                for (int i = 0; i < Constants.WEEK_DAY_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.WEEK_DAY_VALUE[i]%>"><%=Constants.WEEK_DAY_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>                                                                                        
                                        </html:select>
                                        <html:messages id="scheduleWeekDay" property="scheduleWeekDay">
                                            <bean:write name="scheduleWeekDay"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Hour</th>
                                    <td>
                                        <html:select property="scheduleHour" >
                                            <html:option value="0">12 AM</html:option>
                                            <html:option value="1">1 AM</html:option>
                                            <html:option value="2">2 AM</html:option> 
                                            <html:option value="3">3 AM</html:option>
                                            <html:option value="4">4 AM</html:option>
                                            <html:option value="5">5 AM</html:option>
                                            <html:option value="6">6 AM</html:option>
                                            <html:option value="7">7 AM</html:option>
                                            <html:option value="8">8 AM</html:option>
                                            <html:option value="9">9 AM</html:option>
                                            <html:option value="10">10 AM</html:option>
                                            <html:option value="11">11 AM</html:option>
                                            <html:option value="12">12 PM</html:option>
                                            <html:option value="13">1 PM</html:option>
                                            <html:option value="14">2 PM</html:option> 
                                            <html:option value="15">3 PM</html:option>
                                            <html:option value="16">4 PM</html:option>
                                            <html:option value="17">5 PM</html:option>
                                            <html:option value="18">6 PM</html:option>
                                            <html:option value="19">7 PM</html:option>
                                            <html:option value="20">8 PM</html:option>
                                            <html:option value="21">9 PM</html:option>
                                            <html:option value="22">10 PM</html:option>
                                            <html:option value="23">11 PM</html:option>                                                                    
                                        </html:select>
                                        <html:messages id="scheduleHour" property="scheduleHour">
                                            <bean:write name="scheduleHour"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Minute</th>
                                    <td>
                                        <html:select property="scheduleMinute" >
                                            <html:option value="0">00</html:option>
                                            <html:option value="5">05</html:option>
                                            <html:option value="10">10</html:option>
                                            <html:option value="15">15</html:option>
                                            <html:option value="20">20</html:option>
                                            <html:option value="25">25</html:option>
                                            <html:option value="30">30</html:option>
                                            <html:option value="35">35</html:option>
                                            <html:option value="40">40</html:option>
                                            <html:option value="45">45</html:option>
                                            <html:option value="50">50</html:option>
                                            <html:option value="55">55</html:option>
                                        </html:select>
                                        <html:messages id="scheduleMinute" property="scheduleMinute">
                                            <bean:write name="scheduleMinute"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Refill Amount</th>
                                    <td>
                                        <html:text property="scheduleAmount"  />
                                        <html:messages id="scheduleAmount" property="scheduleAmount">
                                            <bean:write name="scheduleAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                                                      

                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <html:hidden property="scheduleType" value="<%=String.valueOf(Constants.SCHEDULE_TYPE_NO_REPEAT)%>" />
                                        <html:hidden property="groupID" value="<%=request.getParameter("groupID")%>" />
                                        <html:hidden property="groupName" value="<%=request.getParameter("groupName")%>" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>