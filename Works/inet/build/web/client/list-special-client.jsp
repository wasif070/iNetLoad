<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO!=null && !perDTO.CLIENT ) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.client.ClientLoader"%>
<%@page import="java.util.ArrayList,com.myapp.struts.client.ClientDTO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 100;

   if (request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j=0;
        var selectedIDsArray = [];
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" )
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j]=i;
                    j++;
                }
            }
        }
        document.forms[0].reset();
        for(i=0; i<selectedIDsArray.length; i++)
        {
            tempCheckbox = allInputArray[selectedIDsArray[i]];
            tempCheckbox.checked = true;
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }

        link = "../client/listSpecialClient.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+s        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
        document.forms[0].submit();
    }


    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../client/listSpecialClient.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editClient(value1,value2)
    {
        document.forms[0].reset();
        link = "clientId="+document.forms[0].clientId.value+"&clientStatus="+document.forms[0].clientStatus.value+"&sign="+document.forms[0].sign.value+"&clientCredit="+document.forms[0].clientCredit.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = "../client/getClient.do?id="+value1+"&name="+value2+"&special=1&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Special Client List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/client/listClient.do" method="post" styleId="client_form">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Client ID</th>
                                        <td>
                                            <html:text property="clientId">
                                            </html:text>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>
                                            <html:select property="clientStatus">
                                                <html:option value="-1">Select</html:option>
                                                <%
                                                 for(int i=0;i<Constants.LIVE_STATUS_VALUE.length;i++)
                                                 {
                                                %>
                                                <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                                <%
                                                 }
                                                %>
                                            </html:select>
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Balance</th>
                                        <td>
                                            <html:select property="sign" style="width:47px">
                                                <html:option value="0">All</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_EQUAL)%>">=</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_SMALLER_THAN)%>"><</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_GREATER_THAN)%>">></html:option>
                                            </html:select>
                                            <html:text property="clientCredit" style="width:100px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>    
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                              boolean clientEditPer = false;
                              if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.CLIENT_EDIT))
                                      && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                              {
                                  clientEditPer = true;
                              }
                            %>
                        </div>
                        <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                        <c:set var="clientEdit" scope="page" value="<%=clientEditPer%>"/>
                        <div style="border : 1px solid #848284;" align="center">
                            <div class="height-10px"></div>
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ClientForm.clientList" pagesize="<%=recordPerPage%>">
                                <display:setProperty name="paging.banner.item_name" value="Special Client" />
                                <display:setProperty name="paging.banner.items_name" value="Special Clients" />
                                <display:column class="custom_column2" title="Nr" style="width:7%;" media="html">
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column title="Client ID" property="clientId" sortable="true" style="width:27%;" media="html" />
                                 <display:column title="Client ID" property="clientId" media="csv excel pdf" />               
                                <display:column property="clientEmail" title="Client Email" sortable="true" style="width:27%" />                                
                                <display:column  title="Balance" property="clientCredit" format="{0,number,0.00}" sortProperty="clientCredit" sortable="true" class="custom_column2" style="width:17%" />
                                <display:column title="Status" sortProperty="clientStatus" sortable="true" class="custom_column1" style="width:12%" >
                                    <c:choose>
                                        <c:when test="${data.clientStatus==1}">Active</c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${data.clientStatus==0}">Inactive</c:when>
                                                <c:otherwise>Block</c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </display:column>
                                <display:column title="Action" sortable="false" class="custom_column1" style="width:10%" media="html">
                                    <c:choose>
                                        <c:when test="${clientEdit==false}">Edit</c:when>
                                        <c:otherwise><a href="#" onclick="editClient('${data.id}','${data.clientId}')">Edit</a></c:otherwise>
                                    </c:choose>
                                </display:column>
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.CLIENT_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <div class="clear height-10px"></div>
                        </div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>