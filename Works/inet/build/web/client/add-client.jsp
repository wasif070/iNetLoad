<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
            && perDTO != null && !perDTO.CLIENT_ADD && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER
            && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2)
            || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
    long parentID = -1;
    if (request.getParameter("parentId") != null) {
        parentID = Long.parseLong(request.getParameter("parentId"));
    }
%>

<%@page import="com.myapp.struts.client.ClientLoader,java.util.ArrayList,com.myapp.struts.client.ClientDTO"%>
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<script language="javascript" type="text/javascript">

    window.onload=function(){
        getParentList();
    }

    function getParentList(){
        if(document.forms[0].clientTypeId.value=="<%=Constants.CLIENT_TYPE_AGENT%>")
        {
            document.forms[0].clientIp.disabled = false;
            //document.forms[0].clientAni.disabled = false;
        }
        else
        {
            document.forms[0].clientIp.disabled = true;
            //document.forms[0].clientAni.disabled = true;
        }     
        $.ajax({
            url:"../client/parent-list.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random(),type:document.forms[0].clientTypeId.value},
            cache: false,
            dataType:'html',
            async:false,
            success:function(html) {
                if(html!=''){
                    $(".parent-list").html($.trim(html));
                }
            }
        });
    }

    function getSID(){
        var sid=new Date();
        return sid.getTime();
    }
    
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Client</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/client/addClient" method="post" >
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Add New Client</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px; " valign="bottom">
                                        <bean:write name="ClientForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Client ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientId"  /><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Client Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="clientTypeId" onchange="getParentList()">
                                            <%
                                                if (login_dto.getIsUser() || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER) {
                                            %>
                                            <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER]%></html:option>                                
                                            <%
                                                }
                                                if (login_dto.getIsUser()) {
                                            %>
                                            <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER3)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER3]%></html:option>
                                            <%
                                                }
                                                if (login_dto.getIsUser() || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3) {
                                            %>
                                            <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER2)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER2]%></html:option>
                                            <%
                                                }
                                                if (login_dto.getIsUser() || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
                                            %>
                                            <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_AGENT)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_AGENT]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientTypeId" property="clientTypeId">
                                            <bean:write name="clientTypeId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <%
                                    if (login_dto.getIsUser()) {
                                %>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Parent</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <div class="parent-list"></div>

                                        <html:messages id="parentId" property="parentId">
                                            <bean:write name="parentId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <%                                } else {
                                %>
                                <html:hidden property="parentId" value="<%=String.valueOf(login_dto.getId())%>" />
                                <%
                                    }
                                %>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="clientPassword" /><br/>
                                        <html:messages id="clientPassword" property="clientPassword">
                                            <bean:write name="clientPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Status</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="clientStatus">
                                            <%
                                                for (int i = 1; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientStatus" property="clientStatus">
                                            <bean:write name="clientStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                                                                                                       
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Initial Balance</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientCredit" /><br/>
                                        <html:messages id="clientCredit" property="clientCredit">
                                            <bean:write name="clientCredit"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Discount(%)</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientDiscount" /><br/>
                                        <html:messages id="clientDiscount" property="clientDiscount">
                                            <bean:write name="clientDiscount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Min. Deposit</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientDeposit" /><br/>
                                        <html:messages id="clientDeposit" property="clientDeposit">
                                            <bean:write name="clientDeposit"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Full Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientName" /><br/>
                                        <html:messages id="clientName" property="clientName">
                                            <bean:write name="clientName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Email Address</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientEmail" /><br/>
                                        <html:messages id="clientEmail" property="clientEmail">
                                            <bean:write name="clientEmail"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>    
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">SMS Sender</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="smsSender" /><br/>
                                        <html:messages id="smsSender" property="smsSender">
                                            <bean:write name="smsSender"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Allowed IP</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientIp" /><br/>
                                        <html:messages id="clientIp" property="clientIp">
                                            <bean:write name="clientIp"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <%--
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Allowed ANI</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientAni" /><br/>
                                        <html:messages id="clientAni" property="clientAni">
                                            <bean:write name="clientAni"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                --%>
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="clientVersion" value="0" />
                                        <input type="hidden" name="clientRegister" value="0" />
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Save" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>