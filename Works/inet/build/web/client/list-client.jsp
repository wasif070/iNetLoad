<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO!=null && !perDTO.CLIENT 
                && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                ) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.client.ClientLoader"%>
<%@page import="java.util.ArrayList,com.myapp.struts.client.ClientDTO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>


<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 100;

   if (request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j=0;
        var selectedIDsArray = [];
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" )
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j]=i;
                    j++;
                }
            }
        }
        document.forms[0].reset();
        for(i=0; i<selectedIDsArray.length; i++)
        {
            tempCheckbox = allInputArray[selectedIDsArray[i]];
            tempCheckbox.checked = true;
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }

        link = "../client/listClient.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+s        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
        document.forms[0].submit();
    }


    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../client/listClient.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editClient(value1,value2)
    {
        document.forms[0].reset();
        //link = "clientId="+document.forms[0].clientId.value+"&clientTypeId="+document.forms[0].clientTypeId.value+"&clientStatus="+document.forms[0].clientStatus.value+"&clientAni="+document.forms[0].clientId.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        link = "clientId="+document.forms[0].clientId.value+"&clientTypeId="+document.forms[0].clientTypeId.value+"&clientStatus="+document.forms[0].clientStatus.value+"&sign="+document.forms[0].sign.value+"&clientCredit="+document.forms[0].clientCredit.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = "../client/getClient.do?id="+value1+"&name="+value2+"&special=0&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Client List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/client/listClient.do" method="post" styleId="client_form">
                        <div class="full-div">
                            <div class="full-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Client ID</th>
                                        <td>
                                            <html:text property="clientId" />
                                        </td>
                                        <th>Client Type</th>
                                        <td>
                                            <html:select property="clientTypeId">
                                                <html:option value="0">Select</html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER3)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER3]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER2)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_RESELLER2]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_AGENT)%>"><%=Constants.CLIENT_TYPE[Constants.CLIENT_TYPE_AGENT]%></html:option>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>
                                            <html:select property="clientStatus">
                                                <html:option value="-1">Select</html:option>
                                                <%
                                                 for(int i=0;i<Constants.LIVE_STATUS_VALUE.length;i++)
                                                 {
                                                %>
                                                <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                                <%
                                                 }
                                                %>
                                            </html:select>
                                        </td>
                                        <th>Balance</th>
                                        <td>
                                            <html:select property="sign" style="width:47px">
                                                <html:option value="0">All</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_EQUAL)%>">=</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_SMALLER_THAN)%>"><</html:option>
                                                <html:option value="<%=String.valueOf(Constants.BALANCE_GREATER_THAN)%>">></html:option>
                                            </html:select>
                                            <html:text property="clientCredit" style="width:100px" />
                                        </td>                                     
                                        <%--
                                        <th>ANI</th>
                                        <td>
                                            <html:text property="clientAni" />
                                        </td>
                                        --%>
                                        <html:hidden property="parentId" />
                                    </tr>
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                              boolean clientEditPer = false;
                              if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.CLIENT_EDIT)
                                      || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER)
                                      || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3) 
                                      || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2))
                                      && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                              {
                                  clientEditPer = true;
                              }
                            %>
                        </div>
                        <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                        <c:set var="clientEdit" scope="page" value="<%=clientEditPer%>"/>
                        <div style="border : 1px solid #848284;" align="center">
                            <%
                               boolean parFound = false;
                               long parID = 0;
                               String parName = "";
                               String parLink = "";
                               if(request.getParameter("parentId")!=null)
                               {    
                                  parID = Long.parseLong(request.getParameter("parentId"));
                               } 
                               if(parID>0 && login_dto.getIsUser())
                               {
                                   parFound = true;
                                   parName = ClientLoader.getInstance().getClientDTOByID(parID).getClientId();
                                   parLink = "listClient.do?list_all=1&parentId="+ClientLoader.getInstance().getClientDTOByID(parID).getParentId();
                               }
                               if(parFound)
                               {
                            %>
                            <div class="height-5px"></div>
                            <div class="height-20px">Parent: <a href="<%=parLink%>" ><u><%=parName%></u></a></div>
                            <%
                               }
                               else
                               {
                            %>
                            <div class="height-20px"></div>
                            <%
                               }
                            %>
                        <jsp:scriptlet>
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                            public String addRowClass() {
                                ClientDTO obj = (ClientDTO) getCurrentRowObject();
                                if (obj.getClientStatus() == Constants.USER_STATUS_BLOCK || obj.getClientStatus() == Constants.USER_STATUS_INACTIVE) {
                                    return "red";
                                } else {
                                    return "";
                                }
                            }
                        });
                        </jsp:scriptlet>                             
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" decorator="dyndecorator" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ClientForm.clientList" pagesize="<%=recordPerPage%>">
                                <display:setProperty name="paging.banner.item_name" value="Client" />
                                <display:setProperty name="paging.banner.items_name" value="Clients" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%" media="html">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:7%;" media="html">
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column title="Client ID" sortProperty="clientId" sortable="true" style="width:25%;" media="html">
                                    <c:choose>
                                        <c:when test="${is_user==true && (data.clientTypeId==1 || data.clientTypeId==3 || data.clientTypeId==4)}"><a href="../client/listClient.do?list_all=1&parentId=${data.id}"><u>${data.clientId}</u></a></c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${data.clientTypeId==1 || data.clientTypeId==3 || data.clientTypeId==4}"><font color="blue">${data.clientId}</font></c:when>
                                                <c:otherwise>${data.clientId} </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </display:column>
                                 <display:column title="Client ID" property="clientId" media="csv excel pdf" />               
                                <display:column property="clientTypeName" title="Client Type" sortable="true" class="custom_column1" style="width:15%" />
                                <display:column title="Status" sortProperty="clientStatus" sortable="true" class="custom_column1" style="width:13%">
                                    <c:choose>
                                        <c:when test="${data.clientStatus==1}">Active</c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${data.clientStatus==0}">Inactive</c:when>
                                                <c:otherwise>Block</c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </display:column>
                                <display:column  title="Balance" property="clientCredit" format="{0,number,0.0000}" sortProperty="clientCredit" sortable="true" class="custom_column2" style="width:15%" />
                                <display:column  title="Discount(%)" property="clientDiscount" format="{0,number,0.0000}" sortProperty="clientDiscount" sortable="true" class="custom_column2" style="width:15%" />
                                <display:column title="Action" sortable="false" class="custom_column1" style="width:10%" media="html">
                                    <c:choose>
                                        <c:when test="${clientEdit==false}">Edit</c:when>
                                        <c:otherwise><a href="#" onclick="editClient('${data.id}','${data.clientId}')">Edit</a></c:otherwise>
                                    </c:choose>
                                </display:column>
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.CLIENT_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <%
                                request.removeAttribute(Constants.CLIENT_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                                if(clientEditPer)
                                {
                            %>
                            <div class="clear height-5px"></div>
                            <input type="button" class="action-button" value="Activate Selected" style="width: 130px;" onclick="submitConfirm('<%=Constants.ACTIVATION%>')" />
                            <input type="button" class="action-button" value="Inactivate Selected" style="width: 140px;" onclick="submitConfirm('<%=Constants.DEACTIVATION%>')" />
                            <input type="button" class="action-button" value="Block Selected" style="width: 115px;" onclick="submitConfirm('<%=Constants.BLOCK%>')" />
                            <input type="button" class="action-button" value="Delete Selected" style="width: 115px;" onclick="submitConfirm('<%=Constants.DELETE%>')" />
                            <%
                                }
                            %>
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                        <div class="blank-height"></div>
                        <div class="blank-height"></div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
<div id="dialog-confirm" title="Recheck!!">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><div id="confirm-message" style="font-weight: normal; color:#e17009; font-size: 1.05em; text-align: left;margin-left: 50px"></div></p>
</div>

<script type="text/javascript" language="javascript">
    function submitConfirm(action){
        var message="You are going to";
        if(action==<%=Constants.ACTIVATION%>)
        {
            message +=" activate Clients?"
        }
        else if(action==<%=Constants.DEACTIVATION%>)
        {
            message +=" inactivate Clients?"
        }
        else if(action==<%=Constants.BLOCK%>)
        {
            message +=" block Clients?"
        }
        else if(action==<%=Constants.DELETE%>)
        {
            message +=" delete Clients?"
        }
        actionVal = action;
        $("#confirm-message").html(message);
        $('#dialog-confirm').dialog('option','title','Please Recheck!!').dialog('open');             
        return true;
    }
    $(function() {
        $("#dialog-confirm").dialog({autoOpen: false, modal: true, width:350,
            buttons: {
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Ok: function() {
                    $(this).dialog( "close" );
                    submitform(actionVal);
                }
            }
        });
        // End Changed
    });
</script>