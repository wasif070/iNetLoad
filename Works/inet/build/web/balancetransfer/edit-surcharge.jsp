<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
            String link = request.getQueryString();
            if (request.getParameter("searchLink") != null) {
                link = request.getParameter("searchLink");
            } else {
                link = "?" + link.substring(link.indexOf("&link=") + 6);
            }
%>

<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,java.util.ArrayList,com.myapp.struts.dealers.DealerDTO,com.myapp.struts.dealers.DealerLoader" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Edit Balance Trans. Surcharge</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/balancetransfer/editSurcharge" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Edit Balance Trans. Surcharge: <font color="#e17009"><%=request.getParameter("name")%></font></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="BalanceTransferForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Transfer Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="typeID" >
                                            <%
                                               for (int i = 1; i < Constants.TRANSFER_TYPE.length; i++) {
                                            %>
                                            <html:option value="<%=String.valueOf(i)%>"><%=Constants.TRANSFER_TYPE[i]%></html:option>
                                            <%
                                                                                            }
                                            %>
                                        </html:select><br/>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Min Transferred Amount</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="minTransferredAmount" /><br/>
                                        <html:messages id="minTransferredAmount" property="minTransferredAmount">
                                            <bean:write name="minTransferredAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Max Transferred Amount</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="maxTransferredAmount" /><br/>
                                        <html:messages id="maxTransferredAmount" property="maxTransferredAmount">
                                            <bean:write name="maxTransferredAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Surcharge (%)</th>
                                    <td>
                                        <html:text property="surchargeAmount" /><br/>
                                        <html:messages id="surchargeAmount" property="surchargeAmount">
                                            <bean:write name="surchargeAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="searchLink" value="<%=link%>" />
                                        <html:hidden property="id" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Update" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>