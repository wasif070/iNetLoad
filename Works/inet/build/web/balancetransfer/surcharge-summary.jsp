<%@page import="java.util.ArrayList,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.balancetransfer.BalanceTransferLoader,com.myapp.struts.balancetransfer.BalanceTransferDTO" %>

<%
          int btTypeID = Constants.TRANSFER_TYPE_BKASH;
          if (request.getParameter("btType") != null) {
              btTypeID = Integer.parseInt(request.getParameter("btType"));
          }
          ArrayList btSurchargeList = BalanceTransferLoader.getInstance().getBTSurchargeDTOList();  
          if(btSurchargeList != null && btSurchargeList.size() > 0)
          {                
%>          
<table style="width:90%;border: 2px solid #FFFFDD;" cellpadding="0" cellspacing="0" align="right">
    <tr>
        <td width="100%" colspan="2" align="center" style="font-weight: bold;border-bottom:  2px solid #FFFFDD;">
            <%
               out.print(Constants.TRANSFER_TYPE[btTypeID]+ " Surcharge List");
            %>
        </td>
    </tr> 
    <%
       BalanceTransferDTO dto = null;
       for(int i=0;i<btSurchargeList.size();i++)
       {   
           dto = (BalanceTransferDTO) btSurchargeList.get(i);           
           if(dto.getTypeID() != btTypeID)
           {
               continue;
           }         
    %>    
    <tr>           
        <td align="center"  style="width:65%; font-size: 10px; font-weight: bold; color: olive;border-right: 1px solid #FFFFDD;border-bottom:  1px solid #FFFFDD;"><%=dto.getMinTransferredAmount() + "-" + dto.getMaxTransferredAmount()%></td>
        <td align="right"  style="padding-right: 5px; width:35%; font-size: 10px; font-weight: bold; color: #363636;border-right: 1px solid #FFFFDD;border-bottom:  1px solid #FFFFDD;"><%=dto.getSurchargeAmount()+"%"%></td>
    </tr>    
<%
     }
%>
</table>
<%
          }
%>          
   
