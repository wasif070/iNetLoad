<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.sms.RatePlanDTO" %>

<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.SC && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
    int pageNo = 1;
    int sortingOrder = 0;
    int sortedItem = 0;
    int list_all = 0;
    int recordPerPage = 100;

    if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE) != null) {
        recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE).toString());
    }
    if (request.getParameter("d-49216-p") != null) {
        pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
    }
    if (request.getParameter("d-49216-s") != null) {
        sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
    }
    if (request.getParameter("d-49216-o") != null) {
        sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
    }
    if (request.getParameter("list_all") != null) {
        list_all = Integer.parseInt(request.getParameter("list_all"));
    }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }    

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j= 0;
        var selectedIDsArray = [];     
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j] = i;
                    j++;
                }
            }
        }
        document.forms[0].reset(); 
        for(t=0; t<allInputArray.length; t++)
        {
                            
            checkFlag = 0;
            for(i=0; i<selectedIDsArray.length; i++)
            {
                if(t==selectedIDsArray[i])
                {
                    checkFlag = 1;
                    break;
                }
            }
            if(checkFlag==1)
            {
                tempCheckbox = allInputArray[selectedIDsArray[i]];
                tempCheckbox.checked = true;
            }
            else
            {
                tempCheckbox = allInputArray[t];
                tempCheckbox.checked = false;
            }
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }    
        link = "../sms/listRatePlan.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../sms/listRatePlan.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editRatePlan(value1,name)
    {
        document.forms[0].reset();
        link = "country_name="+document.forms[0].country_name.value+"&country_code="+document.forms[0].country_code.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = "../sms/getRatePlan.do?id="+value1+"&name="+name+"&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Rate Plan List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>

                <div class="view-page-body">   
                    <div class="blank-height" align="right"><a href="../smsrateplan/add-rateplan.jsp"><u>Add Rate Plan</u></a></div>
                    <html:form action="/sms/listRatePlan.do" method="post">
                        <div class="full-div">
                            <div class="three-fourth-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">                                                                      
                                    <tr>
                                        <th>Country Name</th>
                                        <td>
                                            <html:text property="country_name" />
                                        </td>
                                        <th>Country Code</th>
                                        <td>
                                            <html:text property="country_code" />
                                        </td>
                                    </tr>                                        
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="full-div" style="text-align: center;">
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-30px">
                            <%
                                if (request.getSession(true).getAttribute(Constants.MESSAGE) != null) {
                                    out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                } 
                                boolean scEditPer = false;
                                if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.SC_EDIT))
                                      && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                                {
                                  scEditPer = true;
                                }    
                            %>
                        </div>
                        <c:set var="scEdit" scope="page" value="<%=scEditPer%>"/>
                        <div style="border : 1px solid #848284;" align="center">
                            <jsp:scriptlet>
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                            public String addRowClass() {
                                RatePlanDTO obj = (RatePlanDTO) getCurrentRowObject();
                                if (obj.getRate_status () == Constants.USER_STATUS_BLOCK) {
                                    return "red";
                                } 
                                else if (obj.getRate_status() == Constants.USER_STATUS_BLOCK) {
                                    return "green";
                                }
                                else {
                                    return "";
                                }
                            }
                        });
                            </jsp:scriptlet>                            
                            <div class="blank-height"></div>
                            <script type="text/javascript">var count=<%=(pageNo - 1) * recordPerPage%>;</script>
                            <display:table class="reporting_table" decorator="dyndecorator" defaultsort="7"  cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.RatePlanForm.ratePlanList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Scratch Card" />
                                <display:setProperty name="paging.banner.items_name" value="Scratch Cards" />
                                <display:column class="custom_column1" media="html" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs" value="${data.rateplan_id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" media="html" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Country Name" property="country_name" sortable="true" style="width:14%"  /> 
                                <display:column  class="custom_column1" property="country_code" title="Country Code" sortable="true" style="width:14%"  />
                                <display:column  class="custom_column2" property="rate_amount" title="Rate Amount" sortable="true" style="width:14%"  />
                                <display:column  class="custom_column1" property="statusName"  sortProperty="rate_status" title="Status" sortable="true" style="width:12%" />
                                <display:column title="Action" sortable="false" class="custom_column1" style="width:10%" media="html">
                                    <c:choose>
                                        <c:when test="${scEdit==false}">${data.country_code}</c:when>
                                        <c:otherwise><a href="#" onclick="editRatePlan('${data.rateplan_id}','${data.country_name}')">Edit</a></c:otherwise>
                                    </c:choose>
                                </display:column>
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.SCRATCH_CARD_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <%
                                request.removeAttribute(Constants.SCRATCH_CARD_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);    
                                if(scEditPer)
                                {                                
                            %>
                            <BR>
                                <input type="submit" class="action-button" value="Activate Selected" style="width: 120px;" onclick="submitform('<%=Constants.ACTIVATION%>')" /> 
                                <input type="submit" class="action-button" value="Block Selected" style="width: 115px;" onclick="submitform('<%=Constants.BLOCK%>')" /> 
                                <input type="submit" class="action-button" value="Delete Selected" style="width: 115px;" onclick="submitform('<%=Constants.DELETE%>')" />                                  
                                <div class="blank-height"></div>
                                <%
                                    }  
                                %>                                
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>