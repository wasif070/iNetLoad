<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.rechargecard.RechargecardDTO,com.myapp.struts.session.Constants" %>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Refill Request Processing</title>
    </head>
    <body>

        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div class="blank-height"></div>
                <div class="body">
                    <%
                        RechargecardDTO dto = (RechargecardDTO) request.getSession(true).getAttribute(Constants.REFILLBYCARD_SUCCESS_DTO);
                        String phone_no = dto.getPhoneNumber();
                        String amount = String.valueOf(dto.getGroupAmount());
                    %>
                    <h1>Flexiload Request Was Sent Successfully!!!</h1>
                    <p>Flexiload Phone Number: <%=phone_no%></p>
                    <p>Flexiload Amount: <%=amount%></p>
                    <p>Your Request Will Be Processed Within A Moment....</p>
                    <p>Thank You.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>