<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
                    && perDTO != null && !perDTO.RCS)
                    || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.client.ClientLoader"%>
<%@page import="java.util.ArrayList,com.myapp.struts.client.ClientDTO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 100;

   if (request.getSession(true).getAttribute(Constants.CARD_SHOP_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.CARD_SHOP_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }   

    function submitform(action)
    {   
        var j=0;
        var selectedIDsArray = [];
        var allInputArray = document.getElementsByTagName("input");
        
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" )
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j]=i;
                    j++;
                }
            }
        }
        document.forms[0].reset();
        for(i=0; i<selectedIDsArray.length; i++)
        {
            tempCheckbox = allInputArray[selectedIDsArray[i]];
            tempCheckbox.checked = true;
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        link = "../rechargecard/listCardShop.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+s;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }
        document.forms[0].action = link;
        document.forms[0].submit();
    }


    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../rechargecard/listCardShop.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editCardShop(value1,value2)
    {
        document.forms[0].reset();
        link = "shopName="+document.forms[0].shopName.value+"&shopOwner="+document.forms[0].shopOwner.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }
 
        document.forms[0].action = "../rechargecard/getCardShop.do?id="+value1+"&name="+value2+"&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Card Shop Details</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/rechargecard/listCardShop.do" method="post" styleId="rcg_form">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Shop Name</th>
                                        <td>
                                            <html:text property="shopName">
                                            </html:text>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Owner</th>
                                        <td>
                                            <html:text property="shopOwner">
                                            </html:text>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>    
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                              boolean rcsEditPer = false;
                              if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.RCS_EDIT))
                                      && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                              {
                                  rcsEditPer = true;
                              }
                            %>
                        </div>
                        <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                        <c:set var="rcsEdit" scope="page" value="<%=rcsEditPer%>"/>
                        <div style="border : 1px solid #848284;" align="center">
                            <div class="height-20px"></div>
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.CardShopForm.shopList" pagesize="<%=recordPerPage%>">
                                <display:setProperty name="paging.banner.item_name" value="Card Shop" />
                                <display:setProperty name="paging.banner.items_name" value="Card Shops" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column title="Shop Name" sortProperty="shopName" sortable="true" style="width:20%;" >   
                                    <c:choose>
                                        <c:when test="${rcsEdit==false}">${data.shopName}</c:when>
                                        <c:otherwise><a href="../rechargecard/cardShopSummery.do?list_all=1&id=${data.id}&name=${data.shopName}"><u>${data.shopName}</u></a></c:otherwise>
                                    </c:choose> 
                                </display:column>                                    
                                <display:column class="custom_column1" property="shopOwner" title="Owner" sortable="true" style="width:14%" />
                                <display:column class="custom_column1" property="shopContactNumber" title="Contact Number" sortable="true" style="width:17%" />
                                <display:column class="custom_column2" property="shopAddress" title="Address" sortable="true" style="width:16%" />
                                <display:column class="custom_column2" property="shopDescription" title="Description" sortable="true" style="width:15%" /> 
                                <display:column title="Action" sortable="false" class="custom_column1" style="width:8%" >
                                    <c:choose>
                                        <c:when test="${rcsEdit==false}">Edit</c:when>
                                        <c:otherwise><a href="#" onclick="editCardShop('${data.id}','${data.shopName}')"><u>Edit</u></a></c:otherwise>
                                    </c:choose> 
                                </display:column>                                
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.CARD_SHOP_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(",'"+tempCheckbox.value+"',")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <%
                                request.removeAttribute(Constants.CARD_SHOP_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                                if(rcsEditPer)
                                {
                            %>
                            <div class="clear height-5px"></div>
                            <input type="button" class="action-button" value="Delete" style="width: 70px;" onclick="submitConfirm('<%=Constants.DELETE%>')" />
                            <%
                                }
                            %>
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                        <div class="blank-height"></div>
                        <div class="blank-height"></div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
<div id="dialog-confirm" title="Recheck!!">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
        <div id="confirm-message" style="font-weight: normal; color:#e17009; font-size: 1.05em; text-align: left;margin-left: 50px"></div>       
    </p>
    <input type="hidden" id="jaction"/>
</div>

<script type="text/javascript" language="javascript">
    function submitConfirm(action){
        $('#jaction').val(action);
        var message="You are going to delete card shops?";
        $("#confirm-message").html(message);        
        $('#dialog-confirm').dialog('option','title','Please Recheck!!').dialog('open');
    }
    
    $(function() {
        $("#dialog-confirm").dialog({autoOpen: false, modal: true, height:250,width:350,
            buttons: {
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Ok: function() {
                    $(this).dialog( "close" );
                    var action=$('#jaction').val();                    
                    submitform(action);
                }
            },
            close: function() {
            }
        });
        // End Changed
    });
    
</script>