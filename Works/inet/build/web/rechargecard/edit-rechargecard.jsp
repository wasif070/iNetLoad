<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
            && perDTO != null && !perDTO.RCG_EDIT)
            || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
    String link = request.getQueryString();
    if (request.getParameter("searchLink") != null) {
        link = request.getParameter("searchLink");
    } else {
        link = "?" + link.substring(link.indexOf("&link=") + 6);
    }
%>

<%@page import="com.myapp.struts.rechargecard.CardShopDAO,com.myapp.struts.rechargecard.CardShopLoader,java.util.ArrayList,com.myapp.struts.rechargecard.CardShopDTO" %>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Edit Recharge Card</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/rechargecard/editRechargecard" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Edit Recharge Card: <%=request.getParameter("name")%></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="RechargecardForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Serial No</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="serialNo"  readonly="true" /><br/>
                                        <html:messages id="serialNo" property="serialNo">
                                            <bean:write name="serialNo"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card No</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="cardNo" readonly="true" /><br/>
                                        <html:messages id="cardNo" property="cardNo">
                                            <bean:write name="cardNo"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Country ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupCountryName"  readonly="true" /><br/>
                                        <html:messages id="groupCountryName" property="groupCountryName">
                                            <bean:write name="groupCountryName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Package Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupPackageName" readonly="true" /><br/>
                                        <html:messages id="groupPackageName" property="groupPackageName">
                                            <bean:write name="groupPackageName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                                                                 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card Amount</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="groupAmount" readonly="true" /><br/>
                                        <html:messages id="groupAmount" property="groupAmount">
                                            <bean:write name="groupAmount"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card Status</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="cardStatus">
                                            <html:option value="-1">Select</html:option>
                                            <%
                                                for (int i = 0; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select>
                                        <html:messages id="cardStatus" property="cardStatus">
                                            <bean:write name="cardStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card Shop</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="cardShop" >
                                            <%
                                                CardShopDAO csDAO = new CardShopDAO();
                                                ArrayList<CardShopDTO> csList = csDAO.getCardShopDTOsSortedByShopName(CardShopLoader.getInstance().getCardShopDTOList());
                                                if (csList != null) {
                                                    int size = csList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        CardShopDTO csDTO = csList.get(i);
                                            %>
                                            <html:option  value="<%=String.valueOf(csDTO.getId())%>" ><%=csDTO.getShopName()%></html:option>
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>                                         
                                        <html:messages id="cardShop" property="cardShop">
                                            <bean:write name="cardShop"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card Price</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="cardPrice" /><br/>
                                        <html:messages id="cardPrice" property="cardPrice">
                                            <bean:write name="cardPrice"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Activated By</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="activatedBy" readonly="true" /><br/>
                                        <html:messages id="activatedBy" property="activatedBy">
                                            <bean:write name="activatedBy"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Activated At</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="activatedAtStr" readonly="true" /><br/>
                                        <html:messages id="activatedAtStr" property="activatedAtStr">
                                            <bean:write name="activatedAtStr"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>    
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Blocked By</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="blockedBy" readonly="true" /><br/>
                                        <html:messages id="blockedBy" property="blockedBy">
                                            <bean:write name="blockedBy"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>   
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">blocked At</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="blockedAtStr" readonly="true" /><br/>
                                        <html:messages id="blockedAtStr" property="blockedAtStr">
                                            <bean:write name="blockedAtStr"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <html:hidden property="id" />
                                        <%
                                            if (request.getParameter("groupName") != null) {
                                        %>                                        
                                        <html:hidden property="groupName" />
                                        <html:hidden property="groupIdentifier" />
                                        <%                                }
                                        %>                                                  
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Edit" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                        <input type="hidden" name="searchLink" value="<%=link%>" />
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>