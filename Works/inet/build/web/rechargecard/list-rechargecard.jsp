<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if ((login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
                    && perDTO != null && !perDTO.RCG)
                    || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,com.myapp.struts.rechargecard.RechargecardDTO" %>
<%@page import="com.myapp.struts.rechargecard.CardShopDAO,com.myapp.struts.rechargecard.CardShopLoader,java.util.ArrayList,com.myapp.struts.rechargecard.CardShopDTO" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 1000;

   if (request.getSession(true).getAttribute(Constants.RC_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.RC_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }
 
    function isEmpty(text)
    {
        for(var i=0;i<text.length;i++)
            if(text.charAt(i)!=' ')break;
     
        if(i==text.length)
            return true;
        else
            return false;      
    }
    
    function validateInteger(text)
    {
        var charpos = text.search("[^0-9]"); 
        if( eval(text.length) > 0 &&  charpos >= 0 ) 
        { 
            return false; 
        }

        return true;
    }

    function validateDecimal(text) 
    {
        if(!text.match("^([0-9]*)(.[0-9]+)?$")) 
        {
            return false;
        }    
        return true;
    }      

    function submitform(action,cardShop,cardPrice,startingSerial,totalCards)
    {   
        var j=0;
        var selectedIDsArray = [];
        var allInputArray = document.getElementsByTagName("input");
        
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" )
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j]=i;
                    j++;
                }
            }
        }
        document.forms[0].reset();
        for(i=0; i<selectedIDsArray.length; i++)
        {
            tempCheckbox = allInputArray[selectedIDsArray[i]];
            tempCheckbox.checked = true;
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        link = "../rechargecard/listRechargecard.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>+"&cardShop="+cardShop+"&cardPrice="+cardPrice+"&startingSerial="+startingSerial+"&totalCards="+totalCards;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+s;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
        document.forms[0].submit();
    }


    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../rechargecard/listRechargecard.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editRechargecard(value1,value2)
    {
        document.forms[0].reset();
        if(document.forms[0].groupPackage!=undefined)
        {    
            link = "cardNo="+document.forms[0].cardNo.value+"&serialNo="+document.forms[0].serialNo.value+"&cardStatus="+document.forms[0].cardStatus.value+"&groupPackage="+document.forms[0].groupPackage.value+"&sign="+document.forms[0].sign.value+"&groupAmount="+document.forms[0].groupAmount.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        }
        else
        {
            link = "cardNo="+document.forms[0].cardNo.value+"&serialNo="+document.forms[0].serialNo.value+"&cardStatus="+document.forms[0].cardStatus.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;        
        }    
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }    
        document.forms[0].action = "../rechargecard/getRechargecard.do?id="+value1+"&name="+value2+"&link="+link;       
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Recharge Card List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body1">
                    <html:form action="/rechargecard/listRechargecard.do" method="post" styleId="rc_form">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Serial No</th>
                                        <td>
                                            <html:text property="serialNo" />
                                        </td>  
                                    </tr>
                                    <tr>
                                        <th>Card No</th>
                                        <td>
                                            <html:text property="cardNo" />
                                        </td>
                                    </tr>  
                                    <%
                                       if(request.getParameter("groupName") == null)
                                       {
                                    %>                                  
                                    <tr>
                                        <th>Package Type</th>
                                        <td>
                                            <html:select property="groupPackage">
                                                <html:option value="-1">Select</html:option>
                                                <%
                                                 for(int i=0;i<Constants.PACKAGE_ID_VALUE.length;i++)
                                                 {
                                                %>
                                                <html:option value="<%=Constants.PACKAGE_ID_VALUE[i]%>"><%=Constants.PACKAGE_ID_STRING[i]%></html:option>
                                                <%
                                                 }
                                                %>
                                            </html:select>
                                        </td>
                                    </tr> 
                                    <%
                                       }
                                    %>                                     
                                    <tr>
                                        <th>Card Status</th>
                                        <td>
                                            <html:select property="cardStatus">
                                                <html:option value="-1">Select</html:option>
                                                <%
                                                 for(int i=0;i<Constants.LIVE_STATUS_VALUE.length;i++)
                                                 {
                                                %>
                                                <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                                <%
                                                 }
                                                %>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Distributed To</th>
                                        <td>
                                            <html:text property="cardShopName" />
                                        </td>
                                    </tr>                                    
                                    <%
                                       if(request.getParameter("groupName") == null)
                                       {
                                    %>            
                                    <tr>
                                        <th>Card Amount</th>
                                        <td>
                                            <html:select property="sign" style="width:47px">
                                                <html:option value="0">All</html:option>
                                                <html:option value="1">=</html:option>
                                                <html:option value="2"><</html:option>
                                                <html:option value="3">></html:option>
                                            </html:select>
                                            <html:text property="groupAmount" style="width:100px" />
                                        </td>                      
                                    </tr>
                                    <%
                                       }
                                    %>                                       
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td> 
                                    </tr>
                                    <tr>    
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <%
                           if(request.getParameter("groupName") != null)
                           {
                        %>           
                        <div class="clear height-20px" style="color: #e17009; font-weight: bold">Recharge Card Group: <%=request.getParameter("groupName")%></div>
                        <%
                           }                      
                        %>                                               
                        <div class="clear height-20px">
                            <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                              }
                              boolean rcgEditPer = false;
                              if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.RCG_EDIT))
                                      && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                              {
                                  rcgEditPer = true;
                              }
                            %>
                        </div>

                        <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                        <c:set var="rcgEdit" scope="page" value="<%=rcgEditPer%>"/>
                        <div class="full-div" align="center">
                            <jsp:scriptlet>
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                            public String addRowClass() {
                                RechargecardDTO obj = (RechargecardDTO) getCurrentRowObject();
                                if (obj.getCardStatus() == Constants.USER_STATUS_BLOCK) {
                                    return "red";
                                }
                                else if (obj.getCardStatus() == Constants.USER_STATUS_INACTIVE) {  
                                   return "blue";
                                }                      
                                else {
                                    return "";
                                }
                            }
                        });
                            </jsp:scriptlet>                             
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <%
                               if(request.getParameter("groupName") != null)
                               {    
                            %>
                            <html:hidden property="groupName" />
                            <html:hidden property="groupIdentifier" />                              
                            <display:table class="reporting_table" decorator="dyndecorator" defaultsort="2" defaultorder="ascending" cellpadding="0" cellspacing="0" id="data" name="sessionScope.RechargecardForm.cardList" pagesize="<%=recordPerPage%>" export="true">
                                <display:setProperty name="paging.banner.item_name" value="Recharge Card" />
                                <display:setProperty name="paging.banner.items_name" value="Recharge Cards" /> 
                                <display:column class="custom_column2" title="Nr" style="width:5%;" media="html">
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column title="Serial No" sortProperty="serialNo" sortable="true" style="width:16%;" media="html">
                                    <c:choose>
                                        <c:when test="${rcgEdit==false}">${data.serialNo}</c:when>
                                        <c:otherwise><a href="#" onclick="editRechargecard('${data.id}','${data.serialNo}')"><u>${data.serialNo}</u></a></c:otherwise>
                                    </c:choose>                                      
                                </display:column>   
                                <display:column title="Serial No" property="serialNo" media="csv excel pdf" />        
                                <display:column class="custom_column1" title="Card No" property="cardNo" sortable="true" style="width:16%;" />                                                    
                                <display:column class="custom_column1" property="groupPackageName" title="Package Type" sortable="true" style="width:13%" />
                                <display:column class="custom_column2" property="groupAmount" title="Card Amount" format="{0,number,0.00}" sortable="true" style="width:10%" />
                                <display:column class="custom_column1" property="cardStatusName" sortProperty="cardStatus" title="Status" sortable="true" style="width:10%" />  
                                <display:column property="cardShopName" title="Distributed To" sortable="true" style="width:15%"  /> 
                                <display:column class="custom_column2" property="cardPrice" title="Dis. Price" sortable="true" style="width:15%"  />

                            </display:table>
                            <%
                               }
                               else
                               {    
                            %> 
                            <display:table class="reporting_table" decorator="dyndecorator" defaultsort="3" defaultorder="ascending" cellpadding="0" cellspacing="0" id="data" name="sessionScope.RechargecardForm.cardList" pagesize="<%=recordPerPage%>" export="false">
                                <display:setProperty name="paging.banner.item_name" value="Recharge Card" />
                                <display:setProperty name="paging.banner.items_name" value="Recharge Cards" /> 
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:7%" >
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column title="Serial No" sortProperty="serialNo" sortable="true" style="width:16%;" >
                                    <a href="#" onclick="editRechargecard('${data.id}','${data.serialNo}')"><u>${data.serialNo}</u></a>
                                </display:column>               
                                <display:column class="custom_column1" title="Card No" property="cardNo" sortable="true" style="width:16%;" />                                                    
                                <display:column class="custom_column1" property="groupPackageName" title="Package Type" sortable="true" style="width:13%" />
                                <display:column class="custom_column2" property="groupAmount" title="Card Amount" format="{0,number,0.00}" sortable="true" style="width:10%" />
                                <display:column class="custom_column1" property="cardStatusName" sortProperty="cardStatus" title="Status" sortable="true" style="width:10%" />  
                                <display:column property="cardShopName" title="Distributed To" sortable="true" style="width:15%"  /> 
                                <display:column class="custom_column2" property="cardPrice" title="Dis. Price" sortable="true" style="width:15%"  />

                            </display:table>                            

                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.RC_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <%
                                request.removeAttribute(Constants.RC_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                                if(rcgEditPer)
                                {
                            %>
                            <div class="clear height-5px"></div>
                            <input type="button" class="action-button" value="Distribute" style="width: 80px;" onclick="submitConfirm('<%=Constants.ACTIVATION%>')" />
                            <input type="button" class="action-button" value="Return" style="width: 70px;" onclick="submitConfirm('<%=Constants.DEACTIVATION%>')" />
                            <input type="button" class="action-button" value="Block" style="width: 70px;" onclick="submitConfirm('<%=Constants.BLOCK%>')" />
                            <input type="button" class="action-button" value="Delete" style="width: 70px;" onclick="submitConfirm('<%=Constants.DELETE%>')" />
                            <%
                                }
                              }  
                            %>
                            <div class="blank-height"></div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
<div id="dialog-confirm" title="Recheck!!">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
        <div id="confirm-message" style="font-weight: normal; color:#e17009; font-size: 1.05em; text-align: left;margin-left: 50px"></div>
        <table align="center" border="0" cellspacing="0" cellpadding="0" >            
            <tr>
                <td align="right">
                    <label for="cardShop">Card Shop</label>
                </td>
                <td>
                    <select class="jcardShop"  name="cardShop" >
                        <%
                              CardShopDAO csDAO =  new CardShopDAO();
                              ArrayList<CardShopDTO> csList = csDAO.getCardShopDTOsSortedByShopName(CardShopLoader.getInstance().getCardShopDTOList());
                              if (csList != null) {
                                  int size = csList.size();
                                  for (int i = 0; i < size; i++) {
                                      CardShopDTO csDTO = csList.get(i);
                        %>
                        <option  value="<%=String.valueOf(csDTO.getId())%>" ><%=csDTO.getShopName()%></option>
                        <%
                                                }
                                            }
                        %>
                    </select>                     
                </td>
            </tr>            
            <tr>
                <td align="right">
                    <label for="cardPrice">Card Price</label>                
                </td>
                <td>
                    <input type="text" class="jcardPrice" name="cardPrice"  />   
                </td>
            </tr>
            <tr>
                <td  align="right">
                    <label for="startingSerial">Starting Serial</label>                 
                </td>
                <td>
                    <input type="text" class="jstartingSerial" name="startingSerial"  />   
                </td>
            </tr> 
            <tr>
                <td  align="right">
                    <label for="totalCards">Total Cards</label>                
                </td>
                <td>
                    <input type="text" class="jtotalCards" name="totalCards"  />   
                </td>
            </tr>                     
        </table>        
    </p>
    <input type="hidden" id="jaction"/>
    <input type="hidden" id="jfound"/>
</div>

<script type="text/javascript" language="javascript">
    function submitConfirm(action){       
        var found = 0;
        var message="You are going to";
        if(action==<%=Constants.ACTIVATION%>)
        {
            message +=" distribute Recharge Cards?"; 
            $("input[name='cardPrice']").attr('value', ''); 
            $("input[name='startingSerial']").attr('value', ''); 
            $("input[name='totalCards']").attr('value', ''); 
            var inputArray = document.getElementsByTagName("input");
            
            for(i=0; i<inputArray.length; i++)
            {
                if(inputArray[i].name == "selectedIDs" )
                {
                    tempCheckbox = inputArray[i];
                    if(tempCheckbox.checked)
                    {
                        found = 1;
                        break;
                    }
                }
            }  
            if(found==0) 
            {
                $("input[name='startingSerial']").attr('style', 'visibility: visible; width: 100px');
                $("label[for='startingSerial']").attr('style', 'visibility: visible; font-weight: bold; color: #0c66ca;width: 200px');
                $("input[name='totalCards']").attr('style', 'visibility: visible; width: 100px');
                $("label[for='totalCards']").attr('style', 'visibility: visible; font-weight: bold; color: #0c66ca; width: 200px');                 
            } 
            else
            {
                $("input[name='startingSerial']").attr('style', 'visibility: hidden');
                $("label[for='startingSerial']").attr('style', 'visibility: hidden');  
                $("input[name='totalCards']").attr('style', 'visibility: hidden');
                $("label[for='totalCards']").attr('style', 'visibility: hidden');                
            }    
            $("input[name='cardPrice']").attr('style', 'visibility: visible; width: 100px');
            $("label[for='cardPrice']").attr('style', 'visibility: visible; font-weight: bold; color: #0c66ca');              
            $("select[name='cardShop']").attr('style', 'visibility: visible; width: 105px');
            $("label[for='cardShop']").attr('style', 'visibility: visible; font-weight: bold; color: #0c66ca' );  
        }
        else if(action==<%=Constants.DEACTIVATION%>)
        {
            message +=" return Recharge Cards?";
            $("input[name='startingSerial']").attr('style', 'visibility: hidden');
            $("label[for='startingSerial']").attr('style', 'visibility: hidden');  
            $("input[name='totalCards']").attr('style', 'visibility: hidden');
            $("label[for='totalCards']").attr('style', 'visibility: hidden');              
            $("input[name='cardPrice']").attr('style', 'visibility: hidden');
            $("label[for='cardPrice']").attr('style', 'visibility: hidden');              
            $("select[name='cardShop']").attr('style', 'visibility: hidden');
            $("label[for='cardShop']").attr('style', 'visibility: hidden');             
        }        
        else if(action==<%=Constants.BLOCK%>)
        {
            message +=" block Recharge Cards?";
            $("input[name='startingSerial']").attr('style', 'visibility: hidden');
            $("label[for='startingSerial']").attr('style', 'visibility: hidden');  
            $("input[name='totalCards']").attr('style', 'visibility: hidden');
            $("label[for='totalCards']").attr('style', 'visibility: hidden');             
            $("input[name='cardPrice']").attr('style', 'visibility: hidden');
            $("label[for='cardPrice']").attr('style', 'visibility: hidden');              
            $("select[name='cardShop']").attr('style', 'visibility: hidden');
            $("label[for='cardShop']").attr('style', 'visibility: hidden');             
        }
        else if(action==<%=Constants.DELETE%>)
        {
            message +=" delete Recharge Cards?";
            $("input[name='startingSerial']").attr('style', 'visibility: hidden');
            $("label[for='startingSerial']").attr('style', 'visibility: hidden');  
            $("input[name='totalCards']").attr('style', 'visibility: hidden');
            $("label[for='totalCards']").attr('style', 'visibility: hidden');             
            $("input[name='cardPrice']").attr('style', 'visibility: hidden');
            $("label[for='cardPrice']").attr('style', 'visibility: hidden');             
            $("select[name='cardShop']").attr('style', 'visibility: hidden');
            $("label[for='cardShop']").attr('style', 'visibility: hidden');              
        }
        $('#jaction').val(action);
        $('#jfound').val(found);
        $("#confirm-message").html(message);        
        $('#dialog-confirm').dialog('option','title','Please Recheck!!').dialog('open');
    }
    
    $(function() {
        $("#dialog-confirm").dialog({autoOpen: false, modal: true, height:300,width:350,
            buttons: {
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Ok: function() {
                    $(this).dialog( "close" );
                    var cardShop=$("select[name='cardShop']").val();
                    var cardPrice=$("input:text[name='cardPrice']").val();
                    var startingSerial=$("input:text[name='startingSerial']").val();
                    var totalCards=$("input:text[name='totalCards']").val();
                    var action=$('#jaction').val();
                    var found=$('#jfound').val();
                    if(action==<%=Constants.ACTIVATION%> )
                    {
                        if(isEmpty(cardPrice) || !validateDecimal(cardPrice))
                        {
                            alert("Please enter valid card price!!!");
                            return;
                        }  
                        if(found==0)
                        {    
                            if(isEmpty(startingSerial) || !validateInteger(startingSerial))
                            {
                                alert("Please enter valid starting serial number!!!");
                                return;                        
                            }
                            if(isEmpty(totalCards) || !validateInteger(totalCards))
                            {
                                alert("Please enter valid total cards!!!");
                                return;                        
                            }                            
                        }
                    }      
                    submitform(action,cardShop,cardPrice,startingSerial,totalCards);
                }
            },
            close: function() {
            }
        });
        // End Changed
    });
    
</script>