<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
            if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                response.sendRedirect("../index.do");
                return;
            }
            String link = request.getQueryString();

            if (request.getParameter("searchLink") != null) {
                link = request.getParameter("searchLink");
            } else {
                link = "?" + link.substring(link.indexOf("&link=") + 6);
            }
%>
<%@page import="com.myapp.struts.session.PermissionConstants,com.myapp.struts.user.PermissionDTO,com.myapp.struts.user.UserLoader" %>

<script language="javascript" type="text/javascript">

    function checkMainBox(mainBoxName)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].id == mainBoxName)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked == false)
                {
                    tempCheckbox.checked = true;
                }
                break;
            }
        }
    }

    function unCheckParBox(parBoxName,iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        if(iobj.checked == false)
        {
            for(i=0; i<allInputArray.length; i++)
            {
                if(allInputArray[i].id == parBoxName)
                {
                    tempCheckbox = allInputArray[i];
                    if(tempCheckbox.checked == true)
                    {
                        tempCheckbox.checked = false;
                    }
                }
            }
        }
    }
</script>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Edit Role</title>
    </head>
    <%
                PermissionDTO permissionDTO = UserLoader.getInstance().getRoleDTOByID(Long.parseLong(request.getParameter("id"))).getPermissionDTO();
    %>
    <body style="height: 100%">
        <div class="main_body">            
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/role/editRole" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Edit Role: <font color="#e17009"><%=request.getParameter("name")%></font></span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 10px;" valign="bottom">
                                        <bean:write name="RoleForm" property="message" filter="false"/>
                                        <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Role Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="roleName" /><br/>
                                        <html:messages id="roleName" property="roleName">
                                            <bean:write name="roleName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Description</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="roleDescription" style="width:100;" /><br/>
                                        <html:messages id="roleDescription" property="roleDescription">
                                            <bean:write name="roleDescription"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Permissions:</th>
                                    <td valign="top" style="padding-top: 6px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    String checked = " ";
                                                    if (permissionDTO != null && permissionDTO.CLIENT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="client" name="permissionIDs"  <%=checked%> value="<%=PermissionConstants.CLIENT%>" style="width: 25px;" onchange="unCheckParBox('clientPer',this)" />Client
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.CLIENT_ADD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="clientPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.CLIENT_ADD%>" align="right" style="width: 20px" onchange="checkMainBox('client')"/>Add
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.CLIENT_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="clientPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.CLIENT_EDIT%>" align="right" style="width: 20px" onchange="checkMainBox('client')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.CLIENT_RECHARGE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="clientPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.CLIENT_RECHARGE%>" align="right" style="width: 20px" onchange="checkMainBox('client')"/>Recharge
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.CLIENT_RETURN) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="clientPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.CLIENT_RETURN%>" align="right" style="width: 20px" onchange="checkMainBox('client')"/>Return
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.CLIENT_RECEIVE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="clientPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.CLIENT_RECEIVE%>" align="right" style="width: 20px" onchange="checkMainBox('client')"/>Receive
                                    </td>
                                </tr>                                    
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCG) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcg" name="permissionIDs"  <%=checked%> value="<%=PermissionConstants.RCG%>" style="width: 25px;" onchange="unCheckParBox('rcgPer',this)" />Recharge Card
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCG_ADD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcgPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.RCG_ADD%>" align="right" style="width: 20px" onchange="checkMainBox('rcg')"/>Add
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCG_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcgPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.RCG_EDIT%>" align="right" style="width: 20px" onchange="checkMainBox('rcg')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCG_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcgPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.RCG_DELETE%>" align="right" style="width: 20px" onchange="checkMainBox('rcg')"/>Delete                                        
                                    </td>
                                </tr>   
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCS) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcs" name="permissionIDs"  <%=checked%> value="<%=PermissionConstants.RCS%>" style="width: 25px;" onchange="unCheckParBox('rcsPer',this)" />Recharge Card Shop
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCS_ADD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcsPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.RCS_ADD%>" align="right" style="width: 20px" onchange="checkMainBox('rcs')"/>Add
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCS_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcsPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.RCS_EDIT%>" align="right" style="width: 20px" onchange="checkMainBox('rcs')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.RCS_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="rcsPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.RCS_DELETE%>" align="right" style="width: 20px" onchange="checkMainBox('rcs')"/>Delete                                        
                                    </td>
                                </tr>
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SC) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="sc" name="permissionIDs"  <%=checked%> value="<%=PermissionConstants.SC%>" style="width: 25px;" onchange="unCheckParBox('scPer',this)" />Scratch Card
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SC_ADD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="scPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SC_ADD%>" align="right" style="width: 20px" onchange="checkMainBox('sc')"/>Add
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SC_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="scPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SC_EDIT%>" align="right" style="width: 20px" onchange="checkMainBox('sc')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SC_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="scPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SC_DELETE%>" align="right" style="width: 20px" onchange="checkMainBox('sc')"/>Delete                                        
                                    </td>
                                </tr>                                      

                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_SIM_SETTINGS) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiSIM" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_SIM_SETTINGS%>" style="width: 25px;" onchange="unCheckParBox('flexiSIMPer',this)"/>Flexi SIM Settings
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_SIM_SETTINGS_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiSIMPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_SIM_SETTINGS_EDIT%>" align="right" style="width: 30px" onchange="checkMainBox('flexiSIM')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_SIM_SETTINGS_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiSIMPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_SIM_SETTINGS_DELETE%>" align="right" style="width: 30px" onchange="checkMainBox('flexiSIM')"/>Delete
                                    </td>
                                </tr>
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_SIM_RECHARGE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiSIMRecharge" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_SIM_RECHARGE%>" style="width: 25px;" onchange="unCheckParBox('flexiSIMRechargePer',this)"/>Flexi SIM Recharge
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_SIM_RECHARGE_ADD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiSIMRechargePer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_SIM_RECHARGE_ADD%>" align="right" style="width: 30px" onchange="checkMainBox('flexiSIMRecharge')"/>Add                                        
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_SIM_RECHARGE_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiSIMRechargePer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_SIM_RECHARGE_EDIT%>" align="right" style="width: 30px" onchange="checkMainBox('flexiSIMRecharge')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_SIM_RECHARGE_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiSIMRechargePer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_SIM_RECHARGE_DELETE%>" align="right" style="width: 30px" onchange="checkMainBox('flexiSIMRecharge')"/>Delete
                                    </td>
                                </tr>
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_MESSAGE_LOG) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiMessageLog" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_MESSAGE_LOG%>" style="width: 25px;" onchange="unCheckParBox('flexiMessageLogPer',this)"/>Flexi SIM Message Log
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FLEXI_MESSAGE_LOG_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="flexiMessageLogPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FLEXI_MESSAGE_LOG_DELETE%>" align="right" style="width: 30px" onchange="checkMainBox('flexiMessageLog')"/>Delete
                                    </td>
                                </tr>
                    
                                 <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="fd" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FD%>" style="width: 25px;" onchange="unCheckParBox('fdPer',this)"/>Flexi Dealer
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FD_ADD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="fdPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FD_ADD%>" align="right" style="width: 30px" onchange="checkMainBox('fd')"/>Add                                        
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FD_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="fdPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FD_EDIT%>" align="right" style="width: 30px" onchange="checkMainBox('fd')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FD_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="fdPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FD_DELETE%>" align="right" style="width: 30px" onchange="checkMainBox('fd')"/>Delete
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FD_PR) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="fdPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FD_PR%>" align="right" style="width: 30px" onchange="checkMainBox('fd')"/>Payment/Receive                                        
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FD_TH) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="fdPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FD_TH%>" align="right" style="width: 30px" onchange="checkMainBox('fd')"/>Transaction History                                         
                                    </td>
                                </tr>
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SMS_SETTINGS) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="smsSIM" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SMS_SETTINGS%>" style="width: 25px;" onchange="unCheckParBox('smsSIMPer',this)"/>SMS Gateways
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SMS_SETTINGS_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="smsSIMPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SMS_SETTINGS_EDIT%>" align="right" style="width: 30px" onchange="checkMainBox('smsSIM')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SMS_SETTINGS_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="smsSIMPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SMS_SETTINGS_DELETE%>" align="right" style="width: 30px" onchange="checkMainBox('smsSIM')"/>Delete
                                    </td>
                                </tr> 
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SMS_LOG) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="smsLog" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SMS_LOG%>" style="width: 25px;" onchange="unCheckParBox('smsLogPer',this)"/>SMS Log
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.SMS_LOG_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="smsLogPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.SMS_LOG_DELETE%>" align="right" style="width: 30px" onchange="checkMainBox('smsLog')"/>Delete
                                    </td>
                                </tr>    
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FA) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="fa" name="permissionIDs"  <%=checked%> value="<%=PermissionConstants.FA%>" style="width: 25px;" onchange="unCheckParBox('faPer',this)" />Flexi API
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FA_ADD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="faPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FA_ADD%>" align="right" style="width: 20px" onchange="checkMainBox('fa')"/>Add
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FA_EDIT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="faPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FA_EDIT%>" align="right" style="width: 20px" onchange="checkMainBox('fa')"/>Edit
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.FA_DELETE) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="faPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.FA_DELETE%>" align="right" style="width: 20px" onchange="checkMainBox('fa')"/>Delete                                        
                                    </td>
                                </tr>                                       
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refill" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL%>" style="width: 25px;" onchange="unCheckParBox('refillPer',this)"/>Refill Operation
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_GP) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_GP%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>GP
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_BL) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_BL%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>BL
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_RB) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_RB%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>RB
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_WR) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_WR%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>WR
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_TT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_TT%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>TT
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_CC) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_CC%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>CC
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_CARD) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_CARD%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>CARD
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REFILL_BT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="refillPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REFILL_BT%>" align="right" style="width: 25px" onchange="checkMainBox('refill')"/>Balance Transfer                                        
                                    </td>
                                </tr>
                                    
                                <tr>
                                    <th class="customth" valign="top" style="padding-top: 8px;" colspan="2">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REPORT) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="report" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REPORT%>" style="width: 25px;" onchange="unCheckParBox('reportPer',this)"/>Reports
                                    </th>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">&nbsp;</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REPORT_REFILL_SUMMERY) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="reportPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REPORT_REFILL_SUMMERY%>" align="right" style="width: 20px" onchange="checkMainBox('report')"/>Refill Summery<br/>
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REPORT_PAYMENT_SUMMERY) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="reportPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REPORT_PAYMENT_SUMMERY%>" align="right" style="width: 20px" onchange="checkMainBox('report')"/>Payment Summary<br/>
                                        <%
                                                    checked = " ";
                                                    if (permissionDTO != null && permissionDTO.REPORT_PAYMENT_MADE_HISTORY) {
                                                        checked = "checked";
                                                    }
                                        %>
                                        <input type="checkbox" id="reportPer" name="permissionIDs" <%=checked%> value="<%=PermissionConstants.REPORT_PAYMENT_MADE_HISTORY%>" align="right" style="width: 20px" onchange="checkMainBox('report')"/>Payment Made History<br/>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <html:hidden property="id" />
                                        <input type="hidden" name="searchLink" value="<%=link%>" />
                                        <input type="hidden" name="link" value="<%=link%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Update" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>
    </body>
</html>