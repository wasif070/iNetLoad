<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.REPORT_PAYMENT_MADE_HISTORY && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = SettingsLoader.getInstance().getSettingsDTO().getRecordPerPageForReporting();
   long id = -1;
   String name = " ";
   String type = " ";
   String des = " ";

   if (request.getSession(true).getAttribute(Constants.PAYMENT_CLIENT_SUMMERY_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.PAYMENT_CLIENT_SUMMERY_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   if (request.getParameter("id") != null) {
     id = Long.parseLong(request.getParameter("id"));
   }
   if (request.getParameter("name") != null) {
     name = request.getParameter("name");
   }
   if (request.getParameter("type") != null) {
     type = request.getParameter("type");
   }
   des = "Client: "+name+"("+type+")";
%>
<script language="javascript" type="text/javascript">
    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../payment/clientPaymentSummery.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value+"&id=<%=id%>&name=<%=name%>&type=<%=type%>";
    }

</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Client's Payment Summery</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <html:form action="/payment/clientPaymentSummery.do" method="post">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Start Time</th>
                                        <td>
                                            <html:select property="startDay" styleClass="width-50px" styleId="startDay">
                                                <%
                                                            ArrayList<Integer> days = Utils.getDay();
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=increment%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="startMonth" styleClass="width-50px" styleId="startMonth">
                                                <%
                                                            ArrayList<String> months = Utils.getMonth();
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=month%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="startYear" styleClass="width-60px" styleId="startYear">
                                                <%
                                                            ArrayList<Integer> years = Utils.getYear();
                                                            for (int i = 0; i < years.size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                %>
                                                <html:option value="<%=year%>"><%=year%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>End Time</th>
                                        <td>
                                            <html:select property="endDay" styleClass="width-50px" styleId="endDay">
                                                <%
                                                            ArrayList<Integer> days1 = Utils.getDay();
                                                            for (int i = 0; i < days1.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=increment%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="endMonth" styleClass="width-50px" styleId="endMonth">
                                                <%
                                                            ArrayList<String> months1 = Utils.getMonth();
                                                            for (int i = 0; i < months1.size(); i++) {
                                                                String month = months1.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=month%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="endYear" styleClass="width-60px" styleId="endYear">
                                                <%
                                                            ArrayList<Integer> years1 = Utils.getYear();
                                                            for (int i = 0; i < years1.size(); i++) {
                                                                String year = String.valueOf(years1.get(i));
                                                %>
                                                <html:option value="<%=year%>"><%=year%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Payment Type</th>
                                        <td>
                                            <html:select property="paymentTypeId">
                                                <html:option value="-1" >Select</html:option>
                                                <html:option value="<%=String.valueOf(Constants.PAYMENT_TYPE_PAID)%>" ><%=Constants.PAYMENT_TYPE[Constants.PAYMENT_TYPE_PAID]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.PAYMENT_TYPE_RECEIVED)%>" ><%=Constants.PAYMENT_TYPE[Constants.PAYMENT_TYPE_RECEIVED]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.PAYMENT_TYPE_RETURNED)%>" ><%=Constants.PAYMENT_TYPE[Constants.PAYMENT_TYPE_RETURNED]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.PAYMENT_TYPE_CANCELLED)%>" ><%=Constants.PAYMENT_TYPE[Constants.PAYMENT_TYPE_CANCELLED]%></html:option>
                                                <html:option value="<%=String.valueOf(Constants.PAYMENT_TYPE_INITIAL)%>" ><%=Constants.PAYMENT_TYPE[Constants.PAYMENT_TYPE_INITIAL]%></html:option>
                                            </html:select>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>    
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                                <div class="clear height-5px"></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </html:form>
                    <div class="clear height-10px">
                        <%
                          if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                          {
                            out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                          }
                        %>
                    </div>
                    <div class="height-20px"><b><%=des%></b></div>
                    <div class="full-div">
                        <script type="text/javascript">
                            var total_paid=0;
                            var total_received=0;
                            var total_discount=0;
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>
                        <display:table class="reporting_table" style="width:100%;" defaultsort="5" defaultorder="descending" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.PaymentForm.clientPaymentSummery"  pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Entry" />
                            <display:setProperty name="paging.banner.items_name" value="Entries" />
                            <display:column class="custom_column2" title="Nr" style="width:7%;" media="html" >
                                <script type="text/javascript">
                                        if(${data.paymentTypeId}==1 || ${data.paymentTypeId}==5 )
                                        {
                                            total_paid=total_paid+${data.paymentAmount};
                                            total_discount=total_discount+${data.paymentDiscount};
                                        }
                                        else if(${data.paymentTypeId}==3)
                                        {
                                            total_paid=total_paid-${data.paymentAmount};
                                        }
                                        else if(${data.paymentTypeId}==2)
                                        {
                                            total_received=total_received+${data.paymentAmount};
                                        }
                                        else if(${data.paymentTypeId}==4)
                                        {
                                            total_received=total_received-${data.paymentAmount};
                                        }
                                        document.write(++count+".");
                                </script>
                            </display:column>
                            <display:column  property="paymentTypeName" title="Payment Type" sortable="true" style="width:16%" />
                            <display:column  class="custom_column2" property="paymentAmount" title="Amount" sortable="true" format="{0,number,0.0000}" style="width:18%"  />
                            <display:column  class="custom_column2" property="paymentDiscount" title="Discount" sortable="true" format="{0,number,0.0000}" style="width:14%"  />
                            <display:column  property="paymentTime" title="Date" sortable="true" class="custom_column1" decorator="implement.displaytag.LongDateWrapper" style="width:20%" />
                            <display:column  property="paymentDescription" title="Description" sortable="true"  style="width:25%" />
                        </display:table>
                        <div class="clear"></div>
                        <div class="half-div">
                            <table class="reporting_table" cellspacing="0" border="0" cellpadding="0" style="width:100%;">
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 0;" align="right" >Total Paid Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 0;" align="right" width="35%">
                                        <script type="text/javascript">
                                            var paid = parseInt(total_paid+"");
                                            document.write(paid+".00 Tk.");
                                        </script>
                                    </td>
                                    <td style="color: #0c66ca; font-weight: bold;" align="right" width="15%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 0;" align="right" >Total Received Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 0;" align="right" width="35%">
                                        <script type="text/javascript">
                                            var received = parseInt(total_received+"");
                                            document.write(received+".00 Tk.");
                                        </script>
                                    </td>
                                    <td style="color: #0c66ca; font-weight: bold;" align="right" width="15%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 0;" align="right" >Total Due Amount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 0;" align="right" width="35%">
                                        <script type="text/javascript">
                                            document.write((paid-received)+".00 Tk.");
                                        </script>
                                    </td>
                                    <td style="color: #0c66ca; font-weight: bold;" align="right" width="15%">&nbsp;</td>
                                </tr>
                                    <td style="color: #0c66ca; font-weight: bold;padding-right: 5px;border-width: 0;" align="right" >Total Discount:</td>
                                    <td style="color: #0000ff; font-weight: bold;border-width: 0;" align="right" width="35%">
                                        <script type="text/javascript">
                                            var discount = parseInt(total_discount+"");
                                            document.write(discount+".00 Tk.");
                                        </script>
                                    </td>
                                    <td style="color: #0c66ca; font-weight: bold;" align="right" width="15%">&nbsp;</td>
                                </tr>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>