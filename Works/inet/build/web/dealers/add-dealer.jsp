<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.FD_ADD && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
%>

<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,java.util.ArrayList" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Dealer</title>
    </head>
    <body>
        <body style="height: 100%">
            <div class="main_body">
                <div><%@include file="../includes/header.jsp"%></div>
                <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
                <div class="body_content fl_left">
                    <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                    <div class="body">
                        <html:form action="/dealers/addDealer" method="post">                        
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <thead>
                                    <tr><th colspan="2"><h3>Add New Dealer</h3></th></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                            <bean:write name="DealerForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" style="padding-top: 8px;">Dealer Name</th>
                                        <td valign="top" style="padding-top: 6px;">
                                            <html:text property="dealerName" /><br/>
                                            <html:messages id="dealerName" property="dealerName">
                                                <bean:write name="dealerName"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" style="padding-top: 8px;">Address</th>
                                        <td valign="top" style="padding-top: 6px;">
                                            <html:text property="dealerAddress" /><br/>
                                            <html:messages id="dealerAddress" property="dealerAddress">
                                                <bean:write name="dealerAddress"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" style="padding-top: 8px;">Phone</th>
                                        <td valign="top" style="padding-top: 6px;">
                                            <html:text property="dealerPhone" /><br/>
                                            <html:messages id="dealerPhone" property="dealerPhone">
                                                <bean:write name="dealerPhone"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="height: 40px;"></th>
                                        <td style="height: 40px;">
                                            <html:hidden property="action" value="<%=String.valueOf(Constants.ADD)%>" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>                                            
                                </tbody>
                            </table>
                            <div class="blank-height"></div>
                        </html:form>                         
                    </div>
                </div>
                <div class="clear"></div>
                <div><%@include file="../includes/footer.jsp"%></div>
            </div>
        </body>
</html>