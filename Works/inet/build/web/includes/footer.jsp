<%@page import="com.myapp.struts.util.Utils,com.myapp.struts.system.SettingsLoader "%>
<div class="footer">
    <div class="full-div">
        <div class="fl_left" style="font-weight: bold">
            &nbsp;&nbsp;Server Time:&nbsp;<%=Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis())%>
        </div>
        <div class="fl_right" style="padding-right: 10px" >
            Copyright&copy;<a href="<%=SettingsLoader.getInstance().getSettingsDTO().getCopyrightLink()%>" target="_blank"><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%></a>
        </div>    
    </div>
</div>