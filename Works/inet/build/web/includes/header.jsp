<%@page import="com.myapp.struts.system.SettingsLoader" %>
<link href="../stylesheets/default.css" type="text/css" rel="stylesheet">
<link href="../stylesheets/default-color.css" type="text/css" rel="stylesheet">
<link href="../stylesheets/default-font.css" type="text/css" rel="stylesheet">
<link href="../stylesheets/style.css" rel="stylesheet" type="text/css" />

<div class="full-div bg-repeat" >
    <div class="top" ></div>
    <div class="notice">
        <div class="height-8px"></div>
        <marquee behavior="scroll" scrollamount="3" direction="left"><%=SettingsLoader.getInstance().getSettingsDTO().getEmergencyNoticeText()%></marquee>
    </div>
</div>
<!-- logo,header contents  -->



