<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.client.OldReportDAO,com.myapp.struts.payment.PaymentDTO,java.util.ArrayList" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Home</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>                
                <div style="height: 20px"></div>
                <div class="body">
                    <%
                        OldReportDAO oldReportDAO = new OldReportDAO();
                        ArrayList summeryList = oldReportDAO.getOldReportDTOs(login_dto);
                        if (summeryList != null && summeryList.size() > 0) {
                            double totalPaid = 0.0;
                            double totalReceived = 0.0;
                            double totalUsed = 0.0;
                            double totalPreviousBalance = 0.0;
                            double totalBalance = 0.0;
                    %>
                    <table class="input_table" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="7" align="center"  style="width:20%; font-size: 12px; font-weight: bold; color: blue;border-bottom:  1px solid #6c6c6c;">Old Transaction Report</td>
                        </tr>
                        <tr>
                            <td align="center"  style="width:13%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Client ID</td>
                            <td align="center"  style="width:12%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Type</td>
                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Previous Balance</td>
                            <%
                                if (!login_dto.getIsUser() && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                            %>
                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Total Received</td>
                            <%                                                                                                } else {
                            %>
                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Total Paid</td>
                            <%                                                                                                }
                            %>
                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Total Used</td>                                           
                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;">Current Balance</td>
                            <%
                                if (!login_dto.getIsUser() && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT) {
                            %>
                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-bottom:  1px solid #6c6c6c;">Total Paid</td>
                            <%                                                                                                } else {
                            %>
                            <td align="center"  style="width:15%; font-size: 12px; font-weight: bold; color: olive;border-bottom:  1px solid #6c6c6c;">Total Received</td>
                            <%                                                                                                }
                            %>
                        </tr>
                        <%
                            for (int i = 0; i < summeryList.size(); i++) {
                                PaymentDTO pdto = (PaymentDTO) summeryList.get(i);
                                totalPaid += pdto.getPaymentAmount();
                                totalReceived += pdto.getReceivedAmount();
                                totalUsed += pdto.getClientUses();
                                totalPreviousBalance += pdto.getClientPreviousBalance();
                        %>
                        <tr>
                            <td align="left"  style="width:13%; padding-left: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=pdto.getPaymentToClient()%></td>
                            <td align="center"  style="width:12%; padding-left: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=pdto.getPaymentToClientTypeName()%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c; border-bottom:  1px solid #6c6c6c;"><%=(long) pdto.getClientPreviousBalance()%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=(long) pdto.getPaymentAmount()%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=(long) pdto.getClientUses()%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;border-bottom:  1px solid #6c6c6c;"><%=(long) (pdto.getClientBalance())%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-bottom:  1px solid #6c6c6c;"><%=(long) (pdto.getReceivedAmount())%></td>
                        </tr>
                        <%
                                totalBalance += (long) (pdto.getClientBalance());
                            }
                        %>
                        <tr>
                            <td align="left"  colspan="2" style="width:25%; text-align: center;padding-left: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;">Total</td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;"><%=(long) totalPreviousBalance%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;"><%=(long) totalPaid%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;"><%=(long) totalUsed%></td>                                      
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; border-right: 1px solid #6c6c6c;"><%=(long) totalBalance%></td>
                            <td align="right"  style="width:15%; padding-right: 5px; font-weight: bold; font-size: 12px; color: #363636; "><%=(long) totalReceived%></td>                                      
                        </tr>
                    </table>
                    <%                                                 }
                    %>
                </div>
                <div class="clear height-20px"></div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>