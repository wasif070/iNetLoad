<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.SC_ADD && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
        response.sendRedirect("../index.do");
        return;
    }
%>
<%@page import="com.myapp.struts.scratchcard.ScratchcardDTO,com.myapp.struts.scratchcard.ScratchcardTypeLoader,java.util.ArrayList"%>

<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>
<script language="javascript" type="text/javascript">

    window.onload=function(){
        getTypeList();
    } 

    function getTypeList(){     
        $.ajax({
            url:"../scratchcard/type-list.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random(),company:document.forms[0].companyName.value},
            cache: false,
            dataType:'html',
            async:false,
            success:function(html) {
                if(html!=''){
                    $(".card-type-list").html($.trim(html));
                }
            }
        });
    }
    
    function getTypeList(type){     
        $.ajax({
            url:"../scratchcard/type-list.jsp",
            type:"POST",
            data:{sid:getSID(),rand:Math.random(),company:document.forms[0].companyName.value,refType:type},
            cache: false,
            dataType:'html',
            async:false,
            success:function(html) {
                if(html!=''){
                    $(".card-type-list").html($.trim(html));
                }
            }
        });
    }    

    function getSID(){
        var sid=new Date();
        return sid.getTime();
    }
        
    
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Scratch Card</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <html:form action="/scratchcard/addScratchcard" method="post" styleId="scratchcard_form">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Add Scratch Card</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="ScratchcardForm" property="message" filter="false"/>
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th>Card Name</th>
                                    <td>
                                        <html:select property="companyName" onchange="getTypeList()" styleId="companyName" >
                                            <%
                                                ArrayList<ScratchcardDTO> cardTypeList = ScratchcardTypeLoader.getInstance().getScratchcardTypeDTOList();
                                                if (cardTypeList != null && cardTypeList.size() > 0) {
                                                    for (int i = 0; i < cardTypeList.size(); i++) {
                                                        ScratchcardDTO dto = cardTypeList.get(i);
                                            %>
                                            <html:option value="<%=dto.getCompanyName()%>"><%=dto.getCompanyName()%></html:option>
                                            <%
                                                    }
                                                }
                                            %>                                            
                                        </html:select>    
                                        <html:messages id="companyName" property="companyName">
                                            <bean:write name="companyName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Card Type</th>
                                    <td> 
                                        <div class="card-type-list"></div>  
                                    </td>
                                </tr>                                                                               
                                <tr>
                                    <th>Serial No</th>
                                    <td>
                                        <html:text property="serialNo" styleId="serialNo" />
                                        <html:messages id="serialNo" property="serialNo">
                                            <bean:write name="serialNo"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr> 
                                <tr>
                                    <th>PIN</th>
                                    <td>
                                        <html:text property="pin" styleId="pin" />
                                        <html:messages id="pin" property="pin">
                                            <bean:write name="pin"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                         
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="type" value="0" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submitButton" type="button" class="custom-button" value="Add" onclick="submitConfirm();" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>                       
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

<div id="dialog-confirm" title="Recheck!!">
    <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><div id="confirm-message" style="font-weight: normal; color:#e17009; font-size: 1.05em; text-align: left;margin-left: 50px"></div></p>
</div>

<script type="text/javascript" language="javascript">
    function submitConfirm(){      
        var message="Are these information correct?<br>";
        message+="Card Type : "+$("#companyName").val()+"-"+$("#refillType").val()+"-"+$("#refillAmount").val()+"<br>";
        message+="Serial No : "+$("#serialNo").val()+"<br>";
        message+="PIN : "+$("#pin").val()+"<br>";          
        $("#confirm-message").html(message);
        $('#dialog-confirm').dialog('option','title','Please Recheck!!!').dialog('open');
        return true;
    }
    $(function() {
        $("#dialog-confirm").dialog({autoOpen: false, modal: true, width:350,
            buttons: {
                Cancel: function() {
                    $(this).dialog( "close" );
                },
                Ok: function() {
                    $(this).dialog( "close" );
                    $("#scratchcard_form").submit();
                }
            }
        });
        // End Changed
    });
</script>