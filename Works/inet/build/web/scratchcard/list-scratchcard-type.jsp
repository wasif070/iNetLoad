<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.scratchcard.ScratchcardDTO" %>

<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO != null && !perDTO.SC && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
    int pageNo = 1;
    int sortingOrder = 0;
    int sortedItem = 0;
    int list_all = 0;
    int recordPerPage = 100;

    if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_TYPE_RECORD_PER_PAGE) != null) {
        recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_TYPE_RECORD_PER_PAGE).toString());
    }
    if (request.getParameter("d-49216-p") != null) {
        pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
    }
    if (request.getParameter("d-49216-s") != null) {
        sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
    }
    if (request.getParameter("d-49216-o") != null) {
        sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
    }
    if (request.getParameter("list_all") != null) {
        list_all = Integer.parseInt(request.getParameter("list_all"));
    }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }    

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j= 0;
        var selectedIDsArray = [];     
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j] = i;
                    j++;
                }
            }
        }
        document.forms[0].reset(); 
        for(t=0; t<allInputArray.length; t++)
        {
                            
            checkFlag = 0;
            for(i=0; i<selectedIDsArray.length; i++)
            {
                if(t==selectedIDsArray[i])
                {
                    checkFlag = 1;
                    break;
                }
            }
            if(checkFlag==1)
            {
                tempCheckbox = allInputArray[selectedIDsArray[i]];
                tempCheckbox.checked = true;
            }
            else
            {
                tempCheckbox = allInputArray[t];
                tempCheckbox.checked = false;
            }
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }    
        link = "../scratchcard/listScratchcardType.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../scratchcard/listScratchcardType.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

    function editScratchCardType(value1,value2)
    {
        document.forms[0].reset();
        link = "companyNamePar="+document.forms[0].companyNamePar.value+"&d-49216-p="+document.forms[0].pageNo.value+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = "../scratchcard/getScratchcardType.do?id="+value1+"&name="+value2+"&link="+link;
        document.forms[0].submit();
    }
</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Scratch Card Type List</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">   
                    <%
                      if(login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (perDTO != null && perDTO.RCG_ADD))    
                      {
                    %>                     
                    <div class="blank-height" align="right"><a href="../scratchcard/add-scratchcard-type.jsp"><font color="blue" style="text-decoration: underline">Add Scratch Card Type</font></a></div>
                    <%
                      }
                    %>
                    <html:form action="/scratchcard/listScratchcardType.do" method="post">
                        <div class="full-div">
                            <div class="half-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">                                                                      
                                    <tr>
                                        <th>Company Name</th>
                                        <td>
                                            <html:text property="companyNamePar" />
                                        </td>
                                    </tr>                                                                     
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="full-div">
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-30px">
                            <%
                                if (request.getSession(true).getAttribute(Constants.MESSAGE) != null) {
                                    out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                } 
                                boolean scEditPer = false;
                                if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.SC_EDIT))
                                      && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                                {
                                  scEditPer = true;
                                }    
                            %>
                        </div>
                        <c:set var="scEdit" scope="page" value="<%=scEditPer%>"/>
                        <div style="border : 1px solid #848284;" align="center">
                            <jsp:scriptlet>
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                            public String addRowClass() {
                                ScratchcardDTO obj = (ScratchcardDTO) getCurrentRowObject();
                                if (obj.getStatusID() == Constants.USER_STATUS_BLOCK) {
                                    return "red";
                                } else {
                                    return "";
                                }
                            }
                        });
                            </jsp:scriptlet>                            
                            <div class="blank-height"></div>
                            <script type="text/javascript">var count=<%=(pageNo - 1) * recordPerPage%>;</script>
                            <display:table class="reporting_table" decorator="dyndecorator" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ScratchcardForm.scratchCardTypeList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Scratch Card Type" />
                                <display:setProperty name="paging.banner.items_name" value="Scratch Card Types" />
                                <display:column class="custom_column1" media="html" title="<input type='checkbox' name='allbox' onclick='checkAll(this)' style='width:100%;text-align: center;' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs" value="${data.id}" style="width:100%;text-align: center;" />
                                </display:column>
                                <display:column class="custom_column2" title="Nr" media="html" style="width:7%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Company Name" sortable="true" style="width:18%" media="html"  > 
                                    <c:choose>
                                        <c:when test="${scEdit==false}">${data.companyName}</c:when>
                                        <c:otherwise><a href="#" onclick="editScratchCardType('${data.id}','${data.companyName}')"><u>${data.companyName}</u></a></c:otherwise>
                                    </c:choose>                                                                                                           
                                </display:column> 
                                <display:column  title="Company Name" property="companyName" sortable="true" style="width:16%" media="csv excel pdf"  /> 
                                <display:column  property="supportedRefillTypes" title="Supported Refill Types" sortable="false" style="width:28%" />
                                <display:column  property="supportedRefillAmounts" title="Supported Refill Amounts" sortable="false" style="width:30%" />
                                <display:column  class="custom_column1" property="statusName"  sortProperty="statusID" title="Status" sortable="true" style="width:10%" />                               
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <script type="text/javascript">
                                idList = "<%=request.getAttribute(Constants.SCRATCH_CARD_TYPE_ID_LIST)%>";
                                if(idList.indexOf(",")!=-1)
                                {
                                    inputArray = document.getElementsByTagName("input");
                                    for(i=0; i<inputArray.length; i++)
                                    {
                                        if(inputArray[i].name == "selectedIDs" )
                                        {
                                            tempCheckbox = inputArray[i];

                                            if(idList.indexOf(","+tempCheckbox.value+",")!=-1)
                                            {
                                                tempCheckbox.checked = true;
                                            }
                                            else
                                            {
                                                tempCheckbox.checked = false;
                                            }
                                        }
                                    }
                                }
                            </script>
                            <%
                                request.removeAttribute(Constants.SCRATCH_CARD_TYPE_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);    
                                if(scEditPer)
                                {                                
                            %>
                            <BR>
                                <input type="submit" class="action-button" value="Activate Selected" style="width: 120px;" onclick="submitform('<%=Constants.ACTIVATION%>')" /> 
                                <input type="submit" class="action-button" value="Block Selected" style="width: 115px;" onclick="submitform('<%=Constants.BLOCK%>')" /> 
                                <input type="submit" class="action-button" value="Delete Selected" style="width: 115px;" onclick="submitform('<%=Constants.DELETE%>')" />                                  
                                <div class="blank-height"></div>
                                <%
                                    }  
                                %>                                
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>