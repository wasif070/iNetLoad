<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.scratchcard.ScratchcardDTO,com.myapp.struts.session.Constants" %>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Refill Request Processing</title>
    </head>
    <body>

        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div class="blank-height"></div>
                <div class="body">
                    <%
                        ScratchcardDTO dto = (ScratchcardDTO) request.getSession(true).getAttribute(Constants.SCRATCH_CARD_SUCCESS_DTO);
                        String name = dto.getCompanyName();
                        String type = dto.getRefillType();
                        String amount = dto.getRefillAmount();
                    %>
                    <h1>Scratch Card Buy Request Was Sent Successfully!!!</h1>
                    <div class="blank-height"></div>
                    <p style="color: blueviolet; font-weight: bold; border-style: solid; border-color: #363636; border-width: 1px;">Card Name: <%=name%>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Type: <%=type%>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Amount: <%=amount%></p>
                    <p style="color: brown; font-weight: bold; text-align: justify; font-size: 16px;">Your Request Will Be Processed Within A Moment. New Scratch Card Will Be Found In 'My Scratch Cards'. Thank You.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>