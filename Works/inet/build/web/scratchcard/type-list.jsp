<%@page import="com.myapp.struts.scratchcard.ScratchcardDTO,com.myapp.struts.scratchcard.ScratchcardTypeLoader,java.util.ArrayList"%>
<%@page import="com.myapp.struts.session.Constants"%>

<%
    String company = request.getParameter("company");
    String refType = request.getParameter("refType");
    String refAmount = request.getParameter("refAmount");
    ScratchcardDTO cardTypeDTO = ScratchcardTypeLoader.getInstance().getScratchcardTypeDTOByCompany(company);
    if (cardTypeDTO != null) {
        String[] refillTypes = cardTypeDTO.getSupportedRefillTypes().split(",");
        if (refType == null) {
            refType = refillTypes[0];
        }
        String[] refillAmounts = cardTypeDTO.getSupportedRefillAmounts().split(",");
        String refillAmountsForSelectedType = null;
%>         
<select name="refillType"  id="refillType" style="width: 90px" onchange="getTypeList(this.value)">
    <%
        String selected = "";
        if (refillTypes != null) {
            for (int i = 0; i < refillTypes.length; i++) {
                selected = "";
                if (refillTypes[i].equalsIgnoreCase(refType)) {
                    selected = "selected";
                    refillAmountsForSelectedType = refillAmounts[i];
                }
    %>
    <option <%=selected%> value="<%=refillTypes[i]%>" ><%=refillTypes[i]%></option>
    <%
            }
        }
    %>
</select>
<select name="refillAmount" id="refillAmount" style="width: 90px">
    <%
        if (refillAmountsForSelectedType != null) {
            String[] refillAmountList = refillAmountsForSelectedType.split("\\|");
            if (refillAmountList != null) {
                for (int i = 0; i < refillAmountList.length; i++) {
                    selected = "";
                    if (refillAmountList[i].equalsIgnoreCase(refAmount)) {
                        selected = "selected";
                    }
    %>
    <option <%=selected%> value="<%=refillAmountList[i]%>" ><%=refillAmountList[i]%></option>
    <%
                }
            }
        }
    %>
</select>    
<%
    }
%>          