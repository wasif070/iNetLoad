<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.scratchcard.ScratchcardDTO,java.util.ArrayList,com.myapp.struts.util.Utils" %>

<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            response.sendRedirect("../home/home.do");
            return;
        } else {
            if (login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                response.sendRedirect("../home/home.do");
                return;
            }
        }
    }
%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<link href="../stylesheets/jquery-ui-1.8.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery-ui-1.8.2.custom.min.js"></script>

<%
    int pageNo = 1;
    int sortingOrder = 0;
    int sortedItem = 0;
    int list_all = 0;
    int recordPerPage = 100;

    if (request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE) != null) {
        recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SCRATCH_CARD_RECORD_PER_PAGE).toString());
    }
    if (request.getParameter("d-49216-p") != null) {
        pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
    }
    if (request.getParameter("d-49216-s") != null) {
        sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
    }
    if (request.getParameter("d-49216-o") != null) {
        sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
    }
    if (request.getParameter("list_all") != null) {
        list_all = Integer.parseInt(request.getParameter("list_all"));
    }
%>
<script language="javascript" type="text/javascript">

    function highlightTableRows(tableId) {
        var previousClass = null;
        var table = document.getElementById(tableId);
        var tbody = table.getElementsByTagName("tbody")[0];
        var rows = tbody.getElementsByTagName("tr");
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className; this.className='WUIrsrecordrowactive';};
            rows[i].onmouseout = function(){ this.className=previousClass };
        }
    }    

    function checkAll(iobj)
    {
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs" && allInputArray[i] != iobj)
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    tempCheckbox.checked = false;
                }
                else
                {
                    tempCheckbox.checked = true;
                }
            }
        }
    }

    function submitform(action)
    {
        var j= 0;
        var selectedIDsArray = [];     
        var allInputArray = document.getElementsByTagName("input");
        for(i=0; i<allInputArray.length; i++)
        {
            if(allInputArray[i].name == "selectedIDs")
            {
                tempCheckbox = allInputArray[i];
                if(tempCheckbox.checked)
                {
                    selectedIDsArray[j] = i;
                    j++;
                }
            }
        }
        document.forms[0].reset(); 
        for(t=0; t<allInputArray.length; t++)
        {
                            
            checkFlag = 0;
            for(i=0; i<selectedIDsArray.length; i++)
            {
                if(t==selectedIDsArray[i])
                {
                    checkFlag = 1;
                    break;
                }
            }
            if(checkFlag==1)
            {
                tempCheckbox = allInputArray[selectedIDsArray[i]];
                tempCheckbox.checked = true;
            }
            else
            {
                tempCheckbox = allInputArray[t];
                tempCheckbox.checked = false;
            }
        }
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }    
        link = "../scratchcard/myScratchcards.do?d-49216-p="+document.forms[0].pageNo.value+"&action="+action+"&list_all="+<%=list_all%>;
        sortedItemVal = <%=sortedItem%>;
        if(sortedItemVal>0)
        {
            link += "&d-49216-s="+sortedItemVal;
        }
        sortingOrderVal = <%=sortingOrder%>;
        if(sortingOrderVal>0)
        {
            link += "&d-49216-o="+sortingOrderVal;
        }

        document.forms[0].action = link;
    }

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../scratchcard/myScratchcards.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

</script>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: My Scratch Cards</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-20px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">   
                    <html:form action="/scratchcard/myScratchcards.do" method="post">
                        <div class="full-div">
                            <div class="full-div">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">                                                                      
                                    <tr>
                                        <th>Card Name</th>
                                        <td>
                                            <html:text property="companyNamePar" />
                                        </td>
                                        <th>Card Type</th>
                                        <td>
                                            <html:text property="refillTypePar" />
                                        </td>
                                    </tr>  
                                    <tr>
                                        <th>Card Amount</th>
                                        <td>
                                            <html:text property="refillAmountPar" />
                                        </td>
                                        <th>Serial No</th>
                                        <td>
                                            <html:text property="serialNoPar" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Start Time</th>
                                        <td>
                                            <html:select property="startDay" styleClass="width-50px" styleId="startDay">
                                                <%
                                                            ArrayList<Integer> days = Utils.getDay();
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=increment%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="startMonth" styleClass="width-50px" styleId="startMonth">
                                                <%
                                                            ArrayList<String> months = Utils.getMonth();
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=month%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="startYear" styleClass="width-60px" styleId="startYear">
                                                <%
                                                            ArrayList<Integer> years = Utils.getYear();
                                                            for (int i = 0; i < years .size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                %>
                                                <html:option value="<%=year%>"><%=year%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                        <th>End Time</th>
                                        <td>
                                            <html:select property="endDay" styleClass="width-50px" styleId="endDay">
                                                <%
                                                            ArrayList<Integer> days1 = Utils.getDay();
                                                            for (int i = 0; i < days1.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=increment%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="endMonth" styleClass="width-50px" styleId="endMonth">
                                                <%
                                                            ArrayList<String> months1 = Utils.getMonth();
                                                            for (int i = 0; i < months1.size(); i++) {
                                                                String month = months1.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                %>
                                                <html:option value="<%=increment%>"><%=month%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="endYear" styleClass="width-60px" styleId="endYear">
                                                <%
                                                            ArrayList<Integer> years1 = Utils.getYear();
                                                            for (int i = 0; i < years1.size(); i++) {
                                                                String year = String.valueOf(years1.get(i));
                                                %>
                                                <html:option value="<%=year%>"><%=year%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>
                                    </tr>                                        
                                    <tr>
                                        <th>Record Per Page</th>
                                        <td>
                                            <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                        </td>
                                        <th>Go To Page No.</th>
                                        <td>
                                            <input type="text" name="pageNo" value="<%=pageNo%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="full-div" style="text-align: center;">
                                                <html:submit styleClass="search-button" value="Search" onclick="search()" />
                                                <html:reset styleClass="search-button" value="Reset" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear height-30px">
                            <%
                                if (request.getSession(true).getAttribute(Constants.MESSAGE) != null) {
                                    out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                }     
                            %>
                        </div>
                        <div style="border : 1px solid #848284;" align="center">                           
                            <div class="blank-height"></div>
                            <jsp:scriptlet>
                            request.setAttribute("dyndecorator", new org.displaytag.decorator.TableDecorator() {
                                public String addRowClass() {
                                    long time_diff = 30 * 60 * 1000;
                                    ScratchcardDTO obj = (ScratchcardDTO) getCurrentRowObject();
                                    if ((System.currentTimeMillis()-obj.getUsedTime()) > time_diff) {
                                        return "red";
                                    } else {
                                        return "";
                                    }
                                }
                            });
                            </jsp:scriptlet>                            
                            <script type="text/javascript">var count=<%=(pageNo - 1) * recordPerPage%>;</script>
                            <display:table class="reporting_table" decorator="dyndecorator" defaultsort="7" defaultorder="descending" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ScratchcardForm.scratchCardList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Scratch Card" />
                                <display:setProperty name="paging.banner.items_name" value="Scratch Cards" />
                                <display:column class="custom_column2" title="Nr" media="html" style="width:5%; font-size: 14px;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Card Name" property="companyName" sortable="true" style="width:15%; font-size: 14px;"  /> 
                                <display:column  class="custom_column1" property="refillType" title="Type" sortable="true" style="width:12%; font-size: 14px;"  />
                                <display:column  class="custom_column2" property="refillAmount" title="Amount" sortable="true" style="width:12%; font-size: 14px;"  /> 
                                <display:column  property="serialNo" title="Serial No." sortable="true" style="width:21%; font-size: 14px;" /> 
                                <display:column  property="pin" title="PIN" sortable="true" style="width:21%; font-size: 14px;" /> 
                                <display:column  class="custom_column1" decorator="implement.displaytag.LongDateWrapper" property="usedTime" sortProperty="usedTime" title="Buy Time" sortable="true" style="width:14%; font-size: 14px;" /> 
                            </display:table>
                            <script type="text/javascript">highlightTableRows("data");</script>
                            <%
                                request.getSession(true).removeAttribute(Constants.MESSAGE);    
                            %>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>