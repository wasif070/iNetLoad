<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
            response.sendRedirect("../home/home.do");
            return;
        }
    }
%>
<%@page import="com.myapp.struts.client.ClientLoader,com.myapp.struts.client.ClientDTO" %>
<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,java.util.ArrayList" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Upload Contacts</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">
                    <form action="../contacts/uploadContacts.do?groupID=<%=request.getParameter("groupID")%>" method="post" name="upform" enctype="multipart/form-data">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Upload Contacts</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;color: #ff0000;font-weight: bold" valign="bottom">
                                        <%
                                            if (request.getSession(true).getAttribute(Constants.MESSAGE) != null) {
                                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                                            }
                                        %>
                                    </td>
                                </tr> 
                                <tr>
                                    <td colspan="2" align="center">
                                        <b><u>CSV File Format</u></b>
                                    </td>
                                </tr>   
                                <tr>
                                    <td colspan="2" align="center">
                                        Syntax: Name,Description,Number,Refill Type(1:Prepaid|2:Postpaid),Refill Amount
                                    </td>
                                </tr> 
                                <tr>
                                    <td colspan="2" align="center">
                                        Sample: Axxx,Dxxxxx,880XXXXXXXXXX,1,100
                                    </td>
                                </tr>                                     
                                <tr>
                                    <th>Browse for csv file</th>
                                    <td>
                                        <input type="file" name="uploadfile" size="20" />
                                    </td>
                                </tr>                                
                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input name="submit" type="submit" class="custom-button" value="Upload" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>