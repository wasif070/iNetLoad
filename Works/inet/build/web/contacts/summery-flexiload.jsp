<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
    if (login_dto == null) {
        response.sendRedirect("../home/home.do");
        return;
    }
    if (login_dto.getIsUser() == true) {
        response.sendRedirect("../home/home.do");
        return;
    } else {
        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
            response.sendRedirect("../home/home.do");
            return;
        }

    }
%>
<%@page import="com.myapp.struts.flexi.FlexiDTO,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />

<%
            int pageNo = 1;
            int list_all = 0;
            int recordPerPage = 100;

            if (request.getSession(true).getAttribute(Constants.CONTACT_SUMMERY_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.CONTACT_SUMMERY_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }
            if (request.getParameter("list_all") != null) {
               list_all = Integer.parseInt(request.getParameter("list_all"));
            }
%>
<script language="javascript" type="text/javascript">

    function search()
    {
        if(document.forms[0].pageNo.value<=0)
        {
            document.forms[0].pageNo.value = 1;
        }
        document.forms[0].action = "../contacts/summeryFlexiload.do?list_all=0&d-49216-p="+document.forms[0].pageNo.value;
    }

</script>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Refill Summery</title>
    </head>
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-10px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="view-page-body">
                    <div class="full-div height-5px"></div>
                    <div class="full-div"><strong>Refill Summery</strong></div>
                    <div class="clear"></div>
                    <div class="full-div">
                        <%
                            String action = "/contacts/summeryFlexiload.do";
                        %>
                        <html:form action="<%=action%>" method="post">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Start Time</th>
                                    <td>
                                        <html:select property="startDay" styleClass="width-50px" styleId="startDay">
                                            <%
                                                        ArrayList<Integer> days = Utils.getDay();
                                                        for (int i = 0; i < days.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startMonth" styleClass="width-50px" styleId="startMonth">
                                            <%
                                                        ArrayList<String> months = Utils.getMonth();
                                                        for (int i = 0; i < months.size(); i++) {
                                                            String month = months.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="startYear" styleClass="width-60px" styleId="startYear">
                                            <%
                                                        ArrayList<Integer> years = Utils.getYear();
                                                        for (int i = 0; i < years .size(); i++) {
                                                            String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>Group Name</th>
                                    <td>
                                        <html:text property="groupNamePar" styleId="groupNamePar"/>
                                    </td> 
                                </tr>
                                <tr>
                                    <th>End Time</th>
                                    <td>
                                        <html:select property="endDay" styleClass="width-50px" styleId="endDay">
                                            <%
                                                        ArrayList<Integer> days1 = Utils.getDay();
                                                        for (int i = 0; i < days1.size(); i++) {
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=increment%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endMonth" styleClass="width-50px" styleId="endMonth">
                                            <%
                                                        ArrayList<String> months1 = Utils.getMonth();
                                                        for (int i = 0; i < months1.size(); i++) {
                                                            String month = months1.get(i);
                                                            String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="endYear" styleClass="width-60px" styleId="endYear">
                                            <%
                                                        ArrayList<Integer> years1 = Utils.getYear();
                                                        for (int i = 0; i < years1.size(); i++) {
                                                            String year = String.valueOf(years1.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>Contact Name / No</th>
                                    <td>
                                        <html:text property="contactNamePar" styleId="contactNamePar"/>
                                    </td>                                                                        
                                </tr>
                                <tr>    
                                    <th>Report Type</th>
                                    <td colspan="3">
                                        <html:radio property="reportType" style="width:10px;" value="0"></html:radio><B>Group Wise</B>&nbsp;&nbsp;
                                        <html:radio property="reportType" value="1" style="width:10px"></html:radio><B>Contact Wise</B>&nbsp;&nbsp;
                                        <html:radio property="reportType" value="2" style="width:10px"></html:radio><B>Detail</B> 
                                    </td>
                                </tr>                                    
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="pageNo" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="text-align: center;">
                                        <html:submit property="startSearch" styleClass="search-button" value="Search" onclick="search()" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </html:form>
                    </div>
                    <div class="clear"></div>                  
                    <%
                       int reportType  = 0;
                       if(request.getParameter("reportType")!=null)
                        {
                           reportType = Integer.parseInt(request.getParameter("reportType"));
                        }
                    %>
                    <% if(reportType==2){  %>
                    <div>
                        <script type="text/javascript">
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>                        
                        <display:table class="reporting_table" decorator="org.displaytag.decorator.TotalTableDecorator" defaultsort="7" defaultorder="descending" style="width:100%;" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ContactForm.summeryList" pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Record" />
                            <display:setProperty name="paging.banner.items_name" value="Records" />
                            <display:column class="custom_column2" title="Nr" style="width:6%;" media="html">
                                <script type="text/javascript">
                                    document.write(++count+".");
                                </script>
                            </display:column>                            
                            <display:column property="contactName" title="Contact Name" sortable="true"  style="width:15%;" />
                            <display:column class="custom_column1" property="contactNo" title="Contact No"  sortable="true"  style="width:15%;" />  
                            <display:column property="contactDescription" title="Description" sortable="true" style="width:17%;" />
                            <display:column property="groupName" title="Group Name"  sortable="true"  style="width:15%;" />                                                
                            <display:column class="custom_column2" property="totalRefillAmount" title="Amount" format="{0,number,0.0}" sortable="true" total="true" style="width:8%;" />
                            <display:column class="custom_column1" property="reportDate"  title="Refill Time" decorator="implement.displaytag.LongDateWrapper" sortable="true"  style="width:15%;" />
                            <display:column class="custom_column1" property="statusName"  title="Status" sortable="true"  style="width:9%;" />
                        </display:table>
                        <div class="clear"></div>
                    </div>
                    <% } else if(reportType==1){ %>
                    <div>
                        <script type="text/javascript">
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>
                        <display:table class="reporting_table" decorator="org.displaytag.decorator.TotalTableDecorator" defaultsort="2" defaultorder="ascending" style="width:100%;" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ContactForm.summeryList" pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Record" />
                            <display:setProperty name="paging.banner.items_name" value="Records" />
                            <display:column class="custom_column2" title="Nr" style="width:6%;" media="html" >
                                <script type="text/javascript" >
                                    document.write(++count+".");
                                </script>
                            </display:column>
                            <display:column property="contactName" title="Contact Name" sortable="true"  style="width:20%;" />
                            <display:column class="custom_column1" property="contactNo" title="Contact No"  sortable="true"  style="width:19%;" />                             
                            <display:column property="contactDescription" title="Description" sortable="true"  style="width:20%;" />
                            <display:column property="groupName" title="Group Name"  sortable="true"  style="width:20%;" />                        
                            <display:column class="custom_column2" property="totalRefillAmount" title="Total Refill" format="{0,number,0.0}" sortable="true" total="true" style="width:15%;" />
                        </display:table>
                        <div class="clear"></div>
                    </div>
                    <% } else { %>
                    <div>
                        <script type="text/javascript">
                            var count=<%=(pageNo-1)*recordPerPage%>;
                        </script>
                        <display:table class="reporting_table" decorator="org.displaytag.decorator.TotalTableDecorator" defaultsort="2" defaultorder="ascending" style="width:100%;" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.ContactForm.summeryList" pagesize="<%=recordPerPage%>" >
                            <display:setProperty name="paging.banner.item_name" value="Record" />
                            <display:setProperty name="paging.banner.items_name" value="Records" />
                            <display:column class="custom_column2" title="Nr" style="width:10%;" media="html" >
                                <script type="text/javascript" >
                                    document.write(++count+".");
                                </script>
                            </display:column>
                            <display:column property="groupName" title="Group Name"  sortable="true"  style="width:40%;" />                       
                            <display:column class="custom_column2" property="totalRefillAmount" title="Total Refill" format="{0,number,0.0}" sortable="true" total="true" style="width:50%;" />
                        </display:table>
                        <div class="clear"></div>
                    </div>
                    <% }%>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>