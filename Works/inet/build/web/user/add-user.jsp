<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%
        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE) {
                request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                response.sendRedirect("../index.do");
                return;
        }
%>
<%@page import="com.myapp.struts.role.RoleDTO,com.myapp.struts.user.UserLoader,java.util.ArrayList" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add User</title>
    </head>    
    <body style="height: 100%">
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div align="right" class="height-30px"><span  style="font-size: 14px; font-weight: bold; font-family:'Bookman Old Style', serif; color: #686B6F; padding-right: 15px; font-style: oblique">User ID:&nbsp;<%=login_dto.getClientId()%></span></div>
                <div class="body">                    
                    <html:form action="/user/addUser" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        <span style="font-size: 20px; font-weight: bold; font-family:'Bookman Old Style', serif; color: blueviolet; padding-left: 50px;">Add New User</span> 
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                    <td colspan="2" align="center" style="padding-top: 6px;" valign="bottom">
                                        <bean:write name="UserForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">User ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="userId" /><br/>
                                        <html:messages id="userId" property="userId">
                                            <bean:write name="userId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Role</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="userRoleId">
                                         <%
                                            ArrayList roleDTOList = UserLoader.getInstance().getRoleDTOList();
                                            if(roleDTOList!=null)
                                                {
                                            for(int i=0;i<roleDTOList.size();i++)
                                            {
                                                RoleDTO roleDTO = (RoleDTO) roleDTOList.get(i);
                                         %>
                                             <html:option value="<%=String.valueOf(roleDTO.getId())%>"><%=roleDTO.getRoleName()%></html:option>
                                         <%
                                            }
                                            }
                                         %>
                                        </html:select><br/>
                                        <html:messages id="userRoleId" property="userRoleId">
                                            <bean:write name="userRoleId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="userName" /><br/>
                                        <html:messages id="userName" property="userName">
                                            <bean:write name="userName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Email</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="userEmail" /><br/>
                                        <html:messages id="userEmail" property="userEmail">
                                            <bean:write name="userEmail"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="userPassword" /><br/>
                                        <html:messages id="userPassword" property="userPassword">
                                            <bean:write name="userPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:password property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Status</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="userStatus">
                                            <%
                                             for(int i=1;i<Constants.LIVE_STATUS_VALUE.length;i++)
                                             {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                             }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="userStatus" property="userStatus">
                                            <bean:write name="userStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>

                                <tr>
                                    <th style="height: 40px;"></th>
                                    <td style="height: 40px;">
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>