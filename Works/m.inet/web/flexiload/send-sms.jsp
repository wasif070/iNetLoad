<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.client.ClientDAO,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<script type='text/javascript' src='../js/jquery-1.4.2.min.js'></script>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Send SMS Request</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <%
                if (login_dto == null) {
                    response.sendRedirect("../home/home.do");
                    return;
                }
                if (login_dto.getIsUser()) {
                    response.sendRedirect("../home/home.do");
                    return;
                } else {
                    if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
                        response.sendRedirect("../home/home.do");
                        return;
                    } else {
                        if (login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                            response.sendRedirect("../home/home.do");
                            return;
                        }
                    }
                }
                ClientDAO dao = new ClientDAO();
                double credit = dao.getClientCredit(login_dto.getId());
            %>

            <div class="body_content fl_left">                
                <div class="body">
                    <html:form action="/flexiload/sendSMS.do" method="post" styleId="flexi_form">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">Send New Message</th>
                                </tr>
                                <tr>
                                    <th colspan="2" align="center">Balance: <%=credit%></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center">
                                        <bean:write name="FlexiForm" property="message" filter="false"/>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 4px;">Country Code</th>
                                    <td valign="top" style="padding-top: 2px;">
                                        <html:select property="smsCountryCode" >
                                            <html:option value="+88">+88</html:option>
                                            <html:option value="+40">+40</html:option>
                                        </html:select>
                                        <html:messages id="smsCountryCode" property="smsCountryCode">
                                            <bean:write name="smsCountryCode"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 4px;">Mobile Number</th>
                                    <td valign="top" style="padding-top: 2px;">
                                        <html:text property="phoneNumber"  styleId="phoneNumber" />
                                        <html:messages id="phoneNumber" property="phoneNumber">
                                            <bean:write name="phoneNumber"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>       
                                <tr>
                                    <th valign="top" style="padding-top: 4px;">Sender Name</th>
                                    <td valign="top" style="padding-top: 2px;">
                                        <html:text property="smsSenderName" styleId="smsSenderName"  /><BR>
                                            <html:messages id="smsSenderName" property="smsSenderName">
                                                <bean:write name="smsSenderName"  filter="false"/>
                                            </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 4px;">Message</th>
                                    <td valign="top" style="padding-top: 2px;">
                                        <html:textarea property="smsMessage" styleId="smsMessage" style="width:60%"/><BR>
                                            <html:messages id="smsMessage" property="smsMessage">
                                                <bean:write name="smsMessage"  filter="false"/>
                                            </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding-top: 10px">
                                        <input type="hidden" name="sms" value="1" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <div style="margin-left: 40px;margin-top: 7px; margin-bottom: 5px;">
                                            <input name="submitButton" type="submit" class="custom-button" value="Send"/>
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <!-- <td valign="middle" style="padding-top:6px;">
                                         <div><span class="surcharge-summary blue"></span></div>
                                     </td>
                                    --!>
                                 </tr>
                             </tbody>
                         </table>
                                </html:form>
                            </div>
                        </div>                        
                        <div class="clear"></div>
                        <div><%@include file="../includes/footer.jsp"%></div>            
                    </div>        
                </body>
            </html>