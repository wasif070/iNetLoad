<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader,java.util.ArrayList,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Update Client</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <%
                        long edid = Long.parseLong(request.getParameter("id"));
                        if (login_dto == null) {
                            response.sendRedirect("../home/home.do");
                            return;
                        }
                        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
                                && perDTO != null && !perDTO.CLIENT_EDIT && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2
                                && login_dto.getId() != edid && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                            response.sendRedirect("../index.do");
                            return;
                        }
            %>
            <div class="body_content fl_left">                
                <div class="body full-div">
                    <html:form action="/client/editClient" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        Edit Client<br>
                                            <bean:write name="ClientForm" property="message" filter="false"/>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Client ID</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <html:text property="clientId" /><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <html:text property="clientPassword" /><br/>
                                        <html:messages id="clientPassword" property="clientPassword">
                                            <bean:write name="clientPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Pass.</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <html:text property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Status</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <%
                                                    if (login_dto.getIsUser() || edid != login_dto.getId()) {          
                                        %>
                                        <input type="hidden" name="searchLink" value="/client/listClient.do?list_all=0" />
                                        <html:select property="clientStatus" >
                                            <%                                                 
                                                        for (int i = 0; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                                        }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientStatus" property="clientStatus">
                                            <bean:write name="clientStatus"  filter="false"/>
                                        </html:messages>
                                        <%
                                           }
                                           else
                                           {                                                                        
                                        %> 
                                        <html:text property="clientStatusName" readonly="true" />
                                        <%
                                           }
                                        %> 
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">                                        
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <div style="margin-left: 40px; margin-top: 7px; margin-bottom: 5px; display: block">
                                            <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Update" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <html:hidden property="id" />
                        <html:hidden property="clientTypeId" />
                        <html:hidden property="parentId" />                        
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>