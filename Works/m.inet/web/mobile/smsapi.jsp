<%-- 
    Document   : index
    Created on : Aug 20, 2013, 9:46:13 AM
    Author     : user
--%>

<%@page import="com.myapp.struts.system.SettingsLoader"%>
<%@page import="com.myapp.struts.client.ClientDAO"%>
<%@page import="com.myapp.struts.util.MyAppError"%>
<%@page import="com.myapp.struts.login.LoginDTO"%>
<%@page import="com.myapp.struts.session.Constants"%>
<%@page import="com.myapp.struts.util.Utils"%>
<%@page import="com.myapp.struts.client.ClientLoader"%>
<%@page import="com.myapp.struts.client.ClientDTO"%>
<%@page import="com.myapp.struts.sms.SMSDAO"%>
<%@page import="com.myapp.struts.sms.SMSDTO"%>

<%
    boolean api = false;
    if (request.getParameter("FromAPI") != null) {
        api = true;
    }
    if (request.getParameter("RequestKey") == null || request.getParameter("RequestCode") == null || request.getParameter("UserId") == null
            || request.getParameter("MobileNumber") == null || request.getParameter("Message") == null) {
        if (api) {
            out.println("Error=1");
        } else {
            out.println("Access denied!");
        }
        return;
    }

    String user_name = request.getParameter("UserId");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        if (api) {
            out.println("Error=2");
        } else {
            out.println("Access denied!");
        }
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserId(user_name.trim().toLowerCase());
        if (clientDTO == null) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }
    }

    if (clientDTO != null) {
        String req_key = request.getParameter("RequestKey");
        String req_code = request.getParameter("RequestCode");
        String res_code = Utils.getMD5(req_key, clientDTO.getClientPassword().toLowerCase());
        if (!req_code.equals(res_code)) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }

        String number = request.getParameter("MobileNumber");
        String message = request.getParameter("Message");

        if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }

        if (clientDTO.getClientIp() != null && clientDTO.getClientIp().trim().length() > 0 && !request.getRemoteAddr().equals(clientDTO.getClientIp())) {
            if (api) {
                out.println("Error=2" + request.getRemoteAddr());
            } else {
                out.println("Access denied!");
            }
            return;
        }


        SMSDTO dto = new SMSDTO();

        if (number.length() == 0) {
            if (api) {
                out.println("Error=3");
            } else {
                out.println("Please enter mobile number");
            }
            return;
        } else if (number.length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
            if (api) {
                out.println("Error=3");
            } else {
                out.println("Please check mobile number");
            }
            return;
        } else if (number.length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
            if (number.startsWith("+")) {
                number = number.replaceFirst("+", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("00")) {
                number = number.replaceFirst("00", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("88")) {
                number = number.replaceFirst("88", "");
            }
            number = number.replace("-", "");
            number = number.replace(" ", "");
            String prefix = number.substring(0, 3);
            int operatorTypeID = 0;
            for (int i = 1; i < Constants.OPERATOR_TYPE_PREFIX.length; i++) {
                if (prefix.equals(Constants.OPERATOR_TYPE_PREFIX[i])) {
                    operatorTypeID = i;
                    break;
                }
            }
            if (operatorTypeID < Constants.GP) {
                if (api) {
                    out.println("Error=3");
                } else {
                    out.println("Mobile number is invalid!");
                }
                return;
            } else {
                if (number.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                    dto.setOperatorTypeID(operatorTypeID);
                    dto.setPhoneNumber(number);
                } else {
                    if (api) {
                        out.println("Error=3");
                    } else {
                        out.println("Mobile number is invalid!");
                    }
                    return;
                }
            }
        }

        if (message != null && message.length() <= 0) {
            if (api) {
                out.println("Error=4");
            } else {
                out.println("Message is required!");
            }
            return;
        }

        SMSDAO flDAO = new SMSDAO();

        dto.setMessage(message);
        dto.setAmount(2.50); // This is per SMS cost....
        LoginDTO login_dto = new LoginDTO();
        login_dto.setId(clientDTO.getId());
        login_dto.setClientParentId(clientDTO.getParentId());
        login_dto.setClientTypeId(clientDTO.getClientTypeId());

        MyAppError error = flDAO.addSMSRequest(login_dto, dto);
        if (error.getErrorType() == error.NoError) {
            if (api) {
                out.println("RequestID=" + error.getErrorMessage());
            } else {
                ClientDAO clDAO = new ClientDAO();
                double balance = clDAO.getClientCredit(clientDTO.getId());
                out.println("Refill Request Sent Successfully.");
                out.println(" Current Balance: " + balance + " Tk.");
            }
        } else if (error.getErrorType() == error.ValidationError) {
            if (api) {
                out.println("Error=5");
            } else {
                out.println("Refill request is failed!" + error.getErrorMessage());
            }
        } else {
            if (api) {
                out.println("Error=7");
            } else {
                out.println("Refill request is failed!" + error.getErrorMessage());
            }
        }
    }
%>