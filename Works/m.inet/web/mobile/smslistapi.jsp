<%@page import="com.myapp.struts.sms.SMSRequestDTO"%>
<%@page import="com.myapp.struts.sms.SMSDAO"%>
<%@page import="com.myapp.struts.util.Utils,com.myapp.struts.session.Constants"%>
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader"%>
<%@page import="com.myapp.struts.mobile.LoadRequestDAO,com.myapp.struts.mobile.RequestDTO,java.util.ArrayList,com.google.gson.Gson"%>

<%
    if (request.getParameter("RequestKey") == null || request.getParameter("RequestCode") == null || request.getParameter("UserId") == null || request.getParameter("listType") == null) {
        out.println("Error=1");
        return;
    }

    String user_name = request.getParameter("UserId");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        out.println("Error=2");
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserId(user_name.trim().toLowerCase());
        if (clientDTO == null) {
            out.println("Error=2");
            return;
        }
    }

    if (clientDTO != null) {
        String req_key = request.getParameter("RequestKey");
        String req_code = request.getParameter("RequestCode");
        String res_code = Utils.getMD5(req_key, clientDTO.getClientPassword().toLowerCase());
        if (!req_code.equals(res_code)) {
            out.println("Error=2");
            return;
        }
        if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            out.println("Error=2");
            return;
        }
        Gson gson = new Gson();
        if (request.getParameter("listType") != null) {
            ArrayList<SMSRequestDTO> data = SMSDAO.getSMSRequests(clientDTO, Integer.parseInt(request.getParameter("listType")));
            if (data != null && data.size() > 0) {
                out.print(gson.toJson(data));
            }
        }
    }
%>  
