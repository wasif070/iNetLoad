<%@page language="java"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.session.Constants,com.myapp.struts.session.PermissionConstants,com.myapp.struts.user.UserLoader" %>
<%@include file="../login/login-check.jsp"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<link href="../stylesheets/default-font.css" type="text/css" rel="stylesheet">
<link href="../stylesheets/style.css" rel="stylesheet" type="text/css" />
<%
            if (login_dto.getRoleID() > Constants.ROOT_RESELLER_PARENT_ID) {
                perDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO();
            }
%>
<div class="full-div" ><div class="top" ></div></div>
<div class="height-5px"></div>
<div class="full-div height-40px">
<div class="full-div" style="display: block; margin-left: 3px">
    <div class="fl_left"><a href="../home/home.do">Home&nbsp;</a></div>
    <%
    if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
    {
    %>
    <div class="fl_left"><a href="../flexiload/add-flexiload.jsp" >|&nbsp;Refill&nbsp;Now&nbsp;</a></div>
    <div class="fl_left"><a href="../flexiload/add-bt.jsp" >|&nbsp;Balance&nbsp;Transfer&nbsp;</a></div>
    <div class="fl_left"><a href="../flexiload/send-sms.jsp" >|&nbsp;SMS&nbsp;</a></div>
    <div class="fl_left"><a href="../flexiload/surcharge-list.jsp" >|&nbsp;Surcharge &nbsp;List&nbsp;</a></div>
    <%
    }
    if (!login_dto.getIsUser() && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT)
    {
    %>
    <div class="fl_left"><a href="../client/listClient.do?list_all=1&parentId=<%=login_dto.getId()%>">|&nbsp;Clients&nbsp;</a></div>
    <%
    }
    else if(login_dto.getIsUser() && ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (perDTO != null && perDTO.CLIENT)))
    {
    %>
    <div class="fl_left"><a href="../client/listClient.do?list_all=1&parentId=0">|&nbsp;Clients&nbsp;</a></div>
    <%
    }
    if (!login_dto.getIsUser() || (login_dto.getIsUser() && ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (perDTO != null && perDTO.REFILL))))
    {
    %>
    <div class="fl_left"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=5">|&nbsp;Refill&nbsp;Details&nbsp;</a></div>
    <%
    }
    %>
    <div class="fl_left"><a href="../login/logout.do" target="_self">|&nbsp;Logout</a></div>
</div>
<div class="height-5px">User:&nbsp;<%=login_dto.getClientId()%></div>    
</div>

