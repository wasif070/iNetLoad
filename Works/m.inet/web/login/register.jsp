<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Register</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/login-header.jsp"%></div>
            <div class="body_content fl_left">                
                <div class="body">
                    <html:form action="/client/addSpecialAgent" method="post" >
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">Registration For Special Agent</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center">
                                        <bean:write name="ClientForm" property="message" filter="false"/>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 4px;">Client ID</th>
                                    <td valign="top" style="padding-top: 2px;">
                                        <html:text property="clientId"  styleId="clientId" />
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Name</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientName" styleId="clientName"  />
                                        <html:messages id="clientName" property="clientName">
                                            <bean:write name="clientName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Email</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientEmail" styleId="clientEmail"  />
                                        <html:messages id="clientEmail" property="clientEmail">
                                            <bean:write name="clientEmail"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientPassword" styleId="clientPassword"  />
                                        <html:messages id="clientPassword" property="clientPassword">
                                            <bean:write name="clientPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="retypePassword" styleId="retypePassword"  />
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <td colspan="2" style="padding-top: 10px">
                                        <input type="hidden" name="clientVersion" value="0" />
                                        <input type="hidden" name="clientRegister" value="1" />
                                        <input type="hidden" name="searchLink" value="nai" />                                        
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <div style="margin-left: 40px;margin-top: 7px; margin-bottom: 5px;">
                                            <input name="submit" type="submit" class="custom-button" value="Register"/>
                                            <input name="submit" type="submit" class="custom-button" value="Cancel"/>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>                        
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>            
        </div>        
    </body>
</html>