<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Login</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/login-header.jsp"%></div>
            <div class="body_content fl_left" style="height: 100%; width: 100%;">
                <div class="body">
                    <html:form action="/login/login" method="post">
                        <table class="login_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2"><B>Login to <%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%></B></th>
                                </tr>                                
                            </thead>
                            <tbody>
                                <tr>
                                    <th>User ID</th>
                                    <td>
                                        <html:text property="loginId"  />
                                        <html:messages id="loginId" property="loginId">
                                            <bean:write name="loginId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Password</th>
                                    <td>
                                        <html:password property="loginPass" />
                                        <html:messages id="loginPass" property="loginPass">
                                            <bean:write name="loginPass"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="hidden" name="clientVersion" value="0" />
                                        <html:hidden property="doValidation" value="1" />
                                        <div class="full-div" style="margin-left: 50px; display: block; width: 150px !impotant;">
                                            <input name="submit" type="submit" class="custom-button" style="float:left;" value="Login" />
                                            <input type="reset" class="custom-button" style="margin-left:5px;float:left" value="Reset" />                                         
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center"><bean:write name="LoginForm" property="message" filter="false"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center"  style="padding-bottom: 10px;">
                                       <a href="../login/register.jsp"><font style="color: blue;text-decoration: underline; font-family: Tahoma, Geneva, sans-serif;">Create A Special Agent For You!!!</font></a><BR> 
				       <a href="../iNetLoad.jar"><font style="color: blue;text-decoration: underline; font-family: Tahoma, Geneva, sans-serif;">Download <%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> FlexiDialer</font></a><BR>
				       <a href="../opera.jar"><font style="color: blue;text-decoration: underline; font-family: Tahoma, Geneva, sans-serif;">Download Opera Mini</font></a>
				    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>            
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>        
    </body>
</html>