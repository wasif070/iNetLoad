package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListClientAction
        extends Action {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            ClientForm clientForm = (ClientForm) form;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == clientForm.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == clientForm.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == clientForm.getParentId()))) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                ClientTaskSchedular scheduler = new ClientTaskSchedular();
                if (list_all == 0) {
                    if (clientForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.CLIENT_RECORD_PER_PAGE, clientForm.getRecordPerPage());
                    }
                    ClientDTO cldto = new ClientDTO();
                    if (clientForm.getClientId() != null && clientForm.getClientId().trim().length() > 0) {
                        cldto.searchWithClientID = true;
                        cldto.setClientId(clientForm.getClientId().toLowerCase());
                    }
                    if (clientForm.getClientTypeId() > 0) {
                        cldto.searchWithType = true;
                        cldto.setClientTypeId(clientForm.getClientTypeId());
                    }
                    clientForm.setClientList(scheduler.getClientDTOsWithSearchParam(cldto, clientForm.getParentId()));
                } else if (list_all == 2) {
                    if (request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE) != null) {
                        clientForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, clientForm.getMessage());
                    clientForm.setClientList(scheduler.getClientDTOsSorted(clientForm.getParentId()));
                } else {
                    if (request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE) != null) {
                        clientForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.CLIENT_RECORD_PER_PAGE).toString()));
                    }
                    clientForm.setClientList(scheduler.getClientDTOs(clientForm.getParentId()));
                }
                clientForm.setSelectedIDs(null);
                if (clientForm.getClientList() != null && clientForm.getClientList().size() > 0 && clientForm.getClientList().size() <= (clientForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
