package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import org.apache.log4j.Logger;

public class EditClientAction
        extends Action {

    static Logger logger = Logger.getLogger(EditClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ClientForm formBean = (ClientForm) form;
            PermissionDTO userPerDTO = null;
            String submitVal = request.getParameter("submit");
            if (submitVal.equals("Update")) {
                if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT_EDIT)
                        || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == formBean.getParentId())
                        || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == formBean.getParentId())
                        || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == formBean.getParentId())
                        || login_dto.getId() == formBean.getId()) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                    if ((formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER)
                            || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID)
                            || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3)) {
                        target = "failure";
                        return (mapping.findForward(target));
                    }
                    MyAppError error = new MyAppError();
                    ClientDTO dto = new ClientDTO();
                    ClientTaskSchedular scheduler = new ClientTaskSchedular();
                    dto.setId(formBean.getId());
                    dto.setClientId(formBean.getClientId());
                    dto.setClientPassword(formBean.getClientPassword());
                    dto.setRetypePassword(formBean.getRetypePassword());
                    dto.setClientStatus(formBean.getClientStatus());
                    dto.setParentId(formBean.getParentId());
                    dto.setClientTypeId(formBean.getClientTypeId());
                    error = scheduler.editClientInformation(dto, login_dto);
                    if (error.getErrorType() > 0) {
                        target = "failure";
                        formBean.setMessage(true, error.getErrorMessage());
                    } else if (request.getParameter("searchLink") != null) {
                        formBean.setMessage(false, "Client is updated successfully!!!");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                        ActionForward changedActionForward = new ActionForward("../client/listClient.do?list_all=1&parentId=" + dto.getParentId(), true);
                        return changedActionForward;
                    } else {
                        ActionForward changedActionForward = new ActionForward("../home/home.do", true);
                        return changedActionForward;
                    }
                }
            } else if (submitVal.equals("Recharge")) {
                MyAppError error = new MyAppError();
                double amount = 0.0;
                try {
                    amount = Double.parseDouble(request.getParameter("amount"));
                } catch (Exception e) {
                    target = "failure";
                    error.setErrorType(MyAppError.ValidationError);
                    formBean.setMessage(true, "Invalid amount.");
                }
                if(amount < 0.0)
                {
                    target = "failure";
                    error.setErrorType(MyAppError.ValidationError);
                    formBean.setMessage(true, "Invalid amount.");                    
                }    
                else {
                    if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT_RECHARGE)
                            || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == formBean.getParentId())
                            || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == formBean.getParentId())
                            || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == formBean.getParentId())
                            || login_dto.getId() == formBean.getId()) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                        if ((formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER)
                                || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID)
                                || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3)) {
                            target = "failure";
                            return (mapping.findForward(target));
                        }
                        ClientDTO dto = new ClientDTO();
                        ClientTaskSchedular scheduler = new ClientTaskSchedular();
                        dto.setId(formBean.getId());
                        dto.setParentId(formBean.getParentId());

                        if (error.getErrorType() == MyAppError.NoError) {
                            String description = "";
                            if (request.getParameter("description") != null) {
                                description = request.getParameter("description");
                            }
                            error = scheduler.rechargeClient(login_dto, dto.getId(), amount, description);
                            if (error.getErrorType() > 0) {
                                target = "failure";
                                formBean.setMessage(true, error.getErrorMessage());
                            } else {
                                formBean.setMessage(false, "Your request will be processed. Please wait!!!");
                                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                                ActionForward changedActionForward = new ActionForward("../client/listClient.do?list_all=1&parentId=" + dto.getParentId(), true);
                                return changedActionForward;
                            }
                        }
                    }
                } 
            }else if (submitVal.equals("Return")) {
                MyAppError error = new MyAppError();
                double amount = 0.0;
                try {
                    amount = Double.parseDouble(request.getParameter("amount"));
                } catch (Exception e) {
                    target = "failure";
                    error.setErrorType(MyAppError.ValidationError);
                    formBean.setMessage(true, "Invalid amount.");
                }
                if(amount < 0.0)
                {
                    target = "failure";
                    error.setErrorType(MyAppError.ValidationError);
                    formBean.setMessage(true, "Invalid amount.");                    
                }    
                else {
                    if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT_RETURN)
                            || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == formBean.getParentId())
                            || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == formBean.getParentId())
                            || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == formBean.getParentId())
                            || login_dto.getId() == formBean.getId()) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                        if ((formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER)
                                || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID)
                                || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3)) {
                            target = "failure";
                            return (mapping.findForward(target));
                        }
                        ClientDTO dto = new ClientDTO();
                        ClientTaskSchedular scheduler = new ClientTaskSchedular();
                        dto.setId(formBean.getId());
                        dto.setParentId(formBean.getParentId());
                        if (error.getErrorType() == MyAppError.NoError) {
                            String description = "";
                            if (request.getParameter("description") != null) {
                                description = request.getParameter("description");
                            }
                            error = scheduler.returnClient(login_dto, dto.getId(), amount, description);
                            if (error.getErrorType() > 0) {
                                target = "failure";
                                formBean.setMessage(true, error.getErrorMessage());
                            } else {
                                formBean.setMessage(false, "Your request will be processed. Please wait!!!");
                                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                                ActionForward changedActionForward = new ActionForward("../client/listClient.do?list_all=1&parentId=" + dto.getParentId(), true);
                                return changedActionForward;
                            }
                        }
                    }
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
