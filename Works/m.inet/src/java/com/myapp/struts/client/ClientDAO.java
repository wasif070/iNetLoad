package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.payment.PaymentDAO;
import com.myapp.struts.payment.PaymentDTO;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import sendmail.SimpleMail;
import org.apache.log4j.Logger;

public class ClientDAO {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ClientDAO() {
    }
    
    public MyAppError addClientInformation(ClientDTO cl_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        PaymentDAO dao = new PaymentDAO();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select user_id from user where user_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, cl_dto.getClientId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_deleted=0 and client_id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, cl_dto.getClientId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();


            if (cl_dto.getParentId() == 0 || getClientCredit(cl_dto.getParentId()) >= cl_dto.getClientCredit()) {
                sql = "insert into client (client_id,client_type_id,client_parent_id,client_password,client_status,client_reg_time,client_version) ";
                sql += "values(?,?,?,?,?,?,?)";
                ps3 = dbConnection.connection.prepareStatement(sql);

                ps3.setString(1, cl_dto.getClientId());
                ps3.setInt(2, cl_dto.getClientTypeId());
                ps3.setLong(3, cl_dto.getParentId());
                ps3.setString(4, cl_dto.getClientPassword());
                ps3.setInt(5, cl_dto.getClientStatus());
                ps3.setLong(6, System.currentTimeMillis());
                ps3.setInt(7, cl_dto.getClientVersion());
                ps3.executeUpdate();
                ClientLoader.getInstance().forceReload();
                if (cl_dto.getClientCredit() != 0.0) {
                    sql = "select id from client where client_id='" + cl_dto.getClientId() + "'";
                    Statement stmt = dbConnection.connection.createStatement();
                    ResultSet rs = stmt.executeQuery(sql);

                    if (rs.next()) {
                        PaymentDTO dto = new PaymentDTO();
                        if (l_dto.getIsUser()) {
                            if (cl_dto.getParentId() > Constants.ROOT_RESELLER_PARENT_ID) {
                                dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(cl_dto.getParentId()).getClientTypeId());
                                dto.setPaymentFromClientId(cl_dto.getParentId());
                            } else {
                                dto.setPaymentFromClientTypeId(Constants.USER_TYPE);
                                dto.setPaymentFromClientId(Constants.ROOT_RESELLER_PARENT_ID);
                            }
                        } else {
                            dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(cl_dto.getParentId()).getClientTypeId());
                            dto.setPaymentFromClientId(cl_dto.getParentId());
                        }

                        dto.setPaymentToClientTypeId(cl_dto.getClientTypeId());
                        dto.setPaymentToClientId(rs.getLong("id"));
                        dto.setPaymentTypeId(Constants.PAYMENT_TYPE_INITIAL);
                        dto.setPaymentAmount(cl_dto.getClientCredit());
                        dto.setPaymentDescription(Constants.INITIAL_CREDIT);
                        dto.setPaymentDiscount((dto.getPaymentAmount()*cl_dto.getClientDiscount())/100.00);
                        error = dao.addRechargePaymentRequest(dto);
                    }
                    rs.close();
                }
            } else {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Insufficient Parent Balance.");
                return error;
            }
        } catch (Exception ex) {
            logger.fatal("Error while adding client: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }            
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
 
    public MyAppError addSpecialAgentInformation(ClientDTO cl_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select user_id from user where user_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, cl_dto.getClientId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_deleted=0 and client_id=?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, cl_dto.getClientId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into client (client_id,client_type_id,client_parent_id,client_full_name,client_email,client_password,client_status,client_reg_time,client_ip) ";
            sql += "values(?,?,?,?,?,?,?,?,?)";
            ps3 = dbConnection.connection.prepareStatement(sql);

            ps3.setString(1, cl_dto.getClientId());
            ps3.setInt(2, Constants.CLIENT_TYPE_AGENT);
            ps3.setLong(3, Constants.ROOT_RESELLER_PARENT_ID);
            ps3.setString(4, cl_dto.getClientName());
            ps3.setString(5, cl_dto.getClientEmail());
            ps3.setString(6, cl_dto.getClientPassword());
            ps3.setInt(7, Constants.USER_STATUS_ACTIVE);
            ps3.setLong(8, System.currentTimeMillis());
            ps3.setString(9, "");
            ps3.executeUpdate();
            ClientLoader.getInstance().forceReload();
            try {
                SimpleMail.sendMail(SettingsLoader.getInstance().getSettingsDTO().getContactEmail(), SettingsLoader.getInstance().getSettingsDTO().getBrandName(), cl_dto.getClientEmail(), "Your Special Agent Account Info",
                        "Your registration for special agent is successful. Your Client ID: " + cl_dto.getClientId() + " and Password: " + cl_dto.getClientPassword(), null);
            } catch (Exception e) {
                logger.fatal("Exception in sendingg mail:" + e);
            }
        } catch (Exception ex) {
            logger.fatal("Error while adding client: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            } 
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }    

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ArrayList<ClientDTO> list, ClientDTO cldto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ClientDTO dto = (ClientDTO) i.next();
                if ((cldto.searchWithClientID && !dto.getClientId().toLowerCase().startsWith(cldto.getClientId()))
                        || (cldto.searchWithType && dto.getClientTypeId() != cldto.getClientTypeId())
                        || (cldto.searchWithStatus && dto.getClientStatus() != cldto.getClientStatus())) {
                    continue;
                }

                switch (cldto.getSign()) {
                    case Constants.BALANCE_EQUAL:
                        if (dto.getClientCredit() == cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    case Constants.BALANCE_SMALLER_THAN:
                        if (dto.getClientCredit() < cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    case Constants.BALANCE_GREATER_THAN:
                        if (dto.getClientCredit() > cldto.getClientCredit()) {
                            newList.add(dto);
                        }
                        break;
                    default:
                        newList.add(dto);
                        break;
                }
            }
        }
        return newList;
    }

    public ArrayList<ClientDTO> getClientDTOsSorted(ArrayList<ClientDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public ArrayList<ClientDTO> getClientDTOsSortedByClientID(ArrayList<ClientDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    return dto1.getClientId().compareTo(dto2.getClientId());
                }
            });
        }
        return list;
    }

    public MyAppError editClientInformation(ClientDTO cl_dto, LoginDTO login_dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        PreparedStatement ps4 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select user_id from user where user_id=?";
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, cl_dto.getClientId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_deleted=0 and client_id=? and id!=" + cl_dto.getId();
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, cl_dto.getClientId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            if (login_dto.getIsUser() || cl_dto.getId() != login_dto.getId()) {
                sql = "update client set client_id=?,client_password=? where id=" + cl_dto.getId();
                ps3 = dbConnection.connection.prepareStatement(sql);

                ps3.setString(1, cl_dto.getClientId());
                ps3.setString(2, cl_dto.getClientPassword());
                ps3.executeUpdate();
                String list = "" + cl_dto.getId();
                if (cl_dto.getClientStatus() != ClientLoader.getInstance().getClientDTOByID(cl_dto.getId()).getClientStatus()) {
                    switch (cl_dto.getClientStatus()) {
                        case Constants.USER_STATUS_ACTIVE:
                            activateClients(list);
                            break;
                        case Constants.USER_STATUS_INACTIVE:
                            deactivateClients(list);
                            break;
                        case Constants.USER_STATUS_BLOCK:
                            blockClients(list);
                            break;
                        default:
                            break;
                    }
                }
            } else {
                sql = "update client set client_id=?,client_password=? where id=" + cl_dto.getId();
                ps4 = dbConnection.connection.prepareStatement(sql);

                ps4.setString(1, cl_dto.getClientId());
                ps4.setString(2, cl_dto.getClientPassword());
                ps4.executeUpdate();
            }

            ClientLoader.getInstance().forceReload();
            if (!login_dto.getIsUser() && cl_dto.getId() == login_dto.getId()) {
                login_dto.setClientId(cl_dto.getClientId());
            }
        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps4 != null) {
                    ps4.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public double getClientCredit(long id) {
        String sql = "select client_credit_all from client where id=" + id;
        double credit = 0.0;
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                credit = resultSet.getDouble("client_credit_all");
            }
            resultSet.close();
        } catch (Exception ex) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return credit;
    }

    public String getParentChildrenList(String parentList) {
        ArrayList<ClientDTO> allClientList = ClientLoader.getInstance().getClientDTOList();
        String parentChildrenList = "," + parentList + ",";
        if (allClientList != null) {
            for (int i = 0; i < allClientList.size(); i++) {
                ClientDTO dto = allClientList.get(i);
                if (parentChildrenList.contains("," + dto.getParentId() + ",")) {
                    parentChildrenList += dto.getId() + ",";
                }
            }
        }
        return parentChildrenList.substring(parentChildrenList.indexOf(",") + 1, parentChildrenList.lastIndexOf(","));
    }

    public MyAppError deactivateClients(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update client set client_status=" + Constants.USER_STATUS_INACTIVE + " where id in(" + getParentChildrenList(list) + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating user status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError activateClients(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update client set client_status = " + Constants.USER_STATUS_ACTIVE + " where id in(" + getParentChildrenList(list) + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating user status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError blockClients(String list) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update client set client_status=" + Constants.USER_STATUS_BLOCK + " where id in(" + getParentChildrenList(list) + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            ClientLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while updating user status: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError rechargeClient(LoginDTO login_dto, long id, double amount, String description) {
        MyAppError error = new MyAppError();
        PaymentDAO dao = new PaymentDAO();
        PaymentDTO dto = new PaymentDTO();
        ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(id);
        if (login_dto.getIsUser()) {
            if (clientDTO.getParentId() > Constants.ROOT_RESELLER_PARENT_ID) {
                dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
                dto.setPaymentFromClientId(clientDTO.getParentId());
            } else {
                dto.setPaymentFromClientTypeId(Constants.USER_TYPE);
                dto.setPaymentFromClientId(Constants.ROOT_RESELLER_PARENT_ID);
            }
        } else {
            dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
            dto.setPaymentFromClientId(clientDTO.getParentId());
        }

        dto.setPaymentToClientTypeId(clientDTO.getClientTypeId());
        dto.setPaymentToClientId(id);
        dto.setPaymentTypeId(Constants.PAYMENT_TYPE_PAID);
        dto.setPaymentAmount(amount);
        dto.setPaymentDiscount((dto.getPaymentAmount() * clientDTO.getClientDiscount()) / 100.00);
        dto.setPaymentDescription(description);
        error = dao.addRechargePaymentRequest(dto);
        if (error.getErrorType() != MyAppError.NoError) {
            return error;
        }
        return error;
    }

    public MyAppError returnClient(LoginDTO login_dto, long id, double amount, String description) {
        MyAppError error = new MyAppError();
        PaymentDAO dao = new PaymentDAO();
        PaymentDTO dto = new PaymentDTO();
        ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(id);
        if (login_dto.getIsUser()) {
            if (clientDTO.getParentId() > Constants.ROOT_RESELLER_PARENT_ID) {
                dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
                dto.setPaymentFromClientId(clientDTO.getParentId());
            } else {
                dto.setPaymentFromClientTypeId(Constants.USER_TYPE);
                dto.setPaymentFromClientId(Constants.ROOT_RESELLER_PARENT_ID);
            }
        } else {
            dto.setPaymentFromClientTypeId(ClientLoader.getInstance().getClientDTOByID(clientDTO.getParentId()).getClientTypeId());
            dto.setPaymentFromClientId(clientDTO.getParentId());
        }

        dto.setPaymentToClientTypeId(clientDTO.getClientTypeId());
        dto.setPaymentToClientId(id);
        dto.setPaymentTypeId(Constants.PAYMENT_TYPE_RETURNED);
        dto.setPaymentAmount(amount);
        dto.setPaymentDescription(description);
        error = dao.addReturnPaymentRequest(dto);
        if (error.getErrorType() != MyAppError.NoError) {
            return error;
        }
        return error;
    }

    public long countPendingRequests(LoginDTO login_dto) {
        long totalPendingReqequests = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        String sql = null;

        if (login_dto.getIsUser()) {
            String list = "-1";
            ArrayList<ClientDTO> rootChildList = ClientLoader.getInstance().getAllClientDTOsByParentID(Constants.ROOT_RESELLER_PARENT_ID);
            if (rootChildList != null) {
                for (int i = 0; i < rootChildList.size(); i++) {
                    list += "," + rootChildList.get(i).getId();
                }
            }
            PermissionDTO userPerDTO = null;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                sql = "select count(*) as total from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
            } else if ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null) {
                String condition = " and operator_type_id in(-1";
                if (userPerDTO.REFILL_GP) {
                    condition += "," + Constants.GP;
                }
                if (userPerDTO.REFILL_BL) {
                    condition += "," + Constants.BL;
                }
                if (userPerDTO.REFILL_RB) {
                    condition += "," + Constants.RB;
                }
                if (userPerDTO.REFILL_CC) {
                    condition += "," + Constants.CC;
                }
                if (userPerDTO.REFILL_TT) {
                    condition += "," + Constants.TT;
                }
                if (userPerDTO.REFILL_WR) {
                    condition += "," + Constants.WR;
                }
                condition += ") ";
                sql = "select count(*) as total from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")" + condition;
            }

        } else if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) {
            String list = "-1";
            ArrayList<ClientDTO> childList = ClientLoader.getInstance().getAllClientDTOsByParentID(login_dto.getId());
            if (childList != null) {
                for (int i = 0; i < childList.size(); i++) {
                    list += "," + childList.get(i).getId();
                }
            }
            sql = "select count(*) as total from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id in(" + list + ")";
        } else {
            sql = "select count(*) as total from inetload_fdr where inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + " and inetload_to_user_id=" + login_dto.getId();
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                totalPendingReqequests = rs.getLong("total");
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in countPendingRequests: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalPendingReqequests;
    }
}
