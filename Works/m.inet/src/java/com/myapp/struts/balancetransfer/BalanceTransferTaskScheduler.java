package com.myapp.struts.balancetransfer;

import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class BalanceTransferTaskScheduler {

    public BalanceTransferTaskScheduler() {
    }

    public MyAppError addBTSurcharge(BalanceTransferDTO btdto) {
        BalanceTransferDAO balanceTransferDAO = new BalanceTransferDAO();
        return balanceTransferDAO.addBTSurcharge(btdto);
    }

    public MyAppError editBTSurcharge(BalanceTransferDTO btdto) {
        BalanceTransferDAO balanceTransferDAO = new BalanceTransferDAO();
        return balanceTransferDAO.editBTSurcharge(btdto);
    }

    public MyAppError deleteBTSurcharges(String list) {
        BalanceTransferDAO balanceTransferDAO = new BalanceTransferDAO();
        return balanceTransferDAO.deleteBTSurcharges(list);
    }

    public ArrayList<BalanceTransferDTO> getBTSurchargeDTOsWithSearchParam(BalanceTransferDTO btdto) {
        BalanceTransferDAO balanceTransferDAO = new BalanceTransferDAO();
        ArrayList<BalanceTransferDTO> list = BalanceTransferLoader.getInstance().getBTSurchargeDTOList();
        if(list!=null)
        {
          return balanceTransferDAO.getBTSurchargeDTOsWithSearchParam((ArrayList<BalanceTransferDTO>) list.clone(),btdto);
        }
        return null;
    }

    public ArrayList<BalanceTransferDTO> getBTSurchargeDTOsSorted() {
        BalanceTransferDAO balanceTransferDAO = new BalanceTransferDAO();
        ArrayList<BalanceTransferDTO> list = BalanceTransferLoader.getInstance().getBTSurchargeDTOList();
        if(list!=null)
        {
          return balanceTransferDAO.getBTSurchargeDTOsSorted((ArrayList<BalanceTransferDTO>) list.clone());
        }
        return null;         
    }

    public ArrayList<BalanceTransferDTO> getBTSurchargeDTOs() {
        return BalanceTransferLoader.getInstance().getBTSurchargeDTOList();
    }

    public BalanceTransferDTO getBTSurchargeDTO(long id) {
        return BalanceTransferLoader.getInstance().getBTSurchargeDTOByID(id);
    }
}
