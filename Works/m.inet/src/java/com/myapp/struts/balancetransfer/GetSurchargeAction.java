package com.myapp.struts.balancetransfer;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetSurchargeAction
        extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            BalanceTransferForm formBean = (BalanceTransferForm) form;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                long id = Long.parseLong(request.getParameter("id"));
                BalanceTransferDTO dto = new BalanceTransferDTO();
                BalanceTransferTaskScheduler scheduler = new BalanceTransferTaskScheduler();
                dto = scheduler.getBTSurchargeDTO(id);

                if (dto != null) {
                    formBean.setId(dto.getId());
                    formBean.setTypeID(dto.getTypeID());
                    formBean.setMaxTransferredAmount(dto.getMaxTransferredAmount());
                    formBean.setMinTransferredAmount(dto.getMinTransferredAmount());
                    formBean.setSurchargeAmount(dto.getSurchargeAmount());
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
