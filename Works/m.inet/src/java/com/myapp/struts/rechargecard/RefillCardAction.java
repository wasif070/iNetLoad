package com.myapp.struts.rechargecard;

import com.myapp.struts.client.ClientDAO;
import com.myapp.struts.flexi.FlexiDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.MyAppError;

public class RefillCardAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);

        if (login_dto != null && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && login_dto.getIsSpecialAgent() && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            RechargecardForm formBean = (RechargecardForm) form;
            request.setAttribute(mapping.getAttribute(), formBean);
            String prefix = formBean.getPhoneNumber().substring(0, 3);
            int operatorTypeID = 0;
            for (int i = 1; i < Constants.OPERATOR_TYPE_PREFIX.length ; i++) {
                if (prefix.equals(Constants.OPERATOR_TYPE_PREFIX[i])) {
                    operatorTypeID = i;
                    break;
                }
            }
            if (operatorTypeID < Constants.GP) {
                target = "failure";
                formBean.setMessage(true, "Enter Valid Phone Number.");
                return mapping.findForward(target);
            }

            FlexiDAO flDAO = new FlexiDAO();
            long curTime = System.currentTimeMillis();
            long lastRefillTime = flDAO.getLastRefillTime(formBean.getPhoneNumber(), curTime);
            if (lastRefillTime > 0) {
                long refillInterval = curTime - lastRefillTime;
                if (refillInterval < (SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000)) {
                    target = "failure";
                    int minute = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000) - refillInterval) / 1000) / 60);
                    int sec = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000) - refillInterval) / 1000) % 60);
                    formBean.setMessage(true, "You have already sent a request to same number.<br>Please wait " + minute + " minutes " + sec + " seconds");
                    return mapping.findForward(target);
                }
            }

            RechargecardDTO dto = new RechargecardDTO();
            dto.setSerialNo(formBean.getSerialNo());
            dto.setCardNo(formBean.getCardNo());
            dto.setPhoneNumber(formBean.getPhoneNumber());
            dto.setOperatorTypeID(operatorTypeID);
            dto.setRefillType(formBean.getRefillType());
            RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
            MyAppError error = scheduler.refillByCard(dto, login_dto);
            if (error.getErrorType() > MyAppError.NoError) {
                if (error.getErrorType() == MyAppError.OtherError && login_dto.getWrongEntry() >= 3) {
                    target = "index";
                    ClientDAO dao = new ClientDAO();
                    dao.blockClients(String.valueOf(login_dto.getId()));
                } else {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                }
            } else {
                request.getSession(true).setAttribute(Constants.REFILLBYCARD_SUCCESS_DTO, dto);
            }
        } else {
            target = "index";
        }
        return mapping.findForward(target);
    }
}
