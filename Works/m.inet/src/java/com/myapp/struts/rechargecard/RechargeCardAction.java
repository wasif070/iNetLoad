package com.myapp.struts.rechargecard;

import com.myapp.struts.client.ClientDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;

public class RechargeCardAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);

        if (login_dto != null && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && login_dto.getIsSpecialAgent() && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            RechargecardForm formBean = (RechargecardForm) form;
            request.setAttribute(mapping.getAttribute(), formBean);

            RechargecardDTO dto = new RechargecardDTO();
            dto.setSerialNo(formBean.getSerialNo());
            dto.setCardNo(formBean.getCardNo());
            RechargecardGroupTaskSchedular scheduler = new RechargecardGroupTaskSchedular();
            MyAppError error = scheduler.rechargeByCard(dto, login_dto);
            if (error.getErrorType() > MyAppError.NoError) {
                if(error.getErrorType() == MyAppError.OtherError && login_dto.getWrongEntry() >= 3)
                {
                    target = "index";
                    ClientDAO dao = new ClientDAO();
                    dao.blockClients(String.valueOf(login_dto.getId()));
                }   
                else
                {    
                  target = "failure";
                  formBean.setMessage(true, error.getErrorMessage());
                }  
            }
        } else {
            target = "index";
        }
        return mapping.findForward(target);
    }
}
