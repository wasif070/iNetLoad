package com.myapp.struts.rechargecard;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class RechargecardForm extends org.apache.struts.action.ActionForm {

    private int doValidate;
    private int sign;
    private int groupCountry;
    private int groupPackage;
    private int pageNo;
    private int recordPerPage;
    private int cardStatus;
    private int refillType;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;    
    private long id;
    private long activatedAt;
    private long blockedAt;
    private long cardShop;
    private double groupAmount;
    private double cardPrice;
    private String blockedBy;
    private String activatedBy;
    private String phoneNumber;
    private String serialNo;
    private String cardNo;
    private String cardStatusName;
    private String groupIdentifier;
    private String groupCountryName;
    private String groupPackageName;
    private String groupName;
    private String cardShopName;
    private String message;
    private String activatedAtStr;
    private String blockedAtStr;
    private String cardStatusStr;
    private long[] selectedIDs;
    private ArrayList cardList;
    private ArrayList cardUsageList;
    private ArrayList cardSummeryList;
    public static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public RechargecardForm() {
        super();
        groupCountry = -1;
        groupPackage = -1;
        cardStatus = -1;
        groupIdentifier = null;
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis());
        String dateParts1[] = Utils.getDateParts(System.currentTimeMillis()-(24*60*60*1000));

        this.startDay = Integer.parseInt(dateParts1[0]);
        this.startMonth = Integer.parseInt(dateParts1[1]);
        this.startYear = Integer.parseInt(dateParts1[2]);
        this.endDay = Integer.parseInt(dateParts[0]);
        this.endMonth = Integer.parseInt(dateParts[1]);
        this.endYear = Integer.parseInt(dateParts[2]);        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
  
   public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }    

    public int getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(int cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardStatusStr() {
        return cardStatusStr;
    }

    public void setCardStatusStr(int cardStatus) {
        this.cardStatusStr = Constants.LIVE_STATUS_STRING[cardStatus];
    }

    public int getGroupCountry() {
        return groupCountry;
    }

    public void setGroupCountry(int groupCountry) {
        this.groupCountry = groupCountry;
    }

    public int getGroupPackage() {
        return groupPackage;
    }

    public void setGroupPackage(int groupPackage) {
        this.groupPackage = groupPackage;
    }
    
    public String getCardShopName() {
        return cardShopName;
    }

    public void setCardShopName(String cardShopName) {
        this.cardShopName = cardShopName;
    }      

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public String getActivatedBy() {
        return activatedBy;
    }

    public void setActivatedBy(String activatedBy) {
        this.activatedBy = activatedBy;
    }

    public String getBlockedBy() {
        return blockedBy;
    }

    public void setBlockedBy(String blockedBy) {
        this.blockedBy = blockedBy;
    }

    public long getActivatedAt() {
        return activatedAt;
    }

    public void setActivatedAt(long activatedAt) {
        this.activatedAt = activatedAt;
    }

    public long getBlockedAt() {
        return blockedAt;
    }

    public void setBlockedAt(long blockedAt) {
        this.blockedAt = blockedAt;
    }

    public String getActivatedAtStr() {
        if (activatedAt > 0) {
            return dateFormat.format(activatedAt);
        } else {
            return " ";
        }
    }

    public void setActivatedAtStr(String activatedAtStr) {
        this.activatedAtStr = activatedAtStr;
    }

    public String getBlockedAtStr() {
        if (blockedAt > 0) {
            return dateFormat.format(blockedAt);
        } else {
            return " ";
        }
    }

    public void setBlockedAtStr(String blockedAtStr) {
        this.blockedAtStr = blockedAtStr;
    }

    public double getCardPrice() {
        return cardPrice;
    }

    public void setCardPrice(double cardPrice) {
        this.cardPrice = cardPrice;
    }

    public long getCardShop() {
        return cardShop;
    }

    public void setCardShop(long cardShop) {
        this.cardShop = cardShop;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardStatusName() {
        return cardStatusName;
    }

    public void setCardStatusName(String cardStatusName) {
        this.cardStatusName = cardStatusName;
    }

    public String getGroupIdentifier() {
        return groupIdentifier;
    }

    public void setGroupIdentifier(String groupIdentifier) {
        this.groupIdentifier = groupIdentifier;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public double getGroupAmount() {
        return groupAmount;
    }

    public void setGroupAmount(double groupAmount) {
        this.groupAmount = groupAmount;
    }

    public String getGroupCountryName() {
        return groupCountryName;
    }

    public void setGroupCountryName(String groupCountryName) {
        this.groupCountryName = groupCountryName;
    }

    public String getGroupPackageName() {
        return groupPackageName;
    }

    public void setGroupPackageName(String groupPackageName) {
        this.groupPackageName = groupPackageName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getRefillType() {
        return refillType;
    }

    public void setRefillType(int refillType) {
        this.refillType = refillType;
    }

    public String getMessage() {
        return message;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public ArrayList getCardList() {
        return cardList;
    }

    public void setCardList(ArrayList cardList) {
        this.cardList = cardList;
    }
    
    public ArrayList getCardUsageList() {
        return cardUsageList;
    }

    public void setCardUsageList(ArrayList cardUsageList) {
        this.cardUsageList = cardUsageList;
    } 
    
    public ArrayList getCardSummeryList() {
        return cardSummeryList;
    }

    public void setCardSummeryList(ArrayList cardSummeryList) {
        this.cardSummeryList = cardSummeryList;
    }     

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 10px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 10px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getSerialNo() == null || getSerialNo().length() < 1) {
                errors.add("serialNo", new ActionMessage("errors.serial_no.required"));
            }
            if (getCardNo() == null || getCardNo().length() < 1) {
                errors.add("cardNo", new ActionMessage("errors.card_no.required"));
            }
            if (request.getParameter("refillByCard") != null) {
                if (getPhoneNumber() == null || getPhoneNumber().length() == 0) {
                    errors.add("phoneNumber", new ActionMessage("errors.phnNumber.required"));
                } else if (getPhoneNumber().length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                    errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
                } else if (getPhoneNumber().length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                    String phoneNo = getPhoneNumber();
                    if (phoneNo.startsWith("+")) {
                        phoneNo = phoneNo.replace("+", "");
                    }
                    if (phoneNo.startsWith("88")) {
                        phoneNo = phoneNo.replace("88", "");
                    }
                    phoneNo = phoneNo.replace("-", "");
                    phoneNo = phoneNo.replace(" ", "");
                    if (phoneNo.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                        setPhoneNumber(phoneNo);
                    } else {
                        errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
                    }
                }
            }
        }
        return errors;
    }
}
