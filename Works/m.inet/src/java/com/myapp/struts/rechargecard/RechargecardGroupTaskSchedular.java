package com.myapp.struts.rechargecard;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;

public class RechargecardGroupTaskSchedular {

    public RechargecardGroupTaskSchedular() {
    }
    
    public MyAppError refillByCard(RechargecardDTO rc_dto, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.refillByCard(rc_dto, l_dto);
    }     
    
    public MyAppError rechargeByCard(RechargecardDTO rc_dto, LoginDTO l_dto) {
        RechargecardGroupDAO rcgDAO = new RechargecardGroupDAO();
        return rcgDAO.rechargeByCard(rc_dto, l_dto);
    }    
}
