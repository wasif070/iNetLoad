/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

/**
 *
 * @author user
 */
public class SMSDTO {

    private String message;
    private String phoneNumber;
    private String senderName;
    private int operatorTypeID;
    private double amount;

    public SMSDTO() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getOperatorTypeID() {
        return operatorTypeID;
    }

    public void setOperatorTypeID(int operatorTypeID) {
        this.operatorTypeID = operatorTypeID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
