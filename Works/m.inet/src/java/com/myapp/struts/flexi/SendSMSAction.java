/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.flexi;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Wasif Islam
 */
public class SendSMSAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            FlexiForm formBean = (FlexiForm) form;
            request.setAttribute(mapping.getAttribute(), formBean);
            int operatorTypeID = Constants.SMS;
            FlexiDTO dto = new FlexiDTO();
            dto.setSmsCountryCode(formBean.getSmsCountryCode().trim());
            dto.setPhoneNumber(formBean.getSmsCountryCode().trim() + formBean.getPhoneNumber().trim());
            dto.setOperatorTypeID(operatorTypeID);

            double amount = SMSDAO.getSMSRate(formBean.getSmsCountryCode());
            if (amount > 0) {
                double len = formBean.getSmsMessage().length();
                double count = Math.ceil(len / 140);

                formBean.setAmount(amount * count);
                dto.setAmount(formBean.getAmount());
                dto.setSmsSenderName(formBean.getSmsSenderName());
                dto.setSmsMessage(formBean.getSmsMessage());
                FlexiTaskScheduler scheduler = new FlexiTaskScheduler();
                MyAppError error = scheduler.sendSMSRequest(login_dto, dto);

                if (error.getErrorType() > MyAppError.NoError) {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());
                } else {
                    request.getSession(true).setAttribute(Constants.SMS_SUCCESS_DTO, dto);
                }
            } else {
                target = "failure";
                formBean.setMessage(true, "Message transfer is not allowed for this country code.");
                return mapping.findForward(target);
            }
        } else {
            target = "index";
        }

        return mapping.findForward(target);
    }
}
