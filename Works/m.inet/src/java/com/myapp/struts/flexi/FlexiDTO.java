package com.myapp.struts.flexi;

public class FlexiDTO {
    public boolean searchWithFromClientId = false;
    public boolean searchWithTransactionId = false;
    public boolean searchWithRequestId = false;
    public boolean searchWithPhoneNumber = false;
    public boolean searchWithAmount = false;
    private int sign;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int refillType;
    private int accountType;
    private int pageNo;
    private int recordPerPage;
    private int operatorTypeID;
    private int reportType;
    private double amount;
    private double surcharge;
    private String fromClientId;
    private String transactionId;
    private String phoneNumber;
    private String flexiRequestId;
    private String flexiPhoneNo;
    private String flexiFrom;
    private int flexiOpType;
    private int flexiTypeVal;
    private long flexiCardID;
    private long flexiAgentID;
    private long flexiContactID;
    private long flexiGroupID;
    private String flexiType;
    private double flexiAmount;
    private int flexiRequestType;
    private String flexiRequestTypeName;
    private long flexiRequestTimeVal;
    private long flexiSuccessTimeVal;
    private String flexiTransactionId;
    private String flexiMessage;
    private String smsMessage;
    private String smsSenderName;
    private String smsCountryCode;

    public FlexiDTO() {
        this.flexiCardID = 0;
        this.flexiContactID = 0;
        this.flexiGroupID = 0;
        this.surcharge = 0.0;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }
    
    public long getFlexiContactID() {
        return flexiContactID;
    }

    public void setFlexiContactID(long flexiContactID) {
        this.flexiContactID = flexiContactID;
    } 
    
    public long getFlexiGroupID() {
        return flexiGroupID;
    }

    public void setFlexiGroupID(long flexiGroupID) {
        this.flexiGroupID = flexiGroupID;
    }     
    
    public long getFlexiCardID() {
        return flexiCardID;
    }

    public void setFlexiCardID(long flexiCardID) {
        this.flexiCardID = flexiCardID;
    }     
    
    public int getFlexiOpType() {
        return flexiOpType;
    }

    public void setFlexiOpType(int flexiOpType) {
        this.flexiOpType = flexiOpType;
    }    

    public String getFlexiRequestId() {
        return flexiRequestId;
    }

    public void setFlexiRequestId(String flexiRequestId) {
        this.flexiRequestId = flexiRequestId;
    }

    public String getFlexiRequestTypeName() {
        return flexiRequestTypeName;
    }

    public void setFlexiRequestTypeName(String flexiRequestTypeName) {
        this.flexiRequestTypeName = flexiRequestTypeName;
    }

    public String getFlexiFrom() {
        return flexiFrom;
    }

    public void setFlexiFrom(String flexiFrom) {
        this.flexiFrom = flexiFrom;
    }

    public long getFlexiAgentID() {
        return flexiAgentID;
    }

    public void setFlexiAgentID(long flexiAgentID) {
        this.flexiAgentID = flexiAgentID;
    }

    public int getFlexiTypeVal() {
        return flexiTypeVal;
    }

    public void setFlexiTypeVal(int flexiTypeVal) {
        this.flexiTypeVal = flexiTypeVal;
    }

    public String getFlexiType() {
        return flexiType;
    }

    public void setFlexiType(String flexiType) {
        this.flexiType = flexiType;
    }

    public double getFlexiAmount() {
        return flexiAmount;
    }

    public void setFlexiAmount(double flexiAmount) {
        this.flexiAmount = flexiAmount;
    }

    public int getFlexiRequestType() {
        return flexiRequestType;
    }

    public void setFlexiRequestType(int flexiRequestType) {
        this.flexiRequestType = flexiRequestType;
    }

    public long getFlexiRequestTimeVal() {
        return flexiRequestTimeVal;
    }

    public void setFlexiRequestTimeVal(long flexiRequestTimeVal) {
        this.flexiRequestTimeVal = flexiRequestTimeVal;
    }

    public long getFlexiSuccessTimeVal() {
        return flexiSuccessTimeVal;
    }

    public void setFlexiSuccessTimeVal(long flexiSuccessTimeVal) {
        this.flexiSuccessTimeVal = flexiSuccessTimeVal;
    }

    public String getFlexiTransactionId() {
        return flexiTransactionId;
    }

    public void setFlexiTransactionId(String flexiTransactionId) {
        this.flexiTransactionId = flexiTransactionId;
    }

    public String getFlexiMessage() {
        return flexiMessage;
    }

    public void setFlexiMessage(String flexiMessage) {
        this.flexiMessage = flexiMessage;
    }

    public String getFlexiPhoneNo() {
        return flexiPhoneNo;
    }

    public void setFlexiPhoneNo(String flexiPhoneNo) {
        this.flexiPhoneNo = flexiPhoneNo;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getRefillType() {
        return refillType;
    }

    public void setRefillType(int refillType) {
        this.refillType = refillType;
    } 
    
    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }    

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public String getFromClientId() {
        return fromClientId;
    }

    public void setFromClientId(String fromClientId) {
        this.fromClientId = fromClientId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    public double getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(double surcharge) {
        this.surcharge = surcharge;
    }    

    public int getOperatorTypeID() {
        return operatorTypeID;
    }

    public void setOperatorTypeID(int operatorTypeID) {
        this.operatorTypeID = operatorTypeID;
    }

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public String getSmsSenderName() {
        return smsSenderName;
    }

    public void setSmsSenderName(String smsSenderName) {
        this.smsSenderName = smsSenderName;
    }

    public String getSmsCountryCode() {
        return smsCountryCode;
    }

    public void setSmsCountryCode(String smsCountryCode) {
        this.smsCountryCode = smsCountryCode;
    }
    
}
