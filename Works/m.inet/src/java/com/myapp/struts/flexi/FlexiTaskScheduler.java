package com.myapp.struts.flexi;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import java.util.ArrayList;

public class FlexiTaskScheduler {

    public MyAppError addFlexiRequest(LoginDTO login_dto, FlexiDTO dto) {
        FlexiDAO flexiDAO = new FlexiDAO();
        return flexiDAO.addFlexiRequest(login_dto, dto);
    }

    public ArrayList<FlexiDTO> getFlexiDTOs(LoginDTO login_dto, FlexiDTO fdto, int flexiType) {
        FlexiDAO flexiDAO = new FlexiDAO();
        return flexiDAO.getFlexiDTOs(login_dto, fdto, flexiType);
    }

    public MyAppError sendSMSRequest(LoginDTO login_dto, FlexiDTO dto) {
        SMSDAO smsDAO = new SMSDAO();
        return smsDAO.addSMSRequest(login_dto, dto);
    }

    /* public ArrayList<OperatorTypeDTO> getSummeryRefill(LoginDTO login_dto, FlexiDTO fdto) {
     FlexiDAO flexiDAO = new FlexiDAO();
     ArrayList<OperatorTypeDTO> data = null;
     if (fdto.getReportType() == 2) {
     data = flexiDAO.getRefillSummeryByAPI(login_dto, fdto);
     }        
     else if (fdto.getReportType() == 1) {
     data = flexiDAO.getRefillSummeryByDate(login_dto, fdto);
     } else {
     data = flexiDAO.getRefillSummery(login_dto, fdto);
     }
     return data;
     }*/
}
