package com.myapp.struts.flexi;

import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import java.util.ArrayList;
import org.apache.struts.action.ActionMessage;

public class FlexiForm extends org.apache.struts.action.ActionForm {

    private int sign;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int refillType;
    private int accountType;
    private int balanceTransfer;
    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private int reportType;
    private double amount;
    private String fromClientId;
    private String transactionId;
    private String phoneNumber;
    private String message;
    private ArrayList flexiList;
    private ArrayList flexiSummeryList;
    private String flexiRequestId;
    private String flexiPhoneNo;
    private String flexiFrom;
    private String flexiType;
    private double flexiAmount;
    private int flexiRequestType;
    private long flexiRequestTimeVal;
    private long flexiSuccessTimeVal;
    private String flexiTransactionId;
    private String flexiMessage;
    private String smsMessage;
    private String smsSenderName;
    private String smsCountryCode;
    private int sms;

    public FlexiForm() {
        super();
    }

    public ArrayList getFlexiSummeryList() {
        return flexiSummeryList;
    }

    public void setFlexiSummeryList(ArrayList flexiSummeryList) {
        this.flexiSummeryList = flexiSummeryList;
    }

    public int getReportType() {
        return reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    public String getFlexiRequestId() {
        return flexiRequestId;
    }

    public void setFlexiRequestId(String flexiRequestId) {
        this.flexiRequestId = flexiRequestId;
    }

    public String getFlexiFrom() {
        return flexiFrom;
    }

    public void setFlexiFrom(String flexiFrom) {
        this.flexiFrom = flexiFrom;
    }

    public String getFlexiType() {
        return flexiType;
    }

    public void setFlexiType(String flexiType) {
        this.flexiType = flexiType;
    }

    public double getFlexiAmount() {
        return flexiAmount;
    }

    public void setFlexiAmount(double flexiAmount) {
        this.flexiAmount = flexiAmount;
    }

    public int getFlexiRequestType() {
        return flexiRequestType;
    }

    public void setFlexiRequestType(int flexiRequestType) {
        this.flexiRequestType = flexiRequestType;
    }

    public long getFlexiRequestTimeVal() {
        return flexiRequestTimeVal;
    }

    public void setFlexiRequestTimeVal(long flexiRequestTimeVal) {
        this.flexiRequestTimeVal = flexiRequestTimeVal;
    }

    public long getFlexiSuccessTimeVal() {
        return flexiSuccessTimeVal;
    }

    public void setFlexiSuccessTimeVal(long flexiSuccessTimeVal) {
        this.flexiSuccessTimeVal = flexiSuccessTimeVal;
    }

    public String getFlexiTransactionId() {
        return flexiTransactionId;
    }

    public void setFlexiTransactionId(String flexiTransactionId) {
        this.flexiTransactionId = flexiTransactionId;
    }

    public String getFlexiMessage() {
        return flexiMessage;
    }

    public void setFlexiMessage(String flexiMessage) {
        this.flexiMessage = flexiMessage;
    }

    public String getFlexiPhoneNo() {
        return flexiPhoneNo;
    }

    public void setFlexiPhoneNo(String flexiPhoneNo) {
        this.flexiPhoneNo = flexiPhoneNo;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getBalanceTransfer() {
        return balanceTransfer;
    }

    public void setBalanceTransfer(int balanceTransfer) {
        this.balanceTransfer = balanceTransfer;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public int getRefillType() {
        return refillType;
    }

    public void setRefillType(int refillType) {
        this.refillType = refillType;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public String getFromClientId() {
        return fromClientId;
    }

    public void setFromClientId(String fromClientId) {
        this.fromClientId = fromClientId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public ArrayList getFlexiList() {
        return flexiList;
    }

    public void setFlexiList(ArrayList flexiList) {
        this.flexiList = flexiList;
    }

    public String getMessage() {
        return message;
    }

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }

    public String getSmsSenderName() {
        return smsSenderName;
    }

    public void setSmsSenderName(String smsSenderName) {
        this.smsSenderName = smsSenderName;
    }

    public String getSmsCountryCode() {
        return smsCountryCode;
    }

    public void setSmsCountryCode(String smsCountryCode) {
        this.smsCountryCode = smsCountryCode;
    }

    public int getSms() {
        return sms;
    }

    public void setSms(int sms) {
        this.sms = sms;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (this.getBalanceTransfer() == 1) {
                if (getPhoneNumber() == null || getPhoneNumber().length() == 0) {
                    errors.add("phoneNumber", new ActionMessage("errors.accountNumber.required"));
                } else if (getPhoneNumber().length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                    errors.add("phoneNumber", new ActionMessage("errors.invalidAccount"));
                } else if (getPhoneNumber().length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                    String phoneNo = getPhoneNumber();
                    if (phoneNo.startsWith("+")) {
                        phoneNo = phoneNo.replace("+", "");
                    }
                    if (phoneNo.startsWith("88")) {
                        phoneNo = phoneNo.replace("88", "");
                    }
                    phoneNo = phoneNo.replace("-", "");
                    phoneNo = phoneNo.replace(" ", "");
                    if (phoneNo.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                        setPhoneNumber(phoneNo);
                    } else {
                        errors.add("phoneNumber", new ActionMessage("errors.invalidAccount"));
                    }
                }
                if (!Utils.isDouble(request.getParameter("amount").toString())) {
                    errors.add("amount", new ActionMessage("errors.transfer_amount.valid"));
                } else if (getAmount() <= 0.0) {
                    errors.add("amount", new ActionMessage("errors.transfer_amount.required"));
                }
            } else {
                if (getPhoneNumber() == null || getPhoneNumber().length() == 0) {
                    errors.add("phoneNumber", new ActionMessage("errors.phnNumber.required"));
                } else if (getPhoneNumber().length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                    errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
                } else if (getPhoneNumber().length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                    String phoneNo = getPhoneNumber();
                    if (phoneNo.startsWith("+")) {
                        phoneNo = phoneNo.replace("+", "");
                    }
                    if (phoneNo.startsWith("88")) {
                        phoneNo = phoneNo.replace("88", "");
                    }
                    phoneNo = phoneNo.replace("-", "");
                    phoneNo = phoneNo.replace(" ", "");
                    if (phoneNo.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                        setPhoneNumber(phoneNo);
                    } else {
                        errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
                    }
                }
                if (getSms() != 1) {
                    if (!Utils.isDouble(request.getParameter("amount").toString())) {
                        errors.add("amount", new ActionMessage("errors.flexi_amount.valid"));
                    } else if (getAmount() <= 0.0) {
                        errors.add("amount", new ActionMessage("errors.amount.required"));
                    } else if (getRefillType() == Constants.REFILL_TYPE_PREPAID) {
                        if (getAmount() < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || getAmount() > SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount()) {
                            errors.add("amount", new ActionMessage("errors.prepaid.amount"));
                        }
                    } else if (getRefillType() == Constants.REFILL_TYPE_POSTPAID) {
                        if (getAmount() < SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() || getAmount() > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                            errors.add("amount", new ActionMessage("errors.postpaid.amount"));
                        }
                    }
                }
            }
        }
        return errors;
    }
}
