package com.myapp.struts.session;

public class PermissionConstants {
public final static int CLIENT = 10;
public final static int CLIENT_ADD = 11;
public final static int CLIENT_EDIT = 12;
public final static int CLIENT_RECHARGE = 13;
public final static int CLIENT_RETURN = 14;
public final static int CLIENT_RECEIVE = 15;
public final static int RCG = 20;
public final static int RCG_ADD = 21;
public final static int RCG_EDIT = 22;
public final static int REFILL = 30;
public final static int REFILL_GP = 31;
public final static int REFILL_BL = 32;
public final static int REFILL_RB = 33;
public final static int REFILL_WR = 34;
public final static int REFILL_TT = 35;
public final static int REFILL_CC = 36;
public final static int FLEXI_SIM_SETTINGS = 40;
public final static int FLEXI_SIM_SETTINGS_EDIT = 41;
public final static int FLEXI_SIM_SETTINGS_DELETE = 42;
public final static int REPORT = 50;
public final static int REPORT_REFILL_SUMMERY = 51;
public final static int REPORT_PAYMENT_SUMMERY = 52;
public final static int REPORT_PAYMENT_MADE_HISTORY = 53;
public final static int REPORT_SIM_RECHARGE_HISTORY = 54;
public final static int FLEXI_SIM_RECHARGE = 60;
public final static int FLEXI_SIM_RECHARGE_ADD = 61;
public final static int FLEXI_SIM_RECHARGE_EDIT = 62;
public final static int FLEXI_SIM_RECHARGE_DELETE = 63;
public final static int FLEXI_MESSAGE_LOG = 70;
public final static int FLEXI_MESSAGE_LOG_DELETE = 71;
}
