package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;

public class UserTaskSchedular {

    public UserTaskSchedular() {
    }

    public MyAppError editUserInformation(UserDTO p_dto, LoginDTO l_dto) {
        UserDAO userDAO = new UserDAO();
        return userDAO.editUserInformation(p_dto, l_dto);
    }

    public UserDTO getUserDTO(long id) {
        return UserLoader.getInstance().getUserDTOByID(id);
    }
}
