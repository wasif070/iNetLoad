package com.myapp.struts.user;

import com.myapp.struts.role.RoleDTO;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class UserLoader {

    static Logger logger = Logger.getLogger(UserLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<UserDTO> userDTOList = null;
    private HashMap<String, UserDTO> userDTOByUserID = null;
    private HashMap<Long, UserDTO> userDTOByID = null;
    private ArrayList<RoleDTO> roleDTOList = null;
    private HashMap<Long, RoleDTO> roleDTOByID = null;
    private HashMap<String, PermissionDTO> permissionDTOByRolePermissionID = null;
    static UserLoader userLoader = null;

    public UserLoader() {
        forceReload();
    }

    public static UserLoader getInstance() {
        if (userLoader == null) {
            createUserLoader();
        }
        return userLoader;
    }

    private synchronized static void createUserLoader() {
        if (userLoader == null) {
            userLoader = new UserLoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = null;
            ResultSet resultSet = null;
            permissionDTOByRolePermissionID = new HashMap<String, PermissionDTO>();
            sql = "select * from role_permission";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                PermissionDTO dto =  permissionDTOByRolePermissionID.get(resultSet.getString("role_permission_id"));
                if(dto==null)
                {
                  dto = new PermissionDTO();
                  permissionDTOByRolePermissionID.put(resultSet.getString("role_permission_id"), dto);
                  dto.setPermission(resultSet.getInt("role_permission_item_id"));
                }
                else
                {
                  dto.setPermission(resultSet.getInt("role_permission_item_id"));
                }
            }
            resultSet.close();
            roleDTOByID = new HashMap<Long, RoleDTO>();
            roleDTOList = new ArrayList<RoleDTO>();
            sql = "select * from role";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RoleDTO dto = new RoleDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setRoleName(resultSet.getString("role_id"));
                dto.setRoleDescription(resultSet.getString("role_description"));
                dto.setRoleCreatedByID(resultSet.getLong("role_created_by"));
                dto.setPermissionDTO(permissionDTOByRolePermissionID.get(resultSet.getString("role_permission_id")));
                roleDTOByID.put(dto.getId(), dto);
                roleDTOList.add(dto);
            }
            resultSet.close();
            userDTOByUserID = new HashMap<String, UserDTO>();
            userDTOByID = new HashMap<Long, UserDTO>();
            userDTOList = new ArrayList<UserDTO>();
            sql = "select * from user";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                UserDTO dto = new UserDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setUserId(resultSet.getString("user_id"));
                dto.setUserEmail(resultSet.getString("user_email"));
                dto.setUserName(resultSet.getString("user_full_name"));
                dto.setUserPassword(resultSet.getString("user_password"));
                dto.setUserStatus(resultSet.getInt("user_status"));
                dto.setUserStatusName(Constants.LIVE_STATUS_STRING[resultSet.getInt("user_status")]);
                dto.setUserRoleId(resultSet.getLong("user_role_id"));
                RoleDTO rdto = roleDTOByID.get(dto.getUserRoleId());
                if (rdto != null) {
                    dto.setUserRoleName(rdto.getRoleName());
                } else {
                    dto.setUserRoleName(" ");
                }
                userDTOByUserID.put(dto.getUserId(), dto);
                userDTOByID.put(dto.getId(), dto);
                userDTOList.add(dto);
            }
            resultSet.close();
            if (roleDTOList != null) {
                for (int i = 0; i < roleDTOList.size(); i++) {
                    RoleDTO roledto = roleDTOList.get(i);
                    UserDTO userdto = userDTOByID.get(roledto.getRoleCreatedByID());
                    if (userdto != null) {
                        roledto.setRoleCreatedBy(userdto.getUserId());
                    }
                    else
                    {
                        roledto.setRoleCreatedBy(" ");
                    }
                }
            }

        } catch (Exception e) {
            logger.fatal("Exception in UserLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<UserDTO> getUserDTOList() {
        checkForReload();
        return userDTOList;
    }

    public synchronized UserDTO getUserDTOByUserIDPass(String user_id, String password) {
        checkForReload();
        UserDTO dto = null;
        if(user_id != null)
        {
           dto = userDTOByUserID.get(user_id.toLowerCase());
        }
        if (dto != null) {
            if (dto.getUserPassword().equalsIgnoreCase(password)) {
                return dto;
            }
        }
        return null;        
    }

    public synchronized UserDTO getUserDTOByID(long id) {
        checkForReload();
        return userDTOByID.get(id);
    }

    public synchronized ArrayList<RoleDTO> getRoleDTOList() {
        checkForReload();
        return roleDTOList;
    }

    public synchronized RoleDTO getRoleDTOByID(long id) {
        checkForReload();
        return roleDTOByID.get(id);
    }
}
