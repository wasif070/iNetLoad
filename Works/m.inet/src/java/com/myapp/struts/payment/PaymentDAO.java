package com.myapp.struts.payment;

import com.myapp.struts.client.ClientDAO;
import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import org.apache.log4j.Logger;

public class PaymentDAO {

    static Logger logger = Logger.getLogger(PaymentDAO.class.getName());

    public PaymentDAO() {
    }

    public MyAppError addRechargePaymentRequest(PaymentDTO p_dto) {
        String sql = "";
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        MyAppError error = new MyAppError();

        try {
            ClientDAO clDAO = new ClientDAO();
            ClientDTO parDTO = ClientLoader.getInstance().getClientDTOByID(p_dto.getPaymentFromClientId());
            double minDeposit = 0.0;
            if(parDTO != null)
            {    
               minDeposit = parDTO.getClientDeposit();
            }
            double parentBalance = clDAO.getClientCredit(p_dto.getPaymentFromClientId());

            if (p_dto.getPaymentFromClientTypeId() == Constants.USER_TYPE || p_dto.getPaymentFromClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || p_dto.getPaymentFromClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 || (parentBalance-minDeposit) >= p_dto.getPaymentAmount()) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                sql = "insert into payment(payment_from_client_type_id,payment_to_client_type_id,payment_from_client_id,payment_to_client_id,"
                        + "payment_type_id,payment_amount,payment_time,payment_description,payment_marker,payment_discount,payment_amount_temp,payment_discount_temp) values(?,?,?,?,?,?,?,?,?,?,?,?)";
                long paymentTime = System.currentTimeMillis();
                String paymentMarker = paymentTime + ":" + Utils.getRandomNumber();
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setInt(1, p_dto.getPaymentFromClientTypeId());
                ps.setInt(2, p_dto.getPaymentToClientTypeId());
                ps.setLong(3, p_dto.getPaymentFromClientId());
                ps.setLong(4, p_dto.getPaymentToClientId());
                ps.setInt(5, p_dto.getPaymentTypeId());
                ps.setDouble(6, 0.0);
                ps.setLong(7, paymentTime);
                ps.setString(8, p_dto.getPaymentDescription());
                ps.setString(9, paymentMarker);
                ps.setDouble(10, 0.0);
                ps.setDouble(11, p_dto.getPaymentAmount());
                ps.setDouble(12, p_dto.getPaymentDiscount());                
                ps.executeUpdate();
            } else {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Insufficient Parent Balance.");
            }
        } catch (Exception ex) {
            logger.fatal("Error while adding recharge payment request: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addReturnPaymentRequest(PaymentDTO p_dto) {
        String sql = "";
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        MyAppError error = new MyAppError();
        try {
            ClientDAO clDAO = new ClientDAO();
            double balance = clDAO.getClientCredit(p_dto.getPaymentToClientId());

            if (balance - p_dto.getPaymentAmount() >= 0) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                sql = "insert into payment(payment_from_client_type_id,payment_to_client_type_id,payment_from_client_id,payment_to_client_id,"
                        + "payment_type_id,payment_amount,payment_time,payment_description,payment_marker,payment_amount_temp) values(?,?,?,?,?,?,?,?,?,?)";
                long paymentTime = System.currentTimeMillis();
                String paymentMarker = paymentTime + ":" + Utils.getRandomNumber();
                ps = dbConnection.connection.prepareStatement(sql);
                ps.setInt(1, p_dto.getPaymentFromClientTypeId());
                ps.setInt(2, p_dto.getPaymentToClientTypeId());
                ps.setLong(3, p_dto.getPaymentFromClientId());
                ps.setLong(4, p_dto.getPaymentToClientId());
                ps.setInt(5, p_dto.getPaymentTypeId());
                ps.setDouble(6, 0.0);
                ps.setLong(7, paymentTime);
                ps.setString(8, p_dto.getPaymentDescription());
                ps.setString(9, paymentMarker);
                ps.setDouble(10, p_dto.getPaymentAmount());
                ps.executeUpdate();
            } else {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Insufficient Client Balance.");
            }

        } catch (Exception ex) {
            logger.fatal("Error while adding return payment request: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addReceivePaymentRequest(PaymentDTO p_dto) {
        String sql = "";        
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        MyAppError error = new MyAppError();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "insert into payment(payment_from_client_type_id,payment_to_client_type_id,payment_from_client_id,payment_to_client_id,"
                    + "payment_type_id,payment_amount,payment_time,payment_description,payment_marker) values(?,?,?,?,?,?,?,?,?)";
            long paymentTime = System.currentTimeMillis();
            String paymentMarker = paymentTime + ":" + Utils.getRandomNumber();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setInt(1, p_dto.getPaymentFromClientTypeId());
            ps.setInt(2, p_dto.getPaymentToClientTypeId());
            ps.setLong(3, p_dto.getPaymentFromClientId());
            ps.setLong(4, p_dto.getPaymentToClientId());
            ps.setInt(5, p_dto.getPaymentTypeId());
            ps.setDouble(6, Math.abs(p_dto.getPaymentAmount()));
            ps.setLong(7, paymentTime);
            ps.setString(8, p_dto.getPaymentDescription());
            ps.setString(9, paymentMarker);
            ps.setDouble(10, p_dto.getPaymentAmount());
            ps.executeUpdate();
        } catch (Exception ex) {
            logger.fatal("Error while adding receive payment request: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList<PaymentDTO> getPaymentMadeDTOs(LoginDTO login_dto, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + ")"
                    + " and payment_time between " + start + " and " + end;

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = new PaymentDTO();
                dto.setPaymentId(rs.getLong("payment_id"));
                dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                dto.setPaymentDescription(rs.getString("payment_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentMadeDTOs:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<PaymentDTO> getPaymentMadeDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + ")"
                    + " and payment_time between " + start + " and " + end;
            logger.debug("SQL:" + sql);
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                ClientDTO cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("payment_to_client_id"));
                if ((pdto.searchWithClient && !cldto.getClientId().toLowerCase().startsWith(pdto.getPaymentToClient()))) {
                    continue;
                }
                PaymentDTO dto = new PaymentDTO();
                dto.setPaymentId(rs.getLong("payment_id"));
                dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                dto.setPaymentToClient(cldto.getClientId());
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDescription(rs.getString("payment_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentMadeDTOsWithSearchParam:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<PaymentDTO> getPaymentReceivedDTOs(LoginDTO login_dto, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where payment_to_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + ")"
                    + " and payment_time between " + start + " and " + end;
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = new PaymentDTO();
                dto.setPaymentId(rs.getLong("payment_id"));
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDescription(rs.getString("payment_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentMadeDTOs:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<PaymentDTO> getPaymentReceivedDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where payment_to_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + ")"
                    + " and payment_time between " + start + " and " + end;
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = new PaymentDTO();
                dto.setPaymentId(rs.getLong("payment_id"));
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDescription(rs.getString("payment_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception getPaymentMadeDTOsWithSearchParam:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
    
    public ArrayList<PaymentDTO> getPaymentPaidDTOs(LoginDTO login_dto, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_RECEIVED + "," + Constants.PAYMENT_TYPE_CANCELLED + ")"
                    + " and payment_time between " + start + " and " + end;
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = new PaymentDTO();
                dto.setPaymentId(rs.getLong("payment_id"));
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDescription(rs.getString("payment_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentMadeDTOs:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<PaymentDTO> getPaymentPaidDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_RECEIVED + "," + Constants.PAYMENT_TYPE_CANCELLED + ")"
                    + " and payment_time between " + start + " and " + end;
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = new PaymentDTO();
                dto.setPaymentId(rs.getLong("payment_id"));
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDescription(rs.getString("payment_description"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception getPaymentMadeDTOsWithSearchParam:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }    

    public ArrayList<PaymentDTO> getClientPaymentSummeryDTOs(long id, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            String sql = "select * from payment where ((payment_to_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + "))"
                    + " or (payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_RECEIVED + "," + Constants.PAYMENT_TYPE_CANCELLED + ")))"
                    + " and payment_time between " + start + " and " + end;
            logger.debug(sql);
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = new PaymentDTO();
                dto = new PaymentDTO();
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDescription(rs.getString("payment_description"));
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getClientPaymentSummeryDTOs:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<PaymentDTO> getClientPaymentSummeryDTOsWithSearchParam(long id, PaymentDTO pdto) {
        ArrayList<PaymentDTO> data = new ArrayList<PaymentDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long start = new GregorianCalendar(pdto.getStartYear(), pdto.getStartMonth() - 1, pdto.getStartDay(), 0, 0, 0).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            long end = new GregorianCalendar(pdto.getEndYear(), pdto.getEndMonth() - 1, pdto.getEndDay(), 23, 59, 59).getTimeInMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000*(-1));
            String sql = "select * from payment where ((payment_to_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + "))"
                    + " or (payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_RECEIVED + "," + Constants.PAYMENT_TYPE_CANCELLED + ")))"
                    + " and payment_time between " + start + " and " + end;
            logger.debug(sql);
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                if ((pdto.searchWithPaymentType && pdto.getPaymentTypeId() != rs.getInt("payment_type_id"))) {
                    continue;
                }
                PaymentDTO dto = new PaymentDTO();
                dto = new PaymentDTO();
                dto.setPaymentTypeId(rs.getInt("payment_type_id"));
                dto.setPaymentTypeName(Constants.PAYMENT_TYPE[dto.getPaymentTypeId()]);
                dto.setPaymentTime(rs.getLong("payment_time") + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime()*60*60*1000));
                dto.setPaymentDescription(rs.getString("payment_description"));
                dto.setPaymentAmount(rs.getDouble("payment_amount"));
                dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getClientPaymentSummeryDTOs:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<PaymentDTO> getPaymentSummeryDTOs(LoginDTO login_dto) {
        HashMap map = new HashMap();
        ArrayList<PaymentDTO> data = null;
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where (payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + "))"
                    + " or (payment_to_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_RECEIVED + "," + Constants.PAYMENT_TYPE_CANCELLED + "))";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = null;
                switch (rs.getInt("payment_type_id")) {
                    case Constants.PAYMENT_TYPE_INITIAL:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setPaymentAmount(rs.getDouble("payment_amount"));
                            dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_PAID:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setPaymentAmount(rs.getDouble("payment_amount"));
                            dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_RETURNED:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() - rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setPaymentAmount(0.0 - rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_RECEIVED:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_from_client_id"));
                        if (dto != null) {
                            dto.setReceivedAmount(dto.getReceivedAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_from_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_from_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setReceivedAmount(rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_CANCELLED:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_from_client_id"));
                        if (dto != null) {
                            dto.setReceivedAmount(dto.getReceivedAmount() - rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_from_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_from_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setReceivedAmount(0.0 - rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    default:
                        break;
                }
            }
            rs.close();
            data = new ArrayList<PaymentDTO>(map.values());
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentSummeryDTOs:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public ArrayList<PaymentDTO> getPaymentSummeryDTOsWithSearchParam(LoginDTO login_dto, PaymentDTO pdto) {
        HashMap map = new HashMap();
        ArrayList<PaymentDTO> data = null;
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long id = Constants.ROOT_RESELLER_PARENT_ID;
            if (!login_dto.getIsUser()) {
                id = login_dto.getId();
            }
            String sql = "select * from payment where (payment_from_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + "))"
                    + " or (payment_to_client_id=" + id
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_RECEIVED + "," + Constants.PAYMENT_TYPE_CANCELLED + "))";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = null;
                ClientDTO cldto = null;
                switch (rs.getInt("payment_type_id")) {
                    case Constants.PAYMENT_TYPE_INITIAL:
                        cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("payment_to_client_id"));
                        if (pdto.searchWithClient && !cldto.getClientId().toLowerCase().startsWith(pdto.getPaymentToClient())) {
                            break;
                        }
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(cldto.getClientId());
                            dto.setPaymentAmount(rs.getDouble("payment_amount"));
                            dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_PAID:
                        cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("payment_to_client_id"));
                        if (pdto.searchWithClient && !cldto.getClientId().toLowerCase().startsWith(pdto.getPaymentToClient())) {
                            break;
                        }
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(cldto.getClientId());
                            dto.setPaymentAmount(rs.getDouble("payment_amount"));
                            dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_RETURNED:
                        cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("payment_to_client_id"));
                        if (pdto.searchWithClient && !cldto.getClientId().toLowerCase().startsWith(pdto.getPaymentToClient())) {
                            break;
                        }
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() - rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(cldto.getClientId());
                            dto.setPaymentAmount(0.0 - rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_RECEIVED:
                        cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("payment_from_client_id"));
                        if (pdto.searchWithClient && !cldto.getClientId().toLowerCase().startsWith(pdto.getPaymentToClient())) {
                            break;
                        }
                        dto = (PaymentDTO) map.get(rs.getLong("payment_from_client_id"));
                        if (dto != null) {
                            dto.setReceivedAmount(dto.getReceivedAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_from_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_from_client_id"));
                            dto.setPaymentToClient(cldto.getClientId());
                            dto.setReceivedAmount(rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_CANCELLED:
                        cldto = ClientLoader.getInstance().getAllClientDTOByID(rs.getLong("payment_from_client_id"));
                        if (pdto.searchWithClient && !cldto.getClientId().toLowerCase().startsWith(pdto.getPaymentToClient())) {
                            break;
                        }
                        dto = (PaymentDTO) map.get(rs.getLong("payment_from_client_id"));
                        if (dto != null) {
                            dto.setReceivedAmount(dto.getReceivedAmount() - rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_from_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_from_client_id"));
                            dto.setPaymentToClient(cldto.getClientId());
                            dto.setReceivedAmount(0.0 - rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    default:
                        break;
                }
            }
            rs.close();
            data = new ArrayList<PaymentDTO>(map.values());
        } catch (Exception e) {
            logger.fatal("Exception getPaymentSummeryDTOsWithSearchParam:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
    
    public ArrayList<PaymentDTO> getAccountSummeryDTOs(LoginDTO login_dto) {
        HashMap map = new HashMap();
        ArrayList<PaymentDTO> data = null;
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String sql = "select * from payment where (payment_to_client_id=" + login_dto.getId()
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_INITIAL + "," + Constants.PAYMENT_TYPE_PAID + "," + Constants.PAYMENT_TYPE_RETURNED + "))"
                    + " or (payment_from_client_id=" + login_dto.getId()
                    + " and payment_type_id in(" + Constants.PAYMENT_TYPE_RECEIVED + "," + Constants.PAYMENT_TYPE_CANCELLED + "))";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                PaymentDTO dto = null;
                switch (rs.getInt("payment_type_id")) {
                    case Constants.PAYMENT_TYPE_INITIAL:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setPaymentAmount(rs.getDouble("payment_amount"));
                            dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_PAID:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setPaymentAmount(rs.getDouble("payment_amount"));
                            dto.setPaymentDiscount(rs.getDouble("payment_discount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_RETURNED:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_to_client_id"));
                        if (dto != null) {
                            dto.setPaymentAmount(dto.getPaymentAmount() - rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_to_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_to_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setPaymentAmount(0.0 - rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_RECEIVED:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_from_client_id"));
                        if (dto != null) {
                            dto.setReceivedAmount(dto.getReceivedAmount() + rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_from_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_from_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setReceivedAmount(rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    case Constants.PAYMENT_TYPE_CANCELLED:
                        dto = (PaymentDTO) map.get(rs.getLong("payment_from_client_id"));
                        if (dto != null) {
                            dto.setReceivedAmount(dto.getReceivedAmount() - rs.getDouble("payment_amount"));
                        } else {
                            dto = new PaymentDTO();
                            dto.setPaymentToClientTypeId(rs.getInt("payment_from_client_type_id"));
                            dto.setPaymentToClientTypeName(Constants.CLIENT_TYPE[dto.getPaymentToClientTypeId()]);
                            dto.setPaymentToClientId(rs.getLong("payment_from_client_id"));
                            dto.setPaymentToClient(ClientLoader.getInstance().getAllClientDTOByID(dto.getPaymentToClientId()).getClientId());
                            dto.setReceivedAmount(0.0 - rs.getDouble("payment_amount"));
                            map.put(dto.getPaymentToClientId(), dto);
                        }
                        break;
                    default:
                        break;
                }
            }
            rs.close();
            data = new ArrayList<PaymentDTO>(map.values());
        } catch (Exception e) {
            logger.fatal("Exception in getPaymentSummeryDTOs:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }    
}
