package com.myapp.struts.mobile;

public class SessionValueDTO {

    private String userName;
    private String pass;
    private String time;

    public SessionValueDTO() {
        userName = "";
        pass = "";
        time = "";
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
