<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader,java.util.ArrayList,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Add Client</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <%
                        if (login_dto == null) {
                            response.sendRedirect("../home/home.do");
                            return;
                        }
                        if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2
                                ) {
                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                            response.sendRedirect("../index.do");
                            return;
                        }
            %>
            <div class="body_content fl_left">
                <div class="body full-div">
                    <html:form action="/client/addClient" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        Add Client<br>
                                            <bean:write name="ClientForm" property="message" filter="false"/>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <html:hidden property="parentId" value="<%=String.valueOf(login_dto.getId())%>" />
                                    <th valign="top" style="padding-top: 8px;">Client ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientId" /><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                 <tr>
                                    <th valign="top" style="padding-top: 8px;">Client Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="clientTypeId">
                                            <%
                                                 if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER) {
                                            %>
                                            <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER)%>">Reseller</html:option>
                                            <%
                                                 }
                                                 if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3)
                                                 {
                                            %>
                                            <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER2)%>">Reseller-2</html:option>
                                            <%
                                                 }
                                                 if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2)
                                                 {
                                            %>
                                            <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_AGENT)%>">Agent</html:option>
                                            <%
                                                 }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientTypeId" property="clientTypeId">
                                            <bean:write name="clientTypeId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <html:text property="clientPassword" /><br/>
                                        <html:messages id="clientPassword" property="clientPassword">
                                            <bean:write name="clientPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Pass.</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <html:text property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Status</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="clientStatus">
                                            <%
                                                        for (int i = 1; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                                        }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientStatus" property="clientStatus">
                                            <bean:write name="clientStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Initial Balance</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="clientCredit" /><br/>
                                        <html:messages id="clientCredit" property="clientCredit">
                                            <bean:write name="clientCredit"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="hidden" name="clientVersion" value="0" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <div style="margin-left: 40px;  margin-top: 7px; margin-bottom: 5px;display: block">
                                            <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Save" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>