<%@page import="com.myapp.struts.mobile.LoadRequestDAO,com.myapp.struts.mobile.RequestDTO,java.util.ArrayList,com.google.gson.Gson"%>
<%@include file="login-check.jsp"%>
<%
    int start = 0;
    int limit = 10;
    if (request.getParameter("start") != null) {
        start = Integer.parseInt(request.getParameter("start"));
    }
    Gson gson = new Gson();
    if (clientDTO != null) {
        if (request.getParameter("request_type") != null) {
            ArrayList<RequestDTO> data = LoadRequestDAO.getRequests(clientDTO, Integer.parseInt(request.getParameter("request_type")));
            if (data != null && data.size() > 0) {
                out.print(gson.toJson(data));
            }
        }
    }
%>