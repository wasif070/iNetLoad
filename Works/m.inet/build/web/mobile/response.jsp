<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.mobile.Decription,com.myapp.struts.flexi.FlexiDTO"%>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.util.MyAppError"%>
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientDAO,com.myapp.struts.client.ClientLoader"%>
<%     
       if(request.getParameter("RequestID")==null || request.getParameter("UserId")==null || request.getParameter("Password") == null)
       {
            out.println("Error=1");
            return;           
       }    
       String requestID = request.getParameter("RequestID");                         
       String user_name = request.getParameter("UserId");
       String password = request.getParameter("Password");
       ClientDTO clientDTO = null;               
       if (user_name.length() == 0) {
            out.println("Error=2");
            return;
       }
       else
       {
           clientDTO = ClientLoader.getInstance().getClientDTOByUserIdPass(user_name,password,0);
           if (clientDTO == null) {
               out.println("Error=2");
               return;
           }           
       }                             

       if (clientDTO != null) {
  
                FlexiDAO flDAO = new FlexiDAO();

                MyAppError error = flDAO.getFlexiResponse(clientDTO.getId(),requestID);
                if (error.getErrorType() == error.NoError) {
                    out.println(error.getErrorMessage());
                    return;
                } else {
                    out.println("Error=3");
                }
            }
%>