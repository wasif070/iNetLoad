<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.flexi.FlexiDTO,com.myapp.struts.session.Constants,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Request Processing</title>
    </head>
<body>
    
    <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>            
            <div class="body_content fl_left">                
                <div class="body full-div">
                    <%
                         FlexiDTO dto = (FlexiDTO) request.getSession(true).getAttribute(Constants.SMS_SUCCESS_DTO);
                         String phone_no = dto.getPhoneNumber();
                         String amount = String.valueOf(dto.getAmount());
                    %>
                    <h1>Message Sent Successfully!!!</h1>
                    <p>Phone Number: <%=phone_no%></p>
                    <p>Amount: <%=amount%></p>
                    <p>Your Request Will Be Processed Within A Moment....</p>
                    <p>Thank You.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>