<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader,java.util.ArrayList,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Recharge/Return Client</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <%
                        long edid = Long.parseLong(request.getParameter("id"));
                        if (login_dto == null) {
                            response.sendRedirect("../home/home.do");
                            return;
                        }
                        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE
                                && perDTO != null && !perDTO.CLIENT_RECHARGE && !perDTO.CLIENT_RETURN && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2
                                && login_dto.getId() != edid && login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                            response.sendRedirect("../index.do");
                            return;
                        }
            %>
            <div class="body_content fl_left">                
                <div class="body full-div">
                    <html:form action="/client/rechargeReturnClient" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        Recharge/Return Client<br>
                                            <bean:write name="ClientForm" property="message" filter="false"/>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Client ID</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <html:text property="clientId" readonly="true" /><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Status</th>
                                    <td valign="top" style="padding-top: 8px;">
                                        <html:select property="clientStatus" disabled="true" >
                                            <%                                                 
                                                        for (int i = 0; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                                        }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="clientStatus" property="clientStatus">
                                            <bean:write name="clientStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                               </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Amount</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <input type="text" name="amount" value="0"  />
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Description</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <input type="text" name="description" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">                                        
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <div style="margin-left: 40px;  margin-top: 7px; margin-bottom: 5px; display: block">
                                            <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Recharge" />
                                            <input name="submit" type="submit" class="custom-button" value="Return" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <html:hidden property="id" />
                        <html:hidden property="parentId" />
                        <html:hidden property="clientTypeId" />
                        <html:hidden property="clientPassword" />
                        <html:hidden property="retypePassword" />
                        <html:hidden property="clientId" />
                        <input type="hidden" name="searchLink" value="/client/listClient.do?list_all=0" />
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>