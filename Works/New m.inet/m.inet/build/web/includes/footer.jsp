<div class="footer">    
    <div class="full-div">Copyright&copy;<a href="<%=SettingsLoader.getInstance().getSettingsDTO().getCopyrightLink()%>" target="_blank"><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%></a></div>
    <div class="full-div">Server Time:&nbsp;<%=Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis())%></div>
</div>