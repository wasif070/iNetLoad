<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientLoader,com.myapp.struts.client.ClientDTO,com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.client.ClientDAO,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<%@page import="java.util.ArrayList,com.myapp.struts.session.Constants" %>
<%@page import="com.myapp.struts.balancetransfer.BalanceTransferLoader,com.myapp.struts.balancetransfer.BalanceTransferDTO" %>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Home</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>            
            <div class="body_content fl_left">                             
                <%
                    /*int btTypeID = Constants.TRANSFER_TYPE_BKASH;
                     if (request.getParameter("btType") != null) {
                     btTypeID = Integer.parseInt(request.getParameter("btType"));
                     }*/
                    ArrayList btSurchargeList = BalanceTransferLoader.getInstance().getBTSurchargeDTOList();
                    if (btSurchargeList != null && btSurchargeList.size() > 0) {
                %>          
                <table style="width:100%;border: 2px solid #FFFFDD;" cellpadding="0" cellspacing="0" align="left">
                    <tr>
                        <td width="100%" colspan="2" align="center" style="font-weight: bold;border-bottom:  2px solid #FFFFDD;">
                            <%
                                out.print(Constants.TRANSFER_TYPE[Constants.TRANSFER_TYPE_BKASH] + " Surcharge List");
                            %>
                        </td>
                    </tr> 
                    <%
                        BalanceTransferDTO dto = null;
                        for (int i = 0; i < btSurchargeList.size(); i++) {
                            dto = (BalanceTransferDTO) btSurchargeList.get(i);
                            if (dto.getTypeID() != Constants.TRANSFER_TYPE_BKASH) {
                                continue;
                            }
                    %>    
                    <tr>           
                        <td align="center"  style="width:65%; font-size: 10px; font-weight: bold; color: olive;border-right: 1px solid #FFFFDD;border-bottom:  1px solid #FFFFDD;"><%=dto.getMinTransferredAmount() + "-" + dto.getMaxTransferredAmount()%></td>
                        <td align="right"  style="padding-right: 5px; width:35%; font-size: 10px; font-weight: bold; color: #363636;border-right: 1px solid #FFFFDD;border-bottom:  1px solid #FFFFDD;"><%=dto.getSurchargeAmount() + "%"%></td>
                    </tr>    
                    <%
                        }
                    %>
                </table>
                <table style="width:100%;border: 2px solid #FFFFDD;" cellpadding="0" cellspacing="0" align="left">
                    <tr>
                        <td width="100%" colspan="2" align="center" style="font-weight: bold;border-bottom:  2px solid #FFFFDD;">
                            <%
                                out.print(Constants.TRANSFER_TYPE[Constants.TRANSFER_TYPE_DBBL] + " Surcharge List");
                            %>
                        </td>
                    </tr> 
                    <%
                        dto = null;
                        for (int i = 0; i < btSurchargeList.size(); i++) {
                            dto = (BalanceTransferDTO) btSurchargeList.get(i);
                            if (dto.getTypeID() != Constants.TRANSFER_TYPE_DBBL) {
                                continue;
                            }
                    %>    
                    <tr>           
                        <td align="center"  style="width:65%; font-size: 10px; font-weight: bold; color: olive;border-right: 1px solid #FFFFDD;border-bottom:  1px solid #FFFFDD;"><%=dto.getMinTransferredAmount() + "-" + dto.getMaxTransferredAmount()%></td>
                        <td align="right"  style="padding-right: 5px; width:35%; font-size: 10px; font-weight: bold; color: #363636;border-right: 1px solid #FFFFDD;border-bottom:  1px solid #FFFFDD;"><%=dto.getSurchargeAmount() + "%"%></td>
                    </tr>    
                    <%
                        }
                    %>
                </table>
                <%
                    }
                %>        
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>