
CREATE TABLE IF NOT EXISTS `sms_rateplan` (
  `rateplan_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `rate_amount` decimal(10,4) NOT NULL,
  `rate_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rateplan_id`)
);

ALTER TABLE `client` ADD `client_sms_active` TINYINT( 1 ) NOT NULL AFTER `client_bt_active` ;
ALTER TABLE `client` ADD `client_sms_sender` VARCHAR( 15 ) NOT NULL AFTER `client_sms_active`;
ALTER TABLE `client` CHANGE `client_sms_active` `client_sms_active` TINYINT( 1 ) NOT NULL DEFAULT '1';