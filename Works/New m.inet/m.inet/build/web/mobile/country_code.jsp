<%-- 
    Document   : country_code
    Created on : Sep 8, 2013, 4:33:27 PM
    Author     : user
--%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.myapp.struts.sms.RatePlanLoader"%>
<%@page import="com.myapp.struts.sms.RatePlanDTO"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<RatePlanDTO> data = RatePlanLoader.getInstance().getRatePlanDTOList();
    Gson gson = new Gson();
    if (data != null && data.size() > 0) {
        out.print(gson.toJson(data));
    }
%>
