<%@page import="com.myapp.struts.util.Utils"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.flexi.FlexiDTO"%>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.util.MyAppError,com.myapp.struts.system.SettingsLoader"%>
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader,com.myapp.struts.util.Utils"%>
<%
    if (request.getParameter("Request") == null || request.getParameter("CountryCode") == null || request.getParameter("UserId") == null
            || request.getParameter("Password") == null || request.getParameter("MobileNumber") == null || request.getParameter("Amount") == null 
            || request.getParameter("NumberType") == null) {
        out.println("Error=1");
        return;
    }
    String req_type = request.getParameter("Request");
    String country_code = request.getParameter("CountryCode");
    if (!req_type.equals("TopUp") || !country_code.equals("BD")) {
        out.println("Error=1");
        return;
    }
    String user_name = request.getParameter("UserId");
    String password = request.getParameter("Password");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        out.println("Error=2");
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserIdPass(user_name,password,0);
        if (clientDTO == null) {
            out.println("Error=2");
            return;
        }
    }

    if (clientDTO != null) {
        String number = request.getParameter("MobileNumber");
        String amount_str = request.getParameter("Amount");
        String type_str = request.getParameter("NumberType");
        if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            out.println("Error=2");
            return;
        }
        FlexiDTO dto = new FlexiDTO();
        if (clientDTO.getClientIp() != null && clientDTO.getClientIp().trim().length() > 0 && !request.getRemoteAddr().equals(clientDTO.getClientIp())) {
            out.println("Error=2" + request.getRemoteAddr());
            return;
        }
        int flexi_type = 0;
        if (type_str != null) {
            flexi_type = Integer.parseInt(type_str);
        }

        if (flexi_type < Constants.REFILL_TYPE_PREPAID || flexi_type > Constants.REFILL_TYPE_POSTPAID) {
            out.println("Error=3");
            return;
        }

        if (number.length() == 0) {
            out.println("Error=3");
            return;
        } else if (number.length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
            out.println("Error=3");
            return;
        } else if (number.length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
            if (number.startsWith("+")) {
                number = number.replaceFirst("+", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("00")) {
                number = number.replaceFirst("00", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("88")) {
                number = number.replaceFirst("88", "");
            }
            number = number.replace("-", "");
            number = number.replace(" ", "");
            String prefix = number.substring(0, 3);
            int operatorTypeID = 0;
            for (int i = 1; i < Constants.OPERATOR_TYPE_PREFIX.length; i++) {
                if (prefix.equals(Constants.OPERATOR_TYPE_PREFIX[i])) {
                    operatorTypeID = i;
                    break;
                }
            }
            if (operatorTypeID < Constants.GP) {
                out.println("Error=3");
                return;
            } else {
                if (number.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                    dto.setOperatorTypeID(operatorTypeID);
                    dto.setPhoneNumber(number);
                } else {
                    out.println("Error=3");
                    return;
                }
            }
        }
        long amount = 0;
        if (amount_str != null) {
            amount = Long.parseLong(amount_str);
        }
        if (amount <= 0) {
            out.println("Error=4");
            return;
        } else if (flexi_type == Constants.REFILL_TYPE_PREPAID) {
            if (amount < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || amount > SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount()) {
                out.println("Error=4");
                return;
            }
        } else if (flexi_type == Constants.REFILL_TYPE_POSTPAID) {
            if (amount < SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() || amount > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                out.println("Error=4");
                return;
            }
        }

        FlexiDAO flDAO = new FlexiDAO();
        long curTime = System.currentTimeMillis();
        long lastRefillTime = flDAO.getLastRefillTime(number, curTime);
        if (lastRefillTime > 0) {
            long refillInterval = curTime - lastRefillTime;
            if (refillInterval < (SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000)) {
                out.println("Error=6");
                return;
            }
        }
        dto.setAmount(amount);
        dto.setRefillType(flexi_type);
        LoginDTO login_dto = new LoginDTO();
        login_dto.setId(clientDTO.getId());
        login_dto.setClientParentId(clientDTO.getParentId());
        login_dto.setClientTypeId(clientDTO.getClientTypeId());

        MyAppError error = flDAO.addFlexiRequest(login_dto, dto);
        if (error.getErrorType() == error.NoError) {
            out.println("RequestID=" + error.getErrorMessage());
            return;
        } else if (error.getErrorType() == error.ValidationError) {
            out.println("Error=5");
            return;
        } else {
            out.println("Error=7");
            return;
        }
    }
%>