<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.mobile.Decription,com.myapp.struts.flexi.FlexiDTO"%>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.util.MyAppError"%>
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientDAO,com.myapp.struts.client.ClientLoader,com.myapp.struts.util.Utils"%>
<%
    if (request.getParameter("RequestKey") == null || request.getParameter("RequestCode") == null || request.getParameter("RequestID") == null || request.getParameter("UserId") == null) {
        out.println("Error=1");
        return;
    }
    String requestID = request.getParameter("RequestID");
    String user_name = request.getParameter("UserId");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        out.println("Error=2");
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserId(user_name);
        if (clientDTO == null) {
            out.println("Error=2");
            return;
        }
    }

    if (clientDTO != null) {
        String req_key = request.getParameter("RequestKey");
        String req_code = request.getParameter("RequestCode");
        String res_code = Utils.getMD5(req_key, clientDTO.getClientPassword());
        if (!req_code.equals(res_code)) {
            out.println("Error=2");
            return;
        }
        FlexiDAO flDAO = new FlexiDAO();

        MyAppError error = flDAO.getFlexiResponse(clientDTO.getId(), requestID);
        if (error.getErrorType() == error.NoError) {
            out.println(error.getErrorMessage());
            return;
        } else {
            out.println("Error=3");
        }
    }
%>