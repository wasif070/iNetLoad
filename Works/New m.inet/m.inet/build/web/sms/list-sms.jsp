<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.sms.SMSDTO,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<%
            int requestType = Constants.INETLOAD_STATUS_SUBMITTED;
            if (request.getParameter("requestType") != null) {
                requestType = Integer.parseInt(request.getParameter("requestType"));
            }
            String title = SettingsLoader.getInstance().getSettingsDTO().getBrandName() + " :: SMS List";
            String des = "SMS Requests";
            if (requestType == Constants.INETLOAD_STATUS_SUBMITTED) {
                title = SettingsLoader.getInstance().getSettingsDTO().getBrandName() + " :: Pending SMS List";
                des = "Pending SMS Requests";
            }
            int recordPerPage = 1000;
            int list_all = 0;

            if (request.getParameter("list_all") != null) {
               list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            String action="/sms/listSMS.do?list_all=0&requestType="+ requestType;
%>
<html>
    <head>
        <title><%=title%></title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/search-header.jsp"%></div>
            <%
                        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO!=null && !perDTO.CLIENT
                            && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3
                            && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2 && login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT)
                             {
                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                            response.sendRedirect("../index.do");
                            return;
                    }
            %>
            <div class="body_content fl_left">
                <div class="view-page-body">
                    <div class="fl_right height-5px"></div>
                    <div class="full-div">
                    <div class="half-div">
                        <html:form action="<%=action%>" method="post">                        
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Request From</th>
                                    <td><html:text property="fromClientId" styleId="fromClientId"/></td>
                                </tr>
                                <tr>
                                    <th>Phone Number</th>
                                    <td><html:text property="phoneNumber" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <div class="full-div" style="text-align: center;">
                                            <div class="clear height-5px"></div>
                                            <input type="submit" class="search-button" style="width:70px;"  name="startSearching" value="Search" />
                                            <input type="submit" class="search-button" style="margin-left:5px; width:70px;" value="Reset" />
                                            <div class="clear height-5px"></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </html:form>
                    </div>
                    </div>
                    <%
                              if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                              {
                                out.print(request.getSession(true).getAttribute(Constants.MESSAGE)+"<BR><BR>");
                              }
                              boolean refillPer = false;
                              if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.REFILL))
                                      && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                              {
                                  refillPer = true;
                              }
                    %>
                    <c:set var="refillPer" scope="page" value="<%=refillPer%>"/>
                    <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                    <jsp:scriptlet>
                        request.setAttribute("dyndecorator", new org.displaytag.decorator.TotalTableDecorator() {
                            public String addRowClass() {
                                long time_diff = 30 * 60 * 1000;
                                SMSDTO obj = (SMSDTO) getCurrentRowObject();
                                if ((System.currentTimeMillis() - obj.getSmsRequestTimeVal()) > time_diff && obj.getSmsRequestType() == Constants.INETLOAD_STATUS_PENDING) {
                                    return "red";
                                } else {
                                    return "";
                                }
                            }
                        });
                    </jsp:scriptlet>
                    <display:table class="reporting_table" defaultsort="6" defaultorder="descending" decorator="dyndecorator" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.SMSForm.smsList" pagesize="<%=recordPerPage%>" >
                        <display:setProperty name="paging.banner.item_name" value="Request" />
                        <display:setProperty name="paging.banner.items_name" value="Requests" />
                        <display:column property="phoneNumber" title="Phone No" sortable="true" headerClass="left-align" class="left-align" />
                        <display:column property="SMSFrom" title="Request From" sortable="true" headerClass="left-align" class="left-align" />
                        <display:column property="flexiType" title="Type" sortable="true" headerClass="left-align" class="left-align" />
                        <display:column property="amount" format="{0,number,0.00}" title="Amount" sortable="true" headerClass="right-align" class="right-align" total="true" />
                        <display:column title="Status" property="smsRequestTypeName" sortable="true" headerClass="center-align" class="center-align" />
                        <display:column property="smsRequestTimeVal" decorator="implement.displaytag.LongDateWrapper" title="Reqest Time" sortable="true" headerClass="center-align" class="center-align" />                        
                        <display:column property="message" title="Message" sortable="true" headerClass="center-align" class="center-align" />
                    </display:table>
                </div>
                <%
                   request.getSession(true).removeAttribute(Constants.MESSAGE);
                %>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
