<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.user.UserLoader,java.util.ArrayList,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Edit User</title>
    </head>
    <body>
        <div class="main_body">            
            <div><%@include file="../includes/header.jsp"%></div>
            <%
                        long edid = Long.parseLong(request.getParameter("id"));
                        if (login_dto == null) {
                            response.sendRedirect("../home/home.do");
                            return;
                        }
                        if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && login_dto.getId() != edid) {
                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
                            response.sendRedirect("../index.do");
                            return;
                        }
            %>
            <div class="body_content fl_left">
                <div class="body full-div">
                    <html:form action="/user/editUser" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        Edit User<br>
                                            <bean:write name="UserForm" property="message" filter="false"/>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">User ID</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="userId" /><br/>
                                        <html:messages id="userId" property="userId">
                                            <bean:write name="userId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>  
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="userPassword" /><br/>
                                        <html:messages id="userPassword" property="userPassword">
                                            <bean:write name="userPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Retype Password</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <td colspan="2">
                                        <html:hidden property="id" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <div style="margin-left: 40px; margin-top: 7px; margin-bottom: 5px; display: block">
                                            <input type="hidden" name="name" value="<%=request.getParameter("name")%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Update" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>
    </body>
</html>