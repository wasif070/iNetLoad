<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientDAO,com.myapp.struts.session.Constants,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Recharge Account</title>
    </head>
    <body>

        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="body_content fl_left"> 
               <div class="body full-div">
                    <%
                        ClientDAO dao = new ClientDAO();
                        double credit = dao.getClientCredit(login_dto.getId());                            
                    %>
                    <h1>Your Account Is Recharged Successfully!!!</h1>
                    <p>Your New Balance: <%=credit%></p>
                    <p>Thank You.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>