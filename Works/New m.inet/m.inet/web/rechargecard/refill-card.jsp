<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientDAO,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<style type="text/css">
    table tbody th{font-weight: normal}
</style>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Refill By Card</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <%
                        if (login_dto == null) {
                            response.sendRedirect("../home/home.do");
                            return;
                        }
                        if (login_dto.getIsUser()) {
                            response.sendRedirect("../home/home.do");
                            return;
                        } else {
                            if (login_dto.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || !login_dto.getIsSpecialAgent()) {
                                response.sendRedirect("../home/home.do");
                                return;
                            } else {
                                if (login_dto.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                                    response.sendRedirect("../home/home.do");
                                    return;
                                }
                            }
                        }
            %>
            <div class="body_content fl_left">                
                <div class="body">
                    <html:form action="/rechargecard/refillCard.do" method="post" styleId="refill_form">
                        <table class="input_table" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th colspan="2">Refill By Normal Card</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center">
                                        <bean:write name="RechargecardForm" property="message" filter="false"/>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 4px;">Serial No</th>
                                    <td valign="top" style="padding-top: 2px;">
                                        <html:text property="serialNo"  styleId="serialNo" />
                                        <html:messages id="serialNo" property="serialNo">
                                            <bean:write name="serialNo"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Card No</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="cardNo" styleId="cardNo"  />
                                        <html:messages id="cardNo" property="cardNo">
                                            <bean:write name="cardNo"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Phone Number</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:text property="phoneNumber" styleId="phoneNumber"  />
                                        <html:messages id="phoneNumber" property="phoneNumber">
                                            <bean:write name="phoneNumber"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>                                        
                                <tr>
                                    <th valign="top" style="padding-top: 8px;">Refill Type</th>
                                    <td valign="top" style="padding-top: 6px;">
                                        <html:select property="refillType" >
                                            <html:option value="<%=String.valueOf(Constants.REFILL_TYPE_PREPAID)%>"><%=Constants.REFILL_TYPE[Constants.REFILL_TYPE_PREPAID]%></html:option>
                                            <html:option value="<%=String.valueOf(Constants.REFILL_TYPE_POSTPAID)%>"><%=Constants.REFILL_TYPE[Constants.REFILL_TYPE_POSTPAID]%></html:option>
                                        </html:select>
                                        <html:messages id="refillType" property="refillType">
                                            <bean:write name="refillType"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="padding-top: 10px">
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <div style="margin-left: 40px;margin-top: 7px; margin-bottom: 5px;">
                                            <input name="submitButton" type="submit" class="custom-button" value="Send"/>
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>                        
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>            
        </div>        
    </body>
</html>