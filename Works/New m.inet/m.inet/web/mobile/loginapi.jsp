<%@page import="com.myapp.struts.util.Utils,com.myapp.struts.session.Constants"%>
<%@page import="com.myapp.struts.client.ClientDAO,com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader"%>

<%
    if (request.getParameter("RequestKey") == null || request.getParameter("RequestCode") == null || request.getParameter("UserId") == null) {
        out.println("Access Denied");
        return;
    }

    String user_name = request.getParameter("UserId");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        out.println("Access Denied");
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserId(user_name.trim().toLowerCase());
        if (clientDTO == null) {
            out.println("Access Denied");
            return;
        }
    }
    
    if (clientDTO != null) {
        String req_key = request.getParameter("RequestKey");
        String req_code = request.getParameter("RequestCode");
        String res_code = Utils.getMD5(req_key, clientDTO.getClientPassword().toLowerCase());
        if (!req_code.equals(res_code)) {
            out.println("Access Denied");
            return;
        }
        if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            out.println("Access Denied");
            return;
        }   
        else
        {
            ClientDAO clDAO = new ClientDAO(); 
            double balance = clDAO.getClientCredit(clientDTO.getId());
            out.println("Welcome "+clientDTO.getClientId());
            out.println("Current Balance: " + balance + " Tk.");            
        }                       
    }    
%>    
