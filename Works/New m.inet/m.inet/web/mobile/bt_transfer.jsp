<%-- 
    Document   : bt_transfer
    Created on : Sep 5, 2013, 4:04:53 PM
    Author     : user
--%>

<%@page import="com.myapp.struts.client.ClientDAO"%>
<%@page import="com.myapp.struts.balancetransfer.BalanceTransferDAO"%>
<%@page import="com.myapp.struts.util.Utils"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.flexi.FlexiDTO"%>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.util.MyAppError,com.myapp.struts.system.SettingsLoader"%>
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader,com.myapp.struts.util.Utils"%>
<%
    boolean api = false;
    if (request.getParameter("FromAPI") != null) {
        api = true;
    }
    if (request.getParameter("RequestKey") == null || request.getParameter("RequestCode") == null || request.getParameter("UserId") == null
            || request.getParameter("MobileNumber") == null || request.getParameter("Amount") == null || request.getParameter("NumberType") == null
            || request.getParameter("AccountType") == null) {
        if (api) {
            out.println("Error=1");
        } else {
            out.println("Access denied!");
        }
        return;
    }

    String user_name = request.getParameter("UserId");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        if (api) {
            out.println("Error=2");
        } else {
            out.println("Access denied!");
        }
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserId(user_name.trim().toLowerCase());
        if (clientDTO == null) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }
    }

    if (clientDTO != null) {

        String req_key = request.getParameter("RequestKey");
        String req_code = request.getParameter("RequestCode");
        String res_code = Utils.getMD5(req_key, clientDTO.getClientPassword().toLowerCase());
        if (!req_code.equals(res_code)) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }

        String number = request.getParameter("MobileNumber");
        String amount_str = request.getParameter("Amount");
        String type_str = request.getParameter("NumberType");
        String account_str = request.getParameter("AccountType");

        int operatorTypeID = 0;
        double surcharge = 0.0;
        if (clientDTO.clientBTActive == 1) {
            operatorTypeID = Constants.BT;
        } else {
            out.println("You are not allowed to transfer balance.");
            return;
        }

        long amount = 0;
        if (amount_str != null) {
            amount = Long.parseLong(amount_str);
        }
        if (amount <= 0) {
            if (api) {
                out.println("Error=4");
            } else {
                out.println("Transfer amount should be greater than 0");
            }
            return;
        }
        int flexi_type = 0;
        if (type_str != null) {
            flexi_type = Integer.parseInt(type_str);
        }
        int account_type = 0;
        if (account_str != null) {
            account_type = Integer.parseInt(account_str);
        }
        BalanceTransferDAO btdao = new BalanceTransferDAO();
        double surchargePercentage = btdao.getSurcharge(amount, flexi_type);
        if (surchargePercentage <= 0.0) {
            out.println("Amount To Transfer is not allowed in this system.");
            return;
        } else {
            surcharge = (surchargePercentage * amount) / 100;
        }

        if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            if (api) {
                out.println("Error=2" + request.getRemoteAddr());
            } else {
                out.println("Access denied!");
            }
            return;
        }
        FlexiDTO dto = new FlexiDTO();
        if (clientDTO.getClientIp() != null && clientDTO.getClientIp().trim().length() > 0 && !request.getRemoteAddr().equals(clientDTO.getClientIp())) {
            if (api) {
                out.println("Error=2" + request.getRemoteAddr());
            } else {
                out.println("Access denied!");
            }
            return;
        }


        FlexiDAO flDAO = new FlexiDAO();
        long curTime = System.currentTimeMillis();
        long lastRefillTime = flDAO.getLastRefillTime(number, curTime);
        if (lastRefillTime > 0) {
            long refillInterval = curTime - lastRefillTime;
            if (refillInterval < (SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000)) {
                if (api) {
                    out.println("Error=6");
                } else {
                    int minute = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000) - refillInterval) / 1000) / 60);
                    int sec = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000) - refillInterval) / 1000) % 60);
                    out.println("You have already sent a request to same number.");
                    out.println("Please wait " + minute + " min " + sec + " sec.");
                }
                return;
            }
        }
        dto.setPhoneNumber(number);
        dto.setAmount(amount);
        dto.setSurcharge(surcharge);
        dto.setRefillType(flexi_type);
        dto.setAccountType(account_type);
        dto.setOperatorTypeID(operatorTypeID);

        LoginDTO login_dto = new LoginDTO();
        login_dto.setId(clientDTO.getId());
        login_dto.setClientParentId(clientDTO.getParentId());
        login_dto.setClientTypeId(clientDTO.getClientTypeId());


        MyAppError error = flDAO.addFlexiRequest(login_dto, dto);
        if (error.getErrorType() == error.NoError) {
            if (api) {
                out.println("RequestID=" + error.getErrorMessage());
            } else {
                ClientDAO clDAO = new ClientDAO();
                double balance = clDAO.getClientCredit(clientDTO.getId());
                out.println("Transfer Request Sent Successfully.");
                out.println(" Current Balance: " + balance + " Tk.");
            }
        } else if (error.getErrorType() == error.ValidationError) {
            if (api) {
                out.println("Error=5");
            } else {
                out.println("Transfer request is failed!");
            }
        } else {
            if (api) {
                out.println("Error=7");
            } else {
                out.println("Transfer request is failed!");
            }
        }
    }
%>