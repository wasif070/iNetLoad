<%@include file="login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.mobile.Decription,com.myapp.struts.flexi.FlexiDTO"%>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.util.MyAppError,com.myapp.struts.system.SettingsLoader"%>
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientDAO,com.myapp.struts.client.ClientLoader"%>
<%
            if (clientDTO != null) {
                if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT || clientDTO.getClientStatus() != Constants.USER_STATUS_ACTIVE) {
                    out.println("Access denied!");
                    return;
                }
                FlexiDTO dto = new FlexiDTO();
                String number = request.getParameter("m_phn_number");
                if (number != null) {
                    number = Decription.decreptedString(request.getParameter("m_phn_number"));
                }
                long amount = 0;
                if (request.getParameter("m_amount") != null) {
                    amount = Long.parseLong(Decription.decreptedString(request.getParameter("m_amount")));
                }
                int flexi_type = 0;
                if (request.getParameter("m_flexi_type") != null) {
                    flexi_type = Integer.parseInt(request.getParameter("m_flexi_type"));
                }

                if (flexi_type < Constants.REFILL_TYPE_PREPAID || flexi_type > Constants.REFILL_TYPE_POSTPAID) {
                    out.println("Select load type");
                    return;
                }
                
                if (number == null || number.length() == 0) {
                    out.println("Insert mobile number");
                    return;
                } else if (number.length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                    out.println("Please check mobile number");
                    return;
                } else if (number.length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                    if (number.startsWith("+")) {
                        number = number.replaceFirst("+", "");
                    }
                    if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("00")) {
                        number = number.replaceFirst("00", "");
                    }					
                    if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("88")) {
                        number = number.replaceFirst("88", "");
                    }
                    number = number.replace("-", "");
                    number = number.replace(" ", "");
                    String prefix = number.substring(0, 3);
                    int operatorTypeID = 0;
                    for (int i = 1; i < Constants.OPERATOR_TYPE_PREFIX.length; i++) {
                        if (prefix.equals(Constants.OPERATOR_TYPE_PREFIX[i])) {
                            operatorTypeID = i;
                            break;
                        }
                    }
                    if (operatorTypeID < Constants.GP) {
                        out.println("Mobile number not valid");
                        return;
                    } else {
                        if (number.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                            dto.setOperatorTypeID(operatorTypeID);
                            dto.setPhoneNumber(number);
                        } else {
                            out.println("Mobile number not valid");
                            return;
                        }
                    }
                }
                if (amount <= 0) {
                    out.println("Amount must be greater than 0");
                    return;
                } else if (flexi_type == Constants.REFILL_TYPE_PREPAID) {
                    if (amount < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || amount > SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount()) {
                        out.println("Amount must be between " + SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() + " and " + SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount());
                        return;
                    }
                } else if (flexi_type == Constants.REFILL_TYPE_POSTPAID) {
                    if (amount < SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() || amount > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                        out.println("Amount must be between " + SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() + " and " + SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount());
                        return;
                    }
                }
                
                FlexiDAO flDAO = new FlexiDAO();
                long curTime = System.currentTimeMillis();
                long lastRefillTime = flDAO.getLastRefillTime(number, curTime);
                if (lastRefillTime > 0) {
                    long refillInterval = curTime - lastRefillTime;
                    if (refillInterval < (SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000)) {
                        int minute = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000) - refillInterval) / 1000) / 60);
                        int sec = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval()*60*1000) - refillInterval) / 1000) % 60);
                        out.println("You have already sent a request to same number.");
                        out.println("Please wait " + minute + " min " + sec + " sec.");
                        return;
                    }
                }
                                
                dto.setAmount(amount);
                dto.setRefillType(flexi_type);
                LoginDTO login_dto = new LoginDTO();
                login_dto.setId(clientDTO.getId());
                login_dto.setClientParentId(clientDTO.getParentId());
                login_dto.setClientTypeId(clientDTO.getClientTypeId());

                MyAppError error = flDAO.addFlexiRequest(login_dto,dto);
                if (error.getErrorType() == error.NoError) {
                    clientDTO = ClientLoader.getInstance().getClientDTOByID(clientDTO.getId());
                    out.println("Request Sent Successfully");
                    out.println("Current Balance: " + clientDTO.getClientCredit() + " Tk.");
                } else {
                    out.println("Sending Request is failed");
                }
            }
%>