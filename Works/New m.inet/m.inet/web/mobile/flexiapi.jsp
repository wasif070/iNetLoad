<%@page import="com.myapp.struts.util.Utils,com.myapp.struts.session.Constants"%>
<%@page import="com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.flexi.FlexiDTO"%>
<%@page import="com.myapp.struts.client.ClientDAO,com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader"%>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.util.MyAppError,com.myapp.struts.system.SettingsLoader"%>

<%
    boolean api = false;
    if (request.getParameter("FromAPI") != null) {
        api = true;
    }
    if (request.getParameter("RequestKey") == null || request.getParameter("RequestCode") == null || request.getParameter("UserId") == null
            || request.getParameter("MobileNumber") == null || request.getParameter("Amount") == null || request.getParameter("NumberType") == null) {
        if (api) {
            out.println("Error=1");
        } else {
            out.println("Access denied!");
        }
        return;
    }

    String user_name = request.getParameter("UserId");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        if (api) {
            out.println("Error=2");
        } else {
            out.println("Access denied!");
        }
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserId(user_name.trim().toLowerCase());
        if (clientDTO == null) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }
    }

    if (clientDTO != null) {
        String req_key = request.getParameter("RequestKey");
        String req_code = request.getParameter("RequestCode");
        String res_code = Utils.getMD5(req_key, clientDTO.getClientPassword().toLowerCase());
        if (!req_code.equals(res_code)) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }

        String number = request.getParameter("MobileNumber");
        String amount_str = request.getParameter("Amount");
        String type_str = request.getParameter("NumberType");
        if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }

        if (clientDTO.getClientIp() != null && clientDTO.getClientIp().trim().length() > 0 && !request.getRemoteAddr().equals(clientDTO.getClientIp())) {
            if (api) {
                out.println("Error=2" + request.getRemoteAddr());
            } else {
                out.println("Access denied!");
            }
            return;
        }

        int flexi_type = 0;
        if (type_str != null) {
            flexi_type = Integer.parseInt(type_str);
        }

        if (flexi_type < Constants.REFILL_TYPE_PREPAID || flexi_type > Constants.REFILL_TYPE_POSTPAID) {
            if (api) {
                out.println("Error=3");
            } else {
                out.println("Please select refill type");
            }
            return;
        }

        FlexiDTO dto = new FlexiDTO();

        if (number.length() == 0) {
            if (api) {
                out.println("Error=3");
            } else {
                out.println("Please enter mobile number");
            }
            return;
        } else if (number.length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
            if (api) {
                out.println("Error=3");
            } else {
                out.println("Please check mobile number");
            }
            return;
        } else if (number.length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
            if (number.startsWith("+")) {
                number = number.replaceFirst("+", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("00")) {
                number = number.replaceFirst("00", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("88")) {
                number = number.replaceFirst("88", "");
            }
            number = number.replace("-", "");
            number = number.replace(" ", "");
            String prefix = number.substring(0, 3);
            int operatorTypeID = 0;
            for (int i = 1; i < Constants.OPERATOR_TYPE_PREFIX.length; i++) {
                if (prefix.equals(Constants.OPERATOR_TYPE_PREFIX[i])) {
                    operatorTypeID = i;
                    break;
                }
            }
            if (operatorTypeID < Constants.GP) {
                if (api) {
                    out.println("Error=3");
                } else {
                    out.println("Mobile number is invalid!");
                }
                return;
            } else {
                if (number.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                    dto.setOperatorTypeID(operatorTypeID);
                    dto.setPhoneNumber(number);
                } else {
                    if (api) {
                        out.println("Error=3");
                    } else {
                        out.println("Mobile number is invalid!");
                    }
                    return;
                }
            }
        }
        long amount = 0;
        if (amount_str != null) {
            amount = Long.parseLong(amount_str);
        }
        if (amount <= 0) {
            if (api) {
                out.println("Error=4");
            } else {
                out.println("Refill amount should be greater than 0");
            }
            return;
        } else if (flexi_type == Constants.REFILL_TYPE_PREPAID) {
            if (amount < SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() || amount > SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount()) {
                if (api) {
                    out.println("Error=4");
                } else {
                    out.println("Refill amount should be between " + SettingsLoader.getInstance().getSettingsDTO().getPrepaidMinRefillAmount() + " and " + SettingsLoader.getInstance().getSettingsDTO().getPrepaidMaxRefillAmount());
                }
                return;
            }
        } else if (flexi_type == Constants.REFILL_TYPE_POSTPAID) {
            if (amount < SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() || amount > SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount()) {
                if (api) {
                    out.println("Error=4");
                } else {
                    out.println("Refill amount should be between " + SettingsLoader.getInstance().getSettingsDTO().getPostpaidMinRefillAmount() + " and " + SettingsLoader.getInstance().getSettingsDTO().getPostpaidMaxRefillAmount());
                }
                return;
            }
        }

        FlexiDAO flDAO = new FlexiDAO();
        long curTime = System.currentTimeMillis();
        long lastRefillTime = flDAO.getLastRefillTime(number, curTime);
        if (lastRefillTime > 0) {
            long refillInterval = curTime - lastRefillTime;
            if (refillInterval < (SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000)) {
                if (api) {
                    out.println("Error=6");
                } else {
                    int minute = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000) - refillInterval) / 1000) / 60);
                    int sec = (int) ((((SettingsLoader.getInstance().getSettingsDTO().getSameNumberRefillInterval() * 60 * 1000) - refillInterval) / 1000) % 60);
                    out.println("You have already sent a request to same number.");
                    out.println("Please wait " + minute + " min " + sec + " sec.");
                }
                return;
            }
        }
        dto.setAmount(amount);
        dto.setRefillType(flexi_type);
        LoginDTO login_dto = new LoginDTO();
        login_dto.setId(clientDTO.getId());
        login_dto.setClientParentId(clientDTO.getParentId());
        login_dto.setClientTypeId(clientDTO.getClientTypeId());

        MyAppError error = flDAO.addFlexiRequest(login_dto, dto);
        if (error.getErrorType() == error.NoError) {
            if (api) {
                out.println("RequestID=" + error.getErrorMessage());
            } else {
                ClientDAO clDAO = new ClientDAO(); 
                double balance = clDAO.getClientCredit(clientDTO.getId());
                out.println("Refill Request Sent Successfully.");
                out.println(" Current Balance: " + balance + " Tk.");
            }
        } else if (error.getErrorType() == error.ValidationError) {
            if (api) {
                out.println("Error=5");
            } else {
                out.println("Refill request is failed!");
            }
        } else {
            if (api) {
                out.println("Error=7");
            } else {
                out.println("Refill request is failed!");
            }
        }
    }
%>