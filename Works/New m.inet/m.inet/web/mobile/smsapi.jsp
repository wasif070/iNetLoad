<%-- 
    Document   : index
    Created on : Aug 20, 2013, 9:46:13 AM
    Author     : user
--%>

<%@page import="com.myapp.struts.system.SettingsLoader"%>
<%@page import="com.myapp.struts.client.ClientDAO"%>
<%@page import="com.myapp.struts.util.MyAppError"%>
<%@page import="com.myapp.struts.login.LoginDTO"%>
<%@page import="com.myapp.struts.session.Constants"%>
<%@page import="com.myapp.struts.util.Utils"%>
<%@page import="com.myapp.struts.client.ClientLoader"%>
<%@page import="com.myapp.struts.client.ClientDTO"%>
<%@page import="com.myapp.struts.sms.SMSDAO"%>
<%@page import="com.myapp.struts.sms.SMSDTO"%>

<%
    boolean api = false;
    if (request.getParameter("FromAPI") != null) {
        api = true;
    }
    if (request.getParameter("RequestKey") == null || request.getParameter("RequestCode") == null || request.getParameter("UserId") == null
            || request.getParameter("MobileNumber") == null || request.getParameter("CountryCode") == null
            || request.getParameter("Message") == null) {
        if (api) {
            out.println("Error=1");
        } else {
            out.println("Access denied!");
        }
        return;
    }

    String user_name = request.getParameter("UserId");
    ClientDTO clientDTO = null;
    if (user_name.length() == 0) {
        if (api) {
            out.println("Error=2");
        } else {
            out.println("Access denied!");
        }
        return;
    } else {
        clientDTO = ClientLoader.getInstance().getClientDTOByUserId(user_name.trim().toLowerCase());
        if (clientDTO == null) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }
    }
    if (clientDTO != null) {
        String req_key = request.getParameter("RequestKey");
        String req_code = request.getParameter("RequestCode");
        String res_code = Utils.getMD5(req_key, clientDTO.getClientPassword().toLowerCase());
        if (!req_code.equals(res_code)) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }

        String number = request.getParameter("MobileNumber");
        String message = request.getParameter("Message");
        String countryCode = request.getParameter("CountryCode");
        if (clientDTO.getClientTypeId() != Constants.CLIENT_TYPE_AGENT) {
            if (api) {
                out.println("Error=2");
            } else {
                out.println("Access denied!");
            }
            return;
        }

        int operatorTypeID = 0;
        if (clientDTO.clientSMSActive == 1) {
            operatorTypeID = Constants.SMS;
        } else {
            out.println("You are not allowed to transfer balance.");
            return;
        }
        if (countryCode != null && countryCode.length() <= 0) {
            if (api) {
                out.println("Error=4");
            } else {
                out.println("Country is required!");
            }
            return;
        } else {
            countryCode = countryCode.replaceFirst(" ", "+");
        }
        if (message != null && message.length() <= 0) {
            if (api) {
                out.println("Error=4");
            } else {
                out.println("Message is required!");
            }
            return;
        }
        SMSDAO btdao = new SMSDAO();
        double smsRate = btdao.getSMSRate(countryCode);
        double amount = 0;
        if (smsRate <= 0.0) {
            out.println("SMS for " + countryCode + " is not allowed in this system.");
            return;
        } else {
            amount = (int) Math.ceil(((double) message.length() / Constants.SMS_MAX_LENGTH)) * smsRate;
        }

        if (clientDTO.getClientIp() != null && clientDTO.getClientIp().trim().length() > 0 && !request.getRemoteAddr().equals(clientDTO.getClientIp())) {
            if (api) {
                out.println("Error=2" + request.getRemoteAddr());
            } else {
                out.println("Access denied!");
            }
            return;
        }

        SMSDTO dto = new SMSDTO();

        if (number.length() == 0) {
            if (api) {
                out.println("Error=3");
            } else {
                out.println("Please enter mobile number");
            }
            return;
        } else {

            if (number.startsWith("+")) {
                number = number.replaceFirst("+", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("00")) {
                number = number.replaceFirst("00", "");
            }
            if (number.length() > Constants.VALID_PHONE_NUMBER_LENGTH && number.startsWith("88")) {
                number = number.replaceFirst("88", "");
            }
            number = number.replace("-", "");
            number = number.replace(" ", "");
        }

        SMSDAO flDAO = new SMSDAO();
        dto.setMessage(message);
        dto.setAmount(amount);
        dto.setPhoneNumber(countryCode + "" + number);
        dto.setOperatorTypeID(operatorTypeID);
        dto.setSenderName(clientDTO.getSmsSender());

        LoginDTO login_dto = new LoginDTO();
        login_dto.setId(clientDTO.getId());
        login_dto.setClientParentId(clientDTO.getParentId());
        login_dto.setClientTypeId(clientDTO.getClientTypeId());

        MyAppError error = flDAO.addSMSRequest(login_dto, dto);
        if (error.getErrorType() == error.NoError) {
            if (api) {
                out.println("RequestID=" + error.getErrorMessage());
            } else {
                ClientDAO clDAO = new ClientDAO();
                double balance = clDAO.getClientCredit(clientDTO.getId());
                out.println("SMS Sent Successfully.");
                out.println(" Current Balance: " + balance + " Tk.");
            }
        } else if (error.getErrorType() == error.ValidationError) {
            if (api) {
                out.println("Error=5");
            } else {
                out.println("SMS request is failed!" + error.getErrorMessage());
            }
        } else {
            if (api) {
                out.println("Error=7");
            } else {
                out.println("SMS request is failed!" + error.getErrorMessage());
            }
        }
    }
%>