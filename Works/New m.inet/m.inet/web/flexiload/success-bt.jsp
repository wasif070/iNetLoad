<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.flexi.FlexiDTO,com.myapp.struts.session.Constants" %>

<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Balance Transfer Request Processing</title>
    </head>
    <body>

        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>
            <div class="left_menu fl_left"><%@include file="../includes/menu.jsp"%></div>
            <div class="body_content fl_left">
                <div class="blank-height"></div>
                <div class="body">
                    <%
                                FlexiDTO dto = (FlexiDTO) request.getSession(true).getAttribute(Constants.REFILL_SUCCESS_DTO);
                                String phone_no = dto.getPhoneNumber();
                                String amount = String.valueOf(dto.getAmount());
                                String surcharge = String.valueOf(dto.getSurcharge());
                    %>
                    <h1>Balance Transfer Request Was Sent Successfully!!!</h1>
                    <p>Account Number: <%=phone_no%></p>
                    <p>Amount To Transfer: <%=amount%></p>
                    <p>Balance Transfer Surcharge: <%=surcharge%></p>
                    <p>Your Request Will Be Processed Within A Moment....</p>
                    <p>Thank You.</p>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>