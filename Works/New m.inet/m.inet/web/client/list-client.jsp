<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientDTO,com.myapp.struts.client.ClientLoader,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<%@page import="com.myapp.struts.user.UserDTO,com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../stylesheets/display-style.css" rel="stylesheet" type="text/css" />
<%
            int recordPerPage = 100;
            int list_all = 0;

            if (request.getParameter("list_all") != null) {
               list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            String action="/client/listClient.do?list_all=0";
%>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Client List</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/search-header.jsp"%></div>
            <%
                    if (login_dto.getRoleID() != Constants.SUPER_ADMIN_ROLE && perDTO!=null && !perDTO.CLIENT
                            && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                            && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                            && ((login_dto.getClientTypeId() != Constants.CLIENT_TYPE_RESELLER2) || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId()!=Integer.parseInt(request.getParameter("parentId"))))
                            ) {
                            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
                            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED,"yes");
                            response.sendRedirect("../index.do");
                            return;
                    }
            %>
            <div class="body_content fl_left">               
                <div class="view-page-body">
                    <div class="fl_right height-5px"></div>
                    <div class="full-div">
                        <div class="half-div">
                            <html:form action="<%=action%>" method="post">
                                <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th>Client ID</th>
                                        <td>
                                            <html:text property="clientId">
                                            </html:text>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Client Type</th>
                                        <td>
                                            <html:select property="clientTypeId">
                                                <html:option value="0">Select</html:option>
                                                <%
                                                   if(login_dto.getIsUser())
                                                   {    
                                                %>                                             
                                                <html:option value="1">Reseller</html:option>
                                                <html:option value="3">Reseller3</html:option>
                                                <html:option value="4">Reseller2</html:option>
                                                <html:option value="2">Agent</html:option>
                                                <%
                                                    }
                                                    else
                                                    {                                                        
                                                     if (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER) {
                                                %>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER)%>">Reseller</html:option>
                                                <%
                                                     }
                                                     if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3)
                                                     {
                                                %>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_RESELLER2)%>">Reseller-2</html:option>
                                                <%
                                                     }
                                                     if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2)
                                                     {
                                                %>
                                                <html:option value="<%=String.valueOf(Constants.CLIENT_TYPE_AGENT)%>">Agent</html:option>
                                                <%
                                                     }
                                                                                                        } 
                                                %>                                                
                                            </html:select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <div class="full-div" style="text-align: center;">
                                                <div class="clear height-5px"></div>
                                                <input type="submit" class="search-button" style="width:70px;"  name="startSearching" value="Search" />
                                                <input type="submit" class="search-button" style="margin-left:5px; width:70px;" value="Reset" />

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </html:form>
                        </div>
                    </div>
                    <%
                       if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2){
                    %>
                    <div class="clear height-20px"><a href="../client/add-client.jsp" style="font-size: 11px; font-weight: bold;text-decoration: underline; font-style: italic;">Add Client</a></div>
                    <%
                       }
                    %>
                    <div class="clear height-10px">
                        <%
                          if(request.getSession(true).getAttribute(Constants.MESSAGE)!=null)
                          {
                            out.print(request.getSession(true).getAttribute(Constants.MESSAGE));
                          }
                          request.getSession(true).removeAttribute(Constants.MESSAGE);
                          boolean clientRechargePer = false;
                          boolean clientReturnPer = false;
                          boolean clientEditPer = false;
                          if(((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) || (login_dto.getIsUser() && perDTO.CLIENT_EDIT)
                          || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER)
                          || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3)
                          || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2))
                          && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                          {
                               clientEditPer = true;
                          }
                          if((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 || login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                          {
                              clientRechargePer = true;
                              clientReturnPer = true;
                          }
                          else if(login_dto.getIsUser() && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)
                          {
                              if(perDTO.CLIENT_RECHARGE)
                              {
                                 clientRechargePer = true;
                              }
                              if(perDTO.CLIENT_RETURN)
                              {
                                 clientReturnPer = true;
                              }
                          }
                        %>
                    </div>
                    <c:set var="is_user" scope="page" value="<%=login_dto.getIsUser()%>"/>
                    <c:set var="clientEdit" scope="page" value="<%=clientEditPer%>"/>
                    <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.ClientForm.clientList" pagesize="<%=recordPerPage%>">
                        <display:setProperty name="paging.banner.item_name" value="Client" />
                        <display:setProperty name="paging.banner.items_name" value="Clients" />
                        <display:column title="Client ID" sortable="true" class="left-align">
                            <c:choose>
                                <c:when test="${is_user==true && (data.clientTypeId==1 || data.clientTypeId==3 || data.clientTypeId==4)}"><a href="../client/listClient.do?list_all=1&parentId=${data.id}">${data.clientId}</a></c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${data.clientTypeId==1 || data.clientTypeId==3 || data.clientTypeId==4}"><font color="blue">${data.clientId}</font></c:when>
                                        <c:otherwise>${data.clientId} </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </display:column>
                        <display:column property="clientTypeName" sortProperty="clientType" title="Type" sortable="true" class="left-align" />
                        <display:column title="Status" sortProperty="clientStatus" sortable="true" class="left-align" >
                            <c:choose>
                                <c:when test="${data.clientStatus==1}">Active</c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${data.clientStatus==0}">Inactive</c:when>
                                        <c:otherwise>Block</c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </display:column>                        
                        <display:column property="clientCredit" title="Balance" format="{0,number,0}" sortable="true" class="right-align" />
                        <display:column title="Action" headerClass="black" sortable="false">
                            <c:choose>
                                <c:when test="${client_status==0}">Recharge&nbsp;|&nbsp;Update</c:when>
                                <c:otherwise>
                                    <a href="../client/rechargeClient.do?id=${data.id}&name=${data.clientId}&clientTypeId=${data.clientTypeId}&parentId=${data.parentId}&recharge=1">
                                        Recharge
                                    </a>
                                    &nbsp;|&nbsp;
                                    <a href="../client/getClient.do?id=${data.id}&name=${data.clientId}&clientTypeId=${data.clientTypeId}&parentId=${data.parentId}&recharge=0">
                                        Update
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </display:column>
                    </display:table>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
