<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.client.ClientLoader,com.myapp.struts.client.ClientDTO,com.myapp.struts.flexi.FlexiDAO,com.myapp.struts.client.ClientDAO,com.myapp.struts.system.SettingsLoader,com.myapp.struts.util.Utils" %>
<html>
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO().getBrandName()%> :: Home</title>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/header.jsp"%></div>            
            <div class="body_content fl_left">                             
                <div class="body">
                    <table class="login_table" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <td colspan="2" align="center">
                                    <div class="clear height-5px"></div>
                                    <div class="full-div">
                                        <strong><%=SettingsLoader.getInstance().getSettingsDTO().getWelcomeNote()%></strong>
                                    </div>
                                    <div class="clear height-5px"></div>
                                    <div class="full-div">
                                        <%
                                            if (login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                                                if (login_dto.getIsUser()) {
                                        %>
                                        <a href='../user/getUser.do?id=<%=login_dto.getId()%>&name=<%=login_dto.getClientId()%>' style="font-weight: bold">[ Edit Profile ]</a>
                                        <%
                                        } else {
                                        %>
                                        <a href='../client/getClient.do?id=<%=login_dto.getId()%>&name=<%=login_dto.getClientId()%>' style="font-weight: bold">[ Edit Profile ]</a>
                                        <%
                                                }
                                            }
                                        %>
                                    </div>
                                    <div class="clear height-10px"></div>
                                    <%
                                        ClientDAO dao = new ClientDAO();
                                    %>
                                    <div class="full-div"><a href="../flexiload/listFlexiload.do?list_all=1&requestType=<%=Constants.INETLOAD_STATUS_SUBMITTED%>">Pending Load Requests (<%=dao.countPendingRequests(login_dto)%>) </a></div>
                                    <div class="clear height-10px"></div>
                                    <%
                                        if (!login_dto.getIsUser()) {
                                            LoginDTO l_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
                                            double credit = dao.getClientCredit(l_dto.getId());
                                    %>
                                    <div class="full-div" style="font-size: 11px; font-weight: bold; color: #44c;">
                                        My Credit Amount:&nbsp;<%=credit%>
                                    </div>                                    
                                    <%}%>  
                                    <div class="clear height-10px"></div>
                                    <%
                                       if(login_dto.getClientTypeId() == Constants.CLIENT_TYPE_AGENT && login_dto.getIsSpecialAgent()){
                                    %>
                                    <div class="clear height-20px"><a href="../rechargecard/refill-card.jsp" style="font-size: 11px; font-weight: bold;text-decoration: underline; font-style: italic;">Refill By Card</a></div>
                                    <div class="clear height-20px"><a href="../rechargecard/recharge-card.jsp" style="font-size: 11px; font-weight: bold;text-decoration: underline; font-style: italic;">Recharge Account</a></div>
                                    <%
                                       }
                                    %>  
                                    <div class="clear height-10px"></div>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>               
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>