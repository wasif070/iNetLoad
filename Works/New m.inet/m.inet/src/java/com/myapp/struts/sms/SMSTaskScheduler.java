/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Wasif Islam
 */
public class SMSTaskScheduler {

    public MyAppError sendSMSRequest(LoginDTO login_dto, SMSDTO dto) {
        SMSDAO smsDAO = new SMSDAO();
        return smsDAO.addSMSRequest(login_dto, dto);
    }
    
      /*  public ArrayList<SMSDTO> getSMSDTOs(LoginDTO login_dto, SMSDTO sdto, int flexiType) {
        SMSDAO SMSDAO = new SMSDAO();
        return SMSDAO.getSMSDTOs(login_dto, sdto, flexiType);
    }
        */
        public ArrayList<SMSRequestDTO> getSMSRequests (ClientDTO clientDTO, int request_type){
            SMSDAO SMSDAO = new SMSDAO();
            return SMSDAO.getSMSRequests(clientDTO, request_type);
            
        }
}
