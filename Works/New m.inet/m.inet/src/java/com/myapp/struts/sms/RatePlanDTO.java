/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class RatePlanDTO {

    private String country_code;
    private double rate_amount;

    public RatePlanDTO() {
    }
    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public double getRate_amount() {
        return rate_amount;
    }

    public void setRate_amount(double rate_amount) {
        this.rate_amount = rate_amount;
    }
}