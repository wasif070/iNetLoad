package com.myapp.struts.sms;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.flexi.*;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;


import org.apache.log4j.Logger;

public class ListSMSAction
        extends Action {

    static Logger logger = Logger.getLogger(ListSMSAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.REFILL && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int list_all = 0;
                int pageNo = 1;
                int requestType = Constants.INETLOAD_STATUS_SUBMITTED;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }

                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("requestType") != null) {
                    requestType = Integer.parseInt(request.getParameter("requestType"));
                }
                SMSTaskScheduler scheduler = new SMSTaskScheduler();
                SMSForm smsForm = (SMSForm) form;
                SMSDTO sdto = new SMSDTO();
                if (list_all == 0) {
                if (smsForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.FLEXI_RECORD_PER_PAGE, smsForm.getRecordPerPage());
                }
                    if (smsForm.getPhoneNumber() != null && smsForm.getPhoneNumber().trim().length() > 0) {
                        sdto.searchWithPhoneNumber = true;
                        sdto.setPhoneNumber(smsForm.getPhoneNumber());
                    }
                    if (smsForm.getFromClientId() != null && smsForm.getFromClientId().trim().length() > 0) {
                        sdto.searchWithFromClientId = true;
                        sdto.setFromClientId(smsForm.getFromClientId().toLowerCase());
                    }
                 
                    if (smsForm.getSign() > 0) {
                        sdto.searchWithAmount = true;
                        sdto.setSign(smsForm.getSign());
                        sdto.setAmount(smsForm.getAmount());
                    }
                    sdto.setStartDay(smsForm.getStartDay());
                    sdto.setStartMonth(smsForm.getStartMonth());
                    sdto.setStartYear(smsForm.getStartYear());
                    sdto.setEndDay(smsForm.getEndDay());
                    sdto.setEndMonth(smsForm.getEndMonth());
                    sdto.setEndYear(smsForm.getEndYear());
                }
                else
                {
                    if (request.getSession(true).getAttribute(Constants.FLEXI_RECORD_PER_PAGE) != null) {
                        smsForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_RECORD_PER_PAGE).toString()));
                    }
                    String dateParts[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    String dateParts1[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) -(24*60*60*1000));
                    smsForm.setStartDay(Integer.parseInt(dateParts1[0]));
                    smsForm.setStartMonth(Integer.parseInt(dateParts1[1]));
                    smsForm.setStartYear(Integer.parseInt(dateParts1[2]));
                    smsForm.setEndDay(Integer.parseInt(dateParts[0]));
                    smsForm.setEndMonth(Integer.parseInt(dateParts[1]));
                    smsForm.setEndYear(Integer.parseInt(dateParts[2])); 
                    sdto.setStartDay(smsForm.getStartDay());
                    sdto.setStartMonth(smsForm.getStartMonth());
                    sdto.setStartYear(smsForm.getStartYear());
                    sdto.setEndDay(smsForm.getEndDay());
                    sdto.setEndMonth(smsForm.getEndMonth());
                    sdto.setEndYear(smsForm.getEndYear());                    
                }
                //smsForm.setSMSList(scheduler.getSMSDTOs(login_dto, sdto, requestType));
                ClientDTO cdto = ClientLoader.getInstance().getClientDTOByUserId(login_dto.getClientId());
                smsForm.setSmsList(scheduler.getSMSRequests(cdto, requestType));
                if (smsForm.getSmsList() != null && smsForm.getSmsList().size() > 0 && smsForm.getSmsList().size() <= (smsForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
