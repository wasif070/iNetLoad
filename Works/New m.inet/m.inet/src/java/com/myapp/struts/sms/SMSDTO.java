/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

/**
 *
 * @author user
 */
public class SMSDTO {

    private String message;
    private String phoneNumber;
    private int operatorTypeID;
    private double amount;
    private String countryCode;
    private String senderName;
    private String smsRequestTypeName;
    private long smsRequestTimeVal;
    private int smsRequestType;
    public boolean searchWithPhoneNumber = false;
    public boolean searchWithFromClientId = false;
    public boolean searchWithAmount = false;
    private String fromClientId;
    private int sign;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;

    public SMSDTO() {
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public String getFromClientId() {
        return fromClientId;
    }

    public void setFromClientId(String fromClientId) {
        this.fromClientId = fromClientId;
    }

    public int getSmsRequestType() {
        return smsRequestType;
    }

    public void setSmsRequestType(int smsRequestType) {
        this.smsRequestType = smsRequestType;
    }

    public long getSmsRequestTimeVal() {
        return smsRequestTimeVal;
    }

    public void setSmsRequestTimeVal(long smsRequestTimeVal) {
        this.smsRequestTimeVal = smsRequestTimeVal;
    }

    public String getSmsRequestTypeName() {
        return smsRequestTypeName;
    }

    public void setSmsRequestTypeName(String smsRequestTypeName) {
        this.smsRequestTypeName = smsRequestTypeName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getOperatorTypeID() {
        return operatorTypeID;
    }

    public void setOperatorTypeID(int operatorTypeID) {
        this.operatorTypeID = operatorTypeID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}
