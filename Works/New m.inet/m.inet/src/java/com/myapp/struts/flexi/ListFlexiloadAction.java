package com.myapp.struts.flexi;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;


import org.apache.log4j.Logger;

public class ListFlexiloadAction
        extends Action {

    static Logger logger = Logger.getLogger(ListFlexiloadAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            PermissionDTO userPerDTO = null;
            if (!login_dto.getIsUser() || login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE || ((userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.REFILL && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE)) {
                int list_all = 0;
                int pageNo = 1;
                int requestType = Constants.INETLOAD_STATUS_SUBMITTED;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }

                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
                if (request.getParameter("requestType") != null) {
                    requestType = Integer.parseInt(request.getParameter("requestType"));
                }
                FlexiTaskScheduler scheduler = new FlexiTaskScheduler();
                FlexiForm flexiForm = (FlexiForm) form;
                FlexiDTO fdto = new FlexiDTO();
                if (list_all == 0) {
                if (flexiForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.FLEXI_RECORD_PER_PAGE, flexiForm.getRecordPerPage());
                }
                    if (flexiForm.getPhoneNumber() != null && flexiForm.getPhoneNumber().trim().length() > 0) {
                        fdto.searchWithPhoneNumber = true;
                        fdto.setPhoneNumber(flexiForm.getPhoneNumber());
                    }
                    if (flexiForm.getFromClientId() != null && flexiForm.getFromClientId().trim().length() > 0) {
                        fdto.searchWithFromClientId = true;
                        fdto.setFromClientId(flexiForm.getFromClientId().toLowerCase());
                    }
                    if (flexiForm.getTransactionId() != null && flexiForm.getTransactionId().trim().length() > 0) {
                        fdto.searchWithTransactionId = true;
                        fdto.setTransactionId(flexiForm.getTransactionId());
                    }
                    if (flexiForm.getFlexiRequestId() != null && flexiForm.getFlexiRequestId().trim().length() > 0) {
                        fdto.searchWithRequestId = true;
                        fdto.setFlexiRequestId(flexiForm.getFlexiRequestId());
                    }                    
                    if (flexiForm.getSign() > 0) {
                        fdto.searchWithAmount = true;
                        fdto.setSign(flexiForm.getSign());
                        fdto.setAmount(flexiForm.getAmount());
                    }
                    fdto.setStartDay(flexiForm.getStartDay());
                    fdto.setStartMonth(flexiForm.getStartMonth());
                    fdto.setStartYear(flexiForm.getStartYear());
                    fdto.setEndDay(flexiForm.getEndDay());
                    fdto.setEndMonth(flexiForm.getEndMonth());
                    fdto.setEndYear(flexiForm.getEndYear());
                }
                else
                {
                    if (request.getSession(true).getAttribute(Constants.FLEXI_RECORD_PER_PAGE) != null) {
                        flexiForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.FLEXI_RECORD_PER_PAGE).toString()));
                    }
                    String dateParts[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000));
                    String dateParts1[] = Utils.getDateParts(System.currentTimeMillis() + (SettingsLoader.getInstance().getSettingsDTO().getOffsetFromServerTime() * 60 * 60 * 1000) -(24*60*60*1000));
                    flexiForm.setStartDay(Integer.parseInt(dateParts1[0]));
                    flexiForm.setStartMonth(Integer.parseInt(dateParts1[1]));
                    flexiForm.setStartYear(Integer.parseInt(dateParts1[2]));
                    flexiForm.setEndDay(Integer.parseInt(dateParts[0]));
                    flexiForm.setEndMonth(Integer.parseInt(dateParts[1]));
                    flexiForm.setEndYear(Integer.parseInt(dateParts[2])); 
                    fdto.setStartDay(flexiForm.getStartDay());
                    fdto.setStartMonth(flexiForm.getStartMonth());
                    fdto.setStartYear(flexiForm.getStartYear());
                    fdto.setEndDay(flexiForm.getEndDay());
                    fdto.setEndMonth(flexiForm.getEndMonth());
                    fdto.setEndYear(flexiForm.getEndYear());                    
                }
                flexiForm.setFlexiList(scheduler.getFlexiDTOs(login_dto, fdto, requestType));
                if (flexiForm.getFlexiList() != null && flexiForm.getFlexiList().size() > 0 && flexiForm.getFlexiList().size() <= (flexiForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
