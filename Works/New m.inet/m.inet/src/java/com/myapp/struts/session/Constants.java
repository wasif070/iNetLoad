package com.myapp.struts.session;

public class Constants {

    public final static int CHECK_VALIDATION = 1;
    /* Action value */
    public final static int ADD = 1;
    public final static int EDIT = 2;
    public final static int UPDATE = 3;
    public final static int DELETE = 4;
    public final static int RECHARGE = 5;
    public final static int RETURN = 6;
    public final static int RECEIVE = 7;
    public final static int ACTIVATION = 8;
    public final static int DEACTIVATION = 9;
    public final static int BLOCK = 10;
    public final static String MESSAGE = "message_str";
    /*Login related constants*/
    public final static String LOGIN_DTO = "LOGIN_DTO";
    public final static String LOGIN_SUCCESS = "LOGIN_SUCCESS";
    public final static String LOGIN_TIMEOUT = "Your Login Time Is Expired!!!";
    public final static String LOGIN_ACCESS_DENIED = "Access Denied!!!";
    public final static long LOGIN_EXPIRE_TIME = 3 * 60 * 60 * 1000L;
    /*User related constants*/
    public final static String USER_LIST = "USER_LIST";
    public final static String USER_ID_LIST = "USER_ID_LIST";
    public final static String USER_RECORD_PER_PAGE = "USER_RECORD_PER_PAGE";
    public final static String ROLE_RECORD_PER_PAGE = "ROLE_RECORD_PER_PAGE";
    public final static int USER_TYPE = 0;
    public final static int SUPER_ADMIN_ROLE = 0;
    public final static int USER_STATUS_INACTIVE = 0;
    public final static int USER_STATUS_ACTIVE = 1;
    public final static int USER_STATUS_BLOCK = 2;
    public final static String LIVE_STATUS_VALUE[] = {"0", "1", "2"};
    public final static String LIVE_STATUS_STRING[] = {"Inactive", "Active", "Block"};
    /*Client related constants*/
    public final static int CLIENT_TYPE_RESELLER = 1;
    public final static int CLIENT_TYPE_AGENT = 2;
    public final static int CLIENT_TYPE_RESELLER3 = 3;
    public final static int CLIENT_TYPE_RESELLER2 = 4;
    public final static int ROOT_RESELLER_PARENT_ID = 0;
    public final static String[] CLIENT_TYPE = {"", "Reseller", "Agent", "Reseller-3", "Reseller-2"};
    public final static String CLIENT_RECORD_PER_PAGE = "CLIENT_RECORD_PER_PAGE";
    public final static String CLIENT_ID_LIST = "CLIENT_ID_LIST";
    public final static int BALANCE_EQUAL = 1;
    public final static int BALANCE_SMALLER_THAN = 2;
    public final static int BALANCE_GREATER_THAN = 3;
    /*Recharge Card Group related constants*/
    public final static String RCG_RECORD_PER_PAGE = "RCG_RECORD_PER_PAGE";
    public final static String RCG_ID_LIST = "RCG_ID_LIST";
    public final static String RC_RECORD_PER_PAGE = "RC_RECORD_PER_PAGE";
    public final static String RC_ID_LIST = "RC_ID_LIST";
    public final static String RC_USAGE_RECORD_PER_PAGE = "RC_USAGE_RECORD_PER_PAGE";
    public final static int COUNTRY_ID_BAN = 0;
    public final static int COUNTRY_ID_IND = 1;
    public final static int COUNTRY_ID_NEP = 2;
    public final static int COUNTRY_ID_PAK = 3;
    public final static String COUNTRY_ID_VALUE[] = {"0", "1", "2", "3"};
    public final static String COUNTRY_ID_STRING[] = {"BAN", "IND", "NEP", "PAK"};
    public final static int PACKAGE_ID_NORMAL = 0;
    public final static int PACKAGE_ID_SPECIAL = 1;
    public final static String PACKAGE_ID_VALUE[] = {"0", "1"};
    public final static String PACKAGE_ID_STRING[] = {"Normal", "Special"};
    public final static String CARD_SHOP_RECORD_PER_PAGE = "CARD_SHOP_RECORD_PER_PAGE";
    public final static String CARD_SHOP_ID_LIST = "CARD_SHOP_ID_LIST";
    public final static long CARD_BASE = 100000;
    public final static String CARD_SHOP_SUMMERY_RECORD_PER_PAGE = "CARD_SHOP_SUMMERY_RECORD_PER_PAGE";
    /*Payment related constants*/
    public final static int PAYMENT_TYPE_PAID = 1;
    public final static int PAYMENT_TYPE_RECEIVED = 2;
    public final static int PAYMENT_TYPE_RETURNED = 3;
    public final static int PAYMENT_TYPE_CANCELLED = 4;
    public final static int PAYMENT_TYPE_INITIAL = 5;
    public final static String[] PAYMENT_TYPE = {"", "Paid", "Received", "Returned", "Cancelled", "Initial Payment", "Pending Credit", "Approved Credit"};
    public final static String INITIAL_CREDIT = "Initial Credit";
    public final static String PAYMENT_MADE_RECORD_PER_PAGE = "PAYMENT_MADE_RECORD_PER_PAGE";
    public final static String PAYMENT_RECEIVED_RECORD_PER_PAGE = "PAYMENT_RECEIVED_RECORD_PER_PAGE";
    public final static String PAYMENT_PAID_RECORD_PER_PAGE = "PAYMENT_PAID_RECORD_PER_PAGE";
    public final static String PAYMENT_SUMMERY_RECORD_PER_PAGE = "PAYMENT_SUMMERY_RECORD_PER_PAGE";
    public final static String PAYMENT_CLIENT_SUMMERY_RECORD_PER_PAGE = "PAYMENT_CLIENT_SUMMERY_RECORD_PER_PAGE";
    public final static String MY_ACCOUNT_SUMMERY_RECORD_PER_PAGE = "MY_ACCOUNT_SUMMERY_RECORD_PER_PAGE";
    /*Operator related constants*/
    public final static int GP = 1;
    public final static int BL = 2;
    public final static int WR = 3;
    public final static int RB = 4;
    public final static int TT = 5;
    public final static int CC = 6;
    public final static int CARD = 7;
    public final static int BT = 8;
    public final static int SMS = 9;
    public final static String[] OPERATOR_TYPE_NAME = {"All", "GrameenPhone", "BanglaLink", "Airtel", "Robi", "TeleTalk", "CityCell", "Scratch Card", "Balance Transfer", "SMS"};
    public final static String[] OPERATOR_TYPE_PREFIX = {"", "017", "019", "016", "018", "015", "011"};
    /*Flexi SIM related constants*/
    public final static String FLEXI_SIM_RECORD_PER_PAGE = "FLEXI_SIM_RECORD_PER_PAGE";
    public final static String FLEXI_SIM_ID_LIST = "FLEXI_SIM_ID_LIST";
    public final static String FLEXI_SIM_HISTORY_RECORD_PER_PAGE = "FLEXI_SIM_HISTORY_RECORD_PER_PAGE";
    public final static String FLEXI_SIM_HISTORY_ID_LIST = "FLEXI_SIM_HISTORY_ID_LIST";
    public final static String FLEXI_MESSAGE_LOG_RECORD_PER_PAGE = "FLEXI_MESSAGE_LOG_RECORD_PER_PAGE";
    public final static String FLEXI_MESSAGE_LOG_ID_LIST = "FLEXI_MESSAGE_LOG_ID_LIST";
    public final static int FLEXI_SIM_STATUS_DELETED = -1;
    public final static int FLEXI_SIM_STATUS_INACTIVE = 0;
    public final static int FLEXI_SIM_STATUS_ACTIVE = 1;
    public final static int FLEXI_STATUS_INACTIVE = 0;
    public final static int FLEXI_STATUS_ACTIVE = 1;
    public final static String FLEXI_SIM_STATUS_VALUE[] = {"0", "1"};
    public final static String FLEXI_SIM_STATUS_STRING[] = {"Inactive", "Active"};
    /*Flexi Request related constants*/
    public final static String FLEXI_RECORD_PER_PAGE = "FLEXI_RECORD_PER_PAGE";
    public final static String FLEXI_SUMMERY_RECORD_PER_PAGE = "FLEXI_SUMMERY_RECORD_PER_PAGE";
    public final static int INETLOAD_STATUS_SUBMITTED = 1;
    public final static int INETLOAD_STATUS_SUCCESSFUL = 2;
    public final static int INETLOAD_STATUS_REJECTED = 3;
    public final static int INETLOAD_STATUS_PENDING = 4;
    public final static String[] INETLOAD_STATUS_VALUE = {"", "1", "2", "3", "4"};
    public final static String[] INETLOAD_STATUS_STRING = {"", "Submitted", "Successful", "Rejected", "Pending"};
    public final static int REFILL_TYPE_PREPAID = 1;
    public final static int REFILL_TYPE_POSTPAID = 2;
    public final static int REFILL_TYPE_SCRATCH_CARD = 3;
    public final static String[] REFILL_TYPE = {"", "Prepaid", "Postpaid", "Scratch Card"};
    public final static int VALID_PHONE_NUMBER_LENGTH = 11;
    public final static long SIM_ACTIVE_TIME = 6 * 60 * 1000L;
    public static String REFILL_SUCCESS_DTO = "REFILL_SUCCESS_DTO";
    public static String SMS_SUCCESS_DTO = "SMS_SUCCESS_DTO";
    public static String REFILLBYCARD_SUCCESS_DTO = "REFILLBYCARD_SUCCESS_DTO";
    /*Contact related constants*/
    public final static String CONTACT_RECORD_PER_PAGE = "CONTACT_RECORD_PER_PAGE";
    public final static String CONTACT_ID_LIST = "CONTACT_ID_LIST";
    public final static String CONTACT_GROUP_RECORD_PER_PAGE = "CONTACT_GROUP_RECORD_PER_PAGE";
    public final static String CONTACT_GROUP_ID_LIST = "CONTACT_GROUP_ID_LIST";
    public final static String CONTACT_SUMMERY_RECORD_PER_PAGE = "CONTACT_SUMMERY_RECORD_PER_PAGE";
    public final static String GROUP_SCHEDULING_RECORD_PER_PAGE = "GROUP_SCHEDULING_RECORD_PER_PAGE";
    public final static String GROUP_SCHEDULING_ID_LIST = "GROUP_SCHEDULING_ID_LIST";
    public final static int SCHEDULE_TYPE_NO_REPEAT = 0;
    public final static int SCHEDULE_TYPE_REPEAT = 1;
    public final static String SCHEDULE_TYPE[] = {"No Repeat", "Repeat"};
    public final static int SUN_DAY = 6;
    public final static int MON_DAY = 0;
    public final static int TUES_DAY = 1;
    public final static int WEDNES_DAY = 2;
    public final static int THURS_DAY = 3;
    public final static int FRI_DAY = 4;
    public final static int SATUR_DAY = 5;
    public final static String WEEK_DAY_VALUE[] = {"0", "1", "2", "3", "4", "5", "6"};
    public final static String WEEK_DAY_STRING[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
    /*Flexi Dealer related constants*/
    public final static String DEALER_ID_LIST = "DEALER_ID_LIST";
    public final static String DEALER_RECORD_PER_PAGE = "DEALER_RECORD_PER_PAGE";
    public final static String DEALER_PAYMENT_ID_LIST = "DEALER_PAYMENT_ID_LIST";
    public final static String DEALER_RECHARGE_RECORD_PER_PAGE = "DEALER_RECHARGE_RECORD_PER_PAGE";
    public final static String DEALER_PAYMENT_RECORD_PER_PAGE = "DEALER_PAYMENT_RECORD_PER_PAGE";
    /*API related constants*/
    public final static String API_ID_LIST = "API_ID_LIST";
    public final static String API_RECORD_PER_PAGE = "API_RECORD_PER_PAGE";
    /*Scratchcard related constants*/
    public final static String SCRATCH_CARD_TYPE_ID_LIST = "SCRATCH_CARD_TYPE_ID_LIST";
    public final static String SCRATCH_CARD_TYPE_RECORD_PER_PAGE = "SCRATCH_CARD_TYPE_RECORD_PER_PAGE";
    public final static String SCRATCH_CARD_ID_LIST = "SCRATCH_CARD_ID_LIST";
    public final static String SCRATCH_CARD_RECORD_PER_PAGE = "SCRATCH_CARD_RECORD_PER_PAGE";
    public static String SCRATCH_CARD_SUCCESS_DTO = "SCRATCH_CARD_SUCCESS_DTO";
    public final static int TRANSFER_TYPE_BKASH = 1;
    public final static int TRANSFER_TYPE_DBBL = 2;
    public final static String[] TRANSFER_TYPE = {"", "BKash", "DBBL"};
    public final static int ACCOUNT_TYPE_PERSONAL = 1;
    public final static int ACCOUNT_TYPE_AGENT = 2;
    public final static String[] ACCOUNT_TYPE = {"", "Personal", "Agent"};
    /*Balance Transfer related constants*/
    public final static String BT_SURCHARGE_RECORD_PER_PAGE = "BT_SURCHARGE_RECORD_PER_PAGE";
    public final static String BT_SURCHARGE_ID_LIST = "BT_SURCHARGE_ID_LIST";
    public final static int SMS_MAX_LENGTH  = 140;
}
