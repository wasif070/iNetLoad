package com.myapp.struts.balancetransfer;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListSurchargeAction extends org.apache.struts.action.Action {

    static Logger logger = Logger.getLogger(ListSurchargeAction.class.getName());
    static SimpleDateFormat bangDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            BalanceTransferForm btForm = (BalanceTransferForm) form;
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                int list_all = 0;
                int pageNo = 1;
                if (request.getParameter("list_all") != null) {
                    list_all = Integer.parseInt(request.getParameter("list_all"));
                }
                if (request.getParameter("d-49216-p") != null) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }

                BalanceTransferTaskScheduler scheduler = new BalanceTransferTaskScheduler();

                int action = 0;
                if (request.getParameter("action") != null) {
                    action = Integer.parseInt(request.getParameter("action"));
                    String idList = "";
                    if (action > Constants.UPDATE) {
                        if (btForm.getSelectedIDs() != null) {
                            int length = btForm.getSelectedIDs().length;
                            long[] idListArray = btForm.getSelectedIDs();
                            if (length > 0) {
                                idList += idListArray[0];
                                for (int i = 1; i < length; i++) {
                                    idList += "," + idListArray[i];
                                }
                                MyAppError error = new MyAppError();
                                switch (action) {
                                    case Constants.DELETE:
                                        error = scheduler.deleteBTSurcharges(idList);
                                        if (error.getErrorType() > 0) {
                                            target = "failure";
                                            btForm.setMessage(true, error.getErrorMessage());
                                        } else {
                                            btForm.setMessage(false, "Balance Transfer Surcharges are deleted successfully.");
                                        }
                                        request.getSession(true).setAttribute(Constants.MESSAGE, btForm.getMessage());
                                        break;                                    
                                    default:
                                        break;
                                }
                            }
                        } else {
                            btForm.setMessage(true, "No Balance Transfer Surcharge is selected.");
                            request.getSession(true).setAttribute(Constants.MESSAGE, btForm.getMessage());
                        }
                    }
                }

                if (list_all == 0) {
                    if (btForm.getRecordPerPage() > 0) {
                        request.getSession(true).setAttribute(Constants.BT_SURCHARGE_RECORD_PER_PAGE, btForm.getRecordPerPage());
                    }
                    BalanceTransferDTO btdto = new BalanceTransferDTO();
                    if (btForm.getTypeIDPar() >= 0) {
                        btdto.searchWithTypeID = true;
                        btdto.setTypeIDPar(btForm.getTypeIDPar());
                    }
                    btForm.setSurchargeList(scheduler.getBTSurchargeDTOsWithSearchParam(btdto));
                } else if (list_all == 2) {
                    if (action == Constants.EDIT) {
                        btForm.setMessage(false, "Balance Transfer is updated successfully.");
                    }
                    if (request.getSession(true).getAttribute(Constants.BT_SURCHARGE_RECORD_PER_PAGE) != null) {
                        btForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.BT_SURCHARGE_RECORD_PER_PAGE).toString()));
                    }
                    request.getSession(true).setAttribute(Constants.MESSAGE, btForm.getMessage());
                    btForm.setSurchargeList(scheduler.getBTSurchargeDTOsSorted());
                } else {
                    if (request.getSession(true).getAttribute(Constants.BT_SURCHARGE_RECORD_PER_PAGE) != null) {
                        btForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.BT_SURCHARGE_RECORD_PER_PAGE).toString()));
                    }
                    btForm.setSurchargeList(scheduler.getBTSurchargeDTOs());
                }
                btForm.setSelectedIDs(null);
                if (btForm.getSurchargeList() != null && btForm.getSurchargeList().size() > 0 && btForm.getSurchargeList().size() <= (btForm.getRecordPerPage() * (pageNo - 1))) {
                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                    return changedActionForward;
                }
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
