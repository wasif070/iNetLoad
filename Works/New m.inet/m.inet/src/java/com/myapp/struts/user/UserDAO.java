package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

public class UserDAO {

    static Logger logger = Logger.getLogger(UserDAO.class.getName());

    public UserDAO() {
    }

    public MyAppError editUserInformation(UserDTO p_dto, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select user_id from user where user_id = ? and id!=" + p_dto.getId();
            logger.debug("Edit user duplicate sql: " + sql);
            ps1 = dbConnection.connection.prepareStatement(sql);
            ps1.setString(1, p_dto.getUserId());
            ResultSet resultSet = ps1.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "select client_id from client where client_id = ?";
            ps2 = dbConnection.connection.prepareStatement(sql);
            ps2.setString(1, p_dto.getUserId());
            resultSet = ps2.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID is found.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update user set user_id=?,user_password=? where id=" + p_dto.getId();
            ps3 = dbConnection.connection.prepareStatement(sql);

            ps3.setString(1, p_dto.getUserId());
            ps3.setString(2, p_dto.getUserPassword());
            ps3.executeUpdate();
            l_dto.setClientId(p_dto.getUserId());
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing user: ", ex);
        } finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps3 != null) {
                    ps3.close();
                }
            } catch (Exception e) {
            }           
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

}
