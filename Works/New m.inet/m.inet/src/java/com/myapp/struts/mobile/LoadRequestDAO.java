package com.myapp.struts.mobile;

import com.myapp.struts.client.ClientDTO;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class LoadRequestDAO {

    static Logger logger = Logger.getLogger(LoadRequestDAO.class.getName());

    public static ArrayList<RequestDTO> getRequests(ClientDTO clientDTO, int request_type) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        ArrayList<RequestDTO> data = new ArrayList<RequestDTO>();
        RequestDTO dto = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select inetload_id,inetload_amount,inetload_phn_no,inetload_refill_success_time,inetload_time from inetload_fdr where inetload_to_user_id=" + clientDTO.getId() + " and  inetload_status=" + request_type + " order by inetload_time DESC limit 10";
            if (request_type == Constants.INETLOAD_STATUS_SUCCESSFUL) {
                sql = "select inetload_id,inetload_amount,inetload_phn_no,inetload_refill_success_time,inetload_time from inetload_fdr where inetload_to_user_id=" + clientDTO.getId() + " and  inetload_status=" + request_type + " order by inetload_refill_success_time DESC limit 10";
            }
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                dto = new RequestDTO();
                dto.setId(rs.getLong("inetload_id"));
                dto.setAmount(String.valueOf(rs.getLong("inetload_amount")));
                dto.setPhoneNo(rs.getString("inetload_phn_no"));
                if (request_type == Constants.INETLOAD_STATUS_SUCCESSFUL) {
                    dto.setTime(com.myapp.struts.util.Utils.ToDateDDMMYYhhmm(rs.getLong("inetload_refill_success_time")));
                } else {
                    dto.setTime(com.myapp.struts.util.Utils.ToDateDDMMYYhhmm(rs.getLong("inetload_time")));
                }
                data.add(dto);
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception getSessionValue:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }
}