package com.myapp.struts.login;

public class LoginDTO {

    private int isBTActive;

    public int getIsBTActive() {
        return isBTActive;
    }

    public void setIsBTActive(int isBTActive) {
        this.isBTActive = isBTActive;
    }
    private boolean isUser;
    private boolean isSpecialAgent;
    private int clientTypeId;
    private int clientStatus;
    private int wrongEntry;
    private long id;
    private long roleID;
    private long createdByID;
    private long loginTime;
    private long clientParentId;
    private long clientChildId;
    private float clientCredit;
    private String clientId;
    private String clientName;
    private String clientTypeName;
    private String clientEmail;
    private String clientPassword;

    public LoginDTO() {
        isSpecialAgent = false;
        wrongEntry = 0;
    }
    
    public int getWrongEntry() {
        return wrongEntry;
    }

    public void setWrongEntry(int wrongEntry) {
        this.wrongEntry = wrongEntry;
    }    

    public long getClientChildId() {
        return clientChildId;
    }

    public void setClientChildId(long clientChildId) {
        this.clientChildId = clientChildId;
    }

    public long getRoleID() {
        return roleID;
    }

    public void setRoleID(long roleID) {
        this.roleID = roleID;
    }

    public long getCreatedByID() {
        return createdByID;
    }

    public void setCreatedByID(long createdByID) {
        this.createdByID = createdByID;
    }

    public float getClientCredit() {
        return clientCredit;
    }

    public void setClientCredit(float clientCredit) {
        this.clientCredit = clientCredit;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public long getClientParentId() {
        return clientParentId;
    }

    public void setClientParentId(long clientParentId) {
        this.clientParentId = clientParentId;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    public int getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(int clientStatus) {
        this.clientStatus = clientStatus;
    }

    public int getClientTypeId() {
        return clientTypeId;
    }

    public void setClientTypeId(int clientTypeId) {
        this.clientTypeId = clientTypeId;
    }

    public String getClientTypeName() {
        return clientTypeName;
    }

    public void setClientTypeName(String clientTypeName) {
        this.clientTypeName = clientTypeName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public boolean getIsUser() {
        return isUser;
    }

    public void setIsUser(boolean isUser) {
        this.isUser = isUser;
    }
    
    public boolean getIsSpecialAgent() {
        return isSpecialAgent;
    }

    public void setIsSpecialAgent(boolean isSpecialAgent) {
        this.isSpecialAgent = isSpecialAgent;
    }    

}
