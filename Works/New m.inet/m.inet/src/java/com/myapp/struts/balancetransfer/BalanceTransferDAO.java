package com.myapp.struts.balancetransfer;

import com.myapp.struts.session.Constants;
import com.myapp.struts.system.SettingsLoader;
import com.myapp.struts.util.MyAppError;
import java.sql.Statement;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class BalanceTransferDAO {

    static Logger logger = Logger.getLogger(BalanceTransferDAO.class.getName());

    public BalanceTransferDAO() {
    }

    public ArrayList<BalanceTransferDTO> getBTSurchargeDTOsWithSearchParam(ArrayList<BalanceTransferDTO> list, BalanceTransferDTO btdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                BalanceTransferDTO dto = (BalanceTransferDTO) i.next();
                if ((btdto.searchWithTypeID && dto.getTypeID() != btdto.getTypeIDPar())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<BalanceTransferDTO> getBTSurchargeDTOsSorted(ArrayList<BalanceTransferDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    BalanceTransferDTO dto1 = (BalanceTransferDTO) o1;
                    BalanceTransferDTO dto2 = (BalanceTransferDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public MyAppError addBTSurcharge(BalanceTransferDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            boolean duplicateLimitFound = isDuplicateLimitFound(dto.getMinTransferredAmount(), dto.getMaxTransferredAmount(), dto.getTypeID(), 0);
            if (duplicateLimitFound) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Min transferred amount or Max transferred amount is found in another limit.");
                return error;
            }
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            sql = "insert into bt_surcharge(bt_type_id,bt_max_amount,bt_min_amount,bt_surcharge_amount) values("
                    + dto.getTypeID() + "," + dto.getMaxTransferredAmount() + "," + dto.getMinTransferredAmount() + "," + dto.getSurchargeAmount() + ")";
            statement.execute(sql);
            BalanceTransferLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error in addBTSurcharge: ", ex);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editBTSurcharge(BalanceTransferDTO dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            boolean duplicateLimitFound = isDuplicateLimitFound(dto.getMinTransferredAmount(), dto.getMaxTransferredAmount(), dto.getTypeID(), dto.getId());
            if (duplicateLimitFound) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Min transferred amount or Max transferred amount is found in another limit.");
                return error;
            }
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            sql = "update bt_surcharge set bt_type_id=" + dto.getTypeID() + ",bt_max_amount=" + dto.getMaxTransferredAmount() + ",bt_min_amount=" + dto.getMinTransferredAmount()
                    + ",bt_surcharge_amount=" + dto.getSurchargeAmount() + " where id=" + dto.getId();
            statement.execute(sql);
            BalanceTransferLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error in editBTSurcharge: ", ex);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteBTSurcharges(String list) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from bt_surcharge where id in(" + list + ");";
            stmt = dbConnection.connection.createStatement();
            stmt.execute(sql);
            BalanceTransferLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error in deleteBTSurcharges: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public boolean isDuplicateLimitFound(double minTransferredAmount, double maxTransferredAmount, int typeID, long id) {
        boolean found = false;
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select id from bt_surcharge where bt_type_id=" + typeID + " and id!=" + id + " and ((" + minTransferredAmount + " between bt_min_amount and bt_max_amount) or (" + maxTransferredAmount + " between bt_min_amount and bt_max_amount))";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                found = true;
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in isDuplicateLimitFound: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return found;
    }
    
    public double getSurcharge(double transferredAmount, int typeID) {
        double surcharge = -1.0;
        DBConnection dbConnection = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select bt_surcharge_amount from bt_surcharge where bt_type_id=" + typeID + " and (" + transferredAmount + " between bt_min_amount and bt_max_amount)";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                surcharge = rs.getDouble("bt_surcharge_amount");
            }
            rs.close();
        } catch (Exception ex) {
            logger.debug("Exception in isDuplicateLimitFound: " + ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return surcharge;
    }    
}
