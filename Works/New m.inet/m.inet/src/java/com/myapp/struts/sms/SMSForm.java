/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.sms;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Wasif Islam
 */
public class SMSForm extends org.apache.struts.action.ActionForm {

    private String message;
    private String phoneNumber;
    private int operatorTypeID;
    private double amount;
    private String countryCode;
    private String msg;
    private int doValidate;
    private ArrayList smsList;
    private int pageNo;
    private int recordPerPage;
    private String smsFrom;
    private long smsRequestTimeVal;
    private int smsRequestType;
    private String fromClientId;
    private int sign;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;

    public SMSForm() {
        super();
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public String getFromClientId() {
        return fromClientId;
    }

    public void setFromClientId(String fromClientId) {
        this.fromClientId = fromClientId;
    }

    public int getSmsRequestType() {
        return smsRequestType;
    }

    public void setSmsRequestType(int smsRequestType) {
        this.smsRequestType = smsRequestType;
    }

    public String getSmsFrom() {
        return smsFrom;
    }

    public void setSmsFrom(String smsFrom) {
        this.smsFrom = smsFrom;
    }

    public long getSmsRequestTimeVal() {
        return smsRequestTimeVal;
    }

    public void setSmsRequestTimeVal(long smsRequestTimeVal) {
        this.smsRequestTimeVal = smsRequestTimeVal;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getOperatorTypeID() {
        return operatorTypeID;
    }

    public void setOperatorTypeID(int operatorTypeID) {
        this.operatorTypeID = operatorTypeID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public ArrayList getSmsList() {
        return smsList;
    }

    public void setSmsList(ArrayList smsList) {
        this.smsList = smsList;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }
    

    public String getMsg() {
        return msg;
    }

    public void setMsg(boolean error, String msg) {
        if (error) {
            this.msg = "<span style='color:red; font-size: 12px;font-weight:bold'>" + msg + "</span>";
        } else {
            this.msg = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + msg + "</span>";
        }
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getPhoneNumber() == null || getPhoneNumber().length() == 0) {
                errors.add("phoneNumber", new ActionMessage("errors.phnNumber.required"));
            } else if (getPhoneNumber().length() < Constants.VALID_PHONE_NUMBER_LENGTH) {
                errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
            } else if (getPhoneNumber().length() >= Constants.VALID_PHONE_NUMBER_LENGTH) {
                String phoneNo = getPhoneNumber();
                if (phoneNo.startsWith("+")) {
                    phoneNo = phoneNo.replace("+", "");
                }
                if (phoneNo.startsWith("88")) {
                    phoneNo = phoneNo.replace("88", "");
                }
                phoneNo = phoneNo.replace("-", "");
                phoneNo = phoneNo.replace(" ", "");
                if (phoneNo.length() == Constants.VALID_PHONE_NUMBER_LENGTH) {
                    setPhoneNumber(phoneNo);
                } else {
                    errors.add("phoneNumber", new ActionMessage("errors.invalidPhn"));
                }
            }
            if (getCountryCode() == null || getCountryCode().length() == 0) {
                errors.add("countryCode", new ActionMessage("errors.countryCode.required"));
            }
            if (getMessage() == null || getMessage().length() == 0) {
                errors.add("message", new ActionMessage("errors.message.required"));
            }

            /*if (!Utils.isDouble(request.getParameter("amount").toString())) {
             errors.add("amount", new ActionMessage("errors.flexi_amount.valid"));
             } else if (getAmount() <= 0.0) {
             errors.add("amount", new ActionMessage("errors.amount.required"));
             }*/


        }


        return errors;
    }
}
