package com.myapp.struts.balancetransfer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;

public class EditSurchargeAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        BalanceTransferForm formBean = (BalanceTransferForm) form;
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            if (login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE) {
                request.setAttribute(mapping.getAttribute(), formBean);
                BalanceTransferDTO dto = new BalanceTransferDTO();
                BalanceTransferTaskScheduler scheduler = new BalanceTransferTaskScheduler();
                dto.setId(formBean.getId());
                dto.setTypeID(formBean.getTypeID());
                dto.setMaxTransferredAmount(formBean.getMaxTransferredAmount());
                dto.setMinTransferredAmount(formBean.getMinTransferredAmount());
                dto.setSurchargeAmount(formBean.getSurchargeAmount());

                MyAppError error = new MyAppError();
                long id = Long.parseLong(request.getParameter("id"));
                dto.setId(id);
                error = scheduler.editBTSurcharge(dto);

                if (error.getErrorType() == MyAppError.NoError) {
                    formBean.setMessage(false, "Balance Transfer Surcharge is updated successfully.");
                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                }
                else
                {
                    target = "failure";
                    formBean.setMessage(true, error.getErrorMessage());                    
                }    
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return mapping.findForward(target);
    }
}
