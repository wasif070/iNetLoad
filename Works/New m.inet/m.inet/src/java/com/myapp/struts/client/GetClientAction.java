package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.user.PermissionDTO;
import com.myapp.struts.user.UserLoader;

public class GetClientAction
        extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
            ClientForm formBean = (ClientForm) form;
            PermissionDTO userPerDTO = null;
            if ((login_dto.getRoleID() == Constants.SUPER_ADMIN_ROLE
                    || (login_dto.getIsUser() && (userPerDTO = UserLoader.getInstance().getRoleDTOByID(login_dto.getRoleID()).getPermissionDTO()) != null && userPerDTO.CLIENT_EDIT)
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && login_dto.getId() == formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && login_dto.getId() == formBean.getParentId())
                    || (login_dto.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && login_dto.getId() == formBean.getParentId())
                    || login_dto.getId() == formBean.getId()) && login_dto.getClientStatus() == Constants.USER_STATUS_ACTIVE) {
                if ((formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER)
                        || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER3 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID)
                        || (formBean.getClientTypeId() == Constants.CLIENT_TYPE_RESELLER2 && formBean.getParentId() != Constants.ROOT_RESELLER_PARENT_ID && ClientLoader.getInstance().getClientDTOByID(formBean.getParentId()).getClientTypeId() != Constants.CLIENT_TYPE_RESELLER3)) {
                    target = "failure";
                    return (mapping.findForward(target));
                }
                long id = Long.parseLong(request.getParameter("id"));

                ClientDTO dto = new ClientDTO();
                ClientTaskSchedular scheduler = new ClientTaskSchedular();

                dto = scheduler.getClientDTO(id);

                if (dto != null && (login_dto.getIsUser() || dto.getParentId() == login_dto.getId())) {
                    formBean.setId(dto.getId());                   
                    formBean.setClientId(dto.getClientId());
                    formBean.setClientPassword(dto.getClientPassword());
                    formBean.setRetypePassword(dto.getClientPassword());
                    formBean.setClientStatus(dto.getClientStatus());
                    formBean.setClientStatusName(Constants.LIVE_STATUS_STRING[dto.getClientStatus()]); 
                    formBean.setClientTypeId(dto.getClientTypeId());
                    formBean.setParentId(dto.getParentId());
                    formBean.setClientCredit(dto.getClientCredit());
                }
                else
                {
                   target = "failure";
                }   
                MyAppError error = new MyAppError();

                if (error.getErrorType() > 0) {
                    target = "failure";
                }

                if (mapping.getScope().equals("request")) {
                    request.setAttribute(mapping.getAttribute(), formBean);
                } else {
                    request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
                }
            }
        } else {
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
