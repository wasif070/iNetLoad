package com.myapp.struts.rechargecard;

import com.myapp.struts.client.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class RechargecardGroupDAO {

    static Logger logger = Logger.getLogger(RechargecardGroupDAO.class.getName());

    public RechargecardGroupDAO() {
    }

    public long getNormalRechargeCard(RechargecardDTO dto) {
        long rcID = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select id,card_balance from rechargecard where card_no = '" + dto.getCardNo() + "' and serial_no = " + dto.getSerialNo() + " and status_id = " + Constants.USER_STATUS_ACTIVE + " and package_id = " + Constants.PACKAGE_ID_NORMAL;
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                rcID = resultSet.getLong("id");
                dto.setGroupAmount(resultSet.getDouble("card_balance"));
            }
            resultSet.close();
        } catch (Exception ex) {
            logger.fatal("Error while getNormalRechargeCard: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rcID;
    }

    public long getSpecialRechargeCard(RechargecardDTO dto) {
        long rcID = 0;
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select id from rechargecard where card_no = '" + dto.getCardNo() + "' and serial_no = " + dto.getSerialNo() + " and status_id = " + Constants.USER_STATUS_ACTIVE + " and package_id = " + Constants.PACKAGE_ID_SPECIAL;
            stmt = dbConnection.connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(sql);
            if (resultSet.next()) {
                rcID = resultSet.getLong("id");
            }
            resultSet.close();
        } catch (Exception ex) {
            logger.fatal("Error while getSpecialRechargeCard: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rcID;
    }

    public MyAppError refillByCard(RechargecardDTO rcDTO, LoginDTO l_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        String marker = System.currentTimeMillis() + ":" + rcDTO.getPhoneNumber();

        try {
            long rcID = getNormalRechargeCard(rcDTO);
            if (rcID > 0) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                stmt = dbConnection.connection.createStatement();
                String sql = "insert into inetload_fdr(operator_type_id,inetload_phn_no,"
                        + "inetload_from_user_id,inetload_from_client_type_id,inetload_to_user_id,inetload_to_client_type_id,"
                        + "inetload_type_id,inetload_amount,inetload_time,inetload_marker,inetload_status,card_id) values";
                int fromTypeID = Constants.CLIENT_TYPE_AGENT;
                int toTypeID = fromTypeID;
                long fromID = l_dto.getId();
                long toID = fromID;
                sql += "(" + rcDTO.getOperatorTypeID() + ",'" + rcDTO.getPhoneNumber() + "'," + fromID + "," + fromTypeID + "," + toID + "," + toTypeID + ","
                        + rcDTO.getRefillType() + "," + rcDTO.getGroupAmount() + "," + System.currentTimeMillis() + ",'" + marker + "'," + Constants.INETLOAD_STATUS_PENDING + "," + rcID + ")";
                stmt.execute(sql);
                sql = "update rechargecard,inetload_fdr set inetload_status=" + Constants.INETLOAD_STATUS_SUBMITTED + ",status_id=" + Constants.USER_STATUS_BLOCK + ",blocked_by=" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + ",refill_no='" + rcDTO.getPhoneNumber() + "' where id=" + rcID + " and inetload_marker='" + marker + "'";
                stmt.execute(sql);
            } else {
                l_dto.setWrongEntry(l_dto.getWrongEntry() + 1);
                error.setErrorType(MyAppError.OtherError);
                error.setErrorMessage("Invalid Recarge Card. Please check serial no, card no and card type.");
            }

        } catch (Exception ex) {
            logger.fatal("Error while refillByCard: ", ex);
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error is Occured.");
            return error;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError rechargeByCard(RechargecardDTO rcDTO, LoginDTO l_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            long rcID = getSpecialRechargeCard(rcDTO);
            if (rcID > 0) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                String sql = "update rechargecard,client set client_credit_all=(client_credit_all+card_balance),status_id=" + Constants.USER_STATUS_BLOCK + ",blocked_by=" + l_dto.getId() + ",blocked_time=" + System.currentTimeMillis() + " where rechargecard.id = " + rcID + " and client.id=" + l_dto.getId() + ";";
                stmt = dbConnection.connection.createStatement();
                stmt.execute(sql);
                ClientLoader.getInstance().forceReload();
            } else {
                l_dto.setWrongEntry(l_dto.getWrongEntry() + 1);
                error.setErrorType(MyAppError.OtherError);
                error.setErrorMessage("Invalid Recarge Card. Please check serial no, card no and card type.");
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while blockRechargecards: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

   
}
