package com.myapp.struts.mobile;

import java.util.HashMap;

public class Decription {

    public void Decription() {
    }

    public static String decreptedString(String input) {
        if (input == null) {
            return "";
        } else if (input.length() <= 0) {
            return "";
        }

        HashMap<String, String> hash = new HashMap<String, String>();
        hash.put("0", "cU3n");
        hash.put("1", "4J19");
        hash.put("2", "2iui9");
        hash.put("3", "xiuY");
        hash.put("4", "voT");
        hash.put("5", "3E2R");
        hash.put("6", "1vse");
        hash.put("7", "nMG");
        hash.put("8", "xs");
        hash.put("9", "mJN");

        char[] charArray = input.toCharArray();
        String result = "";
        try {
            while (charArray.length > 0) {
                result += String.valueOf(charArray[0]);
                String map_str = hash.get(String.valueOf(charArray[1]));
                input = input.substring(map_str.length() + 2);
                charArray = input.toCharArray();
            }
        } catch (Exception ex) {
            return "";
        }
        return result;
    }

    private String encryption(String user_text) {
        String result = "";
        StringBuffer encryptedString = new StringBuffer();
        HashMap<String, String> hash = new HashMap<String, String>();
        hash.put("0", "cU3n");
        hash.put("1", "4J19");
        hash.put("2", "2iui9");
        hash.put("3", "xiuY");
        hash.put("4", "voT");
        hash.put("5", "3E2R");
        hash.put("6", "1vse");
        hash.put("7", "nMG");
        hash.put("8", "xs");
        hash.put("9", "mJN");

        String timeInString = Long.toString(System.currentTimeMillis());
        timeInString = new StringBuffer(timeInString).reverse().toString();

        char timeInchar[] = timeInString.toCharArray();


        char usernameTOcharArray[] = user_text.toCharArray();
        for (int i = 0, j = 0; i < usernameTOcharArray.length; i++) {
            encryptedString.append(String.valueOf(usernameTOcharArray[i]));
            if (j == timeInchar.length) {
                j = 0;
            }
            encryptedString.append(String.valueOf(timeInchar[j]));
            encryptedString.append(hash.get(String.valueOf(timeInchar[j])));
            j++;

        }
        result = encryptedString.toString();
        encryptedString.delete(0, encryptedString.length());

        return result;
    }    
}
