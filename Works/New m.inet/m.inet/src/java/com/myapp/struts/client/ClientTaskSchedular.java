package com.myapp.struts.client;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class ClientTaskSchedular {

    public ClientTaskSchedular() {
    }  
    
    public MyAppError addClientInformation(ClientDTO cl_dto, LoginDTO l_dto) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.addClientInformation(cl_dto, l_dto);
    }    
    
    public MyAppError addSpecialAgentInformation(ClientDTO cl_dto) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.addSpecialAgentInformation(cl_dto);
    }      

    public MyAppError editClientInformation(ClientDTO cl_dto, LoginDTO l_dto) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.editClientInformation(cl_dto, l_dto);
    }

    public MyAppError deactivateClients(String list) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.deactivateClients(list);
    }

    public MyAppError activateClients(String list) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.activateClients(list);
    }

    public MyAppError blockClients(String list) {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.blockClients(list);
    }

    public MyAppError rechargeClient(LoginDTO l_dto, long id, double amount, String description)
    {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.rechargeClient(l_dto,id,amount,description);
    }

    public MyAppError returnClient(LoginDTO l_dto, long id, double amount, String description)
    {
        ClientDAO clientDAO = new ClientDAO();
        return clientDAO.returnClient(l_dto,id,amount,description);
    }

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ClientDTO cldto,long parentId) {
        ClientDAO clinetDAO = new ClientDAO();
        ArrayList<ClientDTO> list = ClientLoader.getInstance().getClientDTOsByParentID(parentId);
        if(list!=null)
        {
          return clinetDAO.getClientDTOsWithSearchParam((ArrayList<ClientDTO>) list.clone(),cldto);
        }
        return null;
    }

    public ArrayList<ClientDTO> getClientDTOsSorted(long parentId) {
        ClientDAO clinetDAO = new ClientDAO();
        ArrayList<ClientDTO> list = ClientLoader.getInstance().getClientDTOsByParentID(parentId);
        if(list!=null)
        {
          return clinetDAO.getClientDTOsSorted((ArrayList<ClientDTO>) list.clone());
        }
        return null;
    }

    public ArrayList<ClientDTO> getClientDTOs(long parentId) {
            return ClientLoader.getInstance().getClientDTOsByParentID(parentId);
    }

    public ClientDTO getClientDTO(long id) {
        return ClientLoader.getInstance().getClientDTOByID(id);
    }  
}
