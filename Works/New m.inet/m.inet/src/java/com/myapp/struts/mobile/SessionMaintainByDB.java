package com.myapp.struts.mobile;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class SessionMaintainByDB {

    static Logger logger = Logger.getLogger(SessionMaintainByDB.class.getName());

    public static boolean setSessionValue(String user_name, String pass, String time) {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "delete from session_values where user_name='" + user_name + "'";
            statement.execute(sql);
            sql = "insert into session_values values('" + user_name + "','" + pass + "','" + time + "'," + System.currentTimeMillis() + ")";
            statement.execute(sql);
        } catch (Exception e) {
            logger.fatal("Exception in setSessionValue:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return true;
    }

    public static SessionValueDTO getSessionValue(String user_name) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        SessionValueDTO dto = new SessionValueDTO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select user_name,password,time from session_values where user_name='" + user_name + "'";            
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                dto = new SessionValueDTO();
                dto.setUserName(rs.getString("user_name"));
                dto.setPass(rs.getString("password"));
                dto.setTime(rs.getString("time"));                
            }
            rs.close();
        } catch (Exception e) {
            logger.fatal("Exception in getSessionValue:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public static boolean deleteByTime() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            long diff = System.currentTimeMillis() - 15 * 60 * 1000L;
            String sql = "delete from session_values where current_time>" + diff;
            statement.execute(sql);
        } catch (Exception e) {
            logger.fatal("Exception deleteSessionValue:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return true;
    }
}
