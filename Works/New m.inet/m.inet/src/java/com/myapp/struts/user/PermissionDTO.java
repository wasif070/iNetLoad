package com.myapp.struts.user;

import com.myapp.struts.session.PermissionConstants;

public class PermissionDTO {

    public boolean CLIENT = false;
    public boolean CLIENT_ADD = false;
    public boolean CLIENT_EDIT = false;
    public boolean CLIENT_RECHARGE = false;
    public boolean CLIENT_RETURN = false;
    public boolean CLIENT_RECEIVE = false;  
    public boolean RCG = false;
    public boolean RCG_ADD = false;
    public boolean RCG_EDIT = false;    
    public boolean REFILL = false;
    public boolean REFILL_GP = false;
    public boolean REFILL_BL = false;
    public boolean REFILL_RB = false;
    public boolean REFILL_WR = false;
    public boolean REFILL_TT = false;
    public boolean REFILL_CC = false;
    public boolean REFILL_CARD = false;
    public boolean REFILL_BT = false;  
    public boolean FLEXI_SIM_SETTINGS = false;
    public boolean FLEXI_SIM_SETTINGS_EDIT = false;
    public boolean FLEXI_SIM_SETTINGS_DELETE = false;
    public boolean SMS_SIM_SETTINGS = false;
    public boolean SMS_SIM_SETTINGS_EDIT = false;
    public boolean SMS_SIM_SETTINGS_DELETE = false;    
    public boolean FLEXI_SIM_RECHARGE = false;
    public boolean FLEXI_SIM_RECHARGE_ADD = false;
    public boolean FLEXI_SIM_RECHARGE_EDIT = false;
    public boolean FLEXI_SIM_RECHARGE_DELETE = false;
    public boolean FLEXI_MESSAGE_LOG = false;
    public boolean FLEXI_MESSAGE_LOG_DELETE = false;
    public boolean SMS_MESSAGE_LOG = false;
    public boolean SMS_MESSAGE_LOG_DELETE = false;    
    public boolean REPORT = false;
    public boolean REPORT_REFILL_SUMMERY = false;
    public boolean REPORT_PAYMENT_SUMMERY = false;
    public boolean REPORT_PAYMENT_MADE_HISTORY = false;

    public void setPermission(int permissionID) {
        switch(permissionID)
        {
            case PermissionConstants.CLIENT:
                CLIENT = true;
                break;
            case PermissionConstants.CLIENT_ADD:
                CLIENT_ADD = true;
                break;
            case PermissionConstants.CLIENT_EDIT:
                CLIENT_EDIT = true;
                break;
            case PermissionConstants.CLIENT_RECHARGE:
                CLIENT_RECHARGE = true;
                break;
            case PermissionConstants.CLIENT_RETURN:
                CLIENT_RETURN = true;
                break;
            case PermissionConstants.CLIENT_RECEIVE:
                CLIENT_RECEIVE = true;
                break;                
            case PermissionConstants.RCG:
                RCG = true;
                break;
            case PermissionConstants.RCG_ADD:
                RCG_ADD = true;
                break;
            case PermissionConstants.RCG_EDIT:
                RCG_EDIT = true;
                break;                
            case PermissionConstants.REFILL:
                REFILL = true;
                break;
            case PermissionConstants.REFILL_GP:
                REFILL_GP = true;
                break;
            case PermissionConstants.REFILL_BL:
                REFILL_BL = true;
                break;
            case PermissionConstants.REFILL_RB:
                REFILL_RB = true;
                break;
            case PermissionConstants.REFILL_WR:
                REFILL_WR = true;
                break;
            case PermissionConstants.REFILL_TT:
                REFILL_TT = true;
                break;
            case PermissionConstants.REFILL_CC:
                REFILL_CC = true;
                break;
            case PermissionConstants.FLEXI_SIM_SETTINGS:
                FLEXI_SIM_SETTINGS = true;
                break;
            case PermissionConstants.FLEXI_SIM_SETTINGS_EDIT:
                FLEXI_SIM_SETTINGS_EDIT = true;
                break;
            case PermissionConstants.FLEXI_SIM_SETTINGS_DELETE:
                FLEXI_SIM_SETTINGS_DELETE = true;
                break;                
            case PermissionConstants.FLEXI_SIM_RECHARGE:
                FLEXI_SIM_RECHARGE = true;
                break;
            case PermissionConstants.FLEXI_SIM_RECHARGE_ADD:
                FLEXI_SIM_RECHARGE_ADD = true;
                break;
            case PermissionConstants.FLEXI_SIM_RECHARGE_EDIT:
                FLEXI_SIM_RECHARGE_EDIT = true;
                break;
            case PermissionConstants.FLEXI_SIM_RECHARGE_DELETE:
                FLEXI_SIM_RECHARGE_DELETE = true;
                break;
            case PermissionConstants.FLEXI_MESSAGE_LOG:
                FLEXI_MESSAGE_LOG = true;
                break;
            case PermissionConstants.FLEXI_MESSAGE_LOG_DELETE:
                FLEXI_MESSAGE_LOG_DELETE = true;
                break;               
            case PermissionConstants.REPORT:
                REPORT = true;
                break;
            case PermissionConstants.REPORT_REFILL_SUMMERY:
                REPORT_REFILL_SUMMERY = true;
                break;
            case PermissionConstants.REPORT_PAYMENT_SUMMERY:
                REPORT_PAYMENT_SUMMERY = true;
                break;
            case PermissionConstants.REPORT_PAYMENT_MADE_HISTORY:
                REPORT_PAYMENT_MADE_HISTORY = true;
                break;
            default:
                break;
        }
    }
}
