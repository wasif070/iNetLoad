package com.myapp.struts.payment;

import com.myapp.struts.util.Utils;

public class PaymentDTO {

    public boolean searchWithClient = false;
    public boolean searchWithPaymentType = false;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int paymentTypeId;
    private int paymentFromClientTypeId;
    private int paymentToClientTypeId;
    private long paymentId;
    private long paymentFromClientId;
    private long paymentToClientId;
    private long paymentTime;
    private double receivedAmount;
    private double paymentAmount;
    private double paymentDiscount;
    private double clientBalance;
    private double clientPreviousBalance;
    private double clientUses;
    private String paymentFromClient;
    private String paymentToClient;
    private String paymentTypeName;
    private String paymentDescription;
    private String paymentFromClientTypeName;
    private String paymentToClientTypeName;

    public PaymentDTO() {
        String dateParts[] = Utils.getDateParts(System.currentTimeMillis());

        this.startDay = Integer.parseInt(dateParts[0]);
        this.startMonth = Integer.parseInt(dateParts[1]);
        this.startYear = Integer.parseInt(dateParts[2]);
        this.endDay = this.startDay;
        this.endMonth = this.startMonth;
        this.endYear = this.startYear;
        paymentAmount = 0.0;
        receivedAmount = 0.0;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public String getPaymentFromClient() {
        return paymentFromClient;
    }

    public void setPaymentFromUserId(String paymentFromClient) {
        this.paymentFromClient = paymentFromClient;
    }

    public String getPaymentToClient() {
        return paymentToClient;
    }

    public void setPaymentToClient(String paymentToClient) {
        this.paymentToClient = paymentToClient;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
    
    public double getPaymentDiscount() {
        return paymentDiscount;
    }

    public void setPaymentDiscount(double paymentDiscount) {
        this.paymentDiscount = paymentDiscount;
    }    

    public double getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(double receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public double getClientBalance() {
        return clientBalance;
    }

    public void setClientBalance(double clientBalance) {
        this.clientBalance = clientBalance;
    }
    
    public double getClientPreviousBalance() {
        return clientPreviousBalance;
    }

    public void setClientPreviousBalance(double clientPreviousBalance) {
        this.clientPreviousBalance = clientPreviousBalance;
    }    

    public double getClientUses() {
        return clientUses;
    }

    public void setClientUses(double clientUses) {
        this.clientUses = clientUses;
    }   

    public long getPaymentFromClientId() {
        return paymentFromClientId;
    }

    public void setPaymentFromClientId(long paymentFromClientId) {
        this.paymentFromClientId = paymentFromClientId;
    }

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public long getPaymentToClientId() {
        return paymentToClientId;
    }

    public void setPaymentToClientId(long paymentToClientId) {
        this.paymentToClientId = paymentToClientId;
    }

    public int getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(int paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public int getPaymentFromClientTypeId() {
        return paymentFromClientTypeId;
    }

    public void setPaymentFromClientTypeId(int paymentFromClientTypeId) {
        this.paymentFromClientTypeId = paymentFromClientTypeId;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentFromClientTypeName(String paymentFromClientTypeName) {
        this.paymentFromClientTypeName = paymentFromClientTypeName;
    }

    public String getPaymentFromClientTypeName() {
        return paymentFromClientTypeName;
    }

    public void setPaymentToClientTypeName(String paymentToClientTypeName) {
        this.paymentToClientTypeName = paymentToClientTypeName;
    }

    public String getPaymentToClientTypeName() {
        return paymentToClientTypeName;
    }

    public int getPaymentToClientTypeId() {
        return paymentToClientTypeId;
    }

    public void setPaymentToClientTypeId(int paymentToClientTypeId) {
        this.paymentToClientTypeId = paymentToClientTypeId;
    }
}
