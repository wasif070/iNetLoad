package com.myapp.struts.client;
import com.myapp.struts.session.Constants;

public class ClientDTO {
    public boolean searchWithClientID = false;
    public boolean searchWithType = false;
    public boolean searchWithStatus = false;
    public boolean searchWithCredit = false;
    public boolean isDeleted = false;
    public int clientGPActive = 0;
    public int clientRBActive = 0;
    public int clientBLActive = 0;
    public int clientTTActive = 0;
    public int clientCCActive = 0;
    public int clientWRActive = 0;   
    public int clientBTActive = 0;
    public int clientSMSActive = 0;
    private int clientTypeId;
    private int clientStatus;
    private int clientVersion;
    private int sign;
    private int pageNo;
    private int recordPerPage;
    private long id;
    private long parentId;
    private double clientCredit;
    private double clientDiscount;
    private double clientDeposit;
    private double clientPreviousBalance;
    private String clientIp;
    private String clientId;
    private String clientName;
    private String smsSender;
    private String clientEmail;
    private String clientPassword;
    private String retypePassword;

    public ClientDTO() {  
        clientIp = null;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }    
    
    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        if(isDeleted==1)
        {
           this.isDeleted = true;
        }
        else
        {
            this.isDeleted = false;
        }
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }
    
    public int getClientTypeId() {
        return clientTypeId;
    }

    public void setClientTypeId(int clientTypeId) {
        this.clientTypeId = clientTypeId;
    }    

    public int getClientVersion() {
        return clientVersion;
    }
    
    public void setClientVersion(int clientVersion) {
        this.clientVersion = clientVersion;
    }    
    
    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getClientTypeName() {
        return Constants.CLIENT_TYPE[clientTypeId];
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.clientPassword = retypePassword;
    }

    public double getClientCredit() {
        return clientCredit;
    }

    public void setClientCredit(double clientCredit) {
        this.clientCredit = clientCredit;
    }
    
    public double getClientDiscount() {
        return clientDiscount;
    }
    
    public void setClientDiscount(double clientDiscount) {
        this.clientDiscount = clientDiscount;
    }  
    
    public double getClientDeposit() {
        return clientDeposit;
    }
    
    public void setClientDeposit(double clientDeposit) {
        this.clientDeposit = clientDeposit;
    }        
    
    public double getClientPreviousBalance() {
        return clientPreviousBalance;
    }

    public void setClientPreviousBalance(double clientPreviousBalance) {
        this.clientPreviousBalance = clientPreviousBalance;
    }      

    public int getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(int clientStatus) {
        this.clientStatus = clientStatus;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public String getSmsSender() {
        return smsSender;
    }

    public void setSmsSender(String smsSender) {
        this.smsSender = smsSender;
    }
    
}
