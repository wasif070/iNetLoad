package com.myapp.struts.system;

public class SettingsDTO {

    private int sameNumberRefillInterval;
    private int offsetFromServerTime;
    private int pendingTimeForManualSystem;
    private int pendingTimeForAutoSystem;
    private int dataBackupInterval;
    private int simMessageLogRestoreInterval;
    private int recordPerPageForReporting;
    private int notifyAfterPending;
    private double prepaidMinRefillAmount;
    private double prepaidMaxRefillAmount;
    private double postpaidMinRefillAmount;
    private double postpaidMaxRefillAmount;
    private double minFlexiSIMBalance;
    private double maxAllowedDiscount;
    private double minAllowedDiscount;
    private String emergencyNoticeText;
    private String brandName;
    private String motto;
    private String welcomeNote;
    private String copyrightLink;
    private String contactEmail;
    private String contactEmailPass;
    private String notificationEmail;

    public SettingsDTO() {
        sameNumberRefillInterval = 15;
        offsetFromServerTime = 0;
        pendingTimeForManualSystem = 24;
        pendingTimeForAutoSystem = 6;
        dataBackupInterval = 15;
        simMessageLogRestoreInterval = 7;
        recordPerPageForReporting = 100;
        notifyAfterPending = 25;
        prepaidMinRefillAmount = 10.0;
        prepaidMaxRefillAmount = 1000.0;
        postpaidMinRefillAmount = 50.0;
        postpaidMaxRefillAmount = 5000.0;
        minFlexiSIMBalance = 500.0;
        maxAllowedDiscount = 3.0;
        minAllowedDiscount = -3.0;        
    }
    
    public String getCopyrightLink() {
        return copyrightLink;
    }

    public void setCopyrightLink(String copyrightLink) {
        this.copyrightLink = copyrightLink;
    }
    
    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
    
    public String getContactEmailPass() {
        return contactEmailPass;
    }

    public void setContactEmailPass(String contactEmailPass) {
        this.contactEmailPass = contactEmailPass;
    }    
    
    public String getNotificationEmail() {
        return notificationEmail;
    }

    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }    

    public String getEmergencyNoticeText() {
        return emergencyNoticeText;
    }

    public void setEmergencyNoticeText(String emergencyNoticeText) {
        this.emergencyNoticeText = emergencyNoticeText;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getWelcomeNote() {
        return welcomeNote;
    }

    public void setWelcomeNote(String welcomeNote) {
        this.welcomeNote = welcomeNote;
    }
    
    public int getNotifyAfterPending() {
        return notifyAfterPending;
    }

    public void setNotifyAfterPending(int notifyAfterPending) {
        this.notifyAfterPending = notifyAfterPending;
    }    
    
    public int getOffsetFromServerTime() {
        return offsetFromServerTime;
    }

    public void setOffsetFromServerTime(int offsetFromServerTime) {
        this.offsetFromServerTime = offsetFromServerTime;
    }    

    public int getPendingTimeForManualSystem() {
        return pendingTimeForManualSystem;
    }

    public void setPendingTimeForManualSystem(int pendingTimeForManualSystem) {
        this.pendingTimeForManualSystem = pendingTimeForManualSystem;
    }  
    
    public int getPendingTimeForAutoSystem() {
        return pendingTimeForAutoSystem;
    }

    public void setPendingTimeForAutoSystem(int pendingTimeForAutoSystem) {
        this.pendingTimeForAutoSystem = pendingTimeForAutoSystem;
    }  
    
    public int getDataBackupInterval() {
        return dataBackupInterval;
    }

    public void setDataBackupInterval(int dataBackupInterval) {
        this.dataBackupInterval = dataBackupInterval;
    }   
    
    public int getRecordPerPageForReporting() {
        return recordPerPageForReporting;
    }

    public void setRecordPerPageForReporting(int recordPerPageForReporting) {
        this.recordPerPageForReporting = recordPerPageForReporting;
    }     
    
    public int getSimMessageLogRestoreInterval() {
        return simMessageLogRestoreInterval;
    }

    public void setSimMessageLogRestoreInterval(int simMessageLogRestoreInterval) {
        this.simMessageLogRestoreInterval = simMessageLogRestoreInterval;
    }      
    
    public int getSameNumberRefillInterval() {
        return sameNumberRefillInterval;
    }

    public void setSameNumberRefillInterval(int sameNumberRefillInterval) {
        this.sameNumberRefillInterval = sameNumberRefillInterval;
    }

    public double getPrepaidMinRefillAmount() {
        return prepaidMinRefillAmount;
    }

    public void setPrepaidMinRefillAmount(double prepaidMinRefillAmount) {
        this.prepaidMinRefillAmount = prepaidMinRefillAmount;
    }

    public double getPrepaidMaxRefillAmount() {
        return prepaidMaxRefillAmount;
    }

    public void setPrepaidMaxRefillAmount(double prepaidMaxRefillAmount) {
        this.prepaidMaxRefillAmount = prepaidMaxRefillAmount;
    }

    public double getPostpaidMinRefillAmount() {
        return postpaidMinRefillAmount;
    }

    public void setPostpaidMinRefillAmount(double postpaidMinRefillAmount) {
        this.postpaidMinRefillAmount = postpaidMinRefillAmount;
    }

    public double getPostpaidMaxRefillAmount() {
        return postpaidMaxRefillAmount;
    }

    public void setPostpaidMaxRefillAmount(double postpaidMaxRefillAmount) {
        this.postpaidMaxRefillAmount = postpaidMaxRefillAmount;
    }

    public double getMinFlexiSIMBalance() {
        return minFlexiSIMBalance;
    }

    public void setMinFlexiSIMBalance(double minFlexiSIMBalance) {
        this.minFlexiSIMBalance = minFlexiSIMBalance;
    }
    
    public double getMaxAllowedDiscount() {
        return maxAllowedDiscount;
    }

    public void setMaxAllowedDiscount(double maxAllowedDiscount) {
        this.maxAllowedDiscount = maxAllowedDiscount;
    }
  
    public double getMinAllowedDiscount() {
        return minAllowedDiscount;
    }

    public void setMinAllowedDiscount(double minAllowedDiscount) {
        this.minAllowedDiscount = minAllowedDiscount;
    }    
}
