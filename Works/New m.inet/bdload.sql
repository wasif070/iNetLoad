-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2013 at 10:47 AM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bdload`
--

-- --------------------------------------------------------

--
-- Table structure for table `bt_surcharge`
--

DROP TABLE IF EXISTS `bt_surcharge`;
CREATE TABLE IF NOT EXISTS `bt_surcharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bt_type_id` int(3) NOT NULL,
  `bt_max_amount` decimal(18,4) NOT NULL,
  `bt_min_amount` decimal(18,4) NOT NULL,
  `bt_surcharge_amount` decimal(18,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bt_surcharge`
--

INSERT INTO `bt_surcharge` (`id`, `bt_type_id`, `bt_max_amount`, `bt_min_amount`, `bt_surcharge_amount`) VALUES
(1, 1, 5000.0000, 1.0000, 7.0000),
(2, 2, 5000.0000, 1.0000, 32.0000);

-- --------------------------------------------------------

--
-- Table structure for table `card_shop`
--

DROP TABLE IF EXISTS `card_shop`;
CREATE TABLE IF NOT EXISTS `card_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(50) NOT NULL,
  `shop_owner` varchar(50) NOT NULL DEFAULT '',
  `shop_description` varchar(100) NOT NULL DEFAULT '',
  `shop_address` varchar(100) NOT NULL DEFAULT '',
  `shop_contact_number` varchar(30) NOT NULL DEFAULT '',
  `shop_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `card_shop`
--


-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(50) NOT NULL,
  `client_type_id` int(3) NOT NULL,
  `client_parent_id` int(11) NOT NULL,
  `client_full_name` varchar(100) NOT NULL DEFAULT '',
  `client_email` varchar(50) NOT NULL DEFAULT '',
  `client_password` varchar(32) NOT NULL,
  `client_credit_all` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_status` tinyint(1) DEFAULT '0',
  `client_reg_time` decimal(18,0) DEFAULT '0',
  `client_deleted` tinyint(1) DEFAULT '0',
  `client_version` int(3) NOT NULL DEFAULT '0',
  `client_ip` varchar(200) NOT NULL DEFAULT '',
  `client_previous_balance` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_discount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_deposit` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_gp_active` tinyint(1) DEFAULT '1',
  `client_rb_active` tinyint(1) DEFAULT '1',
  `client_bl_active` tinyint(1) DEFAULT '1',
  `client_tt_active` tinyint(1) DEFAULT '1',
  `client_cc_active` tinyint(1) DEFAULT '1',
  `client_wr_active` tinyint(1) DEFAULT '1',
  `total_recharged` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `total_received` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `total_used` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_ani` varchar(100) NOT NULL DEFAULT '',
  `client_bt_active` tinyint(1) DEFAULT '1',
  `client_sms_active` tinyint(1) NOT NULL DEFAULT '1',
  `client_sms_sender` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `client_id`, `client_type_id`, `client_parent_id`, `client_full_name`, `client_email`, `client_password`, `client_credit_all`, `client_status`, `client_reg_time`, `client_deleted`, `client_version`, `client_ip`, `client_previous_balance`, `client_discount`, `client_deposit`, `client_gp_active`, `client_rb_active`, `client_bl_active`, `client_tt_active`, `client_cc_active`, `client_wr_active`, `total_recharged`, `total_received`, `total_used`, `client_ani`, `client_bt_active`, `client_sms_active`, `client_sms_sender`) VALUES
(1, 'reseller', 1, 0, 'Reseller 1', '', 'aaaaaa', 2050.0000, 1, 1378277677529, 0, 0, '', 0.0000, 1.0000, 0.0000, 1, 1, 1, 1, 1, 1, 5050.0000, 0.0000, 1675.5000, '', 1, 1, 'Tester'),
(2, 'support1', 2, 1, '', '', 'aaaaaa', 1213.8000, 1, 1378277782291, 0, 0, '', 0.0000, 0.0000, 0.0000, 1, 1, 1, 1, 1, 1, 3000.0000, 0.0000, 1675.5000, '', 1, 1, ''),
(3, 'reseller2', 1, 0, '', '', 'aaaaaa', 500.0000, 1, 1378631356682, 0, 0, '', 0.0000, 0.0000, 0.0000, 1, 1, 1, 1, 1, 1, 500.0000, 0.0000, 0.0000, '', 1, 1, 'SMS sender');

-- --------------------------------------------------------

--
-- Table structure for table `client_old`
--

DROP TABLE IF EXISTS `client_old`;
CREATE TABLE IF NOT EXISTS `client_old` (
  `id` int(11) NOT NULL DEFAULT '0',
  `client_id` varchar(50) NOT NULL,
  `client_type_id` int(3) NOT NULL,
  `client_parent_id` int(11) NOT NULL,
  `client_full_name` varchar(100) NOT NULL DEFAULT '',
  `client_email` varchar(50) NOT NULL DEFAULT '',
  `client_password` varchar(32) NOT NULL,
  `client_credit_all` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_status` tinyint(1) DEFAULT '0',
  `client_reg_time` decimal(18,0) DEFAULT '0',
  `client_deleted` tinyint(1) DEFAULT '0',
  `client_version` int(3) NOT NULL DEFAULT '0',
  `client_ip` varchar(200) NOT NULL DEFAULT '',
  `client_previous_balance` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_discount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_deposit` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_gp_active` tinyint(1) DEFAULT '1',
  `client_rb_active` tinyint(1) DEFAULT '1',
  `client_bl_active` tinyint(1) DEFAULT '1',
  `client_tt_active` tinyint(1) DEFAULT '1',
  `client_cc_active` tinyint(1) DEFAULT '1',
  `client_wr_active` tinyint(1) DEFAULT '1',
  `total_recharged` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `total_received` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `total_used` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `client_ani` varchar(100) NOT NULL DEFAULT '',
  `client_bt_active` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_old`
--


-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(50) NOT NULL DEFAULT '',
  `contact_des` varchar(100) NOT NULL DEFAULT '',
  `contact_group_id` int(11) NOT NULL DEFAULT '0',
  `contact_phn_no` varchar(20) NOT NULL,
  `contact_type_id` int(3) NOT NULL,
  `contact_amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `contact_created_by` int(11) NOT NULL DEFAULT '0',
  `status_id` int(3) NOT NULL DEFAULT '1',
  `contact_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `contact_name`, `contact_des`, `contact_group_id`, `contact_phn_no`, `contact_type_id`, `contact_amount`, `contact_created_by`, `status_id`, `contact_delete`) VALUES
(1, 'I', '', 1, '01717931608', 1, 100.0000, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_group`
--

DROP TABLE IF EXISTS `contact_group`;
CREATE TABLE IF NOT EXISTS `contact_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_des` varchar(100) NOT NULL DEFAULT '',
  `group_created_by` int(11) NOT NULL DEFAULT '0',
  `contact_group_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact_group`
--

INSERT INTO `contact_group` (`id`, `group_name`, `group_des`, `group_created_by`, `contact_group_delete`) VALUES
(1, 'SMSGroup', '', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_group_schedule`
--

DROP TABLE IF EXISTS `contact_group_schedule`;
CREATE TABLE IF NOT EXISTS `contact_group_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_group_id` int(11) NOT NULL DEFAULT '0',
  `schedule_week_day` int(3) NOT NULL,
  `schedule_week_hour` int(3) NOT NULL,
  `schedule_week_minute` int(3) NOT NULL,
  `schedule_type` int(3) NOT NULL DEFAULT '0',
  `schedule_amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `schedule_executedat` decimal(18,0) NOT NULL DEFAULT '0',
  `schedule_description` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `contact_group_schedule`
--


-- --------------------------------------------------------

--
-- Table structure for table `dealer`
--

DROP TABLE IF EXISTS `dealer`;
CREATE TABLE IF NOT EXISTS `dealer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_name` varchar(100) NOT NULL DEFAULT '',
  `dealer_address` varchar(250) NOT NULL DEFAULT '',
  `dealer_phone` varchar(30) NOT NULL,
  `dealer_status` tinyint(1) DEFAULT '0',
  `dealer_create_date` decimal(18,0) DEFAULT '0',
  `dealer_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `dealer`
--


-- --------------------------------------------------------

--
-- Table structure for table `dealer_transaction`
--

DROP TABLE IF EXISTS `dealer_transaction`;
CREATE TABLE IF NOT EXISTS `dealer_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_dealer_id` int(11) NOT NULL,
  `transaction_type_id` int(3) NOT NULL DEFAULT '1',
  `transaction_amount` decimal(18,4) NOT NULL,
  `transaction_time` decimal(18,0) NOT NULL,
  `transaction_description` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `dealer_transaction`
--


-- --------------------------------------------------------

--
-- Table structure for table `inetload_api`
--

DROP TABLE IF EXISTS `inetload_api`;
CREATE TABLE IF NOT EXISTS `inetload_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` varchar(50) NOT NULL DEFAULT '',
  `api_link` varchar(100) NOT NULL DEFAULT '',
  `api_user` varchar(50) NOT NULL,
  `api_password` varchar(50) NOT NULL,
  `api_status` tinyint(1) DEFAULT '0',
  `api_gp_active` tinyint(1) DEFAULT '1',
  `api_rb_active` tinyint(1) DEFAULT '1',
  `api_bl_active` tinyint(1) DEFAULT '1',
  `api_tt_active` tinyint(1) DEFAULT '1',
  `api_cc_active` tinyint(1) DEFAULT '1',
  `api_wr_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `inetload_api`
--


-- --------------------------------------------------------

--
-- Table structure for table `inetload_fdr`
--

DROP TABLE IF EXISTS `inetload_fdr`;
CREATE TABLE IF NOT EXISTS `inetload_fdr` (
  `inetload_id` int(11) NOT NULL AUTO_INCREMENT,
  `operator_type_id` int(4) NOT NULL,
  `inetload_from_user_id` int(11) NOT NULL,
  `inetload_from_client_type_id` int(3) NOT NULL DEFAULT '2',
  `inetload_to_user_id` int(11) NOT NULL,
  `inetload_to_client_type_id` int(3) NOT NULL DEFAULT '1',
  `inetload_phn_no` varchar(20) NOT NULL,
  `inetload_type_id` int(3) NOT NULL,
  `inetload_amount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `inetload_gw_id` int(11) NOT NULL DEFAULT '0',
  `inetload_time` decimal(18,0) NOT NULL,
  `inetload_refill_success_time` decimal(18,0) NOT NULL DEFAULT '0',
  `inetload_processed_time` decimal(18,0) NOT NULL DEFAULT '0',
  `inetload_status` tinyint(1) NOT NULL DEFAULT '1',
  `inetload_processing_status` tinyint(1) NOT NULL DEFAULT '0',
  `inetload_marker` varchar(50) NOT NULL DEFAULT '',
  `inetload_transaction_id` varchar(250) DEFAULT '',
  `inetload_message` varchar(250) DEFAULT '',
  `card_id` int(11) NOT NULL DEFAULT '0',
  `status_changed` tinyint(1) DEFAULT '0',
  `inetload_contact_id` int(11) NOT NULL DEFAULT '0',
  `inetload_group_id` int(11) NOT NULL DEFAULT '0',
  `surcharge` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `account_type` decimal(3,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`inetload_id`),
  UNIQUE KEY `inetload_fdr_unique_key` (`inetload_from_user_id`,`inetload_to_user_id`,`inetload_marker`),
  KEY `operator_type_id_index` (`operator_type_id`),
  KEY `inetload_from_user_id_index` (`inetload_from_user_id`),
  KEY `inetload_to_user_id_index` (`inetload_to_user_id`),
  KEY `inetload_gw_id_index` (`inetload_gw_id`),
  KEY `inetload_time_index` (`inetload_time`),
  KEY `inetload_refill_success_time_index` (`inetload_refill_success_time`),
  KEY `inetload_status_index` (`inetload_status`),
  KEY `inetload_processing_status_index` (`inetload_processing_status`),
  KEY `inetload_marker_index` (`inetload_marker`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

--
-- Dumping data for table `inetload_fdr`
--

INSERT INTO `inetload_fdr` (`inetload_id`, `operator_type_id`, `inetload_from_user_id`, `inetload_from_client_type_id`, `inetload_to_user_id`, `inetload_to_client_type_id`, `inetload_phn_no`, `inetload_type_id`, `inetload_amount`, `inetload_gw_id`, `inetload_time`, `inetload_refill_success_time`, `inetload_processed_time`, `inetload_status`, `inetload_processing_status`, `inetload_marker`, `inetload_transaction_id`, `inetload_message`, `card_id`, `status_changed`, `inetload_contact_id`, `inetload_group_id`, `surcharge`, `account_type`) VALUES
(1, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378645261994, 1378716701928, 0, 2, 0, '1378645261994:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(2, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378645261994, 1378716701928, 0, 2, 0, '1378645261994:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(3, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378645282232, 1378716706954, 0, 2, 0, '1378645282232:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(4, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378645282232, 1378716706954, 0, 2, 0, '1378645282232:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(5, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378645293866, 1378716717031, 0, 2, 0, '1378645293866:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(6, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378645293866, 1378716717031, 0, 2, 0, '1378645293866:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(7, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378645326135, 1378718029350, 0, 2, 0, '1378645326135:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(8, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378645326135, 1378718029350, 0, 2, 0, '1378645326135:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(9, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378645344123, 1378718033937, 0, 2, 0, '1378645344123:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(10, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378645344123, 1378718033937, 0, 2, 0, '1378645344123:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(11, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378645509209, 1378718038119, 0, 2, 0, '1378645509209:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(12, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378645509209, 1378718038119, 0, 2, 0, '1378645509209:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(13, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378645598624, 0, 0, 3, 0, '1378645598624:01736861305', '', 'guj', 0, 0, 0, 0, 0.0000, 0),
(14, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378645598624, 0, 0, 3, 0, '1378645598624:01736861305', '', 'guj', 0, 0, 0, 0, 0.0000, 0),
(15, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378646240182, 0, 0, 3, 0, '1378646240182:01736861305', '', 'guj', 0, 0, 0, 0, 0.0000, 0),
(16, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378646240182, 0, 0, 3, 0, '1378646240182:01736861305', '', 'guj', 0, 0, 0, 0, 0.0000, 0),
(17, 9, 2, 2, 2, 2, '222', 0, 7.5000, 0, 1378647021057, 1378716724521, 0, 2, 0, '1378647021057:222', '', '', 0, 0, 0, 0, 0.0000, 0),
(18, 9, 2, 2, 1, 1, '222', 0, 7.5000, 0, 1378647021057, 1378716724521, 0, 2, 0, '1378647021057:222', '', '', 0, 0, 0, 0, 0.0000, 0),
(19, 9, 2, 2, 2, 2, '222', 0, 7.5000, 0, 1378647152615, 0, 0, 1, 0, '1378647152615:222', '', '2222', 0, 0, 0, 0, 0.0000, 0),
(20, 9, 2, 2, 1, 1, '222', 0, 7.5000, 0, 1378647152615, 0, 0, 1, 0, '1378647152615:222', '', '2222', 0, 0, 0, 0, 0.0000, 0),
(21, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378647325401, 0, 0, 3, 0, '1378647325401:01736861305', '', 'hijh', 0, 0, 0, 0, 0.0000, 0),
(22, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378647325401, 0, 0, 3, 0, '1378647325401:01736861305', '', 'hijh', 0, 0, 0, 0, 0.0000, 0),
(23, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378647560441, 0, 0, 3, 0, '1378647560441:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(24, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378647560441, 0, 0, 3, 0, '1378647560441:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(25, 9, 2, 2, 2, 2, '555555', 0, 2.5000, 0, 1378647587221, 0, 0, 1, 0, '1378647587221:555555', '', '222222', 0, 0, 0, 0, 0.0000, 0),
(26, 9, 2, 2, 1, 1, '555555', 0, 2.5000, 0, 1378647587221, 0, 0, 1, 0, '1378647587221:555555', '', '222222', 0, 0, 0, 0, 0.0000, 0),
(27, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378647642631, 0, 0, 1, 0, '1378647642631:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(28, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378647642631, 0, 0, 1, 0, '1378647642631:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(29, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378647697191, 0, 0, 1, 0, '1378647697191:01736861305', '', 'gii', 0, 0, 0, 0, 0.0000, 0),
(30, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378647697191, 0, 0, 1, 0, '1378647697191:01736861305', '', 'gii', 0, 0, 0, 0, 0.0000, 0),
(31, 9, 2, 2, 2, 2, '02736862305', 0, 2.5000, 0, 1378647812263, 0, 0, 1, 0, '1378647812263:02736862305', '', 'hji', 0, 0, 0, 0, 0.0000, 0),
(32, 9, 2, 2, 1, 1, '02736862305', 0, 2.5000, 0, 1378647812263, 0, 0, 1, 0, '1378647812263:02736862305', '', 'hji', 0, 0, 0, 0, 0.0000, 0),
(33, 9, 2, 2, 2, 2, '22333', 0, 2.5000, 0, 1378647877423, 0, 0, 1, 0, '1378647877423:22333', '', '222255588955', 0, 0, 0, 0, 0.0000, 0),
(34, 9, 2, 2, 1, 1, '22333', 0, 2.5000, 0, 1378647877423, 0, 0, 1, 0, '1378647877423:22333', '', '222255588955', 0, 0, 0, 0, 0.0000, 0),
(35, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378647921573, 0, 0, 1, 0, '1378647921573:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(36, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378647921574, 0, 0, 1, 0, '1378647921573:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(37, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378647970171, 0, 0, 1, 0, '1378647970171:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(38, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378647970171, 0, 0, 1, 0, '1378647970171:01736861305', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(39, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378648120240, 0, 0, 1, 0, '1378648120240:01736861305', '', 'hihg', 0, 0, 0, 0, 0.0000, 0),
(40, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378648120240, 0, 0, 1, 0, '1378648120240:01736861305', '', 'hihg', 0, 0, 0, 0, 0.0000, 0),
(41, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378701709822, 0, 0, 1, 0, '1378701709822:01736861305', '', 'gu', 0, 0, 0, 0, 0.0000, 0),
(42, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378701709822, 0, 0, 1, 0, '1378701709822:01736861305', '', 'gu', 0, 0, 0, 0, 0.0000, 0),
(43, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378701877149, 0, 0, 1, 0, '1378701877148:01736861305', '', 'gu', 0, 0, 0, 0, 0.0000, 0),
(44, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378701877149, 0, 0, 1, 0, '1378701877148:01736861305', '', 'gu', 0, 0, 0, 0, 0.0000, 0),
(45, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378701931362, 0, 0, 1, 0, '1378701931362:01736861305', '', 'hi', 0, 0, 0, 0, 0.0000, 0),
(46, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378701931362, 0, 0, 1, 0, '1378701931362:01736861305', '', 'hi', 0, 0, 0, 0, 0.0000, 0),
(47, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378702164186, 0, 0, 1, 0, '1378702164186:01736861305', '', 'hui', 0, 0, 0, 0, 0.0000, 0),
(48, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378702164186, 0, 0, 1, 0, '1378702164186:01736861305', '', 'hui', 0, 0, 0, 0, 0.0000, 0),
(49, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378702221245, 0, 0, 1, 0, '1378702221245:01736861305', '', 'hui', 0, 0, 0, 0, 0.0000, 0),
(50, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378702221245, 0, 0, 1, 0, '1378702221245:01736861305', '', 'hui', 0, 0, 0, 0, 0.0000, 0),
(51, 9, 2, 2, 2, 2, '81436861305', 0, 2.5000, 0, 1378702350323, 0, 0, 1, 0, '1378702350323:81436861305', '', 'hhi', 0, 0, 0, 0, 0.0000, 0),
(52, 9, 2, 2, 1, 1, '81436861305', 0, 2.5000, 0, 1378702350324, 0, 0, 1, 0, '1378702350323:81436861305', '', 'hhi', 0, 0, 0, 0, 0.0000, 0),
(53, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378702452573, 0, 0, 1, 0, '1378702452573:01736861305', '', 'hiiii', 0, 0, 0, 0, 0.0000, 0),
(54, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378702452573, 0, 0, 1, 0, '1378702452573:01736861305', '', 'hiiii', 0, 0, 0, 0, 0.0000, 0),
(55, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378702772194, 0, 0, 1, 0, '1378702772194:01736861305', '', 'hhuijgg', 0, 0, 0, 0, 0.0000, 0),
(56, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378702772194, 0, 0, 1, 0, '1378702772194:01736861305', '', 'hhuijgg', 0, 0, 0, 0, 0.0000, 0),
(57, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378702845118, 0, 0, 1, 0, '1378702845118:01736861305', '', 'guj', 0, 0, 0, 0, 0.0000, 0),
(58, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378702845118, 0, 0, 1, 0, '1378702845118:01736861305', '', 'guj', 0, 0, 0, 0, 0.0000, 0),
(59, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378703223616, 0, 0, 1, 0, '1378703223616:01736861305', '', 'gui', 0, 0, 0, 0, 0.0000, 0),
(60, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378703223616, 0, 0, 1, 0, '1378703223616:01736861305', '', 'gui', 0, 0, 0, 0, 0.0000, 0),
(61, 9, 2, 2, 2, 2, '01914641383', 0, 2.5000, 0, 1378703509245, 0, 0, 1, 0, '1378703509245:01914641383', '', 'thj', 0, 0, 0, 0, 0.0000, 0),
(62, 9, 2, 2, 1, 1, '01914641383', 0, 2.5000, 0, 1378703509245, 0, 0, 1, 0, '1378703509245:01914641383', '', 'thj', 0, 0, 0, 0, 0.0000, 0),
(63, 9, 2, 2, 2, 2, '01736861305', 0, 2.5000, 0, 1378703646824, 0, 0, 3, 0, '1378703646824:01736861305', '', 'gvh', 0, 0, 0, 0, 0.0000, 0),
(64, 9, 2, 2, 1, 1, '01736861305', 0, 2.5000, 0, 1378703646824, 0, 0, 3, 0, '1378703646824:01736861305', '', 'gvh', 0, 0, 0, 0, 0.0000, 0),
(65, 9, 2, 2, 2, 2, '81736861305', 9, 2.5000, 0, 1378707999357, 0, 0, 1, 0, '1378706138178:81736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(66, 9, 2, 2, 1, 1, '81736861305', 9, 2.5000, 0, 1378707999357, 0, 0, 1, 0, '1378706138178:81736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(69, 9, 2, 2, 2, 2, '01736861305', 9, 2.5000, 0, 1378708119033, 1378718024805, 0, 2, 0, '1378708119033:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(67, 1, 2, 2, 2, 2, '01728119927', 1, 22.0000, 0, 1378707320655, 0, 0, 1, 0, '1378707320655:01728119927', '', '', 0, 0, 0, 0, 0.0000, 0),
(68, 1, 2, 2, 1, 1, '01728119927', 1, 22.0000, 0, 1378707320655, 0, 0, 1, 0, '1378707320655:01728119927', '', '', 0, 0, 0, 0, 0.0000, 0),
(70, 9, 2, 2, 1, 1, '01736861305', 9, 2.5000, 0, 1378708119033, 1378718024805, 0, 2, 0, '1378708119033:01736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(87, 9, 2, 2, 2, 2, '+4055555', 9, 7.5000, 0, 1378718027844, 0, 0, 1, 0, '1378718027844:+4055555', '', '22222', 0, 0, 0, 0, 0.0000, 0),
(71, 9, 2, 2, 2, 2, '+8801736861305', 9, 2.5000, 0, 1378708251700, 0, 0, 1, 0, '1378708251700:+8801736861305', '', 'gujhh', 0, 0, 0, 0, 0.0000, 0),
(72, 9, 2, 2, 1, 1, '+8801736861305', 9, 2.5000, 0, 1378708251700, 0, 0, 1, 0, '1378708251700:+8801736861305', '', 'gujhh', 0, 0, 0, 0, 0.0000, 0),
(73, 9, 2, 2, 2, 2, '+8801736861305', 9, 2.5000, 0, 1378708272726, 1378708474428, 0, 2, 0, '1378708272726:+8801736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(74, 9, 2, 2, 1, 1, '+8801736861305', 9, 2.5000, 0, 1378708272726, 1378708474428, 0, 2, 0, '1378708272726:+8801736861305', '', '', 0, 0, 0, 0, 0.0000, 0),
(75, 3, 2, 2, 2, 2, '01670854678', 1, 50.0000, 0, 1378709352079, 0, 0, 1, 0, '1378709352079:01670854678', '', '', 0, 0, 0, 0, 0.0000, 0),
(76, 3, 2, 2, 1, 1, '01670854678', 1, 50.0000, 0, 1378709352079, 0, 0, 1, 0, '1378709352079:01670854678', '', '', 0, 0, 0, 0, 0.0000, 0),
(77, 1, 2, 2, 2, 2, '01736861382', 1, 100.0000, 0, 1378709363456, 0, 0, 1, 0, '1378709363456:01736861382', '', '', 0, 0, 0, 0, 0.0000, 0),
(78, 1, 2, 2, 1, 1, '01736861382', 1, 100.0000, 0, 1378709363456, 0, 0, 1, 0, '1378709363456:01736861382', '', '', 0, 0, 0, 0, 0.0000, 0),
(79, 8, 2, 2, 2, 2, '01670854678', 1, 500.0000, 0, 1378709375641, 0, 0, 3, 0, '1378709375641:01670854678', '', '', 0, 0, 0, 0, 35.0000, 1),
(80, 8, 2, 2, 1, 1, '01670854678', 1, 500.0000, 0, 1378709375641, 0, 0, 3, 0, '1378709375641:01670854678', '', '', 0, 0, 0, 0, 35.0000, 1),
(81, 8, 2, 2, 2, 2, '01736861306', 1, 100.0000, 0, 1378709448518, 0, 0, 1, 0, '1378709448518:01736861306', '', '', 0, 0, 0, 0, 7.0000, 1),
(82, 8, 2, 2, 1, 1, '01736861306', 1, 100.0000, 0, 1378709448518, 0, 0, 1, 0, '1378709448518:01736861306', '', '', 0, 0, 0, 0, 7.0000, 1),
(83, 8, 2, 2, 2, 2, '01736861305', 1, 100.0000, 0, 1378709459116, 1378711673518, 0, 2, 0, '1378709459116:01736861305', '', '', 0, 0, 0, 0, 7.0000, 1),
(84, 8, 2, 2, 1, 1, '01736861305', 1, 100.0000, 0, 1378709459116, 1378711673518, 0, 2, 0, '1378709459116:01736861305', '', '', 0, 0, 0, 0, 7.0000, 1),
(85, 9, 2, 2, 2, 2, '+8801685923922', 9, 2.5000, 0, 1378709884895, 0, 0, 1, 0, '1378709884895:+8801685923922', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(86, 9, 2, 2, 1, 1, '+8801685923922', 9, 2.5000, 0, 1378709884895, 0, 0, 1, 0, '1378709884895:+8801685923922', '', 'hii', 0, 0, 0, 0, 0.0000, 0),
(88, 9, 2, 2, 1, 1, '+4055555', 9, 7.5000, 0, 1378718027844, 0, 0, 1, 0, '1378718027844:+4055555', '', '22222', 0, 0, 0, 0, 0.0000, 0),
(89, 9, 2, 2, 2, 2, '+8801670854678', 9, 2.5000, 0, 1378719777604, 0, 0, 1, 0, '1378719777604:+8801670854678', '', 'asda dasd asd', 0, 0, 0, 0, 0.0000, 0),
(90, 9, 2, 2, 1, 1, '+8801670854678', 9, 2.5000, 0, 1378719777604, 0, 0, 1, 0, '1378719777604:+8801670854678', '', 'asda dasd asd', 0, 0, 0, 0, 0.0000, 0),
(91, 9, 2, 2, 2, 2, '+8801670854678', 9, 2.5000, 0, 1378720137783, 0, 0, 1, 0, '1378720137783:+8801670854678', '', 'Hello world', 0, 0, 0, 0, 0.0000, 0),
(92, 9, 2, 2, 1, 1, '+8801670854678', 9, 2.5000, 0, 1378720137783, 0, 0, 1, 0, '1378720137783:+8801670854678', '', 'Hello world', 0, 0, 0, 0, 0.0000, 0),
(93, 3, 2, 2, 2, 2, '01670854678', 2, 80.0000, 0, 1378720352914, 0, 0, 1, 0, '1378720352903:01670854678', '', '', 0, 0, 0, 0, 0.0000, 0),
(94, 3, 2, 2, 1, 1, '01670854678', 2, 80.0000, 0, 1378720352914, 0, 0, 1, 0, '1378720352903:01670854678', '', '', 0, 0, 0, 0, 0.0000, 0),
(95, 3, 2, 2, 2, 2, '01685923922', 1, 1000.0000, 0, 1378721484605, 0, 0, 1, 0, '1378721484605:01685923922', '', '', 0, 0, 0, 0, 0.0000, 0),
(96, 3, 2, 2, 1, 1, '01685923922', 1, 1000.0000, 0, 1378721484605, 0, 0, 1, 0, '1378721484605:01685923922', '', '', 0, 0, 0, 0, 0.0000, 0),
(97, 8, 2, 2, 2, 2, '01736861305', 1, 100.0000, 0, 1378722759589, 0, 0, 1, 0, '1378722759589:01736861305', '', '', 0, 0, 0, 0, 7.0000, 1),
(98, 8, 2, 2, 1, 1, '01736861305', 1, 100.0000, 0, 1378722759589, 0, 0, 1, 0, '1378722759589:01736861305', '', '', 0, 0, 0, 0, 7.0000, 1),
(99, 8, 2, 2, 2, 2, '0191485753', 1, 10.0000, 0, 1378723324980, 0, 0, 1, 0, '1378723324980:0191485753', '', '', 0, 0, 0, 0, 0.7000, 1),
(100, 8, 2, 2, 1, 1, '0191485753', 1, 10.0000, 0, 1378723324980, 0, 0, 1, 0, '1378723324980:0191485753', '', '', 0, 0, 0, 0, 0.7000, 1),
(101, 3, 2, 2, 2, 2, '01685923922', 1, 100.0000, 0, 1378723375180, 0, 0, 1, 0, '1378723375180:01685923922', '', '', 0, 0, 0, 0, 0.0000, 0),
(102, 3, 2, 2, 1, 1, '01685923922', 1, 100.0000, 0, 1378723375180, 0, 0, 1, 0, '1378723375180:01685923922', '', '', 0, 0, 0, 0, 0.0000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inetload_gateway`
--

DROP TABLE IF EXISTS `inetload_gateway`;
CREATE TABLE IF NOT EXISTS `inetload_gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operator_id` decimal(3,0) NOT NULL,
  `sim_id` varchar(25) NOT NULL,
  `phone_number` varchar(20) NOT NULL DEFAULT '',
  `pin` varchar(15) NOT NULL DEFAULT '',
  `balance` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `status_id` decimal(3,0) NOT NULL DEFAULT '0',
  `update_time` decimal(18,0) NOT NULL DEFAULT '0',
  `version` int(3) NOT NULL DEFAULT '0',
  `dealer_id` int(11) NOT NULL DEFAULT '0',
  `sim_type` decimal(3,0) NOT NULL DEFAULT '2',
  `query` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `inetload_gateway`
--


-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_from_client_type_id` int(3) NOT NULL,
  `payment_to_client_type_id` int(3) NOT NULL,
  `payment_from_client_id` int(11) NOT NULL,
  `payment_to_client_id` int(11) NOT NULL,
  `payment_type_id` int(3) NOT NULL DEFAULT '1',
  `payment_amount` decimal(18,4) NOT NULL,
  `payment_time` decimal(18,0) NOT NULL,
  `payment_description` varchar(100) NOT NULL DEFAULT '',
  `payment_marker` varchar(50) NOT NULL DEFAULT '',
  `payment_discount` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `payment_amount_temp` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `payment_discount_temp` decimal(18,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `payment_from_client_type_id`, `payment_to_client_type_id`, `payment_from_client_id`, `payment_to_client_id`, `payment_type_id`, `payment_amount`, `payment_time`, `payment_description`, `payment_marker`, `payment_discount`, `payment_amount_temp`, `payment_discount_temp`) VALUES
(1, 0, 1, 0, 1, 5, 0.0000, 1378277677531, 'Initial Credit', '1378277677531:4465263332857638950', 0.0000, 1000.0000, 10.0000),
(2, 0, 1, 0, 1, 1, 5000.0000, 1378626150859, ' ', '1378626150859:-4849942666906859358', 50.0000, -1.0000, 50.0000),
(3, 1, 2, 1, 2, 1, 3000.0000, 1378626220594, ' ', '1378626220594:-3142077169549573348', 0.0000, -1.0000, 0.0000),
(4, 0, 1, 0, 3, 5, 500.0000, 1378631356685, 'Initial Credit', '1378631356685:-6602269048391074220', 0.0000, -1.0000, 0.0000);

-- --------------------------------------------------------

--
-- Table structure for table `rechargecard`
--

DROP TABLE IF EXISTS `rechargecard`;
CREATE TABLE IF NOT EXISTS `rechargecard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_no` varchar(100) NOT NULL,
  `serial_no` int(11) NOT NULL,
  `status_id` decimal(3,0) NOT NULL DEFAULT '0',
  `country_id` decimal(3,0) NOT NULL DEFAULT '0',
  `package_id` decimal(3,0) NOT NULL DEFAULT '0',
  `card_balance` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `activated_time` decimal(18,0) NOT NULL DEFAULT '0',
  `activated_by` int(11) NOT NULL DEFAULT '0',
  `blocked_time` decimal(18,0) NOT NULL DEFAULT '0',
  `blocked_by` int(11) NOT NULL DEFAULT '0',
  `group_identifier` varchar(75) NOT NULL,
  `card_price` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `card_shop` int(11) NOT NULL DEFAULT '0',
  `card_deleted` tinyint(1) DEFAULT '0',
  `refill_no` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rechargecard`
--


-- --------------------------------------------------------

--
-- Table structure for table `rechargecard_group`
--

DROP TABLE IF EXISTS `rechargecard_group`;
CREATE TABLE IF NOT EXISTS `rechargecard_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL DEFAULT '',
  `group_des` varchar(250) NOT NULL DEFAULT '',
  `group_prefix` varchar(10) NOT NULL DEFAULT '',
  `group_created_time` decimal(18,0) NOT NULL DEFAULT '0',
  `group_balance` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `group_created_by` int(11) NOT NULL DEFAULT '0',
  `group_total_cards` decimal(10,0) NOT NULL,
  `group_card_length` decimal(6,0) NOT NULL,
  `group_country` decimal(3,0) NOT NULL DEFAULT '0',
  `group_package` decimal(3,0) NOT NULL DEFAULT '0',
  `group_identifier` varchar(75) NOT NULL,
  `group_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `rechargecard_group`
--


-- --------------------------------------------------------

--
-- Stand-in structure for view `reseller`
--
DROP VIEW IF EXISTS `reseller`;
CREATE TABLE IF NOT EXISTS `reseller` (
`id` int(11)
,`client_id` varchar(50)
,`client_type_id` int(3)
,`client_parent_id` int(11)
,`client_full_name` varchar(100)
,`client_email` varchar(50)
,`client_password` varchar(32)
,`client_credit_all` decimal(18,4)
,`client_status` tinyint(1)
,`client_reg_time` decimal(18,0)
,`client_deleted` tinyint(1)
,`client_version` int(3)
,`client_ip` varchar(200)
,`client_previous_balance` decimal(18,4)
,`client_discount` decimal(18,4)
,`client_deposit` decimal(18,4)
,`client_gp_active` tinyint(1)
,`client_rb_active` tinyint(1)
,`client_bl_active` tinyint(1)
,`client_tt_active` tinyint(1)
,`client_cc_active` tinyint(1)
,`client_wr_active` tinyint(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `reseller2`
--
DROP VIEW IF EXISTS `reseller2`;
CREATE TABLE IF NOT EXISTS `reseller2` (
`id` int(11)
,`client_id` varchar(50)
,`client_type_id` int(3)
,`client_parent_id` int(11)
,`client_full_name` varchar(100)
,`client_email` varchar(50)
,`client_password` varchar(32)
,`client_credit_all` decimal(18,4)
,`client_status` tinyint(1)
,`client_reg_time` decimal(18,0)
,`client_deleted` tinyint(1)
,`client_version` int(3)
,`client_ip` varchar(200)
,`client_previous_balance` decimal(18,4)
,`client_discount` decimal(18,4)
,`client_deposit` decimal(18,4)
,`client_gp_active` tinyint(1)
,`client_rb_active` tinyint(1)
,`client_bl_active` tinyint(1)
,`client_tt_active` tinyint(1)
,`client_cc_active` tinyint(1)
,`client_wr_active` tinyint(1)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `reseller3`
--
DROP VIEW IF EXISTS `reseller3`;
CREATE TABLE IF NOT EXISTS `reseller3` (
`id` int(11)
,`client_id` varchar(50)
,`client_type_id` int(3)
,`client_parent_id` int(11)
,`client_full_name` varchar(100)
,`client_email` varchar(50)
,`client_password` varchar(32)
,`client_credit_all` decimal(18,4)
,`client_status` tinyint(1)
,`client_reg_time` decimal(18,0)
,`client_deleted` tinyint(1)
,`client_version` int(3)
,`client_ip` varchar(200)
,`client_previous_balance` decimal(18,4)
,`client_discount` decimal(18,4)
,`client_deposit` decimal(18,4)
,`client_gp_active` tinyint(1)
,`client_rb_active` tinyint(1)
,`client_bl_active` tinyint(1)
,`client_tt_active` tinyint(1)
,`client_cc_active` tinyint(1)
,`client_wr_active` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` varchar(50) NOT NULL,
  `role_description` varchar(100) DEFAULT NULL,
  `role_created_by` int(11) NOT NULL,
  `role_permission_id` varchar(70) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `role`
--


-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE IF NOT EXISTS `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_permission_id` varchar(70) NOT NULL,
  `role_permission_item_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `role_permission`
--


-- --------------------------------------------------------

--
-- Table structure for table `scratchcard`
--

DROP TABLE IF EXISTS `scratchcard`;
CREATE TABLE IF NOT EXISTS `scratchcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) NOT NULL DEFAULT '',
  `refill_type` varchar(50) NOT NULL DEFAULT '',
  `refill_amount` varchar(50) NOT NULL DEFAULT '',
  `serial_no` varchar(50) NOT NULL DEFAULT '',
  `pin` varchar(50) NOT NULL DEFAULT '',
  `status_id` tinyint(1) DEFAULT '0',
  `used_by_id` int(11) NOT NULL DEFAULT '0',
  `used_time` decimal(18,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `scratchcard`
--


-- --------------------------------------------------------

--
-- Table structure for table `scratchcard_type`
--

DROP TABLE IF EXISTS `scratchcard_type`;
CREATE TABLE IF NOT EXISTS `scratchcard_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) NOT NULL DEFAULT '',
  `supported_refill_types` varchar(100) NOT NULL DEFAULT '',
  `supported_refill_amounts` varchar(100) NOT NULL DEFAULT '',
  `status_id` tinyint(1) DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `scratchcard_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `session_values`
--

DROP TABLE IF EXISTS `session_values`;
CREATE TABLE IF NOT EXISTS `session_values` (
  `user_name` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `time` varchar(100) NOT NULL DEFAULT '',
  `current_time` decimal(18,0) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_values`
--


-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `prepaid_min_refill_amount` decimal(18,4) NOT NULL DEFAULT '10.0000',
  `postpaid_min_refill_amount` decimal(18,4) NOT NULL DEFAULT '50.0000',
  `prepaid_max_refill_amount` decimal(18,4) NOT NULL DEFAULT '1000.0000',
  `postpaid_max_refill_amount` decimal(18,4) NOT NULL DEFAULT '5000.0000',
  `min_flexi_sim_balance` decimal(18,4) NOT NULL DEFAULT '500.0000',
  `min_allowed_discount` decimal(18,4) NOT NULL DEFAULT '-3.0000',
  `max_allowed_discount` decimal(18,4) NOT NULL DEFAULT '3.0000',
  `same_number_refill_interval` int(11) NOT NULL DEFAULT '15',
  `offset_from_server_time` int(11) NOT NULL DEFAULT '0',
  `record_per_page_for_reporting` int(11) NOT NULL DEFAULT '100',
  `sim_message_log_restore_interval` int(11) NOT NULL DEFAULT '7',
  `data_backup_time` int(11) NOT NULL DEFAULT '15',
  `notify_after_pending` int(11) NOT NULL DEFAULT '25',
  `pending_time_for_auto_system` int(11) NOT NULL DEFAULT '6',
  `pending_time_for_manual_system` int(11) NOT NULL DEFAULT '24',
  `emergency_notice_text` varchar(500) NOT NULL DEFAULT '',
  `brand_name` varchar(50) NOT NULL DEFAULT '',
  `motto` varchar(100) NOT NULL DEFAULT '',
  `welcome_note` varchar(250) NOT NULL DEFAULT '',
  `copyright_link` varchar(150) NOT NULL DEFAULT '',
  `contact_email` varchar(100) NOT NULL DEFAULT '',
  `contact_email_pass` varchar(50) NOT NULL DEFAULT '',
  `notification_email` varchar(100) NOT NULL DEFAULT '',
  `last_log_id` decimal(18,0) NOT NULL DEFAULT '0',
  `last_payment_id` decimal(18,0) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`prepaid_min_refill_amount`, `postpaid_min_refill_amount`, `prepaid_max_refill_amount`, `postpaid_max_refill_amount`, `min_flexi_sim_balance`, `min_allowed_discount`, `max_allowed_discount`, `same_number_refill_interval`, `offset_from_server_time`, `record_per_page_for_reporting`, `sim_message_log_restore_interval`, `data_backup_time`, `notify_after_pending`, `pending_time_for_auto_system`, `pending_time_for_manual_system`, `emergency_notice_text`, `brand_name`, `motto`, `welcome_note`, `copyright_link`, `contact_email`, `contact_email_pass`, `notification_email`, `last_log_id`, `last_payment_id`) VALUES
(10.0000, 50.0000, 1000.0000, 5000.0000, 500.0000, -3.0000, 3.0000, 15, 0, 100, 7, 15, 25, 6, 24, '', 'iNetLoad', 'Best Solution for Your Online Mobile Refill System', 'Welcome to iNetLoad', 'http://inetload.com', 'flammabd@gmail.com', 'flamma@sylhet', 'support@flammabd.com', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sim_log`
--

DROP TABLE IF EXISTS `sim_log`;
CREATE TABLE IF NOT EXISTS `sim_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_id` int(11) NOT NULL DEFAULT '0',
  `message` varchar(250) DEFAULT '',
  `log_time` decimal(18,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `log_time_index` (`log_time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sim_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `sim_recharge`
--

DROP TABLE IF EXISTS `sim_recharge`;
CREATE TABLE IF NOT EXISTS `sim_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_id` int(11) NOT NULL DEFAULT '0',
  `recharge_amount` decimal(18,4) NOT NULL,
  `recharge_time` decimal(18,0) NOT NULL DEFAULT '0',
  `recharge_description` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sim_recharge`
--


-- --------------------------------------------------------

--
-- Table structure for table `sms_log`
--

DROP TABLE IF EXISTS `sms_log`;
CREATE TABLE IF NOT EXISTS `sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_id` int(11) NOT NULL DEFAULT '0',
  `sms_type` int(11) NOT NULL DEFAULT '0',
  `mobile_no` varchar(20) NOT NULL DEFAULT '',
  `sms` varchar(250) NOT NULL DEFAULT '',
  `log_time` decimal(18,0) NOT NULL DEFAULT '0',
  `processing_status` tinyint(1) NOT NULL DEFAULT '0',
  `request_marker` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sms_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `sms_rateplan`
--

DROP TABLE IF EXISTS `sms_rateplan`;
CREATE TABLE IF NOT EXISTS `sms_rateplan` (
  `rateplan_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `rate_amount` decimal(10,4) NOT NULL,
  `rate_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rateplan_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sms_rateplan`
--

INSERT INTO `sms_rateplan` (`rateplan_id`, `country_name`, `country_code`, `rate_amount`, `rate_status`) VALUES
(1, 'BD', '+88', 2.5000, 1),
(2, 'USA', '+40', 7.5000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL DEFAULT '',
  `user_full_name` varchar(100) NOT NULL DEFAULT '',
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_status` tinyint(1) DEFAULT '0',
  `user_reg_time` decimal(18,0) DEFAULT '0',
  `user_role_id` int(11) NOT NULL,
  `user_createdBy_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_id`, `user_full_name`, `user_email`, `user_password`, `user_status`, `user_reg_time`, `user_role_id`, `user_createdBy_id`) VALUES
(1, 'admin', 'Flamma', 'flamma@flammabd.com', 'aaaaaa', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure for view `reseller`
--
DROP TABLE IF EXISTS `reseller`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reseller` AS select `client`.`id` AS `id`,`client`.`client_id` AS `client_id`,`client`.`client_type_id` AS `client_type_id`,`client`.`client_parent_id` AS `client_parent_id`,`client`.`client_full_name` AS `client_full_name`,`client`.`client_email` AS `client_email`,`client`.`client_password` AS `client_password`,`client`.`client_credit_all` AS `client_credit_all`,`client`.`client_status` AS `client_status`,`client`.`client_reg_time` AS `client_reg_time`,`client`.`client_deleted` AS `client_deleted`,`client`.`client_version` AS `client_version`,`client`.`client_ip` AS `client_ip`,`client`.`client_previous_balance` AS `client_previous_balance`,`client`.`client_discount` AS `client_discount`,`client`.`client_deposit` AS `client_deposit`,`client`.`client_gp_active` AS `client_gp_active`,`client`.`client_rb_active` AS `client_rb_active`,`client`.`client_bl_active` AS `client_bl_active`,`client`.`client_tt_active` AS `client_tt_active`,`client`.`client_cc_active` AS `client_cc_active`,`client`.`client_wr_active` AS `client_wr_active` from `client` where (`client`.`client_type_id` = 1);

-- --------------------------------------------------------

--
-- Structure for view `reseller2`
--
DROP TABLE IF EXISTS `reseller2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reseller2` AS select `client`.`id` AS `id`,`client`.`client_id` AS `client_id`,`client`.`client_type_id` AS `client_type_id`,`client`.`client_parent_id` AS `client_parent_id`,`client`.`client_full_name` AS `client_full_name`,`client`.`client_email` AS `client_email`,`client`.`client_password` AS `client_password`,`client`.`client_credit_all` AS `client_credit_all`,`client`.`client_status` AS `client_status`,`client`.`client_reg_time` AS `client_reg_time`,`client`.`client_deleted` AS `client_deleted`,`client`.`client_version` AS `client_version`,`client`.`client_ip` AS `client_ip`,`client`.`client_previous_balance` AS `client_previous_balance`,`client`.`client_discount` AS `client_discount`,`client`.`client_deposit` AS `client_deposit`,`client`.`client_gp_active` AS `client_gp_active`,`client`.`client_rb_active` AS `client_rb_active`,`client`.`client_bl_active` AS `client_bl_active`,`client`.`client_tt_active` AS `client_tt_active`,`client`.`client_cc_active` AS `client_cc_active`,`client`.`client_wr_active` AS `client_wr_active` from `client` where (`client`.`client_type_id` = 4);

-- --------------------------------------------------------

--
-- Structure for view `reseller3`
--
DROP TABLE IF EXISTS `reseller3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `reseller3` AS select `client`.`id` AS `id`,`client`.`client_id` AS `client_id`,`client`.`client_type_id` AS `client_type_id`,`client`.`client_parent_id` AS `client_parent_id`,`client`.`client_full_name` AS `client_full_name`,`client`.`client_email` AS `client_email`,`client`.`client_password` AS `client_password`,`client`.`client_credit_all` AS `client_credit_all`,`client`.`client_status` AS `client_status`,`client`.`client_reg_time` AS `client_reg_time`,`client`.`client_deleted` AS `client_deleted`,`client`.`client_version` AS `client_version`,`client`.`client_ip` AS `client_ip`,`client`.`client_previous_balance` AS `client_previous_balance`,`client`.`client_discount` AS `client_discount`,`client`.`client_deposit` AS `client_deposit`,`client`.`client_gp_active` AS `client_gp_active`,`client`.`client_rb_active` AS `client_rb_active`,`client`.`client_bl_active` AS `client_bl_active`,`client`.`client_tt_active` AS `client_tt_active`,`client`.`client_cc_active` AS `client_cc_active`,`client`.`client_wr_active` AS `client_wr_active` from `client` where (`client`.`client_type_id` = 3);
